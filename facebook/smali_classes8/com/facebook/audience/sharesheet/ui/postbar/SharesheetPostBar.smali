.class public Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static d:F

.field public static e:I

.field public static f:I

.field public static g:I


# instance fields
.field public a:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/4mR;

.field public c:LX/AJq;

.field private h:LX/4mU;

.field public i:Landroid/support/v7/widget/RecyclerView;

.field private j:Lcom/facebook/fbui/glyph/GlyphButton;

.field public k:Landroid/view/View;

.field public l:Z

.field public m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1662259
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1662260
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662261
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662262
    new-instance v0, LX/AJp;

    invoke-direct {v0, p0}, LX/AJp;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->b:LX/4mR;

    .line 1662263
    new-instance v0, LX/AJr;

    invoke-direct {v0, p0}, LX/AJr;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->c:LX/AJq;

    .line 1662264
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d()V

    .line 1662265
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1662252
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1662253
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662254
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662255
    new-instance v0, LX/AJp;

    invoke-direct {v0, p0}, LX/AJp;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->b:LX/4mR;

    .line 1662256
    new-instance v0, LX/AJr;

    invoke-direct {v0, p0}, LX/AJr;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->c:LX/AJq;

    .line 1662257
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d()V

    .line 1662258
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1662245
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1662246
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662247
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662248
    new-instance v0, LX/AJp;

    invoke-direct {v0, p0}, LX/AJp;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->b:LX/4mR;

    .line 1662249
    new-instance v0, LX/AJr;

    invoke-direct {v0, p0}, LX/AJr;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->c:LX/AJq;

    .line 1662250
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d()V

    .line 1662251
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    invoke-static {v0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v0

    check-cast v0, LX/4mV;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->a:LX/4mV;

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1662230
    const-class v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    invoke-static {v0, p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1662231
    const v0, 0x7f031319

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1662232
    const v0, 0x7f0d2c5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->k:Landroid/view/View;

    .line 1662233
    const v0, 0x7f0d2c5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1662234
    const v0, 0x7f0d2c5c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->i:Landroid/support/v7/widget/RecyclerView;

    .line 1662235
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->i:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1662236
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1b88

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->e:I

    .line 1662237
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1b92

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->f:I

    .line 1662238
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1b90

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d:F

    .line 1662239
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->a:LX/4mV;

    invoke-virtual {v0, p0}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    .line 1662240
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 1662241
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    sget v1, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d:F

    invoke-virtual {v0, v1}, LX/4mU;->i(F)V

    .line 1662242
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->b:LX/4mR;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1662243
    new-instance v0, LX/AJs;

    invoke-direct {v0, p0}, LX/AJs;-><init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V

    invoke-virtual {p0, v0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1662244
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1662181
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    if-eqz v0, :cond_0

    .line 1662182
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    invoke-virtual {v0}, LX/4mU;->a()V

    .line 1662183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662184
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->i(F)V

    .line 1662185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662186
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1662221
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    if-eqz v0, :cond_1

    .line 1662222
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    invoke-virtual {v0}, LX/4mU;->a()V

    .line 1662223
    iput-boolean v2, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662224
    iput-boolean v2, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662225
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1662226
    :cond_0
    :goto_0
    return-void

    .line 1662227
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    if-eqz v0, :cond_0

    .line 1662228
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    if-nez v0, :cond_0

    .line 1662229
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1662215
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    if-eqz v0, :cond_0

    .line 1662216
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    invoke-virtual {v0}, LX/4mU;->a()V

    .line 1662217
    iput-boolean v2, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662218
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    sget v1, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d:F

    invoke-virtual {v0, v1}, LX/4mU;->i(F)V

    .line 1662219
    iput-boolean v2, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662220
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1662206
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    if-nez v0, :cond_1

    .line 1662207
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    if-nez v0, :cond_2

    .line 1662208
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    invoke-virtual {v0}, LX/4mU;->a()V

    .line 1662209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->l:Z

    .line 1662210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    .line 1662211
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    sget v1, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d:F

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1662212
    :cond_1
    :goto_0
    return-void

    .line 1662213
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->m:Z

    if-eqz v0, :cond_1

    .line 1662214
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h:LX/4mU;

    sget v1, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->d:F

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/AJt;Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x96

    .line 1662195
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1662196
    new-instance v0, LX/AJo;

    invoke-direct {v0}, LX/AJo;-><init>()V

    .line 1662197
    iput-wide v2, v0, LX/1Of;->c:J

    .line 1662198
    iput-wide v2, v0, LX/1Of;->d:J

    .line 1662199
    iput-wide v2, v0, LX/1Of;->e:J

    .line 1662200
    iput-wide v2, v0, LX/1Of;->f:J

    .line 1662201
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1662202
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1662203
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->c:LX/AJq;

    .line 1662204
    iput-object v0, p1, LX/AJt;->c:LX/AJq;

    .line 1662205
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1662191
    if-eqz p1, :cond_0

    .line 1662192
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->f()V

    .line 1662193
    :goto_0
    return-void

    .line 1662194
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->e()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1662187
    if-eqz p1, :cond_0

    .line 1662188
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->h()V

    .line 1662189
    :goto_0
    return-void

    .line 1662190
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->g()V

    goto :goto_0
.end method
