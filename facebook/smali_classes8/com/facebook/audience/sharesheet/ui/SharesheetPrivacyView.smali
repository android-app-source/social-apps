.class public Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1662050
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1662051
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b()V

    .line 1662052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1662047
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1662048
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b()V

    .line 1662049
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    invoke-static {v0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v0

    check-cast v0, LX/8Sa;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->a:LX/8Sa;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1662041
    const-class v0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    invoke-static {v0, p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1662042
    const v0, 0x7f03131a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1662043
    const v0, 0x7f0d2c5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1662044
    const v0, 0x7f0d2c60

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1662045
    const v0, 0x7f0d2c61

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1662046
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1662036
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082881

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1662037
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020914

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1662038
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1662039
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1662040
    return-void
.end method

.method public setPrivacy(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 1662027
    if-nez p1, :cond_0

    .line 1662028
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082882

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1662029
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1662030
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1662031
    :goto_0
    return-void

    .line 1662032
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1662033
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->a:LX/8Sa;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    sget-object v3, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v1, v2, v3}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1662034
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1662035
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method
