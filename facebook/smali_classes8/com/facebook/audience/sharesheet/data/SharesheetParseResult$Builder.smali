.class public final Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1661307
    const-class v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1661286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1661287
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1661288
    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->i:LX/0Px;

    .line 1661289
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;
    .locals 2

    .prologue
    .line 1661306
    new-instance v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    invoke-direct {v0, p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;-><init>(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;)V

    return-object v0
.end method

.method public setInspirationGroupSessionId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_group_session_id"
    .end annotation

    .prologue
    .line 1661304
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->a:Ljava/lang/String;

    .line 1661305
    return-object p0
.end method

.method public setIsMyDaySelected(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_my_day_selected"
    .end annotation

    .prologue
    .line 1661302
    iput-boolean p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->b:Z

    .line 1661303
    return-object p0
.end method

.method public setIsNewsFeedSelected(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_news_feed_selected"
    .end annotation

    .prologue
    .line 1661300
    iput-boolean p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->c:Z

    .line 1661301
    return-object p0
.end method

.method public setIsVideo(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_video"
    .end annotation

    .prologue
    .line 1661308
    iput-boolean p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->d:Z

    .line 1661309
    return-object p0
.end method

.method public setMediaContentId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_content_id"
    .end annotation

    .prologue
    .line 1661298
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->e:Ljava/lang/String;

    .line 1661299
    return-object p0
.end method

.method public setPromptId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_id"
    .end annotation

    .prologue
    .line 1661296
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->f:Ljava/lang/String;

    .line 1661297
    return-object p0
.end method

.method public setSearchQuery(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "search_query"
    .end annotation

    .prologue
    .line 1661294
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->g:Ljava/lang/String;

    .line 1661295
    return-object p0
.end method

.method public setSelectablePrivacyData(Lcom/facebook/privacy/model/SelectablePrivacyData;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .param p1    # Lcom/facebook/privacy/model/SelectablePrivacyData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selectable_privacy_data"
    .end annotation

    .prologue
    .line 1661292
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1661293
    return-object p0
.end method

.method public setSelectedAudience(LX/0Px;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_audience"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;)",
            "Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;"
        }
    .end annotation

    .prologue
    .line 1661290
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->i:LX/0Px;

    .line 1661291
    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 1661284
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->j:Ljava/lang/String;

    .line 1661285
    return-object p0
.end method
