.class public final Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1661310
    new-instance v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Deserializer;->a:Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1661311
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1661312
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;
    .locals 1

    .prologue
    .line 1661313
    sget-object v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Deserializer;->a:Lcom/facebook/audience/sharesheet/data/SharesheetParseResult_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    .line 1661314
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->a()Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1661315
    invoke-static {p1, p2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    move-result-object v0

    return-object v0
.end method
