.class public Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1661436
    const-class v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    new-instance v1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;

    invoke-direct {v1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1661437
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1661438
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1661439
    if-nez p0, :cond_0

    .line 1661440
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1661441
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1661442
    invoke-static {p0, p1, p2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;->b(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;LX/0nX;LX/0my;)V

    .line 1661443
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1661444
    return-void
.end method

.method private static b(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1661445
    const-string v0, "inspiration_group_session_id"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getInspirationGroupSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1661446
    const-string v0, "is_my_day_selected"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isMyDaySelected()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1661447
    const-string v0, "is_news_feed_selected"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isNewsFeedSelected()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1661448
    const-string v0, "is_video"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isVideo()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1661449
    const-string v0, "media_content_id"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getMediaContentId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1661450
    const-string v0, "prompt_id"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getPromptId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1661451
    const-string v0, "search_query"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1661452
    const-string v0, "selectable_privacy_data"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSelectablePrivacyData()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1661453
    const-string v0, "selected_audience"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSelectedAudience()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1661454
    const-string v0, "session_id"

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1661455
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1661456
    check-cast p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;->a(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;LX/0nX;LX/0my;)V

    return-void
.end method
