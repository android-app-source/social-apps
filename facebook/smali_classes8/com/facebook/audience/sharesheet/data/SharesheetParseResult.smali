.class public Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1661409
    const-class v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1661362
    const-class v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResultSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1661363
    new-instance v0, LX/AJY;

    invoke-direct {v0}, LX/AJY;-><init>()V

    sput-object v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1661364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1661365
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1661366
    iput-object v3, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    .line 1661367
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    .line 1661368
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    .line 1661369
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    .line 1661370
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1661371
    iput-object v3, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    .line 1661372
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1661373
    iput-object v3, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    .line 1661374
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1661375
    iput-object v3, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    .line 1661376
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 1661377
    iput-object v3, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1661378
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/facebook/audience/model/AudienceControlData;

    .line 1661379
    :goto_8
    array-length v0, v1

    if-ge v2, v0, :cond_8

    .line 1661380
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1661381
    aput-object v0, v1, v2

    .line 1661382
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1661383
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1661384
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1661385
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1661386
    goto :goto_3

    .line 1661387
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    goto :goto_4

    .line 1661388
    :cond_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    goto :goto_5

    .line 1661389
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    goto :goto_6

    .line 1661390
    :cond_7
    sget-object v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_7

    .line 1661391
    :cond_8
    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    .line 1661392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 1661393
    iput-object v3, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    .line 1661394
    :goto_9
    return-void

    .line 1661395
    :cond_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    goto :goto_9
.end method

.method public constructor <init>(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;)V
    .locals 1

    .prologue
    .line 1661396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1661397
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    .line 1661398
    iget-boolean v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    .line 1661399
    iget-boolean v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    .line 1661400
    iget-boolean v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    .line 1661401
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    .line 1661402
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    .line 1661403
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    .line 1661404
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1661405
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->i:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    .line 1661406
    iget-object v0, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    .line 1661407
    return-void
.end method

.method public static newBuilder()Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;
    .locals 2

    .prologue
    .line 1661408
    new-instance v0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    invoke-direct {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1661410
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1661411
    if-ne p0, p1, :cond_1

    .line 1661412
    :cond_0
    :goto_0
    return v0

    .line 1661413
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    if-nez v2, :cond_2

    move v0, v1

    .line 1661414
    goto :goto_0

    .line 1661415
    :cond_2
    check-cast p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    .line 1661416
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1661417
    goto :goto_0

    .line 1661418
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    iget-boolean v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1661419
    goto :goto_0

    .line 1661420
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1661421
    goto :goto_0

    .line 1661422
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    iget-boolean v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1661423
    goto :goto_0

    .line 1661424
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1661425
    goto :goto_0

    .line 1661426
    :cond_7
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1661427
    goto :goto_0

    .line 1661428
    :cond_8
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1661429
    goto :goto_0

    .line 1661430
    :cond_9
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1661431
    goto :goto_0

    .line 1661432
    :cond_a
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1661433
    goto :goto_0

    .line 1661434
    :cond_b
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1661435
    goto :goto_0
.end method

.method public getInspirationGroupSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_group_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661360
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaContentId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_content_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661361
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661316
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchQuery()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "search_query"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661317
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectablePrivacyData()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selectable_privacy_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661318
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    return-object v0
.end method

.method public getSelectedAudience()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_audience"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1661319
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661359
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1661320
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isMyDaySelected()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_my_day_selected"
    .end annotation

    .prologue
    .line 1661321
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    return v0
.end method

.method public isNewsFeedSelected()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_news_feed_selected"
    .end annotation

    .prologue
    .line 1661322
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    return v0
.end method

.method public isVideo()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_video"
    .end annotation

    .prologue
    .line 1661323
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1661324
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1661325
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661326
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661327
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661328
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->d:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661329
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1661330
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661331
    :goto_4
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1661332
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661333
    :goto_5
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1661334
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661335
    :goto_6
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_7

    .line 1661336
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661337
    :goto_7
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661338
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_8
    if-ge v3, v4, :cond_8

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->i:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1661339
    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1661340
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    .line 1661341
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661342
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1661343
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1661344
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1661345
    goto :goto_3

    .line 1661346
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661347
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4

    .line 1661348
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661349
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_5

    .line 1661350
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661351
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_6

    .line 1661352
    :cond_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661353
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/privacy/model/SelectablePrivacyData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_7

    .line 1661354
    :cond_8
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1661355
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661356
    :goto_9
    return-void

    .line 1661357
    :cond_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1661358
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_9
.end method
