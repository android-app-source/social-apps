.class public final Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6ea4aec3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1661631
    const-class v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1661637
    const-class v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1661638
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1661639
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1661640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1661641
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1661642
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1661643
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x7cc042ae

    invoke-static {v3, v2, v4}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1661644
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1661645
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1661646
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1661647
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1661648
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1661649
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1661652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1661653
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1661654
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7cc042ae

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1661655
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1661656
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;

    .line 1661657
    iput v3, v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->g:I

    .line 1661658
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1661659
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1661660
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1661661
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1661650
    new-instance v0, LX/AJc;

    invoke-direct {v0, p1}, LX/AJc;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661651
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1661632
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1661633
    const/4 v0, 0x2

    const v1, -0x7cc042ae

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->g:I

    .line 1661634
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1661635
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1661636
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1661619
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1661620
    new-instance v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;-><init>()V

    .line 1661621
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1661622
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1661623
    const v0, 0x3d459683

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1661624
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661625
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->e:Ljava/lang/String;

    .line 1661626
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661627
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->f:Ljava/lang/String;

    .line 1661628
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661629
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1661630
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
