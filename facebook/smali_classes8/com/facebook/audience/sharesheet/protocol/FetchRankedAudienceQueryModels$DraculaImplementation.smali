.class public final Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1661556
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1661557
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1661554
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1661555
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1661545
    if-nez p1, :cond_0

    .line 1661546
    :goto_0
    return v0

    .line 1661547
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1661548
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1661549
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1661550
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1661551
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1661552
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1661553
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x7cc042ae
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1661544
    new-instance v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1661541
    packed-switch p0, :pswitch_data_0

    .line 1661542
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1661543
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x7cc042ae
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1661540
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1661538
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->b(I)V

    .line 1661539
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1661507
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1661508
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1661509
    :cond_0
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1661510
    iput p2, p0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->b:I

    .line 1661511
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1661537
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1661536
    new-instance v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1661533
    iget v0, p0, LX/1vt;->c:I

    .line 1661534
    move v0, v0

    .line 1661535
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1661530
    iget v0, p0, LX/1vt;->c:I

    .line 1661531
    move v0, v0

    .line 1661532
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1661527
    iget v0, p0, LX/1vt;->b:I

    .line 1661528
    move v0, v0

    .line 1661529
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1661524
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1661525
    move-object v0, v0

    .line 1661526
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1661515
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1661516
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1661517
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1661518
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1661519
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1661520
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1661521
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1661522
    invoke-static {v3, v9, v2}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1661523
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1661512
    iget v0, p0, LX/1vt;->c:I

    .line 1661513
    move v0, v0

    .line 1661514
    return v0
.end method
