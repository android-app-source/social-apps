.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1657751
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;

    new-instance v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1657752
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1657754
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1657755
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1657756
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1657757
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1657758
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1657759
    if-eqz v2, :cond_0

    .line 1657760
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1657761
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1657762
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1657763
    if-eqz v2, :cond_1

    .line 1657764
    const-string p0, "client_subscription_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1657765
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1657766
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1657767
    if-eqz v2, :cond_2

    .line 1657768
    const-string p0, "thread"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1657769
    invoke-static {v1, v2, p1}, LX/AIF;->a(LX/15i;ILX/0nX;)V

    .line 1657770
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1657771
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1657753
    check-cast p1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Serializer;->a(Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
