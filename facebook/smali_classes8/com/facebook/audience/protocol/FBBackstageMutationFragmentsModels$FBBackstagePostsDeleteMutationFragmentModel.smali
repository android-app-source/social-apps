.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3ca8323c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657648
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657647
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1657645
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1657646
    return-void
.end method

.method private a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1657629
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;->e:Ljava/util/List;

    .line 1657630
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1657639
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657640
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1657641
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1657642
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1657643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657644
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1657636
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657638
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1657633
    new-instance v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;-><init>()V

    .line 1657634
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1657635
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1657632
    const v0, -0x7758677b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1657631
    const v0, -0x69c9c57a

    return v0
.end method
