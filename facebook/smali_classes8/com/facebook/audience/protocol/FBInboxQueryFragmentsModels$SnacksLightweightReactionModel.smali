.class public final Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d9b29b3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:D

.field private i:D

.field private j:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1659430
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1659429
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1659427
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1659428
    return-void
.end method

.method private j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659425
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    .line 1659426
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659423
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->f:Ljava/lang/String;

    .line 1659424
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659384
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1659385
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1659410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1659411
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1659412
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1659413
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1659414
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1659415
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1659416
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1659417
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1659418
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->h:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1659419
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->i:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1659420
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->j:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1659421
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1659422
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1659397
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1659398
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1659399
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    .line 1659400
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1659401
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    .line 1659402
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    .line 1659403
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1659404
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1659405
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1659406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    .line 1659407
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1659408
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1659409
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659396
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1659391
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1659392
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->h:D

    .line 1659393
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->i:D

    .line 1659394
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;->j:D

    .line 1659395
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1659388
    new-instance v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;-><init>()V

    .line 1659389
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1659390
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1659387
    const v0, 0x16f60772

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1659386
    const v0, -0x5a44bbf9

    return v0
.end method
