.class public final Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1d1aa1a7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1659371
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1659372
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1659375
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1659376
    return-void
.end method

.method private a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659373
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    .line 1659374
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659360
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 1659361
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1659362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1659363
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1659364
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->j()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1659365
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1659366
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1659367
    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1659368
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1659369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1659370
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1659352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1659353
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1659354
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    .line 1659355
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1659356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    .line 1659357
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel$FaceImageModel;

    .line 1659358
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1659359
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1659349
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1659350
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;->f:I

    .line 1659351
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1659346
    new-instance v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel$FeedbackReactionModel;-><init>()V

    .line 1659347
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1659348
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1659345
    const v0, -0x4d3cefe4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1659344
    const v0, -0x629d3544

    return v0
.end method
