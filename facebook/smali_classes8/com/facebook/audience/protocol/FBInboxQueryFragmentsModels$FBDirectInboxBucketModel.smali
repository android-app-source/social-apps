.class public final Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x35b6cdf7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1658426
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1658429
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1658427
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1658428
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1658424
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->e:Ljava/lang/String;

    .line 1658425
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1658413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1658414
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1658415
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1658416
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1658417
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1658418
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1658419
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1658420
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1658421
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->h:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 1658422
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1658423
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1658400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1658401
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1658402
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1658403
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1658404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    .line 1658405
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1658406
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1658407
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    .line 1658408
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1658409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    .line 1658410
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    .line 1658411
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1658412
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1658430
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1658397
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1658398
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->h:I

    .line 1658399
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1658394
    new-instance v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;-><init>()V

    .line 1658395
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1658396
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1658393
    const v0, -0x79b2338f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1658388
    const v0, 0x33e85f72

    return v0
.end method

.method public final j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1658391
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1658392
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1658389
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    .line 1658390
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    return-object v0
.end method
