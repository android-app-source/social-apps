.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6bcdd72f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657964
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657963
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1657961
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1657962
    return-void
.end method

.method private a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657959
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    .line 1657960
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1657953
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657954
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1657955
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1657956
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1657957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657958
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1657940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657941
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1657942
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    .line 1657943
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1657944
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;

    .line 1657945
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    .line 1657946
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657947
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1657950
    new-instance v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel;-><init>()V

    .line 1657951
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1657952
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1657949
    const v0, 0x6a82aebb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1657948
    const v0, 0x9448c1b

    return v0
.end method
