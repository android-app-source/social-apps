.class public final Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6d3b7dfb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1658911
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1658914
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1658912
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1658913
    return-void
.end method

.method private a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1658903
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    .line 1658904
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1658905
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1658906
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1658907
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1658908
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1658909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1658910
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1658895
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1658896
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1658897
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    .line 1658898
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1658899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;

    .line 1658900
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$SnacksLightweightReactionModel;

    .line 1658901
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1658902
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1658892
    new-instance v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel$EdgesModel;-><init>()V

    .line 1658893
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1658894
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1658891
    const v0, -0x5244c074

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1658890
    const v0, -0x53f312fc

    return v0
.end method
