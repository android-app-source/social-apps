.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x54067336
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657936
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657935
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1657933
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1657934
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657931
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->e:Ljava/lang/String;

    .line 1657932
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657929
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->f:Ljava/lang/String;

    .line 1657930
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1657921
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657922
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1657923
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1657924
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1657925
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1657926
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1657927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657928
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1657937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657938
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657939
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1657920
    new-instance v0, LX/AIC;

    invoke-direct {v0, p1}, LX/AIC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657919
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1657917
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1657918
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1657916
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1657913
    new-instance v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageUpdateAudienceModeMutationModel$UserModel;-><init>()V

    .line 1657914
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1657915
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1657912
    const v0, 0x2b1527d2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1657911
    const v0, 0x285feb

    return v0
.end method
