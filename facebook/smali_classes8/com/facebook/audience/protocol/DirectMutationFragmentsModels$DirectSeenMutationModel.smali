.class public final Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6ffd6534
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657010
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657009
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1657007
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1657008
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657005
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->e:Ljava/lang/String;

    .line 1657006
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657003
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->f:Ljava/lang/String;

    .line 1657004
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1657011
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657012
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1657013
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1657014
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1657015
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1657016
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1657017
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1657018
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1657019
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657020
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1656995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656996
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1656997
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    .line 1656998
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1656999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;

    .line 1657000
    iput-object v0, v1, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->g:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    .line 1657001
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657002
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656993
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->g:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->g:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    .line 1656994
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;->g:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1656990
    new-instance v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel;-><init>()V

    .line 1656991
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1656992
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1656989
    const v0, 0x9622e5c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1656988
    const v0, -0x6a2dd038

    return v0
.end method
