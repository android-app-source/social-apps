.class public final Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656984
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656983
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1656981
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1656982
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1656975
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656976
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1656977
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1656978
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1656979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656980
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1656985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656987
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656974
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1656971
    new-instance v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;-><init>()V

    .line 1656972
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1656973
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1656970
    const v0, 0x685492ac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1656969
    const v0, 0x177e5768

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656967
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;->e:Ljava/lang/String;

    .line 1656968
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectSeenMutationModel$ThreadModel;->e:Ljava/lang/String;

    return-object v0
.end method
