.class public final Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x25fc7096
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656860
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656859
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1656857
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1656858
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1656851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656852
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1656853
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1656854
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1656855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656856
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1656836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656837
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1656838
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    .line 1656839
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1656840
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;

    .line 1656841
    iput-object v0, v1, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    .line 1656842
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656843
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656849
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    .line 1656850
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1656846
    new-instance v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;-><init>()V

    .line 1656847
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1656848
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1656845
    const v0, -0x4f6404b8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1656844
    const v0, 0x177e5768

    return v0
.end method
