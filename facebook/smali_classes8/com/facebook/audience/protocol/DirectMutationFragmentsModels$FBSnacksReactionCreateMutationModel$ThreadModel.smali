.class public final Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf3d23d7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657242
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657241
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1657239
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1657240
    return-void
.end method

.method private j()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657237
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    .line 1657238
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657243
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->f:Ljava/lang/String;

    .line 1657244
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1657229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657230
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->j()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1657231
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1657232
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1657233
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1657234
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1657235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657236
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1657221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657222
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->j()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1657223
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->j()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    .line 1657224
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->j()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1657225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;

    .line 1657226
    iput-object v0, v1, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->e:Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel;

    .line 1657227
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657228
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657220
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1657217
    new-instance v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;-><init>()V

    .line 1657218
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1657219
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1657216
    const v0, 0x3fbc2455

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1657215
    const v0, 0x177e5768

    return v0
.end method
