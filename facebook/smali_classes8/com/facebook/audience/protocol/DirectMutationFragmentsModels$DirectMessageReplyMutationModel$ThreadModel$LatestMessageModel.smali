.class public final Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656808
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656820
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1656827
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1656828
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1656821
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656822
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1656823
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1656824
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1656825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656826
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1656816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656818
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656819
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1656813
    new-instance v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;-><init>()V

    .line 1656814
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1656815
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1656812
    const v0, -0x34ab63cd    # -1.3933619E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1656811
    const v0, 0x361ea3be

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656809
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;->e:Ljava/lang/String;

    .line 1656810
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;->e:Ljava/lang/String;

    return-object v0
.end method
