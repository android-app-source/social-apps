.class public final Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52d2907
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1659270
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1659269
    const-class v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1659267
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1659268
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1659255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1659256
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1659257
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1659258
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1659259
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1659260
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1659261
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1659262
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1659263
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1659264
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1659265
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1659266
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1659242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1659243
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1659244
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1659245
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1659246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1659247
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1659248
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1659249
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1659250
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1659251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    .line 1659252
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1659253
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1659254
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1659241
    new-instance v0, LX/AIL;

    invoke-direct {v0, p1}, LX/AIL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659240
    invoke-virtual {p0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1659238
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1659239
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1659237
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1659234
    new-instance v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;-><init>()V

    .line 1659235
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1659236
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1659233
    const v0, 0x41e425e2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1659224
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659231
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->e:Ljava/lang/String;

    .line 1659232
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659229
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1659230
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->f:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659227
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->g:Ljava/lang/String;

    .line 1659228
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1659225
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1659226
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    return-object v0
.end method
