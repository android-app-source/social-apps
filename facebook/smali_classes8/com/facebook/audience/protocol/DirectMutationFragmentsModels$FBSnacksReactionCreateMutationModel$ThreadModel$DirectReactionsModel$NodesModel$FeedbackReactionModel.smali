.class public final Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x856af2a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657138
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1657137
    const-class v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1657135
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1657136
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1657133
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    iput-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 1657134
    iget-object v0, p0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1657127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657128
    invoke-direct {p0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;->a()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1657129
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1657130
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1657131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657132
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1657119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1657120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1657121
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1657124
    new-instance v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel$DirectReactionsModel$NodesModel$FeedbackReactionModel;-><init>()V

    .line 1657125
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1657126
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1657123
    const v0, -0x3357d66a    # -8.8165552E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1657122
    const v0, -0x629d3544

    return v0
.end method
