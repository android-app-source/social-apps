.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1657719
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;

    new-instance v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1657720
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1657721
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1657722
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1657723
    const/4 v2, 0x0

    .line 1657724
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1657725
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1657726
    :goto_0
    move v1, v2

    .line 1657727
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1657728
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1657729
    new-instance v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;

    invoke-direct {v1}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;-><init>()V

    .line 1657730
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1657731
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1657732
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1657733
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1657734
    :cond_0
    return-object v1

    .line 1657735
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1657736
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_5

    .line 1657737
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1657738
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1657739
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 1657740
    const-string p0, "client_mutation_id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1657741
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1657742
    :cond_3
    const-string p0, "client_subscription_id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1657743
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1657744
    :cond_4
    const-string p0, "thread"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1657745
    invoke-static {p1, v0}, LX/AIF;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1657746
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1657747
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1657748
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1657749
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1657750
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1
.end method
