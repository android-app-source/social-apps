.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d079235
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1658135
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1658134
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1658132
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1658133
    return-void
.end method

.method private a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1658111
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    iput-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    .line 1658112
    iget-object v0, p0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1658126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1658127
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1658128
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1658129
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1658130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1658131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1658118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1658119
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1658120
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    .line 1658121
    invoke-direct {p0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1658122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;

    .line 1658123
    iput-object v0, v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;->e:Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel$UserModel;

    .line 1658124
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1658125
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1658115
    new-instance v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;

    invoke-direct {v0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$UpdateWhiteListMutationModel;-><init>()V

    .line 1658116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1658117
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1658114
    const v0, -0x5fa86af1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1658113
    const v0, -0x2f554e6d

    return v0
.end method
