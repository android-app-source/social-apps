.class public final Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1657616
    const-class v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;

    new-instance v1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1657617
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1657628
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1657619
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1657620
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1657621
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1657622
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1657623
    if-eqz p0, :cond_0

    .line 1657624
    const-string p0, "deleted_post_ids"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1657625
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1657626
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1657627
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1657618
    check-cast p1, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel$Serializer;->a(Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstagePostsDeleteMutationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
