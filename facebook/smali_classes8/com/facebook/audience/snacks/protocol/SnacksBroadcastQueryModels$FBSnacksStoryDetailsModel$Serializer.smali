.class public final Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1663401
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    new-instance v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1663402
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1663403
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1663404
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1663405
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 1663406
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1663407
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1663408
    if-eqz v2, :cond_0

    .line 1663409
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1663410
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1663411
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1663412
    if-eqz v2, :cond_1

    .line 1663413
    const-string v3, "is_seen_by_viewer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1663414
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1663415
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1663416
    if-eqz v2, :cond_2

    .line 1663417
    const-string v3, "seen_direct_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1663418
    invoke-static {v1, v2, p1, p2}, LX/AKW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1663419
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1663420
    cmp-long v4, v2, v4

    if-eqz v4, :cond_3

    .line 1663421
    const-string v4, "time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1663422
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1663423
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1663424
    if-eqz v2, :cond_4

    .line 1663425
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1663426
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1663427
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1663428
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1663429
    check-cast p1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Serializer;->a(Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;LX/0nX;LX/0my;)V

    return-void
.end method
