.class public final Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2f2fec45
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1663732
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1663731
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1663729
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1663730
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1663723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1663724
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1663725
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1663726
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1663727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1663728
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1663708
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->e:Ljava/util/List;

    .line 1663709
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1663715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1663716
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1663717
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1663718
    if-eqz v1, :cond_0

    .line 1663719
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    .line 1663720
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->e:Ljava/util/List;

    .line 1663721
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1663722
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1663712
    new-instance v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    invoke-direct {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;-><init>()V

    .line 1663713
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1663714
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1663711
    const v0, 0x678d898b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1663710
    const v0, -0x4fdb8e34

    return v0
.end method
