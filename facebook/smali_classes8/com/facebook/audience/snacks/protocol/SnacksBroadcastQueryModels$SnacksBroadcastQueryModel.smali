.class public final Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x18bdd7bf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1663802
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1663753
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1663754
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1663755
    return-void
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663756
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1663757
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1663758
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1663759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1663760
    invoke-direct {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->p()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1663761
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1663762
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1663763
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1663764
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1663765
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1663766
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->o()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1663767
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1663768
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1663769
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1663770
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1663771
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1663772
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1663773
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1663774
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1663775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1663776
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1663777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1663778
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1663779
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    .line 1663780
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1663781
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    .line 1663782
    iput-object v0, v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->f:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    .line 1663783
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1663784
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1663785
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1663786
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    .line 1663787
    iput-object v0, v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1663788
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1663789
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1663790
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1663791
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    .line 1663792
    iput-object v0, v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1663793
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->o()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1663794
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->o()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    .line 1663795
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->o()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1663796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    .line 1663797
    iput-object v0, v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    .line 1663798
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1663799
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1663800
    new-instance v0, LX/AKT;

    invoke-direct {v0, p1}, LX/AKT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663801
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1663750
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1663751
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1663752
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1663747
    new-instance v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    invoke-direct {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;-><init>()V

    .line 1663748
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1663749
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1663746
    const v0, 0x26d28b8a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1663745
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663743
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->f:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->f:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    .line 1663744
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->f:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663733
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->g:Ljava/lang/String;

    .line 1663734
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663741
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1663742
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663739
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->i:Ljava/lang/String;

    .line 1663740
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663737
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1663738
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663735
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    .line 1663736
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    return-object v0
.end method
