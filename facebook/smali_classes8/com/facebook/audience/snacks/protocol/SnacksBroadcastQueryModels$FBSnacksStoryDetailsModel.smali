.class public final Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x20f0f1da
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1663469
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1663468
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1663466
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1663467
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663464
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->e:Ljava/lang/String;

    .line 1663465
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663462
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->i:Ljava/lang/String;

    .line 1663463
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1663450
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1663451
    invoke-direct {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1663452
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1663453
    invoke-direct {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1663454
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1663455
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1663456
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1663457
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1663458
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->h:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1663459
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1663460
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1663461
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1663430
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1663431
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1663432
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    .line 1663433
    invoke-virtual {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1663434
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    .line 1663435
    iput-object v0, v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->g:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    .line 1663436
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1663437
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663449
    invoke-direct {p0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1663445
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1663446
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->f:Z

    .line 1663447
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->h:J

    .line 1663448
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1663442
    new-instance v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    invoke-direct {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;-><init>()V

    .line 1663443
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1663444
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1663441
    const v0, 0x3e9aedd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1663440
    const v0, 0x177e5768

    return v0
.end method

.method public final j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663438
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->g:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    iput-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->g:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    .line 1663439
    iget-object v0, p0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->g:Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    return-object v0
.end method
