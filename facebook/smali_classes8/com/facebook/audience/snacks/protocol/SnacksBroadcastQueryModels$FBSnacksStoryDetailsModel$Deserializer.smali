.class public final Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1663309
    const-class v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    new-instance v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1663310
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1663308
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1663301
    invoke-static {p1}, LX/AKX;->a(LX/15w;)LX/15i;

    move-result-object v2

    .line 1663302
    new-instance v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    invoke-direct {v1}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;-><init>()V

    .line 1663303
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1663304
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1663305
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1663306
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1663307
    :cond_0
    return-object v1
.end method
