.class public Lcom/facebook/audience/snacks/model/SnacksMyUserThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksMyUserThread;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/AKL;


# instance fields
.field private final b:Ljava/lang/String;

.field public final c:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksMyThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1663093
    new-instance v0, LX/AKJ;

    invoke-direct {v0}, LX/AKJ;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1663094
    new-instance v0, LX/AKL;

    invoke-direct {v0}, LX/AKL;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->a:LX/AKL;

    return-void
.end method

.method public constructor <init>(LX/AKK;)V
    .locals 2

    .prologue
    .line 1663095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663096
    iget-object v0, p1, LX/AKK;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->b:Ljava/lang/String;

    .line 1663097
    iget-object v0, p1, LX/AKK;->b:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1663098
    iget-object v0, p1, LX/AKK;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    .line 1663099
    iget-object v0, p1, LX/AKK;->d:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    .line 1663100
    const/4 v1, 0x1

    const/4 p1, 0x0

    .line 1663101
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 1663102
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1663103
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1663104
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 1663105
    return-void

    :cond_0
    move v0, p1

    .line 1663106
    goto :goto_0

    :cond_1
    move v1, p1

    .line 1663107
    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1663079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663080
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->b:Ljava/lang/String;

    .line 1663081
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1663082
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1663083
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    .line 1663084
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 1663085
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, v2

    if-ge v1, v0, :cond_1

    .line 1663086
    sget-object v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 1663087
    aput-object v0, v2, v1

    .line 1663088
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1663089
    :cond_0
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    goto :goto_0

    .line 1663090
    :cond_1
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    .line 1663091
    return-void
.end method

.method public static newBuilder()LX/AKK;
    .locals 2

    .prologue
    .line 1663092
    new-instance v0, LX/AKK;

    invoke-direct {v0}, LX/AKK;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1663078
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1663065
    if-ne p0, p1, :cond_1

    .line 1663066
    :cond_0
    :goto_0
    return v0

    .line 1663067
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    if-nez v2, :cond_2

    move v0, v1

    .line 1663068
    goto :goto_0

    .line 1663069
    :cond_2
    check-cast p1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1663070
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1663071
    goto :goto_0

    .line 1663072
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1663073
    goto :goto_0

    .line 1663074
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1663075
    goto :goto_0

    .line 1663076
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1663077
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1663064
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1663053
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1663054
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    if-nez v1, :cond_0

    .line 1663055
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1663056
    :goto_0
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1663057
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1663058
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 1663059
    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/snacks/model/SnacksMyThread;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1663060
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1663061
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1663062
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 1663063
    :cond_1
    return-void
.end method
