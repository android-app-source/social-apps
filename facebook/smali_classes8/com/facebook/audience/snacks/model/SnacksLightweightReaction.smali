.class public Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/AKG;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field private final f:F

.field private final g:F

.field private final h:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1662962
    new-instance v0, LX/AKD;

    invoke-direct {v0}, LX/AKD;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1662963
    new-instance v0, LX/AKG;

    invoke-direct {v0}, LX/AKG;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->a:LX/AKG;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1662951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662952
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->b:Ljava/lang/String;

    .line 1662953
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->c:I

    .line 1662954
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1662955
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662956
    :goto_0
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->values()[Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 1662957
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->f:F

    .line 1662958
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->g:F

    .line 1662959
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->h:F

    .line 1662960
    return-void

    .line 1662961
    :cond_0
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    goto :goto_0
.end method

.method public static newBuilder()LX/AKE;
    .locals 2

    .prologue
    .line 1662950
    new-instance v0, LX/AKE;

    invoke-direct {v0}, LX/AKE;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1662964
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1662931
    if-ne p0, p1, :cond_1

    .line 1662932
    :cond_0
    :goto_0
    return v0

    .line 1662933
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;

    if-nez v2, :cond_2

    move v0, v1

    .line 1662934
    goto :goto_0

    .line 1662935
    :cond_2
    check-cast p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;

    .line 1662936
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1662937
    goto :goto_0

    .line 1662938
    :cond_3
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->c:I

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->c:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1662939
    goto :goto_0

    .line 1662940
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1662941
    goto :goto_0

    .line 1662942
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1662943
    goto :goto_0

    .line 1662944
    :cond_6
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->f:F

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->f:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_7

    move v0, v1

    .line 1662945
    goto :goto_0

    .line 1662946
    :cond_7
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->g:F

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->g:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_8

    move v0, v1

    .line 1662947
    goto :goto_0

    .line 1662948
    :cond_8
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->h:F

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->h:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    .line 1662949
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1662930
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->g:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->h:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1662919
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662920
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662921
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    if-nez v0, :cond_0

    .line 1662922
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662923
    :goto_0
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->e:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662924
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1662925
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->g:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1662926
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1662927
    return-void

    .line 1662928
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662929
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->d:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
