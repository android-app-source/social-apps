.class public Lcom/facebook/audience/snacks/model/SnacksMyThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksMyThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Z

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:J

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1663042
    new-instance v0, LX/AKH;

    invoke-direct {v0}, LX/AKH;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/AKI;)V
    .locals 2

    .prologue
    .line 1663033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663034
    iget-object v0, p1, LX/AKI;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    .line 1663035
    iget-object v0, p1, LX/AKI;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    .line 1663036
    iget-boolean v0, p1, LX/AKI;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->c:Z

    .line 1663037
    iget-object v0, p1, LX/AKI;->d:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    .line 1663038
    iget v0, p1, LX/AKI;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    .line 1663039
    iget-wide v0, p1, LX/AKI;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    .line 1663040
    iget-object v0, p1, LX/AKI;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    .line 1663041
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1663014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663015
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    .line 1663016
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-nez v2, :cond_0

    .line 1663017
    iput-object v3, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    .line 1663018
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->c:Z

    .line 1663019
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;

    .line 1663020
    :goto_2
    array-length v0, v2

    if-ge v1, v0, :cond_2

    .line 1663021
    sget-object v0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;

    .line 1663022
    aput-object v0, v2, v1

    .line 1663023
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1663024
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1663025
    goto :goto_1

    .line 1663026
    :cond_2
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    .line 1663027
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    .line 1663028
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    .line 1663029
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1663030
    iput-object v3, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    .line 1663031
    :goto_3
    return-void

    .line 1663032
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1662975
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1662995
    if-ne p0, p1, :cond_1

    .line 1662996
    :cond_0
    :goto_0
    return v0

    .line 1662997
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    if-nez v2, :cond_2

    move v0, v1

    .line 1662998
    goto :goto_0

    .line 1662999
    :cond_2
    check-cast p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 1663000
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1663001
    goto :goto_0

    .line 1663002
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1663003
    goto :goto_0

    .line 1663004
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1663005
    goto :goto_0

    .line 1663006
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1663007
    goto :goto_0

    .line 1663008
    :cond_6
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1663009
    goto :goto_0

    .line 1663010
    :cond_7
    iget-wide v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    iget-wide v4, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 1663011
    goto :goto_0

    .line 1663012
    :cond_8
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1663013
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1662994
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1662976
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662977
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1662978
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662979
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662980
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662981
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->d:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;

    .line 1662982
    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/snacks/model/SnacksLightweightReaction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1662983
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1662984
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662985
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1662986
    goto :goto_1

    .line 1662987
    :cond_2
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662988
    iget-wide v4, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1662989
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1662990
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662991
    :goto_3
    return-void

    .line 1662992
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662993
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3
.end method
