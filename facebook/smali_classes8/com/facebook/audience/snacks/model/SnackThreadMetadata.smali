.class public Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/AK7;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:J

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1662781
    new-instance v0, LX/AK5;

    invoke-direct {v0}, LX/AK5;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1662782
    new-instance v0, LX/AK7;

    invoke-direct {v0}, LX/AK7;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->a:LX/AK7;

    return-void
.end method

.method public constructor <init>(LX/AK6;)V
    .locals 12

    .prologue
    .line 1662754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662755
    iget-object v0, p1, LX/AK6;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    .line 1662756
    iget-boolean v0, p1, LX/AK6;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    .line 1662757
    iget-object v0, p1, LX/AK6;->c:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662758
    iget-object v0, p1, LX/AK6;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    .line 1662759
    iget-object v0, p1, LX/AK6;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    .line 1662760
    iget-object v0, p1, LX/AK6;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    .line 1662761
    iget v0, p1, LX/AK6;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    .line 1662762
    iget-wide v0, p1, LX/AK6;->h:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    .line 1662763
    iget-object v0, p1, LX/AK6;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    .line 1662764
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1662765
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1662766
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 1662767
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v2

    .line 1662768
    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 1662769
    iget-wide v10, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    move-wide v6, v10

    .line 1662770
    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_2

    move v2, v3

    :goto_2
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 1662771
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1662772
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 1662773
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1662774
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_4
    invoke-static {v3}, LX/0Tp;->b(Z)V

    .line 1662775
    return-void

    :cond_0
    move v2, v4

    .line 1662776
    goto :goto_0

    :cond_1
    move v2, v4

    .line 1662777
    goto :goto_1

    :cond_2
    move v2, v4

    .line 1662778
    goto :goto_2

    :cond_3
    move v2, v4

    .line 1662779
    goto :goto_3

    :cond_4
    move v3, v4

    .line 1662780
    goto :goto_4
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1662736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662737
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    .line 1662738
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    .line 1662739
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1662740
    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662741
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1662742
    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    .line 1662743
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    .line 1662744
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    .line 1662745
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    .line 1662746
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    .line 1662747
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1662748
    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    .line 1662749
    :goto_3
    return-void

    .line 1662750
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1662751
    :cond_1
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    goto :goto_1

    .line 1662752
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    goto :goto_2

    .line 1662753
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    goto :goto_3
.end method

.method public static a(Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)LX/AK6;
    .locals 2

    .prologue
    .line 1662735
    new-instance v0, LX/AK6;

    invoke-direct {v0, p0}, LX/AK6;-><init>(Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)V

    return-object v0
.end method

.method public static newBuilder()LX/AK6;
    .locals 2

    .prologue
    .line 1662734
    new-instance v0, LX/AK6;

    invoke-direct {v0}, LX/AK6;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1662689
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1662711
    if-ne p0, p1, :cond_1

    .line 1662712
    :cond_0
    :goto_0
    return v0

    .line 1662713
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    if-nez v2, :cond_2

    move v0, v1

    .line 1662714
    goto :goto_0

    .line 1662715
    :cond_2
    check-cast p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662716
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1662717
    goto :goto_0

    .line 1662718
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1662719
    goto :goto_0

    .line 1662720
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1662721
    goto :goto_0

    .line 1662722
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1662723
    goto :goto_0

    .line 1662724
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1662725
    goto :goto_0

    .line 1662726
    :cond_7
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1662727
    goto :goto_0

    .line 1662728
    :cond_8
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1662729
    goto :goto_0

    .line 1662730
    :cond_9
    iget-wide v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    iget-wide v4, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    move v0, v1

    .line 1662731
    goto :goto_0

    .line 1662732
    :cond_a
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1662733
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1662710
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1662690
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662691
    iget-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662692
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    if-nez v0, :cond_1

    .line 1662693
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662694
    :goto_1
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1662695
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662696
    :goto_2
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662697
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662698
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662699
    iget-wide v4, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1662700
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1662701
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662702
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 1662703
    goto :goto_0

    .line 1662704
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662705
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    .line 1662706
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662707
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 1662708
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662709
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3
.end method
