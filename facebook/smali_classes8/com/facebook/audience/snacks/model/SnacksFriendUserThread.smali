.class public Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/AKC;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendThread;",
            ">;"
        }
    .end annotation
.end field

.field public final f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1662906
    new-instance v0, LX/AKA;

    invoke-direct {v0}, LX/AKA;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1662907
    new-instance v0, LX/AKC;

    invoke-direct {v0}, LX/AKC;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->a:LX/AKC;

    return-void
.end method

.method public constructor <init>(LX/AKB;)V
    .locals 2

    .prologue
    .line 1662892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662893
    iget-object v0, p1, LX/AKB;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    .line 1662894
    iget-object v0, p1, LX/AKB;->b:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662895
    iget-object v0, p1, LX/AKB;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    .line 1662896
    iget-object v0, p1, LX/AKB;->d:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    .line 1662897
    iget v0, p1, LX/AKB;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    .line 1662898
    const/4 v1, 0x1

    const/4 p1, 0x0

    .line 1662899
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 1662900
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1662901
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1662902
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 1662903
    return-void

    :cond_0
    move v0, p1

    .line 1662904
    goto :goto_0

    :cond_1
    move v1, p1

    .line 1662905
    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1662878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    .line 1662880
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1662881
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662882
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    .line 1662883
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 1662884
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, v2

    if-ge v1, v0, :cond_1

    .line 1662885
    sget-object v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 1662886
    aput-object v0, v2, v1

    .line 1662887
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1662888
    :cond_0
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    goto :goto_0

    .line 1662889
    :cond_1
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    .line 1662890
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    .line 1662891
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1662877
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1662862
    if-ne p0, p1, :cond_1

    .line 1662863
    :cond_0
    :goto_0
    return v0

    .line 1662864
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    if-nez v2, :cond_2

    move v0, v1

    .line 1662865
    goto :goto_0

    .line 1662866
    :cond_2
    check-cast p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1662867
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1662868
    goto :goto_0

    .line 1662869
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1662870
    goto :goto_0

    .line 1662871
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1662872
    goto :goto_0

    .line 1662873
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1662874
    goto :goto_0

    .line 1662875
    :cond_6
    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    iget v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1662876
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1662849
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1662850
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662851
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    if-nez v1, :cond_0

    .line 1662852
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662853
    :goto_0
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662854
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662855
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 1662856
    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1662857
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1662858
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662859
    iget-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 1662860
    :cond_1
    iget v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662861
    return-void
.end method
