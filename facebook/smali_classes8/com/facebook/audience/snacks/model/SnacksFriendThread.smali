.class public Lcom/facebook/audience/snacks/model/SnacksFriendThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:J

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1662790
    new-instance v0, LX/AK8;

    invoke-direct {v0}, LX/AK8;-><init>()V

    sput-object v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/AK9;)V
    .locals 2

    .prologue
    .line 1662833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662834
    iget-object v0, p1, LX/AK9;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    .line 1662835
    iget-object v0, p1, LX/AK9;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    .line 1662836
    iget-boolean v0, p1, LX/AK9;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    .line 1662837
    iget-wide v0, p1, LX/AK9;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    .line 1662838
    iget-object v0, p1, LX/AK9;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    .line 1662839
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1662820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662821
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    .line 1662822
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_0

    .line 1662823
    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    .line 1662824
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    .line 1662825
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    .line 1662826
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1662827
    iput-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    .line 1662828
    :goto_2
    return-void

    .line 1662829
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    goto :goto_0

    .line 1662830
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1662831
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1662832
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1662805
    if-ne p0, p1, :cond_1

    .line 1662806
    :cond_0
    :goto_0
    return v0

    .line 1662807
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    if-nez v2, :cond_2

    move v0, v1

    .line 1662808
    goto :goto_0

    .line 1662809
    :cond_2
    check-cast p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 1662810
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1662811
    goto :goto_0

    .line 1662812
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1662813
    goto :goto_0

    .line 1662814
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1662815
    goto :goto_0

    .line 1662816
    :cond_5
    iget-wide v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    iget-wide v4, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 1662817
    goto :goto_0

    .line 1662818
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1662819
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1662804
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1662791
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1662792
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1662793
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662794
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662795
    iget-wide v4, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1662796
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1662797
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662798
    :goto_2
    return-void

    .line 1662799
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662800
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1662801
    goto :goto_1

    .line 1662802
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1662803
    iget-object v0, p0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2
.end method
