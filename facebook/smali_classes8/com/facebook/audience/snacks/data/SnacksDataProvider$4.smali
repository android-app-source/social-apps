.class public final Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

.field public final synthetic d:Z

.field public final synthetic e:LX/AJz;

.field public final synthetic f:LX/AK0;


# direct methods
.method public constructor <init>(LX/AK0;Ljava/util/List;LX/0Px;Lcom/facebook/audience/snacks/model/SnacksMyUserThread;ZLX/AJz;)V
    .locals 0

    .prologue
    .line 1662416
    iput-object p1, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->f:LX/AK0;

    iput-object p2, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->b:LX/0Px;

    iput-object p4, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->c:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    iput-boolean p5, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->d:Z

    iput-object p6, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->e:LX/AJz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1662417
    iget-object v0, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1662418
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BaN;

    .line 1662419
    if-eqz v0, :cond_0

    .line 1662420
    iget-object v2, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->b:LX/0Px;

    iget-object v3, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->c:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    iget-boolean v4, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->d:Z

    iget-object v5, p0, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;->e:LX/AJz;

    .line 1662421
    iget-object v6, v0, LX/BaN;->a:LX/BaO;

    iget-boolean v6, v6, LX/BaO;->l:Z

    if-eqz v6, :cond_2

    .line 1662422
    :goto_1
    goto :goto_0

    .line 1662423
    :cond_1
    return-void

    .line 1662424
    :cond_2
    iget-object v6, v0, LX/BaN;->a:LX/BaO;

    .line 1662425
    iput-object v3, v6, LX/BaO;->i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1662426
    iput-object v2, v6, LX/BaO;->h:LX/0Px;

    .line 1662427
    iget-object v7, v6, LX/BaO;->a:LX/BaP;

    iget-object v8, v6, LX/BaO;->i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    iget-object v9, v6, LX/BaO;->h:LX/0Px;

    .line 1662428
    iput-object v9, v7, LX/BaP;->d:LX/0Px;

    .line 1662429
    if-eqz v8, :cond_3

    .line 1662430
    iput-object v8, v7, LX/BaP;->e:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1662431
    :cond_3
    invoke-virtual {v7}, LX/1OM;->notifyDataSetChanged()V

    .line 1662432
    const v7, -0x79902e66

    invoke-static {v6, v7}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1662433
    iget-object v6, v0, LX/BaN;->a:LX/BaO;

    .line 1662434
    iput-object v5, v6, LX/BaO;->f:LX/AJz;

    .line 1662435
    iget-object v6, v0, LX/BaN;->a:LX/BaO;

    .line 1662436
    iput-boolean v4, v6, LX/BaO;->e:Z

    .line 1662437
    iget-object v6, v0, LX/BaN;->a:LX/BaO;

    .line 1662438
    iget-object v7, v6, LX/BaO;->f:LX/AJz;

    .line 1662439
    sget-object v8, LX/AJz;->USER_PULL_TO_REFRESH:LX/AJz;

    if-eq v7, v8, :cond_4

    sget-object v8, LX/AJz;->LOAD_UI:LX/AJz;

    if-ne v7, v8, :cond_8

    :cond_4
    const/4 v8, 0x1

    :goto_2
    move v7, v8

    .line 1662440
    if-nez v7, :cond_5

    .line 1662441
    :goto_3
    goto :goto_1

    .line 1662442
    :cond_5
    iget-object v9, v6, LX/BaO;->c:LX/7ga;

    iget-object v7, v6, LX/BaO;->i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1662443
    iget-object v8, v7, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v7, v8

    .line 1662444
    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_6

    const/4 v7, 0x1

    :goto_4
    iget-object v8, v6, LX/BaO;->h:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    iget-object v8, v6, LX/BaO;->h:LX/0Px;

    invoke-static {v8}, LX/BaO;->a(LX/0Px;)I

    move-result v2

    iget-boolean v8, v6, LX/BaO;->e:Z

    if-eqz v8, :cond_7

    sget-object v8, LX/7gd;->SERVER:LX/7gd;

    :goto_5
    invoke-virtual {v9, v7, v0, v2, v8}, LX/7ga;->a(ZIILX/7gd;)V

    goto :goto_3

    :cond_6
    const/4 v7, 0x0

    goto :goto_4

    :cond_7
    sget-object v8, LX/7gd;->CACHE:LX/7gd;

    goto :goto_5

    :cond_8
    const/4 v8, 0x0

    goto :goto_2
.end method
