.class public Lcom/facebook/audience/ui/AutoDismissEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public b:LX/AKh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1664069
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1664070
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1664071
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1664072
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1664073
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664074
    return-void
.end method


# virtual methods
.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1664075
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1664076
    iget-object v0, p0, Lcom/facebook/audience/ui/AutoDismissEditText;->b:LX/AKh;

    if-eqz v0, :cond_0

    .line 1664077
    iget-object v0, p0, Lcom/facebook/audience/ui/AutoDismissEditText;->b:LX/AKh;

    invoke-interface {v0}, LX/AKh;->a()V

    .line 1664078
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setBackKeyListener(LX/AKh;)V
    .locals 0

    .prologue
    .line 1664079
    iput-object p1, p0, Lcom/facebook/audience/ui/AutoDismissEditText;->b:LX/AKh;

    .line 1664080
    return-void
.end method
