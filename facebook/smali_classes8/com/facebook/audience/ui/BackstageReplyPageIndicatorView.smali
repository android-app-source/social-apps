.class public Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1664144
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664146
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664148
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664149
    return-void
.end method

.method private a(Z)Landroid/widget/LinearLayout$LayoutParams;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1664157
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1664158
    if-eqz p1, :cond_0

    .line 1664159
    invoke-virtual {p0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1ac2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1664160
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1664161
    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 1664150
    if-lez p1, :cond_0

    .line 1664151
    add-int/lit8 v0, p1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 1664152
    invoke-direct {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->d(I)LX/AKi;

    move-result-object v1

    .line 1664153
    invoke-virtual {v1}, LX/AKi;->getProgress()I

    move-result v2

    invoke-virtual {v1}, LX/AKi;->getMax()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 1664154
    invoke-virtual {v1}, LX/AKi;->getMax()I

    move-result v2

    invoke-virtual {v1, v2}, LX/AKi;->setProgress(I)V

    .line 1664155
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1664156
    :cond_0
    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 1664137
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a:I

    if-ge v0, v1, :cond_0

    .line 1664138
    add-int/lit8 v0, p1, 0x1

    :goto_0
    iget v1, p0, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a:I

    if-ge v0, v1, :cond_0

    .line 1664139
    invoke-direct {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->d(I)LX/AKi;

    move-result-object v1

    .line 1664140
    invoke-virtual {v1}, LX/AKi;->getProgress()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1664141
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/AKi;->setProgress(I)V

    .line 1664142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1664143
    :cond_0
    return-void
.end method

.method private d(I)LX/AKi;
    .locals 1

    .prologue
    .line 1664115
    invoke-virtual {p0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/AKi;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1664127
    iput p1, p0, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a:I

    .line 1664128
    invoke-virtual {p0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->removeAllViews()V

    move v2, v1

    .line 1664129
    :goto_0
    if-ge v2, p1, :cond_1

    .line 1664130
    new-instance v3, LX/AKi;

    invoke-virtual {p0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, LX/AKi;-><init>(Landroid/content/Context;)V

    .line 1664131
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a(Z)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    .line 1664132
    invoke-virtual {v3, v0}, LX/AKi;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1664133
    invoke-virtual {p0, v3}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->addView(Landroid/view/View;)V

    .line 1664134
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1664135
    goto :goto_1

    .line 1664136
    :cond_1
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1664123
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->d(I)LX/AKi;

    move-result-object v0

    .line 1664124
    if-eqz v0, :cond_0

    .line 1664125
    invoke-virtual {v0, p2}, LX/AKi;->setProgress(I)V

    .line 1664126
    :cond_0
    return-void
.end method

.method public setCurrentSegmentIndex(I)V
    .locals 0

    .prologue
    .line 1664120
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->b(I)V

    .line 1664121
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->c(I)V

    .line 1664122
    return-void
.end method

.method public setIndicatorSegmentProgressToMax(I)V
    .locals 3

    .prologue
    .line 1664116
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->d(I)LX/AKi;

    move-result-object v0

    .line 1664117
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/AKi;->getProgress()I

    move-result v1

    invoke-virtual {v0}, LX/AKi;->getMax()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1664118
    invoke-virtual {v0}, LX/AKi;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, LX/AKi;->setProgress(I)V

    .line 1664119
    :cond_0
    return-void
.end method
