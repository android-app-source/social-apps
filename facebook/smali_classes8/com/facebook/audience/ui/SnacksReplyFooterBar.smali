.class public Lcom/facebook/audience/ui/SnacksReplyFooterBar;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/audience/ui/AutoDismissEditText;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/user/model/User;

.field public g:LX/AL3;

.field private final h:Landroid/text/TextWatcher;

.field private final i:Landroid/widget/TextView$OnEditorActionListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1664550
    const-class v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1664551
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664552
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664559
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664560
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664553
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664554
    new-instance v0, LX/AKz;

    invoke-direct {v0, p0}, LX/AKz;-><init>(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->h:Landroid/text/TextWatcher;

    .line 1664555
    new-instance v0, LX/AL0;

    invoke-direct {v0, p0}, LX/AL0;-><init>(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->i:Landroid/widget/TextView$OnEditorActionListener;

    .line 1664556
    const-class v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-static {v0, p0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1664557
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a(Landroid/content/Context;)V

    .line 1664558
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1664533
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1664534
    const v1, 0x7f03136e

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1664535
    const v0, 0x7f0d2ce5

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1664536
    const v0, 0x7f0d2ce6

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/AutoDismissEditText;

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    .line 1664537
    const v0, 0x7f0d2ce7

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1664538
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->e:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/AL1;

    invoke-direct {v1, p0}, LX/AL1;-><init>(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1664539
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->f:Lcom/facebook/user/model/User;

    .line 1664540
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->f:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    .line 1664541
    iget-object v1, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1664542
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    iget-object v1, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->h:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/AutoDismissEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1664543
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->e:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 1664544
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    iget-object v1, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->i:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/AutoDismissEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1664545
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    new-instance v1, LX/AL2;

    invoke-direct {v1, p0}, LX/AL2;-><init>(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V

    .line 1664546
    iput-object v1, v0, Lcom/facebook/audience/ui/AutoDismissEditText;->b:LX/AKh;

    .line 1664547
    return-void

    .line 1664548
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->f:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    const/16 v1, 0x12cb

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a:LX/0Or;

    return-void
.end method

.method private static a(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1664549
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/audience/ui/SnacksReplyFooterBar;Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1664523
    invoke-static {p1}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V
    .locals 2

    .prologue
    .line 1664517
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/AutoDismissEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1664518
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1664519
    :goto_0
    return-void

    .line 1664520
    :cond_0
    iget-object v1, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->g:LX/AL3;

    if-eqz v1, :cond_1

    .line 1664521
    iget-object v1, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->g:LX/AL3;

    invoke-interface {v1, v0}, LX/AL3;->a(Ljava/lang/String;)V

    .line 1664522
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/AutoDismissEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1664514
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->setVisibility(I)V

    .line 1664515
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    new-instance v1, Lcom/facebook/audience/ui/SnacksReplyFooterBar$5;

    invoke-direct {v1, p0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar$5;-><init>(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/audience/ui/AutoDismissEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1664516
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1664524
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/AutoDismissEditText;->clearFocus()V

    .line 1664525
    invoke-virtual {p0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1664526
    invoke-virtual {p0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1664527
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->setVisibility(I)V

    .line 1664528
    return-void
.end method

.method public setListener(LX/AL3;)V
    .locals 0

    .prologue
    .line 1664529
    iput-object p1, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->g:LX/AL3;

    .line 1664530
    return-void
.end method

.method public setReplyEditTextHint(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1664531
    iget-object v0, p0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->d:Lcom/facebook/audience/ui/AutoDismissEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/audience/ui/AutoDismissEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1664532
    return-void
.end method
