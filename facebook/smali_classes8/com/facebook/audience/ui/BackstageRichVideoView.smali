.class public Lcom/facebook/audience/ui/BackstageRichVideoView;
.super Lcom/facebook/video/player/RichVideoPlayer;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Z

.field public p:Z

.field public q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1664255
    const-class v0, Lcom/facebook/audience/ui/BackstageRichVideoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/ui/BackstageRichVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664253
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/audience/ui/BackstageRichVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1664254
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664251
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/ui/BackstageRichVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664245
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->p:Z

    .line 1664247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->q:Z

    .line 1664248
    const-class v0, Lcom/facebook/audience/ui/BackstageRichVideoView;

    invoke-static {v0, p0}, Lcom/facebook/audience/ui/BackstageRichVideoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1664249
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/BackstageRichVideoView;->a(Landroid/content/Context;)V

    .line 1664250
    return-void
.end method

.method private A()Z
    .locals 3

    .prologue
    .line 1664244
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->m:LX/0Uh;

    sget v1, LX/19n;->s:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;)LX/0P2;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1664240
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 1664241
    if-eqz p0, :cond_0

    .line 1664242
    const-string v1, "CoverImageParamsKey"

    invoke-static {p0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1664243
    :cond_0
    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664230
    sget-object v0, LX/04D;->BACKSTAGE_VIDEOS:LX/04D;

    move-object v0, v0

    .line 1664231
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1664232
    sget-object v0, LX/04G;->OTHERS:LX/04G;

    move-object v0, v0

    .line 1664233
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1664234
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1664235
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1664236
    new-instance v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1664237
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1664238
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1664239
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/ui/BackstageRichVideoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/ui/BackstageRichVideoView;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->m:LX/0Uh;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1664162
    new-instance v0, LX/2oE;

    invoke-direct {v0}, LX/2oE;-><init>()V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1664163
    iput-object v1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1664164
    move-object v0, v0

    .line 1664165
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 1664166
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1664167
    move-object v0, v0

    .line 1664168
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1664169
    new-instance v1, LX/2oH;

    invoke-direct {v1}, LX/2oH;-><init>()V

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    .line 1664170
    iput-object p1, v0, LX/2oH;->b:Ljava/lang/String;

    .line 1664171
    move-object v0, v0

    .line 1664172
    const/4 v1, 0x1

    .line 1664173
    iput-boolean v1, v0, LX/2oH;->g:Z

    .line 1664174
    move-object v0, v0

    .line 1664175
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1664176
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 1664177
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1664178
    move-object v0, v1

    .line 1664179
    invoke-static {p2}, Lcom/facebook/audience/ui/BackstageRichVideoView;->a(Ljava/lang/String;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1664180
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1664181
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1664204
    iget-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->o:Z

    if-nez v0, :cond_0

    .line 1664205
    new-instance v0, LX/2oE;

    invoke-direct {v0}, LX/2oE;-><init>()V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1664206
    iput-object v1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1664207
    move-object v0, v0

    .line 1664208
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 1664209
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1664210
    move-object v0, v0

    .line 1664211
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1664212
    new-instance v1, LX/2oH;

    invoke-direct {v1}, LX/2oH;-><init>()V

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    .line 1664213
    iput-boolean v2, v0, LX/2oH;->g:Z

    .line 1664214
    move-object v0, v0

    .line 1664215
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1664216
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 1664217
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1664218
    move-object v0, v1

    .line 1664219
    invoke-static {p2}, Lcom/facebook/audience/ui/BackstageRichVideoView;->a(Ljava/lang/String;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1664220
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1664221
    iput-boolean v2, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->o:Z

    .line 1664222
    :goto_0
    return-void

    .line 1664223
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v0

    .line 1664224
    if-nez v0, :cond_1

    .line 1664225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->o:Z

    .line 1664226
    invoke-direct {p0, p1, p2}, Lcom/facebook/audience/ui/BackstageRichVideoView;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1664227
    :cond_1
    sget-object v1, LX/7K7;->VIDEO_SOURCE_HLS:LX/7K7;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    .line 1664228
    iget-object p0, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {p0, v1, p1, v2}, LX/2q7;->a(LX/7K7;Ljava/lang/String;LX/04g;)V

    .line 1664229
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1664198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->q:Z

    .line 1664199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->p:Z

    .line 1664200
    invoke-direct {p0}, Lcom/facebook/audience/ui/BackstageRichVideoView;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664201
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1664202
    :goto_0
    return-void

    .line 1664203
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/1cC;)V
    .locals 3
    .param p3    # LX/1cC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664191
    new-instance v0, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/audience/ui/BackstageRichVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/facebook/audience/ui/BackstageRichVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, v1, v2, p3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)V

    .line 1664192
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1664193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->q:Z

    .line 1664194
    invoke-direct {p0}, Lcom/facebook/audience/ui/BackstageRichVideoView;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664195
    invoke-direct {p0, p1, p2}, Lcom/facebook/audience/ui/BackstageRichVideoView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1664196
    :goto_0
    return-void

    .line 1664197
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/audience/ui/BackstageRichVideoView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1664188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->p:Z

    .line 1664189
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1664190
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1664184
    iput-boolean v1, p0, Lcom/facebook/audience/ui/BackstageRichVideoView;->p:Z

    .line 1664185
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1664186
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1664187
    return-void
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 1664183
    sget-object v0, LX/04D;->BACKSTAGE_VIDEOS:LX/04D;

    return-object v0
.end method

.method public getDefaultPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1664182
    sget-object v0, LX/04G;->OTHERS:LX/04G;

    return-object v0
.end method
