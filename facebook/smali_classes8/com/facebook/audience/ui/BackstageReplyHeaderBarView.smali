.class public Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/1xf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/view/View;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final e:Lcom/facebook/resources/ui/FbTextView;

.field public f:LX/AKk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664093
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1664094
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664095
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664096
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664097
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664098
    const-class v0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    invoke-static {v0, p0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1664099
    const v0, 0x7f0311c6

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1664100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->setOrientation(I)V

    .line 1664101
    const v0, 0x7f0d29bd

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->b:Landroid/view/View;

    .line 1664102
    const v0, 0x7f0d29bb

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1664103
    const v0, 0x7f0d29ba

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1664104
    const v0, 0x7f0d06b7

    invoke-virtual {p0, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1664105
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->b:Landroid/view/View;

    new-instance v1, LX/AKj;

    invoke-direct {v1, p0}, LX/AKj;-><init>(Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1664106
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    invoke-static {v0}, LX/1xf;->a(LX/0QB;)LX/1xf;

    move-result-object v0

    check-cast v0, LX/1xf;

    iput-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a:LX/1xf;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLandroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1664107
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1664108
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1664109
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1664110
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1664111
    iget-object v0, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a:LX/1xf;

    sget-object v2, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {v1, v2, p2, p3}, LX/1xf;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1664112
    return-void
.end method

.method public setInteractionListener(LX/AKk;)V
    .locals 0

    .prologue
    .line 1664113
    iput-object p1, p0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->f:LX/AKk;

    .line 1664114
    return-void
.end method
