.class public final Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/ui/ReplyThreadStoryView;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/ui/ReplyThreadStoryView;)V
    .locals 0

    .prologue
    .line 1664404
    iput-object p1, p0, Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;->a:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/audience/ui/ReplyThreadStoryView;B)V
    .locals 0

    .prologue
    .line 1664405
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;-><init>(Lcom/facebook/audience/ui/ReplyThreadStoryView;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1664406
    iget-object v0, p0, Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;->a:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iget-object v0, v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;->c:Lcom/facebook/audience/ui/BackstageRichVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlaybackPercentage()F

    move-result v0

    .line 1664407
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1664408
    if-lez v0, :cond_1

    .line 1664409
    iget-object v1, p0, Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;->a:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iget-object v1, v1, Lcom/facebook/audience/ui/ReplyThreadStoryView;->f:LX/AKv;

    invoke-virtual {v1}, LX/AKv;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1664410
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1664411
    const-string v3, "current_position"

    const/16 v4, 0x5f

    if-lt v0, v4, :cond_0

    const/16 v0, 0x64

    :cond_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1664412
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1664413
    iget-object v0, p0, Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;->a:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iget-object v0, v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;->f:LX/AKv;

    invoke-virtual {v0, v1}, LX/AKv;->sendMessage(Landroid/os/Message;)Z

    .line 1664414
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/ui/ReplyThreadStoryView$UpdateSeekBarRunnable;->a:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iget-object v0, v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;->f:LX/AKv;

    const-wide/16 v2, 0x32

    const v1, -0x1933c27a

    invoke-static {v0, p0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1664415
    return-void
.end method
