.class public Lcom/facebook/audience/ui/ReplyRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field public h:LX/AKu;

.field public i:LX/AKt;

.field private j:LX/3rW;

.field public k:Z

.field private final l:Landroid/view/GestureDetector$SimpleOnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664271
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/audience/ui/ReplyRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1664272
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664273
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/ui/ReplyRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664274
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664275
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->k:Z

    .line 1664277
    new-instance v0, LX/AKn;

    invoke-direct {v0, p0}, LX/AKn;-><init>(Lcom/facebook/audience/ui/ReplyRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->l:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 1664278
    new-instance v0, LX/3rW;

    iget-object v1, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->l:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p1, v1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->j:LX/3rW;

    .line 1664279
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1664280
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(FF)Landroid/view/View;

    move-result-object v0

    .line 1664281
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    .line 1664282
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1664283
    iget-object v0, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->j:LX/3rW;

    invoke-virtual {v0, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664284
    const/4 v0, 0x1

    .line 1664285
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1664286
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1664287
    invoke-virtual {p0}, Lcom/facebook/audience/ui/ReplyRecyclerView;->getHeight()I

    move-result v1

    .line 1664288
    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 1664289
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1664290
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1OM;->i_(I)V

    .line 1664291
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1664292
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 1664293
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->onMeasure(II)V

    .line 1664294
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3a4eabfe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1664295
    iget-boolean v2, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->k:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    if-nez v2, :cond_1

    .line 1664296
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x2d70fc5a

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1664297
    :goto_0
    return v0

    .line 1664298
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1664299
    if-nez v2, :cond_2

    .line 1664300
    invoke-direct {p0, p1}, Lcom/facebook/audience/ui/ReplyRecyclerView;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1664301
    iget-object v2, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    invoke-virtual {v2, v0}, LX/AKu;->a(Z)V

    .line 1664302
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    .line 1664303
    iget-boolean v3, v2, LX/AKu;->k:Z

    move v2, v3

    .line 1664304
    if-eqz v2, :cond_4

    .line 1664305
    iget-object v2, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    invoke-virtual {v2, p1}, LX/AKu;->a(Landroid/view/MotionEvent;)V

    .line 1664306
    const v2, -0x568ab20

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1664307
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/AKu;->a(Z)V

    goto :goto_1

    .line 1664308
    :cond_4
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x323b81b0

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setEventListener(LX/AKt;)V
    .locals 0

    .prologue
    .line 1664309
    iput-object p1, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->i:LX/AKt;

    .line 1664310
    return-void
.end method

.method public setGestureController(LX/AKu;)V
    .locals 0

    .prologue
    .line 1664311
    iput-object p1, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    .line 1664312
    return-void
.end method

.method public setSwipeToDismissEnabled(Z)V
    .locals 0

    .prologue
    .line 1664313
    iput-boolean p1, p0, Lcom/facebook/audience/ui/ReplyRecyclerView;->k:Z

    .line 1664314
    return-void
.end method
