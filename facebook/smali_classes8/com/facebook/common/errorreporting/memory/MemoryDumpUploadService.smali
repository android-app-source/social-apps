.class public Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;
.super LX/1ZN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/8yN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/11H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0VP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/app/ActivityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/00G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0VQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1425447
    const-class v0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;

    const-string v1, "infrastructure"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1425353
    const-string v0, "MemDumpUploadService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1425354
    return-void
.end method

.method private a(Ljava/io/File;Ljava/lang/String;ILjava/lang/String;)LX/8yP;
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1425405
    const-string v4, ""

    const-string v3, ""

    const-string v2, ""

    .line 1425406
    const-string v1, ""

    .line 1425407
    const/4 v0, -0x1

    .line 1425408
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1425409
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1425410
    const-string v6, "Dump cause"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1425411
    const-string v6, "Is Backgrounded"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1425412
    const-string v6, "Was Ever Foregrounded"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1425413
    const-string v6, "app_version_name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1425414
    const-string v6, "app_version_code"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1425415
    :cond_0
    :goto_0
    new-instance v5, LX/8yO;

    invoke-direct {v5, p1}, LX/8yO;-><init>(Ljava/io/File;)V

    .line 1425416
    iput-object p2, v5, LX/8yO;->c:Ljava/lang/String;

    .line 1425417
    move-object v5, v5

    .line 1425418
    iput p3, v5, LX/8yO;->b:I

    .line 1425419
    move-object v5, v5

    .line 1425420
    iput v0, v5, LX/8yO;->d:I

    .line 1425421
    move-object v0, v5

    .line 1425422
    iget-object v5, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->e:Landroid/app/ActivityManager;

    invoke-virtual {v5}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v5

    .line 1425423
    iput v5, v0, LX/8yO;->f:I

    .line 1425424
    move-object v0, v0

    .line 1425425
    iput-object v1, v0, LX/8yO;->i:Ljava/lang/String;

    .line 1425426
    move-object v0, v0

    .line 1425427
    iget-object v1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->h:LX/00H;

    .line 1425428
    iget-object v5, v1, LX/00H;->b:Ljava/lang/String;

    move-object v1, v5

    .line 1425429
    iput-object v1, v0, LX/8yO;->j:Ljava/lang/String;

    .line 1425430
    move-object v0, v0

    .line 1425431
    iget-object v1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->g:LX/00G;

    .line 1425432
    iget-object v5, v1, LX/00G;->b:Ljava/lang/String;

    move-object v1, v5

    .line 1425433
    iput-object v1, v0, LX/8yO;->k:Ljava/lang/String;

    .line 1425434
    move-object v0, v0

    .line 1425435
    iget-object v1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->f:Ljava/lang/String;

    .line 1425436
    iput-object v1, v0, LX/8yO;->l:Ljava/lang/String;

    .line 1425437
    move-object v0, v0

    .line 1425438
    iput-object v4, v0, LX/8yO;->m:Ljava/lang/String;

    .line 1425439
    move-object v0, v0

    .line 1425440
    iput-object v3, v0, LX/8yO;->n:Ljava/lang/String;

    .line 1425441
    move-object v0, v0

    .line 1425442
    iput-object v2, v0, LX/8yO;->o:Ljava/lang/String;

    .line 1425443
    move-object v0, v0

    .line 1425444
    invoke-virtual {v0}, LX/8yO;->a()LX/8yP;

    move-result-object v0

    return-object v0

    .line 1425445
    :catch_0
    move-exception v5

    .line 1425446
    const-string v6, "MemoryDumpUploadService"

    const-string v7, "Error: Metadata can\'t be decoded to json format "

    invoke-static {v6, v7, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;LX/8yN;LX/11H;LX/0ka;LX/0VP;Landroid/app/ActivityManager;Ljava/lang/String;LX/00G;LX/00H;LX/0VQ;)V
    .locals 0

    .prologue
    .line 1425404
    iput-object p1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->a:LX/8yN;

    iput-object p2, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->b:LX/11H;

    iput-object p3, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->c:LX/0ka;

    iput-object p4, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->d:LX/0VP;

    iput-object p5, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->e:Landroid/app/ActivityManager;

    iput-object p6, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->g:LX/00G;

    iput-object p8, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->h:LX/00H;

    iput-object p9, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->i:LX/0VQ;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;

    new-instance v1, LX/8yN;

    invoke-direct {v1}, LX/8yN;-><init>()V

    move-object v1, v1

    move-object v1, v1

    check-cast v1, LX/8yN;

    invoke-static {v9}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v2

    check-cast v2, LX/11H;

    invoke-static {v9}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v3

    check-cast v3, LX/0ka;

    invoke-static {v9}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v4

    check-cast v4, LX/0VP;

    invoke-static {v9}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    invoke-static {v9}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v9}, LX/0VX;->b(LX/0QB;)LX/00G;

    move-result-object v7

    check-cast v7, LX/00G;

    const-class v8, LX/00H;

    invoke-interface {v9, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/00H;

    invoke-static {v9}, LX/0VQ;->a(LX/0QB;)LX/0VQ;

    move-result-object v9

    check-cast v9, LX/0VQ;

    invoke-static/range {v0 .. v9}, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->a(Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;LX/8yN;LX/11H;LX/0ka;LX/0VP;Landroid/app/ActivityManager;Ljava/lang/String;LX/00G;LX/00H;LX/0VQ;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v0, 0x24

    const v1, -0x362a6ac

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1425358
    iget-object v0, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->d:LX/0VP;

    iget-object v1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->d:LX/0VP;

    invoke-virtual {v1}, LX/0VP;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/2Hr;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1, v3}, LX/0VP;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v5

    .line 1425359
    sget-boolean v0, LX/0VQ;->e:Z

    if-eqz v0, :cond_8

    .line 1425360
    const-string v0, ""

    .line 1425361
    :cond_0
    :goto_0
    move-object v6, v0

    .line 1425362
    iget-object v0, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->c:LX/0ka;

    invoke-virtual {v0, v7}, LX/0ka;->a(Z)Z

    move-result v7

    .line 1425363
    if-eqz v5, :cond_1

    array-length v0, v5

    if-nez v0, :cond_2

    .line 1425364
    :cond_1
    const/16 v0, 0x25

    const v1, -0x6a5159c1

    invoke-static {v8, v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1425365
    :goto_1
    return-void

    .line 1425366
    :cond_2
    :try_start_0
    array-length v8, v5

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v8, :cond_5

    aget-object v9, v5, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1425367
    if-eqz v7, :cond_7

    .line 1425368
    :try_start_1
    sget-object v0, LX/2Hr;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1425369
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1425370
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 1425371
    :goto_3
    const-string v1, "hprof_compression"

    const v10, 0x5f464bd

    invoke-static {v1, v10}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1425372
    :try_start_2
    const-string v1, "%s.gz"

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1425373
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1425374
    :try_start_3
    invoke-static {v9, v1}, LX/0VP;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 1425375
    const v10, 0x7aa960f8

    :try_start_4
    invoke-static {v10}, LX/02m;->a(I)V

    .line 1425376
    const/4 v10, 0x5

    invoke-direct {p0, v1, v0, v10, v6}, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->a(Ljava/io/File;Ljava/lang/String;ILjava/lang/String;)LX/8yP;

    move-result-object v0

    .line 1425377
    iget-object v10, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->b:LX/11H;

    iget-object v11, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->a:LX/8yN;

    sget-object v12, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v10, v11, v0, v12}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1425378
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 1425379
    :goto_4
    :try_start_5
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1425380
    if-eqz v1, :cond_3

    .line 1425381
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1425382
    :cond_3
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1425383
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    const v10, 0x72e0dbfa

    :try_start_6
    invoke-static {v10}, LX/02m;->a(I)V

    const v10, 0x606858bb

    invoke-static {v10, v4}, LX/02F;->d(II)V

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 1425384
    :catch_0
    move-exception v0

    .line 1425385
    :goto_7
    :try_start_7
    const-string v10, "MemoryDumpUploadService"

    const-string v11, "Error uploading hprof file: "

    invoke-static {v10, v11, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 1425386
    :try_start_8
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1425387
    if-eqz v1, :cond_3

    .line 1425388
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_5

    .line 1425389
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->i:LX/0VQ;

    invoke-virtual {v1}, LX/0VQ;->b()LX/0VQ;

    const v1, -0x2a99e062

    invoke-static {v1, v4}, LX/02F;->d(II)V

    throw v0

    .line 1425390
    :catchall_2
    move-exception v0

    move-object v1, v2

    :goto_8
    :try_start_9
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1425391
    if-eqz v1, :cond_4

    .line 1425392
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_4
    const v1, 0x5520748b

    invoke-static {v1, v4}, LX/02F;->d(II)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1425393
    :cond_5
    iget-object v0, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->i:LX/0VQ;

    invoke-virtual {v0}, LX/0VQ;->b()LX/0VQ;

    .line 1425394
    const v0, -0x75c60f44

    invoke-static {v0, v4}, LX/02F;->d(II)V

    goto/16 :goto_1

    .line 1425395
    :catchall_3
    move-exception v0

    goto :goto_8

    .line 1425396
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_7

    .line 1425397
    :catchall_4
    move-exception v0

    goto :goto_6

    :cond_6
    move-object v0, v2

    goto/16 :goto_3

    :cond_7
    move-object v1, v2

    goto :goto_4

    .line 1425398
    :cond_8
    sget-object v0, LX/0VQ;->c:Landroid/content/SharedPreferences;

    sget-object v1, LX/0VQ;->a:LX/0Tn;

    invoke-virtual {v1}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1425399
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1425400
    const-string v0, ""

    goto/16 :goto_0

    .line 1425401
    :cond_9
    sget-object v1, LX/0VQ;->c:Landroid/content/SharedPreferences;

    invoke-static {v0}, LX/0VQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-virtual {v0}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1425402
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1425403
    const-string v1, "Warning@getDumpMetadata(): No dump metadata found"

    const/4 v3, 0x0

    invoke-static {v1, v3}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x50cb94de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1425355
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1425356
    invoke-static {p0, p0}, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1425357
    const/16 v1, 0x25

    const v2, -0x55f91f0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
