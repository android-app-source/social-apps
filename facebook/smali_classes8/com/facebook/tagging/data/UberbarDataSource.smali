.class public Lcom/facebook/tagging/data/UberbarDataSource;
.super LX/8nB;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0TV;

.field public final d:LX/7Ba;

.field public final e:LX/11H;

.field public final f:LX/3iT;

.field private final g:LX/8nC;

.field public final h:LX/0ad;

.field public final i:Landroid/os/Handler;

.field private final j:LX/8p4;

.field public k:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1400724
    const-class v0, Lcom/facebook/tagging/data/UberbarDataSource;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tagging/data/UberbarDataSource;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0TV;LX/7Ba;LX/11H;LX/3iT;LX/8nC;LX/0ad;LX/8p4;)V
    .locals 2
    .param p2    # LX/0TV;
        .annotation runtime Lcom/facebook/common/executors/SearchTypeaheadNetworkExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400725
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1400726
    iput-object p1, p0, Lcom/facebook/tagging/data/UberbarDataSource;->b:Landroid/content/res/Resources;

    .line 1400727
    iput-object p2, p0, Lcom/facebook/tagging/data/UberbarDataSource;->c:LX/0TV;

    .line 1400728
    iput-object p3, p0, Lcom/facebook/tagging/data/UberbarDataSource;->d:LX/7Ba;

    .line 1400729
    iput-object p4, p0, Lcom/facebook/tagging/data/UberbarDataSource;->e:LX/11H;

    .line 1400730
    iput-object p5, p0, Lcom/facebook/tagging/data/UberbarDataSource;->f:LX/3iT;

    .line 1400731
    iput-object p6, p0, Lcom/facebook/tagging/data/UberbarDataSource;->g:LX/8nC;

    .line 1400732
    iput-object p7, p0, Lcom/facebook/tagging/data/UberbarDataSource;->h:LX/0ad;

    .line 1400733
    iput-object p8, p0, Lcom/facebook/tagging/data/UberbarDataSource;->j:LX/8p4;

    .line 1400734
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/tagging/data/UberbarDataSource;->i:Landroid/os/Handler;

    .line 1400735
    return-void
.end method

.method private static a(ZZ)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "LX/0Px",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400736
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1400737
    if-eqz p0, :cond_0

    .line 1400738
    sget-object v1, LX/7BK;->USER:LX/7BK;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1400739
    :cond_0
    if-eqz p1, :cond_1

    .line 1400740
    sget-object v1, LX/7BK;->PAGE:LX/7BK;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1400741
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;LX/0Px;Ljava/util/List;LX/8JX;)V
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/0Px",
            "<",
            "LX/7BK;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;",
            "LX/8JX;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1400742
    invoke-interface {p4, p1, p3}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1400743
    iget-object v1, p0, Lcom/facebook/tagging/data/UberbarDataSource;->h:LX/0ad;

    sget-short v2, LX/8mx;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 1400744
    if-nez v1, :cond_2

    .line 1400745
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    .line 1400746
    :cond_0
    const/4 v0, 0x5

    if-ge v1, v0, :cond_1

    .line 1400747
    if-eqz p1, :cond_1

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1400748
    :cond_1
    :goto_0
    return-void

    .line 1400749
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 1400750
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400751
    iget-object v4, p0, Lcom/facebook/tagging/data/UberbarDataSource;->j:LX/8p4;

    invoke-virtual {v4, v0, v2}, LX/8p4;->a(Lcom/facebook/tagging/model/TaggingProfile;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1400752
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 1400753
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 1400754
    :cond_4
    new-instance v5, Lcom/facebook/tagging/data/UberbarDataSource$2;

    invoke-direct {v5, p0, p1, p2, p4}, Lcom/facebook/tagging/data/UberbarDataSource$2;-><init>(Lcom/facebook/tagging/data/UberbarDataSource;Ljava/lang/CharSequence;Ljava/util/List;LX/8JX;)V

    move-object v5, v5

    .line 1400755
    iget-object v6, p0, Lcom/facebook/tagging/data/UberbarDataSource;->h:LX/0ad;

    sget v7, LX/8mx;->e:I

    const/16 v8, 0x1f4

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    .line 1400756
    if-lez v6, :cond_5

    .line 1400757
    iget-object v7, p0, Lcom/facebook/tagging/data/UberbarDataSource;->i:Landroid/os/Handler;

    iget-object v8, p0, Lcom/facebook/tagging/data/UberbarDataSource;->k:Ljava/lang/Runnable;

    invoke-static {v7, v8}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1400758
    iget-object v7, p0, Lcom/facebook/tagging/data/UberbarDataSource;->i:Landroid/os/Handler;

    int-to-long v9, v6

    const v6, 0x91d02c8

    invoke-static {v7, v5, v9, v10, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1400759
    iput-object v5, p0, Lcom/facebook/tagging/data/UberbarDataSource;->k:Ljava/lang/Runnable;

    goto :goto_0

    .line 1400760
    :cond_5
    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 10

    .prologue
    .line 1400761
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/8nB;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1400762
    :cond_0
    :goto_0
    return-void

    .line 1400763
    :cond_1
    invoke-static {p4, p5}, Lcom/facebook/tagging/data/UberbarDataSource;->a(ZZ)LX/0Px;

    move-result-object v2

    .line 1400764
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1400765
    iget-object v1, p0, Lcom/facebook/tagging/data/UberbarDataSource;->g:LX/8nC;

    new-instance v9, LX/8nH;

    move-object/from16 v0, p8

    invoke-direct {v9, p0, v2, v0}, LX/8nH;-><init>(Lcom/facebook/tagging/data/UberbarDataSource;LX/0Px;LX/8JX;)V

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v9}, LX/8nB;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1400766
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400767
    const-string v0, "uberbar"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400768
    const-string v0, "@"

    return-object v0
.end method
