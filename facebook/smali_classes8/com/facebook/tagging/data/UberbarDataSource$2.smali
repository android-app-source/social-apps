.class public final Lcom/facebook/tagging/data/UberbarDataSource$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/CharSequence;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/8JX;

.field public final synthetic d:Lcom/facebook/tagging/data/UberbarDataSource;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/data/UberbarDataSource;Ljava/lang/CharSequence;Ljava/util/List;LX/8JX;)V
    .locals 0

    .prologue
    .line 1400704
    iput-object p1, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->d:Lcom/facebook/tagging/data/UberbarDataSource;

    iput-object p2, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->a:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->b:Ljava/util/List;

    iput-object p4, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->c:LX/8JX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1400705
    new-instance v0, LX/7BS;

    invoke-direct {v0}, LX/7BS;-><init>()V

    iget-object v1, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    .line 1400706
    iput-object v1, v0, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1400707
    move-object v0, v0

    .line 1400708
    const/4 v1, 0x5

    .line 1400709
    iput v1, v0, LX/7BS;->e:I

    .line 1400710
    move-object v0, v0

    .line 1400711
    iget-object v1, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->b:Ljava/util/List;

    .line 1400712
    iput-object v1, v0, LX/7BS;->d:Ljava/util/List;

    .line 1400713
    move-object v0, v0

    .line 1400714
    iget-object v1, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->d:Lcom/facebook/tagging/data/UberbarDataSource;

    iget-object v1, v1, Lcom/facebook/tagging/data/UberbarDataSource;->b:Landroid/content/res/Resources;

    const v2, 0x7f0b008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1400715
    iput v1, v0, LX/7BS;->c:I

    .line 1400716
    move-object v0, v0

    .line 1400717
    const-string v1, "mobile_android_tagger"

    .line 1400718
    iput-object v1, v0, LX/7BS;->k:Ljava/lang/String;

    .line 1400719
    move-object v0, v0

    .line 1400720
    invoke-virtual {v0}, LX/7BS;->f()Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    move-result-object v0

    .line 1400721
    iget-object v1, p0, Lcom/facebook/tagging/data/UberbarDataSource$2;->d:Lcom/facebook/tagging/data/UberbarDataSource;

    iget-object v1, v1, Lcom/facebook/tagging/data/UberbarDataSource;->c:LX/0TV;

    new-instance v2, LX/8nI;

    invoke-direct {v2, p0, v0}, LX/8nI;-><init>(Lcom/facebook/tagging/data/UberbarDataSource$2;Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1400722
    new-instance v1, LX/8nJ;

    invoke-direct {v1, p0}, LX/8nJ;-><init>(Lcom/facebook/tagging/data/UberbarDataSource$2;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1400723
    return-void
.end method
