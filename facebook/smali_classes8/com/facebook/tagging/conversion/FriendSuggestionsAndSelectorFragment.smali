.class public Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private A:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/7kp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/A5T;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private D:LX/75F;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private E:Landroid/content/ContentResolver;

.field public F:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/3Oq;
    .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private H:LX/A5h;

.field public I:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:LX/A5m;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private L:LX/8uo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/3LP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public N:LX/2RQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final O:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public P:LX/8tC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public final R:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private S:Landroid/view/ViewGroup;

.field public T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8vB;",
            ">;"
        }
    .end annotation
.end field

.field public U:Z

.field public V:LX/8nB;

.field public W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

.field public X:Z

.field public final Y:Landroid/widget/AbsListView$OnScrollListener;

.field private final Z:Landroid/database/ContentObserver;

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9jA;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8RL;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/A5p;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8nF;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/ImageView;

.field private i:LX/74G;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/8JL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Sy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/widget/listview/BetterListView;

.field public o:Landroid/widget/TextView;

.field public p:Landroid/view/View;

.field public q:Landroid/view/View;

.field private r:LX/0WJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:I

.field public t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public u:LX/0Ot;
    .annotation runtime Lcom/facebook/tagging/conversion/annotation/TaggingConversionPhotoFlowLogger;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;"
        }
    .end annotation
.end field

.field private v:Z

.field public w:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private x:Z

.field public y:Z

.field public z:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1621994
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1621995
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1621996
    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->u:LX/0Ot;

    .line 1621997
    iput-boolean v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->v:Z

    .line 1621998
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    .line 1621999
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    .line 1622000
    iput-boolean v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->U:Z

    .line 1622001
    new-instance v0, LX/A5Y;

    invoke-direct {v0, p0}, LX/A5Y;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Y:Landroid/widget/AbsListView$OnScrollListener;

    .line 1622002
    new-instance v0, LX/A5Z;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, LX/A5Z;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Z:Landroid/database/ContentObserver;

    .line 1622003
    return-void
.end method

.method public static a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1622004
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    check-cast v0, [LX/8ul;

    .line 1622005
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1622006
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1622007
    iget-object p0, v4, LX/8uk;->f:LX/8QK;

    move-object v4, p0

    .line 1622008
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1622009
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1622010
    :cond_0
    return-object v2
.end method

.method public static a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;ILX/A5i;LX/0Px;)V
    .locals 3

    .prologue
    .line 1622011
    new-instance v0, LX/623;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p3}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1622012
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    invoke-virtual {p2}, LX/A5i;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v0}, LX/8tB;->a(ILX/621;)V

    .line 1622013
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    const v1, 0x44cf9620

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1622014
    return-void
.end method

.method private static a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/74G;LX/8JL;LX/0Sy;Landroid/view/inputmethod/InputMethodManager;LX/0WJ;LX/0Ot;LX/03V;LX/1Ck;LX/0Zb;LX/7kp;LX/A5T;LX/75F;Ljava/lang/Boolean;LX/3Oq;Ljava/util/concurrent/Executor;LX/A5m;LX/0TD;LX/8uo;LX/3LP;LX/2RQ;LX/8tC;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;",
            "LX/0Or",
            "<",
            "LX/9jA;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8RL;",
            ">;",
            "LX/0Or",
            "<",
            "LX/A5p;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8nF;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            ">;",
            "LX/74G;",
            "LX/8JL;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck;",
            "LX/0Zb;",
            "LX/7kp;",
            "LX/A5T;",
            "LX/75F;",
            "Ljava/lang/Boolean;",
            "LX/3Oq;",
            "Ljava/util/concurrent/Executor;",
            "LX/A5m;",
            "LX/0TD;",
            "LX/8uo;",
            "LX/3LP;",
            "LX/2RQ;",
            "LX/8tC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1622015
    iput-object p1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->e:LX/0Or;

    iput-object p6, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->f:LX/0Or;

    iput-object p7, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->i:LX/74G;

    iput-object p9, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->j:LX/8JL;

    iput-object p10, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->l:LX/0Sy;

    iput-object p11, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    iput-object p12, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->r:LX/0WJ;

    iput-object p13, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->u:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->w:LX/03V;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->z:LX/1Ck;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->A:LX/0Zb;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->C:LX/A5T;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->D:LX/75F;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->F:Ljava/lang/Boolean;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->G:LX/3Oq;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->I:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->J:LX/A5m;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->K:LX/0TD;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->L:LX/8uo;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->M:LX/3LP;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->N:LX/2RQ;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    return-void
.end method

.method public static a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/A5j;Z)V
    .locals 8

    .prologue
    .line 1622016
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1622017
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1622018
    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK_OBJECT:LX/0XG;

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1622019
    iget-wide v6, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v4, v6

    .line 1622020
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 1622021
    new-instance v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1622022
    iget-object v4, v0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v4, v4

    .line 1622023
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1622024
    iget-object v5, v0, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v0, v5

    .line 1622025
    invoke-direct {v3, v4, v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 1622026
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622027
    iget-object v2, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    move-object v2, v2

    .line 1622028
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1622029
    iget-wide v6, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v4, v6

    .line 1622030
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622031
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1622032
    iget-wide v6, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v4, v6

    .line 1622033
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, LX/A5j;->a(Ljava/lang/Long;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1622034
    if-eqz p2, :cond_0

    .line 1622035
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1622036
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1622037
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 30

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v29

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    const/16 v2, 0x2f23

    move-object/from16 v0, v29

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x455

    move-object/from16 v0, v29

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1032

    move-object/from16 v0, v29

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x378a

    move-object/from16 v0, v29

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x35d2

    move-object/from16 v0, v29

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x35d8

    move-object/from16 v0, v29

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x3f4

    move-object/from16 v0, v29

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {v29 .. v29}, LX/74G;->a(LX/0QB;)LX/74G;

    move-result-object v9

    check-cast v9, LX/74G;

    invoke-static/range {v29 .. v29}, LX/8JL;->a(LX/0QB;)LX/8JL;

    move-result-object v10

    check-cast v10, LX/8JL;

    invoke-static/range {v29 .. v29}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v11

    check-cast v11, LX/0Sy;

    invoke-static/range {v29 .. v29}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v12

    check-cast v12, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {v29 .. v29}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v13

    check-cast v13, LX/0WJ;

    const/16 v14, 0x2e05

    move-object/from16 v0, v29

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v29 .. v29}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v15

    check-cast v15, LX/03V;

    invoke-static/range {v29 .. v29}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v16

    check-cast v16, LX/1Ck;

    invoke-static/range {v29 .. v29}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v17

    check-cast v17, LX/0Zb;

    invoke-static/range {v29 .. v29}, LX/7kp;->a(LX/0QB;)LX/7kp;

    move-result-object v18

    check-cast v18, LX/7kp;

    invoke-static/range {v29 .. v29}, LX/A5T;->a(LX/0QB;)LX/A5T;

    move-result-object v19

    check-cast v19, LX/A5T;

    invoke-static/range {v29 .. v29}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v20

    check-cast v20, LX/75F;

    invoke-static/range {v29 .. v29}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v21

    check-cast v21, Ljava/lang/Boolean;

    invoke-static/range {v29 .. v29}, LX/6NS;->a(LX/0QB;)LX/3Oq;

    move-result-object v22

    check-cast v22, LX/3Oq;

    invoke-static/range {v29 .. v29}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v23

    check-cast v23, Ljava/util/concurrent/Executor;

    invoke-static/range {v29 .. v29}, LX/A5m;->a(LX/0QB;)LX/A5m;

    move-result-object v24

    check-cast v24, LX/A5m;

    invoke-static/range {v29 .. v29}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v25

    check-cast v25, LX/0TD;

    invoke-static/range {v29 .. v29}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v26

    check-cast v26, LX/8uo;

    invoke-static/range {v29 .. v29}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v27

    check-cast v27, LX/3LP;

    invoke-static/range {v29 .. v29}, LX/2RQ;->a(LX/0QB;)LX/2RQ;

    move-result-object v28

    check-cast v28, LX/2RQ;

    invoke-static/range {v29 .. v29}, LX/8tC;->b(LX/0QB;)LX/8tC;

    move-result-object v29

    check-cast v29, LX/8tC;

    invoke-static/range {v1 .. v29}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/74G;LX/8JL;LX/0Sy;Landroid/view/inputmethod/InputMethodManager;LX/0WJ;LX/0Ot;LX/03V;LX/1Ck;LX/0Zb;LX/7kp;LX/A5T;LX/75F;Ljava/lang/Boolean;LX/3Oq;Ljava/util/concurrent/Executor;LX/A5m;LX/0TD;LX/8uo;LX/3LP;LX/2RQ;LX/8tC;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1622038
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 1622039
    iput-object p1, v1, LX/108;->g:Ljava/lang/String;

    .line 1622040
    move-object v1, v1

    .line 1622041
    const/4 v2, 0x1

    .line 1622042
    iput-boolean v2, v1, LX/108;->q:Z

    .line 1622043
    move-object v1, v1

    .line 1622044
    iput-boolean p2, v1, LX/108;->d:Z

    .line 1622045
    move-object v1, v1

    .line 1622046
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1622047
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1622174
    const/4 v0, 0x0

    .line 1622175
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1622176
    const/4 v9, 0x0

    .line 1622177
    instance-of v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v3, :cond_0

    move-object v3, v0

    .line 1622178
    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1622179
    iget-object v4, v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v3, v4

    .line 1622180
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 1622181
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1622182
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1622183
    new-instance v4, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v4 .. v9}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-interface {p2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1622184
    const/4 v9, 0x1

    .line 1622185
    :cond_0
    move v0, v9

    .line 1622186
    if-eqz v0, :cond_2

    .line 1622187
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 1622188
    goto :goto_0

    .line 1622189
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)V
    .locals 4

    .prologue
    .line 1622048
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->T:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1622049
    :cond_0
    return-void

    .line 1622050
    :cond_1
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vB;

    .line 1622051
    iget-object v2, v0, LX/8vB;->e:Ljava/util/List;

    move-object v2, v2

    .line 1622052
    if-eqz v2, :cond_2

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1622053
    iget-object v2, v0, LX/8vB;->e:Ljava/util/List;

    move-object v2, v2

    .line 1622054
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1622055
    invoke-static {p0, v2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)I

    move-result v2

    if-nez v2, :cond_3

    .line 1622056
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1622057
    :goto_1
    goto :goto_0

    .line 1622058
    :cond_4
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V
    .locals 13

    .prologue
    .line 1622059
    instance-of v0, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-nez v0, :cond_0

    .line 1622060
    :goto_0
    return-void

    .line 1622061
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    sget-object v1, LX/A5i;->SUGGESTIONS:LX/A5i;

    invoke-virtual {v1}, LX/A5i;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 1622062
    instance-of v1, v0, LX/A5n;

    if-nez v1, :cond_1

    instance-of v0, v0, LX/623;

    if-eqz v0, :cond_2

    .line 1622063
    :cond_1
    const/4 v0, 0x1

    const/16 v6, 0x32

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1622064
    invoke-static {p2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1622065
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 1622066
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v3, v6, :cond_3

    .line 1622067
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0813b5

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1622068
    :cond_2
    :goto_1
    invoke-static {p0, p1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)V

    goto :goto_0

    .line 1622069
    :cond_3
    invoke-static {p0, p1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)I

    move-result v3

    if-nez v3, :cond_4

    .line 1622070
    invoke-virtual {p2, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1622071
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1622072
    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3, p2}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 1622073
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1622074
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v7, :cond_5

    .line 1622075
    invoke-static {p0, v8}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V

    .line 1622076
    :cond_5
    if-eqz v0, :cond_7

    .line 1622077
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/73w;

    sget-object v3, LX/74U;->UNSET:LX/74U;

    sget-object v4, LX/74T;->COMPOSER_TAGGING_WITH_TAG:LX/74T;

    .line 1622078
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v10

    .line 1622079
    const-string v11, "is_text"

    if-eqz v7, :cond_d

    const-string v9, "true"

    :goto_2
    invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622080
    const-string v9, "ex_tag_screen"

    invoke-virtual {v4}, LX/74T;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622081
    const-string v9, "tag_src"

    invoke-virtual {v3}, LX/74U;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622082
    const-string v9, "ex_tag_index"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622083
    const-string v9, "ex_tag_text_length"

    iget v11, v2, LX/73w;->C:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622084
    if-ltz v8, :cond_6

    .line 1622085
    iget-wide v11, v2, LX/73w;->D:J

    invoke-static {v2, v10, v11, v12}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1622086
    :cond_6
    sget-object v9, LX/74R;->TAG_CREATED:LX/74R;

    const/4 v11, 0x0

    invoke-static {v2, v9, v10, v11}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1622087
    :cond_7
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1622088
    :cond_8
    :goto_3
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    const v3, -0x3159f657

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1

    .line 1622089
    :cond_9
    invoke-static {p0, p1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)I

    move-result v3

    if-ne v3, v7, :cond_a

    .line 1622090
    invoke-static {p0, p1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)LX/8QL;

    move-result-object v3

    invoke-virtual {p2, v3, v7}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1622091
    :cond_a
    invoke-static {p0, p1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)LX/8QL;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1622092
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1622093
    invoke-static {p0, v7}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V

    .line 1622094
    :cond_b
    if-eqz v0, :cond_c

    .line 1622095
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/73w;

    sget-object v3, LX/74T;->COMPOSER_TAGGING_WITH_TAG:LX/74T;

    .line 1622096
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 1622097
    const-string v5, "ex_tag_screen"

    invoke-virtual {v3}, LX/74T;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622098
    sget-object v5, LX/74R;->TAG_DELETED:LX/74R;

    const/4 v6, 0x0

    invoke-static {v2, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1622099
    :cond_c
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1622100
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1622101
    :cond_d
    const-string v9, "false"

    goto/16 :goto_2
.end method

.method public static a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1622102
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->X:Z

    if-nez v0, :cond_0

    .line 1622103
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1622104
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1622105
    :goto_0
    return-void

    .line 1622106
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1622107
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1622108
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)LX/8QL;
    .locals 4

    .prologue
    .line 1622109
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    .line 1622110
    invoke-static {p1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->c(LX/8QL;)Ljava/lang/String;

    move-result-object v1

    .line 1622111
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1622112
    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->c(LX/8QL;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1622113
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/tagging/model/TaggingProfile;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 6

    .prologue
    .line 1622114
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    .line 1622115
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1622116
    iput-object v1, v0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1622117
    move-object v0, v0

    .line 1622118
    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    .line 1622119
    iget-wide v4, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v2, v4

    .line 1622120
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    .line 1622121
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1622122
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1622123
    iput-object v1, v0, LX/0XI;->n:Ljava/lang/String;

    .line 1622124
    move-object v0, v0

    .line 1622125
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1622126
    new-instance v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    return-object v1
.end method

.method public static b(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V
    .locals 12

    .prologue
    .line 1621959
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 1621960
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1621961
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 1621962
    invoke-static {v2, v0, v1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->v:Z

    .line 1621963
    if-eqz p1, :cond_1

    .line 1621964
    const/4 v11, 0x1

    .line 1621965
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1621966
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1621967
    iget-wide v9, v5, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v9, v10}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v8

    iget-object v9, v5, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1621968
    iput-object v9, v8, LX/5Rc;->b:Ljava/lang/String;

    .line 1621969
    move-object v8, v8

    .line 1621970
    iget-object v5, v5, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1621971
    iput-object v5, v8, LX/5Rc;->c:Ljava/lang/String;

    .line 1621972
    move-object v5, v8

    .line 1621973
    invoke-virtual {v5}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1621974
    :cond_0
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v5

    iget-object v7, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621975
    iget-object v8, v7, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    move-object v7, v8

    .line 1621976
    iput-object v7, v5, LX/9jF;->q:LX/9jG;

    .line 1621977
    move-object v5, v5

    .line 1621978
    iget-object v7, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621979
    iget-object v8, v7, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v7, v8

    .line 1621980
    iput-object v7, v5, LX/9jF;->g:Ljava/lang/String;

    .line 1621981
    move-object v5, v5

    .line 1621982
    iput-boolean v11, v5, LX/9jF;->z:Z

    .line 1621983
    move-object v5, v5

    .line 1621984
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1621985
    iput-object v6, v5, LX/9jF;->r:LX/0Px;

    .line 1621986
    move-object v5, v5

    .line 1621987
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    .line 1621988
    iput-object v6, v5, LX/9jF;->s:LX/0Px;

    .line 1621989
    move-object v6, v5

    .line 1621990
    iget-object v5, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    .line 1621991
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v6

    invoke-static {v7, v6}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v6

    invoke-interface {v5, v6, v11, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1621992
    :goto_1
    return-void

    .line 1621993
    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b(Ljava/util/Set;Ljava/util/Set;)V

    goto :goto_1
.end method

.method private b(Ljava/util/Set;Ljava/util/Set;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1622127
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1622128
    const-string v0, "photo_tagged_set_modified"

    iget-boolean v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->v:Z

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1622129
    const-string v0, "profiles"

    invoke-static {p2}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 1622130
    const-string v0, "full_profiles"

    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1622131
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622132
    iget-boolean v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    move v0, v1

    .line 1622133
    if-eqz v0, :cond_0

    .line 1622134
    const-string v0, "extra_tagged_profiles"

    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1622135
    :cond_0
    const-string v0, "extra_place"

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622136
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 1622137
    invoke-static {v8, v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1622138
    const-string v0, "extra_implicit_location"

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622139
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-object v1, v2

    .line 1622140
    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1622141
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622142
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v1

    .line 1622143
    if-eqz v0, :cond_1

    .line 1622144
    const-string v0, "minutiae_object"

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622145
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 1622146
    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1622147
    :cond_1
    invoke-static {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1622148
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "place_picker_place_to_people_skip"

    .line 1622149
    :goto_0
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->A:LX/0Zb;

    invoke-interface {v1, v0, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1622150
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1622151
    const-string v1, "from_checkin"

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622152
    iget-boolean v3, v2, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    move v2, v3

    .line 1622153
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1622154
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1622155
    :cond_2
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jA;

    .line 1622156
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622157
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 1622158
    if-nez v1, :cond_5

    .line 1622159
    :goto_1
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->C:LX/A5T;

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->J:LX/A5m;

    invoke-virtual {v2}, LX/A5m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)Z

    move-result v3

    iget-boolean v4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->y:Z

    iget-object v6, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622160
    iget-object v7, v6, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    move-object v6, v7

    .line 1622161
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    :goto_2
    iget-object v6, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->T:Ljava/util/List;

    iget-object v7, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1622162
    iget-object p1, v7, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v7, p1

    .line 1622163
    invoke-virtual/range {v0 .. v7}, LX/A5T;->a(Ljava/util/ArrayList;Ljava/lang/String;ZZZLjava/util/List;Ljava/lang/String;)V

    .line 1622164
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v8}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1622165
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1622166
    return-void

    .line 1622167
    :cond_3
    const-string v0, "place_picker_place_to_people_select"

    goto :goto_0

    .line 1622168
    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    .line 1622169
    :cond_5
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1622170
    iget-object v2, v0, LX/9jA;->c:LX/9jB;

    invoke-virtual {v2}, LX/9jB;->b()V

    .line 1622171
    goto :goto_1

    .line 1622172
    :cond_6
    iget-object v2, v0, LX/9jA;->c:LX/9jB;

    invoke-virtual {v2}, LX/9jB;->a()V

    .line 1622173
    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1621771
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1621772
    return-void
.end method

.method private static c(LX/8QL;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1621776
    invoke-virtual {p0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V
    .locals 2

    .prologue
    .line 1621773
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->h:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1621774
    return-void

    .line 1621775
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static d(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)I
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1621777
    const-wide/16 v0, -0x1

    .line 1621778
    instance-of v2, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v2, :cond_2

    .line 1621779
    invoke-virtual {p1}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 1621780
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    move-wide v2, v0

    .line 1621781
    :goto_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v4

    :goto_1
    if-ge v4, v5, :cond_0

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1621782
    instance-of v6, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v6, :cond_1

    .line 1621783
    invoke-virtual {v0}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 1621784
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v0, v6, v2

    if-nez v0, :cond_1

    .line 1621785
    add-int/lit8 v0, v1, 0x1

    .line 1621786
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_1

    .line 1621787
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move-wide v2, v0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1621788
    invoke-static {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1621789
    const v1, 0x7f080030

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Ljava/lang/String;Z)V

    .line 1621790
    :goto_0
    return-void

    .line 1621791
    :cond_0
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621792
    iget-boolean v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    move v1, v2

    .line 1621793
    if-eqz v1, :cond_2

    .line 1621794
    const v1, 0x7f080029

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1621795
    :cond_2
    const v1, 0x7f081397

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static o(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)Z
    .locals 1

    .prologue
    .line 1621796
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621797
    iget-object p0, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, p0

    .line 1621798
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1621799
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1621800
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->p:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1621801
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1621802
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 1621803
    :cond_0
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Landroid/view/View;)V

    .line 1621804
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    .line 1621805
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1621806
    invoke-static {p0, v3}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)I

    move-result v5

    if-nez v5, :cond_1

    .line 1621807
    iget-object v5, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v3, v5}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    goto :goto_0

    .line 1621808
    :cond_2
    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    const v4, 0x188297f4

    invoke-static {v3, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1621809
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    invoke-static {p0, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->c(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V

    .line 1621810
    return-void

    :cond_3
    move v0, v1

    .line 1621811
    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1621812
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1621813
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1621814
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621815
    const-string v1, "FragmentInjection"

    invoke-static {v0, v1}, LX/7kp;->c(LX/7kp;Ljava/lang/String;)V

    .line 1621816
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1621817
    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1621818
    if-eqz p1, :cond_0

    .line 1621819
    const-string v0, "friend_selector_config"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621820
    const-string v0, "has_backed_out_of_place_picker"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->x:Z

    .line 1621821
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    if-nez v0, :cond_1

    .line 1621822
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1621823
    if-eqz v0, :cond_1

    .line 1621824
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1621825
    const-string v1, "friend_selector_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1621826
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1621827
    const-string v1, "friend_selector_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621828
    :cond_1
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1621829
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621830
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1621831
    if-eqz v0, :cond_2

    .line 1621832
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73w;

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621833
    iget-object p1, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v1, p1

    .line 1621834
    invoke-virtual {v0, v1}, LX/73w;->a(Ljava/lang/String;)V

    .line 1621835
    :cond_2
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->i:LX/74G;

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621836
    iget-object p1, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v1, p1

    .line 1621837
    iput-object v1, v0, LX/74G;->a:Ljava/lang/String;

    .line 1621838
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->i:LX/74G;

    .line 1621839
    sget-object v1, LX/74F;->LAUNCH_FRIEND_TAGGER:LX/74F;

    invoke-static {v1}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1621840
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621841
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v0, v1

    .line 1621842
    if-eqz v0, :cond_3

    .line 1621843
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nF;

    .line 1621844
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621845
    iget-object p1, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v1, p1

    .line 1621846
    invoke-interface {v0, v1}, LX/8nF;->a(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)LX/8nB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->V:LX/8nB;

    .line 1621847
    :cond_3
    return-void

    .line 1621848
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1621849
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v1, 0x7f0d12a7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->h:Landroid/widget/ImageView;

    .line 1621850
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->h:Landroid/widget/ImageView;

    const v1, 0x7f021620

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1621851
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->h:Landroid/widget/ImageView;

    new-instance v1, LX/A5b;

    invoke-direct {v1, p0}, LX/A5b;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1621852
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1621853
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621854
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v0, v1

    .line 1621855
    if-eqz v0, :cond_0

    .line 1621856
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->V:LX/8nB;

    new-instance v1, LX/A5e;

    invoke-direct {v1, p0}, LX/A5e;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v0, v1}, LX/8nB;->a(LX/8JX;)V

    .line 1621857
    :goto_0
    new-instance v0, LX/A5h;

    invoke-direct {v0, p0}, LX/A5h;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->H:LX/A5h;

    .line 1621858
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->H:LX/A5h;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1621859
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->j:LX/8JL;

    invoke-virtual {v0}, LX/8JL;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->k:Ljava/util/List;

    .line 1621860
    return-void

    .line 1621861
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->z:LX/1Ck;

    move-object v0, v0

    .line 1621862
    const-string v1, "setup_tag_suggestions"

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->K:LX/0TD;

    new-instance v3, LX/A5f;

    invoke-direct {v3, p0}, LX/A5f;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/A5g;

    invoke-direct {v3, p0}, LX/A5g;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1621863
    goto :goto_0
.end method

.method public final d()Z
    .locals 15

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1621864
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->C:LX/A5T;

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->J:LX/A5m;

    invoke-virtual {v1}, LX/A5m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)Z

    move-result v2

    iget-boolean v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->y:Z

    iget-object v4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621865
    iget-object v5, v4, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    move-object v4, v5

    .line 1621866
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    move v4, v6

    :goto_0
    iget-object v5, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621867
    iget-object v8, v5, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v5, v8

    .line 1621868
    iget-object v14, v0, LX/A5T;->a:LX/0Zb;

    const-string v8, "friend_suggestions_cancel"

    move-object v9, v1

    move v10, v2

    move v11, v3

    move v12, v4

    move-object v13, v5

    invoke-static/range {v8 .. v13}, LX/A5T;->a(Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v14, v8}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1621869
    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->x:Z

    if-eqz v0, :cond_1

    .line 1621870
    invoke-static {p0, v7}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V

    .line 1621871
    :goto_1
    return v6

    :cond_0
    move v4, v7

    .line 1621872
    goto :goto_0

    :cond_1
    move v6, v7

    .line 1621873
    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1621874
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1621875
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1621876
    packed-switch p1, :pswitch_data_0

    .line 1621877
    :cond_0
    :goto_0
    return-void

    .line 1621878
    :pswitch_0
    const-string v0, "tag_place_after_tag_people"

    const/4 p1, 0x1

    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1621879
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 p1, -0x1

    invoke-virtual {v0, p1, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1621880
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1621881
    goto :goto_0

    .line 1621882
    :cond_1
    if-nez p2, :cond_0

    .line 1621883
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 1621884
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->x:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x57a2b6a3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1621885
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621886
    const-string v2, "CreateView"

    invoke-static {v0, v2}, LX/7kp;->a(LX/7kp;Ljava/lang/String;)V

    .line 1621887
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621888
    iget v2, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    move v0, v2

    .line 1621889
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621890
    iget v2, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    move v0, v2

    .line 1621891
    :goto_0
    iput v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->s:I

    .line 1621892
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->s:I

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1621893
    const v0, 0x7f0306f9

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    .line 1621894
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v2, 0x7f0d12a5

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1621895
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v2, LX/A5a;

    invoke-direct {v2, p0}, LX/A5a;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1621896
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621897
    iget-boolean v2, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    move v0, v2

    .line 1621898
    if-eqz v0, :cond_0

    .line 1621899
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v2, 0x7f0d12a8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1621900
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1621901
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    .line 1621902
    iput-object v2, v0, LX/8tB;->i:Ljava/util/List;

    .line 1621903
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->E:Landroid/content/ContentResolver;

    .line 1621904
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->E:Landroid/content/ContentResolver;

    sget-object v2, LX/0Oz;->FRIENDS_CONTENT:LX/0Oz;

    invoke-virtual {v2}, LX/0Oz;->getFullUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Z:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v5, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1621905
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v2, 0x7f0d12a3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->q:Landroid/view/View;

    .line 1621906
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v2, 0x7f0d12a4

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o:Landroid/widget/TextView;

    .line 1621907
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v2, 0x7f0d12a6

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->p:Landroid/view/View;

    .line 1621908
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1621909
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1621910
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1621911
    invoke-static {p0, v3}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V

    .line 1621912
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1621913
    iput-boolean v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->y:Z

    .line 1621914
    :goto_1
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->q:Landroid/view/View;

    new-instance v2, LX/A5c;

    invoke-direct {v2, p0}, LX/A5c;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1621915
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621916
    iget-boolean v2, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    move v0, v2

    .line 1621917
    if-eqz v0, :cond_4

    .line 1621918
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A5p;

    .line 1621919
    :goto_2
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    invoke-virtual {v2, v0}, LX/8tB;->a(LX/8RK;)V

    .line 1621920
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    .line 1621921
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1621922
    const/4 v2, 0x0

    :goto_3
    invoke-static {}, LX/A5i;->values()[LX/A5i;

    move-result-object v4

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 1621923
    new-instance v4, LX/623;

    invoke-direct {v4}, LX/623;-><init>()V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1621924
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1621925
    :cond_1
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8tB;->a(Ljava/util/List;)V

    .line 1621926
    const v2, 0x7f0d04af

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/listview/BetterListView;

    iput-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    .line 1621927
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1621928
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Y:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1621929
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/A5d;

    invoke-direct {v3, p0}, LX/A5d;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1621930
    invoke-virtual {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b()V

    .line 1621931
    invoke-static {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    .line 1621932
    invoke-static {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1621933
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->A:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "place_picker_place_to_people_start"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "from_checkin"

    iget-object v4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621934
    iget-boolean p1, v4, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    move v4, p1

    .line 1621935
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1621936
    :cond_2
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 1621937
    sget-short v2, LX/8mx;->a:S

    invoke-interface {v0, v2, v5}, LX/0ad;->a(SZ)Z

    .line 1621938
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621939
    const-string v2, "CreateView"

    invoke-static {v0, v2}, LX/7kp;->c(LX/7kp;Ljava/lang/String;)V

    .line 1621940
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->S:Landroid/view/ViewGroup;

    const v2, -0x55a3de38

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 1621941
    :cond_3
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->F:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f081396

    :goto_4
    move v0, v0

    .line 1621942
    goto/16 :goto_0

    .line 1621943
    :cond_4
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8RL;

    goto/16 :goto_2

    .line 1621944
    :cond_5
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_6
    const v0, 0x7f081395

    goto :goto_4
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x29dc1d22

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1621945
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->z:LX/1Ck;

    if-eqz v1, :cond_0

    .line 1621946
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->z:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1621947
    :cond_0
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->H:LX/A5h;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1621948
    iput-object v4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    .line 1621949
    iput-object v4, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    .line 1621950
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->E:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Z:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1621951
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1621952
    const/16 v1, 0x2b

    const v2, -0x514c9386

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1621953
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1621954
    const-string v0, "has_backed_out_of_place_picker"

    iget-boolean v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1621955
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xb8ceb59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1621956
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->J:LX/A5m;

    invoke-virtual {v1}, LX/A5m;->b()V

    .line 1621957
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1621958
    const/16 v1, 0x2b

    const v2, 0x496d9967

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
