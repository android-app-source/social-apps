.class public final Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public volatile p:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7kp;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

.field private r:LX/0Ot;
    .annotation runtime Lcom/facebook/tagging/conversion/annotation/TaggingConversionPhotoFlowLogger;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/107;

.field private t:Lcom/facebook/tagging/conversion/FriendSelectorConfig;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1621492
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1621493
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1621494
    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->r:LX/0Ot;

    .line 1621495
    new-instance v0, LX/A5U;

    invoke-direct {v0, p0}, LX/A5U;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;)V

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->s:LX/107;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1621557
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1621558
    const-string v1, "friend_selector_config"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1621559
    return-object v0
.end method

.method private static a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;",
            "LX/0Or",
            "<",
            "LX/7kp;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1621556
    iput-object p1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->r:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;

    const/16 v1, 0x1960

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2e05

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;LX/0Or;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1621555
    const-string v0, "friend_suggestions_and_selector"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1621513
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1621514
    invoke-static {p0, p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1621515
    const v0, 0x7f030729

    invoke-virtual {p0, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->setContentView(I)V

    .line 1621516
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1621517
    new-instance v1, LX/A5V;

    invoke-direct {v1, p0}, LX/A5V;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1621518
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 1621519
    iput v3, v1, LX/108;->a:I

    .line 1621520
    move-object v1, v1

    .line 1621521
    const v2, 0x7f081397

    invoke-virtual {p0, v2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1621522
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1621523
    move-object v1, v1

    .line 1621524
    iput-boolean v3, v1, LX/108;->q:Z

    .line 1621525
    move-object v1, v1

    .line 1621526
    const/4 v2, -0x2

    .line 1621527
    iput v2, v1, LX/108;->h:I

    .line 1621528
    move-object v1, v1

    .line 1621529
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1621530
    const v1, 0x7f081395

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1621531
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->s:LX/107;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1621532
    invoke-virtual {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "friend_selector_config"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    iput-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->t:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621533
    if-nez p1, :cond_1

    .line 1621534
    invoke-virtual {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1621535
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1621536
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1621537
    new-instance v3, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {v3}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;-><init>()V

    .line 1621538
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1621539
    move-object v1, v3

    .line 1621540
    iput-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    .line 1621541
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    iget-object v3, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    .line 1621542
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1621543
    :goto_0
    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    .line 1621544
    iput-object v0, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1621545
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->t:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621546
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1621547
    if-eqz v0, :cond_0

    .line 1621548
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73w;

    iget-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->t:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621549
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1621550
    invoke-virtual {v0, v1}, LX/73w;->a(Ljava/lang/String;)V

    .line 1621551
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kp;

    .line 1621552
    const-string v1, "FragmentInjection"

    invoke-static {v0, v1}, LX/7kp;->a(LX/7kp;Ljava/lang/String;)V

    .line 1621553
    return-void

    .line 1621554
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iput-object v1, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1621496
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1621497
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73w;

    const/4 v2, 0x0

    .line 1621498
    sget-object v1, LX/74R;->COMPOSER_TAGGING_CANCEL:LX/74R;

    invoke-static {v0, v1, v2, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1621499
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->t:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621500
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1621501
    if-eqz v0, :cond_1

    .line 1621502
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1621503
    const-string v1, "extra_place"

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->t:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621504
    iget-object v3, v2, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v2, v3

    .line 1621505
    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1621506
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 1621507
    invoke-virtual {p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->finish()V

    .line 1621508
    :goto_0
    return-void

    .line 1621509
    :cond_1
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    if-eqz v0, :cond_2

    .line 1621510
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->q:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1621511
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 1621512
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
