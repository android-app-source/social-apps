.class public Lcom/facebook/tagging/conversion/FriendSelectorConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tagging/conversion/FriendSelectorConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:I

.field public final c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/9jG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Z

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1621400
    new-instance v0, LX/A5R;

    invoke-direct {v0}, LX/A5R;-><init>()V

    sput-object v0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/A5S;)V
    .locals 1

    .prologue
    .line 1621304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621305
    iget-object v0, p1, LX/A5S;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1621306
    iget v0, p1, LX/A5S;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    .line 1621307
    iget-object v0, p1, LX/A5S;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 1621308
    iget-boolean v0, p1, LX/A5S;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    .line 1621309
    iget-boolean v0, p1, LX/A5S;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    .line 1621310
    iget-boolean v0, p1, LX/A5S;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    .line 1621311
    iget-boolean v0, p1, LX/A5S;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    .line 1621312
    iget-object v0, p1, LX/A5S;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1621313
    iget-object v0, p1, LX/A5S;->i:LX/9jG;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    .line 1621314
    iget-object v0, p1, LX/A5S;->j:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    .line 1621315
    iget-object v0, p1, LX/A5S;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    .line 1621316
    iget-boolean v0, p1, LX/A5S;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    .line 1621317
    iget-object v0, p1, LX/A5S;->m:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    .line 1621318
    iget-object v0, p1, LX/A5S;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1621319
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1621401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621402
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1621403
    iput-object v6, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1621404
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    .line 1621405
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1621406
    iput-object v6, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 1621407
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    .line 1621408
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    .line 1621409
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    .line 1621410
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    .line 1621411
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1621412
    iput-object v6, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1621413
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 1621414
    iput-object v6, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    .line 1621415
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/Long;

    move v0, v2

    .line 1621416
    :goto_8
    array-length v4, v3

    if-ge v0, v4, :cond_8

    .line 1621417
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1621418
    aput-object v4, v3, v0

    .line 1621419
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1621420
    :cond_0
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    goto :goto_0

    .line 1621421
    :cond_1
    sget-object v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1621422
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1621423
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1621424
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1621425
    goto :goto_5

    .line 1621426
    :cond_6
    sget-object v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    goto :goto_6

    .line 1621427
    :cond_7
    invoke-static {}, LX/9jG;->values()[LX/9jG;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    goto :goto_7

    .line 1621428
    :cond_8
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    .line 1621429
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 1621430
    iput-object v6, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    .line 1621431
    :goto_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    :goto_a
    iput-boolean v1, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    .line 1621432
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Long;

    .line 1621433
    :goto_b
    array-length v1, v0

    if-ge v2, v1, :cond_b

    .line 1621434
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1621435
    aput-object v1, v0, v2

    .line 1621436
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 1621437
    :cond_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    goto :goto_9

    :cond_a
    move v1, v2

    .line 1621438
    goto :goto_a

    .line 1621439
    :cond_b
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    .line 1621440
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    .line 1621441
    iput-object v6, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1621442
    :goto_c
    return-void

    .line 1621443
    :cond_c
    const-class v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    goto :goto_c
.end method

.method public static newBuilder()LX/A5S;
    .locals 2

    .prologue
    .line 1621398
    new-instance v0, LX/A5S;

    invoke-direct {v0}, LX/A5S;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1621399
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1621365
    if-ne p0, p1, :cond_1

    .line 1621366
    :cond_0
    :goto_0
    return v0

    .line 1621367
    :cond_1
    instance-of v2, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1621368
    goto :goto_0

    .line 1621369
    :cond_2
    check-cast p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621370
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1621371
    goto :goto_0

    .line 1621372
    :cond_3
    iget v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    iget v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1621373
    goto :goto_0

    .line 1621374
    :cond_4
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1621375
    goto :goto_0

    .line 1621376
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    iget-boolean v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1621377
    goto :goto_0

    .line 1621378
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    iget-boolean v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1621379
    goto :goto_0

    .line 1621380
    :cond_7
    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    iget-boolean v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1621381
    goto :goto_0

    .line 1621382
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    iget-boolean v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1621383
    goto :goto_0

    .line 1621384
    :cond_9
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1621385
    goto :goto_0

    .line 1621386
    :cond_a
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1621387
    goto :goto_0

    .line 1621388
    :cond_b
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 1621389
    goto :goto_0

    .line 1621390
    :cond_c
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1621391
    goto :goto_0

    .line 1621392
    :cond_d
    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    iget-boolean v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1621393
    goto :goto_0

    .line 1621394
    :cond_e
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 1621395
    goto/16 :goto_0

    .line 1621396
    :cond_f
    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iget-object v3, p1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1621397
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1621364
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1621320
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v0, :cond_0

    .line 1621321
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621322
    :goto_0
    iget v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621323
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    if-nez v0, :cond_1

    .line 1621324
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621325
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621326
    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->e:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621327
    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->f:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621328
    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->g:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621329
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_6

    .line 1621330
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621331
    :goto_6
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    if-nez v0, :cond_7

    .line 1621332
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621333
    :goto_7
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621334
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_8
    if-ge v3, v4, :cond_8

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1621335
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    .line 1621336
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    .line 1621337
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621338
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_0

    .line 1621339
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621340
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1621341
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1621342
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1621343
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1621344
    goto :goto_5

    .line 1621345
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621346
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_6

    .line 1621347
    :cond_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621348
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->i:LX/9jG;

    invoke-virtual {v0}, LX/9jG;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_7

    .line 1621349
    :cond_8
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1621350
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621351
    :goto_9
    iget-boolean v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->l:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621352
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621353
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_b
    if-ge v3, v4, :cond_b

    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1621354
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    .line 1621355
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    .line 1621356
    :cond_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621357
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_9

    :cond_a
    move v0, v2

    .line 1621358
    goto :goto_a

    .line 1621359
    :cond_b
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    if-nez v0, :cond_c

    .line 1621360
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621361
    :goto_c
    return-void

    .line 1621362
    :cond_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1621363
    iget-object v0, p0, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_c
.end method
