.class public final Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/8nF;

.field private final c:Ljava/lang/Boolean;

.field public final d:LX/8n5;

.field private final e:Landroid/content/Context;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/3Cq;

.field public final i:LX/8my;

.field public j:LX/8my;

.field public k:Z

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1400412
    const-class v0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/8n5;LX/8nF;Ljava/lang/Boolean;LX/3Cq;)V
    .locals 1
    .param p2    # LX/8n5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400400
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1400401
    invoke-static {}, LX/0RA;->d()Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->g:Ljava/util/Set;

    .line 1400402
    new-instance v0, LX/8mz;

    invoke-direct {v0, p0}, LX/8mz;-><init>(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;)V

    iput-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->i:LX/8my;

    .line 1400403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->k:Z

    .line 1400404
    iput-object p1, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->e:Landroid/content/Context;

    .line 1400405
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    .line 1400406
    iput-object p2, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    .line 1400407
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->l:Z

    .line 1400408
    iput-object p3, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->b:LX/8nF;

    .line 1400409
    iput-object p4, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c:Ljava/lang/Boolean;

    .line 1400410
    iput-object p5, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->h:LX/3Cq;

    .line 1400411
    return-void
.end method

.method private c(Lcom/facebook/tagging/model/TaggingProfile;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1400389
    iget-object v0, p1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1400390
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    .line 1400391
    if-nez v0, :cond_0

    .line 1400392
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->e:Landroid/content/Context;

    const v1, 0x7f081394

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1400393
    :cond_0
    iget-object v1, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1400394
    iget-boolean v1, p1, Lcom/facebook/tagging/model/TaggingProfile;->k:Z

    move v1, v1

    .line 1400395
    if-eqz v1, :cond_1

    .line 1400396
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1400397
    const/4 p0, 0x0

    move-object v0, p0

    .line 1400398
    invoke-static {v1, v0}, LX/47q;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v0

    .line 1400399
    :cond_1
    return-object v0
.end method

.method public static c(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Z
    .locals 1

    .prologue
    .line 1400388
    invoke-static {p0, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    return v0
.end method

.method public static d(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 1

    .prologue
    .line 1400387
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    return-object v0
.end method

.method private e(Lcom/facebook/tagging/model/TaggingProfile;)V
    .locals 2

    .prologue
    .line 1400380
    iget-object v1, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    monitor-enter v1

    .line 1400381
    :try_start_0
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400382
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400383
    iget-boolean v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->k:Z

    if-eqz v0, :cond_0

    .line 1400384
    const v0, -0x1bda862b

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1400385
    :cond_0
    return-void

    .line 1400386
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1400373
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1400374
    sget-object v1, LX/8n0;->a:[I

    invoke-static {}, LX/8n2;->values()[LX/8n2;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, LX/8n2;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1400375
    const v1, 0x7f031350

    :goto_0
    move v1, v1

    .line 1400376
    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1400377
    return-object v0

    .line 1400378
    :pswitch_0
    const v1, 0x7f03146a

    goto :goto_0

    .line 1400379
    :pswitch_1
    const v1, 0x7f031471

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 1400332
    invoke-static {}, LX/8n2;->values()[LX/8n2;

    move-result-object v0

    aget-object v0, v0, p4

    .line 1400333
    sget-object v1, LX/8n0;->a:[I

    invoke-virtual {v0}, LX/8n2;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1400334
    :cond_0
    :goto_0
    return-void

    .line 1400335
    :pswitch_0
    instance-of v0, p2, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-nez v0, :cond_0

    .line 1400336
    check-cast p2, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400337
    const v0, 0x7f0d1a16

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1400338
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1400339
    if-eqz v1, :cond_3

    .line 1400340
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v1, v1

    .line 1400341
    sget-object v2, LX/7Gr;->TEXT:LX/7Gr;

    if-eq v1, v2, :cond_3

    .line 1400342
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1400343
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1400344
    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1400345
    :cond_1
    :goto_1
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1400346
    if-nez v1, :cond_2

    .line 1400347
    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1400348
    :cond_2
    const v0, 0x7f0d1b9e

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1400349
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1400350
    invoke-direct {p0, p2}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c(Lcom/facebook/tagging/model/TaggingProfile;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1400351
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1400352
    const v1, 0x7f0d2cb2

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1400353
    iget-object v2, p2, Lcom/facebook/tagging/model/TaggingProfile;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1400354
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1400355
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1400356
    :goto_2
    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1400357
    const/high16 v2, 0x41400000    # 12.0f

    iget-object v3, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->ydpi:F

    const/high16 v4, 0x43200000    # 160.0f

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1400358
    invoke-virtual {v0, v5, v2, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1400359
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1400360
    :cond_3
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v1, v1

    .line 1400361
    sget-object v2, LX/7Gr;->TEXT:LX/7Gr;

    if-ne v1, v2, :cond_1

    .line 1400362
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1

    .line 1400363
    :cond_4
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1400364
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1400365
    :pswitch_1
    check-cast p2, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    .line 1400366
    const v0, 0x7f0d02c4

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1400367
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1400368
    const v0, 0x7f0d02c3

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1400369
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1400370
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1400371
    :cond_5
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1400372
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/8nB;Z)V
    .locals 1

    .prologue
    .line 1400330
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    invoke-virtual {v0, p1, p2}, LX/8n5;->a(LX/8nB;Z)V

    .line 1400331
    return-void
.end method

.method public final a(Ljava/util/Comparator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1400323
    iget-object v1, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    monitor-enter v1

    .line 1400324
    :try_start_0
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1400325
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400326
    iget-boolean v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->k:Z

    if-eqz v0, :cond_0

    .line 1400327
    const v0, -0x227abfd1

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1400328
    :cond_0
    return-void

    .line 1400329
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/tagging/model/TaggingProfile;)Z
    .locals 10

    .prologue
    .line 1400283
    iget-object v4, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->i:LX/8my;

    .line 1400284
    iget-wide v8, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v6, v8

    .line 1400285
    invoke-interface {v4, v6, v7}, LX/8my;->a(J)Z

    move-result v4

    move v0, v4

    .line 1400286
    if-eqz v0, :cond_0

    .line 1400287
    const/4 v0, 0x0

    .line 1400288
    :goto_0
    return v0

    .line 1400289
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->e(Lcom/facebook/tagging/model/TaggingProfile;)V

    .line 1400290
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->g:Ljava/util/Set;

    .line 1400291
    iget-wide v4, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v2, v4

    .line 1400292
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1400293
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Z)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;
    .locals 1

    .prologue
    .line 1400320
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    .line 1400321
    iput-boolean p1, v0, LX/8n5;->g:Z

    .line 1400322
    return-object p0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1400312
    iget-object v1, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    monitor-enter v1

    .line 1400313
    :try_start_0
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1400314
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400315
    iget-boolean v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->k:Z

    if-eqz v0, :cond_0

    .line 1400316
    const v0, 0x730b9f22

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1400317
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1400318
    return-void

    .line 1400319
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c(Z)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;
    .locals 1

    .prologue
    .line 1400309
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    .line 1400310
    iput-boolean p1, v0, LX/8n5;->i:Z

    .line 1400311
    return-object p0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1400308
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1400307
    iget-object v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1400306
    invoke-static {p0, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1400305
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1400298
    invoke-static {p0, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-eqz v0, :cond_0

    .line 1400299
    sget-object v0, LX/8n2;->TAG_EXPANSION_INFO:LX/8n2;

    .line 1400300
    :goto_0
    move-object v0, v0

    .line 1400301
    invoke-virtual {v0}, LX/8n2;->ordinal()I

    move-result v0

    return v0

    .line 1400302
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1400303
    sget-object v0, LX/8n2;->SECTION_HEADER_VIEW:LX/8n2;

    goto :goto_0

    .line 1400304
    :cond_1
    sget-object v0, LX/8n2;->ITEM_VIEW:LX/8n2;

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1400297
    invoke-static {}, LX/8n2;->values()[LX/8n2;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 1400294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->k:Z

    .line 1400295
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 1400296
    return-void
.end method
