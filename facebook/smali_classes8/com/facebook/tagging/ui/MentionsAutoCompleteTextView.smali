.class public Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;
.super Lcom/facebook/resources/ui/FbAutoCompleteTextView;
.source ""

# interfaces
.implements LX/8p2;


# instance fields
.field public b:LX/8oi;

.field private c:LX/8oa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/Dyh;

.field private e:LX/8p3;

.field public f:LX/3iT;

.field private g:LX/8n8;

.field private h:LX/8n8;

.field private i:LX/0if;

.field private j:LX/8ok;

.field private k:Z

.field private l:LX/8oz;

.field private m:I

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/7Gl;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0ad;

.field private p:LX/1zC;

.field public q:I

.field public r:I

.field private final s:LX/8ox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1405274
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1405275
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1405276
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1405277
    iput-boolean v2, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    .line 1405278
    sget-object v0, LX/8oz;->OTHER:LX/8oz;

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->l:LX/8oz;

    .line 1405279
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->n:Ljava/lang/ref/WeakReference;

    .line 1405280
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->q:I

    .line 1405281
    const v0, -0x272016

    iput v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->r:I

    .line 1405282
    new-instance v0, LX/8ox;

    invoke-direct {v0, p0}, LX/8ox;-><init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->s:LX/8ox;

    .line 1405283
    const-class v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-static {v0, p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1405284
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getInputType()I

    move-result v0

    const v1, -0x10001

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setInputType(I)V

    .line 1405285
    new-instance v0, LX/8p1;

    invoke-direct {v0, p0}, LX/8p1;-><init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setEditableFactory(Landroid/text/Editable$Factory;)V

    .line 1405286
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405287
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405288
    iget-object v1, v0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    move-object v0, v1

    .line 1405289
    invoke-virtual {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1405290
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405291
    iget v1, v0, LX/8n8;->i:I

    move v0, v1

    .line 1405292
    invoke-virtual {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setThreshold(I)V

    .line 1405293
    new-instance v0, LX/8ow;

    invoke-direct {v0, p0}, LX/8ow;-><init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1405294
    new-instance v0, LX/8ov;

    iget-object v1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->p:LX/1zC;

    invoke-direct {v0, p0, v1}, LX/8ov;-><init>(Landroid/widget/EditText;LX/1zC;)V

    invoke-virtual {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1405295
    return-void
.end method

.method private a(LX/8of;ILjava/lang/CharSequence;)LX/8n8;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1405265
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, LX/8of;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    goto :goto_0
.end method

.method private a()Landroid/content/ClipData;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1405296
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 1405297
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1405298
    :goto_0
    return-object v0

    .line 1405299
    :cond_0
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v2

    .line 1405300
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1405301
    invoke-static {v1, v3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    move-object v0, v2

    .line 1405302
    goto :goto_0
.end method

.method private a(LX/8p3;LX/3iT;LX/8ok;LX/0if;LX/8n9;LX/0ad;LX/1zC;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1405303
    iput-object p1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->e:LX/8p3;

    .line 1405304
    iput-object p2, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->f:LX/3iT;

    .line 1405305
    iput-object p3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->j:LX/8ok;

    .line 1405306
    iput-object p4, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->i:LX/0if;

    .line 1405307
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->s:LX/8ox;

    invoke-virtual {p5, v0}, LX/8n9;->a(LX/8ox;)LX/8n8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    .line 1405308
    iput-object p6, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->o:LX/0ad;

    .line 1405309
    iput-object p7, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->p:LX/1zC;

    .line 1405310
    return-void
.end method

.method private a(Landroid/content/ClipData;)V
    .locals 2

    .prologue
    .line 1405311
    if-nez p1, :cond_0

    .line 1405312
    :goto_0
    return-void

    .line 1405313
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 1405314
    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-static {v7}, LX/8p3;->b(LX/0QB;)LX/8p3;

    move-result-object v1

    check-cast v1, LX/8p3;

    invoke-static {v7}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v2

    check-cast v2, LX/3iT;

    new-instance v4, LX/8ok;

    invoke-static {v7}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {v4, v3}, LX/8ok;-><init>(LX/0Zb;)V

    move-object v3, v4

    check-cast v3, LX/8ok;

    invoke-static {v7}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    const-class v5, LX/8n9;

    invoke-interface {v7, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/8n9;

    invoke-static {v7}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v7}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v7

    check-cast v7, LX/1zC;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(LX/8p3;LX/3iT;LX/8ok;LX/0if;LX/8n9;LX/0ad;LX/1zC;)V

    return-void
.end method

.method public static getMentionsColorSpanProvider(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)LX/8oa;
    .locals 1

    .prologue
    .line 1405315
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->c:LX/8oa;

    if-nez v0, :cond_0

    .line 1405316
    new-instance v0, LX/8oy;

    invoke-direct {v0, p0}, LX/8oy;-><init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->c:LX/8oa;

    .line 1405317
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->c:LX/8oa;

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1405318
    iget v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->q:I

    if-ne v0, p1, :cond_1

    iget v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->r:I

    if-ne v0, p2, :cond_1

    .line 1405319
    :cond_0
    return-void

    .line 1405320
    :cond_1
    iput p1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->q:I

    .line 1405321
    iput p2, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->r:I

    .line 1405322
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, LX/8of;

    .line 1405323
    new-instance v2, LX/8ow;

    invoke-direct {v2, p0}, LX/8ow;-><init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    invoke-virtual {v2}, LX/8ow;->a()V

    .line 1405324
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->length()I

    move-result v2

    const-class v3, LX/7Gl;

    invoke-virtual {v0, v1, v2, v3}, LX/8of;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 1405325
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Gl;

    .line 1405326
    iget v5, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->q:I

    iget v6, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->r:I

    .line 1405327
    invoke-virtual {v1, v0}, LX/7Gl;->a(Landroid/text/Editable;)I

    move-result v7

    .line 1405328
    invoke-virtual {v1, v0}, LX/7Gl;->b(Landroid/text/Editable;)I

    move-result v8

    .line 1405329
    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result p1

    .line 1405330
    new-instance p2, LX/7Gl;

    invoke-direct {p2, v5, v6, v1}, LX/7Gl;-><init>(IILX/7Gl;)V

    .line 1405331
    invoke-interface {v0, v1}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1405332
    invoke-interface {v0, p2, v7, v8, p1}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1405333
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public final a(LX/8nB;)V
    .locals 2

    .prologue
    .line 1405334
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    .line 1405335
    iget-object v1, v0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    const/4 p0, 0x1

    invoke-virtual {v1, p1, p0}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(LX/8nB;Z)V

    .line 1405336
    return-void
.end method

.method public final a(Lcom/facebook/tagging/model/TaggingProfile;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1405337
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, LX/8of;

    .line 1405338
    invoke-virtual {v0}, LX/8of;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 1405339
    invoke-virtual {v0, v3, v3, p1}, LX/8of;->a(IILcom/facebook/tagging/model/TaggingProfile;)LX/8of;

    .line 1405340
    invoke-virtual {v0}, LX/8of;->length()I

    move-result v1

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, LX/8of;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1405341
    invoke-virtual {v0}, LX/8of;->length()I

    move-result v1

    const-class v2, LX/7Gl;

    invoke-virtual {v0, v3, v1, v2}, LX/8of;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1405342
    aget-object v1, v0, v3

    invoke-virtual {v1}, LX/7Gl;->d()I

    move-result v1

    iput v1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->m:I

    .line 1405343
    new-instance v1, Ljava/lang/ref/WeakReference;

    aget-object v0, v0, v3

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->n:Ljava/lang/ref/WeakReference;

    .line 1405344
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->j:LX/8ok;

    .line 1405345
    const-string v1, "reply_to_mention_clicked"

    invoke-static {v0, v1}, LX/8ok;->a(LX/8ok;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    .line 1405346
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1405347
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1405348
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Long;LX/7Gm;)V
    .locals 6
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1405115
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    .line 1405116
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1405117
    iget-object v1, v0, LX/8n8;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1405118
    :cond_0
    :goto_0
    move v1, v2

    .line 1405119
    if-eqz v1, :cond_1

    .line 1405120
    iget-object v1, v0, LX/8n8;->f:LX/8nX;

    invoke-virtual {v1, p1}, LX/8nX;->a(Ljava/lang/Long;)LX/8nW;

    move-result-object v1

    iput-object v1, v0, LX/8n8;->u:LX/8nW;

    .line 1405121
    iget-object v1, v0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-object v2, v0, LX/8n8;->u:LX/8nW;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(LX/8nB;Z)V

    .line 1405122
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8n8;->s:Z

    .line 1405123
    :cond_1
    iput-object p1, v0, LX/8n8;->q:Ljava/lang/Long;

    .line 1405124
    iput-object p2, v0, LX/8n8;->r:LX/7Gm;

    .line 1405125
    return-void

    .line 1405126
    :cond_2
    sget-object v1, LX/7Gm;->COMPOSER:LX/7Gm;

    invoke-virtual {v1, p2}, LX/7Gm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/8n8;->g:LX/0ad;

    sget-short v4, LX/8mx;->c:S

    invoke-interface {v1, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 1405127
    :goto_1
    sget-object v4, LX/7Gm;->COMMENT:LX/7Gm;

    invoke-virtual {v4, p2}, LX/7Gm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v0, LX/8n8;->g:LX/0ad;

    sget-short v5, LX/8mx;->b:S

    invoke-interface {v4, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v2

    .line 1405128
    :goto_2
    if-nez v1, :cond_0

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :cond_3
    move v1, v3

    .line 1405129
    goto :goto_1

    :cond_4
    move v4, v3

    .line 1405130
    goto :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1405349
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Gl;

    .line 1405350
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/7Gl;->d()I

    move-result v1

    if-lez v1, :cond_1

    .line 1405351
    iget v1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->m:I

    invoke-virtual {v0}, LX/7Gl;->d()I

    move-result v0

    if-le v1, v0, :cond_2

    const/4 v0, 0x1

    .line 1405352
    :goto_0
    iget-object v1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->j:LX/8ok;

    .line 1405353
    const-string v2, "reply_to_mention_submit"

    invoke-static {v1, v2}, LX/8ok;->a(LX/8ok;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    .line 1405354
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1405355
    const-string v3, "shortened"

    invoke-virtual {v2, v3, v0}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1405356
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 1405357
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 1405358
    :cond_1
    return-void

    .line 1405359
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1405360
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405361
    check-cast p1, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1405362
    iget-object p0, p1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object p0, p0

    .line 1405363
    if-nez p0, :cond_0

    .line 1405364
    const/4 p0, 0x0

    .line 1405365
    :goto_0
    move-object v0, p0

    .line 1405366
    return-object v0

    .line 1405367
    :cond_0
    iput-object p1, v0, LX/8n8;->p:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1405368
    invoke-virtual {p1}, Lcom/facebook/tagging/model/TaggingProfile;->j()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public final dismissDropDown()V
    .locals 7

    .prologue
    .line 1405266
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->isPopupShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1405267
    :goto_0
    return-void

    .line 1405268
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405269
    iget-object v1, v0, LX/8n8;->e:LX/8ol;

    const-string v2, ""

    iget-object v3, v0, LX/8n8;->q:Ljava/lang/Long;

    iget-object v4, v0, LX/8n8;->r:LX/7Gm;

    .line 1405270
    iget-object v5, v1, LX/8ol;->a:LX/0Zb;

    const-string v6, "session_end"

    invoke-static {v1, v6}, LX/8ol;->a(LX/8ol;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v0, "session_end_reason"

    invoke-virtual {v6, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v0, "group_id"

    invoke-virtual {v6, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v0, "surface"

    invoke-virtual {v6, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1405271
    const-string v5, ""

    iput-object v5, v1, LX/8ol;->b:Ljava/lang/String;

    .line 1405272
    invoke-super {p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->dismissDropDown()V

    .line 1405273
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->i:LX/0if;

    sget-object v1, LX/0ig;->m:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    goto :goto_0
.end method

.method public getEncodedText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1405112
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/8oj;->a(Landroid/text/Editable;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMentionsEntityRanges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1405113
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/8oj;->a(Landroid/text/Editable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1405114
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x62ca9771

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1405131
    invoke-super {p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onDetachedFromWindow()V

    .line 1405132
    const/16 v1, 0x2d

    const v2, -0x7d23746a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 5
    .param p3    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x2c

    const v3, -0x493e684b

    invoke-static {v4, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1405133
    iget-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->d:LX/Dyh;

    if-nez v3, :cond_0

    .line 1405134
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1405135
    const/16 v0, 0x2d

    const v1, -0x491d8f68

    invoke-static {v4, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1405136
    :goto_0
    return-void

    .line 1405137
    :cond_0
    if-eqz p1, :cond_3

    iget-boolean v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    if-nez v3, :cond_3

    .line 1405138
    iget-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->d:LX/Dyh;

    invoke-virtual {v3}, LX/Dyh;->a()V

    .line 1405139
    iget-boolean v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    if-nez v3, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    .line 1405140
    :cond_1
    :goto_2
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1405141
    const v0, 0x44d1c216

    invoke-static {v0, v2}, LX/02F;->g(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1405142
    goto :goto_1

    .line 1405143
    :cond_3
    if-nez p1, :cond_1

    iget-boolean v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    if-eqz v3, :cond_1

    .line 1405144
    iget-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->d:LX/Dyh;

    invoke-virtual {v3}, LX/Dyh;->b()V

    .line 1405145
    iget-boolean v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    if-nez v3, :cond_4

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1405146
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->d:LX/Dyh;

    if-nez v0, :cond_0

    .line 1405147
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1405148
    :goto_0
    return v0

    .line 1405149
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    if-eqz v0, :cond_1

    .line 1405150
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->d:LX/Dyh;

    invoke-virtual {v0}, LX/Dyh;->b()V

    .line 1405151
    iget-boolean v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->k:Z

    .line 1405152
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->clearFocus()V

    .line 1405153
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1405154
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onTextContextMenuItem(I)Z
    .locals 2

    .prologue
    .line 1405155
    const v0, 0x1020022

    if-ne p1, v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1405156
    invoke-direct {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a()Landroid/content/ClipData;

    move-result-object v1

    .line 1405157
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onTextContextMenuItem(I)Z

    move-result v0

    .line 1405158
    invoke-direct {p0, v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Landroid/content/ClipData;)V

    .line 1405159
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onTextContextMenuItem(I)Z

    move-result v0

    goto :goto_0
.end method

.method public performFiltering(Ljava/lang/CharSequence;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1405160
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getSelectionStart()I

    move-result v2

    .line 1405161
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v2, v0, :cond_1

    .line 1405162
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    invoke-virtual {v0}, LX/8n8;->b()V

    .line 1405163
    :cond_0
    :goto_0
    return-void

    .line 1405164
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, LX/8of;

    .line 1405165
    iget-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->o:LX/0ad;

    sget-short v4, LX/8mx;->f:S

    invoke-interface {v3, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 1405166
    if-nez v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    const/4 v4, 0x0

    .line 1405167
    if-eqz v1, :cond_a

    .line 1405168
    add-int/lit8 v3, v2, -0x1

    :goto_1
    if-ltz v3, :cond_9

    .line 1405169
    invoke-interface {v0, v3}, Landroid/text/Editable;->charAt(I)C

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 1405170
    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 1405171
    :sswitch_0
    add-int/lit8 v5, v3, 0x1

    if-ge v5, v2, :cond_3

    .line 1405172
    add-int/lit8 v3, v3, 0x1

    .line 1405173
    :cond_4
    :goto_2
    :sswitch_1
    move v1, v3

    .line 1405174
    invoke-static {v0, v1, v2}, LX/8p3;->a(LX/8of;II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1405175
    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(LX/8of;ILjava/lang/CharSequence;)LX/8n8;

    move-result-object v3

    .line 1405176
    if-nez v3, :cond_6

    .line 1405177
    invoke-virtual {v0, v1}, LX/8of;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 1405178
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->showDropDown()V

    goto :goto_0

    .line 1405179
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->dismissDropDown()V

    .line 1405180
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    invoke-virtual {v0}, LX/8n8;->b()V

    goto :goto_0

    .line 1405181
    :cond_6
    iput-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405182
    iget-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405183
    iget-object v4, v3, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    move-object v3, v4

    .line 1405184
    invoke-virtual {p0, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1405185
    iget-object v3, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    const/4 p1, 0x0

    const/4 v6, 0x1

    .line 1405186
    const-string v4, ""

    iput-object v4, v3, LX/8n8;->o:Ljava/lang/String;

    .line 1405187
    iget-object v4, v3, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-boolean v5, v3, LX/8n8;->n:Z

    .line 1405188
    iget-object p2, v4, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    .line 1405189
    iput-boolean v5, p2, LX/8n5;->f:Z

    .line 1405190
    move-object v4, v4

    .line 1405191
    invoke-virtual {v4, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->b(Z)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c(Z)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1405192
    invoke-virtual {v0, v1}, LX/8of;->charAt(I)C

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_8

    .line 1405193
    iget-object v4, v3, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-virtual {v4, v6}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c(Z)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1405194
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    iget v5, v3, LX/8n8;->h:I

    if-lt v4, v5, :cond_7

    .line 1405195
    iget-object v4, v3, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-virtual {v4, v6}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->b(Z)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1405196
    :cond_7
    const-string v4, "@"

    iput-object v4, v3, LX/8n8;->o:Ljava/lang/String;

    .line 1405197
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v3, LX/8n8;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/8n8;->o:Ljava/lang/String;

    .line 1405198
    iput-boolean v6, v3, LX/8n8;->t:Z

    .line 1405199
    iget-object v4, v3, LX/8n8;->j:LX/8n5;

    iget-object v5, v3, LX/8n8;->o:Ljava/lang/String;

    .line 1405200
    iput-object v2, v4, LX/8n5;->j:Ljava/lang/CharSequence;

    .line 1405201
    iput-object v5, v4, LX/8n5;->k:Ljava/lang/String;

    .line 1405202
    iget-object v4, v3, LX/8n8;->j:LX/8n5;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, LX/8n5;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    .line 1405203
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->l:LX/8oz;

    sget-object v1, LX/8oz;->UPDATE:LX/8oz;

    if-eq v0, v1, :cond_0

    .line 1405204
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->i:LX/0if;

    sget-object v1, LX/0ig;->m:LX/0ih;

    const-string v2, "mention_list_update"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1405205
    sget-object v0, LX/8oz;->UPDATE:LX/8oz;

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->l:LX/8oz;

    goto/16 :goto_0

    :cond_9
    move v3, v4

    .line 1405206
    goto/16 :goto_2

    .line 1405207
    :cond_a
    const/16 p1, 0x14

    .line 1405208
    add-int/lit8 p2, v2, -0x1

    move v3, v4

    move v5, v4

    :goto_3
    if-ltz p2, :cond_4

    .line 1405209
    invoke-interface {v0, p2}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    sparse-switch v6, :sswitch_data_1

    move v6, p1

    .line 1405210
    :cond_b
    add-int/lit8 p2, p2, -0x1

    move p1, v6

    goto :goto_3

    :sswitch_2
    move v3, p2

    .line 1405211
    goto/16 :goto_2

    .line 1405212
    :sswitch_3
    if-nez v5, :cond_c

    .line 1405213
    const/4 v5, 0x1

    .line 1405214
    add-int/lit8 v3, p2, 0x1

    if-ge v3, v2, :cond_d

    add-int/lit8 v3, p2, 0x1

    .line 1405215
    :cond_c
    :goto_4
    add-int/lit8 v6, p1, -0x1

    if-gtz p1, :cond_b

    .line 1405216
    :goto_5
    add-int/lit8 p1, p2, 0x1

    if-ge p1, v2, :cond_b

    .line 1405217
    if-nez v3, :cond_4

    add-int/lit8 v3, p2, 0x1

    goto/16 :goto_2

    :cond_d
    move v3, v4

    .line 1405218
    goto :goto_4

    :sswitch_4
    move v6, p1

    goto :goto_5

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x20 -> :sswitch_0
        0x40 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_4
        0x20 -> :sswitch_3
        0x40 -> :sswitch_2
    .end sparse-switch
.end method

.method public final replaceText(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 1405219
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->i:LX/0if;

    sget-object v1, LX/0ig;->m:LX/0ih;

    const-string v2, "insert_mentions"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1405220
    sget-object v0, LX/8oz;->OTHER:LX/8oz;

    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->l:LX/8oz;

    .line 1405221
    if-nez p1, :cond_0

    .line 1405222
    :goto_0
    return-void

    .line 1405223
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, LX/8of;

    .line 1405224
    if-nez v0, :cond_1

    .line 1405225
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not editable text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1405226
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getSelectionStart()I

    move-result v1

    .line 1405227
    iget-object v2, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405228
    iget-object p0, v2, LX/8n8;->p:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1405229
    iget-object v3, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object p0, v3

    .line 1405230
    invoke-static {v0, v1, p0}, LX/8p3;->a(LX/8of;ILcom/facebook/user/model/Name;)I

    move-result v3

    .line 1405231
    iget-object p0, v2, LX/8n8;->c:LX/8p3;

    invoke-virtual {p0, v0, v1, v3, p1}, LX/8p3;->a(LX/8of;IILjava/lang/CharSequence;)Z

    move-result p0

    .line 1405232
    if-nez p0, :cond_2

    .line 1405233
    :goto_1
    goto :goto_0

    .line 1405234
    :cond_2
    iget-object p0, v2, LX/8n8;->p:Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {v0, v3, v1, p0}, LX/8of;->a(IILcom/facebook/tagging/model/TaggingProfile;)LX/8of;

    .line 1405235
    invoke-static {v2}, LX/8n8;->g(LX/8n8;)V

    .line 1405236
    iget-object v3, v2, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-virtual {v3}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->b()V

    goto :goto_1
.end method

.method public setIncludeFriends(Z)V
    .locals 1

    .prologue
    .line 1405237
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    .line 1405238
    iput-boolean p1, v0, LX/8n8;->n:Z

    .line 1405239
    return-void
.end method

.method public setMentionChangeListener(LX/8oi;)V
    .locals 0

    .prologue
    .line 1405240
    iput-object p1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b:LX/8oi;

    .line 1405241
    return-void
.end method

.method public setOnSoftKeyboardVisibleListener(LX/Dyh;)V
    .locals 0

    .prologue
    .line 1405242
    iput-object p1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->d:LX/Dyh;

    .line 1405243
    return-void
.end method

.method public setShouldShowTagExpansionInfo(Z)V
    .locals 1

    .prologue
    .line 1405244
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    .line 1405245
    iget-object p0, v0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1405246
    iput-boolean p1, p0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->l:Z

    .line 1405247
    return-void
.end method

.method public setTagTypeaheadDataSourceMetadata(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)V
    .locals 2

    .prologue
    .line 1405248
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->g:LX/8n8;

    .line 1405249
    iget-object v1, v0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1405250
    if-eqz p1, :cond_0

    .line 1405251
    iget-object p0, v1, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->b:LX/8nF;

    invoke-interface {p0, p1}, LX/8nF;->a(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)LX/8nB;

    move-result-object p0

    .line 1405252
    if-eqz p0, :cond_0

    .line 1405253
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(LX/8nB;Z)V

    .line 1405254
    :cond_0
    return-void
.end method

.method public final showDropDown()V
    .locals 7

    .prologue
    .line 1405255
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1405256
    :goto_0
    return-void

    .line 1405257
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->isPopupShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1405258
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->h:LX/8n8;

    .line 1405259
    iget-object v1, v0, LX/8n8;->e:LX/8ol;

    iget-object v2, v0, LX/8n8;->o:Ljava/lang/String;

    iget-object v3, v0, LX/8n8;->q:Ljava/lang/Long;

    iget-object v4, v0, LX/8n8;->r:LX/7Gm;

    .line 1405260
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, LX/8ol;->b:Ljava/lang/String;

    .line 1405261
    iget-object v5, v1, LX/8ol;->a:LX/0Zb;

    const-string v6, "session_start"

    invoke-static {v1, v6}, LX/8ol;->a(LX/8ol;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v0, "selected_input_query"

    invoke-virtual {v6, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v0, "group_id"

    invoke-virtual {v6, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v0, "surface"

    invoke-virtual {v6, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1405262
    :cond_1
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->i:LX/0if;

    sget-object v1, LX/0ig;->m:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1405263
    invoke-super {p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->showDropDown()V

    .line 1405264
    iget-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->i:LX/0if;

    sget-object v1, LX/0ig;->m:LX/0ih;

    const-string v2, "old_ui"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method
