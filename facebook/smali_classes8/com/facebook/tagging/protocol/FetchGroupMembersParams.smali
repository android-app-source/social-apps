.class public Lcom/facebook/tagging/protocol/FetchGroupMembersParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tagging/protocol/FetchGroupMembersParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1405010
    new-instance v0, LX/8ot;

    invoke-direct {v0}, LX/8ot;-><init>()V

    sput-object v0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;I)V
    .locals 1

    .prologue
    .line 1405005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405006
    iput-wide p1, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->a:J

    .line 1405007
    iput-object p3, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->b:Ljava/lang/String;

    .line 1405008
    iput p4, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->c:I

    .line 1405009
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1405000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405001
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->a:J

    .line 1405002
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->b:Ljava/lang/String;

    .line 1405003
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->c:I

    .line 1405004
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1404995
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1404996
    iget-wide v0, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1404997
    iget-object v0, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1404998
    iget v0, p0, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1404999
    return-void
.end method
