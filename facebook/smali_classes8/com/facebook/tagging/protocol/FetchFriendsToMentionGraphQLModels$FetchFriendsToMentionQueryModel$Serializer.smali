.class public final Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1404775
    const-class v0, Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel;

    new-instance v1, Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1404776
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1404777
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1404778
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1404779
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1404780
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1404781
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1404782
    if-eqz v2, :cond_0

    .line 1404783
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1404784
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1404785
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1404786
    if-eqz v2, :cond_1

    .line 1404787
    const-string p0, "friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1404788
    invoke-static {v1, v2, p1, p2}, LX/8op;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1404789
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1404790
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1404791
    check-cast p1, Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel$Serializer;->a(Lcom/facebook/tagging/protocol/FetchFriendsToMentionGraphQLModels$FetchFriendsToMentionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
