.class public final Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/8nN;


# direct methods
.method public constructor <init>(LX/8nN;LX/0Px;)V
    .locals 0

    .prologue
    .line 1400936
    iput-object p1, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->b:LX/8nN;

    iput-object p2, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1400937
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1400938
    iget-object v0, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->b:LX/8nN;

    iget-object v0, v0, LX/8nN;->c:LX/8nP;

    iget-object v0, v0, LX/8nP;->e:LX/87w;

    iget-object v1, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->a:LX/0Px;

    iget-object v2, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->b:LX/8nN;

    iget-object v2, v2, LX/8nN;->a:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/87w;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v10

    .line 1400939
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v11, :cond_1

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/facebook/goodfriends/data/FriendData;

    .line 1400940
    invoke-virtual {v4}, Lcom/facebook/goodfriends/data/FriendData;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400941
    iget-object v0, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->b:LX/8nN;

    iget-object v0, v0, LX/8nN;->c:LX/8nP;

    iget-object v0, v0, LX/8nP;->d:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    .line 1400942
    iget-object v2, v4, Lcom/facebook/goodfriends/data/FriendData;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1400943
    invoke-direct {v1, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1400944
    iget-object v2, v4, Lcom/facebook/goodfriends/data/FriendData;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1400945
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1400946
    iget-object v5, v4, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    move-object v4, v5

    .line 1400947
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/7Gr;->USER:LX/7Gr;

    const-string v6, "goodfriends_fetcher"

    sget-object v7, LX/8nE;->GOOD_FRIENDS:LX/8nE;

    invoke-virtual {v7}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    .line 1400948
    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1400949
    :cond_0
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 1400950
    :cond_1
    iget-object v0, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->b:LX/8nN;

    iget-object v0, v0, LX/8nN;->b:LX/8JX;

    iget-object v1, p0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$1$1;->b:LX/8nN;

    iget-object v1, v1, LX/8nN;->a:Ljava/lang/CharSequence;

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1400951
    return-void
.end method
