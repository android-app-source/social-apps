.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x481284fc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402559
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402560
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1402561
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1402562
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1402563
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1402564
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1402565
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1402566
    if-eqz v0, :cond_0

    .line 1402567
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x6

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1402568
    :cond_0
    return-void

    .line 1402569
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1402570
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1402571
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1402572
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1402573
    if-eqz v0, :cond_0

    .line 1402574
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1402575
    :cond_0
    return-void
.end method

.method private m()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1402519
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->g:Ljava/util/List;

    .line 1402520
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private n()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402576
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    .line 1402577
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402578
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1402579
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402580
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1402581
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1402541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402542
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1402543
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1402544
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->m()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1402545
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->n()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1402546
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1402547
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1402548
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1402549
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1402550
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1402551
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1402552
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1402553
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1402554
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1402555
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1402556
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1402557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402558
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1402582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402583
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->n()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1402584
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->n()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    .line 1402585
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->n()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1402586
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;

    .line 1402587
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    .line 1402588
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1402589
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1402590
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1402591
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;

    .line 1402592
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1402593
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402594
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1402540
    new-instance v0, LX/8ny;

    invoke-direct {v0, p1}, LX/8ny;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402539
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1402529
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1402530
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1402531
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1402532
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1402533
    :goto_0
    return-void

    .line 1402534
    :cond_0
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1402535
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1402536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1402537
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1402538
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1402524
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1402525
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->a(Ljava/lang/String;)V

    .line 1402526
    :cond_0
    :goto_0
    return-void

    .line 1402527
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1402528
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1402521
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;-><init>()V

    .line 1402522
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1402523
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1402518
    const v0, -0x4b3237f0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1402517
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402515
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1402516
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402513
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1402514
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402511
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1402512
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
