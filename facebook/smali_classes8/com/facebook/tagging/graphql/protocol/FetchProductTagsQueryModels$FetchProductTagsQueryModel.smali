.class public final Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x303a910c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403358
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403357
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1403355
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1403356
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1403349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403350
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1403351
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1403352
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1403353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403354
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1403332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403333
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1403334
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    .line 1403335
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1403336
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;

    .line 1403337
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    .line 1403338
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403339
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1403348
    new-instance v0, LX/8oC;

    invoke-direct {v0, p1}, LX/8oC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403359
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    .line 1403360
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1403346
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1403347
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1403345
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1403342
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;-><init>()V

    .line 1403343
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1403344
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1403341
    const v0, -0x54ab17a7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1403340
    const v0, 0x25d6af

    return v0
.end method
