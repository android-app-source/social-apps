.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3954169a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1401504
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1401503
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1401501
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1401502
    return-void
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401498
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1401499
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1401500
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1401483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1401484
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->p()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1401485
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1401486
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1401487
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1401488
    const/4 v4, 0x7

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1401489
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1401490
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1401491
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1401492
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1401493
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1401494
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1401495
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1401496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1401497
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1401452
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1401453
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1401454
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1401455
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1401456
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    .line 1401457
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1401458
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1401459
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401482
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1401477
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1401478
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->g:Z

    .line 1401479
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->h:Z

    .line 1401480
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->i:Z

    .line 1401481
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1401474
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;-><init>()V

    .line 1401475
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1401476
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1401473
    const v0, -0x5c930d8d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1401472
    const v0, 0x4c7ec322    # 6.6784392E7f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401470
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1401471
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1401468
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1401469
    iget-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->g:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1401466
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1401467
    iget-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->h:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1401464
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1401465
    iget-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->i:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401462
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1401463
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401460
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1401461
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
