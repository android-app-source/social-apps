.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1402661
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;

    new-instance v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1402662
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1402663
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1402664
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1402665
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1402666
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1402667
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1402668
    if-eqz v2, :cond_0

    .line 1402669
    const-string p0, "groups"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1402670
    invoke-static {v1, v2, p1, p2}, LX/8o5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1402671
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1402672
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1402673
    check-cast p1, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$Serializer;->a(Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
