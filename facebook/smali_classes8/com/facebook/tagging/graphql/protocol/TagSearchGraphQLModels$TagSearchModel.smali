.class public final Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x20c982f5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1404160
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1404159
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1404157
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1404158
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1404151
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1404152
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1404153
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1404154
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1404155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1404156
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1404143
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1404144
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1404145
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    .line 1404146
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1404147
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;

    .line 1404148
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    .line 1404149
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1404150
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1404136
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    .line 1404137
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1404140
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;-><init>()V

    .line 1404141
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1404142
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1404139
    const v0, 0x33aac2d4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1404138
    const v0, -0x289a23da

    return v0
.end method
