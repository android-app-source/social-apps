.class public final Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1402894
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1402895
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1402892
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1402893
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1402883
    if-nez p1, :cond_0

    .line 1402884
    :goto_0
    return v0

    .line 1402885
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1402886
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1402887
    :pswitch_0
    const-class v1, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1402888
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1402889
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1402890
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1402891
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1fd444e4
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1402882
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1402878
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1402879
    if-eqz v0, :cond_0

    .line 1402880
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1402881
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1402873
    packed-switch p2, :pswitch_data_0

    .line 1402874
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1402875
    :pswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1402876
    invoke-static {v0, p3}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1402877
    return-void

    :pswitch_data_0
    .packed-switch -0x1fd444e4
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1402872
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1402870
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1402871
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1402839
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1402840
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1402841
    :cond_0
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1402842
    iput p2, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->b:I

    .line 1402843
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1402869
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1402868
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1402865
    iget v0, p0, LX/1vt;->c:I

    .line 1402866
    move v0, v0

    .line 1402867
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1402862
    iget v0, p0, LX/1vt;->c:I

    .line 1402863
    move v0, v0

    .line 1402864
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1402859
    iget v0, p0, LX/1vt;->b:I

    .line 1402860
    move v0, v0

    .line 1402861
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402856
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1402857
    move-object v0, v0

    .line 1402858
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1402847
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1402848
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1402849
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1402850
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1402851
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1402852
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1402853
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1402854
    invoke-static {v3, v9, v2}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1402855
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1402844
    iget v0, p0, LX/1vt;->c:I

    .line 1402845
    move v0, v0

    .line 1402846
    return v0
.end method
