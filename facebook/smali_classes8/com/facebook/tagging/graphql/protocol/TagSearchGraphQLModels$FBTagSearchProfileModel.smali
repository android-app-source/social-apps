.class public final Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x51ea4db5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1404050
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1404049
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1404047
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1404048
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1404033
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1404034
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1404035
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1404036
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1404037
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x6b33c81d

    invoke-static {v4, v3, v5}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1404038
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x7b6fecc2

    invoke-static {v5, v4, v6}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1404039
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1404040
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1404041
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1404042
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1404043
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1404044
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1404045
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1404046
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1404017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1404018
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1404019
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6b33c81d

    invoke-static {v2, v0, v3}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1404020
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1404021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;

    .line 1404022
    iput v3, v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->h:I

    move-object v1, v0

    .line 1404023
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1404024
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7b6fecc2

    invoke-static {v2, v0, v3}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1404025
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1404026
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;

    .line 1404027
    iput v3, v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->i:I

    move-object v1, v0

    .line 1404028
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1404029
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1404030
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1404031
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1404032
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1404016
    new-instance v0, LX/8oQ;

    invoke-direct {v0, p1}, LX/8oQ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1404015
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1404006
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1404007
    const/4 v0, 0x3

    const v1, 0x6b33c81d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->h:I

    .line 1404008
    const/4 v0, 0x4

    const v1, -0x7b6fecc2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->i:I

    .line 1404009
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1404013
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1404014
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1404051
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1404010
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;-><init>()V

    .line 1404011
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1404012
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1404005
    const v0, -0x1fb84904

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1404004
    const v0, 0x50c72189

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1404001
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1404002
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1404003
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403999
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->f:Ljava/lang/String;

    .line 1404000
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403997
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->g:Ljava/lang/String;

    .line 1403998
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403995
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1403996
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSocialContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403993
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1403994
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
