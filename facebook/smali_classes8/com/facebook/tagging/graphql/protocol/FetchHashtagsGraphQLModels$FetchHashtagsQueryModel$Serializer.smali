.class public final Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1402962
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel;

    new-instance v1, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1402963
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1402961
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1402943
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1402944
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1402945
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1402946
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1402947
    if-eqz v2, :cond_2

    .line 1402948
    const-string p0, "suggested_hashtags"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1402949
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1402950
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1402951
    if-eqz p0, :cond_1

    .line 1402952
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1402953
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1402954
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1402955
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1}, LX/8o8;->a(LX/15i;ILX/0nX;)V

    .line 1402956
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1402957
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1402958
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1402959
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1402960
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1402942
    check-cast p1, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$Serializer;->a(Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
