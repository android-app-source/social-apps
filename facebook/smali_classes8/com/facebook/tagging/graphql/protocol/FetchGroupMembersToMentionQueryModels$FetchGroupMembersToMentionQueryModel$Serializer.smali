.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1401681
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    new-instance v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1401682
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1401680
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1401620
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1401621
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x3

    .line 1401622
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1401623
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1401624
    if-eqz v2, :cond_8

    .line 1401625
    const-string v3, "group_mentions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401626
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1401627
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1401628
    if-eqz v3, :cond_3

    .line 1401629
    const-string v5, "member_section"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401630
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1401631
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1401632
    if-eqz v5, :cond_2

    .line 1401633
    const-string v6, "edges"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401634
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1401635
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v1, v5}, LX/15i;->c(I)I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 1401636
    invoke-virtual {v1, v5, v6}, LX/15i;->q(II)I

    move-result v7

    .line 1401637
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1401638
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1401639
    if-eqz p0, :cond_0

    .line 1401640
    const-string v3, "node"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401641
    invoke-static {v1, p0, p1, p2}, LX/8nj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1401642
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1401643
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1401644
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1401645
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1401646
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1401647
    if-eqz v3, :cond_7

    .line 1401648
    const-string v5, "non_member_section"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401649
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1401650
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1401651
    if-eqz v5, :cond_6

    .line 1401652
    const-string v6, "edges"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401653
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1401654
    const/4 v6, 0x0

    :goto_1
    invoke-virtual {v1, v5}, LX/15i;->c(I)I

    move-result v7

    if-ge v6, v7, :cond_5

    .line 1401655
    invoke-virtual {v1, v5, v6}, LX/15i;->q(II)I

    move-result v7

    .line 1401656
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1401657
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1401658
    if-eqz p0, :cond_4

    .line 1401659
    const-string v3, "node"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401660
    invoke-static {v1, p0, p1, p2}, LX/8nk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1401661
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1401662
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1401663
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1401664
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1401665
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1401666
    :cond_8
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1401667
    if-eqz v2, :cond_9

    .line 1401668
    const-string v3, "is_multi_company_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401669
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1401670
    :cond_9
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1401671
    if-eqz v2, :cond_a

    .line 1401672
    const-string v3, "parent_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401673
    invoke-static {v1, v2, p1}, LX/8nm;->a(LX/15i;ILX/0nX;)V

    .line 1401674
    :cond_a
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1401675
    if-eqz v2, :cond_b

    .line 1401676
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1401677
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1401678
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1401679
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1401619
    check-cast p1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Serializer;->a(Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
