.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x33e82e18
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402152
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402151
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1402112
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1402113
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1402141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402142
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1402143
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1402144
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x64dfb792

    invoke-static {v3, v2, v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1402145
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1402146
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1402147
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1402148
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1402149
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402150
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1402131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402132
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1402133
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x64dfb792

    invoke-static {v2, v0, v3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1402134
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1402135
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;

    .line 1402136
    iput v3, v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->g:I

    .line 1402137
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402138
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1402139
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1402140
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1402130
    new-instance v0, LX/8nq;

    invoke-direct {v0, p1}, LX/8nq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402129
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1402126
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1402127
    const/4 v0, 0x2

    const v1, -0x64dfb792

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->g:I

    .line 1402128
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1402153
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1402154
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1402125
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1402122
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;-><init>()V

    .line 1402123
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1402124
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1402121
    const v0, -0x55206913

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1402120
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402118
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->e:Ljava/lang/String;

    .line 1402119
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402116
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->f:Ljava/lang/String;

    .line 1402117
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402114
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1402115
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
