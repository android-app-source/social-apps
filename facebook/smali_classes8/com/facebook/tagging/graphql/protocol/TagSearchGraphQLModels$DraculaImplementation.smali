.class public final Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1403600
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1403601
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1403598
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1403599
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1403574
    if-nez p1, :cond_0

    .line 1403575
    :goto_0
    return v0

    .line 1403576
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1403577
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1403578
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1403579
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1403580
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1403581
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1403582
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1403583
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1403584
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1403585
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1403586
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1403587
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1403588
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1403589
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1403590
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1403591
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1403592
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1403593
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1403594
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1403595
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1403596
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1403597
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7b6fecc2 -> :sswitch_3
        0x19625476 -> :sswitch_0
        0x4742d950 -> :sswitch_1
        0x6b33c81d -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1403573
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1403570
    sparse-switch p0, :sswitch_data_0

    .line 1403571
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1403572
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7b6fecc2 -> :sswitch_0
        0x19625476 -> :sswitch_0
        0x4742d950 -> :sswitch_0
        0x6b33c81d -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1403569
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1403536
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->b(I)V

    .line 1403537
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1403564
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1403565
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1403566
    :cond_0
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1403567
    iput p2, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->b:I

    .line 1403568
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1403563
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1403562
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1403559
    iget v0, p0, LX/1vt;->c:I

    .line 1403560
    move v0, v0

    .line 1403561
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1403556
    iget v0, p0, LX/1vt;->c:I

    .line 1403557
    move v0, v0

    .line 1403558
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1403553
    iget v0, p0, LX/1vt;->b:I

    .line 1403554
    move v0, v0

    .line 1403555
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403550
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1403551
    move-object v0, v0

    .line 1403552
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1403541
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1403542
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1403543
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1403544
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1403545
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1403546
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1403547
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1403548
    invoke-static {v3, v9, v2}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1403549
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1403538
    iget v0, p0, LX/1vt;->c:I

    .line 1403539
    move v0, v0

    .line 1403540
    return v0
.end method
