.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3b46c359
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402244
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402243
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1402241
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1402242
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1402235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402236
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1402237
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1402238
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1402239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402240
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1402227
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402228
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1402229
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    .line 1402230
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1402231
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;

    .line 1402232
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    .line 1402233
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402234
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1402216
    new-instance v0, LX/8nr;

    invoke-direct {v0, p1}, LX/8nr;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402225
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    .line 1402226
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->e:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1402223
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1402224
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1402222
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1402219
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;-><init>()V

    .line 1402220
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1402221
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1402218
    const v0, 0x1eeed874

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1402217
    const v0, 0x41e065f

    return v0
.end method
