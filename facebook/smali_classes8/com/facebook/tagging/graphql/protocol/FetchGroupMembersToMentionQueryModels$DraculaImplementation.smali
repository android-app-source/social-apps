.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1401394
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1401395
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1401392
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1401393
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1401360
    if-nez p1, :cond_0

    move v0, v1

    .line 1401361
    :goto_0
    return v0

    .line 1401362
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1401363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1401364
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1401365
    const v2, 0x586db928

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1401366
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1401367
    const v3, -0x2536a5f2

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1401368
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1401369
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1401370
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1401371
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1401372
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1401373
    const v2, 0x11b2952b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1401374
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1401375
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1401376
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1401377
    :sswitch_2
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    .line 1401378
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1401379
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1401380
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1401381
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1401382
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1401383
    const v2, -0x2f722704

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1401384
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1401385
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1401386
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1401387
    :sswitch_4
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    .line 1401388
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1401389
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1401390
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1401391
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5341fafd -> :sswitch_0
        -0x2f722704 -> :sswitch_4
        -0x2536a5f2 -> :sswitch_3
        0x11b2952b -> :sswitch_2
        0x586db928 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1401359
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1401354
    if-eqz p0, :cond_0

    .line 1401355
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1401356
    if-eq v0, p0, :cond_0

    .line 1401357
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1401358
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1401339
    sparse-switch p2, :sswitch_data_0

    .line 1401340
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1401341
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1401342
    const v1, 0x586db928

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1401343
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1401344
    const v1, -0x2536a5f2

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1401345
    :goto_0
    return-void

    .line 1401346
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1401347
    const v1, 0x11b2952b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1401348
    :sswitch_2
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    .line 1401349
    invoke-static {v0, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1401350
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1401351
    const v1, -0x2f722704

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1401352
    :sswitch_4
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    .line 1401353
    invoke-static {v0, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5341fafd -> :sswitch_0
        -0x2f722704 -> :sswitch_4
        -0x2536a5f2 -> :sswitch_3
        0x11b2952b -> :sswitch_2
        0x586db928 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1401329
    if-nez p1, :cond_0

    move v0, v1

    .line 1401330
    :goto_0
    return v0

    .line 1401331
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1401332
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1401333
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1401334
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1401335
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1401336
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1401337
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1401338
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1401322
    if-eqz p1, :cond_0

    .line 1401323
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1401324
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1401325
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1401326
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1401327
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1401328
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1401316
    if-eqz p1, :cond_0

    .line 1401317
    invoke-static {p0, p1, p2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1401318
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    .line 1401319
    if-eq v0, v1, :cond_0

    .line 1401320
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1401321
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1401282
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1401314
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1401315
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1401309
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1401310
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1401311
    :cond_0
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1401312
    iput p2, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->b:I

    .line 1401313
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1401308
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1401307
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1401304
    iget v0, p0, LX/1vt;->c:I

    .line 1401305
    move v0, v0

    .line 1401306
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1401301
    iget v0, p0, LX/1vt;->c:I

    .line 1401302
    move v0, v0

    .line 1401303
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1401298
    iget v0, p0, LX/1vt;->b:I

    .line 1401299
    move v0, v0

    .line 1401300
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401295
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1401296
    move-object v0, v0

    .line 1401297
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1401286
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1401287
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1401288
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1401289
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1401290
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1401291
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1401292
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1401293
    invoke-static {v3, v9, v2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1401294
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1401283
    iget v0, p0, LX/1vt;->c:I

    .line 1401284
    move v0, v0

    .line 1401285
    return v0
.end method
