.class public final Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x18c79d13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402997
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403002
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1403000
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1403001
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402998
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->e:Ljava/lang/String;

    .line 1402999
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402984
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->f:Ljava/lang/String;

    .line 1402985
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402995
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->g:Ljava/lang/String;

    .line 1402996
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1403003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403004
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1403005
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1403006
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1403007
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1403008
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1403009
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1403010
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1403011
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403012
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1402992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402994
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402991
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1402988
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchHashtagsGraphQLModels$FetchHashtagsQueryModel$SuggestedHashtagsModel$NodesModel;-><init>()V

    .line 1402989
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1402990
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1402987
    const v0, 0xd919ce3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1402986
    const v0, -0x7333ac54

    return v0
.end method
