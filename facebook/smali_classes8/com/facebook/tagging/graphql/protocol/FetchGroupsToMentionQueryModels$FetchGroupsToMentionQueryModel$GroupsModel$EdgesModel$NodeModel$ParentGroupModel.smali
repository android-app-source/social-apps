.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x104a4aa6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402503
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1402502
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1402500
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1402501
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1402498
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->e:Ljava/lang/String;

    .line 1402499
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1402492
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->e:Ljava/lang/String;

    .line 1402493
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1402494
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1402495
    if-eqz v0, :cond_0

    .line 1402496
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1402497
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1402486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402487
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1402488
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1402489
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1402490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402491
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1402468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1402469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1402470
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1402485
    new-instance v0, LX/8nz;

    invoke-direct {v0, p1}, LX/8nz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1402479
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1402480
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1402481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1402482
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1402483
    :goto_0
    return-void

    .line 1402484
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1402476
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1402477
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;->a(Ljava/lang/String;)V

    .line 1402478
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1402473
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel$ParentGroupModel;-><init>()V

    .line 1402474
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1402475
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1402472
    const v0, 0x370f2750

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1402471
    const v0, 0x41e065f

    return v0
.end method
