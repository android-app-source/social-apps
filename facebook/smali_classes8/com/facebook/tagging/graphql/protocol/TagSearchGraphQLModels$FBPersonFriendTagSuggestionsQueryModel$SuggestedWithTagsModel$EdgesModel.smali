.class public final Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x265eaa93
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403899
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403898
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1403896
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1403897
    return-void
.end method

.method private j()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403894
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    .line 1403895
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1403888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403889
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->j()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1403890
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1403891
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1403892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403893
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1403880
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403881
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->j()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1403882
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->j()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    .line 1403883
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->j()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1403884
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;

    .line 1403885
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->e:Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    .line 1403886
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403887
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403879
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->j()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1403876
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;-><init>()V

    .line 1403877
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1403878
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1403874
    const v0, 0x7d444e45

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1403875
    const v0, 0x630d02ff

    return v0
.end method
