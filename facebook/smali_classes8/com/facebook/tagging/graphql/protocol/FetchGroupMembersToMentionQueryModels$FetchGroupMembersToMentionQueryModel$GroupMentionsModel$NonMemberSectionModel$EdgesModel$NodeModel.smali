.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1f6adc43
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1401569
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1401568
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1401566
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1401567
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401563
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1401564
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1401565
    :cond_0
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1401550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1401551
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1401552
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1401553
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1401554
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1401555
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1401556
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1401557
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1401558
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1401559
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1401560
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1401561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1401562
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1401542
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1401543
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1401544
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1401545
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1401546
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    .line 1401547
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1401548
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1401549
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401525
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1401539
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1401540
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->g:Z

    .line 1401541
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1401536
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;-><init>()V

    .line 1401537
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1401538
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1401535
    const v0, -0x4877306f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1401534
    const v0, 0x4c7ec322    # 6.6784392E7f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401532
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1401533
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1401530
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1401531
    iget-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->g:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401528
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 1401529
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401526
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1401527
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
