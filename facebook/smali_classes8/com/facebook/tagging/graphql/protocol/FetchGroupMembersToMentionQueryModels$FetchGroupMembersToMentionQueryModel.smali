.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52ca5173
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1401746
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1401745
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1401743
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1401744
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1401736
    iput-object p1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1401737
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1401738
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1401739
    if-eqz v0, :cond_0

    .line 1401740
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1401741
    :cond_0
    return-void

    .line 1401742
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1401725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1401726
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x5341fafd

    invoke-static {v1, v0, v2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1401727
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1401728
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1401729
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1401730
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1401731
    const/4 v0, 0x1

    iget-boolean v3, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->f:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1401732
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1401733
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1401734
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1401735
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1401710
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1401711
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1401712
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5341fafd

    invoke-static {v2, v0, v3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1401713
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1401714
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    .line 1401715
    iput v3, v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->e:I

    move-object v1, v0

    .line 1401716
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1401717
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    .line 1401718
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1401719
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    .line 1401720
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->g:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    .line 1401721
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1401722
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1401723
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1401724
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupMentions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1401708
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1401709
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1401683
    new-instance v0, LX/8ng;

    invoke-direct {v0, p1}, LX/8ng;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1401704
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1401705
    const/4 v0, 0x0

    const v1, -0x5341fafd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->e:I

    .line 1401706
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->f:Z

    .line 1401707
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1401698
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1401699
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1401700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1401701
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 1401702
    :goto_0
    return-void

    .line 1401703
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1401695
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1401696
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1401697
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1401692
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;-><init>()V

    .line 1401693
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1401694
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1401691
    const v0, 0x66d93b99

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1401690
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1401688
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1401689
    iget-boolean v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->f:Z

    return v0
.end method

.method public final k()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401686
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->g:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->g:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    .line 1401687
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->g:Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1401684
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1401685
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
