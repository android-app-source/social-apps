.class public Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1403507
    new-instance v0, LX/8oJ;

    invoke-direct {v0}, LX/8oJ;-><init>()V

    sput-object v0, Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1403508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1403509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;->a:Ljava/lang/String;

    .line 1403510
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;->b:Ljava/lang/String;

    .line 1403511
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1403506
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1403503
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1403504
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/GraphQLTagSearchParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1403505
    return-void
.end method
