.class public final Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1402105
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;

    new-instance v1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1402106
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1402107
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1402108
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1402109
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/8nt;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1402110
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1402111
    check-cast p1, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel$Serializer;->a(Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;LX/0nX;LX/0my;)V

    return-void
.end method
