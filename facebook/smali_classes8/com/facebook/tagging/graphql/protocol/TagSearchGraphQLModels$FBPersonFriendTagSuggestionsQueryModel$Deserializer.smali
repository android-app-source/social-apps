.class public final Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1403602
    const-class v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1403603
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1403604
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1403605
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1403606
    const/4 v2, 0x0

    .line 1403607
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1403608
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1403609
    :goto_0
    move v1, v2

    .line 1403610
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1403611
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1403612
    new-instance v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;

    invoke-direct {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;-><init>()V

    .line 1403613
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1403614
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1403615
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1403616
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1403617
    :cond_0
    return-object v1

    .line 1403618
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1403619
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_5

    .line 1403620
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1403621
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1403622
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 1403623
    const-string p0, "suggested_with_groups"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1403624
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1403625
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_3

    .line 1403626
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_3

    .line 1403627
    invoke-static {p1, v0}, LX/8oT;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1403628
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1403629
    :cond_3
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1403630
    goto :goto_1

    .line 1403631
    :cond_4
    const-string p0, "suggested_with_tags"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1403632
    invoke-static {p1, v0}, LX/8oW;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1403633
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1403634
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1403635
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1403636
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
