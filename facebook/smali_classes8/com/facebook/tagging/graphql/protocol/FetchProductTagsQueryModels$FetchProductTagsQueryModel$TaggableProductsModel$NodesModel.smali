.class public final Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x115a76b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403298
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403259
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1403296
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1403297
    return-void
.end method

.method private m()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403294
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    .line 1403295
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1403282
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403283
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1403284
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1403285
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1403286
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->m()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1403287
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1403288
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1403289
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1403290
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1403291
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1403292
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1403269
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403270
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1403271
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    .line 1403272
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1403273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;

    .line 1403274
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->f:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    .line 1403275
    :cond_0
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->m()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1403276
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->m()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    .line 1403277
    invoke-direct {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->m()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1403278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;

    .line 1403279
    iput-object v0, v1, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->h:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ProductItemPriceModel;

    .line 1403280
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403281
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403299
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1403266
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;-><init>()V

    .line 1403267
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1403268
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1403265
    const v0, 0x6ce9ed9a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1403264
    const v0, 0xa7c5482

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403262
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->e:Ljava/lang/String;

    .line 1403263
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403260
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->f:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->f:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    .line 1403261
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->f:Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403257
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->g:Ljava/lang/String;

    .line 1403258
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method
