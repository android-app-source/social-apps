.class public final Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403202
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1403203
    const-class v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1403204
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1403205
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1403191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403192
    invoke-virtual {p0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1403193
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1403194
    iget v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1403195
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1403196
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1403197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403198
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1403199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1403200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1403201
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1403189
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->f:Ljava/lang/String;

    .line 1403190
    iget-object v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1403185
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1403186
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->e:I

    .line 1403187
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->g:I

    .line 1403188
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1403182
    new-instance v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;-><init>()V

    .line 1403183
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1403184
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1403181
    const v0, 0x3756e6d4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1403180
    const v0, 0x437b93b

    return v0
.end method
