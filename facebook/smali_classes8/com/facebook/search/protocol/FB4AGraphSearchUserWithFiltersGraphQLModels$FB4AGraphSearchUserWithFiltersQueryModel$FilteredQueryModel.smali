.class public final Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x13da01cd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1597901
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1597900
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1597898
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1597899
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597896
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->e:Ljava/lang/String;

    .line 1597897
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1597863
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1597864
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1597865
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1597866
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1597867
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1597868
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1597869
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1597870
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1597871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1597872
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1597883
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1597884
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1597885
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1597886
    if-eqz v1, :cond_2

    .line 1597887
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    .line 1597888
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1597889
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1597890
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    .line 1597891
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1597892
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    .line 1597893
    iput-object v0, v1, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->g:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    .line 1597894
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1597895
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597882
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1597879
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;-><init>()V

    .line 1597880
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1597881
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1597878
    const v0, -0x280b3aa0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1597877
    const v0, -0x1bce060e

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1597875
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->f:Ljava/util/List;

    .line 1597876
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597873
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->g:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->g:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    .line 1597874
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel;->g:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel;

    return-object v0
.end method
