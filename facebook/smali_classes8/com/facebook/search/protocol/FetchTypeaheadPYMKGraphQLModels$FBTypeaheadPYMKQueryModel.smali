.class public final Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xde96732
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604317
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604316
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1604314
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1604315
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1604293
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604294
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1604295
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1604296
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1604297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604298
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1604306
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604307
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1604308
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    .line 1604309
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1604310
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;

    .line 1604311
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->e:Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    .line 1604312
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604313
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604304
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->e:Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->e:Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    .line 1604305
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->e:Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1604301
    new-instance v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;-><init>()V

    .line 1604302
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1604303
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1604300
    const v0, 0x5d4de205

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1604299
    const v0, -0x6747e1ce

    return v0
.end method
