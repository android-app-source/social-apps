.class public final Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604587
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604586
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1604584
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1604585
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604582
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;->e:Ljava/lang/String;

    .line 1604583
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1604588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604589
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1604590
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1604591
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1604592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604593
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1604579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604581
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1604576
    new-instance v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;-><init>()V

    .line 1604577
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1604578
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1604575
    const v0, -0x245053b5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1604574
    const v0, -0x6518607a

    return v0
.end method
