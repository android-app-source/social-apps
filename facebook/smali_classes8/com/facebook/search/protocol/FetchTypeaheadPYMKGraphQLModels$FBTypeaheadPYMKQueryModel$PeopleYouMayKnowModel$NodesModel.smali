.class public final Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d8c6f9b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604232
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604245
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1604243
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1604244
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1604233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604234
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1604235
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1604236
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1604237
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1604238
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1604239
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1604240
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1604241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604242
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1604224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604225
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1604226
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604227
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1604228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;

    .line 1604229
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604230
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604231
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1604223
    new-instance v0, LX/A0E;

    invoke-direct {v0, p1}, LX/A0E;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604222
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1604246
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1604247
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1604221
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1604218
    new-instance v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;-><init>()V

    .line 1604219
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1604220
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1604217
    const v0, 0x4044bfd9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1604210
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604215
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->e:Ljava/lang/String;

    .line 1604216
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604213
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->f:Ljava/lang/String;

    .line 1604214
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604211
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604212
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
