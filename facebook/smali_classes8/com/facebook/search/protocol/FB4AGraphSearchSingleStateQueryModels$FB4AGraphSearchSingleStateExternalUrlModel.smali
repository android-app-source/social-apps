.class public final Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4d39b5cd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1596297
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1596296
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1596294
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1596295
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1596292
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->e:Ljava/lang/String;

    .line 1596293
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1596290
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1596291
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1596298
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1596299
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1596300
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x7ef3d48c

    invoke-static {v2, v1, v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1596301
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1596302
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1596303
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1596304
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1596305
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1596280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1596281
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1596282
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7ef3d48c

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1596283
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1596284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;

    .line 1596285
    iput v3, v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->f:I

    .line 1596286
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1596287
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1596288
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1596289
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1596277
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1596278
    const/4 v0, 0x1

    const v1, 0x7ef3d48c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;->f:I

    .line 1596279
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1596274
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;-><init>()V

    .line 1596275
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1596276
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1596273
    const v0, -0xa3ee3f1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1596272
    const v0, 0x1eaef984

    return v0
.end method
