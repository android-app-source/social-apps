.class public final Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1605815
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1605816
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1605813
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1605814
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    .line 1605774
    if-nez p1, :cond_0

    .line 1605775
    const/4 v0, 0x0

    .line 1605776
    :goto_0
    return v0

    .line 1605777
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1605778
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1605779
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessSuggestionFieldsFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1605780
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1605781
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1605782
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1605783
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1605784
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1605785
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1605786
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1605787
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605788
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1605789
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1605790
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1605791
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1605792
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1605793
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1605794
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1605795
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605796
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1605797
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1605798
    const/4 v7, 0x7

    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1605799
    invoke-virtual {p3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605800
    const/16 v8, 0x8

    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    move-result-object v8

    .line 1605801
    invoke-virtual {p3, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1605802
    const/16 v9, 0x9

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1605803
    const/4 v9, 0x0

    invoke-virtual {p3, v9, v0}, LX/186;->b(II)V

    .line 1605804
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1605805
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1605806
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1605807
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1605808
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1605809
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1605810
    const/4 v0, 0x7

    invoke-virtual {p3, v0, v7}, LX/186;->b(II)V

    .line 1605811
    const/16 v0, 0x8

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 1605812
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1d2de6c4 -> :sswitch_0
        0x2d4f589d -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1605732
    if-nez p0, :cond_0

    move v0, v1

    .line 1605733
    :goto_0
    return v0

    .line 1605734
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1605735
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1605736
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1605737
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1605738
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1605739
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1605740
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1605767
    const/4 v7, 0x0

    .line 1605768
    const/4 v1, 0x0

    .line 1605769
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1605770
    invoke-static {v2, v3, v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1605771
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1605772
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1605773
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1605766
    new-instance v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1605762
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1605763
    if-eqz v0, :cond_0

    .line 1605764
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1605765
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1605757
    sparse-switch p2, :sswitch_data_0

    .line 1605758
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1605759
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessSuggestionFieldsFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1605760
    invoke-static {v0, p3}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1605761
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x1d2de6c4 -> :sswitch_0
        0x2d4f589d -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1605756
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1605817
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1605818
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1605716
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1605717
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1605718
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a:LX/15i;

    .line 1605719
    iput p2, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->b:I

    .line 1605720
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1605721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1605722
    new-instance v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1605723
    iget v0, p0, LX/1vt;->c:I

    .line 1605724
    move v0, v0

    .line 1605725
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1605726
    iget v0, p0, LX/1vt;->c:I

    .line 1605727
    move v0, v0

    .line 1605728
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1605729
    iget v0, p0, LX/1vt;->b:I

    .line 1605730
    move v0, v0

    .line 1605731
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605741
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1605742
    move-object v0, v0

    .line 1605743
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1605744
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1605745
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1605746
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1605747
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1605748
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1605749
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1605750
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1605751
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1605752
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1605753
    iget v0, p0, LX/1vt;->c:I

    .line 1605754
    move v0, v0

    .line 1605755
    return v0
.end method
