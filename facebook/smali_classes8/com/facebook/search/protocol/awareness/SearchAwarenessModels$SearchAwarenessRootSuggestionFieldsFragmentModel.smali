.class public final Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/A0Z;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2fc5564f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1605980
    const-class v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1605981
    const-class v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1605978
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1605979
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1605960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1605961
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x1d2de6c4

    invoke-static {v1, v0, v2}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1605962
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605963
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1605964
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1605965
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1605966
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->fQ_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605967
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1605968
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1605969
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1605970
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1605971
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1605972
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1605973
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1605974
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1605975
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1605976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1605977
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1605950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1605951
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1605952
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1d2de6c4

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1605953
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1605954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    .line 1605955
    iput v3, v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e:I

    .line 1605956
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1605957
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1605958
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1605959
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605949
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1605946
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1605947
    const/4 v0, 0x0

    const v1, -0x1d2de6c4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e:I

    .line 1605948
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1605943
    new-instance v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;-><init>()V

    .line 1605944
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1605945
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605982
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->f:Ljava/lang/String;

    .line 1605983
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1605941
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->g:Ljava/util/List;

    .line 1605942
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605939
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->h:Ljava/lang/String;

    .line 1605940
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1605938
    const v0, -0xa4c100b

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605936
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 1605937
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1605935
    const v0, -0x6ec8414f

    return v0
.end method

.method public final fQ_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605933
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j:Ljava/lang/String;

    .line 1605934
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getChainedUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1605929
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1605930
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605931
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k:Ljava/lang/String;

    .line 1605932
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method
