.class public final Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6c0b1535
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Z

.field private g:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1605900
    const-class v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1605899
    const-class v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1605897
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1605898
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1605895
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1605896
    iget v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1605887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1605888
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1605889
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1605890
    iget v1, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1605891
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->f:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1605892
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1605893
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1605894
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1605901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1605902
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1605903
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    .line 1605904
    invoke-virtual {p0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1605905
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    .line 1605906
    iput-object v0, v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->g:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    .line 1605907
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1605908
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1605883
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1605884
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->e:I

    .line 1605885
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->f:Z

    .line 1605886
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1605880
    new-instance v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;-><init>()V

    .line 1605881
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1605882
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1605879
    const v0, 0x1319cf3f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1605878
    const v0, 0x6ac2086

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1605876
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1605877
    iget-boolean v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->f:Z

    return v0
.end method

.method public final k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1605874
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->g:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->g:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    .line 1605875
    iget-object v0, p0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->g:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    return-object v0
.end method
