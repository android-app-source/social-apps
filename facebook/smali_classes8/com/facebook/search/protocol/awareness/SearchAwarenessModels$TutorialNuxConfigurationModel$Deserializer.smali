.class public final Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1606196
    const-class v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    new-instance v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1606197
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1606233
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1606198
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1606199
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1606200
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1606201
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1606202
    :goto_0
    move v1, v2

    .line 1606203
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1606204
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1606205
    new-instance v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-direct {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;-><init>()V

    .line 1606206
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1606207
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1606208
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1606209
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1606210
    :cond_0
    return-object v1

    .line 1606211
    :cond_1
    const-string p0, "cool_down_hours"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1606212
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    move v8, v5

    move v5, v3

    .line 1606213
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_6

    .line 1606214
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1606215
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1606216
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 1606217
    const-string p0, "carousel"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1606218
    invoke-static {p1, v0}, LX/A0h;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1606219
    :cond_3
    const-string p0, "enabled"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1606220
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v3

    goto :goto_1

    .line 1606221
    :cond_4
    const-string p0, "has_impression_cap_reached"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1606222
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 1606223
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1606224
    :cond_6
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1606225
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1606226
    if-eqz v5, :cond_7

    .line 1606227
    invoke-virtual {v0, v3, v8, v2}, LX/186;->a(III)V

    .line 1606228
    :cond_7
    if-eqz v4, :cond_8

    .line 1606229
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->a(IZ)V

    .line 1606230
    :cond_8
    if-eqz v1, :cond_9

    .line 1606231
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->a(IZ)V

    .line 1606232
    :cond_9
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto :goto_1
.end method
