.class public final Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1606234
    const-class v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    new-instance v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1606235
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1606236
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1606237
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1606238
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1606239
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1606240
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1606241
    if-eqz v2, :cond_0

    .line 1606242
    const-string v3, "carousel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1606243
    invoke-static {v1, v2, p1, p2}, LX/A0h;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1606244
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1606245
    if-eqz v2, :cond_1

    .line 1606246
    const-string v3, "cool_down_hours"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1606247
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1606248
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1606249
    if-eqz v2, :cond_2

    .line 1606250
    const-string v3, "enabled"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1606251
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1606252
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1606253
    if-eqz v2, :cond_3

    .line 1606254
    const-string v3, "has_impression_cap_reached"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1606255
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1606256
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1606257
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1606258
    check-cast p1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel$Serializer;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;LX/0nX;LX/0my;)V

    return-void
.end method
