.class public final Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1602914
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1602915
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1602912
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1602913
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1602896
    if-nez p1, :cond_0

    .line 1602897
    :goto_0
    return v0

    .line 1602898
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1602899
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1602900
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1602901
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1602902
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1602903
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 1602904
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1602905
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1602906
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1602907
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1602908
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1602909
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 1602910
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1602911
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x684af6a4
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1602895
    new-instance v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1602892
    packed-switch p0, :pswitch_data_0

    .line 1602893
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1602894
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x684af6a4
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1602891
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1602889
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->b(I)V

    .line 1602890
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1602916
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1602917
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1602918
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1602919
    iput p2, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->b:I

    .line 1602920
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1602888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1602887
    new-instance v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1602863
    iget v0, p0, LX/1vt;->c:I

    .line 1602864
    move v0, v0

    .line 1602865
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1602884
    iget v0, p0, LX/1vt;->c:I

    .line 1602885
    move v0, v0

    .line 1602886
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1602881
    iget v0, p0, LX/1vt;->b:I

    .line 1602882
    move v0, v0

    .line 1602883
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1602878
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1602879
    move-object v0, v0

    .line 1602880
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1602869
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1602870
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1602871
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1602872
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1602873
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1602874
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1602875
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1602876
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1602877
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1602866
    iget v0, p0, LX/1vt;->c:I

    .line 1602867
    move v0, v0

    .line 1602868
    return v0
.end method
