.class public final Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x335910bc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1597581
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1597580
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1597578
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1597579
    return-void
.end method

.method private j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597576
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->h:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->h:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    .line 1597577
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->h:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1597544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1597545
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1597546
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597547
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1597548
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1597549
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 1597550
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1597551
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1597552
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1597553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1597554
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1597568
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1597569
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1597570
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    .line 1597571
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1597572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;

    .line 1597573
    iput-object v0, v1, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->h:Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    .line 1597574
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1597575
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1597565
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1597566
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->e:Z

    .line 1597567
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1597582
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1597583
    iget-boolean v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1597562
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;-><init>()V

    .line 1597563
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1597564
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597560
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->f:Ljava/lang/String;

    .line 1597561
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597558
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->g:Ljava/lang/String;

    .line 1597559
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597557
    invoke-direct {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->j()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1597556
    const v0, -0xd126941

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1597555
    const v0, -0x7f325c79

    return v0
.end method
