.class public final Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1596224
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1596225
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1596149
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1596150
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1596197
    if-nez p1, :cond_0

    .line 1596198
    :goto_0
    return v0

    .line 1596199
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1596200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1596201
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1596202
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1596203
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1596204
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1596205
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1596206
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1596207
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1596208
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1596209
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1596210
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1596211
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1596212
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1596213
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1596214
    const v3, 0x1ef992d6

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1596215
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1596216
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1596217
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1596218
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1596219
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1596220
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1596221
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1596222
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1596223
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1ef992d6 -> :sswitch_3
        0x5834b395 -> :sswitch_2
        0x6f659f40 -> :sswitch_1
        0x7ef3d48c -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1596196
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1596191
    sparse-switch p2, :sswitch_data_0

    .line 1596192
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1596193
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1596194
    const v1, 0x1ef992d6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1596195
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1ef992d6 -> :sswitch_1
        0x5834b395 -> :sswitch_0
        0x6f659f40 -> :sswitch_1
        0x7ef3d48c -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1596185
    if-eqz p1, :cond_0

    .line 1596186
    invoke-static {p0, p1, p2}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1596187
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    .line 1596188
    if-eq v0, v1, :cond_0

    .line 1596189
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1596190
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1596184
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1596182
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1596183
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1596177
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1596178
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1596179
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1596180
    iput p2, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->b:I

    .line 1596181
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1596176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1596175
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1596172
    iget v0, p0, LX/1vt;->c:I

    .line 1596173
    move v0, v0

    .line 1596174
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1596169
    iget v0, p0, LX/1vt;->c:I

    .line 1596170
    move v0, v0

    .line 1596171
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1596166
    iget v0, p0, LX/1vt;->b:I

    .line 1596167
    move v0, v0

    .line 1596168
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1596163
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1596164
    move-object v0, v0

    .line 1596165
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1596154
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1596155
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1596156
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1596157
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1596158
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1596159
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1596160
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1596161
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1596162
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1596151
    iget v0, p0, LX/1vt;->c:I

    .line 1596152
    move v0, v0

    .line 1596153
    return v0
.end method
