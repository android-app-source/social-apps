.class public final Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x47e24469
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603275
    const-class v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603274
    const-class v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603272
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1603273
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1603263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603264
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1603265
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x684af6a4

    invoke-static {v2, v1, v3}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1603266
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1603267
    iget v2, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->e:I

    invoke-virtual {p1, v4, v2, v4}, LX/186;->a(III)V

    .line 1603268
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1603269
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1603270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603271
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1603261
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->f:Ljava/util/List;

    .line 1603262
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1603246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603247
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1603248
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1603249
    if-eqz v1, :cond_2

    .line 1603250
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    .line 1603251
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1603252
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1603253
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x684af6a4

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1603254
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1603255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    .line 1603256
    iput v3, v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->g:I

    move-object v1, v0

    .line 1603257
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603258
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 1603259
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 1603260
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1603242
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1603243
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->e:I

    .line 1603244
    const/4 v0, 0x2

    const v1, 0x684af6a4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->g:I

    .line 1603245
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1603235
    new-instance v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;-><init>()V

    .line 1603236
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1603237
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1603241
    const v0, 0x56200c14

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1603240
    const v0, 0x6ddb8072

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603238
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1603239
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
