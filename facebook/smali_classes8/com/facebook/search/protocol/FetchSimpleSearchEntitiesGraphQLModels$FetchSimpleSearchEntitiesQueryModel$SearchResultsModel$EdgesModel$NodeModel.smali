.class public final Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6fdf753
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603138
    const-class v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603139
    const-class v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603140
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1603141
    return-void
.end method

.method private j()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603142
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    .line 1603143
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603144
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    .line 1603145
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603185
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1603186
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603146
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1603147
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1603148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603149
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1603150
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1603151
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1603152
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1603153
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->fO_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1603154
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1603155
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1603156
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1603157
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1603158
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1603159
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1603160
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1603161
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1603162
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1603163
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1603164
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1603165
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603166
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1603167
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603168
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1603169
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    .line 1603170
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1603171
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    .line 1603172
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel;

    .line 1603173
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1603174
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    .line 1603175
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1603176
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    .line 1603177
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    .line 1603178
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1603179
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1603180
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1603181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    .line 1603182
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1603183
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603184
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603118
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1603135
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1603136
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->i:Z

    .line 1603137
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603132
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1603133
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1603134
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1603129
    new-instance v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;-><init>()V

    .line 1603130
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1603131
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603128
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603126
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 1603127
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1603125
    const v0, 0x3743c7f4

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1603123
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1603124
    iget-boolean v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1603122
    const v0, 0x4c7ec322    # 6.6784392E7f

    return v0
.end method

.method public final fO_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603120
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1603121
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic fP_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603119
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method
