.class public final Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x444824da
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612022
    const-class v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612021
    const-class v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1612019
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1612020
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612017
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->e:Ljava/lang/String;

    .line 1612018
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612015
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1612016
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getItemPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612013
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1612014
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612011
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->h:Ljava/lang/String;

    .line 1612012
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612023
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->i:Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->i:Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    .line 1612024
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->i:Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1611997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1611998
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1611999
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x676d4e15

    invoke-static {v2, v1, v3}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1612000
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x1147dde2

    invoke-static {v3, v2, v4}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1612001
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1612002
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->n()Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1612003
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1612004
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1612005
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1612006
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1612007
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1612008
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1612009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612010
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1611976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1611977
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1611978
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x676d4e15

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1611979
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1611980
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;

    .line 1611981
    iput v3, v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->f:I

    move-object v1, v0

    .line 1611982
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1611983
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1147dde2

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1611984
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1611985
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;

    .line 1611986
    iput v3, v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->g:I

    move-object v1, v0

    .line 1611987
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->n()Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1611988
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->n()Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    .line 1611989
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->n()Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1611990
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;

    .line 1611991
    iput-object v0, v1, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->i:Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel$SellerModel;

    .line 1611992
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1611993
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1611994
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1611995
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 1611996
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1611975
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1611971
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1611972
    const/4 v0, 0x1

    const v1, -0x676d4e15

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->f:I

    .line 1611973
    const/4 v0, 0x2

    const v1, -0x1147dde2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;->g:I

    .line 1611974
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1611966
    new-instance v0, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/resultpage/SearchProductItemNodeModels$SearchProductItemNodeModel;-><init>()V

    .line 1611967
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1611968
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1611970
    const v0, -0x6f000822

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1611969
    const v0, 0xa7c5482

    return v0
.end method
