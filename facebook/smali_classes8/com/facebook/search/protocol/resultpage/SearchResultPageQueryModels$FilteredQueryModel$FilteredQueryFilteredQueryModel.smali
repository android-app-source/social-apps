.class public final Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4fb4a3a2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612298
    const-class v0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612299
    const-class v0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1612300
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1612301
    return-void
.end method

.method private a()Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612302
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->e:Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->e:Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    .line 1612303
    iget-object v0, p0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->e:Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1612304
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1612305
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->a()Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1612306
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1612307
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1612308
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612309
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1612310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1612311
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->a()Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1612312
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->a()Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    .line 1612313
    invoke-direct {p0}, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->a()Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1612314
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;

    .line 1612315
    iput-object v0, v1, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;->e:Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel$ResultsModel;

    .line 1612316
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612317
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1612318
    new-instance v0, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/resultpage/SearchResultPageQueryModels$FilteredQueryModel$FilteredQueryFilteredQueryModel;-><init>()V

    .line 1612319
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1612320
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1612321
    const v0, 0x6ed52e85

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1612322
    const v0, -0x1bce060e

    return v0
.end method
