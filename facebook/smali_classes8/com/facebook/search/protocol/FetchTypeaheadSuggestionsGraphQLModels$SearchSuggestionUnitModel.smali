.class public final Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/7CB;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3a04871e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604685
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604686
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1604687
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1604688
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604680
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1604681
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1604682
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1604689
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->f:Ljava/util/List;

    .line 1604690
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604691
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1604692
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604693
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1604694
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method private n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604695
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1604696
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604697
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604698
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604699
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->m:Ljava/lang/String;

    .line 1604700
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604701
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o:Ljava/lang/String;

    .line 1604702
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604683
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604684
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private s()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604601
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->q:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->q:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    .line 1604602
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->q:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    return-object v0
.end method

.method private t()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1604638
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r:Ljava/util/List;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r:Ljava/util/List;

    .line 1604639
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604640
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s:Ljava/lang/String;

    .line 1604641
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604642
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1604643
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604644
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->u:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->u:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1604645
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->u:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 16

    .prologue
    .line 1604603
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604604
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1604605
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->k()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1604606
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1604607
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->m()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1604608
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1604609
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1604610
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1604611
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->q()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1604612
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1604613
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1604614
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->t()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->c(Ljava/util/List;)I

    move-result v11

    .line 1604615
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->u()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1604616
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->v()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1604617
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->w()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1604618
    const/16 v15, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 1604619
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 1604620
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1604621
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1604622
    const/4 v1, 0x3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1604623
    const/4 v1, 0x4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1604624
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1604625
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1604626
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1604627
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1604628
    const/16 v1, 0x9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1604629
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1604630
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1604631
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1604632
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1604633
    const/16 v1, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1604634
    const/16 v1, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 1604635
    const/16 v1, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 1604636
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604637
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1604646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604647
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1604648
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1604649
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1604650
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604651
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1604652
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1604653
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604654
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1604655
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604656
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604657
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1604658
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604659
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1604660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604661
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1604662
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1604663
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    .line 1604664
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->s()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1604665
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604666
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->q:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel$QueryTitleModel;

    .line 1604667
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604668
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604669
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1604670
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1604671
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->h:Z

    .line 1604672
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->i:Z

    .line 1604673
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;->n:Z

    .line 1604674
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1604675
    new-instance v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;-><init>()V

    .line 1604676
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1604677
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1604678
    const v0, -0x6ef6617f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1604679
    const v0, 0x223ed590

    return v0
.end method
