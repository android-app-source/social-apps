.class public final Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2dba42db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1611720
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1611719
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1611717
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1611718
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1611711
    iput-boolean p1, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->f:Z

    .line 1611712
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1611713
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1611714
    if-eqz v0, :cond_0

    .line 1611715
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1611716
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1611702
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1611703
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1611704
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1611705
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1611706
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1611707
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1611708
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1611709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1611710
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1611699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1611700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1611701
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1611698
    new-instance v0, LX/A1j;

    invoke-direct {v0, p1}, LX/A1j;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1611697
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1611721
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1611722
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->f:Z

    .line 1611723
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1611691
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1611692
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1611693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1611694
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1611695
    :goto_0
    return-void

    .line 1611696
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1611688
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1611689
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->a(Z)V

    .line 1611690
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1611685
    new-instance v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;-><init>()V

    .line 1611686
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1611687
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1611684
    const v0, 0x4cd5213

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1611683
    const v0, 0xd0d7ab1

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1611681
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->e:Ljava/lang/String;

    .line 1611682
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1611679
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1611680
    iget-boolean v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->f:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1611677
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->g:Ljava/lang/String;

    .line 1611678
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategoryTopicsGraphQLModels$SearchCategoryTopicModel;->g:Ljava/lang/String;

    return-object v0
.end method
