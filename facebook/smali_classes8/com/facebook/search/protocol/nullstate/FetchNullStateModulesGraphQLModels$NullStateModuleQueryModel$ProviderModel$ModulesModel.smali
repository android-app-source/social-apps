.class public final Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x53990215
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1610803
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1610802
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1610800
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1610801
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1610781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1610782
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1610783
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1610784
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1610785
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1610786
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->n()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1610787
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1610788
    const/16 v6, 0x9

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1610789
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1610790
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1610791
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->g:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1610792
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->h:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1610793
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1610794
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1610795
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1610796
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1610797
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1610798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1610799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1610773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1610774
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1610775
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1610776
    if-eqz v1, :cond_0

    .line 1610777
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;

    .line 1610778
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->k:Ljava/util/List;

    .line 1610779
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1610780
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610771
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->e:Ljava/lang/String;

    .line 1610772
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1610761
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1610762
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->g:I

    .line 1610763
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->h:I

    .line 1610764
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->m:Z

    .line 1610765
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1610768
    new-instance v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;-><init>()V

    .line 1610769
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1610770
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1610767
    const v0, 0x1ed9d917

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1610766
    const v0, -0x29562d00

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610759
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->f:Ljava/lang/String;

    .line 1610760
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1610757
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1610758
    iget v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->h:I

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610755
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->i:Ljava/lang/String;

    .line 1610756
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610753
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->j:Ljava/lang/String;

    .line 1610754
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1610751
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->k:Ljava/util/List;

    .line 1610752
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610749
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->l:Ljava/lang/String;

    .line 1610750
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->l:Ljava/lang/String;

    return-object v0
.end method
