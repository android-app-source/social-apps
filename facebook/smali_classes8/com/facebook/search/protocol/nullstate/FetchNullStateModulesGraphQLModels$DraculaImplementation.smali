.class public final Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1610488
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1610489
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1610490
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1610491
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1610492
    if-nez p1, :cond_0

    .line 1610493
    :goto_0
    return v0

    .line 1610494
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1610495
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1610496
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1610497
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1610498
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1610499
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1610500
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1610501
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1610502
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1610503
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1610504
    const v3, 0x9a8383c

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1610505
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1610506
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1610507
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1610508
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1610509
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1610510
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1610511
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1610512
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1610513
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x1e9ebe21 -> :sswitch_0
        0x22208e1 -> :sswitch_1
        0x9a8383c -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1610514
    new-instance v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1610515
    sparse-switch p2, :sswitch_data_0

    .line 1610516
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1610517
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1610518
    const v1, 0x9a8383c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1610519
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1e9ebe21 -> :sswitch_1
        0x22208e1 -> :sswitch_0
        0x9a8383c -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1610520
    if-eqz p1, :cond_0

    .line 1610521
    invoke-static {p0, p1, p2}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1610522
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;

    .line 1610523
    if-eq v0, v1, :cond_0

    .line 1610524
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1610525
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1610526
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1610486
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1610487
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1610527
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1610528
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1610529
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1610530
    iput p2, p0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->b:I

    .line 1610531
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1610485
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1610484
    new-instance v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1610460
    iget v0, p0, LX/1vt;->c:I

    .line 1610461
    move v0, v0

    .line 1610462
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1610481
    iget v0, p0, LX/1vt;->c:I

    .line 1610482
    move v0, v0

    .line 1610483
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1610478
    iget v0, p0, LX/1vt;->b:I

    .line 1610479
    move v0, v0

    .line 1610480
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610475
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1610476
    move-object v0, v0

    .line 1610477
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1610466
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1610467
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1610468
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1610469
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1610470
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1610471
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1610472
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1610473
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1610474
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1610463
    iget v0, p0, LX/1vt;->c:I

    .line 1610464
    move v0, v0

    .line 1610465
    return v0
.end method
