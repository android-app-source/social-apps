.class public final Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1611339
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;

    new-instance v1, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1611340
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1611338
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1611325
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1611326
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1611327
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1611328
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1611329
    if-eqz v2, :cond_0

    .line 1611330
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611331
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1611332
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1611333
    if-eqz v2, :cond_1

    .line 1611334
    const-string p0, "subtopics"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611335
    invoke-static {v1, v2, p1, p2}, LX/A1d;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1611336
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1611337
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1611324
    check-cast p1, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$Serializer;->a(Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;LX/0nX;LX/0my;)V

    return-void
.end method
