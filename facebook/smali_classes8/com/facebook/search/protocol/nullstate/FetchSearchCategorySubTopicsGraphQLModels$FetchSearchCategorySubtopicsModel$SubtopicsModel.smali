.class public final Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1cc46f0b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1611377
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1611376
    const-class v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1611374
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1611375
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1611368
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1611369
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1611370
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1611371
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1611372
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1611373
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1611378
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1611379
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1611380
    invoke-virtual {p0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1611381
    if-eqz v1, :cond_0

    .line 1611382
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;

    .line 1611383
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->e:LX/3Sb;

    .line 1611384
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1611385
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1611366
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x2e817a56

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->e:LX/3Sb;

    .line 1611367
    iget-object v0, p0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1611363
    new-instance v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel$SubtopicsModel;-><init>()V

    .line 1611364
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1611365
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1611362
    const v0, 0xc119e10

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1611361
    const v0, -0x3b239ea7

    return v0
.end method
