.class public Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:LX/A0S;

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1605032
    new-instance v0, LX/A0Q;

    invoke-direct {v0}, LX/A0Q;-><init>()V

    sput-object v0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/A0R;)V
    .locals 4

    .prologue
    .line 1605033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1605034
    iget-object v0, p1, LX/A0R;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1605035
    iput-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->a:Ljava/lang/String;

    .line 1605036
    iget-object v0, p1, LX/A0R;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1605037
    iput-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->b:Ljava/lang/String;

    .line 1605038
    iget-object v0, p1, LX/A0R;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1605039
    iput-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->c:Ljava/lang/String;

    .line 1605040
    iget-object v0, p1, LX/A0R;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1605041
    iput-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->d:Ljava/lang/String;

    .line 1605042
    iget-wide v2, p1, LX/A0R;->e:J

    move-wide v0, v2

    .line 1605043
    iput-wide v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->e:J

    .line 1605044
    iget-object v0, p1, LX/A0R;->f:LX/A0S;

    move-object v0, v0

    .line 1605045
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A0S;

    iput-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->f:LX/A0S;

    .line 1605046
    iget-boolean v0, p1, LX/A0R;->g:Z

    move v0, v0

    .line 1605047
    iput-boolean v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->g:Z

    .line 1605048
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1605049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1605050
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->a:Ljava/lang/String;

    .line 1605051
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->b:Ljava/lang/String;

    .line 1605052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->c:Ljava/lang/String;

    .line 1605053
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->d:Ljava/lang/String;

    .line 1605054
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->e:J

    .line 1605055
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/A0S;->valueOf(Ljava/lang/String;)LX/A0S;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->f:LX/A0S;

    .line 1605056
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->g:Z

    .line 1605057
    return-void

    .line 1605058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1605059
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1605060
    iget-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1605061
    iget-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1605062
    iget-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1605063
    iget-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1605064
    iget-wide v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1605065
    iget-object v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->f:LX/A0S;

    invoke-virtual {v0}, LX/A0S;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1605066
    iget-boolean v0, p0, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1605067
    return-void

    .line 1605068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
