.class public final Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1597041
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1597042
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1597043
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1597044
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1597045
    if-nez p1, :cond_0

    .line 1597046
    :goto_0
    return v0

    .line 1597047
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1597048
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1597049
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1597050
    const v2, 0x6024f6f4

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1597051
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 1597052
    const v3, -0x22b0dd75

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1597053
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 1597054
    const v4, -0x710ed501

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1597055
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v4

    .line 1597056
    const v5, 0x42a81cd5

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 1597057
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1597058
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597059
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1597060
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1597061
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1597062
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1597063
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1597064
    const v2, 0x26761c4d

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1597065
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597066
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597067
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1597068
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597069
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597070
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597071
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597072
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1597073
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1597074
    const v2, 0x250caf5

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1597075
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597076
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597077
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597078
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597079
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597080
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597081
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597082
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597083
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1597084
    const v2, -0x55db13ed

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1597085
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597086
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597087
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597088
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597089
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597090
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597091
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597092
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597093
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1597094
    const v2, -0x6f751383

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1597095
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597096
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597097
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597098
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597099
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597100
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597101
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597102
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597103
    :sswitch_9
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1597104
    const v2, 0x4c8d3c98    # 7.4048704E7f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1597105
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597106
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597107
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597108
    :sswitch_a
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597109
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597110
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597111
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597112
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597113
    :sswitch_b
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597114
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597115
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1597116
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597117
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1597118
    :sswitch_c
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1597119
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1597120
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1597121
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 1597122
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1597123
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1597124
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1597125
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1597126
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1597127
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 1597128
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1597129
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x710ed501 -> :sswitch_5
        -0x6f751383 -> :sswitch_8
        -0x55db13ed -> :sswitch_6
        -0x3e89b748 -> :sswitch_b
        -0x22b0dd75 -> :sswitch_3
        0x250caf5 -> :sswitch_4
        0x7670cf1 -> :sswitch_0
        0x26761c4d -> :sswitch_2
        0x2db5d560 -> :sswitch_9
        0x33c862ba -> :sswitch_c
        0x42a81cd5 -> :sswitch_7
        0x4c8d3c98 -> :sswitch_a
        0x6024f6f4 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1597130
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1597154
    sparse-switch p2, :sswitch_data_0

    .line 1597155
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1597156
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597157
    const v1, 0x6024f6f4

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1597158
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597159
    const v1, -0x22b0dd75

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1597160
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597161
    const v1, -0x710ed501

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1597162
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597163
    const v1, 0x42a81cd5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1597164
    :goto_0
    :sswitch_1
    return-void

    .line 1597165
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597166
    const v1, 0x26761c4d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1597167
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597168
    const v1, 0x250caf5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1597169
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597170
    const v1, -0x55db13ed

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1597171
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597172
    const v1, -0x6f751383

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1597173
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1597174
    const v1, 0x4c8d3c98    # 7.4048704E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x710ed501 -> :sswitch_4
        -0x6f751383 -> :sswitch_1
        -0x55db13ed -> :sswitch_1
        -0x3e89b748 -> :sswitch_1
        -0x22b0dd75 -> :sswitch_3
        0x250caf5 -> :sswitch_1
        0x7670cf1 -> :sswitch_0
        0x26761c4d -> :sswitch_1
        0x2db5d560 -> :sswitch_6
        0x33c862ba -> :sswitch_1
        0x42a81cd5 -> :sswitch_5
        0x4c8d3c98 -> :sswitch_1
        0x6024f6f4 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1597131
    if-nez p1, :cond_0

    move v0, v1

    .line 1597132
    :goto_0
    return v0

    .line 1597133
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1597134
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1597135
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1597136
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1597137
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1597138
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1597139
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1597140
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1597141
    if-eqz p1, :cond_0

    .line 1597142
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1597143
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1597144
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1597145
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1597146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1597147
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1597148
    if-eqz p1, :cond_0

    .line 1597149
    invoke-static {p0, p1, p2}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1597150
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;

    .line 1597151
    if-eq v0, v1, :cond_0

    .line 1597152
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1597153
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1597038
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1597039
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1597040
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1597033
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1597034
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1597035
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1597036
    iput p2, p0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->b:I

    .line 1597037
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1597032
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1597031
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1597028
    iget v0, p0, LX/1vt;->c:I

    .line 1597029
    move v0, v0

    .line 1597030
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1597025
    iget v0, p0, LX/1vt;->c:I

    .line 1597026
    move v0, v0

    .line 1597027
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1597022
    iget v0, p0, LX/1vt;->b:I

    .line 1597023
    move v0, v0

    .line 1597024
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1597019
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1597020
    move-object v0, v0

    .line 1597021
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1597010
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1597011
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1597012
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1597013
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1597014
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1597015
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1597016
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1597017
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1597018
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1597007
    iget v0, p0, LX/1vt;->c:I

    .line 1597008
    move v0, v0

    .line 1597009
    return v0
.end method
