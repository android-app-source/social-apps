.class public final Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x20d9359e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1601300
    const-class v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1601278
    const-class v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1601298
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1601299
    return-void
.end method

.method private j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1601296
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    .line 1601297
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1601294
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->f:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->f:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    .line 1601295
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->f:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1601286
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1601287
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1601288
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->k()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1601289
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1601290
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1601291
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1601292
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1601293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1601301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1601302
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1601303
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    .line 1601304
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1601305
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;

    .line 1601306
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    .line 1601307
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->k()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1601308
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->k()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    .line 1601309
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->k()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1601310
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;

    .line 1601311
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->f:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    .line 1601312
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1601313
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1601285
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1601282
    new-instance v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;-><init>()V

    .line 1601283
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1601284
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1601281
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;->k()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchFlexibleContextMetadataModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1601280
    const v0, 0x6b49c793

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1601279
    const v0, 0x7c3d9a54

    return v0
.end method
