.class public final Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1bf3b7ab
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1600848
    const-class v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1600847
    const-class v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1600845
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1600846
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1600842
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1600843
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1600844
    return-void
.end method

.method private j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600840
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    .line 1600841
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    return-object v0
.end method

.method private k()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600838
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1600839
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1600820
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1600821
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1600822
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1600823
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1600824
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1600825
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->fH_()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1600826
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->fI_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1600827
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1600828
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1600829
    const/4 v0, 0x1

    iget v6, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->f:I

    invoke-virtual {p1, v0, v6, v7}, LX/186;->a(III)V

    .line 1600830
    const/4 v0, 0x2

    iget-boolean v6, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->g:Z

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 1600831
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1600832
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1600833
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1600834
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1600835
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1600836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1600837
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1600802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1600803
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1600804
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    .line 1600805
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1600806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    .line 1600807
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    .line 1600808
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1600809
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1600810
    if-eqz v2, :cond_1

    .line 1600811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    .line 1600812
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 1600813
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1600814
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1600815
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1600816
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    .line 1600817
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1600818
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1600819
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600801
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$CorrectedQueryModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1600849
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1600850
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->f:I

    .line 1600851
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->g:Z

    .line 1600852
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1600798
    new-instance v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;-><init>()V

    .line 1600799
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1600800
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1600796
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1600797
    iget-boolean v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->g:Z

    return v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1600794
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->h:Ljava/util/List;

    .line 1600795
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic d()LX/80n;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600793
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1600792
    const v0, -0x30fa4574

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600790
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j:Ljava/lang/String;

    .line 1600791
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1600789
    const v0, -0x1cb28811

    return v0
.end method

.method public final fH_()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600785
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    .line 1600786
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->k:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    return-object v0
.end method

.method public final fI_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1600787
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->l:Ljava/lang/String;

    .line 1600788
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->l:Ljava/lang/String;

    return-object v0
.end method
