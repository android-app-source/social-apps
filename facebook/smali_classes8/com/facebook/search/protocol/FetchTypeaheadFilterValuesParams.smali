.class public Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1604126
    new-instance v0, LX/A0B;

    invoke-direct {v0}, LX/A0B;-><init>()V

    sput-object v0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1604127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1604128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->a:Ljava/lang/String;

    .line 1604129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->b:Ljava/lang/String;

    .line 1604130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->c:I

    .line 1604131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->d:Ljava/lang/String;

    .line 1604132
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1604133
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1604134
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1604135
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1604136
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadFilterValuesParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1604137
    return-void
.end method
