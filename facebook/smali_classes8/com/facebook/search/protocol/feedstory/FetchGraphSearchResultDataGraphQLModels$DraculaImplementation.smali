.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1608712
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1608713
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1608767
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1608768
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1608751
    if-nez p1, :cond_0

    .line 1608752
    :goto_0
    return v0

    .line 1608753
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1608754
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1608755
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1608756
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1608757
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1608758
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1608759
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1608760
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1608761
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1608762
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 1608763
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1608764
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1608765
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 1608766
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x33f4656c -> :sswitch_0
        0x2f3b8eae -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1608750
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1608747
    sparse-switch p0, :sswitch_data_0

    .line 1608748
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1608749
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x33f4656c -> :sswitch_0
        0x2f3b8eae -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1608746
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1608744
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->b(I)V

    .line 1608745
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1608739
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1608740
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1608741
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1608742
    iput p2, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->b:I

    .line 1608743
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1608769
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1608738
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1608735
    iget v0, p0, LX/1vt;->c:I

    .line 1608736
    move v0, v0

    .line 1608737
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1608732
    iget v0, p0, LX/1vt;->c:I

    .line 1608733
    move v0, v0

    .line 1608734
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1608729
    iget v0, p0, LX/1vt;->b:I

    .line 1608730
    move v0, v0

    .line 1608731
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608726
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1608727
    move-object v0, v0

    .line 1608728
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1608717
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1608718
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1608719
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1608720
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1608721
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1608722
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1608723
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1608724
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1608725
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1608714
    iget v0, p0, LX/1vt;->c:I

    .line 1608715
    move v0, v0

    .line 1608716
    return v0
.end method
