.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x34676577
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607036
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607018
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1607034
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1607035
    return-void
.end method

.method private a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFilteredQuery"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607032
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    .line 1607033
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1607037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607038
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1607039
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1607040
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1607041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607042
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1607024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607025
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1607026
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    .line 1607027
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1607028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;

    .line 1607029
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    .line 1607030
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607031
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1607021
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel;-><init>()V

    .line 1607022
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1607023
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1607020
    const v0, -0x6071e4bf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1607019
    const v0, -0x1bce060e

    return v0
.end method
