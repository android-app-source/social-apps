.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12682a60
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1606989
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607002
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1607000
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1607001
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606998
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->e:Ljava/lang/String;

    .line 1606999
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606996
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->f:Ljava/lang/String;

    .line 1606997
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getModules"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606994
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1606995
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606992
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->h:Ljava/lang/String;

    .line 1606993
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getQueryTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606990
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1606991
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607003
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->j:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->j:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1607004
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->j:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1606938
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->k:Ljava/util/List;

    .line 1606939
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1606940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1606941
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1606942
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1606943
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x435b5b21

    invoke-static {v3, v2, v4}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1606944
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1606945
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x33f4656c    # -3.6596304E7f

    invoke-static {v5, v4, v6}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1606946
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->o()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1606947
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->p()LX/0Px;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->c(Ljava/util/List;)I

    move-result v6

    .line 1606948
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1606949
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1606950
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1606951
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1606952
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1606953
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1606954
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1606955
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1606956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1606957
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1606958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1606959
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1606960
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x435b5b21

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1606961
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1606962
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    .line 1606963
    iput v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->g:I

    move-object v1, v0

    .line 1606964
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1606965
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x33f4656c    # -3.6596304E7f

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1606966
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1606967
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    .line 1606968
    iput v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->i:I

    move-object v1, v0

    .line 1606969
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->o()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1606970
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->o()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1606971
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->o()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1606972
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    .line 1606973
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->j:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1606974
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1606975
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1606976
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1606977
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 1606978
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606979
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1606980
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1606981
    const/4 v0, 0x2

    const v1, 0x435b5b21

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->g:I

    .line 1606982
    const/4 v0, 0x4

    const v1, -0x33f4656c    # -3.6596304E7f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;->i:I

    .line 1606983
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1606984
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FetchGraphSearchForSalePostsWithFiltersModel$FilteredQueryModel;-><init>()V

    .line 1606985
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1606986
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1606987
    const v0, 0x44e745ec

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1606988
    const v0, -0x1bce060e

    return v0
.end method
