.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1245fbc0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607280
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607243
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1607278
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1607279
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1607276
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->e:Ljava/util/List;

    .line 1607277
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLineageSnippets"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1607274
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, 0x35410ac1

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->f:LX/3Sb;

    .line 1607275
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1607272
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->g:Ljava/util/List;

    .line 1607273
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1607262
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607263
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1607264
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 1607265
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1607266
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1607267
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1607268
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1607269
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1607270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607271
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1607249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607250
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1607251
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1607252
    if-eqz v1, :cond_0

    .line 1607253
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    .line 1607254
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->e:Ljava/util/List;

    .line 1607255
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->j()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1607256
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1607257
    if-eqz v1, :cond_1

    .line 1607258
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    .line 1607259
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;->f:LX/3Sb;

    .line 1607260
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607261
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1607246
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;-><init>()V

    .line 1607247
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1607248
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1607245
    const v0, -0x217ee3d3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1607244
    const v0, 0x16973d43

    return v0
.end method
