.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7e51cc9e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607184
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607183
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1607181
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1607182
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1607179
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->e:Ljava/util/List;

    .line 1607180
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607151
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1607152
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1607171
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607172
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1607173
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1607174
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1607175
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1607176
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1607177
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607178
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1607158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607159
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1607160
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1607161
    if-eqz v1, :cond_2

    .line 1607162
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1607163
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1607164
    :goto_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1607165
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1607166
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1607167
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1607168
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1607169
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607170
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1607155
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;-><init>()V

    .line 1607156
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1607157
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1607154
    const v0, 0x6475d65e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1607153
    const v0, 0x5e300d9e

    return v0
.end method
