.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/8d8;
.implements LX/8d9;
.implements LX/8dA;
.implements LX/8d0;
.implements LX/8d2;
.implements LX/8d1;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x54fd6cc0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:J

.field private I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Z

.field private L:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:J

.field private r:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Z

.field private z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1609167
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1609168
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1609169
    const/16 v0, 0x25

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1609170
    return-void
.end method

.method private D()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609171
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1609172
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    return-object v0
.end method

.method private E()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609173
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->o:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->o:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1609174
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->o:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    return-object v0
.end method

.method private F()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609175
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->r:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->r:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1609176
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->r:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    return-object v0
.end method

.method private G()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609177
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->s:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->s:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1609178
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->s:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    return-object v0
.end method

.method private H()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609179
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->u:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->u:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1609180
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->u:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609181
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1609182
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609183
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1609184
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private K()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609185
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->A:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->A:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1609186
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->A:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    return-object v0
.end method

.method private N()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609187
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1609188
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    return-object v0
.end method

.method private O()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609189
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1609190
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    return-object v0
.end method

.method private P()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609191
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1609192
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609193
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1609194
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method private R()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609197
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1609198
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    return-object v0
.end method

.method private S()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609195
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1609196
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1609370
    iput-object p1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1609371
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1609372
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1609373
    if-eqz v0, :cond_0

    .line 1609374
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x21

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1609375
    :cond_0
    return-void

    .line 1609376
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1609363
    iput-object p1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1609364
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1609365
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1609366
    if-eqz v0, :cond_0

    .line 1609367
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x23

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1609368
    :cond_0
    return-void

    .line 1609369
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1609356
    iput-object p1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1609357
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1609358
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1609359
    if-eqz v0, :cond_0

    .line 1609360
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xf

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1609361
    :cond_0
    return-void

    .line 1609362
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1609349
    iput-object p1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->M:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1609350
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1609351
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1609352
    if-eqz v0, :cond_0

    .line 1609353
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x22

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1609354
    :cond_0
    return-void

    .line 1609355
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609347
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1609348
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic B()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609346
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609345
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic L()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609344
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609342
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1609343
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 31

    .prologue
    .line 1609273
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1609274
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1609275
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->A()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1609276
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->c()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 1609277
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1609278
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->v()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v6

    .line 1609279
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1609280
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1609281
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1609282
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1609283
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1609284
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 1609285
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1609286
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1609287
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1609288
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1609289
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->K()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1609290
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1609291
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1609292
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1609293
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->P()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1609294
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->Q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1609295
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->R()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1609296
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1609297
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 1609298
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v26

    .line 1609299
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    .line 1609300
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v28

    .line 1609301
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->S()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1609302
    const/16 v30, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1609303
    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1609304
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1609305
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1609306
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1609307
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609308
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609309
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609310
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1609311
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1609312
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1609313
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1609314
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609315
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->q:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1609316
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1609317
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1609318
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1609319
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1609320
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1609321
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1609322
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609323
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->y:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609324
    const/16 v2, 0x15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->z:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609325
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609326
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609327
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609328
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609329
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609330
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609331
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609332
    const/16 v3, 0x1d

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1609333
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609334
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609335
    const/16 v2, 0x20

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1609336
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609337
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609338
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609339
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609340
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1609341
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1609200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1609201
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1609202
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1609203
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1609204
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609205
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1609206
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1609207
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1609208
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1609209
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609210
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->o:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1609211
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1609212
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1609213
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1609214
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609215
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->r:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1609216
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1609217
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1609218
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1609219
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609220
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->s:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1609221
    :cond_3
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1609222
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1609223
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1609224
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609225
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->u:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1609226
    :cond_4
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1609227
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1609228
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1609229
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609230
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1609231
    :cond_5
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1609232
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1609233
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1609234
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609235
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1609236
    :cond_6
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->K()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1609237
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->K()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1609238
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->K()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1609239
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609240
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->A:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1609241
    :cond_7
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1609242
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1609243
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1609244
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609245
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1609246
    :cond_8
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1609247
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1609248
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1609249
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609250
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1609251
    :cond_9
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->P()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1609252
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->P()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1609253
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->P()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1609254
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609255
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1609256
    :cond_a
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->Q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1609257
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->Q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1609258
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->Q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1609259
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609260
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1609261
    :cond_b
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->R()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1609262
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->R()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1609263
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->R()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1609264
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609265
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1609266
    :cond_c
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->S()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1609267
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->S()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1609268
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->S()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1609269
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    .line 1609270
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1609271
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1609272
    if-nez v1, :cond_e

    :goto_0
    return-object p0

    :cond_e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1609166
    new-instance v0, LX/A16;

    invoke-direct {v0, p1}, LX/A16;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609199
    invoke-virtual {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1609086
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1609087
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->i:Z

    .line 1609088
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->j:Z

    .line 1609089
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->k:Z

    .line 1609090
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->p:Z

    .line 1609091
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->q:J

    .line 1609092
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->y:Z

    .line 1609093
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->z:Z

    .line 1609094
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H:J

    .line 1609095
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->K:Z

    .line 1609096
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1609124
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1609125
    invoke-virtual {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1609126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1609127
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    .line 1609128
    :goto_0
    return-void

    .line 1609129
    :cond_0
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1609130
    invoke-virtual {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1609131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1609132
    const/16 v0, 0x21

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1609133
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1609134
    invoke-virtual {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1609135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1609136
    const/16 v0, 0x22

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1609137
    :cond_2
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1609138
    invoke-virtual {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1609139
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1609140
    const/16 v0, 0x23

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1609141
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1609115
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1609116
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1609117
    :cond_0
    :goto_0
    return-void

    .line 1609118
    :cond_1
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1609119
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0

    .line 1609120
    :cond_2
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1609121
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0

    .line 1609122
    :cond_3
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1609123
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1609112
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;-><init>()V

    .line 1609113
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1609114
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609111
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->E()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1609109
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->g:Ljava/util/List;

    .line 1609110
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609107
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->B:Ljava/lang/String;

    .line 1609108
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609105
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1609106
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609103
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->v:Ljava/lang/String;

    .line 1609104
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1609102
    const v0, -0x6152319

    return v0
.end method

.method public final synthetic e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609101
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->Q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1609100
    const v0, 0x252222

    return v0
.end method

.method public final synthetic j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609099
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609097
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I:Ljava/lang/String;

    .line 1609098
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609084
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1609085
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609142
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1609143
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609144
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1609145
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1609146
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1609147
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1609148
    iget-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->z:Z

    return v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609149
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1609150
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609151
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->H()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609152
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->R()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609153
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->M:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->M:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1609154
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->M:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609155
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->S()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 1609156
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1609157
    iget-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->k:Z

    return v0
.end method

.method public final v()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1609158
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->l:Ljava/util/List;

    .line 1609159
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1609160
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1609161
    iget-boolean v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->p:Z

    return v0
.end method

.method public final synthetic x()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609162
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->N()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609163
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->O()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609164
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1609165
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryResultFragmentModel$EdgesModel$NodeModel;->J:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    return-object v0
.end method
