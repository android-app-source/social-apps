.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1df70f9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1608288
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1608289
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1608290
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1608291
    return-void
.end method

.method private a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMainFilter"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608292
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    .line 1608293
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1608294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1608295
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1608296
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1608297
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1608298
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1608299
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1608300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1608301
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1608302
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    .line 1608303
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1608304
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;

    .line 1608305
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    .line 1608306
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1608307
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1608308
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryMainFilterFragmentModel;-><init>()V

    .line 1608309
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1608310
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1608311
    const v0, 0x19b8b65f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1608312
    const v0, -0x7ffdf76b

    return v0
.end method
