.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2dba40af
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1609035
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1609034
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1609032
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1609033
    return-void
.end method

.method private a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFilteredQuery"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1609011
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    .line 1609012
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1609026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1609027
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1609028
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1609029
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1609030
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1609031
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1609018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1609019
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1609020
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    .line 1609021
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1609022
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;

    .line 1609023
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel$FilteredQueryModel;

    .line 1609024
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1609025
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1609015
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;-><init>()V

    .line 1609016
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1609017
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1609014
    const v0, 0x4f3b66cd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1609013
    const v0, -0x1bce060e

    return v0
.end method
