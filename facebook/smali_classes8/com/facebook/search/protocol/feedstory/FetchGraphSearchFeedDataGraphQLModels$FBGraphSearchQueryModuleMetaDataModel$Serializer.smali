.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1606849
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel;

    new-instance v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1606850
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1606851
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1606852
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1606853
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1606854
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1606855
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1606856
    if-eqz v2, :cond_0

    .line 1606857
    const-string p0, "modules"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1606858
    invoke-static {v1, v2, p1, p2}, LX/A0l;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1606859
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1606860
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1606861
    check-cast p1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel$Serializer;->a(Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$FBGraphSearchQueryModuleMetaDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
