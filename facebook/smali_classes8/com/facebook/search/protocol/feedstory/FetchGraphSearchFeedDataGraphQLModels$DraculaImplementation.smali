.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1606821
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1606822
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1606819
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1606820
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1606768
    if-nez p1, :cond_0

    .line 1606769
    :goto_0
    return v0

    .line 1606770
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1606771
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1606772
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1606773
    const v2, 0x355b3625

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1606774
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1606775
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1606776
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v3

    .line 1606777
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1606778
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1606779
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1606780
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1606781
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1606782
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1606783
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1606784
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1606785
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1606786
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1606787
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1606788
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 1606789
    const v3, 0x6d5f054e

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1606790
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1606791
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1606792
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1606793
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1606794
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1606795
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1606796
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1606797
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1606798
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1606799
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1606800
    const v2, 0x4e9e58ee

    const/4 v4, 0x0

    .line 1606801
    if-nez v1, :cond_1

    move v3, v4

    .line 1606802
    :goto_1
    move v1, v3

    .line 1606803
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1606804
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1606805
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1606806
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1606807
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1606808
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1606809
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1606810
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1606811
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 1606812
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1606813
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1606814
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v7

    .line 1606815
    invoke-static {p0, v7, v2, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v7

    aput v7, v3, v4

    .line 1606816
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1606817
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1606818
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x35410ac1 -> :sswitch_3
        0x355b3625 -> :sswitch_1
        0x435b5b21 -> :sswitch_0
        0x4e9e58ee -> :sswitch_4
        0x6d5f054e -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1606759
    if-nez p0, :cond_0

    move v0, v1

    .line 1606760
    :goto_0
    return v0

    .line 1606761
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1606762
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1606763
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1606764
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1606765
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1606766
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1606767
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1606752
    const/4 v7, 0x0

    .line 1606753
    const/4 v1, 0x0

    .line 1606754
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1606755
    invoke-static {v2, v3, v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1606756
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1606757
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1606758
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1606751
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1606735
    sparse-switch p2, :sswitch_data_0

    .line 1606736
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1606737
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1606738
    const v1, 0x355b3625

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1606739
    :goto_0
    :sswitch_1
    return-void

    .line 1606740
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1606741
    const v1, 0x6d5f054e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1606742
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1606743
    const v1, 0x4e9e58ee

    .line 1606744
    if-eqz v0, :cond_0

    .line 1606745
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1606746
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1606747
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1606748
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1606749
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1606750
    :cond_0
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x35410ac1 -> :sswitch_3
        0x355b3625 -> :sswitch_2
        0x435b5b21 -> :sswitch_0
        0x4e9e58ee -> :sswitch_1
        0x6d5f054e -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1606729
    if-eqz p1, :cond_0

    .line 1606730
    invoke-static {p0, p1, p2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1606731
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    .line 1606732
    if-eq v0, v1, :cond_0

    .line 1606733
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1606734
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1606695
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1606727
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1606728
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1606722
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1606723
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1606724
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1606725
    iput p2, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->b:I

    .line 1606726
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1606721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1606720
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1606717
    iget v0, p0, LX/1vt;->c:I

    .line 1606718
    move v0, v0

    .line 1606719
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1606714
    iget v0, p0, LX/1vt;->c:I

    .line 1606715
    move v0, v0

    .line 1606716
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1606711
    iget v0, p0, LX/1vt;->b:I

    .line 1606712
    move v0, v0

    .line 1606713
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1606708
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1606709
    move-object v0, v0

    .line 1606710
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1606699
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1606700
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1606701
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1606702
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1606703
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1606704
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1606705
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1606706
    invoke-static {v3, v9, v2}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1606707
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1606696
    iget v0, p0, LX/1vt;->c:I

    .line 1606697
    move v0, v0

    .line 1606698
    return v0
.end method
