.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1608808
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel;

    new-instance v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1608809
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1608836
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1608811
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1608812
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    .line 1608813
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1608814
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1608815
    if-eqz v2, :cond_0

    .line 1608816
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1608817
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1608818
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1608819
    if-eqz v2, :cond_1

    .line 1608820
    const-string v3, "logging_unit_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1608821
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1608822
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1608823
    if-eqz v2, :cond_2

    .line 1608824
    const-string v3, "query_function"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1608825
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1608826
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1608827
    if-eqz v2, :cond_3

    .line 1608828
    const-string v3, "query_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1608829
    invoke-static {v1, v2, p1}, LX/A18;->a(LX/15i;ILX/0nX;)V

    .line 1608830
    :cond_3
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1608831
    if-eqz v2, :cond_4

    .line 1608832
    const-string v2, "search_result_style_list"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1608833
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1608834
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1608835
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1608810
    check-cast p1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel$Serializer;->a(Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryDetailsFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
