.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x80101e1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607138
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607141
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1607139
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1607140
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607136
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->e:Ljava/lang/String;

    .line 1607137
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607134
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1607135
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResultDecoration"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607142
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    .line 1607143
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607132
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->h:Ljava/lang/String;

    .line 1607133
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1607120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607121
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1607122
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1607123
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1607124
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1607125
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1607126
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1607127
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1607128
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1607129
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1607130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1607107
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607108
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1607109
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1607110
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1607111
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;

    .line 1607112
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1607113
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1607114
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    .line 1607115
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1607116
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;

    .line 1607117
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$PostSearchResultDecorationModel;

    .line 1607118
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607119
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1607102
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel$EdgesModel;-><init>()V

    .line 1607103
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1607104
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1607106
    const v0, -0x4a202447

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1607105
    const v0, -0x12e2f383

    return v0
.end method
