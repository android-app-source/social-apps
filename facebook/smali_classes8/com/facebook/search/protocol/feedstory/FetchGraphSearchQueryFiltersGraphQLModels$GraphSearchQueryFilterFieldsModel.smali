.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xab81f12
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1608267
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1608266
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1608264
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1608265
    return-void
.end method

.method private j()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentValue"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608262
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    .line 1608263
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608260
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->f:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->f:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    .line 1608261
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->f:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFilterValues"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608258
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    .line 1608259
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608256
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->h:Ljava/lang/String;

    .line 1608257
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608208
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->i:Ljava/lang/String;

    .line 1608209
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608254
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j:Ljava/lang/String;

    .line 1608255
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608252
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k:Ljava/lang/String;

    .line 1608253
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1608234
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1608235
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1608236
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1608237
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->l()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1608238
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1608239
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1608240
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1608241
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1608242
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1608243
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1608244
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1608245
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1608246
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1608247
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1608248
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1608249
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1608250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1608251
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1608216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1608217
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1608218
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    .line 1608219
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->j()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1608220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    .line 1608221
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchFilterValueFragmentModel;

    .line 1608222
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1608223
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    .line 1608224
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->k()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1608225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    .line 1608226
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->f:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$CustomValueModel;

    .line 1608227
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->l()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1608228
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->l()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    .line 1608229
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->l()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1608230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    .line 1608231
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->g:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel$FilterValuesModel;

    .line 1608232
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1608233
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1608215
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1608212
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchQueryFiltersGraphQLModels$GraphSearchQueryFilterFieldsModel;-><init>()V

    .line 1608213
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1608214
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1608211
    const v0, -0x6a847344    # -5.0800066E-26f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1608210
    const v0, -0x55db0bf6

    return v0
.end method
