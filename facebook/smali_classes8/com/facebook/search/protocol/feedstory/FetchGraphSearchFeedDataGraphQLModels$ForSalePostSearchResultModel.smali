.class public final Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x35c52b4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607216
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1607199
    const-class v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1607214
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1607215
    return-void
.end method

.method private a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1607212
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1607213
    iget-object v0, p0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1607217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607218
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1607219
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1607220
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1607221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607222
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1607204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1607205
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1607206
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1607207
    invoke-direct {p0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->a()Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1607208
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;

    .line 1607209
    iput-object v0, v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;->e:Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel$ResultsModel;

    .line 1607210
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1607211
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1607201
    new-instance v0, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchFeedDataGraphQLModels$ForSalePostSearchResultModel;-><init>()V

    .line 1607202
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1607203
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1607200
    const v0, -0x64529bc6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1607198
    const v0, -0x1bce060e

    return v0
.end method
