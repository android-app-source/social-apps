.class public Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1604987
    new-instance v0, LX/A0O;

    invoke-direct {v0}, LX/A0O;-><init>()V

    sput-object v0, Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1604982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1604983
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;->a:Ljava/lang/String;

    .line 1604984
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;->b:Z

    .line 1604985
    return-void

    .line 1604986
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1604976
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1604981
    iget-object v0, p0, Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1604977
    iget-object v0, p0, Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1604978
    iget-boolean v0, p0, Lcom/facebook/search/protocol/GraphSearchFeedTypeValueParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1604979
    return-void

    .line 1604980
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
