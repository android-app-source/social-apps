.class public final Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603019
    const-class v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603007
    const-class v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603017
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1603018
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603015
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;->e:Ljava/lang/String;

    .line 1603016
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1603020
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603021
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1603022
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1603023
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1603024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603025
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1603012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603013
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603014
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1603009
    new-instance v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupIconModel$DarkIconModel;-><init>()V

    .line 1603010
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1603011
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1603008
    const v0, -0x67a6ee3d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1603006
    const v0, 0x22b099

    return v0
.end method
