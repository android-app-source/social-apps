.class public final Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1599078
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    new-instance v1, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1599079
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1599077
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1599064
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1599065
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1599066
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1599067
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1599068
    if-eqz v2, :cond_0

    .line 1599069
    const-string p0, "filtered_query"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599070
    invoke-static {v1, v2, p1, p2}, LX/9z4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1599071
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1599072
    if-eqz v2, :cond_1

    .line 1599073
    const-string p0, "logging_unit_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599074
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1599075
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1599076
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1599063
    check-cast p1, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$Serializer;->a(Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
