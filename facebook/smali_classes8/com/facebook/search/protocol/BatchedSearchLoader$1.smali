.class public final Lcom/facebook/search/protocol/BatchedSearchLoader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9yN;


# direct methods
.method public constructor <init>(LX/9yN;)V
    .locals 0

    .prologue
    .line 1596100
    iput-object p1, p0, Lcom/facebook/search/protocol/BatchedSearchLoader$1;->a:LX/9yN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1596101
    iget-object v0, p0, Lcom/facebook/search/protocol/BatchedSearchLoader$1;->a:LX/9yN;

    .line 1596102
    const/4 v1, 0x0

    .line 1596103
    iget-object v2, v0, LX/9yN;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9yM;

    .line 1596104
    if-eqz v2, :cond_0

    .line 1596105
    :goto_1
    iget-object v4, v1, LX/9yM;->a:LX/9yL;

    invoke-interface {v4}, LX/9yL;->a()LX/0zO;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v4

    iget-object v5, v0, LX/9yN;->c:Ljava/util/concurrent/Executor;

    invoke-virtual {v4, v5}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v4

    new-instance v5, LX/9yK;

    invoke-direct {v5, v0, v1}, LX/9yK;-><init>(LX/9yN;LX/9yM;)V

    invoke-virtual {v4, v5}, LX/0zX;->a(LX/0rl;)LX/0zi;

    goto :goto_0

    .line 1596106
    :cond_0
    new-instance v2, LX/0v6;

    const-string v4, "BatchedSearchLoader"

    invoke-direct {v2, v4}, LX/0v6;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1596107
    :cond_1
    if-nez v2, :cond_2

    .line 1596108
    :goto_2
    iget-object v0, p0, Lcom/facebook/search/protocol/BatchedSearchLoader$1;->a:LX/9yN;

    iget-object v0, v0, LX/9yN;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/search/protocol/BatchedSearchLoader$1;->a:LX/9yN;

    iget-object v1, v1, LX/9yN;->f:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/facebook/search/protocol/BatchedSearchLoader$1;->a:LX/9yN;

    iget v2, v2, LX/9yN;->e:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    const v4, 0x3d8ec209

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1596109
    return-void

    .line 1596110
    :cond_2
    iget-object v1, v0, LX/9yN;->a:LX/0tX;

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0v6;)V

    goto :goto_2
.end method
