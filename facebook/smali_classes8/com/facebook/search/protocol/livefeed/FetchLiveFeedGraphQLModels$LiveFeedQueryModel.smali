.class public final Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x64fa32c2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1610337
    const-class v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1610300
    const-class v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1610335
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1610336
    return-void
.end method

.method private j()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610333
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->f:Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->f:Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    .line 1610334
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->f:Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1610321
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1610322
    invoke-virtual {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1610323
    invoke-direct {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->j()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1610324
    invoke-virtual {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1610325
    invoke-virtual {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1610326
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1610327
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1610328
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1610329
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1610330
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1610331
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1610332
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1610313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1610314
    invoke-direct {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->j()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1610315
    invoke-direct {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->j()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    .line 1610316
    invoke-direct {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->j()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1610317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;

    .line 1610318
    iput-object v0, v1, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->f:Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    .line 1610319
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1610320
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610311
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->e:Ljava/lang/String;

    .line 1610312
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1610308
    new-instance v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;-><init>()V

    .line 1610309
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1610310
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610307
    invoke-direct {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->j()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610305
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->g:Ljava/lang/String;

    .line 1610306
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1610303
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->h:Ljava/lang/String;

    .line 1610304
    iget-object v0, p0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1610302
    const v0, 0x4b80756e    # 1.683734E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1610301
    const v0, -0x1bce060e

    return v0
.end method
