.class public final Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1596255
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;

    new-instance v1, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1596256
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1596258
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1596259
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1596260
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1596261
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1596262
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1596263
    if-eqz v2, :cond_0

    .line 1596264
    const-string p0, "external_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1596265
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1596266
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1596267
    if-eqz v2, :cond_1

    .line 1596268
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1596269
    invoke-static {v1, v2, p1}, LX/9yS;->a(LX/15i;ILX/0nX;)V

    .line 1596270
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1596271
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1596257
    check-cast p1, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel$Serializer;->a(Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateExternalUrlModel;LX/0nX;LX/0my;)V

    return-void
.end method
