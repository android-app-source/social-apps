.class public final Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1598788
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;

    new-instance v1, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1598789
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1598769
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1598771
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1598772
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1598773
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1598774
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1598775
    if-eqz v2, :cond_0

    .line 1598776
    const-string p0, "external_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598777
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1598778
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1598779
    if-eqz v2, :cond_1

    .line 1598780
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598781
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1598782
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1598783
    if-eqz v2, :cond_2

    .line 1598784
    const-string p0, "link_media"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598785
    invoke-static {v1, v2, p1, p2}, LX/9yy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1598786
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1598787
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1598770
    check-cast p1, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Serializer;->a(Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
