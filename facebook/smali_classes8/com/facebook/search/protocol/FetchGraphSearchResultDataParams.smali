.class public Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1599925
    new-instance v0, LX/9zA;

    invoke-direct {v0}, LX/9zA;-><init>()V

    sput-object v0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/9zB;)V
    .locals 1

    .prologue
    .line 1599926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1599927
    iget-object v0, p1, LX/9zB;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1599928
    iput-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->a:Ljava/lang/String;

    .line 1599929
    iget v0, p1, LX/9zB;->d:I

    move v0, v0

    .line 1599930
    iput v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->b:I

    .line 1599931
    iget-object v0, p1, LX/9zB;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1599932
    iput-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->c:Ljava/lang/String;

    .line 1599933
    iget-object v0, p1, LX/9zB;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1599934
    iput-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->d:Ljava/lang/String;

    .line 1599935
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1599936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1599937
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->a:Ljava/lang/String;

    .line 1599938
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->b:I

    .line 1599939
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->c:Ljava/lang/String;

    .line 1599940
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->d:Ljava/lang/String;

    .line 1599941
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1599942
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1599943
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1599944
    iget v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1599945
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1599946
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1599947
    return-void
.end method
