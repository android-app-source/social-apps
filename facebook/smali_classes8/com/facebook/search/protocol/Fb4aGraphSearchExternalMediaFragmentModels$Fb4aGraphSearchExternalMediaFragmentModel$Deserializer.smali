.class public final Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1598676
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;

    new-instance v1, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1598677
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1598678
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1598679
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1598680
    const/4 v2, 0x0

    .line 1598681
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1598682
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1598683
    :goto_0
    move v1, v2

    .line 1598684
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1598685
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1598686
    new-instance v1, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;

    invoke-direct {v1}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel;-><init>()V

    .line 1598687
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1598688
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1598689
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1598690
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1598691
    :cond_0
    return-object v1

    .line 1598692
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1598693
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_5

    .line 1598694
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1598695
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1598696
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 1598697
    const-string p0, "external_url"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1598698
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1598699
    :cond_3
    const-string p0, "id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1598700
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1598701
    :cond_4
    const-string p0, "link_media"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1598702
    invoke-static {p1, v0}, LX/9yy;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1598703
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1598704
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1598705
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1598706
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1598707
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1
.end method
