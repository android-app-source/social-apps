.class public final Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x766d7242
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603800
    const-class v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603799
    const-class v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603797
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1603798
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603766
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1603767
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1603768
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603795
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1603796
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603793
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1603794
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTrendingTopicData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603791
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    .line 1603792
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603789
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 1603790
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603787
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1603788
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1603801
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603802
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1603803
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1603804
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1603805
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->m()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1603806
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1603807
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1603808
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1603809
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1603810
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1603811
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1603812
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1603813
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1603814
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1603815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603816
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1603779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603780
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->m()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1603781
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->m()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    .line 1603782
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->m()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1603783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    .line 1603784
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel$TrendingTopicDataModel;

    .line 1603785
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603786
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1603778
    new-instance v0, LX/A04;

    invoke-direct {v0, p1}, LX/A04;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603777
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1603775
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1603776
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1603774
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1603771
    new-instance v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;-><init>()V

    .line 1603772
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1603773
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1603770
    const v0, -0x3915f503

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1603769
    const v0, 0x50c72189

    return v0
.end method
