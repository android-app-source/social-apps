.class public final Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ba95376
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1599481
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1599480
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1599478
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1599479
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1599470
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1599471
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1599472
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1599473
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1599474
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1599475
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1599476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1599477
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1599482
    iget-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->e:Ljava/util/List;

    .line 1599483
    iget-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1599457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1599458
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1599459
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1599460
    if-eqz v1, :cond_2

    .line 1599461
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    .line 1599462
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1599463
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1599464
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1599465
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1599466
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    .line 1599467
    iput-object v0, v1, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1599468
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1599469
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1599454
    new-instance v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;-><init>()V

    .line 1599455
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1599456
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1599453
    const v0, 0x6c3ca9b2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1599452
    const v0, 0x5e300d9e

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1599450
    iget-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1599451
    iget-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    return-object v0
.end method
