.class public final Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6f49e693
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603865
    const-class v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1603866
    const-class v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603869
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1603870
    return-void
.end method

.method private a()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603867
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->e:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->e:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    .line 1603868
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->e:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603861
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->f:Ljava/lang/String;

    .line 1603862
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getQuery"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603863
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1603864
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1603859
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->h:Ljava/lang/String;

    .line 1603860
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1603847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603848
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1603849
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1603850
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x4a757587

    invoke-static {v3, v2, v4}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1603851
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1603852
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1603853
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1603854
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1603855
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1603856
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1603857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603858
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1603832
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1603833
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1603834
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    .line 1603835
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1603836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;

    .line 1603837
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->e:Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel$NodeModel;

    .line 1603838
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1603839
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4a757587

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1603840
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1603841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;

    .line 1603842
    iput v3, v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->g:I

    move-object v1, v0

    .line 1603843
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1603844
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1603845
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1603846
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1603829
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1603830
    const/4 v0, 0x2

    const v1, -0x4a757587

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;->g:I

    .line 1603831
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1603826
    new-instance v0, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTrendingEntitiesGraphQLModels$FetchTrendingEntitiesModel$TrendingEntitiesModel$EdgesModel;-><init>()V

    .line 1603827
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1603828
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1603825
    const v0, -0x50680d8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1603824
    const v0, -0x30b850fd

    return v0
.end method
