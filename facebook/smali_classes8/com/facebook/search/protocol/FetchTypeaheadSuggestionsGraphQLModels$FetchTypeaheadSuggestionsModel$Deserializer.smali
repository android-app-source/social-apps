.class public final Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$FetchTypeaheadSuggestionsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1604487
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$FetchTypeaheadSuggestionsModel;

    new-instance v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$FetchTypeaheadSuggestionsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$FetchTypeaheadSuggestionsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1604488
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1604486
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1604443
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1604444
    const/4 v2, 0x0

    .line 1604445
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1604446
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1604447
    :goto_0
    move v1, v2

    .line 1604448
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1604449
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1604450
    new-instance v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$FetchTypeaheadSuggestionsModel;

    invoke-direct {v1}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$FetchTypeaheadSuggestionsModel;-><init>()V

    .line 1604451
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1604452
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1604453
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1604454
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1604455
    :cond_0
    return-object v1

    .line 1604456
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1604457
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1604458
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1604459
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1604460
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1604461
    const-string v4, "suggestions"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1604462
    const/4 v3, 0x0

    .line 1604463
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1604464
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1604465
    :goto_2
    move v1, v3

    .line 1604466
    goto :goto_1

    .line 1604467
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1604468
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1604469
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1604470
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1604471
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_8

    .line 1604472
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1604473
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1604474
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1604475
    const-string p0, "edges"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1604476
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1604477
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_7

    .line 1604478
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1604479
    invoke-static {p1, v0}, LX/A0M;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1604480
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1604481
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1604482
    goto :goto_3

    .line 1604483
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1604484
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1604485
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3
.end method
