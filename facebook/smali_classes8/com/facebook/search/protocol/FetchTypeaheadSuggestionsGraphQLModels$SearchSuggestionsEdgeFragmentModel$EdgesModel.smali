.class public final Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x15626adb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604723
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1604724
    const-class v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1604725
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1604726
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604727
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->e:Ljava/lang/String;

    .line 1604728
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604729
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->f:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->f:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604730
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->f:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1604731
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->g:Ljava/lang/String;

    .line 1604732
    iget-object v0, p0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1604733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604734
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1604735
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1604736
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1604737
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1604738
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1604739
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1604740
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1604741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604742
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1604743
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1604744
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1604745
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604746
    invoke-direct {p0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1604747
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;

    .line 1604748
    iput-object v0, v1, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;->f:Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionUnitModel;

    .line 1604749
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1604750
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1604751
    new-instance v0, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FetchTypeaheadSuggestionsGraphQLModels$SearchSuggestionsEdgeFragmentModel$EdgesModel;-><init>()V

    .line 1604752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1604753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1604754
    const v0, 0x1fe22147

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1604755
    const v0, 0x272fe664

    return v0
.end method
