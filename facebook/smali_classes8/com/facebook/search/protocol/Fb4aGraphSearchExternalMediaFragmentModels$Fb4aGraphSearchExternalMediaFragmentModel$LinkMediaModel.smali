.class public final Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x629fb890
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1598763
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1598766
    const-class v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1598764
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1598765
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1598760
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1598761
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1598762
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1598752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1598753
    invoke-direct {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1598754
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x13e3c657

    invoke-static {v2, v1, v3}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1598755
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1598756
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1598757
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1598758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1598759
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1598742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1598743
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1598744
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x13e3c657

    invoke-static {v2, v0, v3}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1598745
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1598746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;

    .line 1598747
    iput v3, v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->f:I

    .line 1598748
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1598749
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1598750
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1598751
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1598767
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1598768
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1598741
    new-instance v0, LX/9yw;

    invoke-direct {v0, p1}, LX/9yw;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1598738
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1598739
    const/4 v0, 0x1

    const v1, 0x13e3c657

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->f:I

    .line 1598740
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1598736
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1598737
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1598735
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1598730
    new-instance v0, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;-><init>()V

    .line 1598731
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1598732
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1598734
    const v0, -0x24d8c172

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1598733
    const v0, 0x46c7fc4

    return v0
.end method
