.class public final Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4e7095af
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1596657
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1596656
    const-class v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1596654
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1596655
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1596648
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1596649
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1596650
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1596651
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1596652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1596653
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1596640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1596641
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1596642
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    .line 1596643
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1596644
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;

    .line 1596645
    iput-object v0, v1, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->e:Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    .line 1596646
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1596647
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProvider"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1596633
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->e:Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    iput-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->e:Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    .line 1596634
    iget-object v0, p0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;->e:Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1596637
    new-instance v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel;-><init>()V

    .line 1596638
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1596639
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1596636
    const v0, -0x114264ae

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1596635
    const v0, -0x5bc5692d

    return v0
.end method
