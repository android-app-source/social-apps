.class public final Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/A3Q;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xeb5bc94
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617566
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617565
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1617563
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617564
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617561
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->e:Ljava/lang/String;

    .line 1617562
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617537
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->f:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->f:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1617538
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->f:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1617553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617554
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1617555
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->k()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1617556
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1617557
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1617558
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1617559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617560
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1617545
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617546
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->k()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1617547
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->k()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1617548
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->k()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1617549
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;

    .line 1617550
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->f:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1617551
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617552
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617544
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1617541
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel;-><init>()V

    .line 1617542
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617543
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1617540
    const v0, -0x2e43fd00

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1617539
    const v0, 0x1eaef984

    return v0
.end method
