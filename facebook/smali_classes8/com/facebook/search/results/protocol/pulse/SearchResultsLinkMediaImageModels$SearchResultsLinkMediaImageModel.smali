.class public final Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/A3S;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1699aff9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1619807
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1619806
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1619804
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1619805
    return-void
.end method

.method private a()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1619802
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->e:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->e:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    .line 1619803
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->e:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1619782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1619783
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1619784
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1619785
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1619786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1619787
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1619794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1619795
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1619796
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    .line 1619797
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1619798
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;

    .line 1619799
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->e:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    .line 1619800
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1619801
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1619791
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;-><init>()V

    .line 1619792
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1619793
    return-object v0
.end method

.method public final synthetic bS()LX/A4E;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1619790
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1619789
    const v0, 0x148f6650

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1619788
    const v0, 0x1eaef984

    return v0
.end method
