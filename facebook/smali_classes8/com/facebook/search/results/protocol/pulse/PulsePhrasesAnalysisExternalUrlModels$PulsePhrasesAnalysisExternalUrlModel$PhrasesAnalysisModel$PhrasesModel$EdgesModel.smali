.class public final Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x56c22a2c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617402
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617401
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1617399
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617400
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1617396
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617397
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617398
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;)Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;
    .locals 8

    .prologue
    .line 1617376
    if-nez p0, :cond_0

    .line 1617377
    const/4 p0, 0x0

    .line 1617378
    :goto_0
    return-object p0

    .line 1617379
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1617380
    check-cast p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;

    goto :goto_0

    .line 1617381
    :cond_1
    new-instance v0, LX/A3o;

    invoke-direct {v0}, LX/A3o;-><init>()V

    .line 1617382
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;->a(Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;)Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v1

    iput-object v1, v0, LX/A3o;->a:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    .line 1617383
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1617384
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1617385
    iget-object v3, v0, LX/A3o;->a:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1617386
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1617387
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1617388
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1617389
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1617390
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1617391
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1617392
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1617393
    new-instance v3, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;-><init>(LX/15i;)V

    .line 1617394
    move-object p0, v3

    .line 1617395
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617403
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    .line 1617404
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1617370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617371
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1617372
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1617373
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1617374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1617362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617363
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1617364
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    .line 1617365
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1617366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;

    .line 1617367
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    .line 1617368
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617369
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617361
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1617358
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel;-><init>()V

    .line 1617359
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617360
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1617357
    const v0, 0x416c0bee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1617356
    const v0, -0x3885f779

    return v0
.end method
