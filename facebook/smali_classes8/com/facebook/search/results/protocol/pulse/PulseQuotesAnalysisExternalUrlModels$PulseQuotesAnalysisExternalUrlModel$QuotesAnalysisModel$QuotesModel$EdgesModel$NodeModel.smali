.class public final Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2e20bef6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617947
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617948
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1617949
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617950
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1617951
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617952
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617953
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;)Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;
    .locals 8

    .prologue
    .line 1617954
    if-nez p0, :cond_0

    .line 1617955
    const/4 p0, 0x0

    .line 1617956
    :goto_0
    return-object p0

    .line 1617957
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 1617958
    check-cast p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    goto :goto_0

    .line 1617959
    :cond_1
    new-instance v0, LX/A47;

    invoke-direct {v0}, LX/A47;-><init>()V

    .line 1617960
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->a()I

    move-result v1

    iput v1, v0, LX/A47;->a:I

    .line 1617961
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A47;->b:Ljava/lang/String;

    .line 1617962
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1617963
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1617964
    iget-object v3, v0, LX/A47;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1617965
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1617966
    iget v5, v0, LX/A47;->a:I

    invoke-virtual {v2, v7, v5, v7}, LX/186;->a(III)V

    .line 1617967
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1617968
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1617969
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1617970
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1617971
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1617972
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1617973
    new-instance v3, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;-><init>(LX/15i;)V

    .line 1617974
    move-object p0, v3

    .line 1617975
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1617938
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1617939
    iget v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1617940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617941
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1617942
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1617943
    iget v1, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1617944
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1617945
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617946
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1617935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617936
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617937
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1617932
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1617933
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->e:I

    .line 1617934
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1617929
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;-><init>()V

    .line 1617930
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617931
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617927
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1617928
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1617926
    const v0, 0x160decea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1617925
    const v0, 0x78647b46

    return v0
.end method
