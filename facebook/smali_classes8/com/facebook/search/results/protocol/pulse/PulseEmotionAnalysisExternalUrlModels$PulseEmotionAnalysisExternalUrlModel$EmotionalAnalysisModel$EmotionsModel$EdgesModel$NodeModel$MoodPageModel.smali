.class public final Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1616410
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1616439
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1616437
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1616438
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1616434
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1616435
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1616436
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;)Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;
    .locals 9

    .prologue
    .line 1616411
    if-nez p0, :cond_0

    .line 1616412
    const/4 p0, 0x0

    .line 1616413
    :goto_0
    return-object p0

    .line 1616414
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    if-eqz v0, :cond_1

    .line 1616415
    check-cast p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    goto :goto_0

    .line 1616416
    :cond_1
    new-instance v0, LX/A3b;

    invoke-direct {v0}, LX/A3b;-><init>()V

    .line 1616417
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A3b;->a:Ljava/lang/String;

    .line 1616418
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A3b;->b:Ljava/lang/String;

    .line 1616419
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1616420
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1616421
    iget-object v3, v0, LX/A3b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1616422
    iget-object v5, v0, LX/A3b;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1616423
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1616424
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1616425
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1616426
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1616427
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1616428
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1616429
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1616430
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1616431
    new-instance v3, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;-><init>(LX/15i;)V

    .line 1616432
    move-object p0, v3

    .line 1616433
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1616402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1616403
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1616404
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1616405
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1616406
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1616407
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1616408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1616409
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1616399
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1616400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1616401
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1616440
    new-instance v0, LX/A3c;

    invoke-direct {v0, p1}, LX/A3c;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616398
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1616396
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1616397
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1616395
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1616386
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;-><init>()V

    .line 1616387
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1616388
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616393
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->e:Ljava/lang/String;

    .line 1616394
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616391
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->f:Ljava/lang/String;

    .line 1616392
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1616390
    const v0, 0x6b25c1f0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1616389
    const v0, 0x25d6af

    return v0
.end method
