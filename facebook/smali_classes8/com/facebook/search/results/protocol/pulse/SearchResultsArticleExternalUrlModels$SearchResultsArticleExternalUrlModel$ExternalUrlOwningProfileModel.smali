.class public final Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x684bc2e9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1618479
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1618478
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1618476
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1618477
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1618473
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1618474
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1618475
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1618471
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    .line 1618472
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1618463
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1618464
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1618465
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1618466
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1618467
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1618468
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1618469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1618470
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1618480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1618481
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1618482
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    .line 1618483
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1618484
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;

    .line 1618485
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    .line 1618486
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1618487
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1618462
    new-instance v0, LX/A4L;

    invoke-direct {v0, p1}, LX/A4L;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1618460
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1618461
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1618459
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1618456
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;-><init>()V

    .line 1618457
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1618458
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1618455
    const v0, 0x103fddcb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1618454
    const v0, 0x50c72189

    return v0
.end method
