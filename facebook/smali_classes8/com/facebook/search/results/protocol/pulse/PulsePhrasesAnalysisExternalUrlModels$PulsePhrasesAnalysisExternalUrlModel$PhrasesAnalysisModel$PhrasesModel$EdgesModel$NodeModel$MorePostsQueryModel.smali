.class public final Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6ba784f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617064
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1617086
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1617087
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617088
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1617089
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1617090
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617091
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;)Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;
    .locals 13

    .prologue
    .line 1617106
    if-nez p0, :cond_0

    .line 1617107
    const/4 p0, 0x0

    .line 1617108
    :goto_0
    return-object p0

    .line 1617109
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;

    if-eqz v0, :cond_1

    .line 1617110
    check-cast p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;

    goto :goto_0

    .line 1617111
    :cond_1
    new-instance v1, LX/A3q;

    invoke-direct {v1}, LX/A3q;-><init>()V

    .line 1617112
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/A3q;->a:Ljava/lang/String;

    .line 1617113
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/A3q;->b:Ljava/lang/String;

    .line 1617114
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->d()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;->a(Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;)Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v0

    iput-object v0, v1, LX/A3q;->c:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    .line 1617115
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1617116
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1617117
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1617118
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1617119
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/A3q;->d:LX/0Px;

    .line 1617120
    const/4 v8, 0x1

    const/4 v12, 0x0

    const/4 v6, 0x0

    .line 1617121
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1617122
    iget-object v5, v1, LX/A3q;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1617123
    iget-object v7, v1, LX/A3q;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1617124
    iget-object v9, v1, LX/A3q;->c:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1617125
    iget-object v10, v1, LX/A3q;->d:LX/0Px;

    invoke-virtual {v4, v10}, LX/186;->c(Ljava/util/List;)I

    move-result v10

    .line 1617126
    const/4 v11, 0x4

    invoke-virtual {v4, v11}, LX/186;->c(I)V

    .line 1617127
    invoke-virtual {v4, v12, v5}, LX/186;->b(II)V

    .line 1617128
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1617129
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1617130
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1617131
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1617132
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1617133
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1617134
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1617135
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1617136
    new-instance v5, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;

    invoke-direct {v5, v4}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;-><init>(LX/15i;)V

    .line 1617137
    move-object p0, v5

    .line 1617138
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617092
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->g:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->g:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    .line 1617093
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->g:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1617094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617095
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1617096
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1617097
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1617098
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->c(Ljava/util/List;)I

    move-result v3

    .line 1617099
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1617100
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1617101
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1617102
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1617103
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1617104
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617105
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1617077
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617078
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1617079
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    .line 1617080
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1617081
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;

    .line 1617082
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->g:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    .line 1617083
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617084
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617085
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1617074
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;-><init>()V

    .line 1617075
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617076
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617072
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->e:Ljava/lang/String;

    .line 1617073
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617070
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->f:Ljava/lang/String;

    .line 1617071
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617069
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->j()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel$QueryTitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1617068
    const v0, -0x495ad8ee

    return v0
.end method

.method public final e()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1617066
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->h:Ljava/util/List;

    .line 1617067
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel$PhrasesModel$EdgesModel$NodeModel$MorePostsQueryModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1617065
    const v0, -0x1bce060e

    return v0
.end method
