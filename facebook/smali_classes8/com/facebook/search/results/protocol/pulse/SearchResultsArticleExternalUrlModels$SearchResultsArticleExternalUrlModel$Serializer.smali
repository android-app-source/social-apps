.class public final Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1618867
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;

    new-instance v1, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1618868
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1618869
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1618870
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1618871
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 1618872
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1618873
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618874
    if-eqz v2, :cond_0

    .line 1618875
    const-string v3, "all_share_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618876
    invoke-static {v1, v2, p1}, LX/A4T;->a(LX/15i;ILX/0nX;)V

    .line 1618877
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1618878
    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1618879
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618880
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1618881
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1618882
    if-eqz v2, :cond_2

    .line 1618883
    const-string v3, "external_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618884
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1618885
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618886
    if-eqz v2, :cond_3

    .line 1618887
    const-string v3, "external_url_owning_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618888
    invoke-static {v1, v2, p1, p2}, LX/A4V;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1618889
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1618890
    if-eqz v2, :cond_4

    .line 1618891
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618892
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1618893
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618894
    if-eqz v2, :cond_5

    .line 1618895
    const-string v3, "instant_article"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618896
    invoke-static {v1, v2, p1, p2}, LX/A4Y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1618897
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618898
    if-eqz v2, :cond_6

    .line 1618899
    const-string v3, "link_media"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618900
    invoke-static {v1, v2, p1, p2}, LX/A4a;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1618901
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618902
    if-eqz v2, :cond_7

    .line 1618903
    const-string v3, "open_graph_node"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618904
    invoke-static {v1, v2, p1}, LX/A4b;->a(LX/15i;ILX/0nX;)V

    .line 1618905
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618906
    if-eqz v2, :cond_8

    .line 1618907
    const-string v3, "source"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618908
    invoke-static {v1, v2, p1}, LX/A4c;->a(LX/15i;ILX/0nX;)V

    .line 1618909
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618910
    if-eqz v2, :cond_9

    .line 1618911
    const-string v3, "summary"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618912
    invoke-static {v1, v2, p1}, LX/A4d;->a(LX/15i;ILX/0nX;)V

    .line 1618913
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618914
    if-eqz v2, :cond_a

    .line 1618915
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618916
    invoke-static {v1, v2, p1}, LX/A4e;->a(LX/15i;ILX/0nX;)V

    .line 1618917
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1618918
    if-eqz v2, :cond_b

    .line 1618919
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618920
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1618921
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1618922
    if-eqz v2, :cond_c

    .line 1618923
    const-string v3, "video_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1618924
    invoke-static {v1, v2, p1}, LX/A4f;->a(LX/15i;ILX/0nX;)V

    .line 1618925
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1618926
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1618927
    check-cast p1, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$Serializer;->a(Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;LX/0nX;LX/0my;)V

    return-void
.end method
