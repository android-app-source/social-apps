.class public final Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/A4G;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1618971
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1618970
    const-class v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1618968
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1618969
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1618965
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1618966
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1618967
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1618959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1618960
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1618961
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1618962
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1618963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1618964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1618956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1618957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1618958
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1618954
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;->e:Ljava/lang/String;

    .line 1618955
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1618949
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;-><init>()V

    .line 1618950
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1618951
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1618953
    const v0, 0x603b05c1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1618952
    const v0, -0x726d476c

    return v0
.end method
