.class public final Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x39f78d87
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1618031
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1618030
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1618028
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1618029
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1618025
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1618026
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1618027
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;)Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;
    .locals 8

    .prologue
    .line 1618005
    if-nez p0, :cond_0

    .line 1618006
    const/4 p0, 0x0

    .line 1618007
    :goto_0
    return-object p0

    .line 1618008
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1618009
    check-cast p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;

    goto :goto_0

    .line 1618010
    :cond_1
    new-instance v0, LX/A46;

    invoke-direct {v0}, LX/A46;-><init>()V

    .line 1618011
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;->a(Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;)Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v1

    iput-object v1, v0, LX/A46;->a:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    .line 1618012
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1618013
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1618014
    iget-object v3, v0, LX/A46;->a:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1618015
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1618016
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1618017
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1618018
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1618019
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1618020
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1618021
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1618022
    new-instance v3, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;-><init>(LX/15i;)V

    .line 1618023
    move-object p0, v3

    .line 1618024
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617997
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    .line 1617998
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1617999
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1618000
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1618001
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1618002
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1618003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1618004
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1617989
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1617990
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1617991
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    .line 1617992
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1617993
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;

    .line 1617994
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->e:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    .line 1617995
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1617996
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1617988
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1617985
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel$QuotesModel$EdgesModel;-><init>()V

    .line 1617986
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1617987
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1617984
    const v0, -0x43dd92fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1617983
    const v0, -0x77e6eeb6

    return v0
.end method
