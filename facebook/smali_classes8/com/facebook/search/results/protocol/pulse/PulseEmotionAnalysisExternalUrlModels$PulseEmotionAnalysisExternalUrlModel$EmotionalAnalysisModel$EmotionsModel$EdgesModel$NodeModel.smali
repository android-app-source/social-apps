.class public final Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e63aba9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1616448
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1616508
    const-class v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1616506
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1616507
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1616503
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1616504
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1616505
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;)Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;
    .locals 9

    .prologue
    .line 1616478
    if-nez p0, :cond_0

    .line 1616479
    const/4 p0, 0x0

    .line 1616480
    :goto_0
    return-object p0

    .line 1616481
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 1616482
    check-cast p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;

    goto :goto_0

    .line 1616483
    :cond_1
    new-instance v0, LX/A3Z;

    invoke-direct {v0}, LX/A3Z;-><init>()V

    .line 1616484
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->a()I

    move-result v1

    iput v1, v0, LX/A3Z;->a:I

    .line 1616485
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->b()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;->a(Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;)Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v1

    iput-object v1, v0, LX/A3Z;->b:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    .line 1616486
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->c()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;->a(Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;)Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v1

    iput-object v1, v0, LX/A3Z;->c:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    .line 1616487
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1616488
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1616489
    iget-object v3, v0, LX/A3Z;->b:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1616490
    iget-object v5, v0, LX/A3Z;->c:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1616491
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1616492
    iget v7, v0, LX/A3Z;->a:I

    invoke-virtual {v2, v8, v7, v8}, LX/186;->a(III)V

    .line 1616493
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1616494
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1616495
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1616496
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1616497
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1616498
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1616499
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1616500
    new-instance v3, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;-><init>(LX/15i;)V

    .line 1616501
    move-object p0, v3

    .line 1616502
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616476
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    .line 1616477
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616474
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    .line 1616475
    iget-object v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1616472
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1616473
    iget v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1616509
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1616510
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1616511
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1616512
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1616513
    iget v2, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 1616514
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1616515
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1616516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1616517
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1616459
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1616460
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1616461
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    .line 1616462
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1616463
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;

    .line 1616464
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    .line 1616465
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1616466
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    .line 1616467
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1616468
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;

    .line 1616469
    iput-object v0, v1, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    .line 1616470
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1616471
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1616456
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1616457
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->e:I

    .line 1616458
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1616453
    new-instance v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;-><init>()V

    .line 1616454
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1616455
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616452
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$IconImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1616451
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel;->k()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel$EmotionsModel$EdgesModel$NodeModel$MoodPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1616450
    const v0, -0x65cf1325

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1616449
    const v0, -0x65cdff2b

    return v0
.end method
