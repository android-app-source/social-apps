.class public final Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x11809337
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612674
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612726
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1612724
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1612725
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1612721
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1612722
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1612723
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 8

    .prologue
    .line 1612698
    if-nez p0, :cond_0

    .line 1612699
    const/4 p0, 0x0

    .line 1612700
    :goto_0
    return-object p0

    .line 1612701
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    if-eqz v0, :cond_1

    .line 1612702
    check-cast p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    goto :goto_0

    .line 1612703
    :cond_1
    new-instance v0, LX/A26;

    invoke-direct {v0}, LX/A26;-><init>()V

    .line 1612704
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a()I

    move-result v1

    iput v1, v0, LX/A26;->a:I

    .line 1612705
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->b()I

    move-result v1

    iput v1, v0, LX/A26;->b:I

    .line 1612706
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->c()I

    move-result v1

    iput v1, v0, LX/A26;->c:I

    .line 1612707
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1612708
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1612709
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 1612710
    iget v3, v0, LX/A26;->a:I

    invoke-virtual {v2, v7, v3, v7}, LX/186;->a(III)V

    .line 1612711
    iget v3, v0, LX/A26;->b:I

    invoke-virtual {v2, v6, v3, v7}, LX/186;->a(III)V

    .line 1612712
    const/4 v3, 0x2

    iget v5, v0, LX/A26;->c:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1612713
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1612714
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1612715
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1612716
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1612717
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1612718
    new-instance v3, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;-><init>(LX/15i;)V

    .line 1612719
    move-object p0, v3

    .line 1612720
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1612696
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1612697
    iget v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1612689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1612690
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1612691
    iget v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 1612692
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1612693
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1612694
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612695
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1612727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1612728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612729
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1612684
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1612685
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->e:I

    .line 1612686
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->f:I

    .line 1612687
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->g:I

    .line 1612688
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1612682
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1612683
    iget v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1612679
    new-instance v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;-><init>()V

    .line 1612680
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1612681
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1612677
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1612678
    iget v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1612676
    const v0, 0x46466bbc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1612675
    const v0, 0x2063ce

    return v0
.end method
