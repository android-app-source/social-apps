.class public final Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1612998
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel;

    new-instance v1, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1612999
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1613041
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1613000
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1613001
    const/4 v2, 0x0

    .line 1613002
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 1613003
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1613004
    :goto_0
    move v1, v2

    .line 1613005
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1613006
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1613007
    new-instance v1, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel;-><init>()V

    .line 1613008
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1613009
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1613010
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1613011
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1613012
    :cond_0
    return-object v1

    .line 1613013
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1613014
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_9

    .line 1613015
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1613016
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1613017
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 1613018
    const-string p0, "cover_photo"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1613019
    invoke-static {p1, v0}, LX/8g0;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1613020
    :cond_3
    const-string p0, "id"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1613021
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1613022
    :cond_4
    const-string p0, "location"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1613023
    invoke-static {p1, v0}, LX/A2M;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1613024
    :cond_5
    const-string p0, "name"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1613025
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1613026
    :cond_6
    const-string p0, "weather_condition"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1613027
    invoke-static {p1, v0}, LX/A2N;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1613028
    :cond_7
    const-string p0, "weather_hourly_forecast"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1613029
    invoke-static {p1, v0}, LX/A2O;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1613030
    :cond_8
    const-string p0, "weather_url"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1613031
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1613032
    :cond_9
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1613033
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1613034
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1613035
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1613036
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1613037
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1613038
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1613039
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1613040
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
