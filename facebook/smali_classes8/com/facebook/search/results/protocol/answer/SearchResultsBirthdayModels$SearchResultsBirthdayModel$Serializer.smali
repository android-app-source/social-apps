.class public final Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1612873
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;

    new-instance v1, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1612874
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1612872
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1612842
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1612843
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1612844
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1612845
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1612846
    if-eqz v2, :cond_0

    .line 1612847
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612848
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1612849
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1612850
    if-eqz v2, :cond_1

    .line 1612851
    const-string p0, "birthdate"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612852
    invoke-static {v1, v2, p1}, LX/A2A;->a(LX/15i;ILX/0nX;)V

    .line 1612853
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1612854
    if-eqz v2, :cond_2

    .line 1612855
    const-string p0, "celebrity_basic_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612856
    invoke-static {v1, v2, p1, p2}, LX/A2B;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1612857
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1612858
    if-eqz v2, :cond_3

    .line 1612859
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612860
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1612861
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1612862
    if-eqz v2, :cond_4

    .line 1612863
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612864
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1612865
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1612866
    if-eqz v2, :cond_5

    .line 1612867
    const-string p0, "short_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612868
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1612869
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1612870
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1612871
    check-cast p1, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Serializer;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;LX/0nX;LX/0my;)V

    return-void
.end method
