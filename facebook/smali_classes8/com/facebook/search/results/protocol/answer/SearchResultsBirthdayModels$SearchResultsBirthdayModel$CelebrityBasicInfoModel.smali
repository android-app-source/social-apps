.class public final Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6272298d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612796
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1612795
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1612793
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1612794
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1612790
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1612791
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1612792
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;
    .locals 2

    .prologue
    .line 1612782
    if-nez p0, :cond_0

    .line 1612783
    const/4 p0, 0x0

    .line 1612784
    :goto_0
    return-object p0

    .line 1612785
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    if-eqz v0, :cond_1

    .line 1612786
    check-cast p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    goto :goto_0

    .line 1612787
    :cond_1
    new-instance v0, LX/A27;

    invoke-direct {v0}, LX/A27;-><init>()V

    .line 1612788
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/A27;->a:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1612789
    invoke-virtual {v0}, LX/A27;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612797
    iget-object v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->e:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->e:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1612798
    iget-object v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->e:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1612776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1612777
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->j()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1612778
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1612779
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1612780
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612781
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1612762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1612763
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->j()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1612764
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->j()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1612765
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->j()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1612766
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 1612767
    iput-object v0, v1, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->e:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1612768
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1612769
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1612775
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->j()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1612772
    new-instance v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;-><init>()V

    .line 1612773
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1612774
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1612771
    const v0, 0x6769efea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1612770
    const v0, -0x48185087

    return v0
.end method
