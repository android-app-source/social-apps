.class public final Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1612799
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;

    new-instance v1, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1612800
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1612801
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1612802
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1612803
    const/4 v2, 0x0

    .line 1612804
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 1612805
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1612806
    :goto_0
    move v1, v2

    .line 1612807
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1612808
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1612809
    new-instance v1, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel;-><init>()V

    .line 1612810
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1612811
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1612812
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1612813
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1612814
    :cond_0
    return-object v1

    .line 1612815
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1612816
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_9

    .line 1612817
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1612818
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1612819
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 1612820
    const-string p0, "__type__"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1612821
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 1612822
    :cond_4
    const-string p0, "birthdate"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1612823
    invoke-static {p1, v0}, LX/A2A;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1612824
    :cond_5
    const-string p0, "celebrity_basic_info"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1612825
    invoke-static {p1, v0}, LX/A2B;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1612826
    :cond_6
    const-string p0, "id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1612827
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1612828
    :cond_7
    const-string p0, "name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1612829
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1612830
    :cond_8
    const-string p0, "short_name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1612831
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1612832
    :cond_9
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1612833
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1612834
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1612835
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1612836
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1612837
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1612838
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1612839
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1
.end method
