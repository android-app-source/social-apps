.class public final Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x77e30f88
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1613446
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1613445
    const-class v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1613443
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1613444
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1613440
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1613441
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1613442
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;
    .locals 4

    .prologue
    .line 1613431
    if-nez p0, :cond_0

    .line 1613432
    const/4 p0, 0x0

    .line 1613433
    :goto_0
    return-object p0

    .line 1613434
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    if-eqz v0, :cond_1

    .line 1613435
    check-cast p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    goto :goto_0

    .line 1613436
    :cond_1
    new-instance v0, LX/A2K;

    invoke-direct {v0}, LX/A2K;-><init>()V

    .line 1613437
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A2K;->a:Ljava/lang/String;

    .line 1613438
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/A2K;->b:D

    .line 1613439
    invoke-virtual {v0}, LX/A2K;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1613424
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1613425
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1613426
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1613427
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1613428
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1613429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1613430
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1613447
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1613448
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1613449
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1613422
    iget-object v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->e:Ljava/lang/String;

    .line 1613423
    iget-object v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1613419
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1613420
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->f:D

    .line 1613421
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1613417
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1613418
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1613414
    new-instance v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;-><init>()V

    .line 1613415
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1613416
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1613413
    const v0, 0x6e1fb151

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1613412
    const v0, -0x48bd3755

    return v0
.end method
