.class public final Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1b370463
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615533
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615482
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1615531
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615532
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1615528
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615529
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615530
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;
    .locals 10

    .prologue
    .line 1615504
    if-nez p0, :cond_0

    .line 1615505
    const/4 p0, 0x0

    .line 1615506
    :goto_0
    return-object p0

    .line 1615507
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    if-eqz v0, :cond_1

    .line 1615508
    check-cast p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    goto :goto_0

    .line 1615509
    :cond_1
    new-instance v2, LX/A39;

    invoke-direct {v2}, LX/A39;-><init>()V

    .line 1615510
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1615511
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1615512
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1615513
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1615514
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/A39;->a:LX/0Px;

    .line 1615515
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1615516
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1615517
    iget-object v5, v2, LX/A39;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1615518
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1615519
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1615520
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1615521
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1615522
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1615523
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1615524
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1615525
    new-instance v5, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    invoke-direct {v5, v4}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;-><init>(LX/15i;)V

    .line 1615526
    move-object p0, v5

    .line 1615527
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1615498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615499
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1615500
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1615501
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1615502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615503
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1615496
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->e:Ljava/util/List;

    .line 1615497
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1615488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615489
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1615490
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1615491
    if-eqz v1, :cond_0

    .line 1615492
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    .line 1615493
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->e:Ljava/util/List;

    .line 1615494
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615495
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1615485
    new-instance v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;-><init>()V

    .line 1615486
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615487
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1615484
    const v0, 0x7b02734b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1615483
    const v0, 0x6b277d4f

    return v0
.end method
