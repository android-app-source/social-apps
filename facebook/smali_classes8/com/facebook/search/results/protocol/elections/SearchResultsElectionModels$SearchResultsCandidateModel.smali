.class public final Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3171ee91
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:D

.field private j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615409
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615410
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1615411
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615412
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1615458
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615459
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615460
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;
    .locals 13

    .prologue
    .line 1615413
    if-nez p0, :cond_0

    .line 1615414
    const/4 p0, 0x0

    .line 1615415
    :goto_0
    return-object p0

    .line 1615416
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    if-eqz v0, :cond_1

    .line 1615417
    check-cast p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    goto :goto_0

    .line 1615418
    :cond_1
    new-instance v0, LX/A38;

    invoke-direct {v0}, LX/A38;-><init>()V

    .line 1615419
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->a()I

    move-result v1

    iput v1, v0, LX/A38;->a:I

    .line 1615420
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A38;->b:Ljava/lang/String;

    .line 1615421
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/A38;->c:Z

    .line 1615422
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A38;->d:Ljava/lang/String;

    .line 1615423
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->e()D

    move-result-wide v2

    iput-wide v2, v0, LX/A38;->e:D

    .line 1615424
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->fv_()I

    move-result v1

    iput v1, v0, LX/A38;->f:I

    .line 1615425
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1615426
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1615427
    iget-object v5, v0, LX/A38;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1615428
    iget-object v6, v0, LX/A38;->d:Ljava/lang/String;

    invoke-virtual {v4, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1615429
    const/4 v7, 0x6

    invoke-virtual {v4, v7}, LX/186;->c(I)V

    .line 1615430
    iget v7, v0, LX/A38;->a:I

    invoke-virtual {v4, v10, v7, v10}, LX/186;->a(III)V

    .line 1615431
    invoke-virtual {v4, v12, v5}, LX/186;->b(II)V

    .line 1615432
    const/4 v5, 0x2

    iget-boolean v7, v0, LX/A38;->c:Z

    invoke-virtual {v4, v5, v7}, LX/186;->a(IZ)V

    .line 1615433
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v6}, LX/186;->b(II)V

    .line 1615434
    const/4 v5, 0x4

    iget-wide v6, v0, LX/A38;->e:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1615435
    const/4 v5, 0x5

    iget v6, v0, LX/A38;->f:I

    invoke-virtual {v4, v5, v6, v10}, LX/186;->a(III)V

    .line 1615436
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1615437
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1615438
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1615439
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1615440
    new-instance v4, LX/15i;

    move-object v6, v11

    move-object v7, v11

    move v8, v12

    move-object v9, v11

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1615441
    new-instance v5, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    invoke-direct {v5, v4}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;-><init>(LX/15i;)V

    .line 1615442
    move-object p0, v5

    .line 1615443
    goto/16 :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1615444
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615445
    iget v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1615446
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615447
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1615448
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1615449
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1615450
    iget v2, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->e:I

    invoke-virtual {p1, v6, v2, v6}, LX/186;->a(III)V

    .line 1615451
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1615452
    const/4 v0, 0x2

    iget-boolean v2, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->g:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1615453
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1615454
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->i:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1615455
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->j:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 1615456
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615457
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1615400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615402
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1615403
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1615404
    invoke-virtual {p1, p2, v4, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->e:I

    .line 1615405
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->g:Z

    .line 1615406
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->i:D

    .line 1615407
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->j:I

    .line 1615408
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1615397
    new-instance v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;-><init>()V

    .line 1615398
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615399
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615395
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->f:Ljava/lang/String;

    .line 1615396
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1615393
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615394
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->g:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615391
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->h:Ljava/lang/String;

    .line 1615392
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1615385
    const v0, 0xf4469f1

    return v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 1615389
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615390
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->i:D

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1615388
    const v0, 0x7c656482

    return v0
.end method

.method public final fv_()I
    .locals 2

    .prologue
    .line 1615386
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615387
    iget v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->j:I

    return v0
.end method
