.class public final Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x29d2d961
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615363
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615362
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1615360
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615361
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1615357
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615358
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615359
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;
    .locals 11

    .prologue
    .line 1615322
    if-nez p0, :cond_0

    .line 1615323
    const/4 p0, 0x0

    .line 1615324
    :goto_0
    return-object p0

    .line 1615325
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    if-eqz v0, :cond_1

    .line 1615326
    check-cast p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    goto :goto_0

    .line 1615327
    :cond_1
    new-instance v0, LX/A37;

    invoke-direct {v0}, LX/A37;-><init>()V

    .line 1615328
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->a()I

    move-result v1

    iput v1, v0, LX/A37;->a:I

    .line 1615329
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A37;->b:Ljava/lang/String;

    .line 1615330
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/A37;->c:Z

    .line 1615331
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A37;->d:Ljava/lang/String;

    .line 1615332
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A37;->e:Ljava/lang/String;

    .line 1615333
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->ft_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A37;->f:Ljava/lang/String;

    .line 1615334
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->fu_()I

    move-result v1

    iput v1, v0, LX/A37;->g:I

    .line 1615335
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v10, 0x0

    .line 1615336
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1615337
    iget-object v3, v0, LX/A37;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1615338
    iget-object v5, v0, LX/A37;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1615339
    iget-object v7, v0, LX/A37;->e:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1615340
    iget-object v8, v0, LX/A37;->f:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1615341
    const/4 v9, 0x7

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1615342
    iget v9, v0, LX/A37;->a:I

    invoke-virtual {v2, v10, v9, v10}, LX/186;->a(III)V

    .line 1615343
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1615344
    const/4 v3, 0x2

    iget-boolean v9, v0, LX/A37;->c:Z

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1615345
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1615346
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1615347
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1615348
    const/4 v3, 0x6

    iget v5, v0, LX/A37;->g:I

    invoke-virtual {v2, v3, v5, v10}, LX/186;->a(III)V

    .line 1615349
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1615350
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1615351
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1615352
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1615353
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1615354
    new-instance v3, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;-><init>(LX/15i;)V

    .line 1615355
    move-object p0, v3

    .line 1615356
    goto/16 :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1615320
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615321
    iget v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1615305
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615306
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1615307
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1615308
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1615309
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->ft_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1615310
    const/4 v4, 0x7

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1615311
    iget v4, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e:I

    invoke-virtual {p1, v5, v4, v5}, LX/186;->a(III)V

    .line 1615312
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1615313
    const/4 v0, 0x2

    iget-boolean v4, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->g:Z

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1615314
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1615315
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1615316
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1615317
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->k:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 1615318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615319
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1615302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615303
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615304
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1615297
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1615298
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e:I

    .line 1615299
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->g:Z

    .line 1615300
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->k:I

    .line 1615301
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1615280
    new-instance v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;-><init>()V

    .line 1615281
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615282
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615295
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->f:Ljava/lang/String;

    .line 1615296
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1615293
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615294
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->g:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615291
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->h:Ljava/lang/String;

    .line 1615292
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1615290
    const v0, -0x4536f983

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615288
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->i:Ljava/lang/String;

    .line 1615289
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1615287
    const v0, 0x4a5e2c50    # 3640084.0f

    return v0
.end method

.method public final ft_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615285
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->j:Ljava/lang/String;

    .line 1615286
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final fu_()I
    .locals 2

    .prologue
    .line 1615283
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615284
    iget v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->k:I

    return v0
.end method
