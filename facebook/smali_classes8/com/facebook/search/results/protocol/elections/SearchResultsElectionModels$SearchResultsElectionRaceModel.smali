.class public final Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd2abe29
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615690
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615689
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1615745
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615746
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1615742
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615743
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615744
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;
    .locals 4

    .prologue
    .line 1615726
    if-nez p0, :cond_0

    .line 1615727
    const/4 p0, 0x0

    .line 1615728
    :goto_0
    return-object p0

    .line 1615729
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    if-eqz v0, :cond_1

    .line 1615730
    check-cast p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    goto :goto_0

    .line 1615731
    :cond_1
    new-instance v2, LX/A3B;

    invoke-direct {v2}, LX/A3B;-><init>()V

    .line 1615732
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1615733
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1615734
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1615735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1615736
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/A3B;->a:LX/0Px;

    .line 1615737
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->b()D

    move-result-wide v0

    iput-wide v0, v2, LX/A3B;->b:D

    .line 1615738
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/A3B;->c:Ljava/lang/String;

    .line 1615739
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/A3B;->d:Ljava/lang/String;

    .line 1615740
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e()D

    move-result-wide v0

    iput-wide v0, v2, LX/A3B;->e:D

    .line 1615741
    invoke-virtual {v2}, LX/A3B;->a()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 1615714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615715
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1615716
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1615717
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1615718
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1615719
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1615720
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1615721
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1615722
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1615723
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->i:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1615724
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615725
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1615712
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e:Ljava/util/List;

    .line 1615713
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1615704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615705
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1615706
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1615707
    if-eqz v1, :cond_0

    .line 1615708
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    .line 1615709
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e:Ljava/util/List;

    .line 1615710
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615711
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1615747
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1615748
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->f:D

    .line 1615749
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->i:D

    .line 1615750
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1615702
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615703
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1615699
    new-instance v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;-><init>()V

    .line 1615700
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615701
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615697
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->g:Ljava/lang/String;

    .line 1615698
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615695
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->h:Ljava/lang/String;

    .line 1615696
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1615694
    const v0, 0x1e1e82ac

    return v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 1615692
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615693
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->i:D

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1615691
    const v0, 0x6b2dd872

    return v0
.end method
