.class public final Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x26bbd555
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615650
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1615649
    const-class v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1615647
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615648
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1615644
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1615645
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615646
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1615603
    if-nez p0, :cond_0

    .line 1615604
    const/4 p0, 0x0

    .line 1615605
    :goto_0
    return-object p0

    .line 1615606
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    if-eqz v0, :cond_1

    .line 1615607
    check-cast p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    goto :goto_0

    .line 1615608
    :cond_1
    new-instance v3, LX/A3A;

    invoke-direct {v3}, LX/A3A;-><init>()V

    .line 1615609
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1615610
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1615611
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;->a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1615612
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1615613
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/A3A;->a:LX/0Px;

    .line 1615614
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1615615
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1615616
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1615617
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1615618
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/A3A;->b:LX/0Px;

    .line 1615619
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/A3A;->c:Ljava/lang/String;

    .line 1615620
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/A3A;->d:Ljava/lang/String;

    .line 1615621
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->e()I

    move-result v0

    iput v0, v3, LX/A3A;->e:I

    .line 1615622
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->fw_()I

    move-result v0

    iput v0, v3, LX/A3A;->f:I

    .line 1615623
    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v13, 0x0

    .line 1615624
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 1615625
    iget-object v6, v3, LX/A3A;->a:LX/0Px;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1615626
    iget-object v8, v3, LX/A3A;->b:LX/0Px;

    invoke-static {v5, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 1615627
    iget-object v10, v3, LX/A3A;->c:Ljava/lang/String;

    invoke-virtual {v5, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1615628
    iget-object v11, v3, LX/A3A;->d:Ljava/lang/String;

    invoke-virtual {v5, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1615629
    const/4 v12, 0x6

    invoke-virtual {v5, v12}, LX/186;->c(I)V

    .line 1615630
    invoke-virtual {v5, v13, v6}, LX/186;->b(II)V

    .line 1615631
    invoke-virtual {v5, v9, v8}, LX/186;->b(II)V

    .line 1615632
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v10}, LX/186;->b(II)V

    .line 1615633
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v11}, LX/186;->b(II)V

    .line 1615634
    const/4 v6, 0x4

    iget v8, v3, LX/A3A;->e:I

    invoke-virtual {v5, v6, v8, v13}, LX/186;->a(III)V

    .line 1615635
    const/4 v6, 0x5

    iget v8, v3, LX/A3A;->f:I

    invoke-virtual {v5, v6, v8, v13}, LX/186;->a(III)V

    .line 1615636
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 1615637
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 1615638
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1615639
    invoke-virtual {v6, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1615640
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1615641
    new-instance v6, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    invoke-direct {v6, v5}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;-><init>(LX/15i;)V

    .line 1615642
    move-object p0, v6

    .line 1615643
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1615589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615590
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1615591
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1615592
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1615593
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1615594
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1615595
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1615596
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1615597
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1615598
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1615599
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->i:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 1615600
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->j:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 1615601
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615602
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1615587
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->e:Ljava/util/List;

    .line 1615588
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1615574
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1615575
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1615576
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1615577
    if-eqz v1, :cond_0

    .line 1615578
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1615579
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->e:Ljava/util/List;

    .line 1615580
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1615581
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1615582
    if-eqz v1, :cond_1

    .line 1615583
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1615584
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->f:Ljava/util/List;

    .line 1615585
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1615586
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1615570
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1615571
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->i:I

    .line 1615572
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->j:I

    .line 1615573
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1615555
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->f:Ljava/util/List;

    .line 1615556
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1615567
    new-instance v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;-><init>()V

    .line 1615568
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1615569
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615565
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->g:Ljava/lang/String;

    .line 1615566
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1615563
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->h:Ljava/lang/String;

    .line 1615564
    iget-object v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1615562
    const v0, 0x613b20eb

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1615560
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615561
    iget v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->i:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1615559
    const v0, 0x1c03a7aa

    return v0
.end method

.method public final fw_()I
    .locals 2

    .prologue
    .line 1615557
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1615558
    iget v0, p0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->j:I

    return v0
.end method
