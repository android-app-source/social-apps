.class public final Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1dc219b9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1614559
    const-class v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1614596
    const-class v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1614594
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1614595
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1614591
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1614592
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1614593
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
    .locals 11

    .prologue
    .line 1614562
    if-nez p0, :cond_0

    .line 1614563
    const/4 p0, 0x0

    .line 1614564
    :goto_0
    return-object p0

    .line 1614565
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    if-eqz v0, :cond_1

    .line 1614566
    check-cast p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    goto :goto_0

    .line 1614567
    :cond_1
    new-instance v0, LX/A2j;

    invoke-direct {v0}, LX/A2j;-><init>()V

    .line 1614568
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/A2j;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1614569
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A2j;->b:Ljava/lang/String;

    .line 1614570
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A2j;->c:Ljava/lang/String;

    .line 1614571
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->e()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;->a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v1

    iput-object v1, v0, LX/A2j;->d:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    .line 1614572
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1614573
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1614574
    iget-object v3, v0, LX/A2j;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1614575
    iget-object v5, v0, LX/A2j;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1614576
    iget-object v7, v0, LX/A2j;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1614577
    iget-object v8, v0, LX/A2j;->d:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1614578
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1614579
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1614580
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1614581
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1614582
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1614583
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1614584
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1614585
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1614586
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1614587
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1614588
    new-instance v3, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;-><init>(LX/15i;)V

    .line 1614589
    move-object p0, v3

    .line 1614590
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614560
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->h:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->h:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    .line 1614561
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->h:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1614547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1614548
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1614549
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1614550
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1614551
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->j()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1614552
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1614553
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1614554
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1614555
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1614556
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1614557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1614558
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1614539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1614540
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->j()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1614541
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->j()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    .line 1614542
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->j()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1614543
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1614544
    iput-object v0, v1, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->h:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    .line 1614545
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1614546
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1614538
    new-instance v0, LX/A2k;

    invoke-direct {v0, p1}, LX/A2k;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614597
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1614536
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1614537
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1614535
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614532
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1614533
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1614534
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1614522
    new-instance v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;-><init>()V

    .line 1614523
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1614524
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614530
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->f:Ljava/lang/String;

    .line 1614531
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614528
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->g:Ljava/lang/String;

    .line 1614529
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1614527
    const v0, -0x1b70604a

    return v0
.end method

.method public final synthetic e()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614526
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->j()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1614525
    const v0, 0x3c2b9d5

    return v0
.end method
