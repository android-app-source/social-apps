.class public final Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3ba47af9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1614431
    const-class v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1614430
    const-class v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1614428
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1614429
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1614425
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1614426
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1614427
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
    .locals 9

    .prologue
    .line 1614385
    if-nez p0, :cond_0

    .line 1614386
    const/4 p0, 0x0

    .line 1614387
    :goto_0
    return-object p0

    .line 1614388
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    if-eqz v0, :cond_1

    .line 1614389
    check-cast p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    goto :goto_0

    .line 1614390
    :cond_1
    new-instance v0, LX/A2i;

    invoke-direct {v0}, LX/A2i;-><init>()V

    .line 1614391
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A2i;->a:Ljava/lang/String;

    .line 1614392
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A2i;->b:Ljava/lang/String;

    .line 1614393
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1614394
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1614395
    iget-object v3, v0, LX/A2i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1614396
    iget-object v5, v0, LX/A2i;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1614397
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1614398
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1614399
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1614400
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1614401
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1614402
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1614403
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1614404
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1614405
    new-instance v3, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;-><init>(LX/15i;)V

    .line 1614406
    move-object p0, v3

    .line 1614407
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1614417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1614418
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1614419
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1614420
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1614421
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1614422
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1614423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1614424
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1614432
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1614433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1614434
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614415
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->e:Ljava/lang/String;

    .line 1614416
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1614412
    new-instance v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;-><init>()V

    .line 1614413
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1614414
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1614410
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->f:Ljava/lang/String;

    .line 1614411
    iget-object v0, p0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1614409
    const v0, -0x28f09522

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1614408
    const v0, 0x2cee5bdc

    return v0
.end method
