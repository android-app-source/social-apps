.class public final Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1614649
    const-class v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel;

    new-instance v1, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1614650
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1614648
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1614599
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1614600
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    .line 1614601
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1614602
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1614603
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 1614604
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614605
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1614606
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1614607
    if-eqz v2, :cond_1

    .line 1614608
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614609
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1614610
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1614611
    if-eqz v2, :cond_2

    .line 1614612
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614613
    invoke-static {v1, v2, p1}, LX/A2n;->a(LX/15i;ILX/0nX;)V

    .line 1614614
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1614615
    if-eqz v2, :cond_3

    .line 1614616
    const-string v3, "is_on_sale"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614617
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1614618
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1614619
    if-eqz v2, :cond_4

    .line 1614620
    const-string v3, "item_price"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614621
    invoke-static {v1, v2, p1}, LX/A2o;->a(LX/15i;ILX/0nX;)V

    .line 1614622
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1614623
    if-eqz v2, :cond_5

    .line 1614624
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614625
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1614626
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1614627
    if-eqz v2, :cond_6

    .line 1614628
    const-string v3, "parent_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614629
    invoke-static {v1, v2, p1, p2}, LX/A2w;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1614630
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1614631
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_7

    .line 1614632
    const-string v4, "product_latitude"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614633
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1614634
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1614635
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_8

    .line 1614636
    const-string v4, "product_longitude"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614637
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1614638
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1614639
    if-eqz v2, :cond_9

    .line 1614640
    const-string v3, "sale_price"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614641
    invoke-static {v1, v2, p1}, LX/A2x;->a(LX/15i;ILX/0nX;)V

    .line 1614642
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1614643
    if-eqz v2, :cond_a

    .line 1614644
    const-string v3, "seller"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1614645
    invoke-static {v1, v2, p1, p2}, LX/A2z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1614646
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1614647
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1614598
    check-cast p1, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$Serializer;->a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel;LX/0nX;LX/0my;)V

    return-void
.end method
