.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1387767
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1387768
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1387832
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1387769
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1387770
    const/4 v11, 0x0

    .line 1387771
    const/4 v10, 0x0

    .line 1387772
    const/4 v9, 0x0

    .line 1387773
    const/4 v8, 0x0

    .line 1387774
    const/4 v7, 0x0

    .line 1387775
    const/4 v6, 0x0

    .line 1387776
    const/4 v5, 0x0

    .line 1387777
    const/4 v4, 0x0

    .line 1387778
    const/4 v3, 0x0

    .line 1387779
    const/4 v2, 0x0

    .line 1387780
    const/4 v1, 0x0

    .line 1387781
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 1387782
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1387783
    const/4 v1, 0x0

    .line 1387784
    :goto_0
    move v1, v1

    .line 1387785
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1387786
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1387787
    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;-><init>()V

    .line 1387788
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1387789
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1387790
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1387791
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1387792
    :cond_0
    return-object v1

    .line 1387793
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1387794
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_d

    .line 1387795
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1387796
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1387797
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1387798
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1387799
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 1387800
    :cond_4
    const-string p0, "community_category"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1387801
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 1387802
    :cond_5
    const-string p0, "group_members"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1387803
    invoke-static {p1, v0}, LX/8gp;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1387804
    :cond_6
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1387805
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1387806
    :cond_7
    const-string p0, "is_verified"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1387807
    const/4 v1, 0x1

    .line 1387808
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1387809
    :cond_8
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1387810
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1387811
    :cond_9
    const-string p0, "profile_picture"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1387812
    invoke-static {p1, v0}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1387813
    :cond_a
    const-string p0, "social_context"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 1387814
    invoke-static {p1, v0}, LX/8gq;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1387815
    :cond_b
    const-string p0, "viewer_join_state"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1387816
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1387817
    :cond_c
    const-string p0, "visibility_sentence"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1387818
    invoke-static {p1, v0}, LX/8gr;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1387819
    :cond_d
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1387820
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1387821
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1387822
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1387823
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1387824
    if-eqz v1, :cond_e

    .line 1387825
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1387826
    :cond_e
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1387827
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1387828
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1387829
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1387830
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1387831
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
