.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1389273
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1389274
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1389246
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1389248
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1389249
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1389250
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1389251
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1389252
    if-eqz v2, :cond_0

    .line 1389253
    const-string p0, "accessibility_caption"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1389254
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1389255
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1389256
    if-eqz v2, :cond_1

    .line 1389257
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1389258
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1389259
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1389260
    if-eqz v2, :cond_2

    .line 1389261
    const-string p0, "image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1389262
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1389263
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1389264
    if-eqz v2, :cond_3

    .line 1389265
    const-string p0, "imageHigh"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1389266
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1389267
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1389268
    if-eqz v2, :cond_4

    .line 1389269
    const-string p0, "photo_owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1389270
    invoke-static {v1, v2, p1}, LX/8hD;->a(LX/15i;ILX/0nX;)V

    .line 1389271
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1389272
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1389247
    check-cast p1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Serializer;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;LX/0nX;LX/0my;)V

    return-void
.end method
