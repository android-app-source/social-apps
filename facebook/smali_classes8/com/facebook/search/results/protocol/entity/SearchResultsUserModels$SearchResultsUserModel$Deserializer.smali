.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1390128
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1390129
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1390127
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1390082
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1390083
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1390084
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_d

    .line 1390085
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1390086
    :goto_0
    move v1, v2

    .line 1390087
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1390088
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1390089
    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;-><init>()V

    .line 1390090
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1390091
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1390092
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1390093
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1390094
    :cond_0
    return-object v1

    .line 1390095
    :cond_1
    const-string p0, "is_verified"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1390096
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    .line 1390097
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_b

    .line 1390098
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1390099
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1390100
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1390101
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1390102
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 1390103
    :cond_4
    const-string p0, "bio_text"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1390104
    invoke-static {p1, v0}, LX/8hY;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1390105
    :cond_5
    const-string p0, "friendship_status"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1390106
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1390107
    :cond_6
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1390108
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1390109
    :cond_7
    const-string p0, "mutual_friends"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1390110
    invoke-static {p1, v0}, LX/8hZ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1390111
    :cond_8
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1390112
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1390113
    :cond_9
    const-string p0, "profile_picture"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1390114
    invoke-static {p1, v0}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1390115
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1390116
    :cond_b
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1390117
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1390118
    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1390119
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1390120
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1390121
    if-eqz v1, :cond_c

    .line 1390122
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1390123
    :cond_c
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1390124
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1390125
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1390126
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
