.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/8d2;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4954790e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1389324
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1389327
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1389325
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1389326
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389322
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1389323
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389320
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1389321
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389318
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1389319
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389316
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->e:Ljava/lang/String;

    .line 1389317
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic B()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389328
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389315
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1389301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1389302
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1389303
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->dW_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1389304
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1389305
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1389306
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1389307
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1389308
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1389309
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1389310
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1389311
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1389312
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1389313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1389314
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1389283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1389284
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1389285
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1389286
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1389287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;

    .line 1389288
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1389289
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1389290
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1389291
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1389292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;

    .line 1389293
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1389294
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1389295
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1389296
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1389297
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;

    .line 1389298
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1389299
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1389300
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389282
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1389279
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;-><init>()V

    .line 1389280
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1389281
    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389275
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->f:Ljava/lang/String;

    .line 1389276
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1389278
    const v0, -0x5f710548

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1389277
    const v0, 0x4984e12

    return v0
.end method
