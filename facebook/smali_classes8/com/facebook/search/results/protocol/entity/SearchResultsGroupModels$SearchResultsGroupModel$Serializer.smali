.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1388030
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1388031
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1388029
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1387983
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1387984
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1387985
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1387986
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1387987
    if-eqz v2, :cond_0

    .line 1387988
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387989
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1387990
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1387991
    if-eqz v2, :cond_1

    .line 1387992
    const-string v2, "community_category"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387993
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387994
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1387995
    if-eqz v2, :cond_2

    .line 1387996
    const-string v3, "group_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387997
    invoke-static {v1, v2, p1, p2}, LX/8gp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1387998
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1387999
    if-eqz v2, :cond_3

    .line 1388000
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388001
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1388002
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1388003
    if-eqz v2, :cond_4

    .line 1388004
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388005
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1388006
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1388007
    if-eqz v2, :cond_5

    .line 1388008
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388009
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1388010
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388011
    if-eqz v2, :cond_6

    .line 1388012
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388013
    invoke-static {v1, v2, p1}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 1388014
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388015
    if-eqz v2, :cond_7

    .line 1388016
    const-string v3, "social_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388017
    invoke-static {v1, v2, p1}, LX/8gq;->a(LX/15i;ILX/0nX;)V

    .line 1388018
    :cond_7
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1388019
    if-eqz v2, :cond_8

    .line 1388020
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388021
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1388022
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388023
    if-eqz v2, :cond_9

    .line 1388024
    const-string v3, "visibility_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388025
    invoke-static {v1, v2, p1}, LX/8gr;->a(LX/15i;ILX/0nX;)V

    .line 1388026
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1388027
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1388028
    check-cast p1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Serializer;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;LX/0nX;LX/0my;)V

    return-void
.end method
