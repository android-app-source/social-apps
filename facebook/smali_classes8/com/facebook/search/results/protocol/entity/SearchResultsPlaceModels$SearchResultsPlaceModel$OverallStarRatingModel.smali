.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d369c4a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1389546
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1389595
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1389593
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1389594
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1389590
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1389591
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1389592
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .locals 13

    .prologue
    .line 1389569
    if-nez p0, :cond_0

    .line 1389570
    const/4 p0, 0x0

    .line 1389571
    :goto_0
    return-object p0

    .line 1389572
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    if-eqz v0, :cond_1

    .line 1389573
    check-cast p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    goto :goto_0

    .line 1389574
    :cond_1
    new-instance v0, LX/8hJ;

    invoke-direct {v0}, LX/8hJ;-><init>()V

    .line 1389575
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->a()I

    move-result v1

    iput v1, v0, LX/8hJ;->a:I

    .line 1389576
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/8hJ;->b:D

    .line 1389577
    const/4 v5, 0x1

    const/4 v12, 0x0

    const/4 v10, 0x0

    .line 1389578
    new-instance v4, LX/186;

    const/16 v6, 0x80

    invoke-direct {v4, v6}, LX/186;-><init>(I)V

    .line 1389579
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, LX/186;->c(I)V

    .line 1389580
    iget v6, v0, LX/8hJ;->a:I

    invoke-virtual {v4, v10, v6, v10}, LX/186;->a(III)V

    .line 1389581
    iget-wide v6, v0, LX/8hJ;->b:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1389582
    invoke-virtual {v4}, LX/186;->d()I

    move-result v6

    .line 1389583
    invoke-virtual {v4, v6}, LX/186;->d(I)V

    .line 1389584
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1389585
    invoke-virtual {v7, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1389586
    new-instance v6, LX/15i;

    move-object v8, v12

    move-object v9, v12

    move v10, v5

    move-object v11, v12

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1389587
    new-instance v4, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    invoke-direct {v4, v6}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;-><init>(LX/15i;)V

    .line 1389588
    move-object p0, v4

    .line 1389589
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1389567
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1389568
    iget v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1389561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1389562
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1389563
    iget v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1389564
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1389565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1389566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1389558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1389559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1389560
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1389554
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1389555
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->e:I

    .line 1389556
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->f:D

    .line 1389557
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1389552
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1389553
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1389549
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;-><init>()V

    .line 1389550
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1389551
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1389548
    const v0, -0x30516dee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1389547
    const v0, -0x6e856243

    return v0
.end method
