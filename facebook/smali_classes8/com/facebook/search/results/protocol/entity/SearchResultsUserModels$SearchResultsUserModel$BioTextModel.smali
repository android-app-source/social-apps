.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1390075
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1390074
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1390072
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1390073
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1390069
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1390070
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1390071
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .locals 2

    .prologue
    .line 1390061
    if-nez p0, :cond_0

    .line 1390062
    const/4 p0, 0x0

    .line 1390063
    :goto_0
    return-object p0

    .line 1390064
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    if-eqz v0, :cond_1

    .line 1390065
    check-cast p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    goto :goto_0

    .line 1390066
    :cond_1
    new-instance v0, LX/8hU;

    invoke-direct {v0}, LX/8hU;-><init>()V

    .line 1390067
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8hU;->a:Ljava/lang/String;

    .line 1390068
    invoke-virtual {v0}, LX/8hU;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1390076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1390077
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1390078
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1390079
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1390080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1390081
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1390058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1390059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1390060
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1390056
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->e:Ljava/lang/String;

    .line 1390057
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1390053
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;-><init>()V

    .line 1390054
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1390055
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1390052
    const v0, 0x7e2c76d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1390051
    const v0, -0x726d476c

    return v0
.end method
