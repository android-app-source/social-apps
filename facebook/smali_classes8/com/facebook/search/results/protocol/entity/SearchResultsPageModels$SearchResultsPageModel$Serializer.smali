.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1388880
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1388881
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1388882
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1388819
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1388820
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0xc

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1388821
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1388822
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1388823
    if-eqz v2, :cond_0

    .line 1388824
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388825
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1388826
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1388827
    if-eqz v2, :cond_1

    .line 1388828
    const-string v3, "can_viewer_follow"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388829
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1388830
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1388831
    if-eqz v2, :cond_2

    .line 1388832
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388833
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1388834
    :cond_2
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1388835
    if-eqz v2, :cond_3

    .line 1388836
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388837
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1388838
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388839
    if-eqz v2, :cond_4

    .line 1388840
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388841
    invoke-static {v1, v2, p1, p2}, LX/8g0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1388842
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1388843
    if-eqz v2, :cond_5

    .line 1388844
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388845
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1388846
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1388847
    if-eqz v2, :cond_6

    .line 1388848
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388849
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1388850
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1388851
    if-eqz v2, :cond_7

    .line 1388852
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388853
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1388854
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1388855
    if-eqz v2, :cond_8

    .line 1388856
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388857
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1388858
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388859
    if-eqz v2, :cond_9

    .line 1388860
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388861
    invoke-static {v1, v2, p1, p2}, LX/8gz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1388862
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388863
    if-eqz v2, :cond_a

    .line 1388864
    const-string v3, "page_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388865
    invoke-static {v1, v2, p1}, LX/8h5;->a(LX/15i;ILX/0nX;)V

    .line 1388866
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1388867
    if-eqz v2, :cond_b

    .line 1388868
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388869
    invoke-static {v1, v2, p1}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 1388870
    :cond_b
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1388871
    if-eqz v2, :cond_c

    .line 1388872
    const-string v2, "verification_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388873
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1388874
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1388875
    if-eqz v2, :cond_d

    .line 1388876
    const-string v3, "video_channel_is_viewer_following"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1388877
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1388878
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1388879
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1388818
    check-cast p1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Serializer;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;LX/0nX;LX/0my;)V

    return-void
.end method
