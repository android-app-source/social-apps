.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/8dA;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x58233cd1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1388206
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1388205
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1388186
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1388187
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1388188
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1388189
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1388190
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1388191
    if-eqz v0, :cond_0

    .line 1388192
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x8

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1388193
    :cond_0
    return-void

    .line 1388194
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1388207
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j:Ljava/lang/String;

    .line 1388208
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1388209
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1388210
    if-eqz v0, :cond_0

    .line 1388211
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1388212
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388213
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->g:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->g:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1388214
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->g:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388215
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1388216
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388217
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1388218
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    return-object v0
.end method

.method private m()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388219
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1388220
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1388221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1388222
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1388223
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1388224
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1388225
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->dW_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1388226
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1388227
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1388228
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1388229
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1388230
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1388231
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1388232
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1388233
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1388234
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1388235
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1388236
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1388237
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1388238
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1388239
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1388240
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1388241
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1388242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1388243
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1388244
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1388245
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1388246
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1388247
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1388248
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    .line 1388249
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->g:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1388250
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1388251
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1388252
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1388253
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    .line 1388254
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1388255
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1388256
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1388257
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1388258
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    .line 1388259
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1388260
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1388261
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1388262
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1388263
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    .line 1388264
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1388265
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1388266
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1388267
    new-instance v0, LX/8gk;

    invoke-direct {v0, p1}, LX/8gk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388268
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1388269
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1388270
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->i:Z

    .line 1388271
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1388195
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1388196
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1388197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1388198
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 1388199
    :goto_0
    return-void

    .line 1388200
    :cond_0
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1388201
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1388202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1388203
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1388204
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1388159
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1388160
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->a(Ljava/lang/String;)V

    .line 1388161
    :cond_0
    :goto_0
    return-void

    .line 1388162
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1388163
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1388164
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;-><init>()V

    .line 1388165
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1388166
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388167
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j:Ljava/lang/String;

    .line 1388168
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388169
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->h:Ljava/lang/String;

    .line 1388170
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1388171
    const v0, -0x2823f4be

    return v0
.end method

.method public final synthetic e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388172
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1388173
    const v0, 0x41e065f

    return v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388174
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1388175
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1388176
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1388177
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1388178
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->i:Z

    return v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388179
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1388180
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388181
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388182
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388183
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1388184
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388185
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel;->m()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    return-object v0
.end method
