.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/8d9;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd60c717
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:J

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1387469
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1387468
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1387515
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1387516
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1387517
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1387518
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1387519
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1387520
    if-eqz v0, :cond_0

    .line 1387521
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xd

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1387522
    :cond_0
    return-void

    .line 1387523
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1387524
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->s:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1387525
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1387526
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1387527
    if-eqz v0, :cond_0

    .line 1387528
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xe

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1387529
    :cond_0
    return-void

    .line 1387530
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1387531
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->n:Ljava/lang/String;

    .line 1387532
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1387533
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1387534
    if-eqz v0, :cond_0

    .line 1387535
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1387536
    :cond_0
    return-void
.end method

.method private p()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387537
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1387538
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    return-object v0
.end method

.method private q()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387539
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->j:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->j:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1387540
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->j:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387570
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1387571
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    .line 1387541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1387542
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1387543
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1387544
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1387545
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1387546
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->dW_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1387547
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1387548
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1387549
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1387550
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 1387551
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1387552
    const/16 v2, 0xf

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1387553
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1387554
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1387555
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1387556
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->h:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1387557
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1387558
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1387559
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1387560
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1387561
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1387562
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1387563
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1387564
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1387565
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1387566
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1387567
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 1387568
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1387569
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1387580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1387581
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1387582
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1387583
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1387584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;

    .line 1387585
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->i:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1387586
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1387587
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1387588
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1387589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;

    .line 1387590
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->j:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1387591
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1387592
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1387593
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1387594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;

    .line 1387595
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1387596
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1387597
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1387598
    new-instance v0, LX/8gY;

    invoke-direct {v0, p1}, LX/8gY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387579
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1387572
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1387573
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->f:Z

    .line 1387574
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->h:J

    .line 1387575
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->l:Z

    .line 1387576
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->m:Z

    .line 1387577
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p:J

    .line 1387578
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1387494
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1387495
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1387496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1387497
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    .line 1387498
    :goto_0
    return-void

    .line 1387499
    :cond_0
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1387500
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1387501
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1387502
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1387503
    :cond_1
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1387504
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1387505
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1387506
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1387507
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1387508
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1387509
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->a(Ljava/lang/String;)V

    .line 1387510
    :cond_0
    :goto_0
    return-void

    .line 1387511
    :cond_1
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1387512
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0

    .line 1387513
    :cond_2
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1387514
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1387491
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;-><init>()V

    .line 1387492
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1387493
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387489
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->n:Ljava/lang/String;

    .line 1387490
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387487
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1387488
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387485
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->k:Ljava/lang/String;

    .line 1387486
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1387484
    const v0, 0x101491b6

    return v0
.end method

.method public final synthetic e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387483
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1387482
    const v0, 0x403827a

    return v0
.end method

.method public final synthetic j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387481
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->p()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387479
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q:Ljava/lang/String;

    .line 1387480
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387477
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1387478
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387475
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->s:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->s:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1387476
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->s:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387472
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1387473
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1387474
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1387470
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1387471
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;->m:Z

    return v0
.end method
