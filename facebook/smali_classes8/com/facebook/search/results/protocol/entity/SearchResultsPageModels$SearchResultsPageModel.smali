.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/8d0;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3cde5170
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1388926
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1388927
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1388928
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1388929
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1388930
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1388931
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1388932
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1388933
    iput-boolean p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j:Z

    .line 1388934
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1388935
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1388936
    if-eqz v0, :cond_0

    .line 1388937
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1388938
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1388939
    iput-boolean p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->r:Z

    .line 1388940
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1388941
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1388942
    if-eqz v0, :cond_0

    .line 1388943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1388944
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388945
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->i:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->i:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1388946
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->i:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388914
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1388915
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388947
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->o:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->o:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1388948
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->o:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388949
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1388950
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 1388951
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1388952
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->r:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1388953
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1388954
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1388955
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1388956
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1388957
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->dW_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1388958
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1388959
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1388960
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1388961
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1388962
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1388963
    const/16 v9, 0xe

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1388964
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1388965
    const/4 v0, 0x1

    iget-boolean v9, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->f:Z

    invoke-virtual {p1, v0, v9}, LX/186;->a(IZ)V

    .line 1388966
    const/4 v0, 0x2

    iget-boolean v9, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->g:Z

    invoke-virtual {p1, v0, v9}, LX/186;->a(IZ)V

    .line 1388967
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1388968
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1388969
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1388970
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1388971
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1388972
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1388973
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1388974
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1388975
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1388976
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1388977
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->r:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1388978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1388979
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1388980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1388981
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1388982
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1388983
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1388984
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    .line 1388985
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->i:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1388986
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1388987
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1388988
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1388989
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    .line 1388990
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->n:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1388991
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1388992
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1388993
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1388994
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    .line 1388995
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->o:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1388996
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1388997
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1388998
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1388999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    .line 1389000
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1389001
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1389002
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1389003
    new-instance v0, LX/8h2;

    invoke-direct {v0, p1}, LX/8h2;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1389004
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1389005
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1389006
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->f:Z

    .line 1389007
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->g:Z

    .line 1389008
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j:Z

    .line 1389009
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l:Z

    .line 1389010
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->r:Z

    .line 1389011
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1388916
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1388917
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->w()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1388918
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1388919
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 1388920
    :goto_0
    return-void

    .line 1388921
    :cond_0
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1388922
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->p()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1388923
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1388924
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1388925
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1388886
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1388887
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->a(Z)V

    .line 1388888
    :cond_0
    :goto_0
    return-void

    .line 1388889
    :cond_1
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1388890
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1388891
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;-><init>()V

    .line 1388892
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1388893
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388894
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388895
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m:Ljava/lang/String;

    .line 1388896
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388897
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k:Ljava/lang/String;

    .line 1388898
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1388899
    const v0, 0x112c15d4

    return v0
.end method

.method public final synthetic e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388900
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1388901
    const v0, 0x25d6af

    return v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388883
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1388884
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1388885
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1388902
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1388903
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l:Z

    return v0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 1388904
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1388905
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->g:Z

    return v0
.end method

.method public final v()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1388906
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->h:Ljava/util/List;

    .line 1388907
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1388908
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1388909
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->j:Z

    return v0
.end method

.method public final synthetic x()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388910
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->k()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388911
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->l()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1388912
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->q:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->q:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1388913
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;->q:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    return-object v0
.end method
