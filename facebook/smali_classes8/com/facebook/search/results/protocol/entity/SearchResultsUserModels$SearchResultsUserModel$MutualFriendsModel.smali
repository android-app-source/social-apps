.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1390163
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1390164
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1390165
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1390166
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1390167
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1390168
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1390169
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .locals 2

    .prologue
    .line 1390170
    if-nez p0, :cond_0

    .line 1390171
    const/4 p0, 0x0

    .line 1390172
    :goto_0
    return-object p0

    .line 1390173
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    if-eqz v0, :cond_1

    .line 1390174
    check-cast p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    goto :goto_0

    .line 1390175
    :cond_1
    new-instance v0, LX/8hW;

    invoke-direct {v0}, LX/8hW;-><init>()V

    .line 1390176
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;->a()I

    move-result v1

    iput v1, v0, LX/8hW;->a:I

    .line 1390177
    invoke-virtual {v0}, LX/8hW;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1390178
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1390179
    iget v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1390180
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1390181
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1390182
    iget v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1390183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1390184
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1390185
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1390186
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1390187
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1390188
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1390189
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;->e:I

    .line 1390190
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1390191
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;-><init>()V

    .line 1390192
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1390193
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1390194
    const v0, -0x46b43cf8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1390195
    const v0, 0x5b54b87f

    return v0
.end method
