.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1390235
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1390236
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1390196
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1390198
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1390199
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    const/4 v3, 0x0

    .line 1390200
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1390201
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1390202
    if-eqz v2, :cond_0

    .line 1390203
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390204
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1390205
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1390206
    if-eqz v2, :cond_1

    .line 1390207
    const-string v3, "bio_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390208
    invoke-static {v1, v2, p1}, LX/8hY;->a(LX/15i;ILX/0nX;)V

    .line 1390209
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1390210
    if-eqz v2, :cond_2

    .line 1390211
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390212
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1390213
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1390214
    if-eqz v2, :cond_3

    .line 1390215
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390216
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1390217
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1390218
    if-eqz v2, :cond_4

    .line 1390219
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390220
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1390221
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1390222
    if-eqz v2, :cond_5

    .line 1390223
    const-string v3, "mutual_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390224
    invoke-static {v1, v2, p1}, LX/8hZ;->a(LX/15i;ILX/0nX;)V

    .line 1390225
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1390226
    if-eqz v2, :cond_6

    .line 1390227
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390228
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1390229
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1390230
    if-eqz v2, :cond_7

    .line 1390231
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390232
    invoke-static {v1, v2, p1}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 1390233
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1390234
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1390197
    check-cast p1, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$Serializer;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel;LX/0nX;LX/0my;)V

    return-void
.end method
