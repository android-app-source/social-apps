.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1388774
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1388775
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1388776
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1388777
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1388778
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1388779
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1388780
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 8

    .prologue
    .line 1388781
    if-nez p0, :cond_0

    .line 1388782
    const/4 p0, 0x0

    .line 1388783
    :goto_0
    return-object p0

    .line 1388784
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    if-eqz v0, :cond_1

    .line 1388785
    check-cast p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    goto :goto_0

    .line 1388786
    :cond_1
    new-instance v0, LX/8h3;

    invoke-direct {v0}, LX/8h3;-><init>()V

    .line 1388787
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;->a()I

    move-result v1

    iput v1, v0, LX/8h3;->a:I

    .line 1388788
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1388789
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1388790
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1388791
    iget v3, v0, LX/8h3;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1388792
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1388793
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1388794
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1388795
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1388796
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1388797
    new-instance v3, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;-><init>(LX/15i;)V

    .line 1388798
    move-object p0, v3

    .line 1388799
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1388800
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1388801
    iget v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1388802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1388803
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1388804
    iget v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1388805
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1388806
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1388807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1388808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1388809
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1388810
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1388811
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;->e:I

    .line 1388812
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1388813
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;-><init>()V

    .line 1388814
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1388815
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1388816
    const v0, -0x574c97c1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1388817
    const v0, 0x25f82de5

    return v0
.end method
