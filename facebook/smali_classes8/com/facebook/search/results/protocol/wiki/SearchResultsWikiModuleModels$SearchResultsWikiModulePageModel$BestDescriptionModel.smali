.class public final Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620922
    const-class v0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620921
    const-class v0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1620919
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1620920
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1620916
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1620917
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1620918
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;)Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;
    .locals 8

    .prologue
    .line 1620893
    if-nez p0, :cond_0

    .line 1620894
    const/4 p0, 0x0

    .line 1620895
    :goto_0
    return-object p0

    .line 1620896
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    if-eqz v0, :cond_1

    .line 1620897
    check-cast p0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    goto :goto_0

    .line 1620898
    :cond_1
    new-instance v0, LX/A5K;

    invoke-direct {v0}, LX/A5K;-><init>()V

    .line 1620899
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A5K;->a:Ljava/lang/String;

    .line 1620900
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1620901
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1620902
    iget-object v3, v0, LX/A5K;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1620903
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1620904
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1620905
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1620906
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1620907
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1620908
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1620909
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1620910
    new-instance v3, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;-><init>(LX/15i;)V

    .line 1620911
    move-object p0, v3

    .line 1620912
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1620923
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620924
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1620925
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1620926
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1620927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620928
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1620913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620914
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620915
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620891
    iget-object v0, p0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->e:Ljava/lang/String;

    .line 1620892
    iget-object v0, p0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1620888
    new-instance v0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;-><init>()V

    .line 1620889
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1620890
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1620887
    const v0, -0x2f86f558

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1620886
    const v0, -0x726d476c

    return v0
.end method
