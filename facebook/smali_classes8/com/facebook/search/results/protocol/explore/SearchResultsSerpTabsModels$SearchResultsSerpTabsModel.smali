.class public final Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x359d4e53
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1390521
    const-class v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1390520
    const-class v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1390518
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1390519
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1390515
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1390516
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1390517
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;)Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;
    .locals 10

    .prologue
    .line 1390489
    if-nez p0, :cond_0

    .line 1390490
    const/4 p0, 0x0

    .line 1390491
    :goto_0
    return-object p0

    .line 1390492
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    if-eqz v0, :cond_1

    .line 1390493
    check-cast p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    goto :goto_0

    .line 1390494
    :cond_1
    new-instance v0, LX/8he;

    invoke-direct {v0}, LX/8he;-><init>()V

    .line 1390495
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8he;->a:Ljava/lang/String;

    .line 1390496
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8he;->b:Ljava/lang/String;

    .line 1390497
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->c()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;->a(Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;)Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v1

    iput-object v1, v0, LX/8he;->c:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    .line 1390498
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1390499
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1390500
    iget-object v3, v0, LX/8he;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1390501
    iget-object v5, v0, LX/8he;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1390502
    iget-object v7, v0, LX/8he;->c:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1390503
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1390504
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1390505
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1390506
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1390507
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1390508
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1390509
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1390510
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1390511
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1390512
    new-instance v3, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;-><init>(LX/15i;)V

    .line 1390513
    move-object p0, v3

    .line 1390514
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1390487
    iget-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->g:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->g:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    .line 1390488
    iget-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->g:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1390477
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1390478
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1390479
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1390480
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->j()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1390481
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1390482
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1390483
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1390484
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1390485
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1390486
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1390469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1390470
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->j()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1390471
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->j()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    .line 1390472
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->j()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1390473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    .line 1390474
    iput-object v0, v1, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->g:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    .line 1390475
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1390476
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1390467
    iget-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->e:Ljava/lang/String;

    .line 1390468
    iget-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1390459
    new-instance v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;-><init>()V

    .line 1390460
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1390461
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1390465
    iget-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->f:Ljava/lang/String;

    .line 1390466
    iget-object v0, p0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1390464
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->j()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1390463
    const v0, 0x102e85c4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1390462
    const v0, -0x1bce060e

    return v0
.end method
