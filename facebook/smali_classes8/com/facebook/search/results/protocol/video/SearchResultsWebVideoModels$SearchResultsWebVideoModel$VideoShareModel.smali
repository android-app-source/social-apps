.class public final Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/A57;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6ef34a69
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620595
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620618
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1620619
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1620620
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1620643
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1620644
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1620645
    return-void
.end method

.method public static a(LX/A57;)Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;
    .locals 8

    .prologue
    .line 1620621
    if-nez p0, :cond_0

    .line 1620622
    const/4 p0, 0x0

    .line 1620623
    :goto_0
    return-object p0

    .line 1620624
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    if-eqz v0, :cond_1

    .line 1620625
    check-cast p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    goto :goto_0

    .line 1620626
    :cond_1
    new-instance v0, LX/A5A;

    invoke-direct {v0}, LX/A5A;-><init>()V

    .line 1620627
    invoke-interface {p0}, LX/A57;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A5A;->a:Ljava/lang/String;

    .line 1620628
    invoke-interface {p0}, LX/A57;->b()I

    move-result v1

    iput v1, v0, LX/A5A;->b:I

    .line 1620629
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1620630
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1620631
    iget-object v3, v0, LX/A5A;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1620632
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1620633
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 1620634
    iget v3, v0, LX/A5A;->b:I

    invoke-virtual {v2, v6, v3, v7}, LX/186;->a(III)V

    .line 1620635
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1620636
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1620637
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1620638
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1620639
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1620640
    new-instance v3, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;-><init>(LX/15i;)V

    .line 1620641
    move-object p0, v3

    .line 1620642
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1620608
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620609
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1620610
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1620611
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1620612
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620613
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620614
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1620615
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620616
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620617
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620606
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->e:Ljava/lang/String;

    .line 1620607
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1620603
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1620604
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->f:I

    .line 1620605
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1620601
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620602
    iget v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1620598
    new-instance v0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;-><init>()V

    .line 1620599
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1620600
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1620597
    const v0, -0x5f0df366

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1620596
    const v0, 0x15b4bc44

    return v0
.end method
