.class public final Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/A4u;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x90e04aa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Serializer;
.end annotation


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:I

.field private D:I

.field private e:J

.field private f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:D

.field private i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:I

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:I

.field private s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:D

.field private u:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:I

.field private w:I

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:D

.field private z:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620182
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620119
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1620183
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1620184
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620185
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1620186
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620187
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l:Ljava/lang/String;

    .line 1620188
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620189
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1620190
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620191
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->u:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->u:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1620192
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->u:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620193
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->x:Ljava/lang/String;

    .line 1620194
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620195
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->A:Ljava/lang/String;

    .line 1620196
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->A:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620197
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->B:Ljava/lang/String;

    .line 1620198
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->B:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final N()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620199
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1620200
    iget-object v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final V()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1620201
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620202
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->e:J

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 1620121
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620122
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1620123
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1620124
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1620125
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1620126
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1620127
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1620128
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1620129
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1620130
    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1620131
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1620132
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1620133
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1620134
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->h:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1620135
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1620136
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1620137
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->k:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620138
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1620139
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620140
    const/16 v0, 0x9

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->n:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620141
    const/16 v0, 0xa

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->o:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620142
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->p:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1620143
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->q:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1620144
    const/16 v0, 0xd

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->r:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620145
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1620146
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->t:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1620147
    const/16 v0, 0x10

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1620148
    const/16 v0, 0x11

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->v:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620149
    const/16 v0, 0x12

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->w:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620150
    const/16 v0, 0x13

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1620151
    const/16 v1, 0x14

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->y:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1620152
    const/16 v1, 0x15

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->z:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1620153
    const/16 v0, 0x16

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1620154
    const/16 v0, 0x17

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 1620155
    const/16 v0, 0x18

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->C:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620156
    const/16 v0, 0x19

    iget v1, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->D:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1620157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620158
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1620159
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620160
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1620161
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1620162
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1620163
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    .line 1620164
    iput-object v0, v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1620165
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1620166
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1620167
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1620168
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    .line 1620169
    iput-object v0, v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1620170
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1620171
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1620172
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1620173
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    .line 1620174
    iput-object v0, v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1620175
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1620176
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1620177
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1620178
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    .line 1620179
    iput-object v0, v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->u:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1620180
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620181
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620082
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1620083
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1620084
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->e:J

    .line 1620085
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->g:Z

    .line 1620086
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->h:D

    .line 1620087
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j:Z

    .line 1620088
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->k:I

    .line 1620089
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m:I

    .line 1620090
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->n:I

    .line 1620091
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->o:I

    .line 1620092
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->p:Z

    .line 1620093
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->q:Z

    .line 1620094
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->r:I

    .line 1620095
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->t:D

    .line 1620096
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->v:I

    .line 1620097
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->w:I

    .line 1620098
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->y:D

    .line 1620099
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->z:D

    .line 1620100
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->C:I

    .line 1620101
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->D:I

    .line 1620102
    return-void
.end method

.method public final synthetic aB()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620103
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->m()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final aE()I
    .locals 2

    .prologue
    .line 1620104
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620105
    iget v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->v:I

    return v0
.end method

.method public final aF()I
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 1620106
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620107
    iget v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->w:I

    return v0
.end method

.method public final ah()Z
    .locals 2

    .prologue
    .line 1620108
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620109
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->j:Z

    return v0
.end method

.method public final ap()Z
    .locals 2

    .prologue
    .line 1620110
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620111
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->p:Z

    return v0
.end method

.method public final au()I
    .locals 2

    .prologue
    .line 1620112
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620113
    iget v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->r:I

    return v0
.end method

.method public final synthetic av()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1620114
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1620115
    new-instance v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;-><init>()V

    .line 1620116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1620117
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1620118
    const v0, -0x5d85d6cd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1620120
    const v0, 0x4ed245b

    return v0
.end method
