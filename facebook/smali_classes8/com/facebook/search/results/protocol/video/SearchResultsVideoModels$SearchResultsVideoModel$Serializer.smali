.class public final Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1620080
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    new-instance v1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1620081
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1619969
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 1619971
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1619972
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 1619973
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1619974
    invoke-virtual {v1, v0, v5, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1619975
    cmp-long v4, v2, v8

    if-eqz v4, :cond_0

    .line 1619976
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619977
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1619978
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1619979
    if-eqz v2, :cond_1

    .line 1619980
    const-string v3, "creation_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619981
    invoke-static {v1, v2, p1, p2}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1619982
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1619983
    if-eqz v2, :cond_2

    .line 1619984
    const-string v3, "enable_focus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619985
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1619986
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1619987
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_3

    .line 1619988
    const-string v4, "focus_width_degrees"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619989
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1619990
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1619991
    if-eqz v2, :cond_4

    .line 1619992
    const-string v3, "guided_tour"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619993
    invoke-static {v1, v2, p1, p2}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1619994
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1619995
    if-eqz v2, :cond_5

    .line 1619996
    const-string v3, "has_viewer_watched_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619997
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1619998
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1619999
    if-eqz v2, :cond_6

    .line 1620000
    const-string v3, "height"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620001
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620002
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1620003
    if-eqz v2, :cond_7

    .line 1620004
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620005
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1620006
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620007
    if-eqz v2, :cond_8

    .line 1620008
    const-string v3, "initial_view_heading_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620009
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620010
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620011
    if-eqz v2, :cond_9

    .line 1620012
    const-string v3, "initial_view_pitch_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620013
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620014
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620015
    if-eqz v2, :cond_a

    .line 1620016
    const-string v3, "initial_view_roll_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620017
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620018
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1620019
    if-eqz v2, :cond_b

    .line 1620020
    const-string v3, "is_live_streaming"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620021
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1620022
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1620023
    if-eqz v2, :cond_c

    .line 1620024
    const-string v3, "is_spherical"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620025
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1620026
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620027
    if-eqz v2, :cond_d

    .line 1620028
    const-string v3, "live_viewer_count_read_only"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620029
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620030
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1620031
    if-eqz v2, :cond_e

    .line 1620032
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620033
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1620034
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1620035
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_f

    .line 1620036
    const-string v4, "off_focus_level"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620037
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1620038
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1620039
    if-eqz v2, :cond_10

    .line 1620040
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620041
    invoke-static {v1, v2, p1}, LX/A4z;->a(LX/15i;ILX/0nX;)V

    .line 1620042
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620043
    if-eqz v2, :cond_11

    .line 1620044
    const-string v3, "play_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620045
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620046
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620047
    if-eqz v2, :cond_12

    .line 1620048
    const-string v3, "playable_duration"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620049
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620050
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1620051
    if-eqz v2, :cond_13

    .line 1620052
    const-string v3, "projection_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620053
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1620054
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1620055
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_14

    .line 1620056
    const-string v4, "sphericalFullscreenAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620057
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1620058
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1620059
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_15

    .line 1620060
    const-string v4, "sphericalInlineAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620061
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1620062
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1620063
    if-eqz v2, :cond_16

    .line 1620064
    const-string v3, "sphericalPlayableUrlHdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620065
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1620066
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1620067
    if-eqz v2, :cond_17

    .line 1620068
    const-string v3, "sphericalPlayableUrlSdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620069
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1620070
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620071
    if-eqz v2, :cond_18

    .line 1620072
    const-string v3, "sphericalPreferredFov"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620073
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620074
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1620075
    if-eqz v2, :cond_19

    .line 1620076
    const-string v3, "width"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1620077
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1620078
    :cond_19
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1620079
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1619970
    check-cast p1, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$Serializer;->a(Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel;LX/0nX;LX/0my;)V

    return-void
.end method
