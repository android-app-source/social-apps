.class public final Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/A55;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620453
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1620452
    const-class v0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1620450
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1620451
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1620448
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1620449
    iget v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1620454
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620455
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1620456
    iget v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1620457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620458
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1620445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1620446
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1620447
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1620442
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1620443
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel;->e:I

    .line 1620444
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1620439
    new-instance v0, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$AllShareStoriesModel;-><init>()V

    .line 1620440
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1620441
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1620438
    const v0, -0x4c35daf6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1620437
    const v0, -0x3eaf5fad

    return v0
.end method
