.class public Lcom/facebook/stickers/ui/StickerDraweeView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# static fields
.field public static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final k:LX/1Up;


# instance fields
.field public c:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8jI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3dt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/8mv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<[",
            "LX/1bf;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1399827
    const-class v0, Lcom/facebook/stickers/ui/StickerDraweeView;

    sput-object v0, Lcom/facebook/stickers/ui/StickerDraweeView;->j:Ljava/lang/Class;

    .line 1399828
    sget-object v0, LX/1Up;->c:LX/1Up;

    sput-object v0, Lcom/facebook/stickers/ui/StickerDraweeView;->k:LX/1Up;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1399829
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1399830
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->d()V

    .line 1399831
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 0

    .prologue
    .line 1399832
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 1399833
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->d()V

    .line 1399834
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1399835
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1399836
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->d()V

    .line 1399837
    return-void
.end method

.method private a([LX/1bf;Ljava/lang/String;)LX/1bf;
    .locals 4
    .param p1    # [LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1399838
    const/4 v0, 0x0

    .line 1399839
    iget-object v1, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->i:LX/0Uh;

    const/16 v2, 0x1de

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1399840
    iget-object v1, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->e:LX/3dt;

    invoke-virtual {v1, p2}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v1

    .line 1399841
    if-eqz v1, :cond_1

    .line 1399842
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1399843
    iget-object p0, v1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez p0, :cond_0

    iget-object p0, v1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-nez p0, :cond_0

    iget-object p0, v1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz p0, :cond_0

    .line 1399844
    iget-object p0, v1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-static {p0, v2, v0}, LX/8mv;->a(Landroid/net/Uri;LX/1bZ;LX/33B;)LX/1bf;

    move-result-object v2

    .line 1399845
    :cond_0
    move-object v0, v2

    .line 1399846
    move-object v0, v0

    .line 1399847
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1399848
    aget-object v1, p1, v3

    invoke-static {v1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v1

    .line 1399849
    iget-object v2, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v2

    .line 1399850
    invoke-virtual {v1, v0}, LX/1bX;->b(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1399851
    :cond_1
    return-object v0
.end method

.method private a(LX/8mj;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8mj;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<[",
            "LX/1bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1399852
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->e:LX/3dt;

    iget-object v1, p1, LX/8mj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 1399853
    if-eqz v0, :cond_0

    .line 1399854
    invoke-direct {p0, v0, p1}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Lcom/facebook/stickers/model/Sticker;LX/8mj;)[LX/1bf;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1399855
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/stickers/ui/StickerDraweeView;->b(LX/8mj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/stickers/ui/StickerDraweeView;LX/0Sh;LX/8jI;LX/3dt;Ljava/util/concurrent/Executor;LX/8mv;LX/1Ad;LX/0Uh;)V
    .locals 0

    .prologue
    .line 1399856
    iput-object p1, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->c:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->d:LX/8jI;

    iput-object p3, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->e:LX/3dt;

    iput-object p4, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->f:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->g:LX/8mv;

    iput-object p6, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->h:LX/1Ad;

    iput-object p7, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->i:LX/0Uh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/stickers/ui/StickerDraweeView;

    invoke-static {v7}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v7}, LX/8jI;->a(LX/0QB;)LX/8jI;

    move-result-object v2

    check-cast v2, LX/8jI;

    invoke-static {v7}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v3

    check-cast v3, LX/3dt;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v7}, LX/8mv;->b(LX/0QB;)LX/8mv;

    move-result-object v5

    check-cast v5, LX/8mv;

    invoke-static {v7}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-static {v7}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static/range {v0 .. v7}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Lcom/facebook/stickers/ui/StickerDraweeView;LX/0Sh;LX/8jI;LX/3dt;Ljava/util/concurrent/Executor;LX/8mv;LX/1Ad;LX/0Uh;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1399822
    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1399823
    invoke-static {p1}, LX/8jU;->a(Ljava/lang/String;)I

    move-result v0

    .line 1399824
    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1399825
    invoke-static {p0, v0, v0}, LX/46y;->a(Landroid/view/View;II)V

    .line 1399826
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/stickers/model/Sticker;LX/8mj;)[LX/1bf;
    .locals 4

    .prologue
    .line 1399801
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399802
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->g:LX/8mv;

    const/4 p0, 0x0

    .line 1399803
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1399804
    new-instance v1, LX/8mi;

    invoke-direct {v1}, LX/8mi;-><init>()V

    .line 1399805
    if-nez p2, :cond_2

    .line 1399806
    :goto_0
    move-object v3, v1

    .line 1399807
    invoke-static {p1}, LX/4m9;->a(Lcom/facebook/stickers/model/Sticker;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/8mv;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1399808
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, LX/8mi;->a(Z)LX/8mi;

    move-result-object v1

    invoke-virtual {v1}, LX/8mi;->a()LX/8mj;

    move-result-object v1

    invoke-static {p1, v1}, LX/8mv;->c(Lcom/facebook/stickers/model/Sticker;LX/8mj;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1399809
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LX/1bf;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/1bf;

    move-object v0, v1

    .line 1399810
    return-object v0

    .line 1399811
    :cond_0
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 1399812
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    iget-object v3, p2, LX/8mj;->d:LX/33B;

    invoke-static {v1, p0, v3}, LX/8mv;->a(Landroid/net/Uri;LX/1bZ;LX/33B;)LX/1bf;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1399813
    :cond_1
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    iget-object v3, p2, LX/8mj;->d:LX/33B;

    invoke-static {v1, p0, v3}, LX/8mv;->a(Landroid/net/Uri;LX/1bZ;LX/33B;)LX/1bf;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1399814
    :cond_2
    iget-object v3, p2, LX/8mj;->a:Ljava/lang/String;

    iput-object v3, v1, LX/8mi;->f:Ljava/lang/String;

    .line 1399815
    iget v3, p2, LX/8mj;->e:I

    iput v3, v1, LX/8mi;->b:I

    .line 1399816
    iget-boolean v3, p2, LX/8mj;->h:Z

    iput-boolean v3, v1, LX/8mi;->c:Z

    .line 1399817
    iget-boolean v3, p2, LX/8mj;->f:Z

    iput-boolean v3, v1, LX/8mi;->d:Z

    .line 1399818
    iget-boolean v3, p2, LX/8mj;->g:Z

    iput-boolean v3, v1, LX/8mi;->e:Z

    .line 1399819
    iget-object v3, p2, LX/8mj;->b:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v3, v1, LX/8mi;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 1399820
    iget-object v3, p2, LX/8mj;->c:LX/1Ai;

    iput-object v3, v1, LX/8mi;->h:LX/1Ai;

    .line 1399821
    iget-object v3, p2, LX/8mj;->d:LX/33B;

    iput-object v3, v1, LX/8mi;->i:LX/33B;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/stickers/ui/StickerDraweeView;Lcom/facebook/stickers/model/Sticker;LX/8mj;)[LX/1bf;
    .locals 1

    .prologue
    .line 1399800
    invoke-direct {p0, p1, p2}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Lcom/facebook/stickers/model/Sticker;LX/8mj;)[LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/stickers/ui/StickerDraweeView;[LX/1bf;LX/8mj;)V
    .locals 2
    .param p0    # Lcom/facebook/stickers/ui/StickerDraweeView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1399762
    if-nez p1, :cond_0

    .line 1399763
    :goto_0
    return-void

    .line 1399764
    :cond_0
    iget-object v0, p2, LX/8mj;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/facebook/stickers/ui/StickerDraweeView;->a([LX/1bf;Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 1399765
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->h:LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p2, LX/8mj;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p2, LX/8mj;->c:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1399766
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method private b(LX/8mj;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8mj;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<[",
            "LX/1bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1399798
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->d:LX/8jI;

    iget-object v1, p1, LX/8mj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8jI;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1399799
    new-instance v1, LX/8ml;

    invoke-direct {v1, p0, p1}, LX/8ml;-><init>(Lcom/facebook/stickers/ui/StickerDraweeView;LX/8mj;)V

    iget-object v2, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1399796
    const-class v0, Lcom/facebook/stickers/ui/StickerDraweeView;

    invoke-static {v0, p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1399797
    return-void
.end method

.method private setPlaceHolderId(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1399784
    const-string v0, "227878347358915"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399785
    const v0, 0x7f02189e

    .line 1399786
    :goto_0
    move v1, v0

    .line 1399787
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/facebook/stickers/ui/StickerDraweeView;->k:LX/1Up;

    invoke-virtual {v0, v1, v2}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 1399788
    return-void

    .line 1399789
    :cond_0
    const-string v0, "369239263222822"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1399790
    const v0, 0x7f02189f

    goto :goto_0

    .line 1399791
    :cond_1
    const-string v0, "369239343222814"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1399792
    const v0, 0x7f02189e

    goto :goto_0

    .line 1399793
    :cond_2
    const-string v0, "369239383222810"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1399794
    const v0, 0x7f02189d

    goto :goto_0

    .line 1399795
    :cond_3
    const v0, 0x7f021323

    goto :goto_0
.end method


# virtual methods
.method public setDrawableResourceId(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1399779
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1399780
    iput-object v1, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->m:Ljava/lang/String;

    .line 1399781
    invoke-virtual {p0, v1}, Lcom/facebook/stickers/ui/StickerDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 1399782
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/facebook/stickers/ui/StickerDraweeView;->k:LX/1Up;

    invoke-virtual {v0, v1, v2}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 1399783
    return-void
.end method

.method public setSticker(LX/8mj;)V
    .locals 3

    .prologue
    .line 1399767
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1399768
    iget-object v0, p1, LX/8mj;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->m:Ljava/lang/String;

    .line 1399769
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->m:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/stickers/ui/StickerDraweeView;->setPlaceHolderId(Ljava/lang/String;)V

    .line 1399770
    iget-boolean v0, p1, LX/8mj;->f:Z

    if-eqz v0, :cond_0

    .line 1399771
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->m:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Ljava/lang/String;)V

    .line 1399772
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->l:LX/1Mv;

    if-eqz v0, :cond_1

    .line 1399773
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->l:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1399774
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(LX/8mj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1399775
    new-instance v1, LX/8mk;

    invoke-direct {v1, p0, p1}, LX/8mk;-><init>(Lcom/facebook/stickers/ui/StickerDraweeView;LX/8mj;)V

    invoke-static {v1}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v1

    .line 1399776
    iget-object v2, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1399777
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerDraweeView;->l:LX/1Mv;

    .line 1399778
    return-void
.end method
