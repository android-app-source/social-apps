.class public Lcom/facebook/stickers/ui/StickerView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# static fields
.field private static final d:LX/0wT;


# instance fields
.field public c:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0wd;

.field private f:Z

.field private final g:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1400245
    const-wide/high16 v0, 0x4054000000000000L    # 80.0

    const-wide/high16 v2, 0x4022000000000000L    # 9.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/ui/StickerView;->d:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1400241
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1400242
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->g:Landroid/graphics/Rect;

    .line 1400243
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerView;->e()V

    .line 1400244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 1

    .prologue
    .line 1400237
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 1400238
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->g:Landroid/graphics/Rect;

    .line 1400239
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerView;->e()V

    .line 1400240
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1400233
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1400234
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->g:Landroid/graphics/Rect;

    .line 1400235
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerView;->e()V

    .line 1400236
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1400229
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1400230
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->g:Landroid/graphics/Rect;

    .line 1400231
    invoke-direct {p0}, Lcom/facebook/stickers/ui/StickerView;->e()V

    .line 1400232
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/stickers/ui/StickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/stickers/ui/StickerView;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->c:LX/0wW;

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1400220
    const-class v1, Lcom/facebook/stickers/ui/StickerView;

    invoke-static {v1, p0}, Lcom/facebook/stickers/ui/StickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1400221
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1400222
    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f010525

    invoke-virtual {v2, v3, v1, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1400223
    iget v2, v1, Landroid/util/TypedValue;->type:I

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/facebook/stickers/ui/StickerView;->f:Z

    .line 1400224
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->c:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->e:LX/0wd;

    .line 1400225
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->e:LX/0wd;

    sget-object v1, Lcom/facebook/stickers/ui/StickerView;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1400226
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->e:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1400227
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->e:LX/0wd;

    new-instance v1, LX/8mw;

    invoke-direct {v1, p0}, LX/8mw;-><init>(Lcom/facebook/stickers/ui/StickerView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1400228
    return-void
.end method


# virtual methods
.method public final a(FF)Z
    .locals 5

    .prologue
    .line 1400218
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->g:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/stickers/ui/StickerView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1400219
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->g:Landroid/graphics/Rect;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1400215
    iget-boolean v0, p0, Lcom/facebook/stickers/ui/StickerView;->f:Z

    if-nez v0, :cond_0

    .line 1400216
    :goto_0
    return-void

    .line 1400217
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->e:LX/0wd;

    const-wide v2, 0x3fecccccc0000000L    # 0.8999999761581421

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1400212
    iget-boolean v0, p0, Lcom/facebook/stickers/ui/StickerView;->f:Z

    if-nez v0, :cond_0

    .line 1400213
    :goto_0
    return-void

    .line 1400214
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerView;->e:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method
