.class public Lcom/facebook/stickers/ui/StickerTagItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1400139
    const-class v0, Lcom/facebook/stickers/ui/StickerTagItemView;

    const-string v1, "stickers_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/ui/StickerTagItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1400140
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1400141
    const-class v0, Lcom/facebook/stickers/ui/StickerTagItemView;

    invoke-static {v0, p0}, Lcom/facebook/stickers/ui/StickerTagItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1400142
    const v0, 0x7f021313

    invoke-virtual {p0, v0}, Lcom/facebook/stickers/ui/StickerTagItemView;->setBackgroundResource(I)V

    .line 1400143
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/stickers/ui/StickerTagItemView;->setGravity(I)V

    .line 1400144
    const v0, 0x7f030da0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1400145
    const v0, 0x7f0d217d

    invoke-virtual {p0, v0}, Lcom/facebook/stickers/ui/StickerTagItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerTagItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1400146
    const v0, 0x7f0d217c

    invoke-virtual {p0, v0}, Lcom/facebook/stickers/ui/StickerTagItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerTagItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1400147
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/stickers/ui/StickerTagItemView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object p0, p1, Lcom/facebook/stickers/ui/StickerTagItemView;->a:LX/1Ad;

    return-void
.end method
