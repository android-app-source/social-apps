.class public Lcom/facebook/stickers/ui/StickerUiModule;
.super LX/0Q6;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1400156
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1400157
    return-void
.end method

.method public static a(Landroid/content/res/Resources;LX/1Ad;)Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/stickers/ui/ForStickerPreview;
    .end annotation

    .prologue
    .line 1400148
    new-instance v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;-><init>(LX/1Ad;)V

    .line 1400149
    const v1, 0x7f0b07d1

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1400150
    iput v1, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->f:I

    .line 1400151
    const-class v1, Lcom/facebook/stickers/ui/ForStickerPreview;

    const-string v2, "sticker_preview_dialog"

    const/4 p1, 0x0

    .line 1400152
    new-instance v3, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0, p1, v2, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 1400153
    iput-object v1, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 1400154
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1400155
    return-void
.end method
