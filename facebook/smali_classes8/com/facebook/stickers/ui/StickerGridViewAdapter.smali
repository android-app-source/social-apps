.class public Lcom/facebook/stickers/ui/StickerGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Ljava/lang/Boolean;

.field private final b:LX/8mv;

.field private final c:LX/1Ad;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/lang/String;

.field private final f:LX/7kc;

.field private final g:Landroid/view/LayoutInflater;

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(LX/8mv;LX/1Ad;LX/0Or;Landroid/content/Context;Ljava/lang/String;LX/7kc;)V
    .locals 2
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/abtest/IsAnimatedStickersInGridEnabled;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/7kc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8mv;",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "LX/7kc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1399988
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1399989
    const-class v0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    const-string v1, "sticker_keyboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 1399990
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a:Ljava/lang/Boolean;

    .line 1399991
    iput-object p1, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->b:LX/8mv;

    .line 1399992
    iput-object p2, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->c:LX/1Ad;

    .line 1399993
    iput-object p4, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->d:Landroid/content/Context;

    .line 1399994
    iput-object p5, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->e:Ljava/lang/String;

    .line 1399995
    iput-object p6, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1399996
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1399997
    iget-object v1, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->g:Landroid/view/LayoutInflater;

    .line 1399998
    return-void
.end method

.method private a(I)Lcom/facebook/stickers/model/Sticker;
    .locals 1

    .prologue
    .line 1399999
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1400000
    iput-object p1, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->h:LX/0Px;

    .line 1400001
    const v0, 0x65ebcd3f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1400002
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1400003
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1400004
    invoke-direct {p0, p1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(I)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1400005
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1400006
    invoke-direct {p0, p1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(I)Lcom/facebook/stickers/model/Sticker;

    move-result-object v3

    .line 1400007
    check-cast p2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1400008
    if-nez p2, :cond_0

    .line 1400009
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->g:Landroid/view/LayoutInflater;

    const v2, 0x7f030d91

    const/4 v4, 0x0

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1400010
    iget-object v2, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1400011
    iget v4, v2, LX/7kc;->i:I

    move v2, v4

    .line 1400012
    iget-object v4, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1400013
    iget v5, v4, LX/7kc;->j:I

    move v4, v5

    .line 1400014
    iget-object v5, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1400015
    iget v6, v5, LX/7kc;->c:I

    move v5, v6

    .line 1400016
    iget-object v6, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1400017
    iget v7, v6, LX/7kc;->e:I

    move v6, v7

    .line 1400018
    iget-object v7, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1400019
    iget v8, v7, LX/7kc;->d:I

    move v7, v8

    .line 1400020
    iget-object v8, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->f:LX/7kc;

    .line 1400021
    iget v9, v8, LX/7kc;->f:I

    move v8, v9

    .line 1400022
    new-instance v9, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v9, v2, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1400023
    invoke-virtual {v0, v5, v6, v7, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    move-object p2, v0

    .line 1400024
    :cond_0
    invoke-static {v3}, LX/4m9;->a(Lcom/facebook/stickers/model/Sticker;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1400025
    const/4 v4, 0x1

    .line 1400026
    new-instance v1, LX/8mi;

    invoke-direct {v1}, LX/8mi;-><init>()V

    const/4 v2, 0x0

    .line 1400027
    iput v2, v1, LX/8mi;->b:I

    .line 1400028
    move-object v1, v1

    .line 1400029
    invoke-virtual {v1, v4}, LX/8mi;->a(Z)LX/8mi;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/8mi;->a(Z)LX/8mi;

    move-result-object v1

    invoke-virtual {v1}, LX/8mi;->a()LX/8mj;

    move-result-object v1

    .line 1400030
    invoke-static {v3, v1}, LX/8mv;->c(Lcom/facebook/stickers/model/Sticker;LX/8mj;)Ljava/util/List;

    move-result-object v2

    .line 1400031
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [LX/1bf;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [LX/1bf;

    move-object v1, v2

    .line 1400032
    move-object v2, v1

    .line 1400033
    invoke-static {v3}, LX/8mv;->c(Lcom/facebook/stickers/model/Sticker;)LX/1bf;

    move-result-object v1

    .line 1400034
    new-instance v0, LX/8mr;

    invoke-direct {v0, p0}, LX/8mr;-><init>(Lcom/facebook/stickers/ui/StickerGridViewAdapter;)V

    move-object v10, v0

    move-object v0, v2

    move-object v2, v1

    move-object v1, v10

    .line 1400035
    :goto_0
    iget-object v3, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->c:LX/1Ad;

    iget-object v4, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1400036
    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1400037
    iget-object v0, p0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->d:Landroid/content/Context;

    const v1, 0x7f0804dd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1400038
    return-object p2

    .line 1400039
    :cond_1
    invoke-static {v3}, LX/8mv;->a(Lcom/facebook/stickers/model/Sticker;)[LX/1bf;

    move-result-object v0

    move-object v2, v1

    .line 1400040
    goto :goto_0
.end method
