.class public Lcom/facebook/stickers/store/StickerStoreActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0h5;

.field private B:LX/63L;

.field private C:Lcom/facebook/stickers/store/StickerStoreFragment;

.field private D:Lcom/facebook/stickers/store/StickerStorePackFragment;

.field public q:LX/0So;

.field public r:LX/11i;

.field private s:LX/63V;

.field private t:LX/00H;

.field public u:LX/03V;

.field private v:LX/67X;

.field private w:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

.field private x:Z

.field public y:LX/4m4;

.field private z:LX/3u1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398708
    const-class v0, Lcom/facebook/stickers/store/StickerStoreActivity;

    sput-object v0, Lcom/facebook/stickers/store/StickerStoreActivity;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1398709
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1398710
    return-void
.end method

.method private a(LX/0So;LX/11i;LX/63V;LX/00H;LX/03V;LX/67X;Lcom/facebook/stickers/client/StickerToPackMetadataLoader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1398610
    iput-object p1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->q:LX/0So;

    .line 1398611
    iput-object p2, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->r:LX/11i;

    .line 1398612
    iput-object p3, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->s:LX/63V;

    .line 1398613
    iput-object p4, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->t:LX/00H;

    .line 1398614
    iput-object p5, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->u:LX/03V;

    .line 1398615
    iput-object p6, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->v:LX/67X;

    .line 1398616
    iput-object p7, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->w:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    .line 1398617
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/stickers/store/StickerStoreActivity;

    invoke-static {v7}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {v7}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v2

    check-cast v2, LX/11i;

    invoke-static {v7}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v3

    check-cast v3, LX/63V;

    const-class v4, LX/00H;

    invoke-interface {v7, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/00H;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v7}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v6

    check-cast v6, LX/67X;

    invoke-static {v7}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->b(LX/0QB;)Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    move-result-object v7

    check-cast v7, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/stickers/store/StickerStoreActivity;->a(LX/0So;LX/11i;LX/63V;LX/00H;LX/03V;LX/67X;Lcom/facebook/stickers/client/StickerToPackMetadataLoader;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1398711
    new-instance v0, LX/8jS;

    invoke-direct {v0, p1}, LX/8jS;-><init>(Ljava/lang/String;)V

    .line 1398712
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->w:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-virtual {v1}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a()V

    .line 1398713
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->w:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    new-instance v2, LX/8mI;

    invoke-direct {v2, p0, p2}, LX/8mI;-><init>(Lcom/facebook/stickers/store/StickerStoreActivity;Z)V

    .line 1398714
    iput-object v2, v1, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    .line 1398715
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->w:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a(LX/8jS;)V

    .line 1398716
    return-void
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreActivity;Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1398717
    if-nez p2, :cond_1

    .line 1398718
    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->finish()V

    .line 1398719
    :cond_0
    :goto_0
    return-void

    .line 1398720
    :cond_1
    instance-of v0, p1, Lcom/facebook/stickers/store/StickerStoreFragment;

    if-eqz v0, :cond_0

    .line 1398721
    const-string v0, "stickerPack"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1398722
    const-string v0, "isDownloaded"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 1398723
    const-string v0, "price"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1398724
    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/stickers/store/StickerStoreActivity;->a$redex0(Lcom/facebook/stickers/store/StickerStoreActivity;Lcom/facebook/stickers/model/StickerPack;ZZLjava/lang/String;Z)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreActivity;Lcom/facebook/stickers/model/StickerPack;ZZLjava/lang/String;Z)V
    .locals 7

    .prologue
    .line 1398725
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v6

    .line 1398726
    invoke-virtual {v6}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1398727
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreActivity;->p:Ljava/lang/Class;

    const-string v1, "Unable to safely commit fragment transactions--aborting operation."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1398728
    :cond_0
    :goto_0
    return-void

    .line 1398729
    :cond_1
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1398730
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    iget-object v5, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    .line 1398731
    iput-object v1, v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1398732
    iput-boolean v2, v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->l:Z

    .line 1398733
    iput-boolean v3, v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->m:Z

    .line 1398734
    iput-object v4, v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->n:Ljava/lang/String;

    .line 1398735
    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->B:LX/0am;

    .line 1398736
    invoke-static {v0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->c(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    .line 1398737
    invoke-virtual {v6}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1398738
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "storeFragment"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1398739
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-virtual {v0, v1}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1398740
    if-eqz p5, :cond_2

    .line 1398741
    const-string v1, "packFragment"

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 1398742
    :cond_2
    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public static b(Lcom/facebook/stickers/model/StickerPack;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1398743
    iget v0, p0, Lcom/facebook/stickers/model/StickerPack;->i:I

    move v0, v0

    .line 1398744
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1398745
    :goto_0
    return-object v0

    .line 1398746
    :cond_0
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "$0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 1398747
    iget v1, p0, Lcom/facebook/stickers/model/StickerPack;->i:I

    move v1, v1

    .line 1398748
    int-to-double v2, v1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/stickers/store/StickerStoreActivity;)V
    .locals 2

    .prologue
    .line 1398700
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 1398701
    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1398702
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreActivity;->p:Ljava/lang/Class;

    const-string v1, "Unable to safely commit fragment transactions--aborting operation."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1398703
    :cond_0
    :goto_0
    return-void

    .line 1398704
    :cond_1
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1398705
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1398706
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->C:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v0, v1}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1398707
    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method private n()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1398749
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    .line 1398750
    const-string v0, "storeFragment"

    invoke-virtual {v2, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/store/StickerStoreFragment;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->C:Lcom/facebook/stickers/store/StickerStoreFragment;

    .line 1398751
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->C:Lcom/facebook/stickers/store/StickerStoreFragment;

    if-eqz v0, :cond_0

    move v0, v1

    .line 1398752
    :goto_0
    return v0

    .line 1398753
    :cond_0
    invoke-virtual {v2}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1398754
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreActivity;->p:Ljava/lang/Class;

    const-string v1, "Unable to safely commit fragment transactions--aborting operation."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1398755
    const/4 v0, 0x0

    goto :goto_0

    .line 1398756
    :cond_1
    new-instance v0, Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-direct {v0}, Lcom/facebook/stickers/store/StickerStoreFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->C:Lcom/facebook/stickers/store/StickerStoreFragment;

    .line 1398757
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1398758
    const v3, 0x7f0d0553

    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->C:Lcom/facebook/stickers/store/StickerStoreFragment;

    const-string v5, "storeFragment"

    invoke-virtual {v0, v3, v4, v5}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1398759
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->C:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v0, v3}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1398760
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1398761
    invoke-virtual {v2}, LX/0gc;->b()Z

    move v0, v1

    .line 1398762
    goto :goto_0
.end method

.method private o()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1398686
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    .line 1398687
    const-string v0, "packFragment"

    invoke-virtual {v2, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/store/StickerStorePackFragment;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    .line 1398688
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    if-eqz v0, :cond_0

    move v0, v1

    .line 1398689
    :goto_0
    return v0

    .line 1398690
    :cond_0
    invoke-virtual {v2}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1398691
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreActivity;->p:Ljava/lang/Class;

    const-string v1, "Unable to safely commit fragment transactions--aborting operation."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1398692
    const/4 v0, 0x0

    goto :goto_0

    .line 1398693
    :cond_1
    new-instance v0, Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-direct {v0}, Lcom/facebook/stickers/store/StickerStorePackFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    .line 1398694
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1398695
    const v3, 0x7f0d0553

    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    const-string v5, "packFragment"

    invoke-virtual {v0, v3, v4, v5}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1398696
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->D:Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-virtual {v0, v3}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1398697
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1398698
    invoke-virtual {v2}, LX/0gc;->b()Z

    move v0, v1

    .line 1398699
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1398680
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1398681
    invoke-static {p0, p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1398682
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->s:LX/63V;

    invoke-virtual {v0}, LX/63V;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->x:Z

    .line 1398683
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->x:Z

    if-eqz v0, :cond_0

    .line 1398684
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->v:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 1398685
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1398676
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 1398677
    instance-of v0, p1, Lcom/facebook/base/fragment/NavigableFragment;

    if-nez v0, :cond_0

    .line 1398678
    :goto_0
    return-void

    .line 1398679
    :cond_0
    check-cast p1, Lcom/facebook/base/fragment/NavigableFragment;

    new-instance v0, LX/8mH;

    invoke-direct {v0, p0}, LX/8mH;-><init>(Lcom/facebook/stickers/store/StickerStoreActivity;)V

    invoke-interface {p1, v0}, Lcom/facebook/base/fragment/NavigableFragment;->a(LX/42n;)V

    goto :goto_0
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 1398675
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->z:LX/3u1;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 1398630
    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 1398631
    invoke-static {v10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1398632
    const-string v0, "stickerContext"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1398633
    const-string v0, "stickerContext"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/4m4;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    .line 1398634
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    if-nez v0, :cond_1

    .line 1398635
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->u:LX/03V;

    const-string v3, "error_no_sticker_context"

    const-string v4, "StickerStoreActivity was created with an intent that either did not have an EXTRA_STICKER_CONTEXT extra or an had an invalid value in this extra."

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    const/16 v4, 0xa

    .line 1398636
    iput v4, v3, LX/0VK;->e:I

    .line 1398637
    move-object v3, v3

    .line 1398638
    iput-boolean v8, v3, LX/0VK;->f:Z

    .line 1398639
    move-object v3, v3

    .line 1398640
    iput-boolean v1, v3, LX/0VK;->d:Z

    .line 1398641
    move-object v3, v3

    .line 1398642
    invoke-virtual {v3}, LX/0VK;->g()LX/0VG;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/03V;->a(LX/0VG;)V

    .line 1398643
    sget-object v0, LX/8mJ;->a:[I

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->t:LX/00H;

    .line 1398644
    iget-object v4, v3, LX/00H;->j:LX/01T;

    move-object v3, v4

    .line 1398645
    invoke-virtual {v3}, LX/01T;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1398646
    :goto_0
    sget-object v0, LX/4m4;->MESSENGER:LX/4m4;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    .line 1398647
    :cond_1
    :goto_1
    const-string v0, "stickerPack"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1398648
    const-string v0, "stickerPack"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1398649
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->r:LX/11i;

    sget-object v3, LX/8lJ;->d:LX/8lI;

    invoke-interface {v1, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    move v7, v8

    move-object v9, v0

    move-object v0, v1

    .line 1398650
    :goto_2
    const-string v1, "startDownload"

    invoke-virtual {v10, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 1398651
    if-eqz v0, :cond_2

    .line 1398652
    const-string v1, "StickerCreateStickerStoreActivity"

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->q:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x186fef86

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1398653
    :cond_2
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1398654
    const v0, 0x7f030d99

    invoke-virtual {p0, v0}, Lcom/facebook/stickers/store/StickerStoreActivity;->setContentView(I)V

    .line 1398655
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->x:Z

    if-eqz v0, :cond_5

    .line 1398656
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->v:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->z:LX/3u1;

    .line 1398657
    new-instance v0, LX/63L;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->z:LX/3u1;

    invoke-direct {v0, p0, v1}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->B:LX/63L;

    .line 1398658
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->B:LX/63L;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->A:LX/0h5;

    .line 1398659
    :goto_3
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->A:LX/0h5;

    const v1, 0x7f080332

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1398660
    if-eqz v7, :cond_6

    .line 1398661
    const-string v0, "stickerId"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1398662
    invoke-direct {p0, v0, v11}, Lcom/facebook/stickers/store/StickerStoreActivity;->a(Ljava/lang/String;Z)V

    .line 1398663
    :goto_4
    return-void

    .line 1398664
    :pswitch_0
    sget-object v0, LX/4m4;->MESSENGER:LX/4m4;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    goto :goto_1

    .line 1398665
    :pswitch_1
    sget-object v0, LX/4m4;->COMMENTS:LX/4m4;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    goto :goto_0

    .line 1398666
    :cond_3
    const-string v0, "stickerId"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v7, v1

    move-object v0, v2

    move-object v9, v2

    .line 1398667
    goto :goto_2

    .line 1398668
    :cond_4
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->r:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    move v7, v8

    move-object v9, v2

    goto :goto_2

    .line 1398669
    :cond_5
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1398670
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->A:LX/0h5;

    goto :goto_3

    .line 1398671
    :cond_6
    if-nez v9, :cond_7

    .line 1398672
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->m(Lcom/facebook/stickers/store/StickerStoreActivity;)V

    goto :goto_4

    .line 1398673
    :cond_7
    invoke-static {v9}, Lcom/facebook/stickers/store/StickerStoreActivity;->b(Lcom/facebook/stickers/model/StickerPack;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, v9

    move v2, v8

    move v3, v11

    move v5, v8

    .line 1398674
    invoke-static/range {v0 .. v5}, Lcom/facebook/stickers/store/StickerStoreActivity;->a$redex0(Lcom/facebook/stickers/store/StickerStoreActivity;Lcom/facebook/stickers/model/StickerPack;ZZLjava/lang/String;Z)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1398626
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 1398627
    iget-boolean v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->x:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1398628
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->B:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 1398629
    :cond_0
    return v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x10483903

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1398623
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1398624
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->w:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-virtual {v1}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a()V

    .line 1398625
    const/16 v1, 0x23

    const v2, 0xe1b3cf1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1398618
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_2

    .line 1398619
    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreActivity;->onBackPressed()V

    move v2, v1

    .line 1398620
    :goto_0
    if-nez v2, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    .line 1398621
    :cond_2
    iget-boolean v2, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->x:Z

    if-eqz v2, :cond_3

    .line 1398622
    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreActivity;->B:LX/63L;

    invoke-virtual {v2, p1}, LX/63L;->a(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_0
.end method
