.class public Lcom/facebook/stickers/store/StickerStoreFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/base/fragment/NavigableFragment;
.implements LX/0YZ;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field private B:Z

.field public C:LX/8mX;

.field public D:LX/8mX;

.field public E:Z

.field public F:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/4m4;",
            ">;"
        }
    .end annotation
.end field

.field private G:LX/0SG;

.field private b:Landroid/content/Context;

.field private c:LX/0Xl;

.field public d:LX/03V;

.field private e:LX/42n;

.field private f:LX/0aG;

.field private g:LX/0Yb;

.field private h:Lcom/facebook/stickers/client/StickerDownloadManager;

.field private i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private j:LX/8j7;

.field private k:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public l:LX/0So;

.field public m:LX/11i;

.field public n:LX/1CX;

.field private o:LX/0h5;

.field private p:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private q:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private r:Z

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/ViewGroup;

.field private w:Lcom/facebook/stickers/store/StickerStoreListView;

.field private x:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public y:LX/8ma;

.field public z:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1399229
    const-class v0, Lcom/facebook/stickers/store/StickerStoreFragment;

    sput-object v0, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1399169
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1399170
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    return-void
.end method

.method private a(LX/3do;LX/0rS;)LX/1ML;
    .locals 4

    .prologue
    .line 1399171
    new-instance v1, LX/3ea;

    invoke-direct {v1, p1, p2}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4m4;

    invoke-static {v0}, LX/8jc;->a(LX/4m4;)Ljava/lang/String;

    move-result-object v0

    .line 1399172
    iput-object v0, v1, LX/3ea;->c:Ljava/lang/String;

    .line 1399173
    move-object v0, v1

    .line 1399174
    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v0

    .line 1399175
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1399176
    const-string v2, "fetchStickerPacksParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1399177
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->f:LX/0aG;

    const-string v2, "fetch_sticker_packs"

    const v3, 0x53c260aa

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1399178
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 1399179
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1399180
    return-void
.end method

.method private a(LX/3do;LX/8mX;)V
    .locals 3

    .prologue
    .line 1399181
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->B:Z

    if-nez v0, :cond_1

    sget-object v0, LX/3do;->STORE_PACKS:LX/3do;

    if-ne p1, v0, :cond_1

    .line 1399182
    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 1399183
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->B:Z

    .line 1399184
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(LX/3do;LX/0rS;)LX/1ML;

    move-result-object v0

    .line 1399185
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->D:LX/8mX;

    if-eq v1, p2, :cond_0

    .line 1399186
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1399187
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Ljava/util/List;Z)V

    .line 1399188
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->n()V

    .line 1399189
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->E:Z

    if-nez v1, :cond_2

    .line 1399190
    :goto_1
    return-void

    .line 1399191
    :cond_1
    sget-object v0, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    goto :goto_0

    .line 1399192
    :cond_2
    new-instance v1, LX/8mV;

    invoke-direct {v1, p0, p2, p1}, LX/8mV;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;LX/8mX;LX/3do;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method

.method private a(Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Xl;LX/03V;LX/0aG;Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8j7;LX/0SG;LX/0So;LX/11i;LX/1CX;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1399193
    iput-object p1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->k:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    .line 1399194
    iput-object p2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->c:LX/0Xl;

    .line 1399195
    iput-object p3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->d:LX/03V;

    .line 1399196
    iput-object p4, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->f:LX/0aG;

    .line 1399197
    iput-object p5, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->h:Lcom/facebook/stickers/client/StickerDownloadManager;

    .line 1399198
    iput-object p6, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1399199
    iput-object p7, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->j:LX/8j7;

    .line 1399200
    iput-object p8, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->G:LX/0SG;

    .line 1399201
    sget-object v0, LX/8mX;->FEATURED:LX/8mX;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    .line 1399202
    iput-object p9, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->l:LX/0So;

    .line 1399203
    iput-object p10, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    .line 1399204
    iput-object p11, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->n:LX/1CX;

    .line 1399205
    return-void
.end method

.method private a(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 3

    .prologue
    .line 1399206
    const-string v0, "sticker_store"

    invoke-static {v0}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1399207
    const-string v1, "action"

    const-string v2, "sticker_pack_selected"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399208
    const-string v1, "sticker_pack"

    .line 1399209
    iget-object v2, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1399210
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399211
    const-string v1, "store_tab"

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    invoke-virtual {v2}, LX/8mX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399212
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->j:LX/8j7;

    invoke-virtual {v1, v0}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1399213
    return-void
.end method

.method private a(Lcom/facebook/stickers/model/StickerPack;LX/8ma;)V
    .locals 4

    .prologue
    .line 1399214
    invoke-virtual {p2, p1}, LX/8ma;->remove(Ljava/lang/Object;)V

    .line 1399215
    iget-object v0, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v0

    .line 1399216
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4m4;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399217
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1399218
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1399219
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399220
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1399221
    const-string v1, "stickerPacks"

    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->o()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1399222
    const-string v1, "deletedStickerPacks"

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/stickers/model/StickerPack;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1399223
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->f:LX/0aG;

    const-string v2, "set_downloaded_sticker_packs"

    const v3, 0x37cb6e31

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1399224
    new-instance v1, LX/8mM;

    invoke-direct {v1, p0, p2, p1}, LX/8mM;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;LX/8ma;Lcom/facebook/stickers/model/StickerPack;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1399225
    return-void

    .line 1399226
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1399227
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1399228
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/stickers/store/StickerStoreListItemView;)V
    .locals 2

    .prologue
    .line 1399230
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1399231
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1399232
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1399233
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1399234
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1399235
    :goto_0
    if-nez v0, :cond_1

    .line 1399236
    invoke-direct {p0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->b(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399237
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->h:Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-virtual {v0, p1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399238
    invoke-virtual {p2}, Lcom/facebook/stickers/store/StickerStoreListItemView;->a()V

    .line 1399239
    :cond_1
    return-void

    .line 1399240
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-static {v11}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v11}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-static {v11}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v11}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v11}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(LX/0QB;)Lcom/facebook/stickers/client/StickerDownloadManager;

    move-result-object v5

    check-cast v5, Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-static {v11}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v11}, LX/8j7;->a(LX/0QB;)LX/8j7;

    move-result-object v7

    check-cast v7, LX/8j7;

    invoke-static {v11}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v11}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    check-cast v9, LX/0So;

    invoke-static {v11}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v10

    check-cast v10, LX/11i;

    invoke-static {v11}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v11

    check-cast v11, LX/1CX;

    invoke-direct/range {v0 .. v11}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Xl;LX/03V;LX/0aG;Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8j7;LX/0SG;LX/0So;LX/11i;LX/1CX;)V

    return-void
.end method

.method private a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1399241
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    sget-object v1, LX/8mX;->OWNED:LX/8mX;

    if-ne v0, v1, :cond_0

    .line 1399242
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    invoke-direct {p0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->d(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->o()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/8ma;->a(Ljava/util/List;Ljava/util/LinkedHashMap;Z)V

    .line 1399243
    :goto_0
    return-void

    .line 1399244
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    sget-object v1, LX/8mX;->AVAILABLE:LX/8mX;

    if-ne v0, v1, :cond_1

    .line 1399245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1399246
    new-instance v1, LX/8mK;

    invoke-direct {v1, p0}, LX/8mK;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1399247
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->o()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p2}, LX/8ma;->a(Ljava/util/List;Ljava/util/LinkedHashMap;Z)V

    goto :goto_0

    .line 1399248
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->o()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, LX/8ma;->a(Ljava/util/List;Ljava/util/LinkedHashMap;Z)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Lcom/facebook/stickers/model/StickerPack;LX/8ma;ZLcom/facebook/stickers/store/StickerStoreListItemView;)V
    .locals 0

    .prologue
    .line 1399249
    if-eqz p3, :cond_0

    .line 1399250
    invoke-direct {p0, p1, p2}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Lcom/facebook/stickers/model/StickerPack;LX/8ma;)V

    .line 1399251
    :goto_0
    return-void

    .line 1399252
    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/stickers/store/StickerStoreListItemView;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Lcom/facebook/stickers/model/StickerPack;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1399253
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->e:LX/42n;

    if-eqz v0, :cond_0

    .line 1399254
    invoke-direct {p0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399255
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->k()V

    .line 1399256
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1399257
    const-string v0, "stickerPack"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1399258
    const-string v0, "isDownloaded"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1399259
    const-string v0, "price"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1399260
    const-string v2, "stickerContext"

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1399261
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->e:LX/42n;

    invoke-interface {v0, p0, v1}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1399262
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1399263
    const v0, 0x7f080039

    .line 1399264
    instance-of v1, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v1, :cond_0

    .line 1399265
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 1399266
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1399267
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    .line 1399268
    const v0, 0x7f08033f

    .line 1399269
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(I)V

    .line 1399270
    return-void
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1399271
    const-string v0, "StickerStoreFragment loadListViewContent"

    const v1, -0x334d1f2c    # -9.3783712E7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1399272
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->m()V

    .line 1399273
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Ljava/util/List;Z)V

    .line 1399274
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2V4;->g:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->G:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2V4;->h:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1399275
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    if-eqz v0, :cond_0

    .line 1399276
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    new-instance v1, LX/8mW;

    invoke-direct {v1, p0, p1}, LX/8mW;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1399277
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    sget-object v1, LX/8mX;->OWNED:LX/8mX;

    if-ne v0, v1, :cond_1

    .line 1399278
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->p:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1399279
    :cond_0
    :goto_0
    const v0, -0x39552e29

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1399280
    return-void

    .line 1399281
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1399282
    :catchall_0
    move-exception v0

    const v1, 0x4d4832b7    # 2.09922928E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V
    .locals 5

    .prologue
    .line 1399283
    sget-object v0, LX/8mN;->a:[I

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    invoke-virtual {v1}, LX/8mX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1399284
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    const-string v1, "Unknown tab specified for reload: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1399285
    :goto_0
    return-void

    .line 1399286
    :pswitch_0
    invoke-static {p0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->b$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V

    goto :goto_0

    .line 1399287
    :pswitch_1
    invoke-static {p0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->c$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V

    goto :goto_0

    .line 1399288
    :pswitch_2
    invoke-static {p0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->d(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(LX/8mX;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1399289
    iput-object p1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    .line 1399290
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->s:Landroid/widget/TextView;

    sget-object v0, LX/8mX;->FEATURED:LX/8mX;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1399291
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->t:Landroid/widget/TextView;

    sget-object v0, LX/8mX;->AVAILABLE:LX/8mX;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1399292
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->u:Landroid/widget/TextView;

    sget-object v3, LX/8mX;->OWNED:LX/8mX;

    if-ne p1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1399293
    return-void

    :cond_0
    move v0, v2

    .line 1399294
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1399295
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1399296
    goto :goto_2
.end method

.method private b(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 3

    .prologue
    .line 1399297
    const-string v0, "sticker_store"

    invoke-static {v0}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1399298
    const-string v1, "action"

    const-string v2, "sticker_pack_obtained"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399299
    const-string v1, "sticker_pack"

    .line 1399300
    iget-object v2, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1399301
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399302
    const-string v1, "store_tab"

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    invoke-virtual {v2}, LX/8mX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399303
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->j:LX/8j7;

    invoke-virtual {v1, v0}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1399304
    return-void
.end method

.method public static b$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1399305
    iput-boolean v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->r:Z

    .line 1399306
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->q:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1399307
    invoke-direct {p0, p1, v2}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Ljava/util/List;Z)V

    .line 1399308
    return-void
.end method

.method public static b$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V
    .locals 2

    .prologue
    .line 1399159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->r:Z

    .line 1399160
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    sget-object v1, LX/8mX;->FEATURED:LX/8mX;

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_0

    .line 1399161
    :goto_0
    return-void

    .line 1399162
    :cond_0
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->k()V

    .line 1399163
    sget-object v0, LX/3do;->STORE_PACKS:LX/3do;

    sget-object v1, LX/8mX;->FEATURED:LX/8mX;

    invoke-direct {p0, v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(LX/3do;LX/8mX;)V

    .line 1399164
    sget-object v0, LX/8mX;->FEATURED:LX/8mX;

    invoke-direct {p0, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->b(LX/8mX;)V

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1399165
    iput-boolean v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->r:Z

    .line 1399166
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->p:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1399167
    invoke-direct {p0, p1, v2}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Ljava/util/List;Z)V

    .line 1399168
    return-void
.end method

.method public static c$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V
    .locals 2

    .prologue
    .line 1398942
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->r:Z

    .line 1398943
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    sget-object v1, LX/8mX;->AVAILABLE:LX/8mX;

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_0

    .line 1398944
    :goto_0
    return-void

    .line 1398945
    :cond_0
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->k()V

    .line 1398946
    sget-object v0, LX/3do;->STORE_PACKS:LX/3do;

    sget-object v1, LX/8mX;->AVAILABLE:LX/8mX;

    invoke-direct {p0, v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(LX/3do;LX/8mX;)V

    .line 1398947
    sget-object v0, LX/8mX;->AVAILABLE:LX/8mX;

    invoke-direct {p0, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->b(LX/8mX;)V

    goto :goto_0
.end method

.method private d(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1398948
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v2

    .line 1398949
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v3

    .line 1398950
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 1398951
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 1398952
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1398953
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1398954
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1398955
    invoke-virtual {v1, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1398956
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v5, v1

    .line 1398957
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4m4;

    invoke-virtual {v5, v1}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1398958
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1398959
    :cond_1
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1398960
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1398961
    invoke-virtual {v1, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1398962
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v5, v1

    .line 1398963
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4m4;

    invoke-virtual {v5, v1}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1398964
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1398965
    :cond_2
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 1398966
    return-object v2
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1398967
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->s:Landroid/widget/TextView;

    new-instance v1, LX/8mR;

    invoke-direct {v1, p0}, LX/8mR;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1398968
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->t:Landroid/widget/TextView;

    new-instance v1, LX/8mS;

    invoke-direct {v1, p0}, LX/8mS;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1398969
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->u:Landroid/widget/TextView;

    new-instance v1, LX/8mT;

    invoke-direct {v1, p0}, LX/8mT;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1398970
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1398971
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010532

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1398972
    iget v1, v0, Landroid/util/TypedValue;->type:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1398973
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398974
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398975
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->u:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398976
    :cond_0
    return-void
.end method

.method public static d(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V
    .locals 2

    .prologue
    .line 1398977
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->r:Z

    if-eqz v0, :cond_0

    .line 1398978
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->q:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1398979
    :goto_0
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    sget-object v1, LX/8mX;->OWNED:LX/8mX;

    if-ne v0, v1, :cond_1

    if-nez p1, :cond_1

    .line 1398980
    :goto_1
    return-void

    .line 1398981
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->p:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto :goto_0

    .line 1398982
    :cond_1
    sget-object v0, LX/3do;->OWNED_PACKS:LX/3do;

    sget-object v1, LX/8mX;->OWNED:LX/8mX;

    invoke-direct {p0, v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(LX/3do;LX/8mX;)V

    .line 1398983
    sget-object v0, LX/8mX;->OWNED:LX/8mX;

    invoke-direct {p0, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->b(LX/8mX;)V

    goto :goto_1
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1398984
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1398985
    if-eqz v0, :cond_0

    .line 1398986
    const-string v1, "StickerFetchingStickerPacks"

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->l:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x777c930b

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1398987
    :cond_0
    sget-object v0, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {p0, v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(LX/3do;LX/0rS;)LX/1ML;

    move-result-object v0

    .line 1398988
    new-instance v1, LX/8mU;

    invoke-direct {v1, p0}, LX/8mU;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1398989
    return-void
.end method

.method public static e(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1398990
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 1398991
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 1398992
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1398993
    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1398994
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1398995
    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1398996
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1398997
    invoke-virtual {v1, v4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1398998
    :cond_1
    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1398999
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1399000
    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1399001
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1399002
    invoke-virtual {v2, v4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1399003
    :cond_2
    iput-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1399004
    iput-object v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1399005
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1399006
    const-string v1, "stickerPacks"

    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1399007
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->f:LX/0aG;

    const-string v2, "set_downloaded_sticker_packs"

    const v3, -0x64f0ebc4

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1399008
    new-instance v1, LX/8mL;

    invoke-direct {v1, p0}, LX/8mL;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1399009
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1399010
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    if-eqz v0, :cond_0

    .line 1399011
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    .line 1399012
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1399013
    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1399014
    :cond_0
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1399015
    const v0, 0x7f080336

    invoke-direct {p0, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(I)V

    .line 1399016
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1399017
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(Ljava/lang/CharSequence;)V

    .line 1399018
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1399019
    return-void
.end method

.method private o()Ljava/util/LinkedHashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1399020
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1399021
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    .line 1399022
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    .line 1399023
    return-object v0
.end method

.method public static p(Lcom/facebook/stickers/store/StickerStoreFragment;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1399024
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->q()LX/0Px;

    move-result-object v0

    .line 1399025
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->r()LX/0Px;

    move-result-object v1

    .line 1399026
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1399027
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1399028
    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1399029
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private q()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1399030
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1399031
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 1399032
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 1399033
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1399034
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1399035
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1399036
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private r()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1399037
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1399038
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 1399039
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 1399040
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1399041
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1399042
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1399043
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/42n;)V
    .locals 0

    .prologue
    .line 1399157
    iput-object p1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->e:LX/42n;

    .line 1399158
    return-void
.end method

.method public final a(LX/8mX;)V
    .locals 3

    .prologue
    .line 1399044
    const-string v0, "sticker_store"

    invoke-static {v0}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1399045
    const-string v1, "action"

    const-string v2, "sticker_store_tab_load_error"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399046
    const-string v1, "store_tab"

    invoke-virtual {p1}, LX/8mX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399047
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->j:LX/8j7;

    invoke-virtual {v1, v0}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1399048
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1399049
    const-string v0, "sticker_store"

    invoke-static {v0}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1399050
    const-string v1, "action"

    const-string v2, "sticker_packs_reordered"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399051
    const-string v1, "store_tab"

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    invoke-virtual {v2}, LX/8mX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399052
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->j:LX/8j7;

    invoke-virtual {v1, v0}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1399053
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x2

    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, 0x3f978b1e

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1399054
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1399055
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/stickers/store/StickerStoreActivity;

    if-eqz v0, :cond_0

    .line 1399056
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/store/StickerStoreActivity;

    .line 1399057
    iget-object v1, v0, Lcom/facebook/stickers/store/StickerStoreActivity;->A:LX/0h5;

    move-object v0, v1

    .line 1399058
    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    .line 1399059
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->o:LX/0h5;

    new-instance v1, LX/8mO;

    invoke-direct {v1, p0}, LX/8mO;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1399060
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/store/StickerStoreActivity;

    .line 1399061
    iget-object v1, v0, Lcom/facebook/stickers/store/StickerStoreActivity;->y:LX/4m4;

    move-object v0, v1

    .line 1399062
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    .line 1399063
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 1399064
    iput v5, v0, LX/108;->a:I

    .line 1399065
    move-object v0, v0

    .line 1399066
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08033d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1399067
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1399068
    move-object v0, v0

    .line 1399069
    const-string v1, "sticker_store_edit"

    .line 1399070
    iput-object v1, v0, LX/108;->f:Ljava/lang/String;

    .line 1399071
    move-object v0, v0

    .line 1399072
    iput v4, v0, LX/108;->h:I

    .line 1399073
    move-object v0, v0

    .line 1399074
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->p:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1399075
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 1399076
    iput v8, v0, LX/108;->a:I

    .line 1399077
    move-object v0, v0

    .line 1399078
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08033e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1399079
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1399080
    move-object v0, v0

    .line 1399081
    const-string v1, "sticker_store_done"

    .line 1399082
    iput-object v1, v0, LX/108;->f:Ljava/lang/String;

    .line 1399083
    move-object v0, v0

    .line 1399084
    iput v4, v0, LX/108;->h:I

    .line 1399085
    move-object v0, v0

    .line 1399086
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->q:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1399087
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->r:Z

    .line 1399088
    const v0, 0x7f0d216c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->s:Landroid/widget/TextView;

    .line 1399089
    const v0, 0x7f0d216d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->t:Landroid/widget/TextView;

    .line 1399090
    const v0, 0x7f0d216e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->u:Landroid/widget/TextView;

    .line 1399091
    const v0, 0x7f0d216f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->v:Landroid/view/ViewGroup;

    .line 1399092
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1399093
    const v1, 0x7f030d9e

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1399094
    const v1, 0x7f0d2179

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/store/StickerStoreListView;

    iput-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->w:Lcom/facebook/stickers/store/StickerStoreListView;

    .line 1399095
    const v1, 0x7f0d217a

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1399096
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x106000d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundColor(I)V

    .line 1399097
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->w:Lcom/facebook/stickers/store/StickerStoreListView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->x:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/store/StickerStoreListView;->setEmptyView(Landroid/view/View;)V

    .line 1399098
    new-instance v1, LX/8ma;

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4m4;

    invoke-direct {v1, v3, v0}, LX/8ma;-><init>(Landroid/content/Context;LX/4m4;)V

    iput-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    .line 1399099
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    new-instance v1, LX/8mP;

    invoke-direct {v1, p0}, LX/8mP;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    .line 1399100
    iput-object v1, v0, LX/8ma;->a:LX/8mP;

    .line 1399101
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->w:Lcom/facebook/stickers/store/StickerStoreListView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/store/StickerStoreListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1399102
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->w:Lcom/facebook/stickers/store/StickerStoreListView;

    new-instance v1, LX/8mQ;

    invoke-direct {v1, p0}, LX/8mQ;-><init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V

    .line 1399103
    iput-object v1, v0, LX/620;->m:LX/61y;

    .line 1399104
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->d()V

    .line 1399105
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_FAILURE"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->g:LX/0Yb;

    .line 1399106
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1399107
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1399108
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1399109
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->e()V

    .line 1399110
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1399111
    if-eqz v0, :cond_1

    .line 1399112
    const-string v1, "StickerCreateStickerStoreActivity"

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->l:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x2f9b6c92

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1399113
    :cond_1
    const/16 v0, 0x2b

    const v1, -0x5813804a

    invoke-static {v8, v0, v1, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6a5b1431

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1399114
    const-class v1, Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-static {v1, p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1399115
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010526

    const v3, 0x7f0e055d

    invoke-static {v1, v2, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->b:Landroid/content/Context;

    .line 1399116
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1399117
    const v2, 0x7f030d9a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1399118
    const-string v3, "sticker_store"

    invoke-static {v1, v3, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 1399119
    const/16 v2, 0x2b

    const v3, 0x4b48f957    # 1.3171031E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3fa58a82

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1399120
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->g:LX/0Yb;

    if-eqz v1, :cond_0

    .line 1399121
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->g:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1399122
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->g:LX/0Yb;

    .line 1399123
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1399124
    const/16 v1, 0x2b

    const v2, 0x6061ecc1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 1

    .prologue
    .line 1399125
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onHiddenChanged(Z)V

    .line 1399126
    if-nez p1, :cond_0

    .line 1399127
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V

    .line 1399128
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStoreFragment;->d()V

    .line 1399129
    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x1981933d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1399130
    const-string v0, "stickerPack"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1399131
    if-nez v0, :cond_0

    .line 1399132
    const/16 v0, 0x27

    const v1, 0x640a5701

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1399133
    :goto_0
    return-void

    .line 1399134
    :cond_0
    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1399135
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1399136
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v3, v1

    .line 1399137
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4m4;

    invoke-virtual {v3, v1}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1399138
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1399139
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1399140
    invoke-virtual {v1, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399141
    :goto_1
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    const/4 v3, 0x1

    .line 1399142
    if-eqz v3, :cond_4

    .line 1399143
    iget-object p0, v1, LX/8ma;->b:Ljava/util/LinkedHashMap;

    .line 1399144
    iget-object p1, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1399145
    invoke-virtual {p0, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399146
    :cond_1
    :goto_2
    const p0, 0x6b6caaeb

    invoke-static {v1, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1399147
    :cond_2
    const v0, 0x13d11438

    invoke-static {v0, v2}, LX/02F;->e(II)V

    goto :goto_0

    .line 1399148
    :cond_3
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1399149
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1399150
    invoke-virtual {v1, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1399151
    :cond_4
    iget-object p0, v1, LX/8ma;->b:Ljava/util/LinkedHashMap;

    .line 1399152
    iget-object p1, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1399153
    invoke-virtual {p0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1399154
    iget-object p0, v1, LX/8ma;->b:Ljava/util/LinkedHashMap;

    .line 1399155
    iget-object p1, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1399156
    invoke-virtual {p0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method
