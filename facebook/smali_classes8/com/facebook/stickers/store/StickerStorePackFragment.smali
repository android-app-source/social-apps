.class public Lcom/facebook/stickers/store/StickerStorePackFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0YZ;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/stickers/store/StickerStorePackFragment;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0So;

.field public B:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/4m4;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/1Ad;

.field private c:Landroid/content/Context;

.field private d:Landroid/view/LayoutInflater;

.field public e:LX/0Xl;

.field public f:Lcom/facebook/stickers/client/StickerDownloadManager;

.field public g:LX/8j7;

.field private h:LX/0Yb;

.field public i:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public j:LX/1CX;

.field public k:Lcom/facebook/stickers/model/StickerPack;

.field public l:Z

.field public m:Z

.field public n:Ljava/lang/String;

.field private o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private p:Landroid/widget/ScrollView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/ProgressBar;

.field private v:Landroid/widget/Button;

.field public w:Landroid/widget/ProgressBar;

.field public x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private y:Landroid/widget/LinearLayout;

.field public z:LX/11i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1399674
    const-class v0, Lcom/facebook/stickers/store/StickerStorePackFragment;

    .line 1399675
    sput-object v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->a:Ljava/lang/Class;

    const-string v1, "sticker_store_pack"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/store/StickerStorePackFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1399676
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 3

    .prologue
    .line 1399682
    const-string v0, "sticker_store_pack"

    invoke-static {v0}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1399683
    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399684
    const-string v1, "sticker_pack"

    .line 1399685
    iget-object v2, p2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1399686
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399687
    const-string v1, "is_featured"

    .line 1399688
    iget-boolean v2, p2, Lcom/facebook/stickers/model/StickerPack;->m:Z

    move v2, v2

    .line 1399689
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1399690
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->g:LX/8j7;

    invoke-virtual {v1, v0}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1399691
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1399677
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/67U;

    if-eqz v0, :cond_0

    .line 1399678
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/67U;

    invoke-interface {v0}, LX/67U;->b()LX/3u1;

    move-result-object v0

    .line 1399679
    if-eqz v0, :cond_0

    .line 1399680
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3u1;->c(I)V

    .line 1399681
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/stickers/store/StickerStorePackFragment;)V
    .locals 8

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1399614
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_1

    .line 1399615
    :cond_0
    :goto_0
    return-void

    .line 1399616
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->p:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 1399617
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399618
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v1, v2

    .line 1399619
    sget-object v2, Lcom/facebook/stickers/store/StickerStorePackFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1399620
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399621
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1399622
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399623
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399624
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1399625
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399626
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->n:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1399627
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->s:Landroid/widget/TextView;

    const v1, 0x7f080337

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1399628
    :goto_1
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399629
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1399630
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399631
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->f:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1399632
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    const v1, 0x7f08033a

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1399633
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1399634
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1399635
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->f:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/client/StickerDownloadManager;->d(Lcom/facebook/stickers/model/StickerPack;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1399636
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399637
    :goto_2
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->B:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399638
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 1399639
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->B:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4m4;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1399640
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1399641
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0809d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399642
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->w:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399643
    new-instance v0, LX/8mc;

    invoke-direct {v0, p0}, LX/8mc;-><init>(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    .line 1399644
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->C:LX/1Ad;

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399645
    iget-object v4, v2, Lcom/facebook/stickers/model/StickerPack;->g:Landroid/net/Uri;

    move-object v2, v4

    .line 1399646
    invoke-virtual {v1, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/stickers/store/StickerStorePackFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1399647
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1399648
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1399649
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    .line 1399650
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    move-object v4, v1

    .line 1399651
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_7

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1399652
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1399653
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->d:Landroid/view/LayoutInflater;

    const v6, 0x7f030d9c

    iget-object v7, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1399654
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399655
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1399656
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1399657
    :cond_4
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1399658
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->l:Z

    if-eqz v0, :cond_6

    .line 1399659
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    const v1, 0x7f08033b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1399660
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1399661
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_2

    .line 1399662
    :cond_6
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    const v1, 0x7f080339

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1399663
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1399664
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_2

    .line 1399665
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->m:Z

    if-eqz v0, :cond_0

    .line 1399666
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->d(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    .line 1399667
    iput-boolean v3, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->m:Z

    goto/16 :goto_0
.end method

.method public static d(Lcom/facebook/stickers/store/StickerStorePackFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1399668
    const-string v0, "sticker_pack_download_tapped"

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0, v0, v1}, Lcom/facebook/stickers/store/StickerStorePackFragment;->a(Ljava/lang/String;Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399669
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1399670
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1399671
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399672
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->f:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399673
    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, 0x4566932e

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1399560
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1399561
    const v0, 0x7f0d1b29

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1399562
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->q:Landroid/widget/TextView;

    .line 1399563
    const v0, 0x7f0d215b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->r:Landroid/widget/TextView;

    .line 1399564
    const v0, 0x7f0d0a0e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->s:Landroid/widget/TextView;

    .line 1399565
    const v0, 0x7f0d0550

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->t:Landroid/widget/TextView;

    .line 1399566
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    .line 1399567
    const v0, 0x7f0d1e70

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    .line 1399568
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->w:Landroid/widget/ProgressBar;

    .line 1399569
    const v0, 0x7f0d2177

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1399570
    const v0, 0x7f0d2178

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->y:Landroid/widget/LinearLayout;

    .line 1399571
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1399572
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->c:Landroid/content/Context;

    const v3, 0x7f01052b

    const v4, 0x7f0211d4

    invoke-static {v1, v3, v4}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 1399573
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    new-instance v1, LX/8mb;

    invoke-direct {v1, p0}, LX/8mb;-><init>(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1399574
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->b()V

    .line 1399575
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_FAILURE"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->h:LX/0Yb;

    .line 1399576
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->c(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    .line 1399577
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->z:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1399578
    if-eqz v0, :cond_0

    .line 1399579
    const-string v1, "StickerCreateStickerStoreActivity"

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->A:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x3a9d6a11

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1399580
    :cond_0
    const/16 v0, 0x2b

    const v1, -0x5854255c

    invoke-static {v8, v0, v1, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x29884853

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1399554
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f010526

    const v3, 0x7f0e055d

    invoke-static {v0, v2, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->c:Landroid/content/Context;

    .line 1399555
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->c:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->d:Landroid/view/LayoutInflater;

    .line 1399556
    const v0, 0x7f030d9d

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->p:Landroid/widget/ScrollView;

    .line 1399557
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v8

    check-cast v8, LX/11i;

    invoke-static {v0}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(LX/0QB;)Lcom/facebook/stickers/client/StickerDownloadManager;

    move-result-object p1

    check-cast p1, Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-static {v0}, LX/8j7;->a(LX/0QB;)LX/8j7;

    move-result-object p2

    check-cast p2, LX/8j7;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p3

    check-cast p3, LX/1Ad;

    invoke-static {v0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v0

    check-cast v0, LX/1CX;

    iput-object v5, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->i:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object v6, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->e:LX/0Xl;

    iput-object v7, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->A:LX/0So;

    iput-object v8, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->z:LX/11i;

    iput-object p1, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->f:Lcom/facebook/stickers/client/StickerDownloadManager;

    iput-object p2, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->g:LX/8j7;

    iput-object p3, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->C:LX/1Ad;

    iput-object v0, v3, Lcom/facebook/stickers/store/StickerStorePackFragment;->j:LX/1CX;

    .line 1399558
    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->p:Landroid/widget/ScrollView;

    const-string v3, "sticker_store"

    invoke-static {v2, v3, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 1399559
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->p:Landroid/widget/ScrollView;

    const/16 v2, 0x2b

    const v3, 0x68396f9d

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x20cdafc2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1399581
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1399582
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->h:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1399583
    const/16 v1, 0x2b

    const v2, 0x1d7c8ee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 0

    .prologue
    .line 1399584
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onHiddenChanged(Z)V

    .line 1399585
    if-nez p1, :cond_0

    .line 1399586
    invoke-direct {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->b()V

    .line 1399587
    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x26

    const v1, -0x3c2b8d19

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1399592
    const-string v0, "stickerPack"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1399593
    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->k:Lcom/facebook/stickers/model/StickerPack;

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1399594
    const/16 v0, 0x27

    const v2, 0x52b0d6c3

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1399595
    :goto_0
    return-void

    .line 1399596
    :cond_0
    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1399597
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    const v2, 0x7f08033a

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 1399598
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->v:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1399599
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1399600
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    const-string v2, "progress"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1399601
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399602
    :cond_1
    :goto_1
    const v0, -0x227c5681

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 1399603
    :cond_2
    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1399604
    const-string v2, "sticker_pack_downloaded"

    invoke-direct {p0, v2, v0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->a(Ljava/lang/String;Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->l:Z

    .line 1399606
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->c(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    goto :goto_1

    .line 1399607
    :cond_3
    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_FAILURE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1399608
    const-string v2, "sticker_pack_download_error"

    invoke-direct {p0, v2, v0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->a(Ljava/lang/String;Lcom/facebook/stickers/model/StickerPack;)V

    .line 1399609
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->c(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    .line 1399610
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->j:LX/1CX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    .line 1399611
    iput-object v3, v2, LX/4mn;->b:Ljava/lang/String;

    .line 1399612
    move-object v2, v2

    .line 1399613
    const v3, 0x7f080039

    invoke-virtual {v2, v3}, LX/4mn;->b(I)LX/4mn;

    move-result-object v2

    invoke-virtual {v2}, LX/4mn;->l()LX/4mm;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    goto :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7e0fea19

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1399588
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1399589
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStorePackFragment;->h:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1399590
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStorePackFragment;->c(Lcom/facebook/stickers/store/StickerStorePackFragment;)V

    .line 1399591
    const/16 v1, 0x2b

    const v2, 0x1c0d3a63

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
