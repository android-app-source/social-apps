.class public Lcom/facebook/stickers/store/StickerStoreListView;
.super LX/620;
.source ""


# instance fields
.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1399470
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/stickers/store/StickerStoreListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1399471
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 1399472
    invoke-direct {p0, p1, p2, p3}, LX/620;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1399473
    iput v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->d:I

    .line 1399474
    iput v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->e:I

    .line 1399475
    iput v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->f:I

    .line 1399476
    iput v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->g:I

    .line 1399477
    sget-object v0, LX/03r;->StickerStoreListView:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1399478
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/stickers/store/StickerStoreListView;->d:I

    .line 1399479
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/stickers/store/StickerStoreListView;->e:I

    .line 1399480
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/stickers/store/StickerStoreListView;->f:I

    .line 1399481
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/stickers/store/StickerStoreListView;->g:I

    .line 1399482
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1399483
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1399484
    invoke-virtual {p0, p1}, Lcom/facebook/stickers/store/StickerStoreListView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399485
    iget v0, p0, Lcom/facebook/stickers/store/StickerStoreListView;->d:I

    invoke-static {p1, v0, v1}, LX/620;->a(Landroid/view/View;II)V

    .line 1399486
    iget v0, p0, Lcom/facebook/stickers/store/StickerStoreListView;->e:I

    invoke-static {p1, v0, v2}, LX/620;->a(Landroid/view/View;II)V

    .line 1399487
    iget v0, p0, Lcom/facebook/stickers/store/StickerStoreListView;->f:I

    invoke-static {p1, v0, v1}, LX/620;->a(Landroid/view/View;II)V

    .line 1399488
    iget v0, p0, Lcom/facebook/stickers/store/StickerStoreListView;->g:I

    invoke-static {p1, v0, v1}, LX/620;->a(Landroid/view/View;II)V

    move-object v0, p1

    .line 1399489
    check-cast v0, Lcom/facebook/stickers/store/StickerStoreListItemView;

    .line 1399490
    const/4 v1, 0x1

    .line 1399491
    iput-boolean v1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->w:Z

    .line 1399492
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1399493
    iget v1, p0, LX/620;->a:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1399494
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1399495
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1399496
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;III)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1399497
    invoke-virtual {p0, p1}, Lcom/facebook/stickers/store/StickerStoreListView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1399498
    check-cast v0, Lcom/facebook/stickers/store/StickerStoreListItemView;

    .line 1399499
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1399500
    const/4 v2, -0x2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1399501
    invoke-virtual {p1, p4}, Landroid/view/View;->setVisibility(I)V

    .line 1399502
    iget v2, p0, LX/620;->b:I

    if-ne p2, v2, :cond_4

    .line 1399503
    const/16 v2, 0x30

    if-ne p3, v2, :cond_3

    .line 1399504
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->f:I

    invoke-static {p1, v2, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 1399505
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->g:I

    invoke-static {p1, v2, v3}, LX/620;->a(Landroid/view/View;II)V

    .line 1399506
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->d:I

    invoke-static {p1, v2, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 1399507
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->e:I

    invoke-static {p1, v2, v3}, LX/620;->a(Landroid/view/View;II)V

    .line 1399508
    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    .line 1399509
    iput-boolean v3, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->w:Z

    .line 1399510
    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1399511
    :cond_2
    return-void

    .line 1399512
    :cond_3
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->f:I

    invoke-static {p1, v2, v3}, LX/620;->a(Landroid/view/View;II)V

    .line 1399513
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->g:I

    invoke-static {p1, v2, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 1399514
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->d:I

    invoke-static {p1, v2, v3}, LX/620;->a(Landroid/view/View;II)V

    .line 1399515
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->e:I

    invoke-static {p1, v2, v3}, LX/620;->a(Landroid/view/View;II)V

    goto :goto_0

    .line 1399516
    :cond_4
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->f:I

    invoke-static {p1, v2, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 1399517
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->g:I

    invoke-static {p1, v2, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 1399518
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->d:I

    invoke-static {p1, v2, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 1399519
    iget v2, p0, Lcom/facebook/stickers/store/StickerStoreListView;->e:I

    invoke-static {p1, v2, v3}, LX/620;->a(Landroid/view/View;II)V

    .line 1399520
    iget v2, p0, LX/620;->a:I

    if-eq p2, v2, :cond_0

    .line 1399521
    iput p2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1399522
    instance-of v0, p1, Lcom/facebook/stickers/store/StickerStoreListItemView;

    if-eqz v0, :cond_1

    .line 1399523
    check-cast p1, Lcom/facebook/stickers/store/StickerStoreListItemView;

    .line 1399524
    const/4 v0, 0x0

    .line 1399525
    iget-object p0, p1, Lcom/facebook/stickers/store/StickerStoreListItemView;->i:Landroid/widget/ImageView;

    if-eqz p0, :cond_0

    .line 1399526
    iget-object p0, p1, Lcom/facebook/stickers/store/StickerStoreListItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getVisibility()I

    move-result p0

    if-nez p0, :cond_0

    const/4 v0, 0x1

    .line 1399527
    :cond_0
    move v0, v0

    .line 1399528
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
