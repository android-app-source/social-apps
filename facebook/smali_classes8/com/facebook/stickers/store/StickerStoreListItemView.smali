.class public Lcom/facebook/stickers/store/StickerStoreListItemView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0YZ;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/ProgressBar;

.field public final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/ImageView;

.field public final h:Landroid/view/View;

.field public final i:Landroid/widget/ImageView;

.field private final j:Landroid/content/Context;

.field public k:LX/0Xl;

.field private l:LX/0Yb;

.field public m:LX/3eW;

.field public n:Lcom/facebook/stickers/client/StickerDownloadManager;

.field public final o:I

.field private final p:I

.field private final q:I

.field public r:Ljava/lang/String;

.field public s:Lcom/facebook/stickers/model/StickerPack;

.field public t:Z

.field public u:Ljava/lang/String;

.field public v:Z

.field public w:Z

.field public x:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/4m4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1399469
    const-class v0, Lcom/facebook/stickers/store/StickerStoreListItemView;

    const-string v1, "sticker_store"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1399450
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1399451
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->x:LX/0am;

    .line 1399452
    const v0, 0x7f030d9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1399453
    const-class v0, Lcom/facebook/stickers/store/StickerStoreListItemView;

    invoke-static {v0, p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1399454
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->w:Z

    .line 1399455
    const v0, 0x7f0d1b29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1399456
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->c:Landroid/widget/ProgressBar;

    .line 1399457
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->d:Landroid/widget/TextView;

    .line 1399458
    const v0, 0x7f0d215b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->e:Landroid/widget/TextView;

    .line 1399459
    const v0, 0x7f0d0a0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->f:Landroid/widget/TextView;

    .line 1399460
    const v0, 0x7f0d19ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    .line 1399461
    const v0, 0x7f0d2173

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->h:Landroid/view/View;

    .line 1399462
    const v0, 0x7f0d1516

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->i:Landroid/widget/ImageView;

    .line 1399463
    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010526

    const v2, 0x7f0e055d

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->j:Landroid/content/Context;

    .line 1399464
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->k:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_FAILURE"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->l:LX/0Yb;

    .line 1399465
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->j:Landroid/content/Context;

    const v1, 0x7f010527

    const v2, 0x7f02131a

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->o:I

    .line 1399466
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->j:Landroid/content/Context;

    const v1, 0x7f010528

    const v2, 0x7f02131d

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->p:I

    .line 1399467
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->j:Landroid/content/Context;

    const v1, 0x7f010529

    const v2, 0x7f021321

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->q:I

    .line 1399468
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/stickers/store/StickerStoreListItemView;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {p0}, LX/3eW;->a(LX/0QB;)LX/3eW;

    move-result-object v2

    check-cast v2, LX/3eW;

    invoke-static {p0}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(LX/0QB;)Lcom/facebook/stickers/client/StickerDownloadManager;

    move-result-object p0

    check-cast p0, Lcom/facebook/stickers/client/StickerDownloadManager;

    iput-object v1, p1, Lcom/facebook/stickers/store/StickerStoreListItemView;->k:LX/0Xl;

    iput-object v2, p1, Lcom/facebook/stickers/store/StickerStoreListItemView;->m:LX/3eW;

    iput-object p0, p1, Lcom/facebook/stickers/store/StickerStoreListItemView;->n:Lcom/facebook/stickers/client/StickerDownloadManager;

    return-void
.end method

.method public static g(Lcom/facebook/stickers/store/StickerStoreListItemView;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1399359
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->n:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v3}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v3

    .line 1399360
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->n:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v4}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399361
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    iget v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->p:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1399362
    const-string v0, "%s %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08033a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399363
    iget-object v6, v5, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1399364
    aput-object v5, v4, v1

    invoke-static {v0, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1399365
    :goto_0
    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1399366
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1399367
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1399368
    return-void

    .line 1399369
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->t:Z

    if-eqz v0, :cond_1

    .line 1399370
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    iget v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->q:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1399371
    const-string v0, "%s %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08033b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399372
    iget-object v6, v5, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1399373
    aput-object v5, v4, v1

    invoke-static {v0, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1399374
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    iget v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->p:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1399375
    const-string v0, "%s %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080339

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399376
    iget-object v6, v5, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1399377
    aput-object v5, v4, v1

    invoke-static {v0, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1399378
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1399395
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399396
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v1, v2

    .line 1399397
    sget-object v2, Lcom/facebook/stickers/store/StickerStoreListItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1399398
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399399
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1399400
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399401
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399402
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1399403
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399404
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->i:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->v:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1399405
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399406
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 1399407
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4m4;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1399408
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1399409
    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1399410
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 1399411
    const v4, 0x7f0b03f9

    invoke-virtual {v0, v4, v3, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 1399412
    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Landroid/util/TypedValue;->getFloat()F

    move-result v3

    invoke-virtual {v4, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1399413
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 1399414
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1399415
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->d:Landroid/widget/TextView;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1399416
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->u:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->u:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399417
    iget-boolean v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->v:Z

    if-eqz v0, :cond_3

    .line 1399418
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399419
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1399420
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    iget v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->o:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1399421
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->m:LX/3eW;

    .line 1399422
    iget-object v3, v0, LX/3eW;->a:LX/0Px;

    move-object v0, v3

    .line 1399423
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 1399424
    :goto_2
    const-string v3, "%s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080338

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399425
    iget-object v6, v5, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1399426
    aput-object v5, v4, v1

    invoke-static {v3, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1399427
    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1399428
    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    :goto_3
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1399429
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1399430
    :goto_4
    return-void

    .line 1399431
    :cond_0
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 1399432
    :cond_1
    const/4 v3, 0x0

    .line 1399433
    invoke-virtual {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1399434
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->f:Landroid/widget/TextView;

    const v2, 0x7f0809d3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1399435
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1399436
    const v2, 0x7f0b03f8

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 1399437
    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1399438
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 1399439
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1399440
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->d:Landroid/widget/TextView;

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1399441
    goto :goto_4

    .line 1399442
    :cond_2
    const v4, 0x7f080337

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_1

    .line 1399443
    :cond_3
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->n:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v3, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v3}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v3

    .line 1399444
    iget-object v4, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->c:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399445
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->c:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->n:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/client/StickerDownloadManager;->d(Lcom/facebook/stickers/model/StickerPack;)I

    move-result v1

    :cond_4
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1399446
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->g(Lcom/facebook/stickers/store/StickerStoreListItemView;)V

    goto :goto_4

    :cond_5
    move v0, v2

    .line 1399447
    goto :goto_5

    :cond_6
    move v0, v2

    .line 1399448
    goto/16 :goto_2

    .line 1399449
    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_3
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1eee81ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1399392
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onAttachedToWindow()V

    .line 1399393
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1399394
    const/16 v1, 0x2d

    const v2, -0x41b71451

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x57660791

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1399389
    iget-object v1, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1399390
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 1399391
    const/16 v1, 0x2d

    const v2, -0x63b123eb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x354215d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1399379
    const-string v0, "stickerPack"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1399380
    iget-boolean v2, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->v:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->r:Ljava/lang/String;

    .line 1399381
    iget-object p1, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, p1

    .line 1399382
    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1399383
    :cond_0
    const/16 v0, 0x27

    const v2, 0x691eb194

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1399384
    :goto_0
    return-void

    .line 1399385
    :cond_1
    invoke-static {p0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->g(Lcom/facebook/stickers/store/StickerStoreListItemView;)V

    .line 1399386
    const-string v0, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1399387
    iget-object v0, p0, Lcom/facebook/stickers/store/StickerStoreListItemView;->c:Landroid/widget/ProgressBar;

    const-string v2, "progress"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1399388
    :cond_2
    const v0, 0x570b9d3d

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0
.end method
