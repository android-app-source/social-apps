.class public Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8CX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/8CX",
        "<",
        "LX/8kl;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:I


# instance fields
.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0Sg;

.field private final e:LX/8jL;

.field private final f:LX/8kd;

.field public final g:LX/8j6;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:LX/8l4;

.field public final j:Landroid/content/Context;

.field private final k:Landroid/view/LayoutInflater;

.field public final l:I

.field public final m:LX/2V6;

.field public n:LX/7kc;

.field public o:LX/8kq;

.field public p:LX/8l5;

.field public q:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

.field private r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

.field public t:LX/8lL;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/stickers/search/StickerSearchContainer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/4m4;

.field public x:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1397639
    const-class v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    const-string v1, "sticker_keyboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1397640
    invoke-static {}, LX/8lD;->values()[LX/8lD;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Sg;LX/8jL;LX/8kd;LX/8j6;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8l4;Landroid/content/Context;Landroid/view/LayoutInflater;LX/2V6;)V
    .locals 3
    .param p8    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1397623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1397624
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1397625
    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->r:LX/0Px;

    .line 1397626
    iput-object p1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->c:Landroid/content/res/Resources;

    .line 1397627
    iput-object p2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->d:LX/0Sg;

    .line 1397628
    iput-object p3, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->e:LX/8jL;

    .line 1397629
    iput-object p4, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->f:LX/8kd;

    .line 1397630
    iput-object p5, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->g:LX/8j6;

    .line 1397631
    iput-object p6, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1397632
    iput-object p7, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->i:LX/8l4;

    .line 1397633
    iput-object p8, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    .line 1397634
    iput-object p9, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->k:Landroid/view/LayoutInflater;

    .line 1397635
    iput-object p10, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->m:LX/2V6;

    .line 1397636
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    const v1, 0x7f010520

    const v2, 0x7f021316

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->l:I

    .line 1397637
    new-instance v0, LX/8l5;

    invoke-direct {v0, p0}, LX/8l5;-><init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;)V

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->p:LX/8l5;

    .line 1397638
    return-void
.end method

.method private a(LX/8km;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 1397600
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->k:Landroid/view/LayoutInflater;

    const v3, 0x7f030d95

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 1397601
    const v2, 0x7f0d1b29

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1397602
    const v3, 0x7f0d037b

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    .line 1397603
    const v4, 0x7f0d215b

    invoke-virtual {v9, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 1397604
    const v5, 0x7f0d0a0e

    invoke-virtual {v9, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbTextView;

    .line 1397605
    const v6, 0x7f0d0550

    invoke-virtual {v9, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/resources/ui/FbTextView;

    .line 1397606
    const v7, 0x7f0d215c

    invoke-virtual {v9, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 1397607
    const v8, 0x7f0d1e70

    invoke-virtual {v9, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 1397608
    move-object/from16 v0, p1

    iget-object v10, v0, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397609
    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->e()Landroid/net/Uri;

    move-result-object v11

    sget-object v12, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v11, v12}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1397610
    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397611
    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397612
    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->i()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 1397613
    :goto_0
    if-nez v2, :cond_1

    .line 1397614
    const v2, 0x7f080337

    invoke-virtual {v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1397615
    :goto_1
    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397616
    const v2, 0x7f08033c

    invoke-virtual {v7, v2}, Landroid/widget/Button;->setText(I)V

    .line 1397617
    new-instance v2, LX/8l9;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v10}, LX/8l9;-><init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Lcom/facebook/stickers/model/StickerPack;)V

    invoke-virtual {v7, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1397618
    const v2, 0x7f080339

    invoke-virtual {v8, v2}, Landroid/widget/Button;->setText(I)V

    .line 1397619
    new-instance v2, LX/8lA;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v10}, LX/8lA;-><init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Lcom/facebook/stickers/model/StickerPack;)V

    invoke-virtual {v8, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1397620
    return-object v9

    .line 1397621
    :cond_0
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "$0.00"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->i()I

    move-result v3

    int-to-double v12, v3

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    div-double/2addr v12, v14

    invoke-virtual {v2, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1397622
    :cond_1
    invoke-virtual {v10}, Lcom/facebook/stickers/model/StickerPack;->i()I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1397592
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->r:LX/0Px;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->w:LX/4m4;

    .line 1397593
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1397594
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/model/Sticker;

    .line 1397595
    iget-object v5, v2, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v5, v1}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1397596
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1397597
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 1397598
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    invoke-virtual {v1, v0, p1}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1397599
    return-void
.end method

.method private static b(LX/8kl;)I
    .locals 2

    .prologue
    .line 1397584
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    if-ne p0, v0, :cond_0

    .line 1397585
    sget-object v0, LX/8lD;->RECENTS:LX/8lD;

    invoke-virtual {v0}, LX/8lD;->ordinal()I

    move-result v0

    .line 1397586
    :goto_0
    return v0

    .line 1397587
    :cond_0
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    if-ne p0, v0, :cond_1

    .line 1397588
    sget-object v0, LX/8lD;->SEARCH:LX/8lD;

    invoke-virtual {v0}, LX/8lD;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1397589
    :cond_1
    instance-of v0, p0, LX/8km;

    if-eqz v0, :cond_2

    .line 1397590
    sget-object v0, LX/8lD;->STICKER_PACK:LX/8lD;

    invoke-virtual {v0}, LX/8lD;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1397591
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown item type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(LX/8km;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1397578
    new-instance v0, LX/8kb;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/8kb;-><init>(Landroid/content/Context;)V

    .line 1397579
    new-instance v1, LX/8l8;

    invoke-direct {v1, p0, p1}, LX/8l8;-><init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;LX/8km;)V

    .line 1397580
    iput-object v1, v0, LX/8kb;->d:LX/8l8;

    .line 1397581
    iget-object v1, p1, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397582
    iget-object p0, v0, LX/8kb;->a:Lcom/facebook/stickers/keyboard/StickerPackInfoView;

    invoke-virtual {p0, v1}, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1397583
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    .line 1397577
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    const v1, 0x7f010522

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->c:Landroid/content/res/Resources;

    const v3, 0x7f0b0760

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v0, v1, v2}, LX/0WH;->d(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1397561
    invoke-static {}, LX/8lD;->values()[LX/8lD;

    move-result-object v0

    aget-object v0, v0, p2

    .line 1397562
    sget-object v1, LX/8lB;->a:[I

    invoke-virtual {v0}, LX/8lD;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1397563
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown item type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397564
    :pswitch_0
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;-><init>(Landroid/content/Context;)V

    .line 1397565
    const v1, 0x7f021317

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setPlaceholderResourceId(I)V

    .line 1397566
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    const p2, 0x7f0804de

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setContentDescription(Ljava/lang/String;)V

    .line 1397567
    new-instance v1, LX/8lC;

    invoke-direct {v1, v0}, LX/8lC;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;)V

    move-object v0, v1

    .line 1397568
    :goto_0
    return-object v0

    .line 1397569
    :pswitch_1
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;-><init>(Landroid/content/Context;)V

    .line 1397570
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->c:Landroid/content/res/Resources;

    const p2, 0x7f080363

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setContentDescription(Ljava/lang/String;)V

    .line 1397571
    new-instance v1, LX/8lC;

    invoke-direct {v1, v0}, LX/8lC;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;)V

    move-object v0, v1

    .line 1397572
    goto :goto_0

    .line 1397573
    :pswitch_2
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;-><init>(Landroid/content/Context;)V

    .line 1397574
    const v1, 0x7f020608

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setPlaceholderResourceId(I)V

    .line 1397575
    new-instance v1, LX/8lC;

    invoke-direct {v1, v0}, LX/8lC;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;)V

    move-object v0, v1

    .line 1397576
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1397432
    check-cast p1, LX/8kl;

    .line 1397433
    const/4 v1, 0x0

    .line 1397434
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->x:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->x:Ljava/lang/String;

    .line 1397435
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    if-ne p1, v0, :cond_2

    .line 1397436
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->i:LX/8l4;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->w:LX/4m4;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->n:LX/7kc;

    invoke-virtual {v0, v1, v2}, LX/8l4;->a(LX/4m4;LX/7kc;)Lcom/facebook/stickers/keyboard/StickerPackPageView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    .line 1397437
    iget-object v0, p1, LX/8kl;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Ljava/lang/String;)V

    .line 1397438
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->p:LX/8l5;

    .line 1397439
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->p:LX/8l5;

    .line 1397440
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    move-object v0, v0

    .line 1397441
    :goto_1
    if-eqz p4, :cond_0

    instance-of v1, p1, LX/8km;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz v1, :cond_0

    .line 1397442
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    invoke-virtual {v1}, LX/8kq;->c()V

    .line 1397443
    :cond_0
    return-object v0

    .line 1397444
    :cond_1
    iget-object v0, p1, LX/8kl;->c:Ljava/lang/String;

    goto :goto_0

    .line 1397445
    :cond_2
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    if-ne p1, v0, :cond_4

    .line 1397446
    new-instance v0, LX/8lL;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/8lL;-><init>(Landroid/content/Context;)V

    .line 1397447
    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->t:LX/8lL;

    .line 1397448
    new-instance v1, Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->j:Landroid/content/Context;

    iget-object p2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->w:LX/4m4;

    invoke-direct {v1, v2, p2}, Lcom/facebook/stickers/search/StickerSearchContainer;-><init>(Landroid/content/Context;LX/4m4;)V

    iput-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->u:Lcom/facebook/stickers/search/StickerSearchContainer;

    .line 1397449
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->u:Lcom/facebook/stickers/search/StickerSearchContainer;

    new-instance v2, LX/8l7;

    invoke-direct {v2, p0, p1}, LX/8l7;-><init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;LX/8kl;)V

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->setStickerSearchListener(LX/8l6;)V

    .line 1397450
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->v:Landroid/os/Bundle;

    if-eqz v1, :cond_3

    .line 1397451
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->u:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->v:Landroid/os/Bundle;

    .line 1397452
    const-string p2, "query"

    invoke-virtual {v2, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v1, Lcom/facebook/stickers/search/StickerSearchContainer;->M:Ljava/lang/String;

    .line 1397453
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->v:Landroid/os/Bundle;

    .line 1397454
    :cond_3
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->u:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-virtual {v0, v1}, LX/8lL;->addView(Landroid/view/View;)V

    .line 1397455
    move-object v0, v0

    .line 1397456
    goto :goto_1

    .line 1397457
    :cond_4
    instance-of v0, p1, LX/8km;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1397458
    check-cast v0, LX/8km;

    .line 1397459
    sget-object v1, LX/8lB;->b:[I

    iget-object v2, v0, LX/8km;->b:LX/8kk;

    invoke-virtual {v2}, LX/8kk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1397460
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown item type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397461
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->i:LX/8l4;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->w:LX/4m4;

    iget-object p2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->n:LX/7kc;

    invoke-virtual {v1, v2, p2}, LX/8l4;->a(LX/4m4;LX/7kc;)Lcom/facebook/stickers/keyboard/StickerPackPageView;

    move-result-object v1

    .line 1397462
    iget-object v2, v0, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397463
    iput-object v2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    .line 1397464
    sget-object p2, LX/0Q7;->a:LX/0Px;

    move-object p2, p2

    .line 1397465
    iget-object p3, v2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p3, p3

    .line 1397466
    invoke-virtual {v1, p2, p3}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1397467
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-virtual {p2, v2}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result p2

    if-nez p2, :cond_6

    .line 1397468
    invoke-static {v1}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397469
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    new-instance p3, LX/8jW;

    .line 1397470
    iget-object v0, v2, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v0, v0

    .line 1397471
    invoke-direct {p3, v0}, LX/8jW;-><init>(Ljava/util/List;)V

    invoke-virtual {p2, p3}, LX/8jY;->a(LX/8jW;)V

    .line 1397472
    :goto_2
    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->p:LX/8l5;

    .line 1397473
    iput-object v2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->p:LX/8l5;

    .line 1397474
    move-object v0, v1

    .line 1397475
    goto/16 :goto_1

    .line 1397476
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->b(LX/8km;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_1

    .line 1397477
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->b(LX/8km;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_1

    .line 1397478
    :pswitch_3
    invoke-direct {p0, v0, p3}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(LX/8km;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    goto/16 :goto_1

    .line 1397479
    :cond_6
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->n:Landroid/view/View;

    if-eqz p2, :cond_7

    .line 1397480
    :goto_3
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->j:Lcom/facebook/stickers/keyboard/StickerPackInfoView;

    invoke-virtual {p2, v2}, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1397481
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->l:Landroid/widget/ImageButton;

    new-instance p3, LX/8l3;

    invoke-direct {p3, v1, v2}, LX/8l3;-><init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;Lcom/facebook/stickers/model/StickerPack;)V

    invoke-virtual {p2, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1397482
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1397483
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->n:Landroid/view/View;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 1397484
    goto :goto_2

    .line 1397485
    :cond_7
    iget-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->o:Landroid/view/ViewStub;

    invoke-virtual {p2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p2

    iput-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->n:Landroid/view/View;

    .line 1397486
    const p2, 0x7f0d215a

    invoke-virtual {v1, p2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/facebook/stickers/keyboard/StickerPackInfoView;

    iput-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->j:Lcom/facebook/stickers/keyboard/StickerPackInfoView;

    .line 1397487
    const p2, 0x7f0d04de

    invoke-virtual {v1, p2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ProgressBar;

    iput-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->k:Landroid/widget/ProgressBar;

    .line 1397488
    const p2, 0x7f0d09a9

    invoke-virtual {v1, p2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageButton;

    iput-object p2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->l:Landroid/widget/ImageButton;

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1397559
    check-cast p1, LX/8kl;

    .line 1397560
    iget-object v0, p1, LX/8kl;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/1a1;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1397526
    check-cast p2, LX/8kl;

    .line 1397527
    invoke-static {}, LX/8lD;->values()[LX/8lD;

    move-result-object v0

    invoke-static {p2}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->b(LX/8kl;)I

    move-result v1

    aget-object v0, v0, v1

    .line 1397528
    sget-object v1, LX/8lB;->a:[I

    invoke-virtual {v0}, LX/8lD;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1397529
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown item type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397530
    :pswitch_0
    check-cast p1, LX/8lC;

    iget-object v0, p1, LX/8lC;->l:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    .line 1397531
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2V4;->q:LX/0Tn;

    const/4 p2, 0x0

    invoke-interface {v1, v2, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1397532
    const v1, 0x7f021318

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setPlaceholderResourceId(I)V

    .line 1397533
    :goto_0
    :pswitch_1
    return-void

    .line 1397534
    :pswitch_2
    check-cast p2, LX/8km;

    .line 1397535
    check-cast p1, LX/8lC;

    iget-object v1, p1, LX/8lC;->l:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    .line 1397536
    iget-object v0, p2, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397537
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v0, v2

    .line 1397538
    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->m:LX/2V6;

    invoke-virtual {v2}, LX/2V6;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p2, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397539
    iget-object p1, v2, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    move-object v2, p1

    .line 1397540
    if-eqz v2, :cond_0

    .line 1397541
    iget-object v0, p2, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397542
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    move-object v0, v2

    .line 1397543
    :cond_0
    iget-object v2, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p1, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1397544
    iget-object v0, p2, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397545
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1397546
    invoke-virtual {v1, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setContentDescription(Ljava/lang/String;)V

    .line 1397547
    iget-object v0, p2, LX/8km;->b:LX/8kk;

    sget-object v2, LX/8kk;->PROMOTED:LX/8kk;

    if-ne v0, v2, :cond_2

    .line 1397548
    iget v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->l:I

    .line 1397549
    invoke-virtual {v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 1397550
    const/16 v2, 0x35

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setForegroundGravity(I)V

    .line 1397551
    :goto_1
    iget-object v0, p2, LX/8km;->b:LX/8kk;

    sget-object v2, LX/8kk;->PULSING_DOWNLOAD_PREVIEW:LX/8kk;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setIconPulsing(Z)V

    .line 1397552
    goto :goto_0

    .line 1397553
    :cond_1
    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->q:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    .line 1397554
    const v1, 0x7f021319

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setPlaceholderResourceId(I)V

    goto :goto_0

    .line 1397555
    :cond_2
    const/4 v0, 0x0

    .line 1397556
    invoke-virtual {v1, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 1397557
    goto :goto_1

    .line 1397558
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/4m4;)V
    .locals 3

    .prologue
    .line 1397514
    iput-object p1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->w:LX/4m4;

    .line 1397515
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->u:Lcom/facebook/stickers/search/StickerSearchContainer;

    if-eqz v0, :cond_0

    .line 1397516
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->u:Lcom/facebook/stickers/search/StickerSearchContainer;

    .line 1397517
    iput-object p1, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    .line 1397518
    iget-object v1, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->n:Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    iget-object v2, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-virtual {v1, v2}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 1397519
    iget-object v1, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->k:LX/8lq;

    iget-object v2, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-virtual {v1, v2}, LX/8lq;->a(LX/4m4;)Lcom/facebook/stickers/search/TaggedStickersLoader;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->l:Lcom/facebook/stickers/search/TaggedStickersLoader;

    .line 1397520
    invoke-static {v0}, Lcom/facebook/stickers/search/StickerSearchContainer;->g(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    .line 1397521
    iget-object v1, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1397522
    invoke-static {v0}, Lcom/facebook/stickers/search/StickerSearchContainer;->m(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    .line 1397523
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    if-eqz v0, :cond_1

    .line 1397524
    const-string v0, "recentStickers"

    invoke-static {p0, v0}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Ljava/lang/String;)V

    .line 1397525
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1397512
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->r:LX/0Px;

    .line 1397513
    return-void
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1397511
    const/4 v0, -0x1

    return v0
.end method

.method public final synthetic c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1397510
    check-cast p1, LX/8kl;

    invoke-static {p1}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->b(LX/8kl;)I

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1397490
    check-cast p1, LX/8kl;

    const/4 v1, 0x0

    .line 1397491
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    if-ne p1, v0, :cond_2

    .line 1397492
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->s:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    if-eqz v0, :cond_0

    .line 1397493
    iget-object v0, p1, LX/8kl;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Ljava/lang/String;)V

    .line 1397494
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->f:LX/8kd;

    iget-object v2, p1, LX/8kl;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/8kd;->a(Ljava/lang/String;Z)V

    .line 1397495
    :cond_1
    :goto_0
    return-void

    .line 1397496
    :cond_2
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    if-ne p1, v0, :cond_4

    .line 1397497
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2V4;->q:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1397498
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->q:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    if-eqz v0, :cond_3

    .line 1397499
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->q:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    const v2, 0x7f021318

    invoke-virtual {v0, v2}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setPlaceholderResourceId(I)V

    .line 1397500
    :cond_3
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->f:LX/8kd;

    iget-object v2, p1, LX/8kl;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/8kd;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1397501
    :cond_4
    instance-of v0, p1, LX/8km;

    if-eqz v0, :cond_1

    .line 1397502
    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->f:LX/8kd;

    iget-object v3, p1, LX/8kl;->c:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, LX/8km;

    iget-object v0, v0, LX/8km;->b:LX/8kk;

    sget-object v4, LX/8kk;->PROMOTED:LX/8kk;

    if-ne v0, v4, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v3, v0}, LX/8kd;->a(Ljava/lang/String;Z)V

    move-object v0, p1

    .line 1397503
    check-cast v0, LX/8km;

    iget-object v0, v0, LX/8km;->b:LX/8kk;

    sget-object v1, LX/8kk;->DOWNLOAD_PREVIEW:LX/8kk;

    if-ne v0, v1, :cond_1

    .line 1397504
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->g:LX/8j6;

    iget-object v1, p1, LX/8kl;->c:Ljava/lang/String;

    .line 1397505
    const-string v2, "download_preview_tab_viewed"

    invoke-static {v0, v2}, LX/8j6;->d(LX/8j6;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1397506
    const-string v3, "pack_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397507
    iget-object v3, v0, LX/8j6;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1397508
    goto :goto_0

    :cond_5
    move v0, v1

    .line 1397509
    goto :goto_1
.end method

.method public final e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1397489
    const/4 v0, 0x1

    return v0
.end method
