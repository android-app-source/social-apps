.class public Lcom/facebook/stickers/keyboard/StickerKeyboardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/8kl;

.field public static final b:LX/8kl;

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8kl;",
            ">;"
        }
    .end annotation
.end field

.field public E:Lcom/facebook/stickers/model/StickerPack;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:I

.field public G:Z

.field public H:LX/11i;

.field private I:LX/0Uh;

.field public J:Z

.field public K:Z

.field public L:Lcom/facebook/stickers/client/StickerDownloadManager;

.field private M:LX/0Xl;

.field private N:LX/0Yb;

.field private O:LX/0YZ;

.field private P:LX/8jI;

.field public Q:LX/4m4;

.field public e:LX/8jL;

.field public f:LX/8ky;

.field public g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

.field private h:LX/7kc;

.field public i:LX/0TD;

.field private j:LX/8lE;

.field public k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

.field public l:LX/03V;

.field public m:LX/8kc;

.field public n:LX/8kd;

.field public o:LX/0aG;

.field private p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private q:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public r:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/lang/String;

.field public v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/0Sg;

.field public y:LX/8kj;

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1397231
    const-class v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    .line 1397232
    const-class v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const-string v1, "sticker_keyboard_selected"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1397233
    new-instance v0, LX/8kl;

    const-string v1, "recentStickers"

    invoke-direct {v0, v1}, LX/8kl;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    .line 1397234
    new-instance v0, LX/8kl;

    const-string v1, "stickerSearch"

    invoke-direct {v0, v1}, LX/8kl;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1397229
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1397230
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1397227
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1397228
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1397224
    const v0, 0x7f01051d

    const v1, 0x7f0e055c

    invoke-static {p1, v0, v1}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1397225
    invoke-direct {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f()V

    .line 1397226
    return-void
.end method

.method private static a(Lcom/facebook/stickers/keyboard/StickerKeyboardView;LX/8jL;LX/8lE;LX/03V;LX/8kc;LX/8kd;LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Or;LX/0Or;LX/0Sg;LX/8kj;LX/0TD;LX/11i;Lcom/facebook/stickers/client/StickerDownloadManager;LX/0Xl;LX/0Uh;LX/8jI;)V
    .locals 1
    .param p8    # Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
        .annotation runtime Lcom/facebook/stickers/client/IsStickerSearchEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/client/IsStickerTrayDownloadablePacksEnabled;
        .end annotation
    .end param
    .param p12    # LX/8kj;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p15    # Lcom/facebook/stickers/client/StickerDownloadManager;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8jL;",
            "LX/8lE;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8kc;",
            "LX/8kd;",
            "LX/0aG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/8kj;",
            "LX/0TD;",
            "LX/11i;",
            "Lcom/facebook/stickers/client/StickerDownloadManager;",
            "LX/0Xl;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/8jI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1397205
    iput-object p1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->e:LX/8jL;

    .line 1397206
    iput-object p2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->j:LX/8lE;

    .line 1397207
    iput-object p3, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l:LX/03V;

    .line 1397208
    iput-object p4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->m:LX/8kc;

    .line 1397209
    iput-object p5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->n:LX/8kd;

    .line 1397210
    iput-object p6, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->o:LX/0aG;

    .line 1397211
    iput-object p7, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1397212
    iput-object p8, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->q:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    .line 1397213
    iput-object p9, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->v:LX/0Or;

    .line 1397214
    iput-object p10, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->w:LX/0Or;

    .line 1397215
    iput-object p11, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->x:LX/0Sg;

    .line 1397216
    iput-object p12, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    .line 1397217
    iput-object p13, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->i:LX/0TD;

    .line 1397218
    iput-object p14, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    .line 1397219
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->L:Lcom/facebook/stickers/client/StickerDownloadManager;

    .line 1397220
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->M:LX/0Xl;

    .line 1397221
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->I:LX/0Uh;

    .line 1397222
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->P:LX/8jI;

    .line 1397223
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-static/range {v19 .. v19}, LX/8jL;->a(LX/0QB;)LX/8jL;

    move-result-object v2

    check-cast v2, LX/8jL;

    const-class v3, LX/8lE;

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8lE;

    invoke-static/range {v19 .. v19}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static/range {v19 .. v19}, LX/8kc;->a(LX/0QB;)LX/8kc;

    move-result-object v5

    check-cast v5, LX/8kc;

    invoke-static/range {v19 .. v19}, LX/8kd;->a(LX/0QB;)LX/8kd;

    move-result-object v6

    check-cast v6, LX/8kd;

    invoke-static/range {v19 .. v19}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static/range {v19 .. v19}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v19 .. v19}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v9

    check-cast v9, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    const/16 v10, 0x157b

    move-object/from16 v0, v19

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    const/16 v10, 0x157a

    move-object/from16 v0, v19

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x157d

    move-object/from16 v0, v19

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {v19 .. v19}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v12

    check-cast v12, LX/0Sg;

    invoke-static/range {v19 .. v19}, LX/8kj;->a(LX/0QB;)LX/8kj;

    move-result-object v13

    check-cast v13, LX/8kj;

    invoke-static/range {v19 .. v19}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v14

    check-cast v14, LX/0TD;

    invoke-static/range {v19 .. v19}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v15

    check-cast v15, LX/11i;

    invoke-static/range {v19 .. v19}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(LX/0QB;)Lcom/facebook/stickers/client/StickerDownloadManager;

    move-result-object v16

    check-cast v16, Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-static/range {v19 .. v19}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v17

    check-cast v17, LX/0Xl;

    invoke-static/range {v19 .. v19}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v18

    check-cast v18, LX/0Uh;

    invoke-static/range {v19 .. v19}, LX/8jI;->a(LX/0QB;)LX/8jI;

    move-result-object v19

    check-cast v19, LX/8jI;

    invoke-static/range {v1 .. v19}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a(Lcom/facebook/stickers/keyboard/StickerKeyboardView;LX/8jL;LX/8lE;LX/03V;LX/8kc;LX/8kd;LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Or;LX/0Or;LX/0Sg;LX/8kj;LX/0TD;LX/11i;Lcom/facebook/stickers/client/StickerDownloadManager;LX/0Xl;LX/0Uh;LX/8jI;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1397198
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1397199
    :goto_0
    return-void

    .line 1397200
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1397201
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1397202
    iget-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->P:LX/8jI;

    invoke-virtual {v4, v0}, LX/8jI;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1397203
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1397204
    :cond_1
    invoke-static {v2}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8kt;

    invoke-direct {v1, p0}, LX/8kt;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->i:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1397072
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1397073
    const-string v0, "StickerKeyboard create view"

    const v1, -0x1435790a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1397074
    :try_start_0
    const-string v0, "StickerKeyboard onCreateView layoutInflation"

    const v1, 0x117110fb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1397075
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1397076
    const v0, 0x7f030d92

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1397077
    const v0, 0x3db4f311

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 1397078
    const v0, 0x7f0d1f02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tabbedpager/TabbedPager;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    .line 1397079
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    const v2, 0x7f0d1ea9

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1397080
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397081
    const-string v2, "sticker_keyboard"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 1397082
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->setFocusableInTouchMode(Z)V

    .line 1397083
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->z:Z

    .line 1397084
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    .line 1397085
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    new-instance v2, LX/8kp;

    invoke-direct {v2, p0}, LX/8kp;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397086
    iput-object v2, v0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->p:LX/8Cd;

    .line 1397087
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->j:LX/8lE;

    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/8lE;->a(Landroid/content/Context;Landroid/view/LayoutInflater;)Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    .line 1397088
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    new-instance v1, LX/8kq;

    invoke-direct {v1, p0}, LX/8kq;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397089
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    .line 1397090
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(LX/4m4;)V

    .line 1397091
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setAdapter(LX/8CX;)V

    .line 1397092
    new-instance v0, LX/8kr;

    invoke-direct {v0, p0}, LX/8kr;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->O:LX/0YZ;

    .line 1397093
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->M:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_QUEUED"

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->O:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.stickers.STICKER_CONFIG_CHANGED"

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->O:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->N:LX/0Yb;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1397094
    const v0, -0x7c738288

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1397095
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->I:LX/0Uh;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->J:Z

    .line 1397096
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->I:LX/0Uh;

    const/16 v1, 0x429

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->K:Z

    .line 1397097
    return-void

    .line 1397098
    :catchall_0
    move-exception v0

    const v1, 0x10df93eb

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1397099
    :catchall_1
    move-exception v0

    const v1, 0x4ece3aa4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static g$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 4

    .prologue
    .line 1397190
    const-string v0, "fetchStickerMetadataWithLoader started"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    .line 1397191
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    new-instance v1, LX/8ks;

    invoke-direct {v1, p0}, LX/8ks;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397192
    iput-object v1, v0, LX/8kj;->d:LX/3Mb;

    .line 1397193
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1397194
    if-eqz v0, :cond_0

    .line 1397195
    const-string v1, "StickerPackLoadForPopup"

    const v2, 0x55473c4d

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1397196
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    new-instance v1, LX/8kh;

    sget-object v2, LX/8kg;->PREFER_CACHE_IF_UP_TO_DATE:LX/8kg;

    iget-object v3, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-direct {v1, v2, v3}, LX/8kh;-><init>(LX/8kg;LX/4m4;)V

    invoke-virtual {v0, v1}, LX/8kj;->a(LX/8kh;)V

    .line 1397197
    return-void
.end method

.method public static h(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 4

    .prologue
    .line 1397235
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1397236
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1397237
    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v3}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1397238
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1397239
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;LX/0Px;)V

    .line 1397240
    return-void
.end method

.method public static i(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 10

    .prologue
    .line 1397100
    const-string v0, "StickerKeyboard updateStickerPacks"

    const v1, -0x4efaaefb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1397101
    :try_start_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1397102
    if-eqz v0, :cond_0

    .line 1397103
    const-string v1, "StickerKeyboardPopulatePacks"

    const v2, -0x1a6a4fe3

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1397104
    :cond_0
    :try_start_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 1397105
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    .line 1397106
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->u:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->u:Ljava/lang/String;

    move-object v1, v0

    .line 1397107
    :goto_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2V4;->e:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 1397108
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a()V

    .line 1397109
    sget-object v0, LX/8ko;->a:[I

    iget-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v4}, LX/4m4;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 1397110
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    move v0, v0

    .line 1397111
    if-eqz v0, :cond_1

    .line 1397112
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    sget-object v4, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1397113
    :cond_1
    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->m(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1397114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->z:Z

    .line 1397115
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    sget-object v4, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1397116
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-virtual {v0, v4}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(Ljava/util/List;)V

    .line 1397117
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->F:I

    .line 1397118
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    .line 1397119
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v0, v4

    .line 1397120
    iget-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v0, v4}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->B:Ljava/util/List;

    iget-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    .line 1397121
    iget-object v5, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1397122
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/stickers/model/StickerPack;

    .line 1397123
    iget-object v0, v5, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v0

    .line 1397124
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1397125
    const/4 v5, 0x1

    .line 1397126
    :goto_2
    move v0, v5

    .line 1397127
    if-nez v0, :cond_7

    .line 1397128
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    new-instance v4, LX/8km;

    iget-object v5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    sget-object v6, LX/8kk;->PULSING_DOWNLOAD_PREVIEW:LX/8kk;

    invoke-direct {v4, v5, v6}, LX/8km;-><init>(Lcom/facebook/stickers/model/StickerPack;LX/8kk;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1397129
    :goto_3
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1397130
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v5, v5

    .line 1397131
    iget-object v6, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v5, v6}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1397132
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1397133
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1397134
    iget-object v5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    new-instance v6, LX/8km;

    sget-object v7, LX/8kk;->DOWNLOADED:LX/8kk;

    invoke-direct {v6, v0, v7}, LX/8km;-><init>(Lcom/facebook/stickers/model/StickerPack;LX/8kk;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 1397135
    :catchall_0
    move-exception v0

    .line 1397136
    :try_start_2
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v2, LX/8lJ;->a:LX/8lF;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 1397137
    if-eqz v1, :cond_5

    .line 1397138
    const-string v2, "StickerKeyboardPopulatePacks"

    const v3, -0x30b351f4    # -3.4339584E9f

    invoke-static {v1, v2, v3}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1397139
    const-string v2, "StickerPackLoadForPopup"

    const v3, -0x2b6252a3

    invoke-static {v1, v2, v3}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1397140
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v2, LX/8lJ;->a:LX/8lF;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 1397141
    :cond_5
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1397142
    :catchall_1
    move-exception v0

    const v1, 0x6fecbe8e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1397143
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2V4;->c:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 1397144
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    goto :goto_3

    .line 1397145
    :cond_8
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setShowStartTabButton(Z)V

    .line 1397146
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setShowEndTabButton(Z)V

    .line 1397147
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    new-instance v4, LX/8ku;

    invoke-direct {v4, p0}, LX/8ku;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setEndTabButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1397148
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080340

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setEndTabButtonContentDescription(Ljava/lang/String;)V

    .line 1397149
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2V4;->h:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 1397150
    const/16 v9, 0x9

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1397151
    if-gt v0, v9, :cond_f

    .line 1397152
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 1397153
    :goto_5
    iget-object v7, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v7, v4}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setEndTabButtonBadgeText(Ljava/lang/String;)V

    .line 1397154
    iget-object v7, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    if-lez v0, :cond_10

    move v4, v5

    :goto_6
    invoke-virtual {v7, v4}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setEndTabButtonBadgeVisibility(Z)V

    .line 1397155
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1397156
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1397157
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v5, v5

    .line 1397158
    iget-object v6, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v5, v6}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1397159
    iget-object v5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v5, :cond_a

    .line 1397160
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1397161
    iget-object v6, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    .line 1397162
    iget-object v7, v6, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v7

    .line 1397163
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1397164
    :cond_a
    iget-object v5, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1397165
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1397166
    iget-object v5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    new-instance v6, LX/8km;

    sget-object v7, LX/8kk;->DOWNLOAD_PREVIEW:LX/8kk;

    invoke-direct {v6, v0, v7}, LX/8km;-><init>(Lcom/facebook/stickers/model/StickerPack;LX/8kk;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1397167
    :cond_b
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 1397168
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8kl;

    .line 1397169
    iget-object v0, v0, LX/8kl;->c:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1397170
    :cond_c
    move-object v0, v2

    .line 1397171
    new-instance v2, LX/3ea;

    sget-object v4, LX/3do;->STORE_PACKS:LX/3do;

    sget-object v5, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {v2, v4, v5}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    iget-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-static {v4}, LX/8jc;->a(LX/4m4;)Ljava/lang/String;

    move-result-object v4

    .line 1397172
    iput-object v4, v2, LX/3ea;->c:Ljava/lang/String;

    .line 1397173
    move-object v2, v2

    .line 1397174
    invoke-virtual {v2}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v2

    .line 1397175
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1397176
    const-string v5, "fetchStickerPacksParams"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1397177
    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->o:LX/0aG;

    const-string v5, "fetch_sticker_packs"

    const v6, 0x472aee37

    invoke-static {v2, v5, v4, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 1397178
    new-instance v4, LX/8kw;

    invoke-direct {v4, p0, v0}, LX/8kw;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;Ljava/util/Set;)V

    .line 1397179
    invoke-static {v2, v4}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->r:LX/1Mv;

    .line 1397180
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->i:LX/0TD;

    invoke-static {v2, v4, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1397181
    :cond_d
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setItems(Ljava/util/List;)V

    .line 1397182
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Ljava/lang/String;)V

    .line 1397183
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1397184
    const v0, 0x45fc75bb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1397185
    return-void

    .line 1397186
    :pswitch_0
    :try_start_4
    iget-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->J:Z

    goto/16 :goto_1

    .line 1397187
    :pswitch_1
    iget-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->K:Z

    goto/16 :goto_1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_e
    :try_start_5
    const/4 v5, 0x0

    goto/16 :goto_2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1397188
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0804df

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    :cond_10
    move v4, v6

    .line 1397189
    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static l(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 1

    .prologue
    .line 1397069
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    if-eqz v0, :cond_0

    .line 1397070
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    invoke-interface {v0}, LX/8ky;->a()V

    .line 1397071
    :cond_0
    return-void
.end method

.method private static m(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)Z
    .locals 3

    .prologue
    .line 1397065
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1397066
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v0, v2}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1397067
    const/4 v0, 0x1

    .line 1397068
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1397048
    iget-boolean v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->z:Z

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->m(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1397049
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    sget-object v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1397050
    if-ltz v1, :cond_0

    add-int/lit8 v0, v1, 0x1

    .line 1397051
    :cond_0
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    sget-object v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    invoke-interface {v1, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1397052
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    sget-object v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a:LX/8kl;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(ILjava/lang/Object;)V

    .line 1397053
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->z:Z

    .line 1397054
    iget v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->F:I

    .line 1397055
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(Ljava/util/List;)V

    .line 1397056
    return-void

    .line 1397057
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->z:Z

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->m(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1397058
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    sget-object v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->b:LX/8kl;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1397059
    if-ltz v1, :cond_3

    add-int/lit8 v1, v1, 0x1

    .line 1397060
    :goto_1
    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1397061
    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c(I)V

    .line 1397062
    iput-boolean v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->z:Z

    .line 1397063
    iget v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->F:I

    goto :goto_0

    :cond_3
    move v1, v0

    .line 1397064
    goto :goto_1
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x545f4054

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397045
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1397046
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->N:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1397047
    const/16 v1, 0x2d

    const v2, -0x1d2bb3b7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/16 v0, 0x2c

    const v1, -0x654bff98

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397024
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1397025
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->N:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1397026
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->u:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1397027
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2V4;->c:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->u:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/2V4;->e:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v3}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->getTabContainerScrollOffsetToRestore()I

    move-result v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1397028
    :cond_0
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->r:LX/1Mv;

    if-eqz v1, :cond_1

    .line 1397029
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->r:LX/1Mv;

    invoke-virtual {v1, v5}, LX/1Mv;->a(Z)V

    .line 1397030
    iput-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->r:LX/1Mv;

    .line 1397031
    :cond_1
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->t:LX/1Mv;

    if-eqz v1, :cond_2

    .line 1397032
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->t:LX/1Mv;

    invoke-virtual {v1, v5}, LX/1Mv;->a(Z)V

    .line 1397033
    iput-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->t:LX/1Mv;

    .line 1397034
    :cond_2
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->s:LX/1Mv;

    if-eqz v1, :cond_3

    .line 1397035
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->s:LX/1Mv;

    invoke-virtual {v1, v5}, LX/1Mv;->a(Z)V

    .line 1397036
    iput-object v4, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->s:LX/1Mv;

    .line 1397037
    :cond_3
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    invoke-virtual {v1}, LX/8kj;->a()V

    .line 1397038
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    .line 1397039
    iput-object v4, v1, LX/8kj;->d:LX/3Mb;

    .line 1397040
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    .line 1397041
    iput-object v4, v1, Lcom/facebook/messaging/tabbedpager/TabbedPager;->p:LX/8Cd;

    .line 1397042
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    .line 1397043
    iput-object v4, v1, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    .line 1397044
    const/16 v1, 0x2d

    const v2, -0x5c9972bd

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1397004
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 1397005
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1397006
    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1397007
    if-ne v3, v4, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1397008
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1397009
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 1397010
    if-lez v0, :cond_1

    if-lez v3, :cond_1

    .line 1397011
    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1397012
    iget-object v5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->h:LX/7kc;

    if-nez v5, :cond_4

    .line 1397013
    :goto_2
    iget-object v5, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-static {v5}, LX/8kW;->a(LX/4m4;)LX/7kb;

    move-result-object v5

    .line 1397014
    new-instance v6, LX/7kd;

    invoke-direct {v6, v4, v5}, LX/7kd;-><init>(Landroid/content/res/Resources;LX/7kb;)V

    const v5, 0x7f0b025b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v6, v0, v3, v2}, LX/7kd;->a(IIZ)LX/7kc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->h:LX/7kc;

    .line 1397015
    if-eqz v1, :cond_0

    .line 1397016
    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397017
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->h:LX/7kc;

    .line 1397018
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->n:LX/7kc;

    .line 1397019
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1397020
    return-void

    :cond_2
    move v0, v2

    .line 1397021
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1397022
    goto :goto_1

    :cond_4
    move v1, v2

    .line 1397023
    goto :goto_2
.end method

.method public setInterface(LX/4m4;)V
    .locals 1

    .prologue
    .line 1396995
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    if-eq v0, p1, :cond_0

    .line 1396996
    iput-object p1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    .line 1396997
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    if-eqz v0, :cond_0

    .line 1396998
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->a(LX/4m4;)V

    .line 1396999
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->h:LX/7kc;

    if-eqz v0, :cond_0

    .line 1397000
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    invoke-virtual {v0}, LX/8kj;->a()V

    .line 1397001
    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397002
    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->h(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397003
    :cond_0
    return-void
.end method
