.class public Lcom/facebook/stickers/keyboard/StickerPackPageView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/stickers/client/StickerDownloadManager;

.field public final c:LX/8jY;

.field private final d:LX/0Yb;

.field private final e:LX/8ms;

.field private final f:LX/4m4;

.field private final g:LX/7kc;

.field private final h:LX/8mp;

.field public final i:Landroid/widget/GridView;

.field public j:Lcom/facebook/stickers/keyboard/StickerPackInfoView;

.field public k:Landroid/widget/ProgressBar;

.field public l:Landroid/widget/ImageButton;

.field public m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

.field public n:Landroid/view/View;

.field public o:Landroid/view/ViewStub;

.field public p:LX/8l5;

.field public q:Ljava/lang/String;

.field public r:Lcom/facebook/stickers/model/StickerPack;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1397327
    const-class v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    const-string v1, "sticker_keyboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/stickers/client/StickerDownloadManager;LX/8jY;LX/0Xl;LX/8mq;LX/8ms;LX/4m4;LX/7kc;)V
    .locals 3
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p7    # LX/4m4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/7kc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1397344
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1397345
    iput-object p2, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    .line 1397346
    iput-object p3, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    .line 1397347
    iput-object p6, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->e:LX/8ms;

    .line 1397348
    iput-object p7, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->f:LX/4m4;

    .line 1397349
    iput-object p8, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->g:LX/7kc;

    .line 1397350
    const v0, 0x7f030d97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1397351
    const v0, 0x7f0d215e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->o:Landroid/view/ViewStub;

    .line 1397352
    const v0, 0x7f0d215f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    .line 1397353
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->g:LX/7kc;

    .line 1397354
    iget v2, v1, LX/7kc;->a:I

    move v1, v2

    .line 1397355
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 1397356
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    new-instance v1, LX/8kz;

    invoke-direct {v1, p0}, LX/8kz;-><init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397357
    iput-object v1, v0, LX/8jY;->e:LX/3Mb;

    .line 1397358
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->f:LX/4m4;

    invoke-virtual {p5, v0, v1}, LX/8mq;->a(Landroid/widget/GridView;LX/4m4;)LX/8mp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->h:LX/8mp;

    .line 1397359
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->h:LX/8mp;

    new-instance v1, LX/8l1;

    invoke-direct {v1, p0}, LX/8l1;-><init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397360
    iput-object v1, v0, LX/8mp;->e:LX/8l0;

    .line 1397361
    new-instance v0, LX/8l2;

    invoke-direct {v0, p0}, LX/8l2;-><init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397362
    invoke-interface {p4}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->d:LX/0Yb;

    .line 1397363
    return-void
.end method

.method public static a(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V
    .locals 2

    .prologue
    .line 1397340
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1397341
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1397342
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1397343
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1397364
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    invoke-virtual {v0}, LX/8jY;->a()V

    .line 1397365
    iput-object p2, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->q:Ljava/lang/String;

    .line 1397366
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    .line 1397367
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->e:LX/8ms;

    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->g:LX/7kc;

    invoke-virtual {v0, v1, p2, v2}, LX/8ms;->a(Landroid/content/Context;Ljava/lang/String;LX/7kc;)Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    .line 1397368
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    sget-object v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1397369
    iput-object v1, v0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 1397370
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(LX/0Px;)V

    .line 1397371
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1397372
    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397373
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, -0x4e661dd7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397332
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1397333
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->d:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1397334
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v2, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    invoke-virtual {v1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1397335
    invoke-static {p0}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397336
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    new-instance v2, LX/8jW;

    iget-object v3, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    .line 1397337
    iget-object p0, v3, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v3, p0

    .line 1397338
    invoke-direct {v2, v3}, LX/8jW;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2}, LX/8jY;->a(LX/8jW;)V

    .line 1397339
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7830ab75

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x260cb01f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397328
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1397329
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->d:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1397330
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    invoke-virtual {v1}, LX/8jY;->a()V

    .line 1397331
    const/16 v1, 0x2d

    const v2, -0x29a7a7b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
