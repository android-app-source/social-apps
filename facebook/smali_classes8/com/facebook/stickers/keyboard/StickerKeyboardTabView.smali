.class public Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Landroid/animation/ValueAnimator;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1396788
    const-class v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    const-string v1, "sticker_keyboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1396789
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1396790
    const p1, 0x7f030d9f

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1396791
    const p1, 0x7f0d217b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1396792
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setClickable(Z)V

    .line 1396793
    const p1, 0x7f0212b0

    invoke-virtual {p0, p1}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setBackgroundResource(I)V

    .line 1396794
    return-void
.end method

.method public static setTabImageScale(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;F)V
    .locals 1

    .prologue
    .line 1396795
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleX(F)V

    .line 1396796
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleY(F)V

    .line 1396797
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x226f4bb2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1396798
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 1396799
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1396800
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1396801
    const/16 v1, 0x2d

    const v2, -0x7e35376

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2088dd2b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1396802
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 1396803
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 1396804
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1396805
    const/16 v1, 0x2d

    const v2, -0x58775e8b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setContentDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1396806
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1396807
    return-void
.end method

.method public setIconPulsing(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 1396808
    if-eqz p1, :cond_2

    .line 1396809
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 1396810
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    .line 1396811
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1396812
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 1396813
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1396814
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1396815
    new-instance v0, LX/8kn;

    invoke-direct {v0, p0}, LX/8kn;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;)V

    .line 1396816
    iget-object v1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1396817
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setTabImageScale(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;F)V

    .line 1396818
    :cond_1
    :goto_0
    return-void

    .line 1396819
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 1396820
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 1396821
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1396822
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->c:Landroid/animation/ValueAnimator;

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public setPlaceholderResourceId(I)V
    .locals 1

    .prologue
    .line 1396823
    invoke-virtual {p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1396824
    iget-object p1, p0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object p1

    check-cast p1, LX/1af;

    invoke-virtual {p1, v0}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1396825
    return-void
.end method
