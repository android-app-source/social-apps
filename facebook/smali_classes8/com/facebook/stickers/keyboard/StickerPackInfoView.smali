.class public Lcom/facebook/stickers/keyboard/StickerPackInfoView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1397269
    const-class v0, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    const-string v1, "sticker_keyboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1397266
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1397267
    invoke-direct {p0}, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a()V

    .line 1397268
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1397263
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1397264
    invoke-direct {p0}, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a()V

    .line 1397265
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1397241
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1397242
    invoke-direct {p0}, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a()V

    .line 1397243
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1397257
    const v0, 0x7f030d94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1397258
    const v0, 0x7f0d1b29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1397259
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1397260
    const v0, 0x7f0d215b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1397261
    const v0, 0x7f0d0550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1397262
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 3

    .prologue
    .line 1397244
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1397245
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 1397246
    sget-object v2, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1397247
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1397248
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1397249
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397250
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1397251
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1397252
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397253
    iget-object v0, p0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1397254
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1397255
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397256
    return-void
.end method
