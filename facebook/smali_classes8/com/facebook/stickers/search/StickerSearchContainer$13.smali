.class public final Lcom/facebook/stickers/search/StickerSearchContainer$13;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1397764
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer$13;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iput-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer$13;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1397765
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer$13;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer$13;->a:Ljava/lang/String;

    .line 1397766
    iget-object v2, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->m:Lcom/facebook/stickers/search/StickerSearchLoader;

    invoke-virtual {v2}, LX/6Lb;->a()V

    .line 1397767
    sget-object v2, LX/8lg;->WAIT_FOR_SEARCH_RESULTS:LX/8lg;

    invoke-static {v0, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1397768
    iget-object v2, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->m:Lcom/facebook/stickers/search/StickerSearchLoader;

    new-instance v3, LX/8li;

    iget-object p0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-direct {v3, v1, p0}, LX/8li;-><init>(Ljava/lang/String;LX/4m4;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 1397769
    iget-object v2, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    .line 1397770
    const-string v3, "search"

    invoke-static {v2, v3}, LX/8j9;->e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1397771
    const-string p0, "search_query"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397772
    const-string p0, "operation_status"

    sget-object v0, LX/8j8;->STARTED:LX/8j8;

    invoke-virtual {v3, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397773
    iget-object p0, v2, LX/8j9;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1397774
    return-void
.end method
