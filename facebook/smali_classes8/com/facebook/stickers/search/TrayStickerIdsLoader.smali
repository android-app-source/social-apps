.class public Lcom/facebook/stickers/search/TrayStickerIdsLoader;
.super LX/6Lb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "LX/4m4;",
        "LX/8ls;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0SI;

.field private final c:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398251
    const-class v0, Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0SI;LX/0aG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1398247
    invoke-direct {p0, p3}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1398248
    iput-object p1, p0, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->b:LX/0SI;

    .line 1398249
    iput-object p2, p0, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->c:LX/0aG;

    .line 1398250
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/stickers/search/TrayStickerIdsLoader;
    .locals 4

    .prologue
    .line 1398252
    new-instance v3, Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/stickers/search/TrayStickerIdsLoader;-><init>(LX/0SI;LX/0aG;Ljava/util/concurrent/Executor;)V

    .line 1398253
    move-object v0, v3

    .line 1398254
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 1398235
    check-cast p1, LX/4m4;

    .line 1398236
    new-instance v0, LX/3ea;

    sget-object v1, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object v2, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-direct {v0, v1, v2}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    invoke-static {p1}, LX/8jc;->a(LX/4m4;)Ljava/lang/String;

    move-result-object v1

    .line 1398237
    iput-object v1, v0, LX/3ea;->c:Ljava/lang/String;

    .line 1398238
    move-object v0, v0

    .line 1398239
    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v2

    .line 1398240
    iget-object v0, p0, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->c:LX/0aG;

    const-string v1, "fetch_sticker_packs"

    .line 1398241
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1398242
    const-string v4, "fetchStickerPacksParams"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1398243
    const-string v4, "overridden_viewer_context"

    iget-object v5, p0, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->b:LX/0SI;

    invoke-interface {v5}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1398244
    move-object v2, v3

    .line 1398245
    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x1bc0b21d

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1398246
    new-instance v1, LX/8lr;

    invoke-direct {v1, p0}, LX/8lr;-><init>(Lcom/facebook/stickers/search/TrayStickerIdsLoader;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 1

    .prologue
    .line 1398234
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    return-object v0
.end method
