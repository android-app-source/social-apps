.class public Lcom/facebook/stickers/search/StickerSearchContainer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Landroid/view/View;

.field public B:Landroid/view/View;

.field public C:Landroid/widget/FrameLayout;

.field public D:Landroid/widget/FrameLayout;

.field public E:Lcom/facebook/resources/ui/FbTextView;

.field public F:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

.field public G:LX/8mu;

.field public H:LX/8lU;

.field public I:LX/7kc;

.field public J:LX/0wd;

.field public K:LX/8l6;

.field public L:LX/8lg;

.field public M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field public O:Z

.field public P:Z

.field public Q:Ljava/lang/Runnable;

.field public R:LX/4m4;

.field public final S:I

.field public T:Z

.field public U:Z

.field private final c:F

.field private final d:I

.field private final e:I

.field public f:Z

.field public g:LX/0wW;

.field public h:Landroid/view/inputmethod/InputMethodManager;

.field private i:LX/8jY;

.field public j:Lcom/facebook/stickers/search/StickerTagsLoader;

.field public k:LX/8lq;

.field public l:Lcom/facebook/stickers/search/TaggedStickersLoader;

.field public m:Lcom/facebook/stickers/search/StickerSearchLoader;

.field public n:Lcom/facebook/stickers/search/TrayStickerIdsLoader;

.field public o:LX/8ms;

.field public p:LX/8mq;

.field private q:LX/0Sh;

.field public r:Landroid/os/Handler;

.field public s:LX/03V;

.field public t:LX/8j9;

.field private u:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public v:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:Landroid/content/Context;

.field public x:Lcom/facebook/resources/ui/FbTextView;

.field public y:Landroid/view/View;

.field public z:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1398134
    const-class v0, Lcom/facebook/stickers/search/StickerSearchContainer;

    const-string v1, "sticker_keyboard"

    const-string v2, "sticker_search"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/search/StickerSearchContainer;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1398135
    const-class v0, Lcom/facebook/stickers/search/StickerSearchContainer;

    sput-object v0, Lcom/facebook/stickers/search/StickerSearchContainer;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/4m4;)V
    .locals 10

    .prologue
    .line 1398091
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1398092
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->c:F

    .line 1398093
    const/16 v0, 0x12c

    iput v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->d:I

    .line 1398094
    const/16 v0, 0xc

    iput v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->e:I

    .line 1398095
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1398096
    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->v:LX/0Rf;

    .line 1398097
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->w:Landroid/content/Context;

    .line 1398098
    iput-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    .line 1398099
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->S:I

    .line 1398100
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1398101
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/facebook/stickers/search/StickerSearchContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1398102
    const v2, 0x7f030d98

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1398103
    const v2, 0x7f0d2169

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->x:Lcom/facebook/resources/ui/FbTextView;

    .line 1398104
    const v2, 0x7f0d2163

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    .line 1398105
    const v2, 0x7f0d2164

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbEditText;

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    .line 1398106
    const v2, 0x7f0d2165

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->A:Landroid/view/View;

    .line 1398107
    const v2, 0x7f0d2168

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->B:Landroid/view/View;

    .line 1398108
    const v2, 0x7f0d2162

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->C:Landroid/widget/FrameLayout;

    .line 1398109
    const v2, 0x7f0d2161

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    .line 1398110
    const v2, 0x7f0d2160

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    .line 1398111
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080024

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398112
    iget-boolean v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->T:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    sget-object v5, LX/4m4;->POSTS:LX/4m4;

    if-ne v2, v5, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->U:Z

    .line 1398113
    iget-boolean v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->U:Z

    if-eqz v2, :cond_0

    .line 1398114
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->A:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1398115
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    const/4 v5, 0x0

    invoke-static {v2, v5}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1398116
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0a0e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/facebook/resources/ui/FbEditText;->setTextColor(I)V

    .line 1398117
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1398118
    :cond_0
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    new-instance v4, LX/8lS;

    invoke-direct {v4, p0}, LX/8lS;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1398119
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->B:Landroid/view/View;

    new-instance v4, LX/8lT;

    invoke-direct {v4, p0}, LX/8lT;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1398120
    new-instance v2, LX/8lU;

    invoke-direct {v2, p0}, LX/8lU;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->H:LX/8lU;

    .line 1398121
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->g:LX/0wW;

    invoke-virtual {v2}, LX/0wW;->a()LX/0wd;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->J:LX/0wd;

    .line 1398122
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->J:LX/0wd;

    new-instance v4, LX/0wT;

    const-wide v6, 0x4082c00000000000L    # 600.0

    const-wide v8, 0x4042800000000000L    # 37.0

    invoke-direct {v4, v6, v7, v8, v9}, LX/0wT;-><init>(DD)V

    invoke-virtual {v2, v4}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v2

    new-instance v4, LX/8lV;

    invoke-direct {v4, p0}, LX/8lV;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v2, v4}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1398123
    invoke-static {p0, v3}, Lcom/facebook/stickers/search/StickerSearchContainer;->a(Lcom/facebook/stickers/search/StickerSearchContainer;Z)V

    .line 1398124
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1398125
    iget-object v3, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->m:Lcom/facebook/stickers/search/StickerSearchLoader;

    new-instance v4, LX/8ld;

    invoke-direct {v4, p0, v2}, LX/8ld;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/util/List;)V

    invoke-virtual {v3, v4}, LX/6Lb;->a(LX/3Mb;)V

    .line 1398126
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->j:Lcom/facebook/stickers/search/StickerTagsLoader;

    new-instance v3, LX/8lY;

    invoke-direct {v3, p0}, LX/8lY;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(LX/3Mb;)V

    .line 1398127
    invoke-static {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->g(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    .line 1398128
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->n:Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    new-instance v3, LX/8le;

    invoke-direct {v3, p0}, LX/8le;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(LX/3Mb;)V

    .line 1398129
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->n:Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    iget-object v3, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-virtual {v2, v3}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 1398130
    sget-object v2, LX/8lg;->UNINITIALIZED:LX/8lg;

    invoke-static {p0, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1398131
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->w:Landroid/content/Context;

    const v4, 0x7f01051e

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a01ce

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v3, v4, v5}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1398132
    return-void

    :cond_1
    move v2, v4

    .line 1398133
    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/Boolean;LX/0wW;LX/8jY;Lcom/facebook/stickers/search/StickerTagsLoader;LX/8lq;Lcom/facebook/stickers/search/TrayStickerIdsLoader;Lcom/facebook/stickers/search/StickerSearchLoader;LX/8ms;LX/8mq;Landroid/view/inputmethod/InputMethodManager;LX/0Sh;Landroid/os/Handler;LX/03V;LX/8j9;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0W9;)V
    .locals 3
    .param p0    # Lcom/facebook/stickers/search/StickerSearchContainer;
        .annotation runtime Lcom/facebook/stickers/abtest/IsStickerContentSearchEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Sh;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1398073
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->f:Z

    .line 1398074
    iput-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->g:LX/0wW;

    .line 1398075
    iput-object p3, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->i:LX/8jY;

    .line 1398076
    iput-object p4, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->j:Lcom/facebook/stickers/search/StickerTagsLoader;

    .line 1398077
    iput-object p5, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->k:LX/8lq;

    .line 1398078
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-virtual {p5, v1}, LX/8lq;->a(LX/4m4;)Lcom/facebook/stickers/search/TaggedStickersLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->l:Lcom/facebook/stickers/search/TaggedStickersLoader;

    .line 1398079
    iput-object p6, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->n:Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    .line 1398080
    iput-object p7, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->m:Lcom/facebook/stickers/search/StickerSearchLoader;

    .line 1398081
    iput-object p8, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->o:LX/8ms;

    .line 1398082
    iput-object p9, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->p:LX/8mq;

    .line 1398083
    iput-object p10, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->h:Landroid/view/inputmethod/InputMethodManager;

    .line 1398084
    iput-object p11, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->q:LX/0Sh;

    .line 1398085
    iput-object p12, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->r:Landroid/os/Handler;

    .line 1398086
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->s:LX/03V;

    .line 1398087
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    .line 1398088
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1398089
    invoke-virtual/range {p16 .. p16}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "en"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->T:Z

    .line 1398090
    return-void
.end method

.method public static a(Lcom/facebook/stickers/search/StickerSearchContainer;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1398063
    iget-boolean v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->U:Z

    if-nez v0, :cond_1

    .line 1398064
    if-eqz p1, :cond_0

    .line 1398065
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1398066
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1398067
    :goto_0
    return-void

    .line 1398068
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1398069
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1398070
    :cond_1
    if-eqz p1, :cond_2

    .line 1398071
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1398072
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-static/range {v17 .. v17}, LX/8j2;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static/range {v17 .. v17}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-static/range {v17 .. v17}, LX/8jY;->a(LX/0QB;)LX/8jY;

    move-result-object v4

    check-cast v4, LX/8jY;

    invoke-static/range {v17 .. v17}, Lcom/facebook/stickers/search/StickerTagsLoader;->a(LX/0QB;)Lcom/facebook/stickers/search/StickerTagsLoader;

    move-result-object v5

    check-cast v5, Lcom/facebook/stickers/search/StickerTagsLoader;

    const-class v6, LX/8lq;

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/8lq;

    invoke-static/range {v17 .. v17}, Lcom/facebook/stickers/search/TrayStickerIdsLoader;->a(LX/0QB;)Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    move-result-object v7

    check-cast v7, Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    invoke-static/range {v17 .. v17}, Lcom/facebook/stickers/search/StickerSearchLoader;->a(LX/0QB;)Lcom/facebook/stickers/search/StickerSearchLoader;

    move-result-object v8

    check-cast v8, Lcom/facebook/stickers/search/StickerSearchLoader;

    const-class v9, LX/8ms;

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/8ms;

    const-class v10, LX/8mq;

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/8mq;

    invoke-static/range {v17 .. v17}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v11

    check-cast v11, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {v17 .. v17}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    invoke-static/range {v17 .. v17}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v13

    check-cast v13, Landroid/os/Handler;

    invoke-static/range {v17 .. v17}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {v17 .. v17}, LX/8j9;->a(LX/0QB;)LX/8j9;

    move-result-object v15

    check-cast v15, LX/8j9;

    invoke-static/range {v17 .. v17}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v16

    check-cast v16, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v17 .. v17}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v17

    check-cast v17, LX/0W9;

    invoke-static/range {v1 .. v17}, Lcom/facebook/stickers/search/StickerSearchContainer;->a(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/Boolean;LX/0wW;LX/8jY;Lcom/facebook/stickers/search/StickerTagsLoader;LX/8lq;Lcom/facebook/stickers/search/TrayStickerIdsLoader;Lcom/facebook/stickers/search/StickerSearchLoader;LX/8ms;LX/8mq;Landroid/view/inputmethod/InputMethodManager;LX/0Sh;Landroid/os/Handler;LX/03V;LX/8j9;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0W9;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;FLX/8lf;)V
    .locals 4

    .prologue
    .line 1398057
    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b07c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    neg-int v0, v0

    .line 1398058
    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/0yq;->b(FFF)F

    move-result v0

    .line 1398059
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->J:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1398060
    sget-object v1, LX/8lf;->JUMP_TO_VALUE:LX/8lf;

    invoke-virtual {p2, v1}, LX/8lf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1398061
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->J:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1398062
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;LX/0Px;LX/8lZ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;",
            "LX/8lZ;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1398043
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1398044
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1398045
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1398046
    iget-object v6, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->v:LX/0Rf;

    iget-object v7, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1398047
    iget-object v6, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1398048
    :goto_1
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1398049
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1398050
    :cond_1
    const/16 v6, 0xc

    if-ge v1, v6, :cond_0

    .line 1398051
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1398052
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->i:LX/8jY;

    invoke-virtual {v0}, LX/8jY;->a()V

    .line 1398053
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->i:LX/8jY;

    new-instance v1, LX/8lQ;

    invoke-direct {v1, p0, v4, p2}, LX/8lQ;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/util/ArrayList;LX/8lZ;)V

    .line 1398054
    iput-object v1, v0, LX/8jY;->e:LX/3Mb;

    .line 1398055
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->i:LX/8jY;

    new-instance v1, LX/8jW;

    invoke-direct {v1, v3}, LX/8jW;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, LX/8jY;->a(LX/8jW;)V

    .line 1398056
    return-void
.end method

.method public static g(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 2

    .prologue
    .line 1398041
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->l:Lcom/facebook/stickers/search/TaggedStickersLoader;

    new-instance v1, LX/8lb;

    invoke-direct {v1, p0}, LX/8lb;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v0, v1}, LX/6Lb;->a(LX/3Mb;)V

    .line 1398042
    return-void
.end method

.method public static m(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 1

    .prologue
    .line 1397923
    sget-object v0, LX/8lg;->TAG_SELECTION:LX/8lg;

    invoke-static {p0, v0}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1397924
    return-void
.end method

.method public static setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V
    .locals 5

    .prologue
    const v4, 0x3e4ccccd    # 0.2f

    const/4 v1, 0x1

    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 1398007
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->q:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1398008
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    if-ne p1, v2, :cond_1

    .line 1398009
    :cond_0
    :goto_0
    return-void

    .line 1398010
    :cond_1
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1398011
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->C:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1398012
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1398013
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1398014
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 1398015
    iput-boolean v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->P:Z

    .line 1398016
    const/4 v2, 0x0

    sget-object v3, LX/8lf;->SPRING_TO_VALUE:LX/8lf;

    invoke-static {p0, v2, v3}, Lcom/facebook/stickers/search/StickerSearchContainer;->a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;FLX/8lf;)V

    .line 1398017
    sget-object v2, LX/8lR;->a:[I

    invoke-virtual {p1}, LX/8lg;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1398018
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "StickerSearchContainer has unhandled state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1398019
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1398020
    :goto_1
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    .line 1398021
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->M:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1398022
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1398023
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->M:Ljava/lang/String;

    goto :goto_0

    .line 1398024
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1398025
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    move v0, v1

    .line 1398026
    goto :goto_1

    .line 1398027
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1398028
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 1398029
    iput-boolean v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->P:Z

    goto :goto_1

    .line 1398030
    :pswitch_3
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->C:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    .line 1398031
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1398032
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 1398033
    iput-boolean v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->P:Z

    goto :goto_1

    .line 1398034
    :pswitch_5
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398035
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 1398036
    :pswitch_6
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->C:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    .line 1398037
    :pswitch_7
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080362

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398038
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 1398039
    :pswitch_8
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398040
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static setQuery(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1397992
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->N:Ljava/lang/String;

    .line 1397993
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->N:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/stickers/search/StickerSearchContainer;->a(Lcom/facebook/stickers/search/StickerSearchContainer;Z)V

    .line 1397994
    iget-boolean v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->O:Z

    if-eqz v0, :cond_0

    .line 1397995
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->O:Z

    .line 1397996
    :goto_0
    return-void

    .line 1397997
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->Q:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 1397998
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->r:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->Q:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1397999
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->N:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1398000
    invoke-static {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->m(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    goto :goto_0

    .line 1398001
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->N:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_3

    .line 1398002
    sget-object v0, LX/8lg;->TYPE_STARTED:LX/8lg;

    invoke-static {p0, v0}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    goto :goto_0

    .line 1398003
    :cond_3
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->N:Ljava/lang/String;

    .line 1398004
    new-instance v2, Lcom/facebook/stickers/search/StickerSearchContainer$13;

    invoke-direct {v2, p0, v0}, Lcom/facebook/stickers/search/StickerSearchContainer$13;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->Q:Ljava/lang/Runnable;

    .line 1398005
    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->r:Landroid/os/Handler;

    iget-object v3, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->Q:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    const v6, -0x3f181593

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1398006
    goto :goto_0
.end method


# virtual methods
.method public getState()Landroid/os/Bundle;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1397986
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1397987
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1397988
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1397989
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1397990
    const-string v2, "query"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1397991
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x228645bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397980
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->i:LX/8jY;

    invoke-virtual {v1}, LX/8jY;->a()V

    .line 1397981
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->j:Lcom/facebook/stickers/search/StickerTagsLoader;

    invoke-virtual {v1}, LX/6Lb;->a()V

    .line 1397982
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->m:Lcom/facebook/stickers/search/StickerSearchLoader;

    invoke-virtual {v1}, LX/6Lb;->a()V

    .line 1397983
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->l:Lcom/facebook/stickers/search/TaggedStickersLoader;

    invoke-virtual {v1}, LX/6Lb;->a()V

    .line 1397984
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1397985
    const/16 v1, 0x2d

    const v2, 0x2f77d9e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1397928
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1397929
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->I:LX/7kc;

    if-eqz v0, :cond_0

    .line 1397930
    :goto_0
    return-void

    .line 1397931
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1397932
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-static {v1}, LX/8kW;->a(LX/4m4;)LX/7kb;

    move-result-object v1

    .line 1397933
    new-instance p1, LX/7kd;

    invoke-direct {p1, v0, v1}, LX/7kd;-><init>(Landroid/content/res/Resources;LX/7kb;)V

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getHeight()I

    move-result p2

    const p3, 0x7f0b025b

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, p2, v0

    const/4 p2, 0x0

    invoke-virtual {p1, v1, v0, p2}, LX/7kd;->a(IIZ)LX/7kc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->I:LX/7kc;

    .line 1397934
    const/4 p5, 0x2

    .line 1397935
    new-instance v0, LX/8mu;

    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->w:Landroid/content/Context;

    iget-boolean p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->U:Z

    iget-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->s:LX/03V;

    invoke-direct {v0, v1, p1, p2}, LX/8mu;-><init>(Landroid/content/Context;ZLX/03V;)V

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->G:LX/8mu;

    .line 1397936
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->G:LX/8mu;

    .line 1397937
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1397938
    invoke-virtual {v0, v1}, LX/8mu;->a(LX/0Px;)V

    .line 1397939
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->G:LX/8mu;

    new-instance v1, LX/8lN;

    invoke-direct {v1, p0}, LX/8lN;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    .line 1397940
    iput-object v1, v0, LX/8mu;->c:LX/8lN;

    .line 1397941
    new-instance v1, Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 1397942
    new-instance v0, LX/8lO;

    invoke-direct {v0, p0}, LX/8lO;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1397943
    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b07c9

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 1397944
    invoke-virtual {v1, p5}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 1397945
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setGravity(I)V

    .line 1397946
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->G:LX/8mu;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1397947
    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p2, 0x7f0b07c8

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 1397948
    invoke-virtual {v1, p1}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 1397949
    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p2, 0x7f0b07c3

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    iget-boolean v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->U:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b07c3

    :goto_1
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f0b07c4

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    invoke-virtual {v1, p2, v0, p3, p1}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 1397950
    const/high16 v0, 0x2000000

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setScrollBarStyle(I)V

    .line 1397951
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setClipToPadding(Z)V

    .line 1397952
    new-instance v0, LX/8lM;

    iget-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->H:LX/8lU;

    invoke-direct {v0, p2, p5, p1}, LX/8lM;-><init>(LX/8lU;II)V

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1397953
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1397954
    const/4 p3, 0x0

    .line 1397955
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->o:LX/8ms;

    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->w:Landroid/content/Context;

    const-string p1, ""

    iget-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->I:LX/7kc;

    invoke-virtual {v0, v1, p1, p2}, LX/8ms;->a(Landroid/content/Context;Ljava/lang/String;LX/7kc;)Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->F:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    .line 1397956
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->F:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    sget-object v1, Lcom/facebook/stickers/search/StickerSearchContainer;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1397957
    iput-object v1, v0, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 1397958
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->F:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    .line 1397959
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1397960
    invoke-virtual {v0, v1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(LX/0Px;)V

    .line 1397961
    new-instance v0, Landroid/widget/GridView;

    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->w:Landroid/content/Context;

    const/4 p1, 0x0

    const p2, 0x7f010524

    invoke-direct {v0, v1, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1397962
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->p:LX/8mq;

    iget-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    invoke-virtual {v1, v0, p1}, LX/8mq;->a(Landroid/widget/GridView;LX/4m4;)LX/8mp;

    move-result-object v1

    .line 1397963
    iget-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->I:LX/7kc;

    .line 1397964
    iget p2, p1, LX/7kc;->a:I

    move p1, p2

    .line 1397965
    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 1397966
    const/16 p1, 0x11

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setGravity(I)V

    .line 1397967
    iget-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->F:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1397968
    new-instance p1, LX/8lP;

    invoke-direct {p1, p0}, LX/8lP;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V

    .line 1397969
    iput-object p1, v1, LX/8mp;->e:LX/8l0;

    .line 1397970
    invoke-virtual {p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b07ca

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, p3, v1, p3, p3}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 1397971
    invoke-virtual {v0, p3}, Landroid/widget/GridView;->setClipToPadding(Z)V

    .line 1397972
    new-instance v1, LX/8lM;

    iget-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->H:LX/8lU;

    iget-object p2, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->I:LX/7kc;

    .line 1397973
    iget p4, p2, LX/7kc;->a:I

    move p2, p4

    .line 1397974
    invoke-direct {v1, p1, p2, p3}, LX/8lM;-><init>(LX/8lU;II)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1397975
    iget-object v1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->C:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1397976
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->j:Lcom/facebook/stickers/search/StickerTagsLoader;

    invoke-virtual {v0}, LX/6Lb;->a()V

    .line 1397977
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->j:Lcom/facebook/stickers/search/StickerTagsLoader;

    new-instance v1, LX/8ll;

    sget-object p1, LX/8m4;->FEATURED:LX/8m4;

    invoke-direct {v1, p1}, LX/8ll;-><init>(LX/8m4;)V

    invoke-virtual {v0, v1}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 1397978
    goto/16 :goto_0

    .line 1397979
    :cond_1
    const v0, 0x7f0b07ca

    goto/16 :goto_1
.end method

.method public setStickerSearchListener(LX/8l6;)V
    .locals 2

    .prologue
    .line 1397925
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/8lW;

    invoke-direct {v1, p0, p1}, LX/8lW;-><init>(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8l6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1397926
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->K:LX/8l6;

    .line 1397927
    return-void
.end method
