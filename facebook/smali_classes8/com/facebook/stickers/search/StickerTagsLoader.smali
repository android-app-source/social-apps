.class public Lcom/facebook/stickers/search/StickerTagsLoader;
.super LX/6Lb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "LX/8ll;",
        "LX/8lm;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/3dt;

.field private final c:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398175
    const-class v0, Lcom/facebook/stickers/search/StickerTagsLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/search/StickerTagsLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/3dt;LX/0aG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1398176
    invoke-direct {p0, p3}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1398177
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerTagsLoader;->b:LX/3dt;

    .line 1398178
    iput-object p2, p0, Lcom/facebook/stickers/search/StickerTagsLoader;->c:LX/0aG;

    .line 1398179
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/stickers/search/StickerTagsLoader;
    .locals 4

    .prologue
    .line 1398180
    new-instance v3, Lcom/facebook/stickers/search/StickerTagsLoader;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v0

    check-cast v0, LX/3dt;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/stickers/search/StickerTagsLoader;-><init>(LX/3dt;LX/0aG;Ljava/util/concurrent/Executor;)V

    .line 1398181
    move-object v0, v3

    .line 1398182
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 1398183
    check-cast p1, LX/8ll;

    .line 1398184
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1398185
    const-string v0, "fetchStickerTagsParam"

    new-instance v1, Lcom/facebook/stickers/service/FetchStickerTagsParams;

    sget-object v3, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    iget-object v4, p1, LX/8ll;->a:LX/8m4;

    invoke-direct {v1, v3, v4}, Lcom/facebook/stickers/service/FetchStickerTagsParams;-><init>(LX/0rS;LX/8m4;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1398186
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerTagsLoader;->c:LX/0aG;

    const-string v1, "fetch_sticker_tags"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/stickers/search/StickerTagsLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x577cedce

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1398187
    new-instance v1, LX/8lk;

    invoke-direct {v1, p0}, LX/8lk;-><init>(Lcom/facebook/stickers/search/StickerTagsLoader;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 2

    .prologue
    .line 1398188
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerTagsLoader;->b:LX/3dt;

    invoke-virtual {v0}, LX/3dt;->g()LX/0Px;

    move-result-object v0

    .line 1398189
    if-eqz v0, :cond_0

    new-instance v1, LX/8lm;

    invoke-direct {v1, v0}, LX/8lm;-><init>(LX/0Px;)V

    invoke-static {v1}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    goto :goto_0
.end method
