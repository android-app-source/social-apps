.class public Lcom/facebook/stickers/search/TaggedStickersLoader;
.super LX/6Lb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "LX/8lo;",
        "LX/8lp;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0aG;

.field private final c:LX/4m4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398202
    const-class v0, Lcom/facebook/stickers/search/TaggedStickersLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/search/TaggedStickersLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/4m4;LX/0aG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # LX/4m4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1398203
    invoke-direct {p0, p3}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1398204
    iput-object p1, p0, Lcom/facebook/stickers/search/TaggedStickersLoader;->c:LX/4m4;

    .line 1398205
    iput-object p2, p0, Lcom/facebook/stickers/search/TaggedStickersLoader;->b:LX/0aG;

    .line 1398206
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 1398207
    check-cast p1, LX/8lo;

    .line 1398208
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1398209
    const-string v0, "fetchTaggedStickerIdsParams"

    new-instance v1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;

    iget-object v3, p1, LX/8lo;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    sget-object v4, LX/8mB;->AVAILABLE_STICKERS:LX/8mB;

    iget-object v5, p0, Lcom/facebook/stickers/search/TaggedStickersLoader;->c:LX/4m4;

    invoke-direct {v1, v3, v4, v5}, Lcom/facebook/stickers/service/FetchTaggedStickersParams;-><init>(LX/0Px;LX/8mB;LX/4m4;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1398210
    iget-object v0, p0, Lcom/facebook/stickers/search/TaggedStickersLoader;->b:LX/0aG;

    const-string v1, "fetch_tagged_sticker_ids"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/stickers/search/TaggedStickersLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x4ab44f23

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1398211
    new-instance v1, LX/8ln;

    invoke-direct {v1, p0, p1}, LX/8ln;-><init>(Lcom/facebook/stickers/search/TaggedStickersLoader;LX/8lo;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 1

    .prologue
    .line 1398212
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    return-object v0
.end method
