.class public final Lcom/facebook/stickers/search/ExpandableFrameLayout$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8lL;


# direct methods
.method public constructor <init>(LX/8lL;)V
    .locals 0

    .prologue
    .line 1397659
    iput-object p1, p0, Lcom/facebook/stickers/search/ExpandableFrameLayout$1;->a:LX/8lL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1397660
    iget-object v0, p0, Lcom/facebook/stickers/search/ExpandableFrameLayout$1;->a:LX/8lL;

    iget-object v1, p0, Lcom/facebook/stickers/search/ExpandableFrameLayout$1;->a:LX/8lL;

    invoke-static {v1}, LX/8lL;->getInlineContainerPositionY(LX/8lL;)F

    move-result v1

    .line 1397661
    iput v1, v0, LX/8lL;->f:F

    .line 1397662
    iget-object v0, p0, Lcom/facebook/stickers/search/ExpandableFrameLayout$1;->a:LX/8lL;

    .line 1397663
    invoke-virtual {v0}, LX/8lL;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 1397664
    :goto_0
    return-void

    .line 1397665
    :cond_0
    iget-object v2, v0, LX/8lL;->b:LX/0wd;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1397666
    iget-object v2, v0, LX/8lL;->b:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-static {v0, v2}, LX/8lL;->setPopOutLayoutExpansion(LX/8lL;F)V

    .line 1397667
    invoke-static {v0}, LX/8lL;->getFullscreenHostView(LX/8lL;)Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, v0, LX/8lL;->a:LX/4nn;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1397668
    invoke-virtual {v0}, LX/8lL;->findFocus()Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, LX/8lL;->h:Landroid/view/View;

    .line 1397669
    iget-object v2, v0, LX/8lL;->a:LX/4nn;

    invoke-static {v0, v2}, LX/8lL;->a(LX/4nn;LX/4nn;)V

    .line 1397670
    invoke-virtual {v0}, LX/8lL;->requestLayout()V

    .line 1397671
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/8lL;->g:Z

    goto :goto_0
.end method
