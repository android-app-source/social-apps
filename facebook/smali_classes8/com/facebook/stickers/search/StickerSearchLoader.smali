.class public Lcom/facebook/stickers/search/StickerSearchLoader;
.super LX/6Lb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "LX/8li;",
        "LX/8lj;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0aG;

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398162
    const-class v0, Lcom/facebook/stickers/search/StickerSearchLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/search/StickerSearchLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1398157
    invoke-direct {p0, p2}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1398158
    iput-object p1, p0, Lcom/facebook/stickers/search/StickerSearchLoader;->b:LX/0aG;

    .line 1398159
    iput-object p2, p0, Lcom/facebook/stickers/search/StickerSearchLoader;->c:Ljava/util/concurrent/Executor;

    .line 1398160
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/stickers/search/StickerSearchLoader;
    .locals 1

    .prologue
    .line 1398161
    invoke-static {p0}, Lcom/facebook/stickers/search/StickerSearchLoader;->b(LX/0QB;)Lcom/facebook/stickers/search/StickerSearchLoader;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/stickers/search/StickerSearchLoader;
    .locals 3

    .prologue
    .line 1398149
    new-instance v2, Lcom/facebook/stickers/search/StickerSearchLoader;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-direct {v2, v0, v1}, Lcom/facebook/stickers/search/StickerSearchLoader;-><init>(LX/0aG;Ljava/util/concurrent/Executor;)V

    .line 1398150
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 1398152
    check-cast p1, LX/8li;

    .line 1398153
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1398154
    const-string v0, "stickerSearchParams"

    new-instance v1, Lcom/facebook/stickers/service/StickerSearchParams;

    iget-object v3, p1, LX/8li;->a:Ljava/lang/String;

    iget-object v4, p1, LX/8li;->b:LX/4m4;

    invoke-static {v4}, LX/8jc;->a(LX/4m4;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/facebook/stickers/service/StickerSearchParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1398155
    iget-object v0, p0, Lcom/facebook/stickers/search/StickerSearchLoader;->b:LX/0aG;

    const-string v1, "sticker_search"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/stickers/search/StickerSearchLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x5d450ba7

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1398156
    new-instance v1, LX/8lh;

    invoke-direct {v1, p0}, LX/8lh;-><init>(Lcom/facebook/stickers/search/StickerSearchLoader;)V

    iget-object v2, p0, Lcom/facebook/stickers/search/StickerSearchLoader;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 1

    .prologue
    .line 1398151
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    return-object v0
.end method
