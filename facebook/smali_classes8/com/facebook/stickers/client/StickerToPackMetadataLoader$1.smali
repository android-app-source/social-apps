.class public final Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Vj;


# instance fields
.field public final synthetic a:LX/8jS;

.field public final synthetic b:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/client/StickerToPackMetadataLoader;LX/8jS;)V
    .locals 0

    .prologue
    .line 1392482
    iput-object p1, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;->b:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iput-object p2, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;->a:LX/8jS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7

    .prologue
    .line 1392483
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v2, 0x0

    .line 1392484
    const-string v0, "fetchStickerAsync done"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    .line 1392485
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 1392486
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v3, v1

    .line 1392487
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1392488
    iget-object v5, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;->a:LX/8jS;

    iget-object v6, v6, LX/8jS;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1392489
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    aput-object v0, v3, v2

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;-><init>(Ljava/util/Collection;)V

    .line 1392490
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1392491
    const-string v0, "fetchStickerPacksByIdParams"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392492
    const-string v0, "fetchStickerPacksAsync started"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    .line 1392493
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;->b:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iget-object v0, v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->b:LX/0aG;

    const-string v1, "fetch_sticker_packs_by_id"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x1e98328e

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1392494
    return-object v0

    .line 1392495
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1392496
    :cond_1
    const-string v0, "fetchStickerAsync failed"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    .line 1392497
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Sticker cannot be fetched."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
