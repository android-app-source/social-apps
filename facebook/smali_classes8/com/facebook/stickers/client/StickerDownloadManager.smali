.class public Lcom/facebook/stickers/client/StickerDownloadManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:Lcom/facebook/stickers/client/StickerDownloadManager;


# instance fields
.field public final c:LX/0aG;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0Xl;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/1Mv;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1392467
    const-class v0, Lcom/facebook/stickers/client/StickerDownloadManager;

    const-string v1, "sticker_download_manager"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1392468
    const-class v0, Lcom/facebook/stickers/client/StickerDownloadManager;

    sput-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392435
    iput-object p1, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->c:LX/0aG;

    .line 1392436
    iput-object p2, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->d:Ljava/util/concurrent/ExecutorService;

    .line 1392437
    iput-object p3, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->e:LX/0Xl;

    .line 1392438
    iput-object p4, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1392439
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    .line 1392440
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->h:Ljava/util/HashMap;

    .line 1392441
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/stickers/client/StickerDownloadManager;
    .locals 7

    .prologue
    .line 1392469
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->i:Lcom/facebook/stickers/client/StickerDownloadManager;

    if-nez v0, :cond_1

    .line 1392470
    const-class v1, Lcom/facebook/stickers/client/StickerDownloadManager;

    monitor-enter v1

    .line 1392471
    :try_start_0
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->i:Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1392472
    if-eqz v2, :cond_0

    .line 1392473
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1392474
    new-instance p0, Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/stickers/client/StickerDownloadManager;-><init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1392475
    move-object v0, p0

    .line 1392476
    sput-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->i:Lcom/facebook/stickers/client/StickerDownloadManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1392477
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1392478
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1392479
    :cond_1
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->i:Lcom/facebook/stickers/client/StickerDownloadManager;

    return-object v0

    .line 1392480
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1392481
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/stickers/client/StickerDownloadManager;ZLcom/facebook/stickers/model/StickerPack;)V
    .locals 3

    .prologue
    .line 1392442
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2V4;->i:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1392443
    iget-object v0, p2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1392444
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392445
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392446
    if-eqz p1, :cond_0

    .line 1392447
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1392448
    :goto_0
    const-string v1, "stickerPack"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1392449
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->e:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1392450
    return-void

    .line 1392451
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_FAILURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 8

    .prologue
    .line 1392452
    invoke-virtual {p0, p1}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1392453
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    const-string v1, "Can\'t start download--download for this pack is already in progress."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1392454
    :goto_0
    return-void

    .line 1392455
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.orca.stickers.DOWNLOAD_QUEUED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1392456
    const-string v1, "stickerPack"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1392457
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->e:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1392458
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1392459
    const-string v2, "stickerPack"

    invoke-virtual {v4, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392460
    iget-object v2, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->c:LX/0aG;

    const-string v3, "add_sticker_pack"

    sget-object v5, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v6, Lcom/facebook/stickers/client/StickerDownloadManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v7, -0xdf7bbba

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 1392461
    new-instance v3, LX/8jO;

    invoke-direct {v3, p0, p1}, LX/8jO;-><init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V

    .line 1392462
    iget-object v4, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1392463
    iget-object v4, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    .line 1392464
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1392465
    invoke-static {v2, v3}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392466
    goto :goto_0
.end method

.method public final c(Lcom/facebook/stickers/model/StickerPack;)Z
    .locals 2

    .prologue
    .line 1392419
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    .line 1392420
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1392421
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 1392422
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 1392423
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    goto :goto_0

    .line 1392424
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1392425
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1392426
    return-void
.end method

.method public final d(Lcom/facebook/stickers/model/StickerPack;)I
    .locals 2

    .prologue
    .line 1392427
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->h:Ljava/util/HashMap;

    .line 1392428
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1392429
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1392430
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerDownloadManager;->h:Ljava/util/HashMap;

    .line 1392431
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1392432
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1392433
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
