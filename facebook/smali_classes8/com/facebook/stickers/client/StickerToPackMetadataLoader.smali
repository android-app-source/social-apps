.class public Lcom/facebook/stickers/client/StickerToPackMetadataLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/3Lc",
        "<",
        "LX/8jS;",
        "LX/8jT;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0aG;

.field public c:Ljava/util/concurrent/Executor;

.field public d:LX/3dt;

.field public e:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "LX/8jS;",
            "LX/8jT;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1Mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1392559
    const-class v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    sput-object v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3dt;LX/0aG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392555
    iput-object p1, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->d:LX/3dt;

    .line 1392556
    iput-object p2, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->b:LX/0aG;

    .line 1392557
    iput-object p3, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->c:Ljava/util/concurrent/Executor;

    .line 1392558
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/stickers/client/StickerToPackMetadataLoader;
    .locals 4

    .prologue
    .line 1392552
    new-instance v3, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v0

    check-cast v0, LX/3dt;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;-><init>(LX/3dt;LX/0aG;Ljava/util/concurrent/Executor;)V

    .line 1392553
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1392524
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->f:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1392525
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->f:LX/1Mv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1392526
    :cond_0
    return-void
.end method

.method public final a(LX/8jS;)V
    .locals 9

    .prologue
    .line 1392527
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    if-nez v0, :cond_0

    .line 1392528
    :goto_0
    return-void

    .line 1392529
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->d:LX/3dt;

    iget-object v1, p1, LX/8jS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 1392530
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1392531
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->d:LX/3dt;

    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/3dt;->b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v2

    .line 1392532
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->d:LX/3dt;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/3dt;->c(Ljava/lang/String;)LX/03R;

    move-result-object v0

    .line 1392533
    if-eqz v2, :cond_3

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1392534
    new-instance v1, LX/8jT;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/8m2;->DOWNLOADED:LX/8m2;

    :goto_1
    invoke-direct {v1, v2, v0}, LX/8jT;-><init>(Lcom/facebook/stickers/model/StickerPack;LX/8m2;)V

    move-object v0, v1

    .line 1392535
    :goto_2
    move-object v0, v0

    .line 1392536
    if-eqz v0, :cond_1

    .line 1392537
    iget-object v1, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    invoke-interface {v1, p1, v0}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1392538
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    .line 1392539
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v8

    .line 1392540
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1392541
    const-string v2, "fetchStickersParams"

    new-instance v3, Lcom/facebook/stickers/service/FetchStickersParams;

    iget-object v5, p1, LX/8jS;->a:Ljava/lang/String;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    sget-object v6, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v3, v5, v6}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392542
    const-string v2, "fetchStickerAsync started"

    invoke-static {v2}, LX/0PR;->b(Ljava/lang/String;)V

    .line 1392543
    iget-object v2, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->b:LX/0aG;

    const-string v3, "fetch_stickers"

    sget-object v5, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const v7, 0x23ec136d

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 1392544
    new-instance v3, Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;

    invoke-direct {v3, p0, p1}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader$1;-><init>(Lcom/facebook/stickers/client/StickerToPackMetadataLoader;LX/8jS;)V

    iget-object v4, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1392545
    new-instance v3, LX/8jR;

    invoke-direct {v3, p0, p1, v8}, LX/8jR;-><init>(Lcom/facebook/stickers/client/StickerToPackMetadataLoader;LX/8jS;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 1392546
    iget-object v4, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1392547
    invoke-static {v2, v3}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->f:LX/1Mv;

    .line 1392548
    move-object v1, v2

    .line 1392549
    invoke-interface {v0, p1, v1}, LX/3Mb;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto/16 :goto_0

    .line 1392550
    :cond_2
    sget-object v0, LX/8m2;->IN_STORE:LX/8m2;

    goto :goto_1

    .line 1392551
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
