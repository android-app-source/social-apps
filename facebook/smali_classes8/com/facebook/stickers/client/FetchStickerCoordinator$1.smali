.class public final Lcom/facebook/stickers/client/FetchStickerCoordinator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8jI;


# direct methods
.method public constructor <init>(LX/8jI;)V
    .locals 0

    .prologue
    .line 1392245
    iput-object p1, p0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;->a:LX/8jI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1392246
    iget-object v0, p0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;->a:LX/8jI;

    iget-object v1, v0, LX/8jI;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1392247
    :try_start_0
    iget-object v0, p0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;->a:LX/8jI;

    const/4 v2, 0x0

    .line 1392248
    iput-boolean v2, v0, LX/8jI;->f:Z

    .line 1392249
    iget-object v0, p0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;->a:LX/8jI;

    iget-object v0, v0, LX/8jI;->e:LX/0Xv;

    .line 1392250
    new-instance v2, LX/0Xq;

    invoke-direct {v2, v0}, LX/0Xq;-><init>(LX/0Xu;)V

    move-object v0, v2

    .line 1392251
    iget-object v2, p0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;->a:LX/8jI;

    iget-object v2, v2, LX/8jI;->e:LX/0Xv;

    invoke-interface {v2}, LX/0Xu;->g()V

    .line 1392252
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1392253
    iget-object v1, p0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;->a:LX/8jI;

    .line 1392254
    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1392255
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1392256
    const-string v4, "fetchStickersParams"

    new-instance v5, Lcom/facebook/stickers/service/FetchStickersParams;

    sget-object p0, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v5, v2, p0}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392257
    iget-object v4, v1, LX/8jI;->b:LX/0aG;

    const-string v5, "fetch_stickers"

    const p0, -0x1b80b419

    invoke-static {v4, v5, v3, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    .line 1392258
    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    move-object v2, v3

    .line 1392259
    new-instance v3, LX/8jH;

    invoke-direct {v3, v1, v0}, LX/8jH;-><init>(LX/8jI;LX/0Xv;)V

    iget-object v4, v1, LX/8jI;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1392260
    return-void

    .line 1392261
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
