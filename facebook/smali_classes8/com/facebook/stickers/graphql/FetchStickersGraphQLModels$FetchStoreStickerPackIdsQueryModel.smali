.class public final Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3420dfa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1393996
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1393995
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1393993
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1393994
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1393987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1393988
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1393989
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1393990
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1393991
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1393992
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1393997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1393998
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1393999
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    .line 1394000
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1394001
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;

    .line 1394002
    iput-object v0, v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->e:Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    .line 1394003
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1394004
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStickerStore"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1393985
    iget-object v0, p0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->e:Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    iput-object v0, p0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->e:Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    .line 1393986
    iget-object v0, p0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->e:Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1393982
    new-instance v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;

    invoke-direct {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;-><init>()V

    .line 1393983
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1393984
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1393981
    const v0, -0x634bdb7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1393980
    const v0, -0x6747e1ce

    return v0
.end method
