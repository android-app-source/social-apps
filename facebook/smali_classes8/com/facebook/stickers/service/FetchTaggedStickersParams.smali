.class public Lcom/facebook/stickers/service/FetchTaggedStickersParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchTaggedStickersParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/8mB;

.field public final c:LX/4m4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398432
    new-instance v0, LX/8mA;

    invoke-direct {v0}, LX/8mA;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/8mB;LX/4m4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8mB;",
            "LX/4m4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1398433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398434
    iput-object p1, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    .line 1398435
    iput-object p2, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->b:LX/8mB;

    .line 1398436
    iput-object p3, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->c:LX/4m4;

    .line 1398437
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1398438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398439
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1398440
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1398441
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    .line 1398442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8mB;->valueOf(Ljava/lang/String;)LX/8mB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->b:LX/8mB;

    .line 1398443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4m4;->valueOf(Ljava/lang/String;)LX/4m4;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->c:LX/4m4;

    .line 1398444
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1398445
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1398446
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1398447
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->b:LX/8mB;

    invoke-virtual {v0}, LX/8mB;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398448
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->c:LX/4m4;

    invoke-virtual {v0}, LX/4m4;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398449
    return-void
.end method
