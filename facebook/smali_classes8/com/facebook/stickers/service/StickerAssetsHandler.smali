.class public Lcom/facebook/stickers/service/StickerAssetsHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:LX/3dt;

.field public final d:LX/3dx;

.field private final e:Lcom/facebook/stickers/data/StickerAssetDownloader;

.field private final f:LX/0kb;

.field private final g:LX/2V5;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Uh;

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3eh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1398492
    const-class v0, Lcom/facebook/stickers/service/StickerAssetsHandler;

    sput-object v0, Lcom/facebook/stickers/service/StickerAssetsHandler;->a:Ljava/lang/Class;

    .line 1398493
    const-class v0, Lcom/facebook/stickers/service/StickerAssetsHandler;

    const-string v1, "sticker_store"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/service/StickerAssetsHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/3dt;LX/3dx;Lcom/facebook/stickers/data/StickerAssetDownloader;LX/0kb;LX/2V5;LX/0Uh;LX/0Or;)V
    .locals 1
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/data/CanSaveStickerAssetsToDisk;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3dt;",
            "Lcom/facebook/stickers/data/StickerDbStorage;",
            "Lcom/facebook/stickers/data/StickerAssetDownloader;",
            "LX/0kb;",
            "LX/2V5;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1398545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398546
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1398547
    iput-object v0, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->j:LX/0Ot;

    .line 1398548
    iput-object p1, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->c:LX/3dt;

    .line 1398549
    iput-object p2, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->d:LX/3dx;

    .line 1398550
    iput-object p3, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->e:Lcom/facebook/stickers/data/StickerAssetDownloader;

    .line 1398551
    iput-object p4, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->f:LX/0kb;

    .line 1398552
    iput-object p5, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->g:LX/2V5;

    .line 1398553
    iput-object p7, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->h:LX/0Or;

    .line 1398554
    iput-object p6, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->i:LX/0Uh;

    .line 1398555
    return-void
.end method

.method public static a(Lcom/facebook/stickers/service/StickerAssetsHandler;Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/common/callercontext/CallerContext;)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 1398539
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->e:Lcom/facebook/stickers/data/StickerAssetDownloader;

    .line 1398540
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1398541
    const-string v2, "thumbnail"

    sget-object v3, LX/3e1;->PREVIEW:LX/3e1;

    .line 1398542
    iget-object v4, p1, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v4, v4

    .line 1398543
    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/stickers/data/StickerAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Ljava/io/File;

    move-result-object v0

    .line 1398544
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/stickers/service/StickerAssetsHandler;LX/1qK;Lcom/facebook/stickers/service/FetchStickersResult;LX/1qH;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/stickers/service/FetchStickersResult;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1398512
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1398513
    const-string v1, "stickerPack"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 1398514
    iget-object v0, p2, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v3, v0

    .line 1398515
    const/4 v0, 0x0

    .line 1398516
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v4, v0

    move v1, v0

    :goto_0
    if-ge v4, v5, :cond_0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1398517
    invoke-static {v0}, LX/2V5;->b(Lcom/facebook/stickers/model/Sticker;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {v0}, LX/2V5;->a(Lcom/facebook/stickers/model/Sticker;)LX/03R;

    move-result-object v0

    sget-object v6, LX/03R;->NO:LX/03R;

    if-eq v0, v6, :cond_5

    .line 1398518
    add-int/lit8 v0, v1, 0x1

    .line 1398519
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 1398520
    :cond_0
    move v0, v1

    .line 1398521
    int-to-double v4, v0

    .line 1398522
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1398523
    invoke-static {}, LX/4m0;->newBuilder()LX/4m0;

    move-result-object v7

    move v1, v2

    .line 1398524
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1398525
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1398526
    invoke-virtual {v7, v0}, LX/4m0;->a(Lcom/facebook/stickers/model/Sticker;)LX/4m0;

    .line 1398527
    iget-object v8, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->i:LX/0Uh;

    const/16 v9, 0x290

    invoke-virtual {v8, v9, v2}, LX/0Uh;->a(IZ)Z

    move-result v8

    .line 1398528
    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->f:LX/0kb;

    invoke-virtual {v8}, LX/0kb;->v()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1398529
    :cond_1
    invoke-static {p0, v0, v7, p4}, Lcom/facebook/stickers/service/StickerAssetsHandler;->a(Lcom/facebook/stickers/service/StickerAssetsHandler;Lcom/facebook/stickers/model/Sticker;LX/4m0;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1398530
    :goto_3
    invoke-virtual {v7}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1398531
    invoke-virtual {v7}, LX/4m0;->b()V

    .line 1398532
    if-eqz p3, :cond_2

    .line 1398533
    add-int/lit8 v0, v1, 0x1

    int-to-double v8, v0

    div-double/2addr v8, v4

    .line 1398534
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-interface {p3, v0}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1398535
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1398536
    :cond_3
    invoke-static {p0, v0, v7, p4}, Lcom/facebook/stickers/service/StickerAssetsHandler;->b(Lcom/facebook/stickers/service/StickerAssetsHandler;Lcom/facebook/stickers/model/Sticker;LX/4m0;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_3

    .line 1398537
    :cond_4
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersResult;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickersResult;-><init>(Ljava/util/List;)V

    .line 1398538
    return-object v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/stickers/service/StickerAssetsHandler;Lcom/facebook/stickers/model/Sticker;LX/4m0;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 1398500
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1398501
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->e:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v3, LX/3e1;->PREVIEW:LX/3e1;

    iget-object v4, p1, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/stickers/data/StickerAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Ljava/io/File;

    move-result-object v0

    .line 1398502
    iget-object v1, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->d:LX/3dx;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v3, LX/3e1;->PREVIEW:LX/3e1;

    invoke-virtual {v1, v2, v3, v0}, LX/3dx;->a(Ljava/lang/String;LX/3e1;Ljava/io/File;)V

    .line 1398503
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1398504
    iput-object v0, p2, LX/4m0;->h:Landroid/net/Uri;

    .line 1398505
    :cond_0
    :goto_0
    return-void

    .line 1398506
    :cond_1
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1398507
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->e:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v3, LX/3e1;->STATIC:LX/3e1;

    iget-object v4, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/stickers/data/StickerAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Ljava/io/File;

    move-result-object v0

    .line 1398508
    iget-object v1, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->d:LX/3dx;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v3, LX/3e1;->STATIC:LX/3e1;

    invoke-virtual {v1, v2, v3, v0}, LX/3dx;->a(Ljava/lang/String;LX/3e1;Ljava/io/File;)V

    .line 1398509
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1398510
    iput-object v0, p2, LX/4m0;->d:Landroid/net/Uri;

    .line 1398511
    goto :goto_0
.end method

.method private static b(Lcom/facebook/stickers/service/StickerAssetsHandler;Lcom/facebook/stickers/model/Sticker;LX/4m0;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 1398494
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1398495
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->e:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v3, LX/3e1;->STATIC:LX/3e1;

    iget-object v4, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/stickers/data/StickerAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Ljava/io/File;

    move-result-object v0

    .line 1398496
    iget-object v1, p0, Lcom/facebook/stickers/service/StickerAssetsHandler;->d:LX/3dx;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v3, LX/3e1;->STATIC:LX/3e1;

    invoke-virtual {v1, v2, v3, v0}, LX/3dx;->a(Ljava/lang/String;LX/3e1;Ljava/io/File;)V

    .line 1398497
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1398498
    iput-object v0, p2, LX/4m0;->d:Landroid/net/Uri;

    .line 1398499
    :cond_0
    return-void
.end method
