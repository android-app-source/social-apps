.class public Lcom/facebook/stickers/service/SaveStickerAssetParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/SaveStickerAssetParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398476
    new-instance v0, LX/8mD;

    invoke-direct {v0}, LX/8mD;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1398477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398478
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->a:Ljava/lang/String;

    .line 1398479
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->b:Ljava/lang/String;

    .line 1398480
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->c:Landroid/net/Uri;

    .line 1398481
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1398482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398483
    iput-object p1, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->a:Ljava/lang/String;

    .line 1398484
    iput-object p2, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->b:Ljava/lang/String;

    .line 1398485
    iput-object p3, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->c:Landroid/net/Uri;

    .line 1398486
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1398487
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1398488
    iget-object v0, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398489
    iget-object v0, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398490
    iget-object v0, p0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1398491
    return-void
.end method
