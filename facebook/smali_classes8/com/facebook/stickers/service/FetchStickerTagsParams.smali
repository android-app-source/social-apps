.class public Lcom/facebook/stickers/service/FetchStickerTagsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerTagsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0rS;

.field public final b:LX/8m4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398380
    new-instance v0, LX/8m3;

    invoke-direct {v0}, LX/8m3;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0rS;LX/8m4;)V
    .locals 0

    .prologue
    .line 1398381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398382
    iput-object p1, p0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->a:LX/0rS;

    .line 1398383
    iput-object p2, p0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->b:LX/8m4;

    .line 1398384
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1398385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->a:LX/0rS;

    .line 1398387
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8m4;->valueOf(Ljava/lang/String;)LX/8m4;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->b:LX/8m4;

    .line 1398388
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1398389
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1398390
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398391
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->b:LX/8m4;

    invoke-virtual {v0}, LX/8m4;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398392
    return-void
.end method
