.class public Lcom/facebook/stickers/service/FetchStickerPacksApiParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerPacksApiParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398345
    new-instance v0, LX/8ly;

    invoke-direct {v0}, LX/8ly;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1398339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398340
    const-class v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    .line 1398341
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    .line 1398342
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    .line 1398343
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    .line 1398344
    return-void
.end method

.method private constructor <init>(Lcom/facebook/stickers/service/FetchStickerPacksParams;ILjava/lang/String;J)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1398333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398334
    iput-object p1, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    .line 1398335
    iput p2, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    .line 1398336
    iput-object p3, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    .line 1398337
    iput-wide p4, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    .line 1398338
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/stickers/service/FetchStickerPacksParams;ILjava/lang/String;JB)V
    .locals 0

    .prologue
    .line 1398332
    invoke-direct/range {p0 .. p5}, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;-><init>(Lcom/facebook/stickers/service/FetchStickerPacksParams;ILjava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1398331
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1398327
    instance-of v1, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    if-nez v1, :cond_1

    .line 1398328
    :cond_0
    :goto_0
    return v0

    .line 1398329
    :cond_1
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    .line 1398330
    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/service/FetchStickerPacksParams;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    iget v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1398319
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    invoke-virtual {v0}, Lcom/facebook/stickers/service/FetchStickerPacksParams;->hashCode()I

    move-result v0

    .line 1398320
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    add-int/2addr v0, v2

    .line 1398321
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1398322
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    iget-wide v4, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1398323
    return v0

    :cond_1
    move v0, v1

    .line 1398324
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1398325
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1398326
    return-void
.end method
