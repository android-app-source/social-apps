.class public Lcom/facebook/stickers/service/FetchStickerPackIdsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerPackIdsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/3do;

.field public b:J

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398272
    new-instance v0, LX/8lv;

    invoke-direct {v0}, LX/8lv;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1398273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1398274
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3do;->valueOf(Ljava/lang/String;)LX/3do;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    .line 1398275
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    .line 1398276
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->c:Z

    .line 1398277
    return-void

    .line 1398278
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/8lw;
    .locals 1

    .prologue
    .line 1398279
    new-instance v0, LX/8lw;

    invoke-direct {v0}, LX/8lw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1398280
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1398281
    instance-of v1, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;

    if-nez v1, :cond_1

    .line 1398282
    :cond_0
    :goto_0
    return v0

    .line 1398283
    :cond_1
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;

    .line 1398284
    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->c:Z

    iget-boolean v2, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->c:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1398285
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    invoke-virtual {v0}, LX/3do;->hashCode()I

    move-result v0

    .line 1398286
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1398287
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->c:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 1398288
    return v0

    :cond_1
    move v0, v1

    .line 1398289
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1398290
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    invoke-virtual {v0}, LX/3do;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1398291
    iget-wide v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1398292
    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1398293
    return-void

    .line 1398294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
