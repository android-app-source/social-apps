.class public final Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5788ef1d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1532747
    const-class v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1532746
    const-class v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1532744
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1532745
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532742
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->e:Ljava/lang/String;

    .line 1532743
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532740
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->f:Ljava/lang/String;

    .line 1532741
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1532732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1532733
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1532734
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1532735
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1532736
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1532737
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1532738
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1532739
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1532729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1532730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1532731
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1532728
    new-instance v0, LX/9lL;

    invoke-direct {v0, p1}, LX/9lL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532719
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1532726
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1532727
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1532725
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1532722
    new-instance v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;-><init>()V

    .line 1532723
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1532724
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1532721
    const v0, -0x619d7901

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1532720
    const v0, -0x78fb05b

    return v0
.end method
