.class public final Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5c4c8c31
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1532783
    const-class v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1532761
    const-class v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1532784
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1532785
    return-void
.end method

.method private a()Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532775
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->e:Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->e:Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    .line 1532776
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->e:Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1532777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1532778
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->a()Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1532779
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1532780
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1532781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1532782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1532767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1532768
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->a()Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1532769
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->a()Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    .line 1532770
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->a()Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1532771
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;

    .line 1532772
    iput-object v0, v1, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;->e:Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel$FeedbackModel;

    .line 1532773
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1532774
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1532764
    new-instance v0, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/presence/protocol/FeedbackStopTypingMutationsModels$FeedbackStopTypingCoreMutationFieldsModel;-><init>()V

    .line 1532765
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1532766
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1532763
    const v0, 0x487cad82

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1532762
    const v0, 0x1565008f

    return v0
.end method
