.class public final Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5788ef1d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1532591
    const-class v0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1532590
    const-class v0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1532588
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1532589
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532586
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->e:Ljava/lang/String;

    .line 1532587
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532584
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->f:Ljava/lang/String;

    .line 1532585
    iget-object v0, p0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1532576
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1532577
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1532578
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1532579
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1532580
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1532581
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1532582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1532583
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1532573
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1532574
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1532575
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1532572
    new-instance v0, LX/9lF;

    invoke-direct {v0, p1}, LX/9lF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1532563
    invoke-direct {p0}, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1532570
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1532571
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1532569
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1532566
    new-instance v0, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/presence/protocol/FeedbackStartTypingMutationsModels$FeedbackStartTypingCoreMutationFieldsModel$FeedbackModel;-><init>()V

    .line 1532567
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1532568
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1532565
    const v0, -0x418daa14

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1532564
    const v0, -0x78fb05b

    return v0
.end method
