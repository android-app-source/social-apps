.class public Lcom/facebook/presence/PresenceItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/presence/PresenceItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/UserKey;

.field public final b:Z

.field public final c:J

.field public final d:I

.field public final e:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1532472
    new-instance v0, LX/9l6;

    invoke-direct {v0}, LX/9l6;-><init>()V

    sput-object v0, Lcom/facebook/presence/PresenceItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1532465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532466
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/presence/PresenceItem;->a:Lcom/facebook/user/model/UserKey;

    .line 1532467
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/presence/PresenceItem;->b:Z

    .line 1532468
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/presence/PresenceItem;->c:J

    .line 1532469
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/presence/PresenceItem;->d:I

    .line 1532470
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/presence/PresenceItem;->e:Ljava/lang/Long;

    .line 1532471
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/UserKey;ZJILjava/lang/Long;)V
    .locals 1
    .param p6    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532451
    iput-object p1, p0, Lcom/facebook/presence/PresenceItem;->a:Lcom/facebook/user/model/UserKey;

    .line 1532452
    iput-boolean p2, p0, Lcom/facebook/presence/PresenceItem;->b:Z

    .line 1532453
    iput-wide p3, p0, Lcom/facebook/presence/PresenceItem;->c:J

    .line 1532454
    iput p5, p0, Lcom/facebook/presence/PresenceItem;->d:I

    .line 1532455
    iput-object p6, p0, Lcom/facebook/presence/PresenceItem;->e:Ljava/lang/Long;

    .line 1532456
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1532464
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1532463
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "user"

    iget-object v2, p0, Lcom/facebook/presence/PresenceItem;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "active"

    iget-boolean v2, p0, Lcom/facebook/presence/PresenceItem;->b:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "last active"

    iget-wide v2, p0, Lcom/facebook/presence/PresenceItem;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "active client bits"

    iget v2, p0, Lcom/facebook/presence/PresenceItem;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "voip capability bits"

    iget-object v2, p0, Lcom/facebook/presence/PresenceItem;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1532457
    iget-object v0, p0, Lcom/facebook/presence/PresenceItem;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1532458
    iget-boolean v0, p0, Lcom/facebook/presence/PresenceItem;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1532459
    iget-wide v0, p0, Lcom/facebook/presence/PresenceItem;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1532460
    iget v0, p0, Lcom/facebook/presence/PresenceItem;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1532461
    iget-object v0, p0, Lcom/facebook/presence/PresenceItem;->e:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1532462
    return-void
.end method
