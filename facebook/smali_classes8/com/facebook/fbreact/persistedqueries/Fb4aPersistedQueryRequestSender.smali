.class public Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/990;

.field public final c:LX/11H;

.field public final d:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1446612
    const-class v0, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/990;LX/11H;LX/0TD;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1446608
    iput-object p1, p0, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;->b:LX/990;

    .line 1446609
    iput-object p2, p0, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;->c:LX/11H;

    .line 1446610
    iput-object p3, p0, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;->d:LX/0TD;

    .line 1446611
    return-void
.end method
