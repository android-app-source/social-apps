.class public Lcom/facebook/fbreact/fragment/ReactActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f1;
.implements LX/2lD;


# instance fields
.field public p:Lcom/facebook/fbreact/fragment/FbReactFragment;

.field private q:Ljava/lang/String;

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/9n2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1446502
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1446503
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->q:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1446499
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->q:Ljava/lang/String;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446500
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a()Ljava/lang/String;

    move-result-object v0

    .line 1446501
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1446495
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 1446496
    instance-of v0, p1, Lcom/facebook/fbreact/fragment/FbReactFragment;

    if-eqz v0, :cond_0

    .line 1446497
    check-cast p1, Lcom/facebook/fbreact/fragment/FbReactFragment;

    iput-object p1, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    .line 1446498
    :cond_0
    return-void
.end method

.method public final a([Ljava/lang/String;ILX/9n2;)V
    .locals 0

    .prologue
    .line 1446492
    iput-object p3, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->s:LX/9n2;

    .line 1446493
    invoke-virtual {p0, p1, p2}, Lcom/facebook/fbreact/fragment/ReactActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 1446494
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1446504
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->r:Ljava/util/Map;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1446461
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1446462
    const v0, 0x7f0310fd

    invoke-virtual {p0, v0}, Lcom/facebook/fbreact/fragment/ReactActivity;->setContentView(I)V

    .line 1446463
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/ReactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1446464
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    if-nez v0, :cond_1

    .line 1446465
    const-string v0, "uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1446466
    const-string v0, "route_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1446467
    const-string v0, "module_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1446468
    const-string v0, "init_props"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1446469
    if-nez v0, :cond_0

    .line 1446470
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1446471
    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbreact/fragment/ReactActivity;->d(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 1446472
    const-string v5, "requested_orientation"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 1446473
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v6

    .line 1446474
    iput-object v0, v6, LX/98r;->f:Landroid/os/Bundle;

    .line 1446475
    move-object v0, v6

    .line 1446476
    iput v5, v0, LX/98r;->h:I

    .line 1446477
    move-object v0, v0

    .line 1446478
    if-eqz v2, :cond_3

    .line 1446479
    iput-object v2, v0, LX/98r;->a:Ljava/lang/String;

    .line 1446480
    move-object v2, v0

    .line 1446481
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 1446482
    :goto_0
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1446483
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    .line 1446484
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d2862

    iget-object v3, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1446485
    :cond_1
    if-eqz v1, :cond_2

    .line 1446486
    const-string v0, "analytics_tag"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1446487
    iput-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->q:Ljava/lang/String;

    .line 1446488
    const-string v0, "analytics_extra_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->r:Ljava/util/Map;

    .line 1446489
    :cond_2
    return-void

    .line 1446490
    :cond_3
    iput-object v4, v0, LX/98r;->c:Ljava/lang/String;

    .line 1446491
    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    .prologue
    .line 1446460
    return-object p1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1446456
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1446457
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    if-eqz v0, :cond_0

    .line 1446458
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1446459
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1446453
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446454
    :goto_0
    return-void

    .line 1446455
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1446450
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->p:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446451
    const/4 v0, 0x1

    .line 1446452
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 1446447
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->s:LX/9n2;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->s:LX/9n2;

    invoke-interface {v0, p1, p3}, LX/9n2;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/ReactActivity;->s:LX/9n2;

    .line 1446449
    :cond_0
    return-void
.end method
