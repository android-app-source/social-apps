.class public Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public m:LX/1AM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Landroid/view/View;

.field private o:Lcom/facebook/fbreact/fragment/FbReactFragment;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/8qU;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/DialogInterface$OnDismissListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1446553
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;

    invoke-static {p0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object p0

    check-cast p0, LX/1AM;

    iput-object p0, p1, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->m:LX/1AM;

    return-void
.end method

.method public static b(Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;)V
    .locals 2

    .prologue
    .line 1446548
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446549
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1446550
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1446551
    invoke-static {v0, v1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1446552
    :cond_0
    return-void
.end method

.method private z()Lcom/facebook/fbreact/fragment/FbReactFragment;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1446547
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0807

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbreact/fragment/FbReactFragment;

    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1446511
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->z()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->z()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446512
    const/4 v0, 0x1

    .line 1446513
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    move-result v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1446544
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->o:Lcom/facebook/fbreact/fragment/FbReactFragment;

    if-eqz v0, :cond_0

    .line 1446545
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->o:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a()Ljava/lang/String;

    move-result-object v0

    .line 1446546
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 1446540
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->b(Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;)V

    .line 1446541
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    .line 1446542
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->m:LX/1AM;

    new-instance v1, LX/1ZJ;

    invoke-direct {v1}, LX/1ZJ;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1446543
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1446539
    const v0, 0x7f0310ff

    return v0
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 1446536
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->p:LX/8qU;

    if-nez v0, :cond_0

    .line 1446537
    new-instance v0, LX/98u;

    invoke-direct {v0, p0}, LX/98u;-><init>(Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->p:LX/8qU;

    .line 1446538
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->p:LX/8qU;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7ef0ba99    # 1.5999179E38f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1446530
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1446531
    const-class v1, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;

    invoke-static {v1, p0}, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1446532
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->o:Lcom/facebook/fbreact/fragment/FbReactFragment;

    if-eqz v1, :cond_0

    .line 1446533
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->b(Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;)V

    .line 1446534
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0807

    iget-object v3, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->o:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1446535
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x6040fb27

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x218531bd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1446526
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 1446527
    const v2, 0x7f0d0807

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->n:Landroid/view/View;

    .line 1446528
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->n:Landroid/view/View;

    new-instance v3, LX/98t;

    invoke-direct {v3, p0}, LX/98t;-><init>(Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1446529
    const/16 v2, 0x2b

    const v3, -0x422cce80

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x21a58e60

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1446523
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onDestroyView()V

    .line 1446524
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->n:Landroid/view/View;

    .line 1446525
    const/16 v1, 0x2b

    const v2, -0x8f4453f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1446517
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1446518
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->q:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1446519
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/DialogInterface$OnDismissListener;

    .line 1446520
    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    goto :goto_0

    .line 1446521
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1446522
    :cond_1
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x465a281b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1446514
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onResume()V

    .line 1446515
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/ReactNativePopoverFragment;->m:LX/1AM;

    new-instance v2, LX/1ZH;

    invoke-direct {v2}, LX/1ZH;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1446516
    const/16 v1, 0x2b

    const v2, 0x37181850

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
