.class public final Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1622444
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel;

    new-instance v1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1622445
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1622446
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1622447
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1622448
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1622449
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1622450
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1622451
    if-eqz v2, :cond_1

    .line 1622452
    const-string p0, "topics"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1622453
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1622454
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 1622455
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1}, LX/A5x;->a(LX/15i;ILX/0nX;)V

    .line 1622456
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1622457
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1622458
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1622459
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1622460
    check-cast p1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$Serializer;->a(Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
