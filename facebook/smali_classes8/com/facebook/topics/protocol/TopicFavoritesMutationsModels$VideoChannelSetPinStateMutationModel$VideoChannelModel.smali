.class public final Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7e4dc0a9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1622788
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1622787
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1622785
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1622786
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1622779
    iput-boolean p1, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->g:Z

    .line 1622780
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1622781
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1622782
    if-eqz v0, :cond_0

    .line 1622783
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1622784
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1622776
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1622777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1622778
    :cond_0
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1622774
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    .line 1622775
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1622772
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1622773
    iget-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->g:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1622763
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622764
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1622765
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1622766
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1622767
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1622768
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1622769
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1622770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622771
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1622741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622743
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1622762
    new-instance v0, LX/A5v;

    invoke-direct {v0, p1}, LX/A5v;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1622761
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1622758
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1622759
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->g:Z

    .line 1622760
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1622752
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622753
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1622754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1622755
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1622756
    :goto_0
    return-void

    .line 1622757
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1622749
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622750
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;->a(Z)V

    .line 1622751
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1622746
    new-instance v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;-><init>()V

    .line 1622747
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1622748
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1622745
    const v0, -0x78bc4056

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1622744
    const v0, 0x2d116428

    return v0
.end method
