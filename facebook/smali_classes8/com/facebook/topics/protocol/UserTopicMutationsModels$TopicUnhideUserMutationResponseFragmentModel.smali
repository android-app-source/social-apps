.class public final Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1624293
    const-class v0, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1624292
    const-class v0, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1624290
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1624291
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1624285
    iget-object v0, p0, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;->e:Ljava/lang/String;

    .line 1624286
    iget-object v0, p0, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1624294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1624295
    invoke-direct {p0}, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1624296
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1624297
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1624298
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1624299
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1624287
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1624288
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1624289
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1624282
    new-instance v0, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/UserTopicMutationsModels$TopicUnhideUserMutationResponseFragmentModel;-><init>()V

    .line 1624283
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1624284
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1624281
    const v0, 0x64d2e9cc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1624280
    const v0, 0x196b4d18

    return v0
.end method
