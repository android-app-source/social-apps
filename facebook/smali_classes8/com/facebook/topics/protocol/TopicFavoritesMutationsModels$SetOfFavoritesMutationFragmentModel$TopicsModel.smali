.class public final Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x11735ec5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1622525
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1622524
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1622522
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1622523
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1622516
    iput-boolean p1, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->f:Z

    .line 1622517
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1622518
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1622519
    if-eqz v0, :cond_0

    .line 1622520
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1622521
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1622514
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->e:Ljava/lang/String;

    .line 1622515
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 1622512
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1622513
    iget-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->f:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1622505
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622506
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1622507
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1622508
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1622509
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1622510
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622511
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1622483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622484
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622485
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1622504
    new-instance v0, LX/A5t;

    invoke-direct {v0, p1}, LX/A5t;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1622503
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1622500
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1622501
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->f:Z

    .line 1622502
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1622494
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622495
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1622496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1622497
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1622498
    :goto_0
    return-void

    .line 1622499
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1622491
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622492
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;->a(Z)V

    .line 1622493
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1622488
    new-instance v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;-><init>()V

    .line 1622489
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1622490
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1622487
    const v0, 0x7b01f3a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1622486
    const v0, 0xd0d7ab1

    return v0
.end method
