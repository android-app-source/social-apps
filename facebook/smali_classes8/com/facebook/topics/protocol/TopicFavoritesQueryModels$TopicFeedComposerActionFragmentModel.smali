.class public final Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3192e458
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1623014
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1623017
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1623015
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1623016
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionIcon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1623012
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623013
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623010
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    .line 1623011
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623018
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->g:Ljava/lang/String;

    .line 1623019
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1623000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1623001
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x1bb291f2

    invoke-static {v1, v0, v2}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1623002
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLTopicFeedComposerType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1623003
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1623004
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1623005
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1623006
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1623007
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1623008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1623009
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1622990
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622991
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1622992
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1bb291f2

    invoke-static {v2, v0, v3}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1622993
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1622994
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;

    .line 1622995
    iput v3, v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->e:I

    .line 1622996
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622997
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1622998
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1622999
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1622987
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1622988
    const/4 v0, 0x0

    const v1, -0x1bb291f2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;->e:I

    .line 1622989
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1622982
    new-instance v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;-><init>()V

    .line 1622983
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1622984
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1622986
    const v0, -0x24fda946

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1622985
    const v0, -0x234e93dd

    return v0
.end method
