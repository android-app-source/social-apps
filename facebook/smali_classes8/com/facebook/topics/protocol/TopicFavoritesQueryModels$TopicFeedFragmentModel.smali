.class public final Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x74e28720
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:I

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1623256
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1623247
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1623248
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1623249
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1623250
    iput-boolean p1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l:Z

    .line 1623251
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1623252
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1623253
    if-eqz v0, :cond_0

    .line 1623254
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1623255
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1623257
    iput-boolean p1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->q:Z

    .line 1623258
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1623259
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1623260
    if-eqz v0, :cond_0

    .line 1623261
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1623262
    :cond_0
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getComposerActions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1623263
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedComposerActionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->e:Ljava/util/List;

    .line 1623264
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623265
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->f:Ljava/lang/String;

    .line 1623266
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623267
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    .line 1623268
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    return-object v0
.end method

.method private m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623269
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    .line 1623270
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    return-object v0
.end method

.method private n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623271
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    .line 1623272
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623243
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->k:Ljava/lang/String;

    .line 1623244
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 1623245
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623246
    iget-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l:Z

    return v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623162
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n:Ljava/lang/String;

    .line 1623163
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method private r()Z
    .locals 2

    .prologue
    .line 1623164
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623165
    iget-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->q:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1623166
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1623167
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1623168
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1623169
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1623170
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1623171
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1623172
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1623173
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1623174
    const/16 v7, 0xd

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1623175
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1623176
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1623177
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623178
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1623179
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1623180
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1623181
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1623182
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623183
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->m:I

    invoke-virtual {p1, v0, v1, v8}, LX/186;->a(III)V

    .line 1623184
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1623185
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->o:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623186
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->p:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623187
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->q:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623188
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1623189
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1623190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1623191
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1623192
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1623193
    if-eqz v1, :cond_4

    .line 1623194
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;

    .line 1623195
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1623196
    :goto_0
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1623197
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    .line 1623198
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1623199
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;

    .line 1623200
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$DisabledFavoriteIconModel;

    .line 1623201
    :cond_0
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1623202
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    .line 1623203
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1623204
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;

    .line 1623205
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$EnabledFavoriteIconModel;

    .line 1623206
    :cond_1
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1623207
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    .line 1623208
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1623209
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;

    .line 1623210
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel$HeaderImageModel;

    .line 1623211
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1623212
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1623213
    new-instance v0, LX/A62;

    invoke-direct {v0, p1}, LX/A62;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623214
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1623215
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1623216
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->g:Z

    .line 1623217
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->l:Z

    .line 1623218
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->m:I

    .line 1623219
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->o:Z

    .line 1623220
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->p:Z

    .line 1623221
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->q:Z

    .line 1623222
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1623223
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623224
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->p()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1623225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1623226
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    .line 1623227
    :goto_0
    return-void

    .line 1623228
    :cond_0
    const-string v0, "show_audience_header"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1623229
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1623230
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1623231
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1623232
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1623238
    const-string v0, "is_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1623239
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->a(Z)V

    .line 1623240
    :cond_0
    :goto_0
    return-void

    .line 1623241
    :cond_1
    const-string v0, "show_audience_header"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623242
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1623233
    new-instance v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$TopicFeedFragmentModel;-><init>()V

    .line 1623234
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1623235
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1623236
    const v0, -0x493627c5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1623237
    const v0, 0xd0d7ab1

    return v0
.end method
