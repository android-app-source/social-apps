.class public final Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1622472
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;

    new-instance v1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1622473
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1622471
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1622461
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1622462
    invoke-static {p1, v0}, LX/A5x;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1622463
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1622464
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1622465
    new-instance v1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;

    invoke-direct {v1}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$SetOfFavoritesMutationFragmentModel$TopicsModel;-><init>()V

    .line 1622466
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1622467
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1622468
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1622469
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1622470
    :cond_0
    return-object v1
.end method
