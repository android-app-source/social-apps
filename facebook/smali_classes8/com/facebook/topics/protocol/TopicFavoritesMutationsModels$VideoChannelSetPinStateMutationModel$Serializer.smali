.class public final Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1622706
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;

    new-instance v1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1622707
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1622708
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1622709
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1622710
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1622711
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1622712
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1622713
    if-eqz v2, :cond_0

    .line 1622714
    const-string p0, "video_channel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1622715
    invoke-static {v1, v2, p1}, LX/A5z;->a(LX/15i;ILX/0nX;)V

    .line 1622716
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1622717
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1622718
    check-cast p1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Serializer;->a(Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
