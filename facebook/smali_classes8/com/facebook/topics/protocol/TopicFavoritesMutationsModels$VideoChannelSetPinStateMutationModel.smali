.class public final Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xee63bbd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1622807
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1622789
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1622805
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1622806
    return-void
.end method

.method private a()Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1622803
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->e:Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->e:Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    .line 1622804
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->e:Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1622808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622809
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->a()Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1622810
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1622811
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1622812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622813
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1622795
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1622796
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->a()Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1622797
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->a()Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    .line 1622798
    invoke-direct {p0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->a()Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1622799
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;

    .line 1622800
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;->e:Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel$VideoChannelModel;

    .line 1622801
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1622802
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1622792
    new-instance v0, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/TopicFavoritesMutationsModels$VideoChannelSetPinStateMutationModel;-><init>()V

    .line 1622793
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1622794
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1622791
    const v0, 0x164fcfcb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1622790
    const v0, 0x6ac99df7

    return v0
.end method
