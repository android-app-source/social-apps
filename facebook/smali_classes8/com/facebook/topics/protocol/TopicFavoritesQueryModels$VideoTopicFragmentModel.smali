.class public final Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/9rj;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x64c46b9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1623607
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1623608
    const-class v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1623609
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1623610
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1623611
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1623612
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1623613
    return-void
.end method

.method public static a(LX/9rj;)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;
    .locals 2

    .prologue
    .line 1623614
    if-nez p0, :cond_0

    .line 1623615
    const/4 p0, 0x0

    .line 1623616
    :goto_0
    return-object p0

    .line 1623617
    :cond_0
    instance-of v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    if-eqz v0, :cond_1

    .line 1623618
    check-cast p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    goto :goto_0

    .line 1623619
    :cond_1
    new-instance v0, LX/A63;

    invoke-direct {v0}, LX/A63;-><init>()V

    .line 1623620
    invoke-interface {p0}, LX/9rj;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/A63;->a:Ljava/lang/String;

    .line 1623621
    invoke-interface {p0}, LX/9rj;->c()I

    move-result v1

    iput v1, v0, LX/A63;->b:I

    .line 1623622
    invoke-interface {p0}, LX/9rj;->d()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;->a(Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v1

    iput-object v1, v0, LX/A63;->c:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 1623623
    invoke-interface {p0}, LX/9rj;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/A63;->d:Z

    .line 1623624
    invoke-interface {p0}, LX/9rj;->gN_()Z

    move-result v1

    iput-boolean v1, v0, LX/A63;->e:Z

    .line 1623625
    invoke-interface {p0}, LX/9rj;->gO_()I

    move-result v1

    iput v1, v0, LX/A63;->f:I

    .line 1623626
    invoke-interface {p0}, LX/9rj;->j()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;->a(Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v1

    iput-object v1, v0, LX/A63;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 1623627
    invoke-interface {p0}, LX/9rj;->k()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;->a(Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v1

    iput-object v1, v0, LX/A63;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 1623628
    invoke-virtual {v0}, LX/A63;->a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1623629
    iput-boolean p1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->i:Z

    .line 1623630
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1623631
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1623632
    if-eqz v0, :cond_0

    .line 1623633
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1623634
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1623653
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1623654
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1623655
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1623656
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1623657
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1623658
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1623659
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1623660
    const/4 v0, 0x1

    iget v4, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->f:I

    invoke-virtual {p1, v0, v4, v5}, LX/186;->a(III)V

    .line 1623661
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1623662
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623663
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1623664
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->j:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 1623665
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1623666
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1623667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1623668
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1623635
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1623636
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1623637
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 1623638
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1623639
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    .line 1623640
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 1623641
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1623642
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 1623643
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1623644
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    .line 1623645
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->k:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 1623646
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1623647
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 1623648
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1623649
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    .line 1623650
    iput-object v0, v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 1623651
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1623652
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1623681
    new-instance v0, LX/A64;

    invoke-direct {v0, p1}, LX/A64;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623682
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1623675
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1623676
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->f:I

    .line 1623677
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->h:Z

    .line 1623678
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->i:Z

    .line 1623679
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->j:I

    .line 1623680
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1623669
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623670
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->gN_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1623671
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1623672
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1623673
    :goto_0
    return-void

    .line 1623674
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1623601
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623602
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->a(Z)V

    .line 1623603
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1623604
    new-instance v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    invoke-direct {v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;-><init>()V

    .line 1623605
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1623606
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623580
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->e:Ljava/lang/String;

    .line 1623581
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1623582
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623583
    iget v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->f:I

    return v0
.end method

.method public final synthetic d()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623584
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1623585
    const v0, 0x293edafb

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1623586
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623587
    iget-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1623588
    const v0, 0xd0d7ab1

    return v0
.end method

.method public final gN_()Z
    .locals 2

    .prologue
    .line 1623589
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623590
    iget-boolean v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->i:Z

    return v0
.end method

.method public final gO_()I
    .locals 2

    .prologue
    .line 1623591
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1623592
    iget v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->j:I

    return v0
.end method

.method public final synthetic j()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623593
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623594
    invoke-virtual {p0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623595
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 1623596
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623597
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->k:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->k:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 1623598
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->k:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1623599
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    iput-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 1623600
    iget-object v0, p0, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    return-object v0
.end method
