.class public final Lcom/facebook/analytics/camerausage/CameraLeakListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/ADY;


# direct methods
.method public constructor <init>(LX/ADY;I)V
    .locals 0

    .prologue
    .line 1644896
    iput-object p1, p0, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;->b:LX/ADY;

    iput p2, p0, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1644897
    iget-object v0, p0, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;->b:LX/ADY;

    iget-object v0, v0, LX/ADY;->a:Landroid/util/SparseArray;

    iget v1, p0, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;->b:LX/ADY;

    iget-object v0, v0, LX/ADY;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1644898
    iget-object v0, p0, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;->b:LX/ADY;

    iget-object v0, v0, LX/ADY;->f:LX/03V;

    const-string v1, "CameraLeakDetector"

    const-string v2, "Camera opened while the app was backgrounded."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1644899
    :cond_0
    return-void
.end method
