.class public Lcom/facebook/photos/dialog/clipping/GlClippingImageView;
.super LX/9eQ;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Landroid/view/View;

.field private e:Landroid/widget/FrameLayout;

.field private f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1519691
    const-class v0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1519624
    invoke-direct {p0, p1}, LX/9eQ;-><init>(Landroid/content/Context;)V

    .line 1519625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1519689
    invoke-direct {p0, p1, p2}, LX/9eQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1519690
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1519687
    invoke-direct {p0, p1, p2, p3}, LX/9eQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1519688
    return-void
.end method

.method private static c(LX/9hP;)Z
    .locals 4

    .prologue
    const/high16 v2, 0x41a00000    # 20.0f

    .line 1519684
    iget-object v0, p0, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 1519685
    iget-object v1, p0, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    .line 1519686
    iget-object v2, p0, LX/9hP;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    sub-float/2addr v2, v0

    iget-object v3, p0, LX/9hP;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    iget-object v2, p0, LX/9hP;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v2, v1

    iget-object v3, p0, LX/9hP;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    iget-object v2, p0, LX/9hP;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget-object v2, p0, LX/9hP;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, LX/9hP;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    add-float/2addr v0, v1

    iget-object v1, p0, LX/9hP;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1519675
    invoke-super {p0}, LX/9eQ;->a()V

    .line 1519676
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    .line 1519677
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    .line 1519678
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1519679
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1519680
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1519681
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setPivotX(F)V

    .line 1519682
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setPivotY(F)V

    .line 1519683
    return-void
.end method

.method public final a(LX/9hP;LX/9hP;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1519654
    iget-boolean v1, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->f:Z

    .line 1519655
    invoke-static {p1}, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->c(LX/9hP;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->c(LX/9hP;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->f:Z

    .line 1519656
    iget-boolean v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1519657
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1519658
    iget-object v2, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->f:Z

    if-ne v1, v2, :cond_1

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 1519659
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1519660
    iget-boolean v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->f:Z

    if-eqz v0, :cond_4

    .line 1519661
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1519662
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setScaleX(F)V

    .line 1519663
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setScaleY(F)V

    .line 1519664
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setScaleX(F)V

    .line 1519665
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setScaleY(F)V

    .line 1519666
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setTranslationX(F)V

    .line 1519667
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 1519668
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 1519669
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 1519670
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setPivotX(F)V

    .line 1519671
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setPivotY(F)V

    .line 1519672
    :cond_2
    return-void

    .line 1519673
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1519674
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1519652
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1519653
    return-void
.end method

.method public final b(LX/9hP;)V
    .locals 6
    .param p1    # LX/9hP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1519626
    invoke-super {p0, p1}, LX/9eQ;->b(LX/9hP;)V

    .line 1519627
    iget-object v0, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1519628
    iget-object v1, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1519629
    iget-boolean v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->f:Z

    if-eqz v2, :cond_0

    .line 1519630
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setTranslationX(F)V

    .line 1519631
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 1519632
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setScaleX(F)V

    .line 1519633
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setScaleY(F)V

    .line 1519634
    iget-object v2, p0, LX/9eQ;->a:LX/9hP;

    iget-object v2, v2, LX/9hP;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1519635
    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, LX/9eQ;->a:LX/9hP;

    iget-object v4, v4, LX/9hP;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v1

    iget-object v4, p0, LX/9eQ;->a:LX/9hP;

    iget-object v4, v4, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1519636
    iget-object v4, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    neg-float v5, v2

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 1519637
    iget-object v4, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    neg-float v5, v3

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 1519638
    iget-object v4, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setPivotX(F)V

    .line 1519639
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setPivotY(F)V

    .line 1519640
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v0

    iget-object v3, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getScaleX()F

    move-result v3

    div-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1519641
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    iget-object v2, p0, LX/9eQ;->a:LX/9hP;

    iget-object v2, v2, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v1

    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getScaleY()F

    move-result v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 1519642
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1519643
    return-void

    .line 1519644
    :cond_0
    iget-object v2, p0, LX/9eQ;->a:LX/9hP;

    iget-object v2, v2, LX/9hP;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    .line 1519645
    iget-object v3, p0, LX/9eQ;->a:LX/9hP;

    iget-object v3, v3, LX/9hP;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    .line 1519646
    iget-object v4, p0, LX/9eQ;->a:LX/9hP;

    iget-object v4, v4, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float v0, v4, v0

    .line 1519647
    iget-object v4, p0, LX/9eQ;->a:LX/9hP;

    iget-object v4, v4, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v1

    .line 1519648
    iget-object v4, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1519649
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 1519650
    iget-object v2, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1519651
    iget-object v0, p0, Lcom/facebook/photos/dialog/clipping/GlClippingImageView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0
.end method
