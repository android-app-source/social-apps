.class public Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:LX/74S;

.field public final e:LX/31M;

.field public final f:I

.field public final g:I

.field public final h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1519535
    new-instance v0, LX/9eL;

    invoke-direct {v0}, LX/9eL;-><init>()V

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1519525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519526
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->a:Ljava/lang/String;

    .line 1519527
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->b:Ljava/lang/String;

    .line 1519528
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->c:I

    .line 1519529
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/74S;->valueOf(Ljava/lang/String;)LX/74S;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->d:LX/74S;

    .line 1519530
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/31M;->valueOf(Ljava/lang/String;)LX/31M;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->e:LX/31M;

    .line 1519531
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->f:I

    .line 1519532
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->g:I

    .line 1519533
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->h:Z

    .line 1519534
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILX/74S;LX/31M;IIZ)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1519514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519515
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->a:Ljava/lang/String;

    .line 1519516
    iput-object p2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->b:Ljava/lang/String;

    .line 1519517
    iput p3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->c:I

    .line 1519518
    iput-object p4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->d:LX/74S;

    .line 1519519
    iput-object p5, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->e:LX/31M;

    .line 1519520
    iput p6, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->f:I

    .line 1519521
    iput p7, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->g:I

    .line 1519522
    iput-boolean p8, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->h:Z

    .line 1519523
    return-void

    .line 1519524
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ILX/74S;LX/31M;IIZB)V
    .locals 0

    .prologue
    .line 1519503
    invoke-direct/range {p0 .. p8}, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;-><init>(Ljava/lang/String;Ljava/lang/String;ILX/74S;LX/31M;IIZ)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1519513
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1519504
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1519505
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1519506
    iget v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1519507
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->d:LX/74S;

    invoke-virtual {v0}, LX/74S;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1519508
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->e:LX/31M;

    invoke-virtual {v0}, LX/31M;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1519509
    iget v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1519510
    iget v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1519511
    iget-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1519512
    return-void
.end method
