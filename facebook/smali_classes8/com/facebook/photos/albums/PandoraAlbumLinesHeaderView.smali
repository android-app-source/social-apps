.class public Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1514528
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1514529
    invoke-direct {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->a()V

    .line 1514530
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1514497
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1514498
    invoke-direct {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->a()V

    .line 1514499
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1514525
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1514526
    invoke-direct {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->a()V

    .line 1514527
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1514509
    const v0, 0x7f0300cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1514510
    const v0, 0x7f0d0515

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1514511
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1514512
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ada

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ad9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1514513
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ad9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v1, v2

    .line 1514514
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v2, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1514515
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0ad9

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    iput v2, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1514516
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0ad8

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1514517
    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1514518
    const v0, 0x7f0d0516

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1514519
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1514520
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0ad7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1514521
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0ad9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1514522
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0ad7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1514523
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1514524
    return-void
.end method


# virtual methods
.method public setUploadButtonLines(Z)V
    .locals 3

    .prologue
    .line 1514500
    const v0, 0x7f0d0515

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1514501
    if-eqz p1, :cond_0

    .line 1514502
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0499

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1514503
    :goto_0
    const v0, 0x7f0d0516

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1514504
    if-eqz p1, :cond_1

    .line 1514505
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0499

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1514506
    :goto_1
    return-void

    .line 1514507
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a049a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 1514508
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a049a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1
.end method
