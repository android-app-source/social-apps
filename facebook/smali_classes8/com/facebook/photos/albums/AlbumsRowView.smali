.class public Lcom/facebook/photos/albums/AlbumsRowView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:D

.field private c:D

.field private d:D

.field private e:I

.field private f:I

.field private g:Lcom/facebook/graphql/model/GraphQLAlbum;

.field private h:Lcom/facebook/graphql/model/GraphQLAlbum;

.field private i:Z

.field private j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0hL;

.field private final l:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1514494
    const-class v0, Lcom/facebook/photos/albums/AlbumsRowView;

    const-string v1, "photos_albums_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/albums/AlbumsRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 1514485
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1514486
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    .line 1514487
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    .line 1514488
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    .line 1514489
    iput v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    .line 1514490
    iput v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    .line 1514491
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    .line 1514492
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->b()V

    .line 1514493
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 1514476
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1514477
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    .line 1514478
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    .line 1514479
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    .line 1514480
    iput v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    .line 1514481
    iput v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    .line 1514482
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    .line 1514483
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->b()V

    .line 1514484
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 1514467
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1514468
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    .line 1514469
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    .line 1514470
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    .line 1514471
    iput v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    .line 1514472
    iput v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    .line 1514473
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    .line 1514474
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->b()V

    .line 1514475
    return-void
.end method

.method private a(LX/9bE;)Landroid/graphics/Rect;
    .locals 8

    .prologue
    .line 1514460
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->k:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    .line 1514461
    if-eqz v0, :cond_0

    sget-object v0, LX/9bE;->RIGHT:LX/9bE;

    .line 1514462
    :goto_0
    if-ne p1, v0, :cond_1

    .line 1514463
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    iget v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    iget-wide v4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    iget v3, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    int-to-double v6, v3

    add-double/2addr v4, v6

    double-to-int v3, v4

    iget-wide v4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    iget v6, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    int-to-double v6, v6

    add-double/2addr v4, v6

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1514464
    :goto_1
    return-object v0

    .line 1514465
    :cond_0
    sget-object v0, LX/9bE;->LEFT:LX/9bE;

    goto :goto_0

    .line 1514466
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    iget-wide v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    iget-wide v4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    add-double/2addr v2, v4

    iget v1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    int-to-double v4, v1

    add-double/2addr v2, v4

    double-to-int v1, v2

    iget v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    iget-wide v4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    double-to-int v3, v4

    iget v4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    sub-int/2addr v3, v4

    iget-wide v4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    double-to-int v4, v4

    iget v5, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1
.end method

.method private a(LX/0Or;LX/0hL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0hL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1514457
    iput-object p1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->j:LX/0Or;

    .line 1514458
    iput-object p2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->k:LX/0hL;

    .line 1514459
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLAlbum;LX/9bE;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1514428
    sget-object v0, LX/9bE;->LEFT:LX/9bE;

    if-ne p2, v0, :cond_0

    move v0, v2

    .line 1514429
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v1, v0}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v5

    .line 1514430
    const/4 v1, 0x0

    .line 1514431
    if-nez p1, :cond_1

    .line 1514432
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0a0048

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v0, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object v4, v1

    move-object v1, v0

    .line 1514433
    :goto_1
    invoke-virtual {v5}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1514434
    invoke-virtual {v5, v4}, LX/1aX;->a(LX/1aZ;)V

    .line 1514435
    invoke-virtual {v5}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1514436
    invoke-direct {p0, p2}, Lcom/facebook/photos/albums/AlbumsRowView;->a(LX/9bE;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1514437
    if-nez p1, :cond_5

    .line 1514438
    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 1514439
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1514440
    :goto_2
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1514441
    return-void

    :cond_0
    move v0, v3

    .line 1514442
    goto :goto_0

    .line 1514443
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1514444
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f020626

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    goto :goto_1

    .line 1514445
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 1514446
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0a0498

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v4, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1514447
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v6, Lcom/facebook/photos/albums/AlbumsRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    .line 1514448
    iget-object v6, v5, LX/1aX;->f:LX/1aZ;

    move-object v6, v6

    .line 1514449
    invoke-virtual {v0, v6}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1514450
    instance-of v0, v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 1514451
    check-cast v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    sget-object v6, LX/397;->ALBUM:LX/397;

    .line 1514452
    invoke-static {v0, v2, v6}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 1514453
    :cond_4
    move-object v7, v4

    move-object v4, v1

    move-object v1, v7

    .line 1514454
    goto/16 :goto_1

    .line 1514455
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->i:Z

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 1514456
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_2
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/albums/AlbumsRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/albums/AlbumsRowView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-direct {p0, v1, v0}, Lcom/facebook/photos/albums/AlbumsRowView;->a(LX/0Or;LX/0hL;)V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1514366
    const-class v0, Lcom/facebook/photos/albums/AlbumsRowView;

    invoke-static {v0, p0}, Lcom/facebook/photos/albums/AlbumsRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1514367
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    .line 1514368
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0ad4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    .line 1514369
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/AlbumsRowView;->setWillNotCacheDrawing(Z)V

    .line 1514370
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1514371
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 1514372
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0498

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1514373
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1514374
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    .line 1514375
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v2

    .line 1514376
    iget-object v3, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v3, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 1514377
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1514378
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->forceLayout()V

    .line 1514379
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1514425
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    sget-object v1, LX/9bE;->LEFT:LX/9bE;

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/albums/AlbumsRowView;->a(Lcom/facebook/graphql/model/GraphQLAlbum;LX/9bE;)V

    .line 1514426
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->h:Lcom/facebook/graphql/model/GraphQLAlbum;

    sget-object v1, LX/9bE;->RIGHT:LX/9bE;

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/albums/AlbumsRowView;->a(Lcom/facebook/graphql/model/GraphQLAlbum;LX/9bE;)V

    .line 1514427
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1514495
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1514496
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1514423
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1514424
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1514418
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->e()V

    .line 1514419
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->d()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1514420
    iget-object v1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v1, v0}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1aX;->a(LX/1aZ;)V

    .line 1514421
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1514422
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/graphql/model/GraphQLAlbum;ZII)V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 1514404
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->a()V

    .line 1514405
    iput-object p1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514406
    iput-object p2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->h:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514407
    iput p5, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    .line 1514408
    iput p4, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    .line 1514409
    iget v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    if-eqz v0, :cond_0

    .line 1514410
    iget v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    .line 1514411
    :cond_0
    iget v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    if-nez v0, :cond_1

    .line 1514412
    iget v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    iput v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    .line 1514413
    :cond_1
    iget v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    iget-wide v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    sub-double/2addr v0, v2

    div-double/2addr v0, v4

    :goto_0
    iput-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    .line 1514414
    iput-boolean p3, p0, Lcom/facebook/photos/albums/AlbumsRowView;->i:Z

    .line 1514415
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->c()V

    .line 1514416
    return-void

    .line 1514417
    :cond_2
    iget-wide v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->b:D

    iget v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->e:I

    mul-int/lit8 v2, v2, 0x2

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->c:D

    sub-double/2addr v0, v2

    div-double/2addr v0, v4

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1514401
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/Canvas;)V

    .line 1514402
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1514403
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x70e886

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1514398
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onAttachedToWindow()V

    .line 1514399
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->d()V

    .line 1514400
    const/16 v1, 0x2d

    const v2, 0x8acd268

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7e427ea8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1514394
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 1514395
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->e()V

    .line 1514396
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->a()V

    .line 1514397
    const/16 v1, 0x2d

    const v2, 0x214b3338

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 1514391
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onFinishTemporaryDetach()V

    .line 1514392
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->d()V

    .line 1514393
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 1514384
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v0, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 1514385
    iget-wide v2, p0, Lcom/facebook/photos/albums/AlbumsRowView;->d:D

    iget v1, p0, Lcom/facebook/photos/albums/AlbumsRowView;->f:I

    int-to-double v4, v1

    add-double/2addr v2, v4

    double-to-int v1, v2

    .line 1514386
    if-ne v1, v0, :cond_0

    .line 1514387
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 1514388
    :goto_0
    return-void

    .line 1514389
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 1514390
    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/albums/AlbumsRowView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 1514381
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onStartTemporaryDetach()V

    .line 1514382
    invoke-direct {p0}, Lcom/facebook/photos/albums/AlbumsRowView;->e()V

    .line 1514383
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1514380
    iget-object v0, p0, Lcom/facebook/photos/albums/AlbumsRowView;->l:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
