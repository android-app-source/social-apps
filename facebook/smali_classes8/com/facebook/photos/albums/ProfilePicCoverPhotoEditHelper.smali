.class public Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1Ck;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/1HI;

.field private final g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation
.end field

.field private final i:LX/9fy;

.field public final j:LX/9iU;

.field private final k:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final l:LX/03V;

.field public final m:LX/0kL;

.field public final n:LX/0tX;

.field private final o:LX/0i4;

.field private final p:LX/0ad;

.field private final q:LX/23P;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1515113
    const-class v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    const-string v1, "set_cover_photo"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1515114
    const-class v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    sput-object v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->c:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(LX/1Ck;Lcom/facebook/content/SecureContextHelper;LX/1HI;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/9fy;LX/9iU;Lcom/facebook/auth/viewercontext/ViewerContext;LX/03V;LX/0kL;LX/0tX;LX/0i4;LX/0ad;LX/23P;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1514982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1514983
    iput-object p1, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->d:LX/1Ck;

    .line 1514984
    iput-object p2, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1514985
    iput-object p3, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->f:LX/1HI;

    .line 1514986
    iput-object p4, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->g:Ljava/util/concurrent/Executor;

    .line 1514987
    iput-object p5, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->h:Ljava/util/concurrent/ExecutorService;

    .line 1514988
    iput-object p6, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->i:LX/9fy;

    .line 1514989
    iput-object p7, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->j:LX/9iU;

    .line 1514990
    iput-object p8, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1514991
    iput-object p9, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->l:LX/03V;

    .line 1514992
    iput-object p10, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->m:LX/0kL;

    .line 1514993
    iput-object p11, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->n:LX/0tX;

    .line 1514994
    iput-object p12, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->o:LX/0i4;

    .line 1514995
    iput-object p13, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->p:LX/0ad;

    .line 1514996
    iput-object p14, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->q:LX/23P;

    .line 1514997
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLPhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1515110
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1515111
    :cond_0
    const/4 v0, 0x0

    .line 1515112
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(JLcom/facebook/photos/base/photos/PhotoFetchInfo;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/facebook/photos/base/photos/PhotoFetchInfo;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515104
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->i:LX/9fy;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1515105
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1515106
    new-instance v3, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    invoke-direct {v3, v1, p3}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;-><init>(Ljava/util/List;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    .line 1515107
    const-string v4, "fetchPhotosMetadataParams"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1515108
    iget-object v4, v0, LX/9fy;->a:LX/0aG;

    const-string p0, "fetch_photos_metadata"

    invoke-virtual {v3}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->c()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    const p1, -0xb1d0a9b

    invoke-static {v4, p0, v2, v3, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    move-object v0, v2

    .line 1515109
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 3

    .prologue
    .line 1515097
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1515098
    const-string v1, "photo"

    invoke-static {v0, v1, p0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1515099
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 1515100
    if-eqz v1, :cond_0

    .line 1515101
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1515102
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 1515103
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;JLandroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;JLandroid/net/Uri;)V
    .locals 11

    .prologue
    .line 1515093
    invoke-static/range {p7 .. p7}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v8

    .line 1515094
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->f:LX/1HI;

    sget-object v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v8, v1}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v9

    .line 1515095
    new-instance v0, LX/9bR;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-wide v4, p1

    move-wide/from16 v6, p5

    invoke-direct/range {v0 .. v8}, LX/9bR;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Landroid/support/v4/app/DialogFragment;Landroid/support/v4/app/FragmentActivity;JJLX/1bf;)V

    iget-object v1, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->g:Ljava/util/concurrent/Executor;

    invoke-interface {v9, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1515096
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;JLandroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;JLX/1bf;)V
    .locals 11

    .prologue
    .line 1515090
    iget-object v2, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->f:LX/1HI;

    sget-object v3, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p7

    invoke-virtual {v2, v0, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v10

    .line 1515091
    new-instance v2, LX/9bT;

    move-object v3, p0

    move-object v4, p4

    move-object v5, p3

    move-wide v6, p1

    move-wide/from16 v8, p5

    invoke-direct/range {v2 .. v9}, LX/9bT;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Landroid/support/v4/app/DialogFragment;Landroid/support/v4/app/FragmentActivity;JJ)V

    iget-object v3, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->g:Ljava/util/concurrent/Executor;

    invoke-interface {v10, v2, v3}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1515092
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1515059
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SF;

    .line 1515060
    new-instance v1, LX/5Rw;

    invoke-direct {v1}, LX/5Rw;-><init>()V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3, p1}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v1

    sget-object v3, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v1, v3}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v1

    sget-object v3, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v1, v3}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object v1

    const/4 v3, 0x1

    .line 1515061
    iput-boolean v3, v1, LX/5Rw;->f:Z

    .line 1515062
    move-object v1, v1

    .line 1515063
    iput-boolean v2, v1, LX/5Rw;->i:Z

    .line 1515064
    move-object v1, v1

    .line 1515065
    iget-object v3, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->q:LX/23P;

    const v4, 0x7f0813d1    # 1.808779E38f

    invoke-virtual {p4, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1515066
    iput-object v3, v1, LX/5Rw;->j:Ljava/lang/String;

    .line 1515067
    move-object v1, v1

    .line 1515068
    invoke-virtual {v1}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v3

    .line 1515069
    if-eqz p3, :cond_0

    .line 1515070
    new-instance v1, LX/5SJ;

    invoke-direct {v1, p3}, LX/5SJ;-><init>(Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v1

    invoke-virtual {v1}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    .line 1515071
    :goto_0
    invoke-interface {v0, p4, v1, v3}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 1515072
    iget-object v1, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->e:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x26bb

    invoke-interface {v1, v0, v2, p4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1515073
    return-void

    .line 1515074
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->p:LX/0ad;

    sget-short v4, LX/0wf;->c:S

    invoke-interface {v1, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    .line 1515075
    :goto_1
    new-instance v4, LX/5SJ;

    invoke-direct {v4}, LX/5SJ;-><init>()V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v4

    sget-object v5, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v5}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v5

    .line 1515076
    iput-object v5, v4, LX/5SJ;->c:Ljava/lang/String;

    .line 1515077
    move-object v4, v4

    .line 1515078
    const-wide/16 v6, 0x0

    .line 1515079
    iput-wide v6, v4, LX/5SJ;->e:J

    .line 1515080
    move-object v4, v4

    .line 1515081
    iget-object v5, v3, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    move-object v5, v5

    .line 1515082
    iput-object v5, v4, LX/5SJ;->d:Ljava/lang/String;

    .line 1515083
    move-object v4, v4

    .line 1515084
    iput-boolean v2, v4, LX/5SJ;->o:Z

    .line 1515085
    move-object v2, v4

    .line 1515086
    iput v1, v2, LX/5SJ;->q:I

    .line 1515087
    move-object v1, v2

    .line 1515088
    invoke-virtual {v1}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 1515089
    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;
    .locals 15

    .prologue
    .line 1515055
    new-instance v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/9fy;->a(LX/0QB;)LX/9fy;

    move-result-object v6

    check-cast v6, LX/9fy;

    invoke-static {p0}, LX/9iU;->a(LX/0QB;)LX/9iU;

    move-result-object v7

    check-cast v7, LX/9iU;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v8

    check-cast v8, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    const-class v12, LX/0i4;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/0i4;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v14

    check-cast v14, LX/23P;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;-><init>(LX/1Ck;Lcom/facebook/content/SecureContextHelper;LX/1HI;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/9fy;LX/9iU;Lcom/facebook/auth/viewercontext/ViewerContext;LX/03V;LX/0kL;LX/0tX;LX/0i4;LX/0ad;LX/23P;)V

    .line 1515056
    const/16 v1, 0x36f4

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 1515057
    iput-object v1, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a:LX/0Or;

    .line 1515058
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1515053
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1515054
    return-void
.end method

.method public final a(JLandroid/net/Uri;Landroid/app/Activity;J)V
    .locals 3
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1515038
    if-nez p3, :cond_1

    .line 1515039
    sget-object v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->c:Ljava/lang/Class;

    const-string v1, "Image path from TempBinaryFileManager cannot be null."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1515040
    :cond_0
    :goto_0
    return-void

    .line 1515041
    :cond_1
    if-eqz p4, :cond_0

    .line 1515042
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.facebook.timeline.coverphoto.activity.CoverPhotoRepositionActivity"

    invoke-direct {v1, p4, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1515043
    const-string v1, "cover_photo_uri"

    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1515044
    const-string v1, "cover_photo_fbid"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1515045
    iget-object v1, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1515046
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v2

    .line 1515047
    if-eqz v1, :cond_2

    .line 1515048
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v2, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1515049
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->PMA_COVERPHOTO_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1515050
    :goto_1
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1515051
    iget-object v1, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->e:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x26bc

    invoke-interface {v1, v0, v2, p4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 1515052
    :cond_2
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->TIMELINE_COVERPHOTO_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method public final a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V
    .locals 5
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1515023
    if-eqz p6, :cond_1

    .line 1515024
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1515025
    new-instance v1, LX/4Xy;

    invoke-direct {v1}, LX/4Xy;-><init>()V

    .line 1515026
    iput-object v0, v1, LX/4Xy;->I:Ljava/lang/String;

    .line 1515027
    move-object v1, v1

    .line 1515028
    if-eqz p3, :cond_0

    .line 1515029
    new-instance v2, LX/2dc;

    invoke-direct {v2}, LX/2dc;-><init>()V

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1515030
    iput-object v3, v2, LX/2dc;->h:Ljava/lang/String;

    .line 1515031
    move-object v2, v2

    .line 1515032
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1515033
    iput-object v2, v1, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1515034
    :cond_0
    invoke-virtual {v1}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    move-object v0, v1

    .line 1515035
    invoke-static {v0, p4}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/base/fragment/FbFragment;)V

    .line 1515036
    :goto_0
    return-void

    .line 1515037
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->d:LX/1Ck;

    sget-object v1, LX/9bV;->FETCH_FACEBOOK_PHOTO:LX/9bV;

    invoke-direct {p0, p1, p2, p5}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/photos/base/photos/PhotoFetchInfo;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/9bN;

    invoke-direct {v3, p0, p4}, LX/9bN;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/base/fragment/FbFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a(JLandroid/support/v4/app/FragmentActivity;JLcom/facebook/photos/base/photos/PhotoFetchInfo;)V
    .locals 4

    .prologue
    .line 1515021
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->d:LX/1Ck;

    sget-object v1, LX/9bV;->FETCH_FACEBOOK_PHOTO:LX/9bV;

    invoke-direct {p0, p1, p2, p6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/photos/base/photos/PhotoFetchInfo;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/9bO;

    invoke-direct {v3, p0, p3, p4, p5}, LX/9bO;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Landroid/support/v4/app/FragmentActivity;J)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1515022
    return-void
.end method

.method public final a(JLcom/facebook/base/fragment/FbFragment;JLcom/facebook/photos/base/photos/PhotoFetchInfo;)V
    .locals 7

    .prologue
    .line 1515018
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1515019
    :cond_0
    :goto_0
    return-void

    .line 1515020
    :cond_1
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object v0, p0

    move-wide v1, p1

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/support/v4/app/FragmentActivity;JLcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    goto :goto_0
.end method

.method public final a(JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V
    .locals 5
    .param p3    # Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1515016
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->d:LX/1Ck;

    sget-object v1, LX/9bV;->FETCH_FACEBOOK_PHOTO:LX/9bV;

    invoke-direct {p0, p1, p2, p5}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/photos/base/photos/PhotoFetchInfo;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/9bM;

    invoke-direct {v3, p0, p3, p4}, LX/9bM;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1515017
    return-void
.end method

.method public final a(JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V
    .locals 7
    .param p3    # Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1515014
    invoke-virtual {p4}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    .line 1515015
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V
    .locals 9

    .prologue
    .line 1515011
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1515012
    :cond_0
    :goto_0
    return-void

    .line 1515013
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->o:LX/0i4;

    invoke-virtual {v0, p2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v6

    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v7, v0

    new-instance v0, LX/9bP;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, LX/9bP;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V

    invoke-virtual {v6, v7, v0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V
    .locals 7
    .param p2    # Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0xb4

    .line 1514998
    invoke-static {p1}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1514999
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 1515000
    :cond_0
    :goto_0
    return-void

    .line 1515001
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    if-lt v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    if-ge v1, v2, :cond_3

    .line 1515002
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->l:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "First query\'s photo is too small to be profile picture"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515003
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1515004
    new-instance v3, LX/5jt;

    invoke-direct {v3}, LX/5jt;-><init>()V

    move-object v3, v3

    .line 1515005
    const-string v4, "node"

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1515006
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x258

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 1515007
    iget-object v4, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->n:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1515008
    iget-object v4, p0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->d:LX/1Ck;

    sget-object v5, LX/9bV;->BEST_AVAILABLE_IMAGE_URI_QUERY:LX/9bV;

    new-instance v6, LX/9bU;

    invoke-direct {v6, p0, v0, p2, p3}, LX/9bU;-><init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Ljava/lang/String;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1515009
    goto :goto_0

    .line 1515010
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0, p2, p3}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a$redex0(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    goto :goto_0
.end method
