.class public Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1514531
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1514532
    invoke-direct {p0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->a()V

    .line 1514533
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1514534
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1514535
    invoke-direct {p0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->a()V

    .line 1514536
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1514537
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1514538
    invoke-direct {p0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->a()V

    .line 1514539
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 1514540
    const v0, 0x7f0300d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1514541
    const v0, 0x7f0d051e

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    .line 1514542
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setUploadButtonLines(Z)V

    .line 1514543
    const v0, 0x7f0d051f

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1514544
    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ada

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1514545
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1514546
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1514547
    const v0, 0x7f0d0520

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1514548
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1514549
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1514550
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1514551
    return-void
.end method
