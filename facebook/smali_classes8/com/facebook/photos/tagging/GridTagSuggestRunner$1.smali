.class public final Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/media/MediaItem;

.field public final synthetic b:LX/74p;

.field public final synthetic c:LX/9ib;


# direct methods
.method public constructor <init>(LX/9ib;Lcom/facebook/ipc/media/MediaItem;LX/74p;)V
    .locals 0

    .prologue
    .line 1528055
    iput-object p1, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iput-object p2, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->a:Lcom/facebook/ipc/media/MediaItem;

    iput-object p3, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->b:LX/74p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1528056
    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iget-object v0, v0, LX/9ib;->e:LX/75S;

    invoke-virtual {v0}, LX/75S;->c()V

    .line 1528057
    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iget-object v1, v0, LX/9ib;->e:LX/75S;

    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->a:Lcom/facebook/ipc/media/MediaItem;

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1, v0}, LX/75S;->a(Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 1528058
    new-instance v1, LX/7ye;

    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->a:Lcom/facebook/ipc/media/MediaItem;

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iget-object v2, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iget-object v2, v2, LX/9ib;->f:LX/75Q;

    iget-object v3, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iget-object v3, v3, LX/9ib;->g:LX/75F;

    invoke-direct {v1, v0, v2, v3}, LX/7ye;-><init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V

    .line 1528059
    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->b:LX/74p;

    sget-object v2, LX/74p;->SELECT:LX/74p;

    if-ne v0, v2, :cond_0

    .line 1528060
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1528061
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1528062
    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iget-object v0, v0, LX/9ib;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(Ljava/util/List;)V

    .line 1528063
    :goto_0
    return-void

    .line 1528064
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;->c:LX/9ib;

    iget-object v0, v0, LX/9ib;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0, v1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;)V

    goto :goto_0
.end method
