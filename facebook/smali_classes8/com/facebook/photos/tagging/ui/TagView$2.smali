.class public final Lcom/facebook/photos/tagging/ui/TagView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9ij;


# direct methods
.method public constructor <init>(LX/9ij;)V
    .locals 0

    .prologue
    .line 1528188
    iput-object p1, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x96

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1528189
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-boolean v0, v0, LX/9ij;->r:Z

    if-nez v0, :cond_0

    .line 1528190
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-virtual {v1}, LX/9ij;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-virtual {v2}, LX/9ij;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1528191
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1528192
    iget-object v1, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-virtual {v1, v0}, LX/9ij;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528193
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->i:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget v2, v2, LX/9ij;->u:I

    iget-object v3, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget v3, v3, LX/9ij;->t:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528194
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-boolean v0, v0, LX/9ij;->r:Z

    if-nez v0, :cond_1

    .line 1528195
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->m:LX/8Hs;

    invoke-virtual {v0, v4}, LX/8Hs;->b(Z)V

    .line 1528196
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1528197
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v1, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-virtual {v1}, LX/9ij;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget v2, v2, LX/9ij;->u:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget v2, v2, LX/9ij;->v:I

    sub-int/2addr v1, v2

    .line 1528198
    iput v1, v0, LX/9ij;->q:I

    .line 1528199
    iget-object v6, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    new-instance v0, LX/8Hm;

    iget-object v1, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    sget-object v2, LX/8Hk;->WIDTH:LX/8Hk;

    sget-object v3, LX/8Hl;->EXPAND:LX/8Hl;

    iget-object v4, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-virtual {v4}, LX/9ij;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget v5, v5, LX/9ij;->q:I

    invoke-direct/range {v0 .. v5}, LX/8Hm;-><init>(Landroid/view/View;LX/8Hk;LX/8Hl;II)V

    .line 1528200
    iput-object v0, v6, LX/9ij;->k:LX/8Hm;

    .line 1528201
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->k:LX/8Hm;

    invoke-virtual {v0, v8, v9}, LX/8Hm;->setDuration(J)V

    .line 1528202
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->k:LX/8Hm;

    new-instance v1, LX/9ie;

    invoke-direct {v1, p0}, LX/9ie;-><init>(Lcom/facebook/photos/tagging/ui/TagView$2;)V

    invoke-virtual {v0, v1}, LX/8Hm;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1528203
    iget-object v6, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    new-instance v0, LX/8Hm;

    iget-object v1, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    sget-object v2, LX/8Hk;->WIDTH:LX/8Hk;

    sget-object v3, LX/8Hl;->COLLAPSE:LX/8Hl;

    iget-object v4, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    invoke-virtual {v4}, LX/9ij;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget v5, v5, LX/9ij;->q:I

    invoke-direct/range {v0 .. v5}, LX/8Hm;-><init>(Landroid/view/View;LX/8Hk;LX/8Hl;II)V

    .line 1528204
    iput-object v0, v6, LX/9ij;->l:LX/8Hm;

    .line 1528205
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->l:LX/8Hm;

    invoke-virtual {v0, v8, v9}, LX/8Hm;->setDuration(J)V

    .line 1528206
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->l:LX/8Hm;

    new-instance v1, LX/9if;

    invoke-direct {v1, p0}, LX/9if;-><init>(Lcom/facebook/photos/tagging/ui/TagView$2;)V

    invoke-virtual {v0, v1}, LX/8Hm;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1528207
    return-void

    .line 1528208
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    .line 1528209
    iput-boolean v5, v0, LX/9ij;->n:Z

    .line 1528210
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagView$2;->a:LX/9ij;

    iget-object v0, v0, LX/9ij;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
