.class public final Lcom/facebook/photos/tagging/ui/TagsView$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/9j0;


# direct methods
.method public constructor <init>(LX/9j0;Z)V
    .locals 0

    .prologue
    .line 1528917
    iput-object p1, p0, Lcom/facebook/photos/tagging/ui/TagsView$3;->b:LX/9j0;

    iput-boolean p2, p0, Lcom/facebook/photos/tagging/ui/TagsView$3;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1528918
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagsView$3;->b:LX/9j0;

    iget-object v0, v0, LX/9j0;->h:LX/8Jm;

    iget-object v1, p0, Lcom/facebook/photos/tagging/ui/TagsView$3;->b:LX/9j0;

    iget-object v1, v1, LX/9j0;->c:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-boolean v2, p0, Lcom/facebook/photos/tagging/ui/TagsView$3;->a:Z

    invoke-virtual {v0, v1, v2}, LX/8Jm;->a(LX/7UZ;Z)V

    .line 1528919
    iget-object v0, p0, Lcom/facebook/photos/tagging/ui/TagsView$3;->b:LX/9j0;

    iget-object v0, v0, LX/9j0;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9ij;

    .line 1528920
    invoke-virtual {v0, v3}, LX/9ij;->setVisibility(I)V

    .line 1528921
    invoke-virtual {v0}, LX/9ij;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LX/9ij;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1528922
    invoke-virtual {v0, v3}, LX/9ij;->a(Z)V

    goto :goto_0

    .line 1528923
    :cond_1
    return-void
.end method
