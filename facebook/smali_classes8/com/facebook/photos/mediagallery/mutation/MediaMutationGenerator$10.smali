.class public final Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/37X;

.field public final synthetic b:Ljava/util/concurrent/Callable;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:LX/9hh;


# direct methods
.method public constructor <init>(LX/9hh;LX/37X;Ljava/util/concurrent/Callable;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1526726
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->d:LX/9hh;

    iput-object p2, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    iput-object p3, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->b:Ljava/util/concurrent/Callable;

    iput-object p4, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1526727
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/37X;->g()I

    .line 1526728
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->b:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    const v1, -0x24348d6b

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 1526729
    iget-boolean v1, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 1526730
    if-nez v1, :cond_1

    .line 1526731
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v0, v1

    .line 1526732
    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1526733
    :catch_0
    move-exception v0

    .line 1526734
    :try_start_1
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/37X;->a(Z)V

    .line 1526735
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1}, LX/0SQ;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1526736
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1526737
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/1NB;->e()V

    .line 1526738
    :goto_0
    return-void

    .line 1526739
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/37X;->a(Z)V

    .line 1526740
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, 0xf51373a

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1526741
    :try_start_3
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->d:LX/9hh;

    iget-object v0, v0, LX/9hh;->b:LX/0tX;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/37X;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1526742
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/1NB;->e()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;->a:LX/37X;

    invoke-virtual {v1}, LX/1NB;->e()V

    throw v0

    :catch_1
    goto :goto_1
.end method
