.class public final Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1527196
    new-instance v0, LX/9i5;

    invoke-direct {v0}, LX/9i5;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1527197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527198
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->a:Ljava/lang/String;

    .line 1527199
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->b:Ljava/lang/String;

    .line 1527200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->c:I

    .line 1527201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->d:I

    .line 1527202
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 1527203
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)V
    .locals 0

    .prologue
    .line 1527204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527205
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->a:Ljava/lang/String;

    .line 1527206
    iput-object p2, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->b:Ljava/lang/String;

    .line 1527207
    iput p3, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->c:I

    .line 1527208
    iput p4, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->d:I

    .line 1527209
    iput-object p5, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 1527210
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1527211
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1527212
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1527213
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1527214
    iget v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1527215
    iget v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1527216
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1527217
    return-void
.end method
