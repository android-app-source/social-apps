.class public Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/21D;

.field public final B:Z

.field public final C:Z

.field public final D:LX/1Up;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final E:Landroid/widget/ImageView$ScaleType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TM;>;"
        }
    .end annotation
.end field

.field public final d:LX/9g5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/8Hh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:I

.field public final q:LX/74S;

.field public final r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:LX/31M;

.field public final t:I

.field public final u:LX/9hM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:I

.field public final w:Landroid/content/res/Resources;

.field public final x:I

.field public final y:Ljava/lang/String;

.field public final z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1526461
    new-instance v0, LX/9hL;

    invoke-direct {v0}, LX/9hL;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1526462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526463
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->a:Ljava/lang/String;

    .line 1526464
    const-class v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1526465
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1526466
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->c:LX/0Px;

    .line 1526467
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->d:LX/9g5;

    .line 1526468
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->e:LX/8Hh;

    .line 1526469
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    .line 1526470
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->g:LX/1bf;

    .line 1526471
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->i:Ljava/lang/String;

    .line 1526472
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->j:Ljava/lang/String;

    .line 1526473
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->h:Ljava/lang/String;

    .line 1526474
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->k:Ljava/lang/String;

    .line 1526475
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->l:Z

    .line 1526476
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->m:Z

    .line 1526477
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->n:Z

    .line 1526478
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->o:Z

    .line 1526479
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->p:I

    .line 1526480
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/74S;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    .line 1526481
    const-class v0, Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 1526482
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/31M;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->s:LX/31M;

    .line 1526483
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->t:I

    .line 1526484
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->u:LX/9hM;

    .line 1526485
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    .line 1526486
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->w:Landroid/content/res/Resources;

    .line 1526487
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->x:I

    .line 1526488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    .line 1526489
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->z:Ljava/lang/String;

    .line 1526490
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/21D;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->A:LX/21D;

    .line 1526491
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->B:Z

    .line 1526492
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->C:Z

    .line 1526493
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->D:LX/1Up;

    .line 1526494
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->E:Landroid/widget/ImageView$ScaleType;

    .line 1526495
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;LX/9g5;LX/8Hh;Ljava/lang/String;LX/1bf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILX/74S;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;LX/31M;ILX/9hM;ILandroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;LX/21D;ZZLX/1Up;Landroid/widget/ImageView$ScaleType;)V
    .locals 2
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/9g5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/8Hh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # LX/9hM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p30    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p31    # Landroid/widget/ImageView$ScaleType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;",
            "LX/0Px",
            "<TM;>;",
            "LX/9g5;",
            "LX/8Hh;",
            "Ljava/lang/String;",
            "LX/1bf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZZI",
            "LX/74S;",
            "Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;",
            "LX/31M;",
            "I",
            "LX/9hM;",
            "I",
            "Landroid/content/res/Resources;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/21D;",
            "ZZ",
            "LX/1Up;",
            "Landroid/widget/ImageView$ScaleType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1526496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526497
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->a:Ljava/lang/String;

    .line 1526498
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1526499
    iput-object p6, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    .line 1526500
    if-eqz p3, :cond_1

    :goto_1
    iput-object p3, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->c:LX/0Px;

    .line 1526501
    iput-object p4, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->d:LX/9g5;

    .line 1526502
    iput-object p5, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->e:LX/8Hh;

    .line 1526503
    iput-object p7, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->g:LX/1bf;

    .line 1526504
    iput-object p8, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->i:Ljava/lang/String;

    .line 1526505
    iput-object p9, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->j:Ljava/lang/String;

    .line 1526506
    iput-object p10, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->h:Ljava/lang/String;

    .line 1526507
    iput-object p11, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->k:Ljava/lang/String;

    .line 1526508
    iput-boolean p12, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->l:Z

    .line 1526509
    iput-boolean p13, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->m:Z

    .line 1526510
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->n:Z

    .line 1526511
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->o:Z

    .line 1526512
    move/from16 v0, p16

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->p:I

    .line 1526513
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    .line 1526514
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 1526515
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->s:LX/31M;

    .line 1526516
    move/from16 v0, p20

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->t:I

    .line 1526517
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->u:LX/9hM;

    .line 1526518
    move/from16 v0, p22

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    .line 1526519
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->w:Landroid/content/res/Resources;

    .line 1526520
    move/from16 v0, p24

    iput v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->x:I

    .line 1526521
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    .line 1526522
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->z:Ljava/lang/String;

    .line 1526523
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->A:LX/21D;

    .line 1526524
    move/from16 v0, p28

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->B:Z

    .line 1526525
    move/from16 v0, p29

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->C:Z

    .line 1526526
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->D:LX/1Up;

    .line 1526527
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->E:Landroid/widget/ImageView$ScaleType;

    .line 1526528
    return-void

    .line 1526529
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1526530
    :cond_1
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object p3

    goto :goto_1
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;LX/9g5;LX/8Hh;Ljava/lang/String;LX/1bf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILX/74S;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;LX/31M;ILX/9hM;ILandroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;LX/21D;ZZLX/1Up;Landroid/widget/ImageView$ScaleType;B)V
    .locals 0

    .prologue
    .line 1526531
    invoke-direct/range {p0 .. p31}, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;-><init>(Ljava/lang/String;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;LX/9g5;LX/8Hh;Ljava/lang/String;LX/1bf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILX/74S;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;LX/31M;ILX/9hM;ILandroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;LX/21D;ZZLX/1Up;Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1526532
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1526533
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526534
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1526535
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526536
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526537
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526538
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526539
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526540
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->l:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1526541
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->m:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1526542
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1526543
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->o:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1526544
    iget v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->p:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1526545
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1526546
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1526547
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->s:LX/31M;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1526548
    iget v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1526549
    iget v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1526550
    iget v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->x:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1526551
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526552
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526553
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->A:LX/21D;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1526554
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->B:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1526555
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->C:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1526556
    return-void
.end method
