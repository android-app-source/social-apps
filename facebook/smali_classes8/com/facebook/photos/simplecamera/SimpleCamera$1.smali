.class public final Lcom/facebook/photos/simplecamera/SimpleCamera$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9iX;

.field public final synthetic b:Landroid/content/Intent;

.field public final synthetic c:LX/9Tq;

.field public final synthetic d:LX/9iY;


# direct methods
.method public constructor <init>(LX/9iY;LX/9iX;Landroid/content/Intent;LX/9Tq;)V
    .locals 0

    .prologue
    .line 1527925
    iput-object p1, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->d:LX/9iY;

    iput-object p2, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->a:LX/9iX;

    iput-object p3, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->b:Landroid/content/Intent;

    iput-object p4, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->c:LX/9Tq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1527926
    iget-object v0, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->d:LX/9iY;

    iget-object v1, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->a:LX/9iX;

    iget-object v2, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->b:Landroid/content/Intent;

    const/4 v3, 0x0

    .line 1527927
    sget-object v4, LX/9iX;->VIDEO:LX/9iX;

    if-ne v1, v4, :cond_3

    .line 1527928
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 1527929
    :cond_0
    :goto_0
    move-object v0, v3

    .line 1527930
    if-nez v0, :cond_1

    .line 1527931
    :goto_1
    return-void

    .line 1527932
    :cond_1
    invoke-static {v0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1527933
    iget-object v1, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->d:LX/9iY;

    iget-object v1, v1, LX/9iY;->d:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x0

    new-instance v3, LX/9iV;

    invoke-direct {v3, p0}, LX/9iV;-><init>(Lcom/facebook/photos/simplecamera/SimpleCamera$1;)V

    invoke-static {v1, v2, v0, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    goto :goto_1

    .line 1527934
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->d:LX/9iY;

    iget-object v2, p0, Lcom/facebook/photos/simplecamera/SimpleCamera$1;->c:LX/9Tq;

    invoke-static {v1, v0, v2}, LX/9iY;->a$redex0(LX/9iY;Landroid/net/Uri;LX/9Tq;)V

    goto :goto_1

    .line 1527935
    :cond_3
    iget-object v4, v0, LX/9iY;->g:Landroid/net/Uri;

    if-eqz v4, :cond_0

    .line 1527936
    invoke-static {v0}, LX/9iY;->c(LX/9iY;)Ljava/io/File;

    move-result-object v3

    .line 1527937
    if-eqz v3, :cond_4

    .line 1527938
    iget-object v4, v0, LX/9iY;->g:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 1527939
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1527940
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v4}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1527941
    :try_start_0
    invoke-static {v3, v5}, LX/1t3;->b(Ljava/io/File;Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1527942
    iget-object v4, v0, LX/9iY;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplecamera/SimpleCamera$3;

    invoke-direct {v6, v0, v5}, Lcom/facebook/photos/simplecamera/SimpleCamera$3;-><init>(LX/9iY;Ljava/io/File;)V

    const v5, 0x4b114bcc    # 9522124.0f

    invoke-static {v4, v6, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1527943
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 1527944
    :catch_0
    :cond_4
    iget-object v3, v0, LX/9iY;->g:Landroid/net/Uri;

    goto :goto_0
.end method
