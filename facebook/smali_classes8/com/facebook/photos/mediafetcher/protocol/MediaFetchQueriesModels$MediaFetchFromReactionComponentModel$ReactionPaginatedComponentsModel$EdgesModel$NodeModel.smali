.class public final Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x39390578
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524293
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524292
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1524290
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1524291
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524287
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1524288
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1524289
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524285
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    .line 1524286
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1524277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524278
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1524279
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1524280
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1524281
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1524282
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1524283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524284
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1524269
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524270
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1524271
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    .line 1524272
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1524273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    .line 1524274
    iput-object v0, v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    .line 1524275
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524276
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/5kD;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524268
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1524265
    new-instance v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;-><init>()V

    .line 1524266
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1524267
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1524263
    const v0, -0x512fd684

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1524264
    const v0, -0x2605f7f0

    return v0
.end method
