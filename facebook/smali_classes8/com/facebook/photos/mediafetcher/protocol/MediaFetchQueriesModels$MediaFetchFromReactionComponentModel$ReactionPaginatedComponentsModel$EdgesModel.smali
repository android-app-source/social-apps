.class public final Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x34dd5c01
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524320
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524319
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1524317
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1524318
    return-void
.end method

.method private j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524315
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->e:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->e:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    .line 1524316
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->e:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1524321
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524322
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1524323
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1524324
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1524325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524326
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1524307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524308
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1524309
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    .line 1524310
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1524311
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;

    .line 1524312
    iput-object v0, v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->e:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    .line 1524313
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524314
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524306
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1524303
    new-instance v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;-><init>()V

    .line 1524304
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1524305
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1524302
    const v0, 0x5822ffa6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1524301
    const v0, -0x1af1ea69

    return v0
.end method
