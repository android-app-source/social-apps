.class public final Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x173ce4e3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524779
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524755
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1524756
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1524757
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1524758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524759
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1524760
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1524761
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1524762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524763
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1524764
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->e:Ljava/util/List;

    .line 1524765
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1524766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524767
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1524768
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1524769
    if-eqz v1, :cond_0

    .line 1524770
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;

    .line 1524771
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;->e:Ljava/util/List;

    .line 1524772
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524773
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1524774
    new-instance v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;

    invoke-direct {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPageMenusModel$PagePhotoMenusModel;-><init>()V

    .line 1524775
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1524776
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1524777
    const v0, -0x6b30b9ab

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1524778
    const v0, -0x34fe4251    # -8502703.0f

    return v0
.end method
