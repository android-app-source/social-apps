.class public final Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x13b57fef
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524367
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1524366
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1524364
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1524365
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1524356
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524357
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1524358
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1524359
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1524360
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1524361
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1524362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524363
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1524354
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->e:Ljava/util/List;

    .line 1524355
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1524341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1524342
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1524343
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1524344
    if-eqz v1, :cond_2

    .line 1524345
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;

    .line 1524346
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1524347
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1524348
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    .line 1524349
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1524350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;

    .line 1524351
    iput-object v0, v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    .line 1524352
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1524353
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1524334
    new-instance v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;

    invoke-direct {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;-><init>()V

    .line 1524335
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1524336
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1524340
    const v0, -0x12d4a606

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1524339
    const v0, 0x54d85b38

    return v0
.end method

.method public final j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524337
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    .line 1524338
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel$ReactionPaginatedComponentsModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    return-object v0
.end method
