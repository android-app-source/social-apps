.class public final Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0us;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5b9a2c1c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1525022
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1525021
    const-class v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1525019
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1525020
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1525016
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1525017
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1525018
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1525014
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->e:Ljava/lang/String;

    .line 1525015
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1525002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1525003
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1525004
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1525005
    invoke-virtual {p0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->p_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1525006
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1525007
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1525008
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1525009
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1525010
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1525011
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1525012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1525013
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1524999
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1525000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1525001
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1525023
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->f:Ljava/lang/String;

    .line 1525024
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1524995
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1524996
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->g:Z

    .line 1524997
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->h:Z

    .line 1524998
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1524992
    new-instance v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-direct {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;-><init>()V

    .line 1524993
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1524994
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1524990
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1524991
    iget-boolean v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->g:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1524988
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1524989
    iget-boolean v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1524987
    const v0, 0x4aeb0953    # 7701673.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1524986
    const v0, 0x370fbffd

    return v0
.end method

.method public final p_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1524984
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->i:Ljava/lang/String;

    .line 1524985
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->i:Ljava/lang/String;

    return-object v0
.end method
