.class public Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;
.super Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/AvailablePhotoCategoriesEnum;
    .end annotation
.end field

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1526073
    new-instance v0, LX/9h5;

    invoke-direct {v0}, LX/9h5;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1526051
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;-><init>()V

    .line 1526052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    .line 1526053
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    .line 1526054
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    .line 1526055
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AvailablePhotoCategoriesEnum;
        .end annotation
    .end param

    .prologue
    .line 1526068
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;-><init>()V

    .line 1526069
    iput-object p1, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    .line 1526070
    iput-object p2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    .line 1526071
    iput-object p3, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    .line 1526072
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1526067
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1526062
    if-ne p0, p1, :cond_1

    .line 1526063
    :cond_0
    :goto_0
    return v0

    .line 1526064
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1526065
    :cond_3
    check-cast p1, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;

    .line 1526066
    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1526061
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1526060
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "pageId"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "categoryName"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "entryPoint"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1526056
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526057
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1526058
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526059
    return-void
.end method
