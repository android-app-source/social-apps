.class public Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;
.super Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1526126
    new-instance v0, LX/9h8;

    invoke-direct {v0}, LX/9h8;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1526123
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;-><init>()V

    .line 1526124
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    .line 1526125
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1526127
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1526118
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 1526119
    :goto_0
    return v0

    .line 1526120
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1526121
    :cond_2
    check-cast p1, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    .line 1526122
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    iget-object v1, p1, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1526117
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1526116
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "ids"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1526114
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1526115
    return-void
.end method
