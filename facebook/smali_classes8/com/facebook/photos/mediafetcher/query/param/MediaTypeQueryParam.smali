.class public Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;
.super Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1526110
    new-instance v0, LX/9h7;

    invoke-direct {v0}, LX/9h7;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526106
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;-><init>()V

    .line 1526107
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    .line 1526108
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    .line 1526109
    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;
    .locals 2

    .prologue
    .line 1526105
    new-instance v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    const-string v1, "PHOTO"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1526104
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1526094
    if-ne p0, p1, :cond_1

    .line 1526095
    :cond_0
    :goto_0
    return v0

    .line 1526096
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1526097
    :cond_3
    check-cast p1, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    .line 1526098
    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1526103
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1526102
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mediaType"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1526099
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526100
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526101
    return-void
.end method
