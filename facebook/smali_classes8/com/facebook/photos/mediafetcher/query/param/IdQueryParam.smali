.class public Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;
.super Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1526077
    new-instance v0, LX/9h6;

    invoke-direct {v0}, LX/9h6;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526083
    invoke-direct {p0}, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;-><init>()V

    .line 1526084
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    .line 1526085
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1526082
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1526086
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 1526087
    :goto_0
    return v0

    .line 1526088
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1526089
    :cond_2
    check-cast p1, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 1526090
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1526081
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1526080
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1526078
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1526079
    return-void
.end method
