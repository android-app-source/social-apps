.class public Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<+",
            "LX/9g8;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1523063
    new-instance v0, LX/9g7;

    invoke-direct {v0}, LX/9g7;-><init>()V

    sput-object v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1523064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1523065
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    .line 1523066
    const-class v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    .line 1523067
    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<+",
            "LX/9g8;",
            ">;>;",
            "Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1523058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1523059
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    .line 1523060
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    iput-object v0, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    .line 1523061
    return-void
.end method

.method public static a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<QUERY:",
            "LX/9g8",
            "<TQUERY_PARAM;>;QUERY_PARAM:",
            "Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TQUERY;>;>;TQUERY_PARAM;)",
            "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;"
        }
    .end annotation

    .prologue
    .line 1523052
    new-instance v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-direct {v0, p0, p1}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;-><init>(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1523062
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1523053
    if-ne p0, p1, :cond_1

    .line 1523054
    :cond_0
    :goto_0
    return v0

    .line 1523055
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1523056
    :cond_3
    check-cast p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1523057
    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    iget-object v3, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1523051
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1523050
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mediaQueryProviderClass"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "queryParam"

    iget-object v2, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1523047
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1523048
    iget-object v0, p0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1523049
    return-void
.end method
