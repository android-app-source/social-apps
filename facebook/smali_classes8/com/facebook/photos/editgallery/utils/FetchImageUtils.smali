.class public Lcom/facebook/photos/editgallery/utils/FetchImageUtils;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:LX/1HI;

.field public final d:LX/1Er;

.field public final e:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1522696
    const-class v0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->a:Ljava/lang/String;

    .line 1522697
    const-class v0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/1Er;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522699
    iput-object p1, p0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->c:LX/1HI;

    .line 1522700
    iput-object p2, p0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->d:LX/1Er;

    .line 1522701
    iput-object p3, p0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->e:Ljava/util/concurrent/Executor;

    .line 1522702
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/editgallery/utils/FetchImageUtils;
    .locals 4

    .prologue
    .line 1522703
    new-instance v3, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v0

    check-cast v0, LX/1HI;

    invoke-static {p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v1

    check-cast v1, LX/1Er;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;-><init>(LX/1HI;LX/1Er;Ljava/util/concurrent/Executor;)V

    .line 1522704
    return-object v3
.end method

.method public static b(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 1522705
    const/4 v2, 0x0

    .line 1522706
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1522707
    :try_start_1
    invoke-static {p0, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1522708
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    return-void

    .line 1522709
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 1522710
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_0
    throw v0

    .line 1522711
    :catchall_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "LX/0Vd",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1522712
    invoke-static {p2}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1522713
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522714
    invoke-static {p2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1522715
    iget-object v1, p0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->c:LX/1HI;

    sget-object p1, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, p1}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1522716
    new-instance v1, LX/9fp;

    invoke-direct {v1, p0, p3}, LX/9fp;-><init>(Lcom/facebook/photos/editgallery/utils/FetchImageUtils;LX/0Vd;)V

    .line 1522717
    iget-object p1, p0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->e:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, p1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1522718
    :goto_0
    return-void

    .line 1522719
    :cond_0
    invoke-static {p2}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1522720
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V

    goto :goto_0

    .line 1522721
    :cond_1
    invoke-virtual {p3, p2}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "LX/0Vd",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1522722
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    const/4 v1, 0x0

    .line 1522723
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1522724
    invoke-static {v0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1522725
    iget-object v3, p0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->d:LX/1Er;

    const-string v4, "edit_gallery_fetch_image_temp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v3, v4, v0, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1522726
    invoke-static {v2, v0}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(Ljava/io/InputStream;Ljava/io/File;)V

    .line 1522727
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1522728
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1522729
    :cond_0
    :goto_0
    return-void

    .line 1522730
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1522731
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    .line 1522732
    new-instance v1, Ljava/lang/Throwable;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1522733
    :catch_2
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method
