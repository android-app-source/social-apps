.class public final Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9fm;


# direct methods
.method public constructor <init>(LX/9fm;)V
    .locals 0

    .prologue
    .line 1522625
    iput-object p1, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1522626
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1522627
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->g:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082429

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1522628
    :cond_0
    :goto_0
    return-void

    .line 1522629
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 1522630
    :goto_1
    if-nez v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->c:LX/8GT;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v1, v1, LX/9fm;->e:LX/9fn;

    iget-object v1, v1, LX/9fn;->a:Ljava/lang/String;

    const-string v2, "jpg"

    invoke-virtual {v0, v1, v2}, LX/8GT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object v7, v0

    .line 1522631
    :goto_2
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1522632
    invoke-static {v0}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    sget-object v1, LX/1ld;->a:LX/1lW;

    if-ne v0, v1, :cond_4

    .line 1522633
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v2, v2, LX/9fm;->c:Landroid/graphics/RectF;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/lang/String;)V

    .line 1522634
    :goto_3
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1522635
    iput-object v1, v0, LX/9fn;->h:Landroid/net/Uri;

    .line 1522636
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->d:LX/1HI;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v1, v1, LX/9fm;->e:LX/9fn;

    iget-object v1, v1, LX/9fn;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1522637
    :catch_0
    move-exception v0

    .line 1522638
    invoke-virtual {v0}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException;->printStackTrace()V

    .line 1522639
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1522640
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->h:Landroid/net/Uri;

    invoke-static {v0}, LX/8GT;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 1522641
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1522642
    :cond_3
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v7, v1

    goto :goto_2

    .line 1522643
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->f:Lcom/facebook/bitmaps/NativeImageProcessor;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v1, v1, LX/9fm;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v2, v2, LX/9fm;->d:LX/434;

    iget v2, v2, LX/434;->b:I

    iget-object v3, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v3, v3, LX/9fm;->d:LX/434;

    iget v3, v3, LX/434;->a:I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v5, v5, LX/9fm;->c:Landroid/graphics/RectF;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;IIILandroid/graphics/RectF;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 1522644
    :catch_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1522645
    iget-object v0, p0, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;->a:LX/9fm;

    iget-object v0, v0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->h:Landroid/net/Uri;

    invoke-static {v0}, LX/8GT;->a(Landroid/net/Uri;)V

    goto/16 :goto_0
.end method
