.class public Lcom/facebook/photos/editgallery/TextEditController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9eb;
.implements LX/9f8;


# instance fields
.field private final a:LX/9de;

.field public final b:LX/9fH;

.field public final c:Landroid/content/Context;

.field private final d:LX/8GT;

.field public final e:LX/8Gc;

.field private final f:Landroid/widget/FrameLayout;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

.field private final i:LX/1Ad;

.field private final j:LX/9fP;

.field private final k:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public final l:Ljava/lang/String;

.field public m:Z

.field public n:LX/9e1;

.field public o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

.field private p:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

.field public q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

.field private r:Landroid/net/Uri;

.field private s:Z

.field private t:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/9fY;

.field private v:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/9f8;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Ljava/lang/String;Landroid/widget/FrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/9fP;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/9f8;LX/0am;LX/1Ad;Landroid/content/Context;LX/8GT;LX/8Gc;)V
    .locals 6
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/ui/titlebar/Fb4aTitleBar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/9fP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/photos/editgallery/EditGalleryFragmentController;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/9f8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/ui/titlebar/Fb4aTitleBar;",
            "Ljava/lang/String;",
            "Landroid/widget/FrameLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Ljava/lang/String;",
            "LX/9fP;",
            "Lcom/facebook/photos/editgallery/RotatingPhotoViewController;",
            "LX/9f8;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "LX/1Ad;",
            "Landroid/content/Context;",
            "LX/8GT;",
            "LX/8Gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522261
    new-instance v1, LX/9fW;

    invoke-direct {v1, p0}, LX/9fW;-><init>(Lcom/facebook/photos/editgallery/TextEditController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->a:LX/9de;

    .line 1522262
    new-instance v1, LX/9fX;

    invoke-direct {v1, p0}, LX/9fX;-><init>(Lcom/facebook/photos/editgallery/TextEditController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->b:LX/9fH;

    .line 1522263
    new-instance v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522264
    iput-object p1, p0, Lcom/facebook/photos/editgallery/TextEditController;->r:Landroid/net/Uri;

    .line 1522265
    iput-object p2, p0, Lcom/facebook/photos/editgallery/TextEditController;->k:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1522266
    iput-object p3, p0, Lcom/facebook/photos/editgallery/TextEditController;->l:Ljava/lang/String;

    .line 1522267
    iput-object p4, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    .line 1522268
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->c:Landroid/content/Context;

    .line 1522269
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->i:LX/1Ad;

    .line 1522270
    iput-object p8, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1522271
    iput-object p6, p0, Lcom/facebook/photos/editgallery/TextEditController;->g:Ljava/lang/String;

    .line 1522272
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->d:LX/8GT;

    .line 1522273
    iput-object p7, p0, Lcom/facebook/photos/editgallery/TextEditController;->j:LX/9fP;

    .line 1522274
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->e:LX/8Gc;

    .line 1522275
    new-instance v1, LX/9e1;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/9e1;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    .line 1522276
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v1}, LX/9e1;->a()V

    .line 1522277
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1522278
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->a:LX/9de;

    invoke-virtual {v1, v2}, LX/9e1;->setCallBack(LX/9de;)V

    .line 1522279
    iput-object p5, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1522280
    sget-object v1, LX/9fY;->TEXT_EDIT:LX/9fY;

    iput-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->u:LX/9fY;

    .line 1522281
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->v:LX/0am;

    .line 1522282
    iput-object p9, p0, Lcom/facebook/photos/editgallery/TextEditController;->w:LX/9f8;

    .line 1522283
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V
    .locals 5

    .prologue
    .line 1522202
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->e:LX/8Gc;

    invoke-virtual {v0}, LX/8Gc;->a()V

    .line 1522203
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1522204
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1522205
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v3

    .line 1522206
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v1, 0x7f0b0c1d

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    move v2, v3

    .line 1522207
    move v1, v2

    .line 1522208
    sub-int/2addr v1, p1

    .line 1522209
    iget-object v2, v0, LX/9e1;->h:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v2, v1}, LX/46y;->a(Landroid/view/View;I)V

    .line 1522210
    iget-object v2, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, LX/9e1;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0c04

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setMaxHeight(I)V

    .line 1522211
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->requestLayout()V

    .line 1522212
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    new-instance v1, Lcom/facebook/photos/editgallery/TextEditController$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/photos/editgallery/TextEditController$3;-><init>(Lcom/facebook/photos/editgallery/TextEditController;I)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, LX/9e1;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1522213
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/editgallery/TextEditController;Lcom/facebook/photos/creativeediting/model/TextParams;)V
    .locals 8
    .param p0    # Lcom/facebook/photos/editgallery/TextEditController;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 1522214
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1522215
    if-eqz p1, :cond_0

    .line 1522216
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, p1}, LX/9dl;->b(LX/362;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1522217
    :cond_0
    invoke-static {p0, v7}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V

    .line 1522218
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->a()V

    .line 1522219
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 1522220
    :goto_0
    return-void

    .line 1522221
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    .line 1522222
    iget-object v2, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->clearComposingText()V

    .line 1522223
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->d:LX/8GT;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->g:Ljava/lang/String;

    const-string v3, ".png"

    invoke-virtual {v0, v2, v3}, LX/8GT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1522224
    if-nez v0, :cond_2

    .line 1522225
    invoke-static {p0, v7}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V

    .line 1522226
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->a()V

    .line 1522227
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    goto :goto_0

    .line 1522228
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    .line 1522229
    iget-object v3, v2, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->a()V

    .line 1522230
    iget-object v3, v2, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v3, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->a(Ljava/io/File;)V

    .line 1522231
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1522232
    if-nez p1, :cond_4

    .line 1522233
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v2}, LX/9e1;->getText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v3}, LX/9e1;->getTextWidth()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v4}, LX/9e1;->getTextHeight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v5}, LX/9e1;->getTextColor()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Landroid/net/Uri;Ljava/lang/String;IIILjava/lang/String;)V

    .line 1522234
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522235
    iget v2, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->g:I

    .line 1522236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522237
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    .line 1522238
    iget-boolean v2, v0, LX/9e1;->i:Z

    move v0, v2

    .line 1522239
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->getTextColor()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 1522240
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->a:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1522241
    :cond_3
    invoke-static {p0, v7}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V

    .line 1522242
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->a()V

    .line 1522243
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    goto :goto_0

    .line 1522244
    :cond_4
    :try_start_3
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->j()LX/362;

    .line 1522245
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v2}, LX/9e1;->getText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v3}, LX/9e1;->getTextWidth()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v4}, LX/9e1;->getTextHeight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v5}, LX/9e1;->getTextColor()I

    move-result v5

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Landroid/net/Uri;Ljava/lang/String;IIILcom/facebook/photos/creativeediting/model/TextParams;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1522246
    :catch_0
    if-eqz v1, :cond_5

    .line 1522247
    :try_start_4
    invoke-static {v1}, LX/8GT;->a(Landroid/net/Uri;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1522248
    :cond_5
    invoke-static {p0, v7}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V

    .line 1522249
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->a()V

    .line 1522250
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    goto/16 :goto_0

    .line 1522251
    :catchall_0
    move-exception v0

    invoke-static {p0, v7}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V

    .line 1522252
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v1}, LX/9e1;->a()V

    .line 1522253
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->invalidate()V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/photos/editgallery/TextEditController;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1522254
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 1522255
    iput-object p1, v0, LX/108;->g:Ljava/lang/String;

    .line 1522256
    move-object v0, v0

    .line 1522257
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1522258
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->k:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1522259
    return-void
.end method

.method private p()V
    .locals 1

    .prologue
    .line 1522168
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->t:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->s:Z

    if-nez v0, :cond_1

    .line 1522169
    :cond_0
    :goto_0
    return-void

    .line 1522170
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->a()V

    goto :goto_0
.end method

.method private q()V
    .locals 7

    .prologue
    .line 1522171
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->p:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522172
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1522173
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1522174
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    .line 1522175
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1522176
    iget v4, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    move v3, v4

    .line 1522177
    int-to-float v3, v3

    invoke-static {v1}, LX/63w;->a(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1522178
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1522179
    iget v5, v4, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    move v4, v5

    .line 1522180
    int-to-float v4, v4

    invoke-static {v1}, LX/63w;->b(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v1

    mul-float/2addr v1, v4

    float-to-int v4, v1

    .line 1522181
    :goto_2
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->j:LX/9fP;

    invoke-virtual {p0}, Lcom/facebook/photos/editgallery/TextEditController;->k()LX/9ek;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-virtual {v6, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v1, v0, v5, v6}, LX/9fP;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/9ek;I)LX/8Hx;

    move-result-object v5

    .line 1522182
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->i:LX/1Ad;

    .line 1522183
    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v6

    new-instance p0, LX/1o9;

    invoke-direct {p0, v3, v4}, LX/1o9;-><init>(II)V

    .line 1522184
    iput-object p0, v6, LX/1bX;->c:LX/1o9;

    .line 1522185
    move-object v6, v6

    .line 1522186
    iput-object v5, v6, LX/1bX;->j:LX/33B;

    .line 1522187
    move-object v6, v6

    .line 1522188
    invoke-virtual {v6}, LX/1bX;->n()LX/1bf;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v6

    check-cast v6, LX/1Ad;

    const-class p0, LX/9e1;

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    invoke-virtual {v6, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v6

    invoke-virtual {v6}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v6

    .line 1522189
    iget-object p0, v0, LX/9e1;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v6}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1522190
    return-void

    .line 1522191
    :cond_0
    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->r:Landroid/net/Uri;

    goto :goto_0

    .line 1522192
    :cond_1
    iget-object v3, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1522193
    iget v4, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    move v3, v4

    .line 1522194
    goto :goto_1

    .line 1522195
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1522196
    iget v4, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    move v4, v4

    .line 1522197
    goto :goto_2
.end method

.method public static r(Lcom/facebook/photos/editgallery/TextEditController;)V
    .locals 4

    .prologue
    .line 1522284
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/9e1;->setAlpha(F)V

    .line 1522285
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->bringToFront()V

    .line 1522286
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1522287
    invoke-virtual {v0}, LX/9e1;->bringToFront()V

    .line 1522288
    iget-object v1, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setVisibility(I)V

    .line 1522289
    iget-object v1, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->d()V

    .line 1522290
    invoke-virtual {v0, v2}, LX/9e1;->setVisibility(I)V

    .line 1522291
    iget-object v1, v0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 1522292
    iget-object v1, v0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v1, v3}, Lcom/facebook/messaging/doodle/ColourIndicator;->setEnabled(Z)V

    .line 1522293
    iget-object v1, v0, LX/9e1;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 1522294
    iget-object v1, v0, LX/9e1;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v1, v3}, Lcom/facebook/messaging/doodle/ColourPicker;->setEnabled(Z)V

    .line 1522295
    iget-object v1, v0, LX/9e1;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1522296
    iget-object v1, v0, LX/9e1;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1522297
    iget-object v1, v0, LX/9e1;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1522298
    sget-object v0, LX/9fY;->TEXT_ENTRY:LX/9fY;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->u:LX/9fY;

    .line 1522299
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1522300
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082413

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5i7;)V
    .locals 1
    .param p1    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522301
    if-eqz p1, :cond_0

    sget-object v0, LX/5i7;->TEXT:LX/5i7;

    if-ne p1, v0, :cond_0

    .line 1522302
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522303
    iget p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->h:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->h:I

    .line 1522304
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1522305
    iput-object p1, p0, Lcom/facebook/photos/editgallery/TextEditController;->t:Landroid/graphics/Rect;

    .line 1522306
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/TextEditController;->p()V

    .line 1522307
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 3

    .prologue
    .line 1522308
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1522309
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522310
    iput-object p1, p0, Lcom/facebook/photos/editgallery/TextEditController;->p:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522311
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const v1, 0x7f0219da

    const v2, 0x7f0813c2

    const p1, 0x7f0813bf

    invoke-virtual {v0, v1, v2, p1}, LX/9dl;->a(III)V

    .line 1522312
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->b:LX/9fH;

    .line 1522313
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    .line 1522314
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->e()V

    .line 1522315
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/TextEditController;->q()V

    .line 1522316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->s:Z

    .line 1522317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522318
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/TextEditController;->p()V

    .line 1522319
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V
    .locals 1

    .prologue
    .line 1522320
    iget v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->a:I

    .line 1522321
    return-void
.end method

.method public final a(Ljava/lang/String;LX/5i7;)V
    .locals 1
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522322
    if-eqz p2, :cond_0

    sget-object v0, LX/5i7;->TEXT:LX/5i7;

    if-ne p2, v0, :cond_0

    .line 1522323
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522324
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522325
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1522326
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1522327
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1522328
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    iput-boolean p1, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->b:Z

    .line 1522329
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1522330
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c7;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522331
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/9c4;->COMPOSER_TEXT_ON_PHOTOS:LX/9c4;

    invoke-virtual {v3}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "composer"

    .line 1522332
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1522333
    move-object v2, v2

    .line 1522334
    sget-object v3, LX/9c5;->NUM_TEXT_ADDED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    .line 1522335
    iget p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->g:I

    iget-object p1, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p0, p1

    move p0, p0

    .line 1522336
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->NUM_TEXT_REMOVED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    .line 1522337
    iget p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->h:I

    move p0, p0

    .line 1522338
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->NUM_TEXT_RESIZED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    .line 1522339
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move p0, p0

    .line 1522340
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->NUM_TEXT_MOVED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    .line 1522341
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move p0, p0

    .line 1522342
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->NUM_TEXT_ROTATED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    .line 1522343
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move p0, p0

    .line 1522344
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->NUM_TEXT_EDITED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    .line 1522345
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move p0, p0

    .line 1522346
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->USED_COLOR_PICKER:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget-boolean p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->a:Z

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/9c5;->ACCEPTED:LX/9c5;

    invoke-virtual {v3}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget-boolean p0, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->b:Z

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1522347
    invoke-static {v0, v2}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1522348
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1522198
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->b()V

    .line 1522199
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->i()V

    .line 1522200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->s:Z

    .line 1522201
    return-void
.end method

.method public final b(Ljava/lang/String;LX/5i7;)V
    .locals 1
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522127
    if-eqz p2, :cond_0

    sget-object v0, LX/5i7;->TEXT:LX/5i7;

    if-ne p2, v0, :cond_0

    .line 1522128
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522129
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522130
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1522131
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1522132
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1522133
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->b:LX/9fH;

    invoke-interface {v0}, LX/9fH;->a()V

    .line 1522134
    return-void
.end method

.method public final c(Ljava/lang/String;LX/5i7;)V
    .locals 1
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522135
    if-eqz p2, :cond_0

    sget-object v0, LX/5i7;->TEXT:LX/5i7;

    if-ne p2, v0, :cond_0

    .line 1522136
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522137
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522138
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1522139
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1522140
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1522141
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1522142
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->u:LX/9fY;

    sget-object v1, LX/9fY;->TEXT_ENTRY:LX/9fY;

    if-ne v0, v1, :cond_0

    .line 1522143
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    .line 1522144
    iget-object v1, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->b()V

    .line 1522145
    const/4 v0, 0x1

    .line 1522146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1522147
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1522148
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v0}, LX/9e1;->a()V

    .line 1522149
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1522150
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1522151
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setAlpha(F)V

    .line 1522152
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1522153
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->e:LX/8Gc;

    invoke-virtual {v0}, LX/8Gc;->a()V

    .line 1522154
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->e:LX/8Gc;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, v1, v2}, LX/8Gc;->a(Landroid/view/View;I)V

    .line 1522155
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setActionButtonEnabled(Z)V

    .line 1522156
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1522157
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1522158
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setActionButtonEnabled(Z)V

    .line 1522159
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1522160
    sget-object v0, LX/5Rr;->TEXT:LX/5Rr;

    return-object v0
.end method

.method public final k()LX/9ek;
    .locals 1

    .prologue
    .line 1522161
    sget-object v0, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1522162
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    return v0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
    .locals 4

    .prologue
    .line 1522163
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->p:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/TextEditController;->p:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522164
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v2

    .line 1522165
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const-class v3, Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-virtual {v2, v3}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Ljava/lang/Class;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setTextParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1522166
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522167
    iget-object v0, p0, Lcom/facebook/photos/editgallery/TextEditController;->p:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    return-object v0
.end method
