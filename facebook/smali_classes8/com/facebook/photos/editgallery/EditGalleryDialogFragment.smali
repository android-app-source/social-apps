.class public Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field private A:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

.field private C:Landroid/view/ViewStub;

.field private D:Landroid/view/ViewStub;

.field private E:Landroid/view/View;

.field public F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private G:Landroid/view/View;

.field private H:Landroid/view/ViewStub;

.field private I:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;",
            ">;"
        }
    .end annotation
.end field

.field public J:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field public K:LX/9fh;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

.field public m:LX/9fB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/net/Uri;

.field public p:I

.field public q:I

.field public r:LX/9el;

.field public s:Z

.field public t:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

.field private w:I

.field private x:Landroid/widget/LinearLayout;

.field public y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

.field private z:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1520560
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1520561
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->w:I

    .line 1520562
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->I:LX/0am;

    return-void
.end method

.method private a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 8

    .prologue
    .line 1520563
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->s:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1520564
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m:LX/9fB;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->r:LX/9el;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->o:Landroid/net/Uri;

    iget v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->K:LX/9fh;

    move-object v1, p0

    move-object v6, p1

    invoke-virtual/range {v0 .. v7}, LX/9fB;->a(Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;LX/9el;Landroid/net/Uri;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;LX/9fh;)Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520565
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520566
    sget-object v1, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v1}, LX/5iL;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1520567
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->i:Landroid/view/View$OnLayoutChangeListener;

    .line 1520568
    :goto_0
    move-object v0, v1

    .line 1520569
    if-eqz v0, :cond_0

    .line 1520570
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->G:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1520571
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520572
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1520573
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v1, v2

    .line 1520574
    new-instance v2, LX/9es;

    invoke-direct {v2, v0}, LX/9es;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1520575
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    const v4, 0x7f0813d7

    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1520576
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 1520577
    move-object v2, v2

    .line 1520578
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1520579
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1520580
    new-instance v2, LX/9et;

    invoke-direct {v2, v0}, LX/9et;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1520581
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520582
    iget-object v1, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1520583
    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Ljava/lang/String;)V

    .line 1520584
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1520585
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520586
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->g:Landroid/content/DialogInterface$OnKeyListener;

    move-object v1, v2

    .line 1520587
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1520588
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    const-class v1, LX/9fB;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9fB;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m:LX/9fB;

    iput-object p0, p1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->n:LX/0ad;

    return-void
.end method

.method private y()I
    .locals 4

    .prologue
    .line 1520589
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c0d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1520590
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-virtual {v1}, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e()Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->n:LX/0ad;

    sget-short v2, LX/8Fn;->b:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1520591
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0c13

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1520592
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0c0b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0c0c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static z(Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1520593
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520594
    iget-object p0, v1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->l:LX/0Px;

    move-object v1, p0

    .line 1520595
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
    .locals 1

    .prologue
    .line 1520596
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    return-object v0
.end method

.method public final b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1520607
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public final k()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1520597
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->x:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final l()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 1520598
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->z:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
    .locals 1

    .prologue
    .line 1520599
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    return-object v0
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 1520600
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->A:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1520601
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->A:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->bringToFront()V

    .line 1520602
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x2a2032df

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1520603
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1520604
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->r:LX/9el;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1520605
    const v0, 0x597f990f

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1520606
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2551cc1c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520537
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1520538
    const-class v1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1520539
    const v1, 0x7f0e09a5

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1520540
    const/16 v1, 0x2b

    const v2, 0x3a8d8c0a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x2221276f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1520541
    const v0, 0x7f03044a

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1520542
    const v0, 0x7f0d0d0c

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->B:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    .line 1520543
    const v0, 0x7f0d0d10

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1520544
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1520545
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y()I

    move-result v3

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 1520546
    const v0, 0x7f0d0d0e

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1520547
    const v0, 0x7f0d0d03

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->x:Landroid/widget/LinearLayout;

    .line 1520548
    const v0, 0x7f0d0d11

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1520549
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->b()V

    .line 1520550
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, v4}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1520551
    const v0, 0x7f0d0bbc

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->z:Landroid/widget/FrameLayout;

    .line 1520552
    new-instance v3, LX/0zw;

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->z:Landroid/widget/FrameLayout;

    const v4, 0x7f0d07c5

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->A:LX/0zw;

    .line 1520553
    const v0, 0x7f0d0d13

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->C:Landroid/view/ViewStub;

    .line 1520554
    const v0, 0x7f0d0d12

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->D:Landroid/view/ViewStub;

    .line 1520555
    const v0, 0x7f0d0d0d

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->E:Landroid/view/View;

    .line 1520556
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->E:Landroid/view/View;

    const v3, 0x7f0d00bc

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1520557
    const v0, 0x7f0d0d0f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->H:Landroid/view/ViewStub;

    .line 1520558
    iput-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->G:Landroid/view/View;

    .line 1520559
    const/16 v0, 0x2b

    const v3, -0x67cd9324

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x78229629

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520437
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520438
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v2}, LX/9ea;->g()V

    .line 1520439
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->R:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    iget-object v4, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Q:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1520440
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p()V

    .line 1520441
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    if-eqz v2, :cond_0

    .line 1520442
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v4, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->c:LX/9cr;

    invoke-virtual {v2, v4}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->b(LX/9cr;)V

    .line 1520443
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1520444
    const/16 v1, 0x2b

    const v2, -0x6b0c35af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4c6a30ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520445
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1520446
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520447
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9fk;

    .line 1520448
    iget-object v1, v2, LX/9fk;->b:LX/9ea;

    move-object v2, v1

    .line 1520449
    invoke-interface {v2}, LX/9ea;->f()V

    goto :goto_0

    .line 1520450
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1787deb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1520451
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1520452
    const-string v0, "edit_gallery_bitmap_width"

    iget v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1520453
    const-string v0, "edit_gallery_bitmap_height"

    iget v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1520454
    const-string v0, "edit_gallery_photo_uri"

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->o:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1520455
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    if-eqz v0, :cond_0

    .line 1520456
    const-string v0, "edit_gallery_controller_state"

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520457
    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->q(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520458
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    move-object v1, v2

    .line 1520459
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1520460
    :cond_0
    const-string v0, "edit_gallery_launch_configuration"

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1520461
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x66e85e8e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520462
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1520463
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->w:I

    .line 1520464
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1520465
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1520466
    const/16 v1, 0x2b

    const v2, 0x3b71bcc5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x41e893dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520467
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1520468
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->w:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1520469
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStop()V

    .line 1520470
    const/16 v1, 0x2b

    const v2, 0x2437c3d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x1aef5b12

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v12

    .line 1520471
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 1520472
    new-instance v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->t:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-direct {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;-><init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1520473
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    if-eqz v1, :cond_0

    .line 1520474
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-virtual {v1}, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e()Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1520475
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520476
    iget-object v2, v1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->c:LX/5Rr;

    move-object v1, v2

    .line 1520477
    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520478
    iget-object v3, v2, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e:LX/5Rq;

    move-object v2, v3

    .line 1520479
    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-virtual {v3}, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520480
    iget-boolean v5, v4, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->f:Z

    move v4, v5

    .line 1520481
    iget-object v5, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520482
    iget-boolean v6, v5, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->g:Z

    move v5, v6

    .line 1520483
    iget-object v6, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520484
    iget-object v7, v6, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    move-object v6, v7

    .line 1520485
    iget-object v7, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520486
    iget-object v8, v7, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->j:Ljava/lang/String;

    move-object v7, v8

    .line 1520487
    if-nez v7, :cond_3

    const v7, 0x7f0813d7

    invoke-virtual {p0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_1
    iget-object v8, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520488
    iget-boolean v9, v8, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->i:Z

    move v8, v9

    .line 1520489
    iget-object v9, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520490
    iget-object v10, v9, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->l:LX/0Px;

    move-object v9, v10

    .line 1520491
    iget-object v10, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520492
    iget-object v11, v10, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-object v10, v11

    .line 1520493
    iget-object v11, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520494
    iget-boolean v13, v11, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->n:Z

    move v11, v13

    .line 1520495
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->i:LX/5Rr;

    .line 1520496
    iput-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->e:LX/5Rq;

    .line 1520497
    iget-object v13, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->d:Ljava/util/List;

    invoke-interface {v13, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1520498
    iput-boolean v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    .line 1520499
    iput-boolean v5, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->g:Z

    .line 1520500
    iput-object v6, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    .line 1520501
    iput-object v7, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    .line 1520502
    iput-boolean v8, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    .line 1520503
    iput-object v9, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    .line 1520504
    iput-object v10, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1520505
    iput-boolean v11, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->r:Z

    .line 1520506
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->J:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 1520507
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->J:Ljava/util/List;

    .line 1520508
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    .line 1520509
    :cond_1
    if-eqz p1, :cond_5

    .line 1520510
    const-string v1, "edit_gallery_bitmap_width"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p:I

    .line 1520511
    const-string v1, "edit_gallery_bitmap_height"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->q:I

    .line 1520512
    const-string v1, "edit_gallery_photo_uri"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->o:Landroid/net/Uri;

    .line 1520513
    const-string v1, "edit_gallery_controller_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1520514
    const-string v0, "edit_gallery_controller_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    move-object v1, v0

    .line 1520515
    :goto_2
    const-string v0, "edit_gallery_launch_configuration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520516
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->s:Z

    .line 1520517
    :goto_3
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    if-eqz v0, :cond_2

    .line 1520518
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1520519
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y()I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 1520520
    :cond_2
    invoke-direct {p0, v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V

    .line 1520521
    const v0, -0x5bd4bbe8

    invoke-static {v0, v12}, LX/02F;->f(II)V

    return-void

    .line 1520522
    :cond_3
    iget-object v7, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520523
    iget-object v8, v7, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->j:Ljava/lang/String;

    move-object v7, v8

    .line 1520524
    goto/16 :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_3

    .line 1520525
    :cond_6
    new-instance v2, LX/5Rw;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-direct {v2, v1}, LX/5Rw;-><init>(Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)V

    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->z(Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;)Z

    move-result v1

    if-nez v1, :cond_7

    sget-object v1, LX/5Rr;->FILTER:LX/5Rr;

    :goto_4
    invoke-virtual {v2, v1}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v1

    invoke-virtual {v1}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 1520526
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->A:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520527
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->A:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1520528
    :cond_0
    return-void
.end method

.method public final r()Lcom/facebook/photos/creativeediting/RotatingFrameLayout;
    .locals 1

    .prologue
    .line 1520529
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->B:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    return-object v0
.end method

.method public final s()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1520530
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->C:Landroid/view/ViewStub;

    return-object v0
.end method

.method public final t()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1520531
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->D:Landroid/view/ViewStub;

    return-object v0
.end method

.method public final u()Landroid/view/View;
    .locals 1

    .prologue
    .line 1520532
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->E:Landroid/view/View;

    return-object v0
.end method

.method public final v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;
    .locals 1

    .prologue
    .line 1520533
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    return-object v0
.end method

.method public final w()Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;
    .locals 1

    .prologue
    .line 1520534
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->I:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1520535
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->H:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->I:LX/0am;

    .line 1520536
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->I:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    return-object v0
.end method
