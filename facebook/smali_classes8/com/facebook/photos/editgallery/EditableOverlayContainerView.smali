.class public Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
.super LX/9dl;
.source ""


# instance fields
.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/9ds;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/9cH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/8GY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0wW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final j:LX/0xi;

.field private final k:LX/9fG;

.field public l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

.field public m:LX/9fH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/9dr;

.field private o:LX/0wd;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1521511
    invoke-direct {p0, p1}, LX/9dl;-><init>(Landroid/content/Context;)V

    .line 1521512
    new-instance v0, LX/9fF;

    invoke-direct {v0, p0}, LX/9fF;-><init>(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->j:LX/0xi;

    .line 1521513
    new-instance v0, LX/9fG;

    invoke-direct {v0, p0}, LX/9fG;-><init>(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->k:LX/9fG;

    .line 1521514
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->n()V

    .line 1521515
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1521516
    invoke-direct {p0, p1, p2}, LX/9dl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1521517
    new-instance v0, LX/9fF;

    invoke-direct {v0, p0}, LX/9fF;-><init>(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->j:LX/0xi;

    .line 1521518
    new-instance v0, LX/9fG;

    invoke-direct {v0, p0}, LX/9fG;-><init>(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->k:LX/9fG;

    .line 1521519
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->n()V

    .line 1521520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1521521
    invoke-direct {p0, p1, p2, p3}, LX/9dl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1521522
    new-instance v0, LX/9fF;

    invoke-direct {v0, p0}, LX/9fF;-><init>(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->j:LX/0xi;

    .line 1521523
    new-instance v0, LX/9fG;

    invoke-direct {v0, p0}, LX/9fG;-><init>(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->k:LX/9fG;

    .line 1521524
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->n()V

    .line 1521525
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;IIIFFFLjava/lang/String;)V
    .locals 4
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1521526
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521527
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521528
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0, p3, p4}, LX/8GY;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    .line 1521529
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1521530
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 1521531
    new-instance v2, LX/5jN;

    invoke-direct {v2, p2, p1}, LX/5jN;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1521532
    iput p6, v2, LX/5jN;->c:F

    .line 1521533
    move-object v2, v2

    .line 1521534
    iput p7, v2, LX/5jN;->d:F

    .line 1521535
    move-object v2, v2

    .line 1521536
    iput v1, v2, LX/5jN;->e:F

    .line 1521537
    move-object v1, v2

    .line 1521538
    iput v0, v1, LX/5jN;->f:F

    .line 1521539
    move-object v0, v1

    .line 1521540
    iput p5, v0, LX/5jN;->h:I

    .line 1521541
    move-object v0, v0

    .line 1521542
    iput p8, v0, LX/5jN;->g:F

    .line 1521543
    move-object v0, v0

    .line 1521544
    iput-object p9, v0, LX/5jN;->i:Ljava/lang/String;

    .line 1521545
    move-object v0, v0

    .line 1521546
    invoke-virtual {v0}, LX/5jN;->a()Lcom/facebook/photos/creativeediting/model/TextParams;

    move-result-object v0

    .line 1521547
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1521548
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(LX/362;)V

    .line 1521549
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1521550
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1521551
    invoke-virtual {p0}, LX/9dl;->h()V

    .line 1521552
    return-void
.end method

.method private static a(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;LX/0Or;LX/9ds;LX/9cH;LX/8GY;LX/0wW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/9ds;",
            "LX/9cH;",
            "LX/8GY;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1521553
    iput-object p1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->e:LX/0Or;

    iput-object p2, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->f:LX/9ds;

    iput-object p3, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->g:LX/9cH;

    iput-object p4, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->h:LX/8GY;

    iput-object p5, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->i:LX/0wW;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/16 v1, 0x259

    invoke-static {v5, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const-class v2, LX/9ds;

    invoke-interface {v5, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9ds;

    const-class v3, LX/9cH;

    invoke-interface {v5, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/9cH;

    invoke-static {v5}, LX/8GY;->b(LX/0QB;)LX/8GY;

    move-result-object v4

    check-cast v4, LX/8GY;

    invoke-static {v5}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v5

    check-cast v5, LX/0wW;

    invoke-static/range {v0 .. v5}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Lcom/facebook/photos/editgallery/EditableOverlayContainerView;LX/0Or;LX/9ds;LX/9cH;LX/8GY;LX/0wW;)V

    return-void
.end method

.method public static c(LX/362;)LX/5i7;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1521554
    if-nez p0, :cond_1

    .line 1521555
    :cond_0
    :goto_0
    return-object v0

    .line 1521556
    :cond_1
    instance-of v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v1, :cond_2

    .line 1521557
    sget-object v0, LX/5i7;->TEXT:LX/5i7;

    goto :goto_0

    .line 1521558
    :cond_2
    instance-of v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-eqz v1, :cond_3

    .line 1521559
    sget-object v0, LX/5i7;->STICKER:LX/5i7;

    goto :goto_0

    .line 1521560
    :cond_3
    instance-of v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v1, :cond_0

    .line 1521561
    sget-object v0, LX/5i7;->DOODLE:LX/5i7;

    goto :goto_0
.end method

.method private c(Z)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<+",
            "LX/362;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1521562
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i()LX/0Px;

    move-result-object v3

    .line 1521563
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1521564
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5i8;

    .line 1521565
    invoke-interface {v0}, LX/5i8;->j()Z

    move-result v6

    if-ne v6, p1, :cond_0

    .line 1521566
    const/4 v6, 0x1

    new-array v6, v6, [LX/5i8;

    aput-object v0, v6, v2

    invoke-virtual {v4, v6}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1521567
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1521568
    :cond_1
    iget-object v0, p0, LX/9dl;->a:LX/8GZ;

    move-object v0, v0

    .line 1521569
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8GZ;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 6

    .prologue
    .line 1521570
    const-class v0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-static {v0, p0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1521571
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->g:LX/9cH;

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/9cH;->a(Landroid/graphics/Rect;)Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1521572
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->f:LX/9ds;

    iget-object v1, p0, LX/9dl;->b:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1521573
    new-instance v4, LX/9dr;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-direct {v4, v1, v2, p0, v3}, LX/9dr;-><init>(Landroid/widget/ImageView;Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;LX/9dl;LX/0wW;)V

    .line 1521574
    move-object v0, v4

    .line 1521575
    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->n:LX/9dr;

    .line 1521576
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->k:LX/9fG;

    .line 1521577
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    .line 1521578
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->i:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x0

    .line 1521579
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1521580
    move-object v0, v0

    .line 1521581
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->j:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    .line 1521582
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1521583
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(Z)Ljava/util/List;

    move-result-object v0

    .line 1521584
    if-nez v0, :cond_0

    .line 1521585
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1521586
    :goto_0
    return-object v0

    .line 1521587
    :cond_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1521588
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1521589
    invoke-virtual {p1, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1521590
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1521591
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;IIFLjava/lang/String;)Lcom/facebook/photos/creativeediting/model/DoodleParams;
    .locals 5
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1521592
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521593
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521594
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0, p2, p3}, LX/8GY;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    .line 1521595
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1521596
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1521597
    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1521598
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    .line 1521599
    new-instance v4, LX/5iF;

    invoke-direct {v4, p1}, LX/5iF;-><init>(Landroid/net/Uri;)V

    .line 1521600
    iput v3, v4, LX/5iF;->b:F

    .line 1521601
    move-object v3, v4

    .line 1521602
    iput v0, v3, LX/5iF;->c:F

    .line 1521603
    move-object v0, v3

    .line 1521604
    iput v1, v0, LX/5iF;->d:F

    .line 1521605
    move-object v0, v0

    .line 1521606
    iput v2, v0, LX/5iF;->e:F

    .line 1521607
    move-object v0, v0

    .line 1521608
    iput p4, v0, LX/5iF;->f:F

    .line 1521609
    move-object v0, v0

    .line 1521610
    iput-object p5, v0, LX/5iF;->g:Ljava/lang/String;

    .line 1521611
    move-object v0, v0

    .line 1521612
    invoke-virtual {v0}, LX/5iF;->a()Lcom/facebook/photos/creativeediting/model/DoodleParams;

    move-result-object v0

    .line 1521613
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1521614
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1521615
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    if-eqz v0, :cond_0

    .line 1521616
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    invoke-interface {v0}, LX/9fH;->a()V

    .line 1521617
    :cond_0
    return-void
.end method

.method public final a(LX/362;)V
    .locals 1

    .prologue
    .line 1521476
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521477
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    if-eqz v0, :cond_0

    .line 1521478
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    invoke-interface {v0, p1}, LX/9fH;->a(LX/362;)V

    .line 1521479
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;IIILcom/facebook/photos/creativeediting/model/TextParams;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 1521480
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521481
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521482
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0, p3, p4}, LX/8GY;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    .line 1521483
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1521484
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 1521485
    invoke-virtual {p6}, Lcom/facebook/photos/creativeediting/model/TextParams;->n()F

    move-result v2

    invoke-virtual {p6}, Lcom/facebook/photos/creativeediting/model/TextParams;->e()F

    move-result v3

    sub-float/2addr v3, v1

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    .line 1521486
    invoke-virtual {p6}, Lcom/facebook/photos/creativeediting/model/TextParams;->o()F

    move-result v3

    invoke-virtual {p6}, Lcom/facebook/photos/creativeediting/model/TextParams;->f()F

    move-result v4

    sub-float/2addr v4, v0

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 1521487
    new-instance v4, LX/5jN;

    invoke-direct {v4, p2, p1}, LX/5jN;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1521488
    iput v2, v4, LX/5jN;->c:F

    .line 1521489
    move-object v2, v4

    .line 1521490
    iput v3, v2, LX/5jN;->d:F

    .line 1521491
    move-object v2, v2

    .line 1521492
    iput v1, v2, LX/5jN;->e:F

    .line 1521493
    move-object v1, v2

    .line 1521494
    iput v0, v1, LX/5jN;->f:F

    .line 1521495
    move-object v0, v1

    .line 1521496
    iput p5, v0, LX/5jN;->h:I

    .line 1521497
    move-object v0, v0

    .line 1521498
    invoke-virtual {p6}, Lcom/facebook/photos/creativeediting/model/TextParams;->c()F

    move-result v1

    .line 1521499
    iput v1, v0, LX/5jN;->g:F

    .line 1521500
    move-object v0, v0

    .line 1521501
    invoke-virtual {p6}, Lcom/facebook/photos/creativeediting/model/TextParams;->g()Ljava/lang/String;

    move-result-object v1

    .line 1521502
    iput-object v1, v0, LX/5jN;->i:Ljava/lang/String;

    .line 1521503
    move-object v0, v0

    .line 1521504
    invoke-virtual {v0}, LX/5jN;->a()Lcom/facebook/photos/creativeediting/model/TextParams;

    move-result-object v0

    .line 1521505
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1521506
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(LX/362;)V

    .line 1521507
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1521508
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1521509
    invoke-virtual {p0}, LX/9dl;->h()V

    .line 1521510
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 10
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1521377
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521378
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521379
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0, p3, p4}, LX/8GY;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    .line 1521380
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float v6, v1, v2

    .line 1521381
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float v7, v0, v1

    .line 1521382
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Landroid/net/Uri;Ljava/lang/String;IIIFFFLjava/lang/String;)V

    .line 1521383
    return-void
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 8

    .prologue
    .line 1521384
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 1521385
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1521386
    const-class v1, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null photo bounds when trying to add sticker"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1521387
    :goto_0
    return-void

    .line 1521388
    :cond_0
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1521389
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    .line 1521390
    :goto_1
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->h:LX/8GY;

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 1521391
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521392
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    :goto_2
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1521393
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-eqz v3, :cond_3

    :goto_3
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 1521394
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    iget v4, v1, LX/8GY;->b:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    .line 1521395
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    iget v5, v1, LX/8GY;->b:I

    int-to-float v5, v5

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    .line 1521396
    new-instance v5, Landroid/graphics/Rect;

    iget v6, v1, LX/8GY;->b:I

    add-int/2addr v6, v3

    iget v7, v1, LX/8GY;->b:I

    add-int/2addr v7, v4

    invoke-direct {v5, v3, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v1, v5

    .line 1521397
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1521398
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1521399
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 1521400
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v1, v5

    .line 1521401
    new-instance v5, LX/5jG;

    iget-object v6, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-direct {v5, v0, v6}, LX/5jG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1521402
    iput v2, v5, LX/5jG;->e:F

    .line 1521403
    move-object v0, v5

    .line 1521404
    iput v3, v0, LX/5jG;->f:F

    .line 1521405
    move-object v0, v0

    .line 1521406
    iput v4, v0, LX/5jG;->g:F

    .line 1521407
    move-object v0, v0

    .line 1521408
    iput v1, v0, LX/5jG;->h:F

    .line 1521409
    move-object v0, v0

    .line 1521410
    invoke-virtual {v0}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    .line 1521411
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1521412
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(LX/362;)V

    .line 1521413
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1521414
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->o:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1521415
    invoke-virtual {p0}, LX/9dl;->h()V

    .line 1521416
    invoke-virtual {p0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->invalidate()V

    goto/16 :goto_0

    .line 1521417
    :cond_1
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    goto/16 :goto_1

    :cond_2
    move v3, v5

    .line 1521418
    goto/16 :goto_2

    :cond_3
    move v4, v5

    .line 1521419
    goto/16 :goto_3
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/362;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1521420
    if-nez p1, :cond_0

    .line 1521421
    :goto_0
    return-void

    .line 1521422
    :cond_0
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521423
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1521424
    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1521425
    iget-object v0, p0, LX/9dl;->a:LX/8GZ;

    move-object v3, v0

    .line 1521426
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/362;

    invoke-virtual {v3, v0}, LX/8GZ;->b(LX/362;)LX/362;

    move-result-object v0

    check-cast v0, LX/5i8;

    invoke-virtual {v2, v0, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1521427
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1521428
    :cond_1
    invoke-virtual {p0}, LX/9dl;->h()V

    .line 1521429
    invoke-virtual {p0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->invalidate()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1521430
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    if-eqz v0, :cond_0

    .line 1521431
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    invoke-interface {v0, p1}, LX/9fH;->a(Z)V

    .line 1521432
    :cond_0
    return-void
.end method

.method public getAnimationController()LX/9dr;
    .locals 1

    .prologue
    .line 1521433
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->n:LX/9dr;

    return-object v0
.end method

.method public getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;
    .locals 1

    .prologue
    .line 1521434
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    return-object v0
.end method

.method public getOverlayParamsForOriginalPhoto()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/5i8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1521435
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(Z)Ljava/util/List;

    move-result-object v0

    .line 1521436
    if-nez v0, :cond_0

    .line 1521437
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1521438
    :goto_0
    return-object v0

    .line 1521439
    :cond_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1521440
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1521441
    check-cast v0, LX/5i8;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1521442
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public getSelectedItem()LX/362;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1521443
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1521444
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, p0

    .line 1521445
    return-object v0
.end method

.method public final j()LX/362;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1521446
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1521447
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1521448
    if-eqz v0, :cond_0

    .line 1521449
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->g()V

    .line 1521450
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/362;)V

    .line 1521451
    :cond_0
    return-object v0
.end method

.method public final k()V
    .locals 5

    .prologue
    .line 1521452
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i()LX/0Px;

    move-result-object v2

    .line 1521453
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1521454
    instance-of v4, v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v4, :cond_1

    .line 1521455
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    check-cast v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/362;)V

    .line 1521456
    :cond_0
    return-void

    .line 1521457
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final l()V
    .locals 5

    .prologue
    .line 1521458
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i()LX/0Px;

    move-result-object v2

    .line 1521459
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1521460
    instance-of v4, v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v4, :cond_0

    .line 1521461
    iget-object v4, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    check-cast v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    invoke-virtual {v4, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/362;)V

    .line 1521462
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1521463
    :cond_1
    return-void
.end method

.method public final m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1521464
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i()LX/0Px;

    move-result-object v1

    .line 1521465
    iget-object v2, p0, LX/9dl;->a:LX/8GZ;

    move-object v2, v2

    .line 1521466
    invoke-virtual {v2, v1}, LX/8GZ;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 1521467
    if-nez v1, :cond_1

    .line 1521468
    :cond_0
    :goto_0
    return v0

    .line 1521469
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1521470
    instance-of v2, v2, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v2, :cond_2

    .line 1521471
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setActionButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 1521472
    iget-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1521473
    return-void
.end method

.method public setCallBack(LX/9fH;)V
    .locals 0

    .prologue
    .line 1521474
    iput-object p1, p0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    .line 1521475
    return-void
.end method
