.class public final Lcom/facebook/photos/editgallery/CropEditController$6$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9eZ;


# direct methods
.method public constructor <init>(LX/9eZ;)V
    .locals 0

    .prologue
    .line 1519782
    iput-object p1, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1519783
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1519784
    :cond_0
    :goto_0
    return-void

    .line 1519785
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v1, v1, LX/9eZ;->b:LX/9ec;

    iget-object v1, v1, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v2

    .line 1519786
    sget-object v1, LX/1ld;->a:LX/1lW;

    if-eq v2, v1, :cond_2

    sget-object v1, LX/1ld;->b:LX/1lW;

    if-ne v2, v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1519787
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 1519788
    :goto_1
    if-nez v0, :cond_6

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->j:LX/8GT;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v1, v1, LX/9eZ;->b:LX/9ec;

    iget-object v1, v1, LX/9ec;->e:Ljava/lang/String;

    .line 1519789
    iget-object v3, v2, LX/1lW;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1519790
    invoke-virtual {v0, v1, v3}, LX/8GT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object v1, v0

    .line 1519791
    :goto_2
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 1519792
    iput-object v3, v0, LX/9ec;->K:Landroid/net/Uri;

    .line 1519793
    sget-object v0, LX/1ld;->a:LX/1lW;

    if-ne v2, v0, :cond_7

    .line 1519794
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v3, v3, LX/9eZ;->a:Landroid/graphics/RectF;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/lang/String;)V

    .line 1519795
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->l:LX/1HI;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v1, v1, LX/9eZ;->b:LX/9ec;

    iget-object v1, v1, LX/9ec;->K:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1519796
    :catch_0
    move-exception v0

    .line 1519797
    invoke-virtual {v0}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException;->printStackTrace()V

    .line 1519798
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->K:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1519799
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->K:Landroid/net/Uri;

    invoke-static {v0}, LX/8GT;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1519800
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1519801
    :cond_6
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 1519802
    :catch_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->K:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1519803
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->K:Landroid/net/Uri;

    invoke-static {v0}, LX/8GT;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1519804
    :cond_7
    :try_start_2
    sget-object v0, LX/1ld;->b:LX/1lW;

    if-ne v2, v0, :cond_4

    .line 1519805
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v2, v2, LX/9eZ;->b:LX/9ec;

    iget-object v2, v2, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1le;->a(Ljava/io/InputStream;)Landroid/util/Pair;

    move-result-object v2

    .line 1519806
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1519807
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v7, 0x400

    .line 1519808
    const/4 v5, 0x1

    .line 1519809
    :goto_4
    div-int v6, v4, v5

    if-gt v6, v7, :cond_8

    div-int v6, v0, v5

    if-le v6, v7, :cond_9

    .line 1519810
    :cond_8
    mul-int/lit8 v5, v5, 0x2

    goto :goto_4

    .line 1519811
    :cond_9
    move v0, v5

    .line 1519812
    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1519813
    const/4 v0, 0x0

    iput-boolean v0, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1519814
    new-instance v4, Landroid/graphics/Rect;

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    iget-object v5, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v5, v5, LX/9eZ;->b:LX/9ec;

    iget-object v5, v5, LX/9ec;->x:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, v5

    iget-object v5, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v5, v5, LX/9eZ;->b:LX/9ec;

    iget-object v5, v5, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v0, v5

    float-to-int v5, v0

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    iget-object v6, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v6, v6, LX/9eZ;->b:LX/9ec;

    iget-object v6, v6, LX/9ec;->x:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, v6

    iget-object v6, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v6, v6, LX/9eZ;->b:LX/9ec;

    iget-object v6, v6, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v0, v6

    float-to-int v6, v0

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    iget-object v7, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v7, v7, LX/9eZ;->b:LX/9ec;

    iget-object v7, v7, LX/9ec;->x:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    mul-float/2addr v0, v7

    iget-object v7, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v7, v7, LX/9eZ;->b:LX/9ec;

    iget-object v7, v7, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    div-float/2addr v0, v7

    float-to-int v7, v0

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v2, v2, LX/9eZ;->b:LX/9ec;

    iget-object v2, v2, LX/9ec;->x:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v2, v2, LX/9eZ;->b:LX/9ec;

    iget-object v2, v2, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {v4, v5, v6, v7, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1519815
    iget-object v0, p0, Lcom/facebook/photos/editgallery/CropEditController$6$1;->a:LX/9eZ;

    iget-object v0, v0, LX/9eZ;->b:LX/9ec;

    iget-object v0, v0, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    .line 1519816
    invoke-virtual {v0, v4, v3}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1519817
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1519818
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v1, v4, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1519819
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 1519820
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 1519821
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3
.end method
