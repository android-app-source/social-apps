.class public final Lcom/facebook/photos/editgallery/EditGalleryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final p:Landroid/graphics/RectF;


# instance fields
.field public A:Ljava/lang/String;

.field public B:LX/5Rz;

.field public C:Ljava/lang/String;

.field public final q:LX/9el;

.field public r:LX/9fD;

.field public s:Landroid/net/Uri;

.field private t:LX/2Qx;

.field public u:LX/8GX;

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

.field public x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field private y:LX/9cA;

.field public z:LX/9c9;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1520389
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->p:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1520390
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1520391
    new-instance v0, LX/9em;

    invoke-direct {v0, p0}, LX/9em;-><init>(Lcom/facebook/photos/editgallery/EditGalleryActivity;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->q:LX/9el;

    return-void
.end method

.method public static a(Lcom/facebook/photos/editgallery/EditGalleryActivity;Landroid/net/Uri;)F
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1520392
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520393
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->u:LX/8GX;

    invoke-virtual {v0, p1}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v2

    .line 1520394
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    .line 1520395
    if-eqz v0, :cond_0

    iget v3, v0, LX/434;->a:I

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 1520396
    :cond_1
    :goto_0
    return v0

    .line 1520397
    :cond_2
    iget v3, v0, LX/434;->b:I

    int-to-float v3, v3

    iget v0, v0, LX/434;->a:I

    int-to-float v0, v0

    div-float v0, v3, v0

    .line 1520398
    rem-int/lit16 v2, v2, 0xb4

    if-eqz v2, :cond_1

    .line 1520399
    div-float v0, v1, v0

    goto :goto_0
.end method

.method private a(LX/2Qx;LX/8GX;Lcom/facebook/photos/editgallery/utils/FetchImageUtils;LX/0Ot;LX/9cA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Qx;",
            "LX/8GX;",
            "Lcom/facebook/photos/editgallery/utils/FetchImageUtils;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/9cA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1520400
    iput-object p1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->t:LX/2Qx;

    .line 1520401
    iput-object p2, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->u:LX/8GX;

    .line 1520402
    iput-object p3, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->w:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    .line 1520403
    iput-object p4, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->v:LX/0Ot;

    .line 1520404
    iput-object p5, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->y:LX/9cA;

    .line 1520405
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;

    invoke-static {v5}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v1

    check-cast v1, LX/2Qx;

    invoke-static {v5}, LX/8GX;->b(LX/0QB;)LX/8GX;

    move-result-object v2

    check-cast v2, LX/8GX;

    invoke-static {v5}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(LX/0QB;)Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    const/16 v4, 0x259

    invoke-static {v5, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v6, LX/9cA;

    invoke-interface {v5, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9cA;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->a(LX/2Qx;LX/8GX;Lcom/facebook/photos/editgallery/utils/FetchImageUtils;LX/0Ot;LX/9cA;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520406
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1520407
    const v0, 0x7f030448

    invoke-virtual {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->setContentView(I)V

    .line 1520408
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1520409
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1520410
    invoke-static {p0, p0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1520411
    new-instance v0, LX/9fD;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9fD;-><init>(LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->r:LX/9fD;

    .line 1520412
    invoke-virtual {p0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1520413
    const-string v1, "extra_edit_gallery_launch_settings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1520414
    iget-object v1, v4, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 1520415
    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    .line 1520416
    iget-object v1, v4, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1520417
    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520418
    iget-object v1, v4, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1520419
    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->A:Ljava/lang/String;

    .line 1520420
    iget-object v1, v4, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1520421
    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->C:Ljava/lang/String;

    .line 1520422
    const-string v1, "extra_edit_gallery_entry_point_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5Rz;->valueOf(Ljava/lang/String;)LX/5Rz;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->B:LX/5Rz;

    .line 1520423
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->y:LX/9cA;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/9cA;->a(Ljava/lang/String;)LX/9c9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->z:LX/9c9;

    .line 1520424
    if-eqz p1, :cond_0

    .line 1520425
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->r:LX/9fD;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->q:LX/9el;

    invoke-virtual {v0, v1}, LX/9fD;->a(LX/9el;)V

    .line 1520426
    :goto_0
    return-void

    .line 1520427
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1520428
    const v0, 0x7f0d07c5

    invoke-virtual {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1520429
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1520430
    new-instance v0, LX/9en;

    invoke-direct {v0, p0, v4}, LX/9en;-><init>(Lcom/facebook/photos/editgallery/EditGalleryActivity;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)V

    .line 1520431
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->w:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-virtual {v1, p0, v2, v0}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->a(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V

    goto :goto_0

    .line 1520432
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->a(Lcom/facebook/photos/editgallery/EditGalleryActivity;Landroid/net/Uri;)F

    move-result v0

    .line 1520433
    invoke-virtual {p0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1941

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1520434
    invoke-static {p0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 1520435
    int-to-float v1, v2

    div-float v0, v1, v0

    float-to-int v3, v0

    .line 1520436
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->r:LX/9fD;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    iget-object v5, p0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->q:LX/9el;

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, LX/9fD;->a(Landroid/net/Uri;IILcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;LX/9el;Ljava/util/List;LX/9fh;)V

    goto :goto_0
.end method
