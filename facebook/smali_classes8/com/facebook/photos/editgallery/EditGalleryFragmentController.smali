.class public Lcom/facebook/photos/editgallery/EditGalleryFragmentController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final A:LX/9d6;

.field private final B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Z

.field private final D:LX/1e4;

.field private final E:Ljava/lang/String;

.field public final F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/75F;",
            ">;"
        }
    .end annotation
.end field

.field public final G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;"
        }
    .end annotation
.end field

.field private final H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

.field public J:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9fi;",
            ">;"
        }
    .end annotation
.end field

.field public K:LX/9d5;

.field public L:LX/9fk;

.field public M:LX/9fk;

.field public N:LX/9eb;

.field public O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

.field public P:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field

.field public Q:Ljava/lang/String;

.field public R:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public S:LX/8GV;

.field public T:Z

.field public U:Z

.field public V:I

.field public W:I

.field public X:Z

.field public Y:Z

.field public Z:Z

.field public aa:I

.field public ab:Landroid/graphics/Rect;

.field public ac:Z

.field private final b:LX/9bz;

.field public final c:LX/9cr;

.field private final d:LX/9f1;

.field private final e:LX/9f2;

.field private final f:LX/9f4;

.field public final g:Landroid/content/DialogInterface$OnKeyListener;

.field public final h:LX/1Ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ai",
            "<",
            "Lcom/facebook/imagepipeline/image/ImageInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Landroid/view/View$OnLayoutChangeListener;

.field private final j:LX/9f8;

.field public final k:Landroid/net/Uri;

.field public final l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

.field public final m:LX/9el;

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9fk;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9f8;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/9ej;

.field private final q:LX/9fZ;

.field private final r:LX/9fV;

.field private final s:LX/9ed;

.field private final t:LX/9fJ;

.field private final u:LX/9fg;

.field public final v:LX/1Ad;

.field public final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;"
        }
    .end annotation
.end field

.field public final x:LX/9fP;

.field private final y:LX/0hB;

.field private final z:LX/9fh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1521071
    const-class v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const-string v1, "edit_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;LX/9el;Landroid/net/Uri;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;LX/9fh;LX/0Or;LX/9d6;LX/0hB;LX/0Ot;LX/0Ot;LX/8GV;LX/9ej;LX/9fZ;LX/9fV;LX/9ed;LX/9fJ;LX/9fg;LX/9c7;LX/1Ad;LX/9fQ;Ljava/lang/Boolean;LX/0Or;LX/1e4;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 7
    .param p1    # Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9el;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/9fh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p23    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p24    # LX/0Or;
        .annotation runtime Lcom/facebook/work/config/community/WorkCommunityName;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;",
            "LX/9el;",
            "Landroid/net/Uri;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;",
            "LX/9fh;",
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;",
            "LX/9d6;",
            "LX/0hB;",
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/8GV;",
            "LX/9ej;",
            "LX/9fZ;",
            "LX/9fV;",
            "LX/9ed;",
            "LX/9fJ;",
            "LX/9fg;",
            "LX/9c7;",
            "LX/1Ad;",
            "LX/9fQ;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1e4;",
            "LX/0Ot",
            "<",
            "LX/75F;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1521072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1521073
    new-instance v1, LX/9ey;

    invoke-direct {v1, p0}, LX/9ey;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b:LX/9bz;

    .line 1521074
    new-instance v1, LX/9f0;

    invoke-direct {v1, p0}, LX/9f0;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->c:LX/9cr;

    .line 1521075
    new-instance v1, LX/9f1;

    invoke-direct {v1, p0}, LX/9f1;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->d:LX/9f1;

    .line 1521076
    new-instance v1, LX/9f3;

    invoke-direct {v1, p0}, LX/9f3;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    .line 1521077
    new-instance v1, LX/9f4;

    invoke-direct {v1, p0}, LX/9f4;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->f:LX/9f4;

    .line 1521078
    new-instance v1, LX/9f5;

    invoke-direct {v1, p0}, LX/9f5;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->g:Landroid/content/DialogInterface$OnKeyListener;

    .line 1521079
    new-instance v1, LX/9f6;

    invoke-direct {v1, p0}, LX/9f6;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->h:LX/1Ai;

    .line 1521080
    new-instance v1, LX/9f7;

    invoke-direct {v1, p0}, LX/9f7;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->i:Landroid/view/View$OnLayoutChangeListener;

    .line 1521081
    new-instance v1, LX/9f9;

    invoke-direct {v1, p0}, LX/9f9;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->j:LX/9f8;

    .line 1521082
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    .line 1521083
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->o:Ljava/util/List;

    .line 1521084
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    .line 1521085
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    .line 1521086
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521087
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521088
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521089
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1521090
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1521091
    iput-object p1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521092
    iput-object p3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    .line 1521093
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    .line 1521094
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    .line 1521095
    iput-object p2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    .line 1521096
    iput-object p6, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521097
    iput-object p7, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->z:LX/9fh;

    .line 1521098
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->y:LX/0hB;

    .line 1521099
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->w:LX/0Ot;

    .line 1521100
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->p:LX/9ej;

    .line 1521101
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->q:LX/9fZ;

    .line 1521102
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->r:LX/9fV;

    .line 1521103
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->R:LX/0Ot;

    .line 1521104
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->S:LX/8GV;

    .line 1521105
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->s:LX/9ed;

    .line 1521106
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->t:LX/9fJ;

    .line 1521107
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->u:LX/9fg;

    .line 1521108
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->v:LX/1Ad;

    .line 1521109
    iput-object p8, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->B:LX/0Or;

    .line 1521110
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->B:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1521111
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->A:LX/9d6;

    .line 1521112
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->A:LX/9d6;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b:LX/9bz;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->y()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    .line 1521113
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    move-object/from16 v0, p22

    invoke-virtual {v0, v1}, LX/9fQ;->a(Ljava/util/List;)LX/9fP;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x:LX/9fP;

    .line 1521114
    invoke-static/range {p20 .. p20}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    .line 1521115
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9c7;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/9c7;->a(Ljava/lang/String;)V

    .line 1521116
    invoke-virtual/range {p23 .. p23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->C:Z

    .line 1521117
    invoke-virtual/range {p23 .. p23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface/range {p24 .. p24}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_2
    iput-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->E:Ljava/lang/String;

    .line 1521118
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->D:LX/1e4;

    .line 1521119
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->F:LX/0Ot;

    .line 1521120
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->G:LX/0Ot;

    .line 1521121
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->H:LX/0Ot;

    .line 1521122
    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l()V

    .line 1521123
    return-void

    .line 1521124
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1521125
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1521126
    :cond_2
    const-string v1, ""

    goto :goto_2
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1521127
    if-ne p1, p2, :cond_0

    .line 1521128
    :goto_0
    return-void

    .line 1521129
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_4

    .line 1521130
    :cond_1
    if-eqz p1, :cond_2

    .line 1521131
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1521132
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1521133
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->h()V

    .line 1521134
    :cond_2
    if-eqz p2, :cond_3

    .line 1521135
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1521136
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->M:LX/9fk;

    .line 1521137
    iget-object p0, v0, LX/9fk;->b:LX/9ea;

    move-object v0, p0

    .line 1521138
    invoke-interface {v0}, LX/9ea;->i()V

    .line 1521139
    :cond_3
    goto :goto_0

    .line 1521140
    :cond_4
    invoke-virtual {p1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1521141
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1521142
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/9ew;

    invoke-direct {v1, p0, p1}, LX/9ew;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1521143
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/9ex;

    invoke-direct {v1, p0, p2}, LX/9ex;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1521144
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521145
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1521146
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_3

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1521147
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1521148
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521149
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521150
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Landroid/net/Uri;)I

    move-result v1

    .line 1521151
    iput v1, v0, LX/9dl;->d:I

    .line 1521152
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521153
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521154
    iput-object p1, v0, LX/9dl;->c:Landroid/graphics/Rect;

    .line 1521155
    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c(Landroid/graphics/Rect;)V

    .line 1521156
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1521157
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1521158
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v0

    .line 1521159
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521160
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v1, v2

    .line 1521161
    iget-object v2, v1, LX/9dl;->a:LX/8GZ;

    iget v3, v1, LX/9dl;->d:I

    invoke-virtual {v2, v0, v3}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1521162
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521163
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521164
    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j()V

    .line 1521165
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521166
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521167
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/graphics/drawable/ColorDrawable;

    iput-object v1, v0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    .line 1521168
    invoke-virtual {v0}, LX/9dl;->h()V

    .line 1521169
    invoke-virtual {v0}, LX/9dl;->invalidate()V

    .line 1521170
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521171
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521172
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v0, :cond_1

    .line 1521173
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521174
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521175
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v1}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Ljava/util/List;)V

    .line 1521176
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0, p1}, LX/9eb;->a(Landroid/graphics/Rect;)V

    .line 1521177
    return-void

    :cond_2
    move v0, v2

    .line 1521178
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 1521179
    goto/16 :goto_1
.end method

.method public static a$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/9fk;)V
    .locals 4

    .prologue
    .line 1521180
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1521181
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->q(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1521182
    iget-object v0, p1, LX/9fk;->b:LX/9ea;

    move-object v0, v0

    .line 1521183
    check-cast v0, LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v2

    .line 1521184
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v0

    move-object v1, v0

    .line 1521185
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->c:Z

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->z:LX/9fh;

    if-eqz v0, :cond_1

    .line 1521186
    :cond_0
    iget-object v0, p1, LX/9fk;->b:LX/9ea;

    move-object v0, v0

    .line 1521187
    check-cast v0, LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v0

    if-eq v1, v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Y:Z

    .line 1521188
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->M:LX/9fk;

    .line 1521189
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    invoke-virtual {p1}, LX/9fk;->a()LX/5Rr;

    move-result-object v3

    .line 1521190
    iput-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->i:LX/5Rr;

    .line 1521191
    iput-object p1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    .line 1521192
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    .line 1521193
    iget-object v3, v0, LX/9fk;->b:LX/9ea;

    move-object v0, v3

    .line 1521194
    check-cast v0, LX/9eb;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    .line 1521195
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v3, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-interface {v0, v3}, LX/9eb;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V

    .line 1521196
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    if-nez v0, :cond_3

    .line 1521197
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    invoke-interface {v0, v3}, LX/9eb;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V

    .line 1521198
    if-eqz v1, :cond_2

    sget-object v0, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    if-eq v2, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1521199
    :cond_2
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->p$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1521200
    :cond_3
    :goto_2
    return-void

    .line 1521201
    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 1521202
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 1521203
    :cond_6
    if-eq v1, v2, :cond_7

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ab:Landroid/graphics/Rect;

    if-eqz v0, :cond_7

    .line 1521204
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ab:Landroid/graphics/Rect;

    invoke-static {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/graphics/Rect;)V

    .line 1521205
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    goto :goto_2

    .line 1521206
    :cond_7
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->r(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1521207
    if-eqz v0, :cond_3

    .line 1521208
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v1, v0}, LX/9eb;->a(Landroid/graphics/Rect;)V

    .line 1521209
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->h()V

    goto :goto_2
.end method

.method public static b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1521292
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1521293
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1521294
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1521295
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1521296
    return-object v0
.end method

.method private b(LX/9fk;)Landroid/view/View;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1521210
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1521211
    :goto_0
    return-object v0

    .line 1521212
    :cond_0
    iget-object v0, p1, LX/9fk;->b:LX/9ea;

    move-object v0, v0

    .line 1521213
    check-cast v0, LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v0

    .line 1521214
    sget-object v2, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    if-ne v0, v2, :cond_1

    .line 1521215
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521216
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1521217
    goto :goto_0

    .line 1521218
    :cond_1
    sget-object v2, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    if-ne v0, v2, :cond_2

    .line 1521219
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521220
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    move-object v0, v1

    .line 1521221
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1521222
    goto :goto_0
.end method

.method public static b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V
    .locals 3

    .prologue
    .line 1521223
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v0, :cond_0

    .line 1521224
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-interface {v0, v1, p1}, LX/9el;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;Z)V

    .line 1521225
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fk;

    .line 1521226
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->c:Z

    if-nez v1, :cond_1

    .line 1521227
    iget-object v1, v0, LX/9fk;->b:LX/9ea;

    move-object v1, v1

    .line 1521228
    check-cast v1, LX/9eb;

    invoke-interface {v1, p1}, LX/9eb;->a(Z)V

    .line 1521229
    :cond_1
    iget-object v1, v0, LX/9fk;->b:LX/9ea;

    move-object v0, v1

    .line 1521230
    invoke-interface {v0}, LX/9ea;->b()V

    goto :goto_0

    .line 1521231
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    .line 1521232
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521233
    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1521234
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p()V

    .line 1521235
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->t$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1521236
    return-void

    .line 1521237
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 1521238
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1521239
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_1
.end method

.method private l()V
    .locals 21

    .prologue
    .line 1521240
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->k()Landroid/view/ViewGroup;

    move-result-object v20

    .line 1521241
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->s()Landroid/view/ViewStub;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v8

    .line 1521242
    const/4 v1, 0x4

    invoke-virtual {v8, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521243
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    sget-object v2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a(LX/5Rr;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1521244
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->t:LX/9fJ;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->t()Landroid/view/ViewStub;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->a()Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v4}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m()Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->d:LX/9f1;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-virtual/range {v1 .. v8}, LX/9fJ;->a(Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;LX/9f1;LX/0am;Landroid/net/Uri;Landroid/view/View;)LX/9fI;

    move-result-object v13

    .line 1521245
    const v1, 0x7f0d0d04

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1521246
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    new-instance v9, LX/9fk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v14

    const v2, 0x7f0d0d05

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    const v2, 0x7f0d0d06

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v9 .. v16}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521247
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    sget-object v2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a(LX/5Rr;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1521248
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f()LX/5Rq;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521249
    const v1, 0x7f0d0d07

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1521250
    sget-object v2, LX/9ez;->a:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    invoke-virtual {v3}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f()LX/5Rq;

    move-result-object v3

    invoke-virtual {v3}, LX/5Rq;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1521251
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    sget-object v2, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a(LX/5Rr;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1521252
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->r:LX/9fV;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->l()Landroid/widget/FrameLayout;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m()Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v4, v4, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->j:LX/9f8;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    invoke-virtual/range {v1 .. v6}, LX/9fV;->a(Landroid/widget/FrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/9f8;LX/0am;)LX/9fU;

    move-result-object v13

    .line 1521253
    const v1, 0x7f0d0d09

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1521254
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    new-instance v9, LX/9fk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v14

    const v2, 0x7f0d0d05

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    const v2, 0x7f0d0d06

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v9 .. v16}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521255
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    sget-object v2, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a(LX/5Rr;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1521256
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->q:LX/9fZ;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v12, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->l()Landroid/widget/FrameLayout;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m()Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v15, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x:LX/9fP;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->j:LX/9f8;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    move-object/from16 v19, v0

    move-object/from16 v17, p0

    invoke-virtual/range {v9 .. v19}, LX/9fZ;->a(Landroid/net/Uri;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Ljava/lang/String;Landroid/widget/FrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/9fP;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/9f8;LX/0am;)Lcom/facebook/photos/editgallery/TextEditController;

    move-result-object v13

    .line 1521257
    const v1, 0x7f0d0d0a

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1521258
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    new-instance v9, LX/9fk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v14

    const v2, 0x7f0d0d05

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    const v2, 0x7f0d0d06

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v9 .. v16}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521259
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    sget-object v2, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a(LX/5Rr;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1521260
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->p:LX/9ej;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->l()Landroid/widget/FrameLayout;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m()Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v10, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    move-object/from16 v12, p0

    invoke-virtual/range {v6 .. v12}, LX/9ej;->a(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/0am;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)LX/9ei;

    move-result-object v5

    .line 1521261
    const v1, 0x7f0d0d0b

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1521262
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    new-instance v1, LX/9fk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v6}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v6

    const v7, 0x7f0d0d05

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    const v8, 0x7f0d0d06

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v1 .. v8}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521263
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9fk;

    .line 1521264
    invoke-virtual {v1}, LX/9fk;->b()LX/9ea;

    move-result-object v2

    instance-of v2, v2, LX/9f8;

    if-eqz v2, :cond_6

    .line 1521265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->o:Ljava/util/List;

    invoke-virtual {v1}, LX/9fk;->b()LX/9ea;

    move-result-object v2

    check-cast v2, LX/9f8;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521266
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    if-nez v2, :cond_5

    invoke-virtual {v1}, LX/9fk;->a()LX/5Rr;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v4, v4, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->i:LX/5Rr;

    if-ne v2, v4, :cond_5

    .line 1521267
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/9fk;->a(Z)V

    .line 1521268
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/9fk;)V

    goto :goto_1

    .line 1521269
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->s:LX/9ed;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->r()Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->m()Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->f:LX/9f4;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v10, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->w()Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    move-result-object v13

    move-object/from16 v11, p0

    invoke-virtual/range {v3 .. v13}, LX/9ed;->a(Landroid/net/Uri;Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/view/View;LX/9f4;Ljava/lang/String;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/0am;Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;)LX/9ec;

    move-result-object v13

    .line 1521270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    new-instance v9, LX/9fk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v14

    const v3, 0x7f0d0d05

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    const v3, 0x7f0d0d06

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/facebook/resources/ui/FbTextView;

    move-object v11, v1

    invoke-direct/range {v9 .. v16}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1521271
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521272
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1521273
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0d1c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1521274
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    .line 1521275
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->C:Z

    if-eqz v1, :cond_7

    .line 1521276
    const v1, 0x7f0d276b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1521277
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->D:LX/1e4;

    const-string v4, "work_list"

    invoke-virtual {v3, v4}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1521278
    const v1, 0x7f0d276c

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1521279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1521280
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->u:LX/9fg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->r()Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->f:LX/9f4;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v5, v5, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/9fg;->a(Landroid/net/Uri;Lcom/facebook/photos/creativeediting/RotatingFrameLayout;LX/9f4;Ljava/lang/String;)LX/9ff;

    move-result-object v13

    .line 1521281
    const v1, 0x7f0d0d08

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1521282
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v11

    .line 1521283
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    new-instance v9, LX/9fk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->e:LX/9f2;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->v()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v14

    const v2, 0x7f0d0d05

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    const v2, 0x7f0d0d06

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v9 .. v16}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1521284
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_9

    .line 1521285
    const/4 v1, 0x4

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521286
    :cond_9
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static m$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 6

    .prologue
    .line 1521287
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521288
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->m()V

    .line 1521289
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1521290
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->A:LX/9d6;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b:LX/9bz;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-direct {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->y()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    .line 1521291
    return-void
.end method

.method public static p$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 1521018
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v0

    .line 1521019
    sget-object v1, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    if-eq v0, v1, :cond_1

    sget-object v1, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    if-eq v0, v1, :cond_1

    .line 1521020
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521021
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1521022
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1521023
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521024
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    move-object v0, v1

    .line 1521025
    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setVisibility(I)V

    .line 1521026
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521027
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v0, v1

    .line 1521028
    invoke-virtual {v0, v2}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1521029
    :cond_0
    :goto_0
    return-void

    .line 1521030
    :cond_1
    sget-object v1, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    if-ne v0, v1, :cond_2

    .line 1521031
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521032
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1521033
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1521034
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521035
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1521036
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1521037
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x:LX/9fP;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    sget-object v2, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget v3, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->h:I

    invoke-virtual {v0, v1, v2, v3}, LX/9fP;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/9ek;I)LX/8Hx;

    move-result-object v0

    .line 1521038
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->v:LX/1Ad;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    new-instance v3, LX/1o9;

    iget v4, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    iget v5, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    invoke-direct {v3, v4, v5}, LX/1o9;-><init>(II)V

    .line 1521039
    iput-object v3, v2, LX/1bX;->c:LX/1o9;

    .line 1521040
    move-object v2, v2

    .line 1521041
    iput-object v0, v2, LX/1bX;->j:LX/33B;

    .line 1521042
    move-object v0, v2

    .line 1521043
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->h:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1521044
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521045
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v2

    .line 1521046
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1521047
    goto :goto_0

    .line 1521048
    :cond_2
    sget-object v1, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    if-ne v0, v1, :cond_0

    .line 1521049
    const/4 v4, 0x1

    .line 1521050
    iget v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    .line 1521051
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    .line 1521052
    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1521053
    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-static {v2}, LX/63w;->a(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 1521054
    int-to-float v0, v0

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-static {v2}, LX/63w;->b(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1521055
    :cond_3
    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521056
    iget-object v5, v3, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    move-object v3, v5

    .line 1521057
    invoke-virtual {v2, v3, v1, v0, v4}, LX/9d5;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V

    .line 1521058
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521059
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    move-object v1, v2

    .line 1521060
    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    .line 1521061
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->O:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->c:LX/9cr;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a(LX/9cr;)V

    .line 1521062
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1521063
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/graphics/RectF;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, LX/9d5;->a([Landroid/graphics/RectF;)V

    .line 1521064
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    invoke-virtual {v0, v4}, LX/9d5;->b(Z)V

    .line 1521065
    goto/16 :goto_0
.end method

.method public static q(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 2

    .prologue
    .line 1521066
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521067
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521068
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    const/4 v1, 0x1

    .line 1521069
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    .line 1521070
    :cond_0
    return-void
.end method

.method public static r(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1520958
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v0

    .line 1520959
    sget-object v2, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    if-ne v0, v2, :cond_2

    .line 1520960
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->w(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1520961
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1520962
    :cond_1
    return-object v0

    .line 1520963
    :cond_2
    sget-object v2, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    if-ne v0, v2, :cond_3

    .line 1520964
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1520965
    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v2

    .line 1520966
    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static s(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 4

    .prologue
    .line 1520947
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520948
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1520949
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->o()V

    .line 1520950
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    .line 1520951
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1520952
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1520953
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 1520954
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SavingTextPhoto_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Q:Ljava/lang/String;

    .line 1520955
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iget-object v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Q:Ljava/lang/String;

    new-instance v3, LX/9eq;

    invoke-direct {v3, p0, v1}, LX/9eq;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/net/Uri;)V

    new-instance v1, LX/9er;

    invoke-direct {v1, p0}, LX/9er;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    invoke-virtual {v0, v2, v3, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1520956
    return-void

    .line 1520957
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_1
.end method

.method public static t$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 9

    .prologue
    .line 1520932
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v1

    .line 1520933
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1520934
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fi;

    .line 1520935
    new-instance v2, LX/9ep;

    invoke-direct {v2, p0}, LX/9ep;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    move-object v2, v2

    .line 1520936
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    iget-object v4, v0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, v0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, v0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-object v7, v0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget v8, v0, LX/9fi;->c:F

    add-float/2addr v7, v8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1520937
    iget v4, v0, LX/9fi;->b:I

    int-to-long v5, v4

    invoke-virtual {v3, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1520938
    if-eqz v2, :cond_0

    .line 1520939
    invoke-virtual {v3, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1520940
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 1520941
    invoke-virtual {v1, v3}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1520942
    invoke-virtual {v3}, Landroid/view/animation/Animation;->startNow()V

    .line 1520943
    :goto_0
    return-void

    .line 1520944
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1520945
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->K:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->m()V

    .line 1520946
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public static u$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1520916
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520917
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520918
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520919
    iput-boolean v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    .line 1520920
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    if-eqz v0, :cond_2

    .line 1520921
    iput-boolean v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->U:Z

    .line 1520922
    :cond_1
    :goto_0
    return-void

    .line 1520923
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    if-eqz v0, :cond_1

    .line 1520924
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520925
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1520926
    invoke-static {v0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    if-eqz v1, :cond_4

    .line 1520927
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->s(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    goto :goto_0

    .line 1520928
    :cond_4
    invoke-static {p0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->z(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520929
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v1, :cond_5

    .line 1520930
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    invoke-interface {v1, v0}, LX/9el;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1520931
    :cond_5
    invoke-static {p0, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    goto :goto_0
.end method

.method public static v$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 3

    .prologue
    .line 1520910
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    if-eqz v0, :cond_0

    .line 1520911
    :goto_0
    return-void

    .line 1520912
    :cond_0
    new-instance v0, LX/0ju;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08241d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 1520913
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08241f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/9eu;

    invoke-direct {v2, p0}, LX/9eu;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1520914
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08241e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/9ev;

    invoke-direct {v2, p0}, LX/9ev;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1520915
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public static w(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1520967
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1520968
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1520969
    iget-object p0, v1, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->M:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    move-object v1, p0

    .line 1520970
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1520971
    return-object v0
.end method

.method public static x(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 7

    .prologue
    .line 1520972
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v1

    .line 1520973
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->z:LX/9fh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1520974
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1520975
    instance-of v0, v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1520976
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v2}, LX/1af;->a(Landroid/graphics/RectF;)V

    move-object v0, v2

    .line 1520977
    :goto_0
    new-instance v2, LX/9fi;

    iget-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->z:LX/9fh;

    invoke-static {v1, v0}, LX/9fh;->a(Landroid/view/View;Landroid/graphics/RectF;)LX/9fh;

    move-result-object v0

    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v5, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->y:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->d()I

    move-result v5

    invoke-direct {v2, v3, v0, v4, v5}, LX/9fi;-><init>(LX/9fh;LX/9fh;Landroid/graphics/PointF;I)V

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    .line 1520978
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fi;

    .line 1520979
    new-instance v2, LX/9eo;

    invoke-direct {v2, p0}, LX/9eo;-><init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    move-object v2, v2

    .line 1520980
    invoke-virtual {v0, v1, v2}, LX/9fi;->a(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V

    .line 1520981
    :cond_0
    :goto_1
    return-void

    .line 1520982
    :cond_1
    instance-of v0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 1520983
    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0

    .line 1520984
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Y:Z

    if-eqz v0, :cond_3

    .line 1520985
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->M:LX/9fk;

    invoke-direct {p0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_1

    .line 1520986
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Z:Z

    if-nez v0, :cond_0

    .line 1520987
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    invoke-direct {p0, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v0

    .line 1520988
    if-eqz v0, :cond_4

    .line 1520989
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1520990
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1520991
    :cond_4
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->M:LX/9fk;

    invoke-direct {p0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(LX/9fk;)Landroid/view/View;

    move-result-object v1

    .line 1520992
    if-eqz v1, :cond_5

    if-eq v1, v0, :cond_5

    .line 1520993
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1520994
    :cond_5
    if-nez v0, :cond_6

    if-eqz v1, :cond_0

    :cond_6
    if-eq v0, v1, :cond_0

    .line 1520995
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->h()V

    .line 1520996
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->M:LX/9fk;

    .line 1520997
    iget-object v1, v0, LX/9fk;->b:LX/9ea;

    move-object v0, v1

    .line 1520998
    invoke-interface {v0}, LX/9ea;->i()V

    goto :goto_1

    :cond_7
    move-object v0, v2

    goto/16 :goto_0
.end method

.method private y()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1521012
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    .line 1521013
    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1521014
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1521015
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1521016
    :cond_0
    :goto_0
    return-object v0

    .line 1521017
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static z(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 3

    .prologue
    .line 1520999
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/7l1;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1521000
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1521001
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1521002
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 1521003
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    invoke-virtual {v0, p1}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1521004
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521005
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->F:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v1

    .line 1521006
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 1521007
    iput-object p1, v1, LX/108;->g:Ljava/lang/String;

    .line 1521008
    move-object v1, v1

    .line 1521009
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1521010
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1521011
    return-void
.end method
