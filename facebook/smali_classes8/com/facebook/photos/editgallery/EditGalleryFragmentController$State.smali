.class public final Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5Rr;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/5Rq;

.field public f:Z

.field public g:Z

.field public h:I

.field public i:LX/5Rr;

.field public j:Z

.field public k:Z

.field public l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1520840
    new-instance v0, LX/9fA;

    invoke-direct {v0}, LX/9fA;-><init>()V

    sput-object v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1520878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1520879
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->d:Ljava/util/List;

    .line 1520880
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    .line 1520881
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->g:Z

    .line 1520882
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    .line 1520883
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    .line 1520884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    .line 1520885
    new-instance v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-direct {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    .line 1520886
    iput-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    .line 1520887
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->c:Z

    .line 1520888
    iput-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    .line 1520889
    iput-boolean v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    .line 1520890
    iput-boolean v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->r:Z

    .line 1520891
    invoke-static {}, LX/5Rr;->values()[LX/5Rr;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->i:LX/5Rr;

    .line 1520892
    invoke-static {}, LX/5Rq;->values()[LX/5Rq;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->e:LX/5Rq;

    .line 1520893
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->d:Ljava/util/List;

    const-class v1, LX/5Rr;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1520894
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    .line 1520895
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    .line 1520896
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->h:I

    .line 1520897
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520898
    const-class v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    .line 1520899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    .line 1520900
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    const-class v1, Landroid/graphics/RectF;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1520901
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    .line 1520902
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->g:Z

    .line 1520903
    iput-boolean v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->c:Z

    .line 1520904
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    .line 1520905
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    .line 1520906
    sget-object v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    .line 1520907
    const-class v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1520908
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->r:Z

    .line 1520909
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1520863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1520864
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->d:Ljava/util/List;

    .line 1520865
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    .line 1520866
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->g:Z

    .line 1520867
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    .line 1520868
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    .line 1520869
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    .line 1520870
    new-instance v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-direct {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    .line 1520871
    iput-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    .line 1520872
    iput-boolean v1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->c:Z

    .line 1520873
    iput-object v3, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    .line 1520874
    iput-boolean v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    .line 1520875
    iput-boolean v2, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->r:Z

    .line 1520876
    iput-object p1, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520877
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1520862
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->r:Z

    return v0
.end method

.method public final a(LX/5Rr;)Z
    .locals 1

    .prologue
    .line 1520861
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1520860
    const/4 v0, 0x0

    return v0
.end method

.method public final f()LX/5Rq;
    .locals 1

    .prologue
    .line 1520859
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->e:LX/5Rq;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1520841
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->i:LX/5Rr;

    invoke-virtual {v0}, LX/5Rr;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1520842
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->e:LX/5Rq;

    invoke-virtual {v0}, LX/5Rq;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1520843
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1520844
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1520845
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1520846
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1520847
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1520848
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1520849
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1520850
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->m:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1520851
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1520852
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1520853
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1520854
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1520855
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1520856
    iget-object v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1520857
    iget-boolean v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->r:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1520858
    return-void
.end method
