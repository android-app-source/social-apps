.class public final Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1521304
    new-instance v0, LX/9fC;

    invoke-direct {v0}, LX/9fC;-><init>()V

    sput-object v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1521319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1521312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1521313
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->d:I

    .line 1521314
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->b:I

    .line 1521315
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->a:I

    .line 1521316
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->c:I

    .line 1521317
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->e:I

    .line 1521318
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1521311
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1521305
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1521306
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1521307
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1521308
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1521309
    iget v0, p0, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1521310
    return-void
.end method
