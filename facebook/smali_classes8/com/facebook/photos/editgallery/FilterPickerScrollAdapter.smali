.class public Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/9fL;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/graphics/drawable/ColorDrawable;

.field public final c:Landroid/graphics/drawable/ColorDrawable;

.field public final d:Landroid/content/Context;

.field public final e:LX/9f1;

.field public final f:LX/1Ad;

.field private final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/9fP;

.field public final i:LX/8GN;

.field public final j:Landroid/net/Uri;

.field public final k:I

.field public l:LX/9fL;

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1521820
    const-class v0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;LX/9f1;LX/1Ad;LX/9fQ;LX/8GN;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/9f1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1521821
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1521822
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->m:I

    .line 1521823
    iput-object p1, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    .line 1521824
    iput-object p5, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->f:LX/1Ad;

    .line 1521825
    iput-object p2, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->j:Landroid/net/Uri;

    .line 1521826
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p6, v0}, LX/9fQ;->a(Ljava/util/List;)LX/9fP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->h:LX/9fP;

    .line 1521827
    iput-object p4, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->e:LX/9f1;

    .line 1521828
    iput-object p7, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->i:LX/8GN;

    .line 1521829
    iget-object v0, p3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    move-object v0, v0

    .line 1521830
    iput-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->g:LX/0Px;

    .line 1521831
    iget-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c12

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->k:I

    .line 1521832
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->b:Landroid/graphics/drawable/ColorDrawable;

    .line 1521833
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->c:Landroid/graphics/drawable/ColorDrawable;

    .line 1521834
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1521835
    const/4 v2, 0x0

    .line 1521836
    sget-object v0, LX/5jI;->FILTER:LX/5jI;

    invoke-virtual {v0}, LX/5jI;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 1521837
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03044d

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1521838
    new-instance v0, LX/9fM;

    invoke-direct {v0, p0, v1}, LX/9fM;-><init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V

    .line 1521839
    :goto_0
    return-object v0

    .line 1521840
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03044e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1521841
    new-instance v0, LX/9fN;

    invoke-direct {v0, p0, v1}, LX/9fN;-><init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 1521842
    check-cast p1, LX/9fL;

    .line 1521843
    iget-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->g:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-virtual {p1, v0}, LX/9fL;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1521844
    iget v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->m:I

    if-ne v0, p2, :cond_0

    .line 1521845
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 1521846
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1521847
    iget-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->g:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1521848
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v0, p0

    .line 1521849
    invoke-virtual {v0}, LX/5jI;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1521850
    iget-object v0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
