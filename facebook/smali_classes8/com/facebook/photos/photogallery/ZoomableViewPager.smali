.class public Lcom/facebook/photos/photogallery/ZoomableViewPager;
.super Landroid/support/v4/view/ViewPager;
.source ""

# interfaces
.implements LX/4ny;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1527840
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/photogallery/ZoomableViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1527841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1527836
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1527837
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->b:Z

    .line 1527838
    const-class v0, Lcom/facebook/photos/photogallery/ZoomableViewPager;

    invoke-static {v0, p0}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1527839
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZIII)Z
    .locals 1

    .prologue
    .line 1527842
    instance-of v0, p1, LX/7UR;

    if-eqz v0, :cond_0

    .line 1527843
    check-cast p1, LX/7UR;

    invoke-virtual {p1, p3}, LX/7UR;->a(I)Z

    move-result v0

    .line 1527844
    :goto_0
    return v0

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1527834
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->b:Z

    .line 1527835
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1527821
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->b:Z

    .line 1527822
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1527831
    iget-boolean v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->b:Z

    if-eqz v0, :cond_0

    .line 1527832
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1527833
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const v0, 0x4947c90b

    invoke-static {v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1527823
    iget-boolean v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->b:Z

    if-eqz v0, :cond_0

    .line 1527824
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const v1, -0x601b955e

    invoke-static {v3, v3, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1527825
    :goto_0
    return v0

    .line 1527826
    :catch_0
    move-exception v3

    .line 1527827
    iget-object v0, p0, Lcom/facebook/photos/photogallery/ZoomableViewPager;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1527828
    const-string v4, "ZoomableViewPager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error during touch event: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1527829
    const v0, -0x7cd57c56

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 1527830
    :cond_0
    const v0, 0x3a8e9751

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0
.end method
