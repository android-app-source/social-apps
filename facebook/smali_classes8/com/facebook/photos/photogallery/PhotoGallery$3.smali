.class public final Lcom/facebook/photos/photogallery/PhotoGallery$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photogallery/PhotoGallery;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V
    .locals 0

    .prologue
    .line 1527643
    iput-object p1, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1527644
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1527645
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "PhotoGallery"

    const-string v2, "ViewPager has no children after the layout after setting the adapter"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527646
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    if-eqz v0, :cond_1

    .line 1527647
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    iget-object v1, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v1}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentIndex()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v2}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentPhotoView()LX/9iQ;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/9iH;->a(ILX/9iQ;)V

    .line 1527648
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentIndex()I

    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentPhotoView()LX/9iQ;

    .line 1527649
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    invoke-interface {v0}, LX/9iH;->a()V

    .line 1527650
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v1, p0, Lcom/facebook/photos/photogallery/PhotoGallery$3;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v1}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentPhotoView()LX/9iQ;

    move-result-object v1

    .line 1527651
    iput-object v1, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->g:LX/9iQ;

    .line 1527652
    return-void
.end method
