.class public abstract Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "LX/74w;",
        "T2:",
        "LX/9iQ;",
        ">",
        "Lcom/facebook/base/fragment/FbFragment;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/photos/photogallery/PhotoGallery;

.field public b:I

.field public c:LX/Duv;

.field public d:LX/9iR;

.field public e:LX/74S;

.field public f:Z

.field public g:Z

.field public h:LX/0fP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/745;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:J

.field private l:LX/0fR;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1527585
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1527586
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1527587
    iput-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->i:LX/0Ot;

    .line 1527588
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->k:J

    .line 1527589
    new-instance v0, LX/9iG;

    invoke-direct {v0, p0}, LX/9iG;-><init>(Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->l:LX/0fR;

    return-void
.end method

.method public static e(Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;)V
    .locals 2

    .prologue
    .line 1527590
    iget-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/745;

    .line 1527591
    iget-object v1, v0, LX/745;->e:Ljava/lang/String;

    move-object v0, v1

    .line 1527592
    iput-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->j:Ljava/lang/String;

    .line 1527593
    iget-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1527594
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->j:Ljava/lang/String;

    .line 1527595
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1527596
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1527597
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    invoke-static {p1}, LX/0fP;->a(LX/0QB;)LX/0fP;

    move-result-object v2

    check-cast v2, LX/0fP;

    const/16 v0, 0x2e06

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    iput-object p1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->i:LX/0Ot;

    .line 1527598
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1527599
    iget-object v0, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentIndex()I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1527600
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x37efbe22

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1527601
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    if-eqz v1, :cond_0

    .line 1527602
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v1}, Lcom/facebook/photos/photogallery/PhotoGallery;->b()V

    .line 1527603
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1527604
    const/16 v1, 0x2b

    const v2, 0x642c1c6d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d684c82

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1527605
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    invoke-virtual {v1}, LX/0fP;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1527606
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    iget-object v2, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->l:LX/0fR;

    invoke-virtual {v1, v2}, LX/0fP;->b(LX/0fR;)V

    .line 1527607
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1527608
    const/16 v1, 0x2b

    const v2, -0x34cba1bb    # -1.1820613E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d734d15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1527609
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1527610
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    invoke-virtual {v1}, LX/0fP;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1527611
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    .line 1527612
    sget-object v2, LX/10e;->CLOSED:LX/10e;

    invoke-static {v1, v2}, LX/0fP;->c(LX/0fP;LX/10e;)Z

    move-result v2

    move v1, v2

    .line 1527613
    if-nez v1, :cond_0

    .line 1527614
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0fP;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1527615
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->h:LX/0fP;

    iget-object v2, p0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->l:LX/0fR;

    invoke-virtual {v1, v2}, LX/0fP;->a(LX/0fR;)V

    .line 1527616
    :cond_1
    invoke-static {p0}, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->e(Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;)V

    .line 1527617
    const/16 v1, 0x2b

    const v2, -0x12eaa5a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
