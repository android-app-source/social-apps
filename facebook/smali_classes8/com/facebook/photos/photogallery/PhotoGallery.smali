.class public Lcom/facebook/photos/photogallery/PhotoGallery;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/support/v4/view/ViewPager;

.field private b:I

.field private c:LX/Duv;

.field private d:LX/9iR;

.field private e:LX/9iM;

.field public f:LX/9iH;

.field public g:LX/9iQ;

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1527699
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/photogallery/PhotoGallery;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1527700
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1527691
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1527692
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1527693
    iput-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->h:LX/0Ot;

    .line 1527694
    const-class v0, Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-static {v0, p0}, Lcom/facebook/photos/photogallery/PhotoGallery;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1527695
    const v0, 0x7f030f41

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1527696
    const v0, 0x7f0d24f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    .line 1527697
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    instance-of v0, v0, LX/4ny;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1527698
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1527665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->j:Z

    .line 1527666
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->e:LX/9iM;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1527667
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1527668
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/photos/photogallery/PhotoGallery$5;

    invoke-direct {v1, p0}, Lcom/facebook/photos/photogallery/PhotoGallery$5;-><init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V

    invoke-static {v0, v1}, LX/8He;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1527669
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/photogallery/PhotoGallery;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/photogallery/PhotoGallery;

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->h:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/photos/photogallery/PhotoGallery;I)LX/9iQ;
    .locals 2

    .prologue
    .line 1527690
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/9iQ;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1527688
    invoke-virtual {p0}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentIndex()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/photogallery/PhotoGallery;->a(I)V

    .line 1527689
    return-void
.end method

.method public final a(ILX/Duv;LX/9iR;)V
    .locals 3

    .prologue
    .line 1527676
    iput p1, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->b:I

    .line 1527677
    iput-object p2, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->c:LX/Duv;

    .line 1527678
    iput-object p3, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->d:LX/9iR;

    .line 1527679
    new-instance v0, LX/9iM;

    iget-object v1, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->c:LX/Duv;

    iget-object v2, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->d:LX/9iR;

    invoke-direct {v0, v1, v2}, LX/9iM;-><init>(LX/Duv;LX/9iR;)V

    iput-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->e:LX/9iM;

    .line 1527680
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->e:LX/9iM;

    new-instance v1, LX/9iK;

    invoke-direct {v1, p0}, LX/9iK;-><init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V

    .line 1527681
    iput-object v1, v0, LX/9iM;->d:LX/9iJ;

    .line 1527682
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1527683
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->e:LX/9iM;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1527684
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1527685
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/9iL;

    invoke-direct {v1, p0}, LX/9iL;-><init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1527686
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/photos/photogallery/PhotoGallery$3;

    invoke-direct {v1, p0}, Lcom/facebook/photos/photogallery/PhotoGallery$3;-><init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V

    invoke-static {v0, v1}, LX/8He;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1527687
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1527674
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    .line 1527675
    return-void
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 1527673
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getCurrentPhotoView()LX/9iQ;
    .locals 1

    .prologue
    .line 1527672
    invoke-virtual {p0}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentIndex()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/photos/photogallery/PhotoGallery;->b(Lcom/facebook/photos/photogallery/PhotoGallery;I)LX/9iQ;

    move-result-object v0

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1527670
    iget-object v0, p0, Lcom/facebook/photos/photogallery/PhotoGallery;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/photos/photogallery/PhotoGallery$4;

    invoke-direct {v1, p0}, Lcom/facebook/photos/photogallery/PhotoGallery$4;-><init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V

    invoke-static {v0, v1}, LX/8He;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1527671
    return-void
.end method
