.class public Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/9cZ;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1516692
    const-class v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    const-string v1, "stickers_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1516693
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/9cZ;LX/0Or;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/creativeediting/abtest/IsFb4aComposerStickerSearchEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9cZ;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516694
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1516695
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1516696
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->c:LX/0Px;

    .line 1516697
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->f:Landroid/content/Context;

    .line 1516698
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->d:LX/9cZ;

    .line 1516699
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->e:LX/0Or;

    .line 1516700
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 1516701
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1516702
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1516703
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1516704
    if-nez p1, :cond_0

    .line 1516705
    sget-object v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->a:Ljava/lang/Object;

    .line 1516706
    :goto_0
    return-object v0

    .line 1516707
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->c:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1516708
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1516709
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1516710
    invoke-virtual {p0, p1}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 1516711
    sget-object v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->a:Ljava/lang/Object;

    if-ne v1, v0, :cond_0

    .line 1516712
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0310f9

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1516713
    :goto_0
    return-object p2

    .line 1516714
    :cond_0
    if-eqz p2, :cond_1

    const v0, 0x7f0d2173

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1516715
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0310fb

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1516716
    :cond_2
    const v0, 0x7f0d2173

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1516717
    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1516718
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v1, v2

    .line 1516719
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1516720
    sget-object v2, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
