.class public Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "LX/5i8;",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private final d:LX/8GY;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field public k:LX/5i8;

.field private l:Z

.field public m:LX/9fG;

.field public n:Landroid/graphics/Rect;

.field public o:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1516296
    const-class v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    const-string v1, "creative_editing_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;Landroid/content/Context;LX/8GY;LX/0Or;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Rect;",
            "Landroid/content/Context;",
            "LX/8GY;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516298
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->o:F

    .line 1516299
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    .line 1516300
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    .line 1516301
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->d:LX/8GY;

    .line 1516302
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->e:LX/0Or;

    .line 1516303
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    .line 1516304
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c05

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->f:I

    .line 1516305
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c06

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->g:I

    .line 1516306
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    .line 1516307
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02041c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i:Landroid/graphics/drawable/Drawable;

    .line 1516308
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02189c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    .line 1516309
    return-void
.end method

.method private a(Landroid/graphics/Canvas;LX/5i8;Landroid/graphics/Rect;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 1516310
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516311
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1516312
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516313
    if-nez v0, :cond_0

    .line 1516314
    :goto_0
    return-void

    .line 1516315
    :cond_0
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1516316
    invoke-interface {p2, p3}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    .line 1516317
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1516318
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-ne v3, p2, :cond_7

    .line 1516319
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    instance-of v3, v3, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v3, :cond_5

    .line 1516320
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, LX/8GY;->c(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1516321
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1516322
    :cond_1
    :goto_1
    iget v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->o:F

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->o:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    .line 1516323
    iget v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->o:F

    iget v4, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->o:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1516324
    :cond_2
    :goto_2
    invoke-interface {p2}, LX/362;->c()F

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1516325
    invoke-interface {p2}, LX/5i8;->h()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1516326
    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {p1, v3, v7, v4, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1516327
    :cond_3
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-ne v2, p2, :cond_4

    .line 1516328
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    instance-of v2, v2, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v2, :cond_8

    .line 1516329
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1516330
    :cond_4
    :goto_3
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1516331
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    .line 1516332
    :cond_5
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    instance-of v3, v3, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-eqz v3, :cond_6

    .line 1516333
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, LX/8GY;->b(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1516334
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    .line 1516335
    :cond_6
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    instance-of v3, v3, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v3, :cond_1

    .line 1516336
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, LX/8GY;->c(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1516337
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    .line 1516338
    :cond_7
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-nez v3, :cond_2

    .line 1516339
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1516340
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_2

    .line 1516341
    :cond_8
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    instance-of v2, v2, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-eqz v2, :cond_9

    .line 1516342
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 1516343
    :cond_9
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    instance-of v2, v2, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v2, :cond_4

    .line 1516344
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3
.end method


# virtual methods
.method public final a(D)V
    .locals 9

    .prologue
    .line 1516345
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-nez v0, :cond_0

    .line 1516346
    :goto_0
    return-void

    .line 1516347
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v0}, LX/5i8;->e()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1516348
    float-to-double v0, v0

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    if-eqz v0, :cond_1

    .line 1516349
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v1}, LX/5i8;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9fG;->a(Ljava/lang/String;)V

    .line 1516350
    :cond_1
    iget v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    .line 1516351
    iget v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->g:I

    if-ge v0, v1, :cond_3

    .line 1516352
    iget v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->g:I

    int-to-double v0, v0

    iget v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-double v2, v2

    div-double p1, v0, v2

    .line 1516353
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516354
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516355
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v1}, LX/5i8;->e()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v2}, LX/5i8;->f()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    .line 1516356
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-static {v2}, LX/5jA;->a(LX/362;)LX/5iE;

    move-result-object v2

    iget v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-double v4, v3

    mul-double/2addr v4, p1

    double-to-float v3, v4

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-interface {v2, v3}, LX/5iE;->g(F)LX/5iE;

    move-result-object v2

    iget v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-double v4, v3

    mul-double/2addr v4, p1

    float-to-double v6, v1

    div-double/2addr v4, v6

    double-to-float v1, v4

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-interface {v2, v1}, LX/5iE;->f(F)LX/5iE;

    move-result-object v1

    invoke-interface {v1}, LX/5iE;->b()LX/5i8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1516357
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1516358
    :cond_3
    iget v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->f:I

    if-le v0, v1, :cond_2

    .line 1516359
    iget v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->f:I

    int-to-double v0, v0

    iget v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-double v2, v2

    div-double p1, v0, v2

    goto :goto_1
.end method

.method public final a(F)V
    .locals 3

    .prologue
    .line 1516360
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-nez v0, :cond_0

    .line 1516361
    :goto_0
    return-void

    .line 1516362
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    if-eqz v0, :cond_1

    .line 1516363
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v1}, LX/5i8;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9fG;->c(Ljava/lang/String;)V

    .line 1516364
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516365
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516366
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-static {v1}, LX/5jA;->a(LX/362;)LX/5iE;

    move-result-object v1

    invoke-interface {v1, p1}, LX/5iE;->j(F)LX/5iE;

    move-result-object v1

    invoke-interface {v1}, LX/5iE;->b()LX/5i8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1516367
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 1516368
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-nez v0, :cond_0

    .line 1516369
    :goto_0
    return-void

    .line 1516370
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    if-eqz v0, :cond_1

    .line 1516371
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v1}, LX/5i8;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9fG;->b(Ljava/lang/String;)V

    .line 1516372
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516373
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516374
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-static {v1}, LX/5jA;->a(LX/362;)LX/5iE;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v2, p1, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, LX/5iE;->i(F)LX/5iE;

    move-result-object v1

    invoke-interface {v1}, LX/5iE;->b()LX/5i8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1516375
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(LX/362;)V
    .locals 1

    .prologue
    .line 1516376
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1516377
    :goto_0
    return-void

    .line 1516378
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1516379
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(LX/362;I)V
    .locals 1

    .prologue
    .line 1516380
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516381
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516382
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1516383
    return-void
.end method

.method public final a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V
    .locals 6

    .prologue
    .line 1516384
    invoke-interface {p1}, LX/5i8;->d()Landroid/net/Uri;

    move-result-object v0

    .line 1516385
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v2, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1516386
    new-instance v2, LX/1Uo;

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v3, LX/1Up;->c:LX/1Up;

    invoke-virtual {v2, v3}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v2

    new-instance v3, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f021af6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0x3e8

    invoke-direct {v3, v4, v5}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1516387
    iput-object v3, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1516388
    move-object v2, v2

    .line 1516389
    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c:Landroid/content/Context;

    invoke-static {v2, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v2

    .line 1516390
    invoke-virtual {v2, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 1516391
    move-object v0, v2

    .line 1516392
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1516393
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516394
    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1516395
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 3
    .param p2    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1516396
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5i8;

    .line 1516397
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p2, :cond_0

    .line 1516398
    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/Canvas;LX/5i8;Landroid/graphics/Rect;)V

    goto :goto_0

    .line 1516399
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1516291
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    .prologue
    .line 1516292
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516293
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1516294
    const/4 v0, 0x1

    .line 1516295
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 1516248
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-nez v0, :cond_0

    .line 1516249
    :goto_0
    return-void

    .line 1516250
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    if-eqz v0, :cond_1

    .line 1516251
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->m:LX/9fG;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v1}, LX/5i8;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9fG;->b(Ljava/lang/String;)V

    .line 1516252
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516253
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516254
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-static {v1}, LX/5jA;->a(LX/362;)LX/5iE;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, p1, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, LX/5iE;->h(F)LX/5iE;

    move-result-object v1

    invoke-interface {v1}, LX/5iE;->b()LX/5i8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1516255
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(LX/362;)V
    .locals 2

    .prologue
    .line 1516258
    instance-of v0, p1, LX/5i8;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/5i8;

    invoke-interface {v0}, LX/5i8;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1516259
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 1516260
    check-cast v0, LX/5i8;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1516261
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516262
    if-eqz v0, :cond_0

    .line 1516263
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516264
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    check-cast p1, LX/5i8;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 1
    .param p2    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1516265
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1516266
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/Canvas;LX/5i8;Landroid/graphics/Rect;)V

    .line 1516267
    :cond_0
    return-void
.end method

.method public final c(LX/362;)D
    .locals 2

    .prologue
    .line 1516268
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516269
    invoke-interface {p1}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    return-wide v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1516270
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->l:Z

    if-eqz v0, :cond_1

    .line 1516271
    :cond_0
    return-void

    .line 1516272
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->l:Z

    .line 1516273
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516274
    invoke-virtual {v0}, LX/1aX;->d()V

    goto :goto_0
.end method

.method public final c(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1516275
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516276
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    .line 1516277
    return-void
.end method

.method public final d(LX/362;)I
    .locals 2

    .prologue
    .line 1516278
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516279
    invoke-interface {p1}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1516280
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->l:Z

    if-nez v0, :cond_1

    .line 1516281
    :cond_0
    return-void

    .line 1516282
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->l:Z

    .line 1516283
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1516284
    if-eqz v0, :cond_2

    .line 1516285
    invoke-virtual {v0}, LX/1aX;->f()V

    goto :goto_0
.end method

.method public final e(LX/362;)I
    .locals 2

    .prologue
    .line 1516286
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516287
    invoke-interface {p1}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1516256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1516257
    return-void
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/5i8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1516288
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1516289
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1516290
    return-void
.end method
