.class public Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/9cG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1516242
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1516243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1516240
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1516241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1516238
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1516239
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1516234
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1516235
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    if-eqz v0, :cond_0

    .line 1516236
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    invoke-interface {v0, p1}, LX/9cG;->a(Landroid/graphics/Canvas;)V

    .line 1516237
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4aa329c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1516244
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1516245
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    if-eqz v1, :cond_0

    .line 1516246
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    invoke-interface {v1}, LX/9cG;->a()V

    .line 1516247
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x4e75c012

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x167bbceb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1516230
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1516231
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    if-eqz v1, :cond_0

    .line 1516232
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    invoke-interface {v1}, LX/9cG;->b()V

    .line 1516233
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7177885d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1516226
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishTemporaryDetach()V

    .line 1516227
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    if-eqz v0, :cond_0

    .line 1516228
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    invoke-interface {v0}, LX/9cG;->a()V

    .line 1516229
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1516222
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 1516223
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    if-eqz v0, :cond_0

    .line 1516224
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    invoke-interface {v0}, LX/9cG;->b()V

    .line 1516225
    :cond_0
    return-void
.end method

.method public setOverlayViewEventListener(LX/9cG;)V
    .locals 0

    .prologue
    .line 1516220
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    .line 1516221
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1516218
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1516219
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    invoke-interface {v1, p1}, LX/9cG;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
