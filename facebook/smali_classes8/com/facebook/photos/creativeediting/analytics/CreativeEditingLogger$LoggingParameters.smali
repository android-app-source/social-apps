.class public final Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:I

.field public c:Z

.field public d:Z

.field public e:J

.field public f:Z

.field public g:Z

.field public h:I

.field public i:I

.field public j:F

.field public k:F

.field public l:Z

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1515958
    new-instance v0, LX/9c6;

    invoke-direct {v0}, LX/9c6;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1515894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1515895
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->c:Z

    .line 1515896
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->d:Z

    .line 1515897
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    .line 1515898
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->g:Z

    .line 1515899
    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    .line 1515900
    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->i:I

    .line 1515901
    iput v1, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    .line 1515902
    iput v1, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->k:F

    .line 1515903
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->l:Z

    .line 1515904
    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    .line 1515905
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1515927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1515928
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->c:Z

    .line 1515929
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->d:Z

    .line 1515930
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    .line 1515931
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->g:Z

    .line 1515932
    iput v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    .line 1515933
    iput v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->i:I

    .line 1515934
    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    .line 1515935
    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->k:F

    .line 1515936
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->l:Z

    .line 1515937
    iput v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    .line 1515938
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->a:Z

    .line 1515939
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->b:I

    .line 1515940
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->c:Z

    .line 1515941
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->d:Z

    .line 1515942
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->e:J

    .line 1515943
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    .line 1515944
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->g:Z

    .line 1515945
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    .line 1515946
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->i:I

    .line 1515947
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    .line 1515948
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->k:F

    .line 1515949
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->l:Z

    .line 1515950
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    .line 1515951
    return-void

    :cond_0
    move v0, v2

    .line 1515952
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1515953
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1515954
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1515955
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1515956
    goto :goto_4

    :cond_5
    move v1, v2

    .line 1515957
    goto :goto_5
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1515926
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1515906
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515907
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515908
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515909
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515910
    iget-wide v4, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->e:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1515911
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515912
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->g:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515913
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515914
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515915
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1515916
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->k:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1515917
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->l:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515918
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1515919
    return-void

    :cond_0
    move v0, v2

    .line 1515920
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1515921
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1515922
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1515923
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1515924
    goto :goto_4

    :cond_5
    move v1, v2

    .line 1515925
    goto :goto_5
.end method
