.class public Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Z

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1516183
    new-instance v0, LX/9cF;

    invoke-direct {v0}, LX/9cF;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1516212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516213
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    .line 1516214
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    .line 1516215
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    .line 1516216
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    .line 1516217
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1516196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516197
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    .line 1516198
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    .line 1516199
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    .line 1516200
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    .line 1516201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->g:I

    .line 1516202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->h:I

    .line 1516203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->a:Z

    .line 1516204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->b:Z

    .line 1516205
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516206
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516207
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516208
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516209
    return-void

    :cond_0
    move v0, v2

    .line 1516210
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1516211
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1516195
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516184
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516185
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516186
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516187
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516188
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516189
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516190
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516191
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516192
    return-void

    :cond_0
    move v0, v2

    .line 1516193
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1516194
    goto :goto_1
.end method
