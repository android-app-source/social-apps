.class public Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:Ljava/lang/String;

.field public final o:LX/5Rz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1516126
    new-instance v0, LX/9cB;

    invoke-direct {v0}, LX/9cB;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/9cC;)V
    .locals 1

    .prologue
    .line 1516109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516110
    iget-boolean v0, p1, LX/9cC;->a:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a:Z

    .line 1516111
    iget-boolean v0, p1, LX/9cC;->b:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->b:Z

    .line 1516112
    iget-boolean v0, p1, LX/9cC;->c:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->c:Z

    .line 1516113
    iget v0, p1, LX/9cC;->d:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    .line 1516114
    iget v0, p1, LX/9cC;->e:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    .line 1516115
    iget v0, p1, LX/9cC;->f:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    .line 1516116
    iget v0, p1, LX/9cC;->g:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    .line 1516117
    iget v0, p1, LX/9cC;->h:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    .line 1516118
    iget v0, p1, LX/9cC;->i:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    .line 1516119
    iget v0, p1, LX/9cC;->j:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->j:I

    .line 1516120
    iget v0, p1, LX/9cC;->k:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->k:I

    .line 1516121
    iget v0, p1, LX/9cC;->l:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    .line 1516122
    iget v0, p1, LX/9cC;->m:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    .line 1516123
    iget-object v0, p1, LX/9cC;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->n:Ljava/lang/String;

    .line 1516124
    iget-object v0, p1, LX/9cC;->o:LX/5Rz;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->o:LX/5Rz;

    .line 1516125
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1516047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516048
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    .line 1516049
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    .line 1516050
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    .line 1516051
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    .line 1516052
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    .line 1516053
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    .line 1516054
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->j:I

    .line 1516055
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->k:I

    .line 1516056
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    .line 1516057
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a:Z

    .line 1516058
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->n:Ljava/lang/String;

    .line 1516059
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->b:Z

    .line 1516060
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->c:Z

    .line 1516061
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    .line 1516062
    const-class v0, LX/5Rz;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rz;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->o:LX/5Rz;

    .line 1516063
    return-void

    :cond_0
    move v0, v2

    .line 1516064
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1516065
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1516066
    goto :goto_2
.end method

.method public static a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;
    .locals 2

    .prologue
    .line 1516108
    new-instance v0, LX/9cC;

    invoke-direct {v0, p0}, LX/9cC;-><init>(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)V

    return-object v0
.end method

.method public static newBuilder()LX/9cC;
    .locals 2

    .prologue
    .line 1516107
    new-instance v0, LX/9cC;

    invoke-direct {v0}, LX/9cC;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1516106
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1516105
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1516086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1516087
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516088
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "edit gallery entries:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516089
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "crop entries:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516090
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isCropped:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516091
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isRotated:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516092
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "text entries:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516093
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#texts:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516094
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#doodles: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516095
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sticker entries:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516096
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#stickers:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516097
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "filter entries in gallery:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516098
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "filter swipes:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516099
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "filter name:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516100
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isDeleted:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516101
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isPublished:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516102
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "edit gallery entry point:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->o:LX/5Rz;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516103
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516067
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516068
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516069
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516070
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516071
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516072
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516073
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516074
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516075
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516076
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516077
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1516078
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516079
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->c:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516080
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516081
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->o:LX/5Rz;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1516082
    return-void

    :cond_0
    move v0, v2

    .line 1516083
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1516084
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1516085
    goto :goto_2
.end method
