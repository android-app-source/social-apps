.class public Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1516179
    new-instance v0, LX/9cE;

    invoke-direct {v0}, LX/9cE;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1516174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516175
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    .line 1516176
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    .line 1516177
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    .line 1516178
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1516153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516154
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    .line 1516155
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    .line 1516156
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    .line 1516157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->e:I

    .line 1516158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->f:I

    .line 1516159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->a:Z

    .line 1516160
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516161
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516162
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1516163
    return-void

    .line 1516164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1516173
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1516165
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516166
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516167
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516168
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516169
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516170
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1516171
    return-void

    .line 1516172
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
