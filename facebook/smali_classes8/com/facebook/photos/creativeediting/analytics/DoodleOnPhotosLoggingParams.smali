.class public Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Z

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1516140
    new-instance v0, LX/9cD;

    invoke-direct {v0}, LX/9cD;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1516139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1516141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->c:I

    .line 1516143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->d:I

    .line 1516144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->e:I

    .line 1516145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->a:Z

    .line 1516146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->b:Z

    .line 1516147
    return-void

    :cond_0
    move v0, v2

    .line 1516148
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1516149
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1516138
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516130
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516131
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516132
    iget v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516133
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516134
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1516135
    return-void

    :cond_0
    move v0, v2

    .line 1516136
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1516137
    goto :goto_1
.end method
