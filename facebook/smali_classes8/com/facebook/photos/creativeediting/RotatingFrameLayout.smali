.class public Lcom/facebook/photos/creativeediting/RotatingFrameLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:D

.field public d:D

.field private e:LX/0wd;

.field private f:Z

.field public g:Z

.field private h:I

.field private i:I

.field private final j:LX/0xi;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1515758
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1515681
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1515682
    new-instance v0, LX/9by;

    invoke-direct {v0, p0}, LX/9by;-><init>(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->j:LX/0xi;

    .line 1515683
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f()V

    .line 1515684
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1515754
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1515755
    new-instance v0, LX/9by;

    invoke-direct {v0, p0}, LX/9by;-><init>(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->j:LX/0xi;

    .line 1515756
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f()V

    .line 1515757
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1515750
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1515751
    new-instance v0, LX/9by;

    invoke-direct {v0, p0}, LX/9by;-><init>(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->j:LX/0xi;

    .line 1515752
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f()V

    .line 1515753
    return-void
.end method

.method public static synthetic a(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;D)D
    .locals 3

    .prologue
    .line 1515749
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    rem-double/2addr v0, p1

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    return-wide v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a:LX/0wW;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Z)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1515741
    if-nez p1, :cond_0

    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    const-wide v2, 0x4066800000000000L    # 180.0

    rem-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 1515742
    :cond_0
    invoke-virtual {p0, v4}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setScaleX(F)V

    .line 1515743
    invoke-virtual {p0, v4}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setScaleY(F)V

    .line 1515744
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->requestLayout()V

    .line 1515745
    return-void

    .line 1515746
    :cond_1
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getScaleForRotation()F

    move-result v0

    .line 1515747
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setScaleX(F)V

    .line 1515748
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setScaleY(F)V

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1515739
    const-class v0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-static {v0, p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1515740
    return-void
.end method

.method private getScaleForRotation()F
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1515732
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1515733
    iget v1, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->i:I

    iget v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->h:I

    if-le v1, v2, :cond_2

    .line 1515734
    cmpl-float v1, v0, v3

    if-lez v1, :cond_0

    div-float v0, v3, v0

    .line 1515735
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f:Z

    if-eqz v1, :cond_1

    .line 1515736
    div-float v0, v3, v0

    .line 1515737
    :cond_1
    return v0

    .line 1515738
    :cond_2
    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    div-float v0, v3, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(D)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1515726
    sget-object v0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1515727
    :goto_0
    return-void

    .line 1515728
    :cond_0
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->g:Z

    .line 1515729
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->c:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    .line 1515730
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->j:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->e:LX/0wd;

    .line 1515731
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->e:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 1515723
    iput p1, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->h:I

    .line 1515724
    iput p2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->i:I

    .line 1515725
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1515718
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->g:Z

    .line 1515719
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->c:D

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    .line 1515720
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setRotation(F)V

    .line 1515721
    invoke-static {p0, v2}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a$redex0(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Z)V

    .line 1515722
    return-void
.end method

.method public getFinalRotation()I
    .locals 4

    .prologue
    .line 1515717
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/16 v0, 0x2c

    const v1, 0x650b2879

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1515713
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1515714
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f:Z

    .line 1515715
    const v0, -0x327ad4bb    # -2.7927568E8f

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 1515716
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1515707
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1515708
    iput-boolean v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f:Z

    .line 1515709
    :goto_0
    invoke-static {p0, v2}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a$redex0(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Z)V

    .line 1515710
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1515711
    return-void

    .line 1515712
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f:Z

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1515695
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 1515696
    check-cast p1, Landroid/os/Bundle;

    .line 1515697
    const-string v0, "rotate_by"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->c:D

    .line 1515698
    const-string v0, "is_landscape"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f:Z

    .line 1515699
    const-string v0, "original_image_width"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "original_image_height"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a(II)V

    .line 1515700
    const-string v0, "measured_width"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "measured_height"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setMeasuredDimension(II)V

    .line 1515701
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    double-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setRotation(F)V

    .line 1515702
    const-string v0, "is_rotated"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->g:Z

    .line 1515703
    const-string v0, "super_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    .line 1515704
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a$redex0(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Z)V

    .line 1515705
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1515706
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 1515685
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1515686
    const-string v1, "super_state"

    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1515687
    const-string v1, "rotate_by"

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 1515688
    const-string v1, "original_image_width"

    iget v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1515689
    const-string v1, "original_image_height"

    iget v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1515690
    const-string v1, "measured_width"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1515691
    const-string v1, "measured_height"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1515692
    const-string v1, "is_landscape"

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->f:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1515693
    const-string v1, "is_rotated"

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1515694
    return-object v0
.end method
