.class public Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;
.super Landroid/widget/EditText;
.source ""


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field public a:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Landroid/text/TextWatcher;

.field public g:LX/9de;

.field public h:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1518374
    const-class v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1518375
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1518376
    new-instance v0, LX/9dc;

    invoke-direct {v0, p0}, LX/9dc;-><init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->f:Landroid/text/TextWatcher;

    .line 1518377
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->e()V

    .line 1518378
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1518308
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1518309
    new-instance v0, LX/9dc;

    invoke-direct {v0, p0}, LX/9dc;-><init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->f:Landroid/text/TextWatcher;

    .line 1518310
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->e()V

    .line 1518311
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1518379
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1518380
    new-instance v0, LX/9dc;

    invoke-direct {v0, p0}, LX/9dc;-><init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->f:Landroid/text/TextWatcher;

    .line 1518381
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->e()V

    .line 1518382
    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;LX/1Er;LX/0w3;LX/0ad;LX/03V;)V
    .locals 0

    .prologue
    .line 1518388
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->a:LX/1Er;

    iput-object p2, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->b:LX/0w3;

    iput-object p3, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->c:LX/0ad;

    iput-object p4, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->d:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-static {v3}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v0

    check-cast v0, LX/1Er;

    invoke-static {v3}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v1

    check-cast v1, LX/0w3;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->a(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;LX/1Er;LX/0w3;LX/0ad;LX/03V;)V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1518383
    const-class v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-static {v0, p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1518384
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->h:Landroid/view/inputmethod/InputMethodManager;

    .line 1518385
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getOnEditorActionListener()Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1518386
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->f:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1518387
    return-void
.end method

.method private getOnEditorActionListener()Landroid/widget/TextView$OnEditorActionListener;
    .locals 1

    .prologue
    .line 1518369
    new-instance v0, LX/9dd;

    invoke-direct {v0, p0}, LX/9dd;-><init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1518370
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLineCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLineHeight()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getMeasuredHeight()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1518371
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getTextSize()F

    move-result v1

    const v2, 0x3f666666    # 0.9f

    mul-float/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextSize(IF)V

    .line 1518372
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->requestLayout()V

    .line 1518373
    :cond_0
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1518344
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setDrawingCacheEnabled(Z)V

    .line 1518345
    const/4 v8, 0x0

    .line 1518346
    :try_start_0
    new-instance v2, Landroid/text/TextPaint;

    const/4 v0, 0x3

    invoke-direct {v2, v0}, Landroid/text/TextPaint;-><init>(I)V

    .line 1518347
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getCurrentTextColor()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1518348
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getTextSize()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1518349
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getMeasuredHeight()I

    move-result v0

    mul-int/lit8 v9, v0, 0x2

    .line 1518350
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getMeasuredWidth()I

    move-result v0

    mul-int/lit8 v3, v0, 0x2

    .line 1518351
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    instance-of v0, v0, Landroid/text/DynamicLayout;

    if-eqz v0, :cond_0

    .line 1518352
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    check-cast v0, Landroid/text/DynamicLayout;

    invoke-static {v2, v0}, LX/8Gf;->a(Landroid/text/TextPaint;Landroid/text/DynamicLayout;)I

    move-result v3

    .line 1518353
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1518354
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v9, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1518355
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1518356
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1518357
    :try_start_1
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1518358
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v2, v0, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1518359
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    return-void

    .line 1518360
    :cond_0
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1518361
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Layout class: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1518362
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ", text class: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1518363
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->d:LX/03V;

    sget-object v4, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1518364
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_1

    .line 1518365
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_1
    throw v0

    .line 1518366
    :cond_2
    :try_start_3
    const-string v0, "Layout class: null"

    goto :goto_1

    .line 1518367
    :cond_3
    const-string v0, ", text class: null"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1518368
    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1518337
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->clearFocus()V

    .line 1518338
    invoke-virtual {p0, v2}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setEnabled(Z)V

    .line 1518339
    invoke-virtual {p0, v2}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setFocusable(Z)V

    .line 1518340
    invoke-virtual {p0, v2}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setFocusableInTouchMode(Z)V

    .line 1518341
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->h:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1518342
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->g:LX/9de;

    invoke-interface {v0}, LX/9de;->a()V

    .line 1518343
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1518334
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->b:LX/0w3;

    .line 1518335
    iget-boolean p0, v0, LX/0w3;->f:Z

    move v0, p0

    .line 1518336
    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1518327
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setEnabled(Z)V

    .line 1518328
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setFocusable(Z)V

    .line 1518329
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setFocusableInTouchMode(Z)V

    .line 1518330
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->bringToFront()V

    .line 1518331
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->requestFocus()Z

    .line 1518332
    new-instance v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText$3;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText$3;-><init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->post(Ljava/lang/Runnable;)Z

    .line 1518333
    return-void
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1518320
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->b:LX/0w3;

    .line 1518321
    iget-boolean p1, v0, LX/0w3;->f:Z

    move v0, p1

    .line 1518322
    if-eqz v0, :cond_1

    .line 1518323
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->b()V

    .line 1518324
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1518325
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    move v0, v1

    .line 1518326
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p2}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1518317
    new-instance v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText$2;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText$2;-><init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->post(Ljava/lang/Runnable;)Z

    .line 1518318
    invoke-super/range {p0 .. p5}, Landroid/widget/EditText;->onLayout(ZIIII)V

    .line 1518319
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1b46e6dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1518314
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->b:LX/0w3;

    invoke-virtual {v1, p0, p2}, LX/0w3;->a(Landroid/view/View;I)V

    .line 1518315
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onMeasure(II)V

    .line 1518316
    const/16 v1, 0x2d

    const v2, 0x43fd3ad4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCallBack(LX/9de;)V
    .locals 0

    .prologue
    .line 1518312
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->g:LX/9de;

    .line 1518313
    return-void
.end method
