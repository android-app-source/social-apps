.class public Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:LX/8GN;

.field public final e:LX/1Ad;

.field public final f:LX/9dC;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9cr;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Landroid/net/Uri;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:I

.field public p:LX/5iG;

.field public q:LX/5iG;

.field public r:LX/5iG;

.field public s:LX/1aX;

.field public t:LX/1aX;

.field public u:LX/1aX;

.field public v:LX/1cC;

.field public w:LX/1cC;

.field public x:LX/1cC;

.field public y:LX/9cy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1517633
    const-class v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const-string v1, "creative_editing_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1517634
    const-class v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Ad;LX/8GN;LX/9dC;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1517625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1517626
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    .line 1517627
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    .line 1517628
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    .line 1517629
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->d:LX/8GN;

    .line 1517630
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    .line 1517631
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021af6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->h:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 1517632
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
    .locals 1

    .prologue
    .line 1517574
    invoke-static {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->b(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;
    .locals 6
    .param p0    # Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1517597
    if-nez p2, :cond_1

    .line 1517598
    const/4 p3, 0x0

    .line 1517599
    :cond_0
    return-object p3

    .line 1517600
    :cond_1
    if-eqz p3, :cond_2

    .line 1517601
    iget-object v0, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1517602
    iget-object v1, p3, LX/5iG;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1517603
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1517604
    :cond_2
    new-instance p3, LX/5iG;

    .line 1517605
    iget-object v0, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1517606
    iget-object v1, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1517607
    invoke-direct {p3, p1, v0, v1}, LX/5iG;-><init>(LX/1aX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1517608
    :cond_3
    iget-object v0, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v0, v0

    .line 1517609
    sget-object v1, LX/5jI;->FRAME:LX/5jI;

    if-ne v0, v1, :cond_0

    .line 1517610
    iget-object v0, p3, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1517611
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1517612
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v4

    .line 1517613
    new-instance v5, LX/1Uo;

    iget-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {v5, p1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object p1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v5, p1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v5

    .line 1517614
    if-nez v4, :cond_4

    .line 1517615
    iget-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->h:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 1517616
    iput-object p1, v5, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1517617
    iget-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->h:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 1517618
    iput-object p1, v5, LX/1Uo;->j:Landroid/graphics/drawable/Drawable;

    .line 1517619
    :cond_4
    iget-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    invoke-virtual {p1, v4}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object p1

    sget-object p2, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p1

    invoke-virtual {p1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p1

    .line 1517620
    invoke-virtual {v5}, LX/1Uo;->u()LX/1af;

    move-result-object v5

    iget-object p2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-static {v5, p2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v5

    .line 1517621
    invoke-virtual {v5, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 1517622
    move-object v4, v5

    .line 1517623
    invoke-virtual {p3, v0, v4}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/1aX;)V

    .line 1517624
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
    .locals 8

    .prologue
    .line 1517592
    new-instance v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v2

    check-cast v2, LX/8GN;

    .line 1517593
    new-instance v6, LX/9dC;

    invoke-static {p0}, LX/1G5;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {p0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    const/16 v7, 0x2e33

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct {v6, v3, v5, v7}, LX/9dC;-><init>(LX/0TD;Lcom/facebook/photos/imageprocessing/FiltersEngine;LX/0Or;)V

    .line 1517594
    move-object v3, v6

    .line 1517595
    check-cast v3, LX/9dC;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;-><init>(Landroid/content/Context;LX/1Ad;LX/8GN;LX/9dC;)V

    .line 1517596
    return-object v4
.end method

.method public static g(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1517580
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v0}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1517581
    :goto_0
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v3}, LX/9cy;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 1517582
    :goto_1
    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v4}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1517583
    :goto_2
    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->j:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    .line 1517584
    iget-boolean v4, v2, LX/9dC;->f:Z

    if-eqz v4, :cond_6

    iget-boolean v4, v2, LX/9dC;->g:Z

    if-eqz v4, :cond_6

    iget-boolean v4, v2, LX/9dC;->h:Z

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    :goto_3
    move v2, v4

    .line 1517585
    if-eqz v2, :cond_1

    :cond_0
    if-nez v0, :cond_1

    if-nez v3, :cond_1

    if-eqz v1, :cond_5

    .line 1517586
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1517587
    goto :goto_0

    :cond_3
    move v3, v2

    .line 1517588
    goto :goto_1

    :cond_4
    move v1, v2

    .line 1517589
    goto :goto_2

    .line 1517590
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cr;

    .line 1517591
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    invoke-interface {v0, v2, v3, v4}, LX/9cr;->b(LX/5iG;LX/5iG;LX/5iG;)V

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public static h(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)LX/1cC;
    .locals 1

    .prologue
    .line 1517579
    new-instance v0, LX/9d8;

    invoke-direct {v0, p0}, LX/9d8;-><init>(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/9cr;)V
    .locals 1

    .prologue
    .line 1517575
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1517576
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1517577
    return-void

    .line 1517578
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1517563
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->j:Z

    if-ne v0, p1, :cond_0

    .line 1517564
    :goto_0
    return-void

    .line 1517565
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->j:Z

    .line 1517566
    if-eqz p1, :cond_2

    .line 1517567
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->k:Z

    .line 1517568
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1517569
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    invoke-virtual {v0}, LX/9dC;->e()V

    goto :goto_0

    .line 1517570
    :cond_1
    invoke-static {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V

    goto :goto_0

    .line 1517571
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->k:Z

    .line 1517572
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    invoke-virtual {v0}, LX/9dC;->d()V

    .line 1517573
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->i:Z

    goto :goto_0
.end method

.method public final b(LX/9cr;)V
    .locals 1

    .prologue
    .line 1517560
    if-nez p1, :cond_0

    .line 1517561
    :goto_0
    return-void

    .line 1517562
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
