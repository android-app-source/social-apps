.class public final Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9dK;


# direct methods
.method public constructor <init>(LX/9dK;)V
    .locals 0

    .prologue
    .line 1518100
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;->a:LX/9dK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1518101
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;->a:LX/9dK;

    .line 1518102
    iget-object v1, v0, LX/9dK;->g:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1518103
    iget-object v1, v0, LX/9dK;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, LX/9dK;->c()F

    move-result v2

    invoke-static {v0, v2}, LX/9dK;->b(LX/9dK;F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 1518104
    iget-object v1, v0, LX/9dK;->g:Landroid/widget/ImageView;

    const/high16 v4, 0x40000000    # 2.0f

    .line 1518105
    iget-object v2, v0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, v0, LX/9dK;->g:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    move v2, v2

    .line 1518106
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 1518107
    iget-object v1, v0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->invalidate()V

    .line 1518108
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1518109
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;->a:LX/9dK;

    .line 1518110
    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v3, v4, v5}, LX/9dK;->a(LX/9dK;FF)Landroid/animation/Animator;

    move-result-object v4

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v4

    .line 1518111
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1518112
    move-object v3, v4

    .line 1518113
    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;->a:LX/9dK;

    invoke-static {v3}, LX/9dK;->p(LX/9dK;)Landroid/animation/Animator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1518114
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1518115
    return-void
.end method
