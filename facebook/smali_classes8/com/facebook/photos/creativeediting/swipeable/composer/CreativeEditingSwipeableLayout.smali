.class public Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

.field public c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/text/TextPaint;

.field private e:Landroid/graphics/Paint;

.field private f:I

.field private final g:Landroid/graphics/RectF;

.field private h:Landroid/graphics/drawable/Drawable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/9cu;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/9cw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1517373
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1517374
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h()V

    .line 1517375
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1517445
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1517446
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h()V

    .line 1517447
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1517448
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1517449
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->g:Landroid/graphics/RectF;

    .line 1517450
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h()V

    .line 1517451
    return-void
.end method

.method private a(Landroid/graphics/Canvas;IF)V
    .locals 6

    .prologue
    .line 1517452
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1517453
    const/4 v2, 0x0

    int-to-float v4, p2

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p3

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1517454
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1517455
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a:LX/0ad;

    return-void
.end method

.method private a(FF)Z
    .locals 3

    .prologue
    .line 1517456
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    .line 1517457
    sub-float v1, p2, p1

    iget v2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->f:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;LX/5jK;F)V
    .locals 7

    .prologue
    .line 1517458
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1517459
    :cond_0
    :goto_0
    return-void

    .line 1517460
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, p3, v0

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 1517461
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->b()LX/5iG;

    move-result-object v0

    .line 1517462
    iget-object v2, v0, LX/5iG;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1517463
    invoke-virtual {p2}, LX/5jK;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1517464
    rsub-int v1, v1, 0xff

    .line 1517465
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->d()LX/5iG;

    move-result-object v0

    .line 1517466
    iget-object v2, v0, LX/5iG;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1517467
    :cond_2
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1517468
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1517469
    invoke-virtual {p0, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1517470
    iget v3, v2, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40400000    # 3.0f

    iget-object v6, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v6

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-direct {p0, v3, v4}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1517471
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 1517472
    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 1517473
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1517474
    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v0, v5, v6, v1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1517475
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v4, v1

    .line 1517476
    iget v1, v2, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    .line 1517477
    :goto_1
    int-to-float v2, v4

    iget v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->f:I

    add-int/2addr v1, v4

    int-to-float v1, v1

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v2, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1517478
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    .line 1517479
    :cond_3
    iget v1, v2, Landroid/graphics/Rect;->top:I

    goto :goto_1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1517480
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->c()LX/5iG;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1517481
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->c()LX/5iG;

    move-result-object v0

    .line 1517482
    iget-object v1, v0, LX/5iG;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1517483
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1517484
    const/16 v0, 0x4000

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->sendAccessibilityEvent(I)V

    .line 1517485
    :cond_0
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1517486
    const-class v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-static {v0, p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1517487
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03025c

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1517488
    const v0, 0x7f0d08f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    .line 1517489
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1517490
    new-instance v1, LX/0zw;

    const v0, 0x7f0d08f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->c:LX/0zw;

    .line 1517491
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    .line 1517492
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1517493
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1517494
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->d:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0c10

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1517495
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c11

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->f:I

    .line 1517496
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->e:Landroid/graphics/Paint;

    .line 1517497
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1517498
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1517499
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1517500
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->c()LX/5iG;

    move-result-object v0

    .line 1517501
    if-nez v0, :cond_0

    .line 1517502
    :goto_0
    return-void

    .line 1517503
    :cond_0
    iget-object v1, v0, LX/5iG;->d:LX/1aX;

    move-object v1, v1

    .line 1517504
    if-eqz v1, :cond_2

    .line 1517505
    iget-object v1, v0, LX/5iG;->d:LX/1aX;

    move-object v1, v1

    .line 1517506
    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    .line 1517507
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getWidth()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getHeight()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1517508
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1517509
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->requestLayout()V

    .line 1517510
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->invalidate()V

    .line 1517511
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1517512
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setActualImageBounds(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1517513
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->b()LX/5iG;

    move-result-object v0

    .line 1517514
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->d()LX/5iG;

    move-result-object v1

    .line 1517515
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v2}, LX/9cw;->c()LX/5iG;

    .line 1517516
    if-eqz v0, :cond_0

    .line 1517517
    iget-object v2, v0, LX/5iG;->d:LX/1aX;

    move-object v2, v2

    .line 1517518
    if-eqz v2, :cond_0

    .line 1517519
    iget-object v2, v0, LX/5iG;->d:LX/1aX;

    move-object v2, v2

    .line 1517520
    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v3, "left item\'s drawable hierarchy was not properly set up"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517521
    iget-object v2, v0, LX/5iG;->d:LX/1aX;

    move-object v0, v2

    .line 1517522
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1517523
    :cond_0
    if-eqz v1, :cond_1

    .line 1517524
    iget-object v0, v1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1517525
    if-eqz v0, :cond_1

    .line 1517526
    iget-object v0, v1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1517527
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string v2, "right item\'s drawable hierarchy was not properly set up"

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517528
    iget-object v0, v1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1517529
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1517530
    :cond_1
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 23

    .prologue
    .line 1517418
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 1517419
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1517420
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setActualImageBounds(Landroid/graphics/RectF;)V

    .line 1517421
    invoke-super/range {p0 .. p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1517422
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    if-nez v3, :cond_2

    .line 1517423
    :cond_1
    :goto_0
    return-void

    .line 1517424
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredHeight()I

    move-result v3

    .line 1517425
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v4}, LX/9cw;->e()LX/5jK;

    move-result-object v4

    invoke-virtual {v4}, LX/5jK;->f()Z

    move-result v4

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v4}, LX/9cw;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1517426
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v4}, LX/9cw;->f()F

    move-result v22

    .line 1517427
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v4}, LX/9cw;->e()LX/5jK;

    move-result-object v4

    .line 1517428
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v5}, LX/9cw;->h()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1517429
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-direct {v0, v1, v3, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a(Landroid/graphics/Canvas;IF)V

    .line 1517430
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->b()LX/5iG;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v5}, LX/9cw;->c()LX/5iG;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v6}, LX/9cw;->d()LX/5iG;

    move-result-object v6

    invoke-static {v4, v3, v5, v6}, LX/8GI;->getType(LX/5jK;LX/5iG;LX/5iG;LX/5iG;)LX/8GI;

    move-result-object v3

    .line 1517431
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredWidth()I

    move-result v18

    .line 1517432
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredHeight()I

    move-result v10

    .line 1517433
    sget-object v5, LX/9d7;->a:[I

    invoke-virtual {v3}, LX/8GI;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1517434
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->b()LX/5iG;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->c()LX/5iG;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->d()LX/5iG;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->h()Z

    move-result v3

    if-eqz v3, :cond_4

    move/from16 v10, v18

    :cond_4
    move/from16 v0, v22

    float-to-int v11, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->h()Z

    move-result v12

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v12}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5jK;Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;LX/5iG;LX/5iG;LX/5iG;Landroid/graphics/RectF;IIZ)V

    .line 1517435
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b(Landroid/graphics/Canvas;LX/5jK;F)V

    goto/16 :goto_0

    .line 1517436
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->b()LX/5iG;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->d()LX/5iG;

    move-result-object v7

    move/from16 v0, v22

    float-to-int v11, v0

    move-object/from16 v5, p1

    move-object v8, v4

    move/from16 v9, v18

    invoke-static/range {v5 .. v11}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;LX/5iG;LX/5jK;III)V

    .line 1517437
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b(Landroid/graphics/Canvas;LX/5jK;F)V

    goto/16 :goto_0

    .line 1517438
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->b()LX/5iG;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->c()LX/5iG;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->d()LX/5iG;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v17

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->h()Z

    move-result v21

    move-object/from16 v11, p1

    move-object v12, v4

    move/from16 v19, v10

    invoke-static/range {v11 .. v21}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5jK;Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;LX/5iG;LX/5iG;LX/5iG;Landroid/graphics/RectF;IIIZ)V

    .line 1517439
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b(Landroid/graphics/Canvas;LX/5jK;F)V

    goto/16 :goto_0

    .line 1517440
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->b()LX/5iG;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->d()LX/5iG;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getActualImageBounds()Landroid/graphics/RectF;

    move-result-object v8

    move/from16 v0, v22

    float-to-int v11, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v3}, LX/9cw;->h()Z

    move-result v12

    move-object/from16 v3, p1

    move/from16 v9, v18

    invoke-static/range {v3 .. v12}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5jK;Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;LX/5iG;LX/5iG;Landroid/graphics/RectF;IIIZ)V

    .line 1517441
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b(Landroid/graphics/Canvas;LX/5jK;F)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1517442
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a()V

    .line 1517443
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->g()V

    .line 1517444
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1517415
    iput-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    .line 1517416
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1517417
    return-void
.end method

.method public getActualImageBounds()Landroid/graphics/RectF;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1517407
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->c()LX/5iG;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->c()LX/5iG;

    move-result-object v0

    .line 1517408
    iget-object v1, v0, LX/5iG;->d:LX/1aX;

    move-object v0, v1

    .line 1517409
    if-eqz v0, :cond_0

    .line 1517410
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v0}, LX/9cw;->c()LX/5iG;

    move-result-object v0

    .line 1517411
    iget-object v1, v0, LX/5iG;->d:LX/1aX;

    move-object v0, v1

    .line 1517412
    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1517413
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->g:Landroid/graphics/RectF;

    return-object v0

    .line 1517414
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->g:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method

.method public getEventListener()LX/9cu;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1517406
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    return-object v0
.end method

.method public getNuxView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1517405
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->c:LX/0zw;

    return-object v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1517402
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1517403
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->invalidate()V

    .line 1517404
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5ea1fb05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1517398
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1517399
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    if-eqz v1, :cond_0

    .line 1517400
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    invoke-interface {v1}, LX/9cu;->b()V

    .line 1517401
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x25fad71c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x240ffe8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1517394
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1517395
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    if-eqz v1, :cond_0

    .line 1517396
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    invoke-interface {v1}, LX/9cu;->a()V

    .line 1517397
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x4c3a8bea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1517390
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1517391
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1517392
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1517393
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1517386
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 1517387
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    if-eqz v0, :cond_0

    .line 1517388
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    invoke-interface {v0}, LX/9cu;->a()V

    .line 1517389
    :cond_0
    return-void
.end method

.method public setDataProvider(LX/9cw;)V
    .locals 0

    .prologue
    .line 1517384
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    .line 1517385
    return-void
.end method

.method public setEventListener(LX/9cu;)V
    .locals 0

    .prologue
    .line 1517382
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    .line 1517383
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1517376
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    if-eqz v1, :cond_3

    .line 1517377
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->b()LX/5iG;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->b()LX/5iG;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1517378
    :cond_0
    :goto_0
    return v0

    .line 1517379
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->c()LX/5iG;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->c()LX/5iG;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1517380
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->d()LX/5iG;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    invoke-interface {v1}, LX/9cw;->d()LX/5iG;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1517381
    :cond_3
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
