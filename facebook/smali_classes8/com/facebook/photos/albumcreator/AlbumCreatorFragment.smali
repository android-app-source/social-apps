.class public Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:Lcom/facebook/resources/ui/FbEditText;

.field public a:LX/93y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/93p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/93j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8RJ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9ac;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/9af;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/93x;

.field public final i:LX/93w;

.field public final j:LX/93q;

.field public final k:LX/93q;

.field public final l:LX/93q;

.field public final m:LX/8Q5;

.field public final n:LX/93X;

.field public o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public p:LX/93Q;

.field public q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public r:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public s:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Z

.field public w:Lcom/facebook/fbui/glyph/GlyphView;

.field public x:Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

.field public y:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

.field public z:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1514109
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1514110
    new-instance v0, LX/9ak;

    invoke-direct {v0, p0}, LX/9ak;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->h:LX/93x;

    .line 1514111
    new-instance v0, LX/9al;

    invoke-direct {v0, p0}, LX/9al;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->i:LX/93w;

    .line 1514112
    new-instance v0, LX/9am;

    invoke-direct {v0, p0}, LX/9am;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->j:LX/93q;

    .line 1514113
    new-instance v0, LX/9an;

    invoke-direct {v0, p0}, LX/9an;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->k:LX/93q;

    .line 1514114
    new-instance v0, LX/9ao;

    invoke-direct {v0, p0}, LX/9ao;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->l:LX/93q;

    .line 1514115
    new-instance v0, LX/9ap;

    invoke-direct {v0, p0}, LX/9ap;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->m:LX/8Q5;

    .line 1514116
    new-instance v0, LX/9aq;

    invoke-direct {v0, p0}, LX/9aq;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->n:LX/93X;

    .line 1514117
    iput-boolean v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->u:Z

    .line 1514118
    iput-boolean v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->v:Z

    return-void
.end method

.method private static a(Lcom/facebook/resources/ui/FbEditText;)Z
    .locals 1

    .prologue
    .line 1514131
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1514125
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v0, :cond_0

    .line 1514126
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->w:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0510

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1514127
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->g:LX/9af;

    .line 1514128
    sget-object v1, LX/9ae;->ALBUM_LOCATION_CHANGED:LX/9ae;

    invoke-static {v1}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1514129
    :goto_0
    return-void

    .line 1514130
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->w:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0a13

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public static o(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V
    .locals 3

    .prologue
    .line 1514120
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1514121
    :cond_0
    :goto_0
    return-void

    .line 1514122
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1514123
    if-eqz v0, :cond_0

    .line 1514124
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public static p(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z
    .locals 1

    .prologue
    .line 1514119
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->a(Lcom/facebook/resources/ui/FbEditText;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->A:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->a(Lcom/facebook/resources/ui/FbEditText;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->u:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z
    .locals 1

    .prologue
    .line 1514028
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z
    .locals 2

    .prologue
    .line 1514108
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1514132
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1514133
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    const-class v3, LX/93y;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/93y;

    const-class v4, LX/93p;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/93p;

    const-class v5, LX/93j;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/93j;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    const/16 v7, 0x2fe5

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p1, 0x2dfb

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/9af;->b(LX/0QB;)LX/9af;

    move-result-object v0

    check-cast v0, LX/9af;

    iput-object v3, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->a:LX/93y;

    iput-object v4, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->b:LX/93p;

    iput-object v5, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->c:LX/93j;

    iput-object v6, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->e:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->f:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->g:LX/9af;

    .line 1514134
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1a6218a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1514061
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1514062
    if-eqz p1, :cond_2

    .line 1514063
    const-string v1, "savedPrivacyKey"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1514064
    const-string v1, "savedAlbumTitle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1514065
    if-eqz v1, :cond_0

    .line 1514066
    iget-object v2, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1514067
    :cond_0
    const-string v1, "savedAlbumDescription"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1514068
    if-eqz v1, :cond_1

    .line 1514069
    iget-object v2, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->A:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1514070
    :cond_1
    const-string v1, "savedLocation"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1514071
    const-string v1, "savedHasPrivacyChanged"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->u:Z

    .line 1514072
    const-string v1, "savedHasUserInteracted"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->v:Z

    .line 1514073
    const-string v1, "savedComposerTargetData"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1514074
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->g:LX/9af;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1514075
    iput-object v2, v1, LX/9af;->b:Ljava/lang/String;

    .line 1514076
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1514077
    iget-object v2, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->g:LX/9af;

    .line 1514078
    sget-object v4, LX/9ae;->ALBUM_CREATOR_OPENED:LX/9ae;

    invoke-static {v4}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "source"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v2, v4}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1514079
    const v1, 0x7f0d00bc

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1514080
    const v2, 0x7f08244a

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1514081
    new-instance v2, LX/9ai;

    invoke-direct {v2, p0}, LX/9ai;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1514082
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v4, 0x7f08244b

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1514083
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 1514084
    move-object v2, v2

    .line 1514085
    const/4 v4, 0x1

    .line 1514086
    iput-boolean v4, v2, LX/108;->q:Z

    .line 1514087
    move-object v2, v2

    .line 1514088
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1514089
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1514090
    new-instance v2, LX/9aj;

    invoke-direct {v2, p0}, LX/9aj;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1514091
    const v1, 0x7f0d0512

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->w:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1514092
    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->w:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/9ah;

    invoke-direct {v2, p0}, LX/9ah;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1514093
    invoke-static {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1514094
    iget-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->b:LX/93p;

    iget-object v5, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->k:LX/93q;

    iget-object v6, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1514095
    iget-object v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v6, v7

    .line 1514096
    invoke-virtual {v4, v5, v6}, LX/93p;->a(LX/93q;Ljava/lang/String;)LX/93o;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->p:LX/93Q;

    .line 1514097
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 1514098
    const-string v2, "AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    move-object v1, v1

    .line 1514099
    if-eqz v1, :cond_3

    .line 1514100
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1514101
    :cond_3
    if-eqz p1, :cond_4

    .line 1514102
    invoke-direct {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->d()V

    .line 1514103
    :cond_4
    const/16 v1, 0x2b

    const v2, -0x38963754

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1514104
    :cond_5
    invoke-static {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1514105
    iget-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->c:LX/93j;

    iget-object v5, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->l:LX/93q;

    iget-object v6, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v6, v6, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/93j;->a(LX/93q;Ljava/lang/Long;)LX/93i;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->p:LX/93Q;

    goto :goto_0

    .line 1514106
    :cond_6
    iget-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->a:LX/93y;

    iget-object v5, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->j:LX/93q;

    iget-object v6, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->i:LX/93w;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->h:LX/93x;

    invoke-virtual {v4, v5, v6, v7, v8}, LX/93y;->a(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/Object;)LX/93t;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->p:LX/93Q;

    .line 1514107
    iget-object v4, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->x:Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    new-instance v5, LX/9ag;

    invoke-direct {v5, p0}, LX/9ag;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    invoke-virtual {v4, v5}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1514057
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1514058
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1514059
    invoke-direct {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->d()V

    .line 1514060
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x78f8992b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1514044
    const v0, 0x7f0300c9

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1514045
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1514046
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "extra_composer_target_data"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1514047
    invoke-static {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1514048
    :cond_0
    const v0, 0x7f0d050e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->y:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    .line 1514049
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->y:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v0, v4}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setVisibility(I)V

    .line 1514050
    :goto_0
    const v0, 0x7f0d0510

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    .line 1514051
    const v0, 0x7f0d0511

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->A:Lcom/facebook/resources/ui/FbEditText;

    .line 1514052
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->setSelected(Z)V

    .line 1514053
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    new-instance v3, LX/9ar;

    invoke-direct {v3, p0}, LX/9ar;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1514054
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->A:Lcom/facebook/resources/ui/FbEditText;

    new-instance v3, LX/9as;

    invoke-direct {v3, p0}, LX/9as;-><init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1514055
    const v0, -0x6d04f5ed

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 1514056
    :cond_1
    const v0, 0x7f0d050c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->x:Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1514035
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1514036
    const-string v0, "savedPrivacyKey"

    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1514037
    const-string v0, "savedAlbumTitle"

    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514038
    const-string v0, "savedAlbumDescription"

    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->A:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514039
    const-string v0, "savedLocation"

    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1514040
    const-string v0, "savedHasPrivacyChanged"

    iget-boolean v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1514041
    const-string v0, "savedHasUserInteracted"

    iget-boolean v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1514042
    const-string v0, "savedComposerTargetData"

    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1514043
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x25cb9393

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1514032
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1514033
    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->p:LX/93Q;

    invoke-virtual {v1}, LX/93Q;->a()V

    .line 1514034
    const/16 v1, 0x2b

    const v2, 0x4cb87e74    # 9.6727968E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x28ccfd2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1514029
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1514030
    iget-object v1, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->p:LX/93Q;

    invoke-virtual {v1}, LX/93Q;->e()V

    .line 1514031
    const/16 v1, 0x2b

    const v2, -0x316cb349

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
