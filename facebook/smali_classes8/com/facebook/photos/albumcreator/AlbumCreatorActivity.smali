.class public Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1513817
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;
    .locals 3

    .prologue
    .line 1513832
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    .line 1513833
    if-eqz v0, :cond_0

    .line 1513834
    :goto_0
    return-object v0

    .line 1513835
    :cond_0
    new-instance v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-direct {v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;-><init>()V

    .line 1513836
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1513827
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1513828
    const v0, 0x7f0300c8

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;->setContentView(I)V

    .line 1513829
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1513830
    invoke-direct {p0}, Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;->a()Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;->p:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    .line 1513831
    return-void
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1513822
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;->p:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    .line 1513823
    iget-object v1, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9ac;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->p(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/9ac;->a(Landroid/app/Activity;Z)V

    .line 1513824
    invoke-static {v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V

    .line 1513825
    goto :goto_0

    .line 1513826
    :goto_0
    return-void
.end method

.method public final onUserInteraction()V
    .locals 1

    .prologue
    .line 1513818
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onUserInteraction()V

    .line 1513819
    iget-object v0, p0, Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;->p:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    .line 1513820
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->v:Z

    .line 1513821
    return-void
.end method
