.class public Lcom/facebook/sounds/configurator/AudioConfigurator;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field public static final b:Ljava/text/DecimalFormat;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8iQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/8ib;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8iO;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/8YK;

.field public f:F

.field public g:F

.field public final h:I

.field private i:Landroid/widget/SeekBar;

.field private j:Landroid/widget/Spinner;

.field public k:LX/8iO;

.field public l:Landroid/widget/EditText;

.field private m:Landroid/widget/SeekBar;

.field private n:Landroid/widget/TextView;

.field public o:LX/8ih;

.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1391837
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->b:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1391612
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1391613
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1391835
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1391836
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v6, 0x2710

    const-wide/16 v2, 0x0

    const/16 v1, 0xe1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 1391806
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1391807
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    .line 1391808
    const/16 v0, 0xff

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iput v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->h:I

    .line 1391809
    const-class v0, Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-static {v0, p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1391810
    new-instance v0, LX/8ib;

    invoke-direct {v0, p0}, LX/8ib;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->c:LX/8ib;

    .line 1391811
    invoke-virtual {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1391812
    const/high16 v1, -0x3e100000    # -30.0f

    invoke-static {v1, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->g:F

    .line 1391813
    const/high16 v1, -0x3c290000    # -430.0f

    invoke-static {v1, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->f:F

    .line 1391814
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    .line 1391815
    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->e:LX/8YK;

    .line 1391816
    new-instance v0, LX/8ia;

    invoke-direct {v0, p0}, LX/8ia;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391817
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->e:LX/8YK;

    invoke-virtual {v1, v4, v5}, LX/8YK;->a(D)LX/8YK;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, LX/8YK;->b(D)LX/8YK;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    .line 1391818
    invoke-direct {p0, p1}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->addView(Landroid/view/View;)V

    .line 1391819
    new-instance v0, LX/8iX;

    invoke-direct {v0, p0}, LX/8iX;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391820
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->i:Landroid/widget/SeekBar;

    invoke-virtual {v1, v6}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1391821
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->i:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1391822
    new-instance v10, LX/8iY;

    invoke-direct {v10, p0}, LX/8iY;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391823
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1391824
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    .line 1391825
    iget v1, v0, LX/8iQ;->a:F

    move v0, v1

    .line 1391826
    float-to-double v0, v0

    const-wide v8, 0x40c3880000000000L    # 10000.0

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/8YN;->a(DDDDD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1391827
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1391828
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v0, v10}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1391829
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->c:LX/8ib;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1391830
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    new-instance v1, LX/8iW;

    invoke-direct {v1, p0}, LX/8iW;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1391831
    invoke-virtual {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a()V

    .line 1391832
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    new-instance v1, LX/8iR;

    invoke-direct {v1, p0}, LX/8iR;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1391833
    iget v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->f:F

    invoke-virtual {p0, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->setTranslationY(F)V

    .line 1391834
    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/view/View;
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, -0x2

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 1391685
    invoke-virtual {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1391686
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v1

    .line 1391687
    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v2, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v2

    .line 1391688
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-static {v3, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v3

    .line 1391689
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1391690
    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1391691
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v6, 0x43e60000    # 460.0f

    invoke-static {v6, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v6

    invoke-direct {v5, v9, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391692
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391693
    const/16 v5, 0x51

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1391694
    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1391695
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x43d70000    # 430.0f

    invoke-static {v7, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v7

    invoke-direct {v6, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391696
    const/16 v7, 0x30

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1391697
    invoke-virtual {v6, v2, v10, v2, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391698
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391699
    invoke-virtual {v5, v3, v10, v3, v10}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1391700
    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1391701
    const/16 v6, 0xc8

    invoke-static {v6, v10, v10, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 1391702
    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setClipToPadding(Z)V

    .line 1391703
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391704
    new-instance v6, Landroid/widget/Spinner;

    invoke-direct {v6, p1, v8}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    .line 1391705
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391706
    const/high16 v7, 0x42200000    # 40.0f

    invoke-static {v7, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v7

    invoke-virtual {v6, v10, v7, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391707
    iget-object v7, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    invoke-virtual {v7, v6}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391708
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    const v7, 0x7f02069b

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 1391709
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    const v7, 0x7f02069b

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setPopupBackgroundResource(I)V

    .line 1391710
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391711
    new-instance v6, Landroid/widget/TextView;

    invoke-direct {v6, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1391712
    const-string v7, "Current Sound Volume:"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391713
    const v7, -0x333334

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1391714
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391715
    invoke-virtual {v7, v10, v2, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391716
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391717
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391718
    new-instance v6, Landroid/widget/EditText;

    invoke-direct {v6, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    .line 1391719
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    const/16 v7, 0x2002

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 1391720
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setGravity(I)V

    .line 1391721
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391722
    invoke-virtual {v6, v10, v1, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391723
    iget-object v7, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v7, v6}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391724
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1391725
    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391726
    new-instance v6, Landroid/widget/SeekBar;

    invoke-direct {v6, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->i:Landroid/widget/SeekBar;

    .line 1391727
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391728
    neg-int v7, v2

    neg-int v8, v2

    invoke-virtual {v6, v7, v2, v8, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391729
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->i:Landroid/widget/SeekBar;

    invoke-virtual {v3, v6}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391730
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->i:Landroid/widget/SeekBar;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391731
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1391732
    const-string v6, "Master Volume:"

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391733
    const v6, -0x333334

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1391734
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391735
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391736
    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391737
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    .line 1391738
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    const/16 v6, 0x2002

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setInputType(I)V

    .line 1391739
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1391740
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1391741
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getTextSize()F

    move-result v6

    invoke-virtual {v3, v10, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1391742
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391743
    invoke-virtual {v3, v10, v1, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391744
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391745
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391746
    new-instance v1, Landroid/widget/SeekBar;

    invoke-direct {v1, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    .line 1391747
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391748
    neg-int v3, v2

    neg-int v6, v2

    invoke-virtual {v1, v3, v2, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391749
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391750
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391751
    neg-int v3, v2

    neg-int v6, v2

    invoke-virtual {v1, v3, v2, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1391752
    iget-object v3, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391753
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391754
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1391755
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391756
    invoke-virtual {v1, v2, v2, v2, v10}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1391757
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setClipToPadding(Z)V

    .line 1391758
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391759
    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391760
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1391761
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v9, v9, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391762
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391763
    new-instance v2, LX/8is;

    invoke-direct {v2, p1}, LX/8is;-><init>(Landroid/content/Context;)V

    .line 1391764
    const-string v3, "Save"

    invoke-virtual {v2, v3}, LX/8is;->setText(Ljava/lang/CharSequence;)V

    .line 1391765
    invoke-virtual {v2, v9}, LX/8is;->setTextColor(I)V

    .line 1391766
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v11, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391767
    invoke-virtual {v2, v3}, LX/8is;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391768
    new-instance v3, LX/8iT;

    invoke-direct {v3, p0}, LX/8iT;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391769
    iput-object v3, v2, LX/8is;->b:LX/8iS;

    .line 1391770
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391771
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1391772
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v9, v9, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391773
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391774
    new-instance v2, LX/8is;

    invoke-direct {v2, p1}, LX/8is;-><init>(Landroid/content/Context;)V

    .line 1391775
    const-string v3, "Clear"

    invoke-virtual {v2, v3}, LX/8is;->setText(Ljava/lang/CharSequence;)V

    .line 1391776
    invoke-virtual {v2, v9}, LX/8is;->setTextColor(I)V

    .line 1391777
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v11, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391778
    invoke-virtual {v2, v3}, LX/8is;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391779
    new-instance v3, LX/8iU;

    invoke-direct {v3, p0}, LX/8iU;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391780
    iput-object v3, v2, LX/8is;->b:LX/8iS;

    .line 1391781
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391782
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1391783
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v9, v9, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391784
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391785
    new-instance v2, LX/8is;

    invoke-direct {v2, p1}, LX/8is;-><init>(Landroid/content/Context;)V

    .line 1391786
    const-string v3, "Share"

    invoke-virtual {v2, v3}, LX/8is;->setText(Ljava/lang/CharSequence;)V

    .line 1391787
    invoke-virtual {v2, v9}, LX/8is;->setTextColor(I)V

    .line 1391788
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v11, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1391789
    const/4 v5, 0x5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1391790
    invoke-virtual {v2, v3}, LX/8is;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391791
    new-instance v3, LX/8iV;

    invoke-direct {v3, p0}, LX/8iV;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391792
    iput-object v3, v2, LX/8is;->b:LX/8iS;

    .line 1391793
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391794
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1391795
    const/high16 v2, 0x42700000    # 60.0f

    invoke-static {v2, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v2

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v3, v0}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v0

    .line 1391796
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v2, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move-object v0, v3

    .line 1391797
    const/16 v2, 0x51

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1391798
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391799
    new-instance v0, LX/8iZ;

    invoke-direct {v0, p0}, LX/8iZ;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1391800
    const/16 v0, 0xff

    const/16 v2, 0xa4

    const/16 v3, 0xd1

    invoke-static {v0, v10, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 1391801
    const-string v0, "audio"

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391802
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1391803
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1391804
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391805
    return-object v4
.end method

.method private a(LX/8iQ;)V
    .locals 2

    .prologue
    .line 1391681
    iget v0, p1, LX/8iQ;->a:F

    move v0, v0

    .line 1391682
    const/4 v1, 0x0

    sub-float/2addr v0, v1

    const v1, 0x461c4000    # 10000.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1391683
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->m:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1391684
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/sounds/configurator/AudioConfigurator;

    const/16 v1, 0x3566

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;LX/8iO;)V
    .locals 4

    .prologue
    .line 1391674
    iget v0, p1, LX/8iO;->c:F

    move v0, v0

    .line 1391675
    const/4 v1, 0x0

    sub-float/2addr v0, v1

    const v1, 0x461c4000    # 10000.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1391676
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->i:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1391677
    sget-object v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->b:Ljava/text/DecimalFormat;

    .line 1391678
    iget v1, p1, LX/8iO;->c:F

    move v1, v1

    .line 1391679
    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    .line 1391680
    return-void
.end method

.method public static c$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 11

    .prologue
    .line 1391648
    invoke-static {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->d(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391649
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    iget v0, v0, LX/8iO;->b:I

    if-nez v0, :cond_0

    .line 1391650
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const-string v1, "Sound resource not found."

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const-string v1, "Error"

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1391651
    :goto_0
    return-void

    .line 1391652
    :cond_0
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    iget v1, v0, LX/8iO;->b:I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    .line 1391653
    const/4 v4, 0x0

    .line 1391654
    new-instance v6, Landroid/os/HandlerThread;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1391655
    invoke-virtual {v6}, Landroid/os/HandlerThread;->start()V

    .line 1391656
    new-instance v5, LX/8if;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    move v7, v1

    move v8, v2

    move-object v9, v3

    move-object v10, v0

    invoke-direct/range {v5 .. v10}, LX/8if;-><init>(Landroid/os/Looper;IZLandroid/content/Context;LX/8iQ;)V

    .line 1391657
    new-instance v6, LX/8ih;

    invoke-direct {v6, v5, v4, v3}, LX/8ih;-><init>(Landroid/os/Handler;ZLandroid/content/Context;)V

    move-object v4, v6

    .line 1391658
    move-object v0, v4

    .line 1391659
    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    .line 1391660
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    const/4 v1, 0x1

    const/4 v3, 0x1

    .line 1391661
    iget-boolean v2, v0, LX/8ih;->a:Z

    if-eqz v2, :cond_1

    .line 1391662
    :goto_1
    goto :goto_0

    .line 1391663
    :cond_1
    if-eqz v1, :cond_4

    .line 1391664
    iget-object v2, v0, LX/8ih;->b:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1391665
    :goto_2
    iget-boolean v2, v0, LX/8ih;->c:Z

    if-eqz v2, :cond_3

    .line 1391666
    iget-object v2, v0, LX/8ih;->d:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    .line 1391667
    iget-object v4, v0, LX/8ih;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-nez v4, :cond_2

    .line 1391668
    new-instance v4, LX/8ig;

    invoke-direct {v4, v0}, LX/8ig;-><init>(LX/8ih;)V

    iput-object v4, v0, LX/8ih;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1391669
    :cond_2
    iget-object v4, v0, LX/8ih;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    move-object v4, v4

    .line 1391670
    const/4 v5, 0x3

    const/4 v6, 0x4

    invoke-virtual {v2, v4, v5, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 1391671
    :cond_3
    iput-boolean v3, v0, LX/8ih;->a:Z

    .line 1391672
    goto :goto_1

    .line 1391673
    :cond_4
    iget-object v2, v0, LX/8ih;->b:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2
.end method

.method public static d(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 1

    .prologue
    .line 1391644
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    if-eqz v0, :cond_0

    .line 1391645
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    invoke-virtual {v0}, LX/8ih;->a()V

    .line 1391646
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    .line 1391647
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 4

    .prologue
    .line 1391639
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    .line 1391640
    iget v1, v0, LX/8iQ;->a:F

    move v0, v1

    .line 1391641
    sget-object v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->b:Ljava/text/DecimalFormat;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 1391642
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391643
    return-void
.end method

.method public static f$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 7

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 1391635
    iget-object v2, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->e:LX/8YK;

    .line 1391636
    iget-wide v5, v2, LX/8YK;->i:D

    move-wide v2, v5

    .line 1391637
    iget-object v4, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->e:LX/8YK;

    cmpl-double v2, v2, v0

    if-nez v2, :cond_0

    const-wide/16 v0, 0x0

    :cond_0
    invoke-virtual {v4, v0, v1}, LX/8YK;->b(D)LX/8YK;

    .line 1391638
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1391616
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->c:LX/8ib;

    invoke-virtual {v0}, LX/8ib;->a()V

    .line 1391617
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1391618
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    invoke-virtual {v0}, LX/8iQ;->b()Ljava/util/List;

    move-result-object v0

    .line 1391619
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iO;

    .line 1391620
    iget-object v2, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1391621
    iget-object v2, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->c:LX/8ib;

    iget-object v0, v0, LX/8iO;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/8ib;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1391622
    :cond_0
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    new-instance v1, LX/8iP;

    invoke-direct {v1}, LX/8iP;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1391623
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->c:LX/8ib;

    const v1, 0x7a680578

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1391624
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1391625
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1391626
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iO;

    iput-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391627
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    invoke-static {p0, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;LX/8iO;)V

    .line 1391628
    sget-object v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->b:Ljava/text/DecimalFormat;

    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391629
    iget v2, v1, LX/8iO;->c:F

    move v1, v2

    .line 1391630
    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 1391631
    iget-object v1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1391632
    invoke-static {p0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->e(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391633
    iget-object v0, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    invoke-direct {p0, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a(LX/8iQ;)V

    .line 1391634
    :cond_1
    return-void
.end method

.method public setFbSharedPreferences(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1391614
    iput-object p1, p0, Lcom/facebook/sounds/configurator/AudioConfigurator;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1391615
    return-void
.end method
