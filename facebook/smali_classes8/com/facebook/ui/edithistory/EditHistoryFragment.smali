.class public abstract Lcom/facebook/ui/edithistory/EditHistoryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field public d:Z

.field public e:Lcom/facebook/widget/listview/BetterListView;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;

.field public h:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/A7F;

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1625829
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1625830
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->d:Z

    .line 1625831
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1625832
    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->k:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/ui/edithistory/EditHistoryFragment;

    const/16 v1, 0xbc

    invoke-static {v2, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v1, 0x12b1

    invoke-static {v2, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    const/16 p0, 0x259

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v3, p1, Lcom/facebook/ui/edithistory/EditHistoryFragment;->a:LX/0Or;

    iput-object v4, p1, Lcom/facebook/ui/edithistory/EditHistoryFragment;->b:LX/0Or;

    iput-object v1, p1, Lcom/facebook/ui/edithistory/EditHistoryFragment;->h:LX/0tX;

    iput-object v2, p1, Lcom/facebook/ui/edithistory/EditHistoryFragment;->k:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/ui/edithistory/EditHistoryFragment;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    .line 1625821
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->i:LX/1Ck;

    const-string v1, "fetchEditHistory"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625822
    :goto_0
    return-void

    .line 1625823
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1625824
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1625825
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1625826
    new-instance v0, LX/A7b;

    invoke-direct {v0}, LX/A7b;-><init>()V

    const-string v1, "node_id"

    iget-object v2, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 1625827
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 1625828
    iget-object v1, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->i:LX/1Ck;

    const-string v2, "fetchEditHistory"

    new-instance v3, LX/A7Z;

    invoke-direct {v3, p0, v0}, LX/A7Z;-><init>(Lcom/facebook/ui/edithistory/EditHistoryFragment;LX/0w7;)V

    new-instance v0, LX/A7a;

    invoke-direct {v0, p0}, LX/A7a;-><init>(Lcom/facebook/ui/edithistory/EditHistoryFragment;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1625791
    const-class v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;

    invoke-static {v0, p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1625792
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1625793
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    .line 1625794
    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->i:LX/1Ck;

    .line 1625795
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 1625796
    const-string v0, "node_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Node ID cannot be null"

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->c:Ljava/lang/String;

    .line 1625797
    const-string v0, "module"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Module name cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1625798
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "open_edit_history"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1625799
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1625800
    move-object v1, v1

    .line 1625801
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 1625802
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1625803
    return-void
.end method

.method public abstract m()I
.end method

.method public abstract n()LX/A7F;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2ec78514

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1625812
    invoke-virtual {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->m()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1625813
    const v0, 0x7f0d1160

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 1625814
    const v0, 0x7f0d115f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->f:Landroid/view/View;

    .line 1625815
    const v0, 0x7f0d0ab3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->g:Landroid/view/View;

    .line 1625816
    invoke-virtual {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->n()LX/A7F;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->j:LX/A7F;

    .line 1625817
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->j:LX/A7F;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1625818
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->g:Landroid/view/View;

    new-instance v3, LX/A7Y;

    invoke-direct {v3, p0}, LX/A7Y;-><init>(Lcom/facebook/ui/edithistory/EditHistoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1625819
    invoke-static {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->b(Lcom/facebook/ui/edithistory/EditHistoryFragment;)V

    .line 1625820
    const/16 v0, 0x2b

    const v3, 0x67f27efb

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1238b71a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1625808
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1625809
    iget-boolean v1, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->d:Z

    if-nez v1, :cond_0

    .line 1625810
    invoke-static {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->b(Lcom/facebook/ui/edithistory/EditHistoryFragment;)V

    .line 1625811
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x68a9d620

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x61365e36

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1625804
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1625805
    iget-object v1, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->i:LX/1Ck;

    if-eqz v1, :cond_0

    .line 1625806
    iget-object v1, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->i:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1625807
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2c0f1954

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
