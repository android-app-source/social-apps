.class public final Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2aaef1dc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1626482
    const-class v0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1626481
    const-class v0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1626479
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1626480
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1626477
    iget-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->e:Ljava/lang/String;

    .line 1626478
    iget-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1626475
    iget-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->h:Ljava/lang/String;

    .line 1626476
    iget-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1626464
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1626465
    invoke-direct {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1626466
    invoke-virtual {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->j()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1626467
    invoke-direct {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1626468
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1626469
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1626470
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1626471
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1626472
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1626473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1626474
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1626443
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1626444
    invoke-virtual {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->j()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1626445
    invoke-virtual {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->j()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    .line 1626446
    invoke-virtual {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->j()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1626447
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;

    .line 1626448
    iput-object v0, v1, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    .line 1626449
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1626450
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1626463
    invoke-direct {p0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1626460
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1626461
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->g:J

    .line 1626462
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1626457
    new-instance v0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;-><init>()V

    .line 1626458
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1626459
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1626456
    const v0, 0x65c8c67b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1626455
    const v0, -0x35367b00    # -6603392.0f

    return v0
.end method

.method public final j()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1626453
    iget-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    iput-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    .line 1626454
    iget-object v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 1626451
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1626452
    iget-wide v0, p0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->g:J

    return-wide v0
.end method
