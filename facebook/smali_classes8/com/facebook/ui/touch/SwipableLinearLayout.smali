.class public Lcom/facebook/ui/touch/SwipableLinearLayout;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:LX/8sw;

.field private b:LX/8sy;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1411988
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1411989
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1411990
    iget-object v0, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->a:LX/8sw;

    if-eqz v0, :cond_0

    .line 1411991
    iget-object v0, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->a:LX/8sw;

    invoke-virtual {v0, p1}, LX/8sw;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->c:Z

    .line 1411992
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->c:Z

    return v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1ea29486

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1411993
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 1411994
    const/16 v1, 0x2d

    const v2, -0x3d4bc706

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x260a10aa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1411995
    iget-boolean v1, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->c:Z

    if-eqz v1, :cond_0

    .line 1411996
    iget-object v1, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->a:LX/8sw;

    invoke-virtual {v1, p1}, LX/8sw;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->c:Z

    .line 1411997
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->c:Z

    const v2, 0x500a6769

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setOnSizeChangedListener(LX/8sy;)V
    .locals 0

    .prologue
    .line 1411998
    iput-object p1, p0, Lcom/facebook/ui/touch/SwipableLinearLayout;->b:LX/8sy;

    .line 1411999
    return-void
.end method
