.class public Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0qd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0qb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1669861
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1669910
    invoke-virtual {p0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1669911
    invoke-direct {p0, v0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 1669912
    invoke-direct {p0, v0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->b(Landroid/preference/PreferenceScreen;)V

    .line 1669913
    invoke-direct {p0, v0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->c(Landroid/preference/PreferenceScreen;)V

    .line 1669914
    invoke-direct {p0, v0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->d(Landroid/preference/PreferenceScreen;)V

    .line 1669915
    invoke-virtual {p0, v0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1669916
    return-void
.end method

.method private final a(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1669903
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1669904
    sget-object v1, LX/0qh;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1669905
    const-string v1, "Turn off clash manager"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669906
    const-string v1, "Turn off all clash management and revert to unmanaged behavior"

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669907
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1669908
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669909
    return-void
.end method

.method private static a(Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;LX/0qd;Ljava/util/concurrent/Executor;LX/0qb;)V
    .locals 0

    .prologue
    .line 1669902
    iput-object p1, p0, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->a:LX/0qd;

    iput-object p2, p0, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->b:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->c:LX/0qb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;

    invoke-static {v2}, LX/0qd;->a(LX/0QB;)LX/0qd;

    move-result-object v0

    check-cast v0, LX/0qd;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2}, LX/0qb;->a(LX/0QB;)LX/0qb;

    move-result-object v2

    check-cast v2, LX/0qb;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->a(Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;LX/0qd;Ljava/util/concurrent/Executor;LX/0qb;)V

    return-void
.end method

.method private final b(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1669896
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1669897
    const-string v1, "Force GraphQL Clash Unit fetch"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669898
    const-string v1, "Force fetch all Clash Unit from server"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669899
    new-instance v1, LX/AOy;

    invoke-direct {v1, p0}, LX/AOy;-><init>(Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1669900
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669901
    return-void
.end method

.method private final c(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1669889
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1669890
    sget-object v1, LX/0qh;->c:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1669891
    const-string v1, "Enable Debug Log"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669892
    const-string v1, "Enable Clash Manager debug log on Android logcat"

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669893
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1669894
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669895
    return-void
.end method

.method private final d(Landroid/preference/PreferenceScreen;)V
    .locals 5

    .prologue
    .line 1669867
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1669868
    const-string v1, "Clash Unit catalog"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669869
    const-string v1, "Catalog of eligible Clash Unit returned from the server"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669870
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669871
    iget-object v0, p0, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->c:LX/0qb;

    invoke-virtual {v0}, LX/0qb;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1669872
    new-instance v3, Landroid/preference/PreferenceCategory;

    invoke-direct {v3, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1669873
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rH;

    invoke-virtual {v1}, LX/0rH;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669874
    invoke-virtual {p1, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669875
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1669876
    const-string v1, "Slot Status"

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669877
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/44w;

    iget-object v1, v1, LX/44w;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669878
    invoke-virtual {p1, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669879
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1669880
    const-string v3, "Force Release Slot"

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669881
    new-instance v3, LX/AOz;

    invoke-direct {v3, p0, v0}, LX/AOz;-><init>(Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;Ljava/util/Map$Entry;)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1669882
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1669883
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1669884
    new-instance v4, Landroid/preference/Preference;

    invoke-direct {v4, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1669885
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qg;

    invoke-virtual {v1}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669886
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669887
    invoke-virtual {p1, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 1669888
    :cond_1
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1669862
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1669863
    invoke-static {p0, p0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1669864
    iget-object v0, p0, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->a:LX/0qd;

    invoke-virtual {v0}, LX/0qd;->a()V

    .line 1669865
    invoke-direct {p0}, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;->a()V

    .line 1669866
    return-void
.end method
