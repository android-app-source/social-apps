.class public Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1643317
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1643318
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a()V

    .line 1643319
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1643314
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643315
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a()V

    .line 1643316
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1643311
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1643312
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a()V

    .line 1643313
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1643308
    const v0, 0x7f03005d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1643309
    const v0, 0x7f0d0419

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a:Landroid/widget/RadioGroup;

    .line 1643310
    return-void
.end method


# virtual methods
.method public getSelectedGender()LX/ACy;
    .locals 2

    .prologue
    .line 1643302
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f0d041b

    if-ne v0, v1, :cond_0

    .line 1643303
    sget-object v0, LX/ACy;->MALE:LX/ACy;

    .line 1643304
    :goto_0
    return-object v0

    .line 1643305
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f0d041c

    if-ne v0, v1, :cond_1

    .line 1643306
    sget-object v0, LX/ACy;->FEMALE:LX/ACy;

    goto :goto_0

    .line 1643307
    :cond_1
    sget-object v0, LX/ACy;->ALL:LX/ACy;

    goto :goto_0
.end method

.method public setGender(LX/ACy;)V
    .locals 3

    .prologue
    .line 1643294
    const v0, 0x7f0d041a

    .line 1643295
    sget-object v1, LX/ACx;->a:[I

    invoke-virtual {p1}, LX/ACy;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1643296
    :goto_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 1643297
    return-void

    .line 1643298
    :pswitch_0
    const v0, 0x7f0d041b

    goto :goto_0

    .line 1643299
    :pswitch_1
    const v0, 0x7f0d041c

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1643300
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1643301
    return-void
.end method
