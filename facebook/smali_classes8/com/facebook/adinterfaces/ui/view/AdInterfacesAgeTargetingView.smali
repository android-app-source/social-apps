.class public Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/BXo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:I

.field public c:I

.field public d:I

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

.field public h:LX/ACw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1643268
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1643269
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a()V

    .line 1643270
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1643271
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643272
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a()V

    .line 1643273
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1643283
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1643284
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a()V

    .line 1643285
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1643274
    const-class v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    invoke-static {v0, p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1643275
    const v0, 0x7f030045

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1643276
    const v0, 0x7f0d03d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1643277
    const v0, 0x7f0d03d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1643278
    const v0, 0x7f0d03d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    .line 1643279
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    new-instance v1, LX/ACv;

    invoke-direct {v1, p0}, LX/ACv;-><init>(Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setRangeSeekBarChangeListener(LX/ACu;)V

    .line 1643280
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a:LX/BXo;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    .line 1643281
    iget-object p0, v0, LX/BXo;->a:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1643282
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    invoke-static {v0}, LX/BXo;->a(LX/0QB;)LX/BXo;

    move-result-object v0

    check-cast v0, LX/BXo;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a:LX/BXo;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;)V
    .locals 4

    .prologue
    .line 1643259
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    .line 1643260
    iget v1, v0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    move v0, v1

    .line 1643261
    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1643262
    iget v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b:I

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    .line 1643263
    iget v3, v2, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    move v2, v3

    .line 1643264
    float-to-int v2, v2

    if-ne v1, v2, :cond_0

    .line 1643265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1643266
    :cond_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1643267
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 1643254
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1643255
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1643256
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getTextScaleX()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextScaleX(F)V

    .line 1643257
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->f:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setMinimumWidth(I)V

    .line 1643258
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 1643249
    iput p2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b:I

    .line 1643250
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c()V

    .line 1643251
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    int-to-float v1, p1

    iget v2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e(FF)V

    .line 1643252
    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b$redex0(Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;)V

    .line 1643253
    return-void
.end method

.method public final b(II)V
    .locals 3

    .prologue
    .line 1643245
    iput p1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    .line 1643246
    iput p2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    .line 1643247
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f(FF)V

    .line 1643248
    return-void
.end method

.method public getAgeEnd()I
    .locals 1

    .prologue
    .line 1643244
    iget v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    return v0
.end method

.method public getAgeStart()I
    .locals 1

    .prologue
    .line 1643243
    iget v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    return v0
.end method

.method public setOnAgeRangeChangeListener(LX/ACw;)V
    .locals 0

    .prologue
    .line 1643241
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->h:LX/ACw;

    .line 1643242
    return-void
.end method
