.class public Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1643320
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1643321
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643322
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1643323
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643324
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643325
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1643326
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1643327
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643328
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1643329
    if-nez p2, :cond_0

    .line 1643330
    :goto_0
    return-void

    .line 1643331
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->AdInterfacesSelector:[I

    invoke-virtual {v0, p2, v1, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1643332
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1643333
    if-eqz v1, :cond_1

    .line 1643334
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1643335
    :cond_1
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1643336
    if-eqz v1, :cond_2

    .line 1643337
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 1643338
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1643339
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1643340
    const v0, 0x7f030086

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1643341
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setOrientation(I)V

    .line 1643342
    const v0, 0x7f0d0470

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1643343
    const v0, 0x7f0d046f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1643344
    const v0, 0x7f0d0476

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1643345
    invoke-direct {p0, p1, p2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1643346
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;LX/1jt;)V
    .locals 4

    .prologue
    .line 1643347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1643348
    const-string v1, ", "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, p2, v2}, LX/0YN;->a(Ljava/lang/StringBuilder;Ljava/lang/String;LX/1jt;[Ljava/lang/Object;)V

    .line 1643349
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setSelectedValues(Ljava/lang/CharSequence;)V

    .line 1643350
    return-void
.end method

.method public setEditButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1643351
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1643352
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1643353
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1643354
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1643355
    return-void
.end method

.method public setSelectedValues(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1643356
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1643357
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1643358
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1643359
    return-void
.end method
