.class public Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/ACs;

.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1643199
    new-instance v0, LX/ACq;

    invoke-direct {v0}, LX/ACq;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/ACs;I)V
    .locals 0

    .prologue
    .line 1643200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1643201
    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->a:LX/ACs;

    .line 1643202
    iput p2, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->b:I

    .line 1643203
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1643204
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/ACs;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;-><init>(LX/ACs;I)V

    .line 1643205
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1643206
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1643207
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->a:LX/ACs;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1643208
    iget v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1643209
    return-void
.end method
