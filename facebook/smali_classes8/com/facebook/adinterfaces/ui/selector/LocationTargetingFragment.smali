.class public Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;
.super Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/ABi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1643108
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;-><init>()V

    .line 1643109
    return-void
.end method

.method public static synthetic a(Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;LX/8QL;)V
    .locals 0

    .prologue
    .line 1643107
    invoke-super {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(LX/8QL;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/8QL;
    .locals 1

    .prologue
    .line 1643105
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 1643106
    new-instance v0, LX/ACl;

    invoke-direct {v0, p1}, LX/ACl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1643098
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->a:LX/ABi;

    .line 1643099
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643100
    new-instance v1, LX/ABb;

    invoke-direct {v1}, LX/ABb;-><init>()V

    move-object v1, v1

    .line 1643101
    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "types"

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->REGION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CITY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, v5, p0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1643102
    iget-object v2, v0, LX/ABi;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1643103
    new-instance v2, LX/ABg;

    invoke-direct {v2, v0}, LX/ABg;-><init>(LX/ABi;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 1643104
    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "LX/621",
            "<+",
            "LX/8QL",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1643088
    sget-object v0, LX/ACl;->e:Ljava/util/Comparator;

    invoke-static {v0}, LX/0dW;->b(Ljava/util/Comparator;)LX/4yi;

    move-result-object v1

    .line 1643089
    sget-object v0, LX/ACl;->e:Ljava/util/Comparator;

    invoke-static {v0}, LX/0dW;->b(Ljava/util/Comparator;)LX/4yi;

    move-result-object v2

    .line 1643090
    sget-object v0, LX/ACl;->e:Ljava/util/Comparator;

    invoke-static {v0}, LX/0dW;->b(Ljava/util/Comparator;)LX/4yi;

    move-result-object v3

    .line 1643091
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 1643092
    new-instance v5, LX/ACl;

    invoke-direct {v5, v0}, LX/ACl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V

    .line 1643093
    sget-object v6, LX/ACj;->a:[I

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1643094
    :pswitch_0
    invoke-virtual {v1, v5}, LX/4yi;->d(Ljava/lang/Object;)LX/4yi;

    goto :goto_0

    .line 1643095
    :pswitch_1
    invoke-virtual {v2, v5}, LX/4yi;->d(Ljava/lang/Object;)LX/4yi;

    goto :goto_0

    .line 1643096
    :pswitch_2
    invoke-virtual {v3, v5}, LX/4yi;->d(Ljava/lang/Object;)LX/4yi;

    goto :goto_0

    .line 1643097
    :cond_0
    new-instance v0, LX/623;

    const v4, 0x7f080b25

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, LX/4yi;->c()LX/0dW;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v4, v1}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    new-instance v1, LX/623;

    const v4, 0x7f080b26

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/4yi;->c()LX/0dW;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v4, v2}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    new-instance v2, LX/623;

    const v4, 0x7f080b27

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LX/4yi;->c()LX/0dW;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v4, v3}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/8QL;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8QL",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1643110
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->l()Ljava/util/ArrayList;

    move-result-object v1

    .line 1643111
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x19

    if-ge v0, v2, :cond_0

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1643112
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 1643113
    check-cast v0, LX/ACl;

    .line 1643114
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1643115
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QL;

    .line 1643116
    invoke-virtual {v1}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 1643117
    iget-object v5, v0, LX/ACl;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v5, v5

    .line 1643118
    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-eq v6, v7, :cond_3

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-eq v6, v7, :cond_3

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_3
    const/4 v6, 0x1

    :goto_2
    move v2, v6

    .line 1643119
    if-eqz v2, :cond_2

    .line 1643120
    check-cast v1, LX/ACl;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1643121
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1643122
    invoke-super {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(LX/8QL;)V

    goto :goto_0

    .line 1643123
    :cond_5
    const-string v0, "; "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    new-instance v1, LX/ACg;

    invoke-direct {v1, p0}, LX/ACg;-><init>(Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;)V

    invoke-static {v3, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 1643124
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f080b24

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/ACi;

    invoke-direct {v2, p0, p1, v3}, LX/ACi;-><init>(Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;LX/8QL;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/ACh;

    invoke-direct {v2, p0}, LX/ACh;-><init>(Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_0

    :cond_6
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1643085
    invoke-super {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1643086
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;

    invoke-static {v0}, LX/ABi;->b(LX/0QB;)LX/ABi;

    move-result-object v0

    check-cast v0, LX/ABi;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->a:LX/ABi;

    .line 1643087
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1643084
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f080b21

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1643083
    const v0, 0x7f080b20

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1643080
    invoke-super {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->k()Ljava/util/ArrayList;

    move-result-object v0

    .line 1643081
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1643082
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x2e931d2f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1643067
    invoke-super {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1643068
    if-nez p1, :cond_0

    .line 1643069
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 1643070
    :cond_0
    if-eqz p1, :cond_1

    .line 1643071
    const-string v0, "defaultLocations"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    .line 1643072
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1643073
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    new-instance v3, LX/ACf;

    invoke-direct {v3, p0}, LX/ACf;-><init>(Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;)V

    invoke-static {v2, v3}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->c:Ljava/lang/String;

    .line 1643074
    :cond_1
    const v0, -0x7faef401

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1643075
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1643076
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1643077
    const-string v1, "defaultLocations"

    invoke-static {p1, v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1643078
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1643079
    return-void
.end method
