.class public abstract Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/base/fragment/FbFragment;"
    }
.end annotation


# instance fields
.field public a:LX/8tB;

.field public b:Landroid/view/inputmethod/InputMethodManager;

.field public c:LX/1Ck;

.field public d:LX/0Sy;

.field public e:Lcom/facebook/widget/listview/BetterListView;

.field public f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/view/View;

.field public final i:LX/3NY;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642953
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1642954
    new-instance v0, LX/ACV;

    invoke-direct {v0, p0}, LX/ACV;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->i:LX/3NY;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Z)V
    .locals 4

    .prologue
    .line 1642955
    const-string v0, ""

    .line 1642956
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->a()Z

    move-result v1

    move v1, v1

    .line 1642957
    if-eqz v1, :cond_1

    .line 1642958
    const v0, 0x7f080b2e

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1642959
    :cond_0
    :goto_0
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1642960
    if-eqz p1, :cond_3

    .line 1642961
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1642962
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1642963
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1642964
    :goto_1
    return-void

    .line 1642965
    :cond_1
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1642966
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1642967
    :cond_2
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1642968
    const v0, 0x7f080b22

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1642969
    :cond_3
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1642970
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    goto :goto_1
.end method

.method public static c(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1642971
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1642972
    return-void
.end method

.method public static n(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V
    .locals 3

    .prologue
    .line 1642973
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->l()Ljava/util/ArrayList;

    move-result-object v0

    .line 1642974
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    .line 1642975
    iput-object v0, v1, LX/8tB;->i:Ljava/util/List;

    .line 1642976
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    const v2, 0x366029f2

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1642977
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1642978
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a$redex0(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Z)V

    .line 1642979
    :cond_0
    return-void
.end method

.method public static p(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V
    .locals 3

    .prologue
    .line 1642980
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1642981
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)LX/8QL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "LX/8QL",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<+",
            "LX/621",
            "<+",
            "LX/8QL",
            "<TT;>;>;>;"
        }
    .end annotation
.end method

.method public a(LX/8QL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8QL",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1642982
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1642983
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1642984
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1642985
    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->n(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1642986
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1642950
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1642951
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-static {v0}, LX/8tB;->b(LX/0QB;)LX/8tB;

    move-result-object v2

    check-cast v2, LX/8tB;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v0

    check-cast v0, LX/0Sy;

    iput-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    iput-object v3, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    iput-object p1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c:LX/1Ck;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->d:LX/0Sy;

    .line 1642952
    return-void
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final b(LX/8QL;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8QL",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1642881
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1642882
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1642883
    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->n(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1642884
    return-void
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public k()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1642885
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->l()Ljava/util/ArrayList;

    move-result-object v0

    .line 1642886
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1642887
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1642888
    invoke-virtual {v0}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1642889
    :cond_0
    return-object v1
.end method

.method public final l()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/8QL",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1642943
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    check-cast v0, [LX/8ul;

    .line 1642944
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1642945
    array-length v4, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v0, v2

    .line 1642946
    iget-object p0, v1, LX/8uk;->f:LX/8QK;

    move-object v1, p0

    .line 1642947
    check-cast v1, LX/8QL;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1642948
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1642949
    :cond_0
    return-object v3
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4dfa6d09

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1642890
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1642891
    const v0, 0x7f0d2e8b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1642892
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1642893
    const v0, 0x7f0d2e8d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 1642894
    const v0, 0x7f0d2e8c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->h:Landroid/view/View;

    .line 1642895
    const v0, 0x7f0d2e8e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->g:Landroid/widget/TextView;

    .line 1642896
    const/4 v0, 0x0

    .line 1642897
    if-nez p1, :cond_1

    .line 1642898
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1642899
    if-eqz v2, :cond_1

    .line 1642900
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1642901
    const-string v2, "initialData"

    invoke-static {v0, v2}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1642902
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1642903
    :cond_0
    const/4 v2, 0x0

    .line 1642904
    :goto_0
    move-object v0, v2

    .line 1642905
    :cond_1
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    new-instance v3, LX/ACW;

    invoke-direct {v3, p0}, LX/ACW;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    invoke-virtual {v2, v3}, LX/8tB;->a(LX/8RK;)V

    .line 1642906
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1642907
    invoke-static {p0, v2}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Ljava/util/List;)V

    .line 1642908
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1642909
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/ACX;

    invoke-direct {v3, p0}, LX/ACX;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1642910
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/ACY;

    invoke-direct {v3, p0}, LX/ACY;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1642911
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1642912
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8QL;

    .line 1642913
    iget-object p1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1

    .line 1642914
    :cond_2
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1642915
    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->n(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1642916
    :goto_2
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/ACZ;

    invoke-direct {v3, p0}, LX/ACZ;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1642917
    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/ACa;

    invoke-direct {v3, p0}, LX/ACa;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1642918
    const/16 v0, 0x2b

    const v2, 0x5f6e7ed4

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1642919
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1642920
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 1642921
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(Ljava/lang/Object;)LX/8QL;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1642922
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_0

    .line 1642923
    :cond_5
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a$redex0(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Z)V

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xa6218c0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1642924
    const v1, 0x7f031474

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5884639d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x439ed72f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1642925
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1642926
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1642927
    const/16 v1, 0x2b

    const v2, 0x792a9fb3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x39ab892c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1642928
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 1642929
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1642930
    const/16 v1, 0x2b

    const v2, 0x6272cd10

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x36242381

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1642931
    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->p(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1642932
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1642933
    const/16 v1, 0x2b

    const v2, -0x18a7ce55

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1642934
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->l()Ljava/util/ArrayList;

    move-result-object v2

    .line 1642935
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1642936
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1642937
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1642938
    invoke-virtual {v0}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1642939
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1642940
    :cond_0
    const-string v0, "initialData"

    invoke-static {p1, v0, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1642941
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1642942
    return-void
.end method
