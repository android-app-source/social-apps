.class public Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;
.super Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/ABi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1643009
    invoke-direct {p0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;-><init>()V

    .line 1643010
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/8QL;
    .locals 1

    .prologue
    .line 1643011
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 1643012
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643013
    new-instance v0, LX/ACe;

    invoke-direct {v0, p1}, LX/ACe;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1643014
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;->a:LX/ABi;

    .line 1643015
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643016
    new-instance v1, LX/ABa;

    invoke-direct {v1}, LX/ABa;-><init>()V

    move-object v1, v1

    .line 1643017
    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1643018
    iget-object v2, v0, LX/ABi;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1643019
    new-instance v2, LX/ABh;

    invoke-direct {v2, v0}, LX/ABh;-><init>(LX/ABi;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 1643020
    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "LX/621",
            "<+",
            "LX/8QL",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1643021
    sget-object v0, LX/ACe;->e:Ljava/util/Comparator;

    invoke-static {v0}, LX/0dW;->b(Ljava/util/Comparator;)LX/4yi;

    move-result-object v1

    .line 1643022
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 1643023
    new-instance v3, LX/ACe;

    invoke-direct {v3, v0}, LX/ACe;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;)V

    invoke-virtual {v1, v3}, LX/4yi;->d(Ljava/lang/Object;)LX/4yi;

    goto :goto_0

    .line 1643024
    :cond_0
    new-instance v0, LX/623;

    const v2, 0x7f080b28

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, LX/4yi;->c()LX/0dW;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1643025
    invoke-super {p0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1643026
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;

    invoke-static {v0}, LX/ABi;->b(LX/0QB;)LX/ABi;

    move-result-object v0

    check-cast v0, LX/ABi;

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;->a:LX/ABi;

    .line 1643027
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1643028
    const v0, 0x7f080b2d

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1643029
    const v0, 0x7f080b2c

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
