.class public Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1643133
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1643134
    return-void
.end method

.method public static a$redex0(Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;)V
    .locals 4

    .prologue
    .line 1643135
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    if-eqz v0, :cond_0

    .line 1643136
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    .line 1643137
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1643138
    const-string v2, "selectedTokens"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 1643139
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1643140
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 1643141
    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->p(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1643142
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1643143
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1643144
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1643145
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selectorArgument"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;

    .line 1643146
    const v1, 0x7f030a4e

    invoke-virtual {p0, v1}, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->setContentView(I)V

    .line 1643147
    const v1, 0x7f0d0a7f

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/94V;

    .line 1643148
    new-instance v2, LX/ACm;

    invoke-direct {v2, p0}, LX/ACm;-><init>(Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;)V

    invoke-virtual {v1, v2}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 1643149
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f080ad5

    invoke-virtual {p0, v3}, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1643150
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 1643151
    move-object v2, v2

    .line 1643152
    const/4 v3, -0x2

    .line 1643153
    iput v3, v2, LX/108;->h:I

    .line 1643154
    move-object v2, v2

    .line 1643155
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1643156
    new-instance v3, LX/94Z;

    new-instance v4, LX/94Y;

    invoke-direct {v4}, LX/94Y;-><init>()V

    .line 1643157
    iput-object v2, v4, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1643158
    move-object v2, v4

    .line 1643159
    iget v4, v0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->b:I

    invoke-virtual {p0, v4}, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1643160
    iput-object v4, v2, LX/94Y;->a:Ljava/lang/String;

    .line 1643161
    move-object v2, v2

    .line 1643162
    new-instance v4, LX/ACn;

    invoke-direct {v4, p0}, LX/ACn;-><init>(Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;)V

    .line 1643163
    iput-object v4, v2, LX/94Y;->c:LX/63W;

    .line 1643164
    move-object v2, v2

    .line 1643165
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v4

    .line 1643166
    iput-object v4, v2, LX/94Y;->d:LX/94c;

    .line 1643167
    move-object v2, v2

    .line 1643168
    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-direct {v3, v1, v2}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    .line 1643169
    sget-object v1, LX/ACo;->a:[I

    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->a:LX/ACs;

    invoke-virtual {v2}, LX/ACs;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1643170
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d1a0f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;

    iput-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    .line 1643171
    :goto_0
    iget-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    if-nez v1, :cond_0

    .line 1643172
    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorArgument;->a:LX/ACs;

    invoke-static {v0}, LX/ACt;->a(LX/ACs;)Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    .line 1643173
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1643174
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1a0f

    iget-object v2, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 1643175
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1643176
    :cond_0
    return-void

    .line 1643177
    :pswitch_0
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d1a0f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;

    iput-object v1, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1643178
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    if-eqz v0, :cond_0

    .line 1643179
    iget-object v0, p0, Lcom/facebook/adinterfaces/ui/selector/TargetingSelectorActivity;->p:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    .line 1643180
    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->p(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1643181
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1643182
    return-void
.end method
