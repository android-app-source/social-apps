.class public final Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5547417d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1641789
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1641800
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1641798
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1641799
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1641790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1641791
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1641792
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1641793
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1641794
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1641795
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1641796
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1641797
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1641769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1641770
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1641771
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    .line 1641772
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1641773
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;

    .line 1641774
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    .line 1641775
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1641776
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1641788
    new-instance v0, LX/ABz;

    invoke-direct {v0, p1}, LX/ABz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1641787
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1641801
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1641802
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1641786
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1641783
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;-><init>()V

    .line 1641784
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1641785
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1641782
    const v0, -0x1e96693b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1641781
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1641779
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->e:Ljava/lang/String;

    .line 1641780
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1641777
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    .line 1641778
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    return-object v0
.end method
