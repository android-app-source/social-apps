.class public final Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1640645
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1640646
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1640643
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1640644
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1640628
    if-nez p1, :cond_0

    .line 1640629
    :goto_0
    return v0

    .line 1640630
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1640631
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1640632
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1640633
    const v2, -0x4e5d3c64

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1640634
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 1640635
    const v3, 0x46a6863d

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1640636
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1640637
    const v4, -0x40bb0213    # -0.7694996f

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1640638
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1640639
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1640640
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1640641
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1640642
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x49f2f430
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1640627
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1640618
    packed-switch p2, :pswitch_data_0

    .line 1640619
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1640620
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1640621
    const v1, -0x4e5d3c64

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1640622
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1640623
    const v1, 0x46a6863d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1640624
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1640625
    const v1, -0x40bb0213    # -0.7694996f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1640626
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x49f2f430
        :pswitch_0
    .end packed-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1640612
    if-eqz p1, :cond_0

    .line 1640613
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1640614
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;

    .line 1640615
    if-eq v0, v1, :cond_0

    .line 1640616
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1640617
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1640611
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1640609
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1640610
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1640647
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1640648
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1640649
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1640650
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->b:I

    .line 1640651
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1640608
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1640607
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1640604
    iget v0, p0, LX/1vt;->c:I

    .line 1640605
    move v0, v0

    .line 1640606
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1640601
    iget v0, p0, LX/1vt;->c:I

    .line 1640602
    move v0, v0

    .line 1640603
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1640598
    iget v0, p0, LX/1vt;->b:I

    .line 1640599
    move v0, v0

    .line 1640600
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1640595
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1640596
    move-object v0, v0

    .line 1640597
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1640586
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1640587
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1640588
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1640589
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1640590
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1640591
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1640592
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1640593
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1640594
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1640583
    iget v0, p0, LX/1vt;->c:I

    .line 1640584
    move v0, v0

    .line 1640585
    return v0
.end method
