.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1638051
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1638052
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1638053
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1638054
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1638055
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1638056
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638057
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638058
    if-eqz v2, :cond_4

    .line 1638059
    const-string v3, "reverse_geocode_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638060
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638061
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1638062
    if-eqz v3, :cond_3

    .line 1638063
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638064
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1638065
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_2

    .line 1638066
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v0

    .line 1638067
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638068
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1638069
    if-eqz v2, :cond_0

    .line 1638070
    const-string p2, "address"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638071
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638072
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1638073
    if-eqz v2, :cond_1

    .line 1638074
    const-string p2, "country_alpha_two"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638075
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638076
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638077
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1638078
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1638079
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638080
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638081
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1638082
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
