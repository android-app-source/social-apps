.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1629432
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1629433
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1629434
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1629435
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1629436
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    .line 1629437
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1629438
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1629439
    if-eqz v2, :cond_0

    .line 1629440
    const-string v3, "distance_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1629441
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1629442
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1629443
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1

    .line 1629444
    const-string v4, "latitude"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1629445
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1629446
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1629447
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_2

    .line 1629448
    const-string v4, "longitude"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1629449
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1629450
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1629451
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_3

    .line 1629452
    const-string v4, "radius"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1629453
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1629454
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1629455
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1629456
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;LX/0nX;LX/0my;)V

    return-void
.end method
