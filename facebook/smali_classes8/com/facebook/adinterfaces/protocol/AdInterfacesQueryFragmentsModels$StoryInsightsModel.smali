.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5957eb7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633703
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633702
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1633700
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1633701
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1633698
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633699
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1633685
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633686
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1633687
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 1633688
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633689
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633690
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->h:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633691
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->i:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633692
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633693
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633694
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->l:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633695
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->m:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1633696
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633697
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1633682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633683
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633684
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1633671
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1633672
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->e:I

    .line 1633673
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->f:I

    .line 1633674
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->g:I

    .line 1633675
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->h:I

    .line 1633676
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->i:I

    .line 1633677
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j:I

    .line 1633678
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k:I

    .line 1633679
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->l:I

    .line 1633680
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->m:I

    .line 1633681
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1633669
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633670
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1633704
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;-><init>()V

    .line 1633705
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1633706
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1633653
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633654
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->g:I

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1633667
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633668
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->h:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1633655
    const v0, -0x5416ad32

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1633656
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633657
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->i:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1633658
    const v0, 0x6188ffd0

    return v0
.end method

.method public final it_()I
    .locals 2

    .prologue
    .line 1633659
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633660
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j:I

    return v0
.end method

.method public final iu_()I
    .locals 2

    .prologue
    .line 1633661
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633662
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k:I

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1633663
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633664
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->l:I

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1633665
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633666
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->m:I

    return v0
.end method
