.class public final Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1640429
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1640430
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1640368
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1640369
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1640370
    const/4 v2, 0x0

    .line 1640371
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 1640372
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640373
    :goto_0
    move v1, v2

    .line 1640374
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1640375
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1640376
    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;-><init>()V

    .line 1640377
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1640378
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1640379
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1640380
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1640381
    :cond_0
    return-object v1

    .line 1640382
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640383
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1640384
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1640385
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640386
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_2

    if-eqz v9, :cond_2

    .line 1640387
    const-string v10, "address"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1640388
    invoke-static {p1, v0}, LX/4aS;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1640389
    :cond_3
    const-string v10, "admin_info"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1640390
    invoke-static {p1, v0}, LX/A9X;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1640391
    :cond_4
    const-string v10, "all_phones"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1640392
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1640393
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_5

    .line 1640394
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_5

    .line 1640395
    const/4 v10, 0x0

    .line 1640396
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v11, :cond_f

    .line 1640397
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640398
    :goto_3
    move v9, v10

    .line 1640399
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1640400
    :cond_5
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1640401
    goto :goto_1

    .line 1640402
    :cond_6
    const-string v10, "location"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1640403
    invoke-static {p1, v0}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1640404
    :cond_7
    const-string v10, "page_thumbnail_uri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1640405
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1640406
    :cond_8
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1640407
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1640408
    :cond_9
    const-string v10, "websites"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1640409
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1640410
    :cond_a
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1640411
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1640412
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1640413
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1640414
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1640415
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1640416
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1640417
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1640418
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1

    .line 1640419
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640420
    :cond_d
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_e

    .line 1640421
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1640422
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640423
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_d

    if-eqz v11, :cond_d

    .line 1640424
    const-string p0, "phone_number"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1640425
    invoke-static {p1, v0}, LX/AAD;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_4

    .line 1640426
    :cond_e
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1640427
    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1640428
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    goto/16 :goto_3

    :cond_f
    move v9, v10

    goto :goto_4
.end method
