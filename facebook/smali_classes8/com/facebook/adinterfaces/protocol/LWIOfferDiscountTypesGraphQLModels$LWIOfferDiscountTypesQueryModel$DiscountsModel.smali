.class public final Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xac986ab
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642204
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642205
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642202
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1642203
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1642192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642193
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1642194
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1642195
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1642196
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1642197
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1642198
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1642199
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1642200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642201
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1642189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642191
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642206
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->e:Ljava/lang/String;

    .line 1642207
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1642186
    new-instance v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;-><init>()V

    .line 1642187
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1642188
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1642185
    const v0, -0x1dbdd342

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1642184
    const v0, -0x7c6b0d7

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642180
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->f:Ljava/lang/String;

    .line 1642181
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642182
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->g:Ljava/lang/String;

    .line 1642183
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->g:Ljava/lang/String;

    return-object v0
.end method
