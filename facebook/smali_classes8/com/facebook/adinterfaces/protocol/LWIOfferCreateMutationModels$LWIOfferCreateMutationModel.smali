.class public final Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4372a4b0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642063
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642062
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642054
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1642055
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1642056
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642057
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->a()Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1642058
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1642059
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1642060
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642061
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1642064
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642065
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->a()Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1642066
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->a()Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    .line 1642067
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->a()Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1642068
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;

    .line 1642069
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    .line 1642070
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642071
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642050
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    .line 1642051
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1642047
    new-instance v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;-><init>()V

    .line 1642048
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1642049
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1642052
    const v0, -0x2e1b0434

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1642053
    const v0, 0x7fdeb51c

    return v0
.end method
