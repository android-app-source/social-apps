.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x16787656
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1628510
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1628518
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1628516
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1628517
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628513
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1628514
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1628515
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628511
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->f:Ljava/lang/String;

    .line 1628512
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1628481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1628482
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1628483
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1628484
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1628485
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1628486
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1628487
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1628488
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1628489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1628490
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1628502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1628503
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1628504
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    .line 1628505
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1628506
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;

    .line 1628507
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    .line 1628508
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1628509
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1628519
    new-instance v0, LX/A8o;

    invoke-direct {v0, p1}, LX/A8o;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628501
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1628499
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1628500
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1628498
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1628495
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;-><init>()V

    .line 1628496
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1628497
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1628494
    const v0, -0x58e9d471

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1628493
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628491
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    .line 1628492
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$PromotionInfoModel;

    return-object v0
.end method
