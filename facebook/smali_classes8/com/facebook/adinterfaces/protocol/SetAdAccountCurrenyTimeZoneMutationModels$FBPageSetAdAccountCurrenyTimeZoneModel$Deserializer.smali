.class public final Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1642539
    const-class v0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1642540
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1642541
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1642542
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1642543
    const/4 v2, 0x0

    .line 1642544
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1642545
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1642546
    :goto_0
    move v1, v2

    .line 1642547
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1642548
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1642549
    new-instance v1, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;-><init>()V

    .line 1642550
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1642551
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1642552
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1642553
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1642554
    :cond_0
    return-object v1

    .line 1642555
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1642556
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1642557
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1642558
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1642559
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1642560
    const-string v4, "ad_account"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1642561
    const/4 v3, 0x0

    .line 1642562
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1642563
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1642564
    :goto_2
    move v1, v3

    .line 1642565
    goto :goto_1

    .line 1642566
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1642567
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1642568
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1642569
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1642570
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1642571
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1642572
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1642573
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, p0, :cond_6

    if-eqz v5, :cond_6

    .line 1642574
    const-string v6, "currency"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1642575
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1642576
    :cond_7
    const-string v6, "timezone_info"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1642577
    const/4 v5, 0x0

    .line 1642578
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_d

    .line 1642579
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1642580
    :goto_4
    move v1, v5

    .line 1642581
    goto :goto_3

    .line 1642582
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1642583
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1642584
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1642585
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v4, v3

    goto :goto_3

    .line 1642586
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1642587
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 1642588
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1642589
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1642590
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 1642591
    const-string p0, "timezone"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1642592
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 1642593
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1642594
    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1642595
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_4

    :cond_d
    move v1, v5

    goto :goto_5
.end method
