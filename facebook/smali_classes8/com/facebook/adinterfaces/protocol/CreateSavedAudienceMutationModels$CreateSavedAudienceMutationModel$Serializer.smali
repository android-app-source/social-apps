.class public final Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1639724
    const-class v0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1639725
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1639726
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1639710
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1639711
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1639712
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1639713
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1639714
    if-eqz v2, :cond_1

    .line 1639715
    const-string p0, "ad_account_ad_audiences_edge"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1639716
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1639717
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1639718
    if-eqz p0, :cond_0

    .line 1639719
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1639720
    invoke-static {v1, p0, p1, p2}, LX/A9U;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1639721
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1639722
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1639723
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1639709
    check-cast p1, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
