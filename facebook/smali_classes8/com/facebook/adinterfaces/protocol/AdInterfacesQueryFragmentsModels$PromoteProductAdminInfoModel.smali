.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x132ebbcc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1632948
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1632930
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1632946
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1632947
    return-void
.end method

.method private a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductPromotions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1632944
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    .line 1632945
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1632949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1632950
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1632951
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1632952
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1632953
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1632954
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1632936
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1632937
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1632938
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    .line 1632939
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1632940
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;

    .line 1632941
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    .line 1632942
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1632943
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1632933
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel;-><init>()V

    .line 1632934
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1632935
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1632932
    const v0, -0x46adca35    # -2.00472E-4f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1632931
    const v0, 0xa7c5482

    return v0
.end method
