.class public final Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1642208
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1642209
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1642210
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1642211
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1642212
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1642213
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642214
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1642215
    if-eqz v2, :cond_1

    .line 1642216
    const-string p0, "discounts"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642217
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1642218
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 1642219
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1}, LX/ACD;->a(LX/15i;ILX/0nX;)V

    .line 1642220
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1642221
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1642222
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642223
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1642224
    check-cast p1, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
