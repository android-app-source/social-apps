.class public final Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1642596
    const-class v0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1642597
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1642598
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1642599
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1642600
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1642601
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642602
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1642603
    if-eqz v2, :cond_3

    .line 1642604
    const-string p0, "ad_account"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642605
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642606
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1642607
    if-eqz p0, :cond_0

    .line 1642608
    const-string v0, "currency"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642609
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1642610
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1642611
    if-eqz p0, :cond_2

    .line 1642612
    const-string v0, "timezone_info"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642613
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642614
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1642615
    if-eqz v0, :cond_1

    .line 1642616
    const-string v2, "timezone"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642617
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1642618
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642619
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642620
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642621
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1642622
    check-cast p1, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;LX/0nX;LX/0my;)V

    return-void
.end method
