.class public final Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1642417
    const-class v0, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1642418
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1642416
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1642396
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1642397
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1642398
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642399
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1642400
    if-eqz v2, :cond_0

    .line 1642401
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642402
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1642403
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1642404
    if-eqz v2, :cond_2

    .line 1642405
    const-string p0, "reach_estimate_data"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642406
    const/4 p0, 0x0

    .line 1642407
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642408
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 1642409
    if-eqz p0, :cond_1

    .line 1642410
    const-string v0, "potential_reach"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642411
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1642412
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642413
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642414
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1642415
    check-cast p1, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/ReachEstimateDataQueryModels$ReachEstimateQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
