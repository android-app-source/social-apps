.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x24bf99b1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633632
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633631
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1633629
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1633630
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1633623
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    .line 1633624
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1633625
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1633626
    if-eqz v0, :cond_0

    .line 1633627
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1633628
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1633617
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    .line 1633618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1633619
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1633620
    if-eqz v0, :cond_0

    .line 1633621
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1633622
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1633606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633607
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1633608
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1633609
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1633610
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1633611
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 1633612
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1633613
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1633614
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1633615
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633616
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1633588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633589
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1633590
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    .line 1633591
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1633592
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 1633593
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    .line 1633594
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1633595
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    .line 1633596
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1633597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 1633598
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    .line 1633599
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1633600
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    .line 1633601
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1633602
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 1633603
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    .line 1633604
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633605
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1633587
    new-instance v0, LX/A9G;

    invoke-direct {v0, p1}, LX/A9G;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1633584
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1633585
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->e:Z

    .line 1633586
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1633558
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1633559
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    .line 1633560
    if-eqz v0, :cond_3

    .line 1633561
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1633562
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1633563
    iput v2, p2, LX/18L;->c:I

    .line 1633564
    :goto_0
    return-void

    .line 1633565
    :cond_0
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1633566
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1633567
    if-eqz v0, :cond_3

    .line 1633568
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1633569
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1633570
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1633571
    :cond_1
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1633572
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1633573
    if-eqz v0, :cond_3

    .line 1633574
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1633575
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1633576
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1633577
    :cond_2
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1633578
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1633579
    if-eqz v0, :cond_3

    .line 1633580
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1633581
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1633582
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1633583
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1633553
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1633554
    check-cast p2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;)V

    .line 1633555
    :cond_0
    :goto_0
    return-void

    .line 1633556
    :cond_1
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1633557
    check-cast p2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1633504
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1633505
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    .line 1633506
    if-eqz v0, :cond_0

    .line 1633507
    if-eqz p3, :cond_1

    .line 1633508
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    .line 1633509
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a(I)V

    .line 1633510
    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    .line 1633511
    :cond_0
    :goto_0
    return-void

    .line 1633512
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a(I)V

    goto :goto_0

    .line 1633513
    :cond_2
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1633514
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1633515
    if-eqz v0, :cond_0

    .line 1633516
    if-eqz p3, :cond_3

    .line 1633517
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    .line 1633518
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a(I)V

    .line 1633519
    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    goto :goto_0

    .line 1633520
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a(I)V

    goto :goto_0

    .line 1633521
    :cond_4
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1633522
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1633523
    if-eqz v0, :cond_0

    .line 1633524
    if-eqz p3, :cond_5

    .line 1633525
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    .line 1633526
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a(I)V

    .line 1633527
    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    goto :goto_0

    .line 1633528
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a(I)V

    goto :goto_0

    .line 1633529
    :cond_6
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1633530
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1633531
    if-eqz v0, :cond_0

    .line 1633532
    if-eqz p3, :cond_7

    .line 1633533
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    .line 1633534
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->b(I)V

    .line 1633535
    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 1633536
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->b(I)V

    goto/16 :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1633551
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633552
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->e:Z

    return v0
.end method

.method public final synthetic b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633537
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1633538
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;-><init>()V

    .line 1633539
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1633540
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633541
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633542
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1633543
    const v0, 0x22f256bd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1633544
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633545
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    .line 1633546
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633547
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    .line 1633548
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633549
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    .line 1633550
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    return-object v0
.end method
