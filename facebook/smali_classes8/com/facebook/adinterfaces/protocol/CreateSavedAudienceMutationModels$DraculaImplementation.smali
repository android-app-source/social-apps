.class public final Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1639808
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1639809
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1639806
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1639807
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1639797
    if-nez p1, :cond_0

    move v0, v1

    .line 1639798
    :goto_0
    return v0

    .line 1639799
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1639800
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1639801
    :pswitch_0
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    .line 1639802
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1639803
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1639804
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1639805
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0xae55a73
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1639796
    new-instance v0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1639791
    if-eqz p0, :cond_0

    .line 1639792
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1639793
    if-eq v0, p0, :cond_0

    .line 1639794
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1639795
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1639786
    packed-switch p2, :pswitch_data_0

    .line 1639787
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1639788
    :pswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    .line 1639789
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1639790
    return-void

    :pswitch_data_0
    .packed-switch -0xae55a73
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1639785
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1639783
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1639784
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1639810
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1639811
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1639812
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->a:LX/15i;

    .line 1639813
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->b:I

    .line 1639814
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1639782
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1639781
    new-instance v0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1639778
    iget v0, p0, LX/1vt;->c:I

    .line 1639779
    move v0, v0

    .line 1639780
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1639775
    iget v0, p0, LX/1vt;->c:I

    .line 1639776
    move v0, v0

    .line 1639777
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1639772
    iget v0, p0, LX/1vt;->b:I

    .line 1639773
    move v0, v0

    .line 1639774
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1639757
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1639758
    move-object v0, v0

    .line 1639759
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1639763
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1639764
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1639765
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1639766
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1639767
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1639768
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1639769
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1639770
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1639771
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1639760
    iget v0, p0, LX/1vt;->c:I

    .line 1639761
    move v0, v0

    .line 1639762
    return v0
.end method
