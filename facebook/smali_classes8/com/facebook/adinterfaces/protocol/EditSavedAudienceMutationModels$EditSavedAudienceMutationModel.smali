.class public final Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x14e3ebf8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1639893
    const-class v0, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1639892
    const-class v0, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1639890
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1639891
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1639869
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1639870
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1639871
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1639872
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1639873
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1639874
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1639882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1639883
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1639884
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    .line 1639885
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1639886
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;

    .line 1639887
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    .line 1639888
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1639889
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdAudience"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1639880
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    .line 1639881
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1639877
    new-instance v0, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;-><init>()V

    .line 1639878
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1639879
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1639876
    const v0, -0xb109381

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1639875
    const v0, -0x79f2bc44

    return v0
.end method
