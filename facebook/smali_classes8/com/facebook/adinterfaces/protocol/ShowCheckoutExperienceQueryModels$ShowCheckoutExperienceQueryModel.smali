.class public final Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x33633ffc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642731
    const-class v0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642730
    const-class v0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642710
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1642711
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1642725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642726
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1642727
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1642728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642729
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1642722
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642724
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1642719
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1642720
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;->e:Z

    .line 1642721
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1642717
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1642718
    iget-boolean v0, p0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1642714
    new-instance v0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;-><init>()V

    .line 1642715
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1642716
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1642713
    const v0, 0x34d71a94

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1642712
    const v0, 0x5661d86a

    return v0
.end method
