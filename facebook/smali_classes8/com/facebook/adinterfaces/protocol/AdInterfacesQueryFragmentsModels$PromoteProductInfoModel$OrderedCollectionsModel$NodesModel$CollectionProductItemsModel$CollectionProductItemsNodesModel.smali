.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2e0a5977
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633047
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633017
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1633018
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1633019
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633001
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:Ljava/lang/String;

    .line 1633002
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1633020
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633021
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1633022
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1633023
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1633024
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v3

    .line 1633025
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1633026
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1633027
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1633028
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1633029
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1633030
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1633031
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1633032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633033
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1633034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633035
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1633036
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1633037
    if-eqz v1, :cond_2

    .line 1633038
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    .line 1633039
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:LX/3Sb;

    move-object v1, v0

    .line 1633040
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1633041
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    .line 1633042
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1633043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    .line 1633044
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    .line 1633045
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633046
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633013
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1633014
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;-><init>()V

    .line 1633015
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1633016
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1633012
    const v0, -0x27737f55

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1633011
    const v0, 0xa7c5482

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633009
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->e:Ljava/lang/String;

    .line 1633010
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633007
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->f:Ljava/lang/String;

    .line 1633008
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOrderedImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1633005
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x3

    const v4, 0x68c7dbb0

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:LX/3Sb;

    .line 1633006
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductPromotions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633003
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    .line 1633004
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    return-object v0
.end method
