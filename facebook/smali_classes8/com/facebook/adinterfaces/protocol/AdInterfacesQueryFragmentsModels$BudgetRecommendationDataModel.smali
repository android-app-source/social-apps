.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x349d0ab5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1631039
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1631038
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1631036
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1631037
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1631033
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1631034
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1631035
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1631017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1631018
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1631019
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1631020
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1631021
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1631022
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1631023
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1631024
    iget v5, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->e:I

    invoke-virtual {p1, v6, v5, v6}, LX/186;->a(III)V

    .line 1631025
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1631026
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1631027
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->h:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 1631028
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1631029
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1631030
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1631031
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1631032
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1630999
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1631000
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1631001
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 1631002
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1631003
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    .line 1631004
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 1631005
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1631006
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 1631007
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1631008
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    .line 1631009
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 1631010
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1631011
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 1631012
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1631013
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    .line 1631014
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 1631015
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1631016
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630997
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 1630998
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1630980
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1630981
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->e:I

    .line 1630982
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->h:I

    .line 1630983
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1630994
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;-><init>()V

    .line 1630995
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1630996
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1630993
    const v0, 0x573d9441

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1630992
    const v0, -0x128cb8b8

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630990
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 1630991
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->g:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-object v0
.end method

.method public final k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630988
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 1630989
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630986
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 1630987
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-object v0
.end method

.method public final m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630984
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 1630985
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    return-object v0
.end method
