.class public final Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x52295430
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1641748
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1641768
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1641766
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1641767
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1641760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1641761
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1641762
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1641763
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1641764
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1641765
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1641757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1641758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1641759
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1641754
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1641755
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1641756
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1641751
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;-><init>()V

    .line 1641752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1641753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1641750
    const v0, -0xbe3c5c1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1641749
    const v0, 0x7c02d003

    return v0
.end method
