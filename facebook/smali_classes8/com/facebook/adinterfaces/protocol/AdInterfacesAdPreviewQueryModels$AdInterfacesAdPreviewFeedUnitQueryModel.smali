.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xa22f7d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1628258
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1628257
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1628255
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1628256
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628252
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1628253
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1628254
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628250
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->g:Ljava/lang/String;

    .line 1628251
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1628240
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1628241
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1628242
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v1

    .line 1628243
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1628244
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1628245
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1628246
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1628247
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1628248
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1628249
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1628232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1628233
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1628234
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1628235
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1628236
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;

    .line 1628237
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 1628238
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1628239
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1628231
    new-instance v0, LX/A8j;

    invoke-direct {v0, p1}, LX/A8j;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628230
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1628220
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1628221
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1628229
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1628226
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;-><init>()V

    .line 1628227
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1628228
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1628225
    const v0, 0x74558fa4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1628224
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628222
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 1628223
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method
