.class public final Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3732905e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1639564
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1639563
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1639561
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1639562
    return-void
.end method

.method private a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBoostedComponent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1639540
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1639541
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1639555
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1639556
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1639557
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1639558
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1639559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1639560
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1639547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1639548
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1639549
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1639550
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1639551
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;

    .line 1639552
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1639553
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1639554
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1639544
    new-instance v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;-><init>()V

    .line 1639545
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1639546
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1639543
    const v0, -0x40e83a31

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1639542
    const v0, -0x8530118

    return v0
.end method
