.class public final Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x85c0ced
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1639653
    const-class v0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1639652
    const-class v0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1639650
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1639651
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1639648
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 1639649
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->e:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1639640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1639641
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->j()Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1639642
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1639643
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1639644
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1639645
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1639646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1639647
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1639632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1639633
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1639634
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    .line 1639635
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1639636
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;

    .line 1639637
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    .line 1639638
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1639639
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1639630
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    .line 1639631
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1639625
    new-instance v0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;-><init>()V

    .line 1639626
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1639627
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1639629
    const v0, -0x6bd9a671

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1639628
    const v0, -0x1ac68c69

    return v0
.end method
