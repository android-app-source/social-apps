.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1631546
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1631547
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1631548
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1631549
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    .line 1631550
    if-nez p1, :cond_0

    .line 1631551
    const/4 v0, 0x0

    .line 1631552
    :goto_0
    return v0

    .line 1631553
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1631554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1631555
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631556
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631557
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631558
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631559
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1631560
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631561
    const v1, 0x2f4a3a20

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631562
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1631563
    const v2, 0x105ac95b

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1631564
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631565
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1631566
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631567
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1631568
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631569
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631570
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 1631571
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631572
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631573
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1631574
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1631575
    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v1, v3}, LX/186;->a(III)V

    .line 1631576
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631577
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1631578
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631579
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631580
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 1631581
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631582
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631583
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1631584
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1631585
    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v1, v3}, LX/186;->a(III)V

    .line 1631586
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631587
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631588
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631589
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631590
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631591
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631592
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631593
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631594
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631595
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631596
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631597
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631598
    :sswitch_6
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631599
    const v1, -0x2051e2a5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631600
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631601
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631602
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631603
    :sswitch_7
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1631604
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1631605
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631606
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631607
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631608
    :sswitch_8
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631609
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v1

    .line 1631610
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1631611
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631612
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1631613
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631614
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631615
    :sswitch_9
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631616
    const v1, -0xbffdeba

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631617
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631618
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631619
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631620
    :sswitch_a
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631621
    const v1, 0x1a455f1d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631622
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631623
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631624
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631625
    :sswitch_b
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631626
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1631627
    const v2, -0x2d0af161

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1631628
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631629
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1631630
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631631
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631632
    :sswitch_c
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 1631633
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1631634
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631635
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631636
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631637
    :sswitch_d
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631638
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631639
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631640
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631641
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v2

    .line 1631642
    const v3, 0x55ee9c8e

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1631643
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1631644
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1631645
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v4

    .line 1631646
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v5

    .line 1631647
    const v6, 0x575591d9

    invoke-static {p0, v5, v6, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    .line 1631648
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1631649
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1631650
    const/4 v7, 0x7

    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1631651
    invoke-virtual {p3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1631652
    const/16 v8, 0x8

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1631653
    const/4 v8, 0x0

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 1631654
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631655
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631656
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1631657
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->a(IZ)V

    .line 1631658
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1631659
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1631660
    const/4 v0, 0x7

    invoke-virtual {p3, v0, v7}, LX/186;->b(II)V

    .line 1631661
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631662
    :sswitch_e
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631663
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1631664
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631665
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1631666
    const/4 v1, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v1, v4}, LX/15i;->a(III)I

    move-result v7

    .line 1631667
    const/4 v1, 0x4

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631668
    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v1, v0, v4}, LX/186;->a(III)V

    .line 1631669
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1631670
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1631671
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 1631672
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631673
    :sswitch_f
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v0

    .line 1631674
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1631675
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1631676
    const v2, -0x3de35f80

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1631677
    const/4 v2, 0x3

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631678
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631679
    const/4 v3, 0x4

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1631680
    const/4 v3, 0x1

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1631681
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631682
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631683
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631684
    :sswitch_10
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631685
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631686
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631687
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631688
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631689
    :sswitch_11
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631690
    const v1, 0x4c046c8a    # 3.4714152E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1631691
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631692
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631693
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631694
    :sswitch_12
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631695
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1631696
    const v2, -0x356d9279    # -4798147.5f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1631697
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631698
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1631699
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631700
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631701
    :sswitch_13
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 1631702
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1631703
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631704
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631705
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631706
    :sswitch_14
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631707
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631708
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631709
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631710
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631711
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631712
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1631713
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1631714
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1631715
    const v5, 0xb2193c4

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 1631716
    const/4 v5, 0x5

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1631717
    const/4 v5, 0x0

    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1631718
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631719
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631720
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1631721
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1631722
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631723
    :sswitch_15
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631724
    const v1, -0x27a4f8fe

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631725
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631726
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631727
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631728
    :sswitch_16
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631729
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631730
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631731
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631732
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631733
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631734
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1631735
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1631736
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631737
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631738
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631739
    :sswitch_17
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631740
    const v1, 0x559eed

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1631741
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 1631742
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v2

    .line 1631743
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v3, v4}, LX/15i;->a(III)I

    move-result v3

    .line 1631744
    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, LX/15i;->a(III)I

    move-result v4

    .line 1631745
    const/4 v5, 0x5

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1631746
    const/4 v5, 0x0

    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1631747
    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-virtual {p3, v0, v1, v5}, LX/186;->a(III)V

    .line 1631748
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, LX/186;->a(III)V

    .line 1631749
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v3, v1}, LX/186;->a(III)V

    .line 1631750
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v4, v1}, LX/186;->a(III)V

    .line 1631751
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631752
    :sswitch_18
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631753
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    .line 1631754
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1631755
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631756
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1631757
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631758
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631759
    :sswitch_19
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631760
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1631761
    const v2, -0x23ca768a

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1631762
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631763
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1631764
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631765
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631766
    :sswitch_1a
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631767
    const v1, -0x40504589

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631768
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631769
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631770
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631771
    :sswitch_1b
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631772
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631773
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631774
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631775
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631776
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1631777
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631778
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631779
    :sswitch_1c
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631780
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631781
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631782
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631783
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631784
    :sswitch_1d
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631785
    const v1, -0xf5a4b2f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631786
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631787
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631788
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631789
    :sswitch_1e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631790
    const v1, 0x41b6d7dc    # 22.8554f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1631791
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631792
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631793
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631794
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631795
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1631796
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1631797
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1631798
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1631799
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1631800
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1631801
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1631802
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1631803
    const/4 v7, 0x7

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1631804
    const/4 v7, 0x0

    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 1631805
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631806
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631807
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1631808
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1631809
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1631810
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1631811
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631812
    :sswitch_1f
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631813
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631814
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631815
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631816
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631817
    :sswitch_20
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1631818
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1631819
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631820
    const v1, 0x68774a93

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v8

    .line 1631821
    const/4 v0, 0x3

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1631822
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1631823
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1631824
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 1631825
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631826
    :sswitch_21
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631827
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631828
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631829
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631830
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631831
    :sswitch_22
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631832
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631833
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631834
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631835
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1631836
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1631837
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1631838
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1631839
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1631840
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1631841
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1631842
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1631843
    const/4 v6, 0x6

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1631844
    const/4 v6, 0x0

    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1631845
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631846
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1631847
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1631848
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1631849
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1631850
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631851
    :sswitch_23
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631852
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631853
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631854
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631855
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631856
    :sswitch_24
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631857
    const v1, -0xf25bc95

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1631858
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631859
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631860
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631861
    :sswitch_25
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631862
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631863
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631864
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631865
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631866
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1631867
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631868
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631869
    :sswitch_26
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631870
    const v1, -0xf25bc95

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1631871
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631872
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631873
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631874
    :sswitch_27
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1631875
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1631876
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631877
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631878
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631879
    :sswitch_28
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631880
    const v1, -0xf25bc95

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1631881
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631882
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631883
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631884
    :sswitch_29
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631885
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631886
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1631887
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1631888
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631889
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1631890
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631891
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631892
    :sswitch_2a
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    .line 1631893
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1631894
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631895
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631896
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631897
    :sswitch_2b
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631898
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631899
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631900
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631901
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631902
    :sswitch_2c
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1631903
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1631904
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1631905
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1631906
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631907
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1631908
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631909
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631910
    :sswitch_2d
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1631911
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1631912
    const v2, 0x7935731c

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1631913
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1631914
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1631915
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1631916
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1631917
    :sswitch_2e
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1631918
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1631919
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1631920
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1631921
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7fd442ff -> :sswitch_8
        -0x6cdbb347 -> :sswitch_11
        -0x663e2ea9 -> :sswitch_2d
        -0x60d7eda3 -> :sswitch_6
        -0x4e5d3c64 -> :sswitch_24
        -0x46568e2f -> :sswitch_23
        -0x444c076f -> :sswitch_4
        -0x4378591b -> :sswitch_2c
        -0x40bb0213 -> :sswitch_28
        -0x40504589 -> :sswitch_5
        -0x3de35f80 -> :sswitch_10
        -0x356d9279 -> :sswitch_13
        -0x2d0af161 -> :sswitch_c
        -0x27a4f8fe -> :sswitch_16
        -0x23ca768a -> :sswitch_1a
        -0x2051e2a5 -> :sswitch_7
        -0x1288323e -> :sswitch_12
        -0xf5a4b2f -> :sswitch_1e
        -0xf25bc95 -> :sswitch_9
        -0xbffdeba -> :sswitch_a
        -0x8f3627 -> :sswitch_17
        0x559eed -> :sswitch_18
        0x6f98a5b -> :sswitch_2a
        0xb2193c4 -> :sswitch_15
        0x105ac95b -> :sswitch_3
        0x12233cd8 -> :sswitch_1d
        0x1679edb2 -> :sswitch_1b
        0x186603de -> :sswitch_1
        0x1a455f1d -> :sswitch_b
        0x2f4a3a20 -> :sswitch_2
        0x2fb8d575 -> :sswitch_1c
        0x3632e8d9 -> :sswitch_0
        0x3df6c4e8 -> :sswitch_14
        0x406e2530 -> :sswitch_2b
        0x41b6d7dc -> :sswitch_1f
        0x44d803e6 -> :sswitch_20
        0x46a6863d -> :sswitch_26
        0x4c046c8a -> :sswitch_d
        0x55ee9c8e -> :sswitch_e
        0x575591d9 -> :sswitch_f
        0x6158e9ce -> :sswitch_29
        0x62f008b1 -> :sswitch_19
        0x677befbf -> :sswitch_25
        0x68774a93 -> :sswitch_21
        0x68c7dbb0 -> :sswitch_27
        0x7935731c -> :sswitch_2e
        0x7b2ce237 -> :sswitch_22
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1631922
    if-nez p0, :cond_0

    move v0, v1

    .line 1631923
    :goto_0
    return v0

    .line 1631924
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1631925
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1631926
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1631927
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1631928
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1631929
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1631930
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1631931
    const/4 v7, 0x0

    .line 1631932
    const/4 v1, 0x0

    .line 1631933
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1631934
    invoke-static {v2, v3, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1631935
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1631936
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1631937
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1632016
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1631938
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1631939
    if-eqz v0, :cond_0

    .line 1631940
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1631941
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1631942
    if-eqz p0, :cond_0

    .line 1631943
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1631944
    if-eq v0, p0, :cond_0

    .line 1631945
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1631946
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const v3, -0xf25bc95

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1631947
    sparse-switch p2, :sswitch_data_0

    .line 1631948
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1631949
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631950
    const v1, 0x2f4a3a20

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1631951
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1631952
    const v1, 0x105ac95b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1631953
    :goto_0
    :sswitch_1
    return-void

    .line 1631954
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631955
    const v1, -0x2051e2a5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631956
    :sswitch_3
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1631957
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1631958
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631959
    const v1, -0xbffdeba

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631960
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631961
    const v1, 0x1a455f1d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631962
    :sswitch_6
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1631963
    const v1, -0x2d0af161

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631964
    :sswitch_7
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 1631965
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1631966
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631967
    const v1, 0x55ee9c8e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1631968
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631969
    const v1, 0x575591d9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631970
    :sswitch_9
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631971
    const v1, -0x3de35f80

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631972
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631973
    const v1, 0x4c046c8a    # 3.4714152E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631974
    :sswitch_b
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1631975
    const v1, -0x356d9279    # -4798147.5f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1631976
    :sswitch_c
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 1631977
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1631978
    :sswitch_d
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631979
    const v1, 0xb2193c4

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631980
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631981
    const v1, -0x27a4f8fe

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631982
    :sswitch_f
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631983
    const v1, 0x559eed

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631984
    :sswitch_10
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1631985
    const v1, -0x23ca768a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631986
    :sswitch_11
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631987
    const v1, -0x40504589

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631988
    :sswitch_12
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631989
    const v1, -0xf5a4b2f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631990
    :sswitch_13
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631991
    const v1, 0x41b6d7dc    # 22.8554f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631992
    :sswitch_14
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1631993
    const v1, 0x68774a93

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631994
    :sswitch_15
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631995
    invoke-static {p0, v0, v3, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631996
    :sswitch_16
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1631997
    invoke-static {p0, v0, v3, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1631998
    :sswitch_17
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1631999
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1632000
    :sswitch_18
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1632001
    invoke-static {p0, v0, v3, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1632002
    :sswitch_19
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1632003
    const v1, 0x7935731c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1632004
    :sswitch_1a
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1632005
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7fd442ff -> :sswitch_1
        -0x6cdbb347 -> :sswitch_a
        -0x663e2ea9 -> :sswitch_19
        -0x60d7eda3 -> :sswitch_2
        -0x4e5d3c64 -> :sswitch_15
        -0x46568e2f -> :sswitch_1
        -0x444c076f -> :sswitch_1
        -0x4378591b -> :sswitch_1
        -0x40bb0213 -> :sswitch_18
        -0x40504589 -> :sswitch_1
        -0x3de35f80 -> :sswitch_1
        -0x356d9279 -> :sswitch_c
        -0x2d0af161 -> :sswitch_7
        -0x27a4f8fe -> :sswitch_1
        -0x23ca768a -> :sswitch_11
        -0x2051e2a5 -> :sswitch_3
        -0x1288323e -> :sswitch_b
        -0xf5a4b2f -> :sswitch_13
        -0xf25bc95 -> :sswitch_4
        -0xbffdeba -> :sswitch_5
        -0x8f3627 -> :sswitch_f
        0x559eed -> :sswitch_1
        0x6f98a5b -> :sswitch_1
        0xb2193c4 -> :sswitch_e
        0x105ac95b -> :sswitch_1
        0x12233cd8 -> :sswitch_12
        0x1679edb2 -> :sswitch_1
        0x186603de -> :sswitch_0
        0x1a455f1d -> :sswitch_6
        0x2f4a3a20 -> :sswitch_1
        0x2fb8d575 -> :sswitch_1
        0x3632e8d9 -> :sswitch_1
        0x3df6c4e8 -> :sswitch_d
        0x406e2530 -> :sswitch_1
        0x41b6d7dc -> :sswitch_1
        0x44d803e6 -> :sswitch_14
        0x46a6863d -> :sswitch_16
        0x4c046c8a -> :sswitch_8
        0x55ee9c8e -> :sswitch_1
        0x575591d9 -> :sswitch_9
        0x6158e9ce -> :sswitch_1
        0x62f008b1 -> :sswitch_10
        0x677befbf -> :sswitch_1
        0x68774a93 -> :sswitch_1
        0x68c7dbb0 -> :sswitch_17
        0x7935731c -> :sswitch_1a
        0x7b2ce237 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1632006
    if-nez p1, :cond_0

    move v0, v1

    .line 1632007
    :goto_0
    return v0

    .line 1632008
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1632009
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1632010
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1632011
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1632012
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1632013
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1632014
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1632015
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1631533
    if-eqz p1, :cond_0

    .line 1631534
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1631535
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1631536
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1631537
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1631538
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1631539
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1631540
    if-eqz p1, :cond_0

    .line 1631541
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 1631542
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    .line 1631543
    if-eq v0, v1, :cond_0

    .line 1631544
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1631545
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1631499
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1631500
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1631501
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1631502
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1631503
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1631504
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 1631505
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->b:I

    .line 1631506
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1631507
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1631508
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1631509
    iget v0, p0, LX/1vt;->c:I

    .line 1631510
    move v0, v0

    .line 1631511
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1631512
    iget v0, p0, LX/1vt;->c:I

    .line 1631513
    move v0, v0

    .line 1631514
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1631515
    iget v0, p0, LX/1vt;->b:I

    .line 1631516
    move v0, v0

    .line 1631517
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1631518
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1631519
    move-object v0, v0

    .line 1631520
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1631521
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1631522
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1631523
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1631524
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1631525
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1631526
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1631527
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1631528
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1631529
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1631530
    iget v0, p0, LX/1vt;->c:I

    .line 1631531
    move v0, v0

    .line 1631532
    return v0
.end method
