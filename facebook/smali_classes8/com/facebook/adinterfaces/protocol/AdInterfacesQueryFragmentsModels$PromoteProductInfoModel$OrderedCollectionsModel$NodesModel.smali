.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x67aa954d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633137
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633136
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1633134
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1633135
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1633128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633129
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1633130
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1633131
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1633132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633133
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1633120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633121
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1633122
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    .line 1633123
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1633124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;

    .line 1633125
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    .line 1633126
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633127
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCollectionProductItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633118
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    .line 1633119
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1633113
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;-><init>()V

    .line 1633114
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1633115
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1633117
    const v0, -0x302e115f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1633116
    const v0, -0x46c32e1c

    return v0
.end method
