.class public final Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1640748
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1640749
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1640750
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1640751
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1640752
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 1640753
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640754
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1640755
    if-eqz v2, :cond_0

    .line 1640756
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640757
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1640758
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640759
    if-eqz v2, :cond_7

    .line 1640760
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640761
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640762
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1640763
    if-eqz v3, :cond_2

    .line 1640764
    const-string v4, "boosted_cta_promotions"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640765
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640766
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1640767
    if-eqz v4, :cond_1

    .line 1640768
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640769
    invoke-static {v1, v4, p1, p2}, LX/A9Y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1640770
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640771
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1640772
    if-eqz v3, :cond_4

    .line 1640773
    const-string v4, "boosted_page_like_promotions"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640774
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640775
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1640776
    if-eqz v4, :cond_3

    .line 1640777
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640778
    invoke-static {v1, v4, p1, p2}, LX/A9Y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1640779
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640780
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1640781
    if-eqz v3, :cond_6

    .line 1640782
    const-string v4, "boosted_website_promotions"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640783
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640784
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1640785
    if-eqz v4, :cond_5

    .line 1640786
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640787
    invoke-static {v1, v4, p1, p2}, LX/A9Y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1640788
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640789
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640790
    :cond_7
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1640791
    if-eqz v2, :cond_8

    .line 1640792
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640793
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1640794
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640795
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1640796
    check-cast p1, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
