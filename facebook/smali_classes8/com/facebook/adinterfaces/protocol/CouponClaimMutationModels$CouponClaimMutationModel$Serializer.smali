.class public final Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1639608
    const-class v0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1639609
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1639610
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1639611
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1639612
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1639613
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1639614
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1639615
    if-eqz v2, :cond_0

    .line 1639616
    const-string v2, "claim_result"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1639617
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1639618
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1639619
    if-eqz v2, :cond_1

    .line 1639620
    const-string p0, "claim_result_message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1639621
    invoke-static {v1, v2, p1, p2}, LX/A9u;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1639622
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1639623
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1639624
    check-cast p1, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
