.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1628666
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1628667
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1628668
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1628669
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1628670
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1628671
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1628672
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1628673
    if-eqz v2, :cond_a

    .line 1628674
    const-string v3, "geocode_address_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628675
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1628676
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1628677
    if-eqz v3, :cond_9

    .line 1628678
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628679
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1628680
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_8

    .line 1628681
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    const/4 v0, 0x0

    .line 1628682
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1628683
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1628684
    if-eqz v0, :cond_0

    .line 1628685
    const-string v2, "geo_score"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628686
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1628687
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1628688
    if-eqz v0, :cond_7

    .line 1628689
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628690
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1628691
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628692
    if-eqz v2, :cond_1

    .line 1628693
    const-string p0, "address"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628694
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628695
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628696
    if-eqz v2, :cond_2

    .line 1628697
    const-string p0, "city"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628698
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628699
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628700
    if-eqz v2, :cond_3

    .line 1628701
    const-string p0, "country"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628702
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628703
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628704
    if-eqz v2, :cond_4

    .line 1628705
    const-string p0, "latitude"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628706
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628707
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628708
    if-eqz v2, :cond_5

    .line 1628709
    const-string p0, "longitude"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628710
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628711
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628712
    if-eqz v2, :cond_6

    .line 1628713
    const-string p0, "postal_code"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628714
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628715
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1628716
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1628717
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1628718
    :cond_8
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1628719
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1628720
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1628721
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1628722
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
