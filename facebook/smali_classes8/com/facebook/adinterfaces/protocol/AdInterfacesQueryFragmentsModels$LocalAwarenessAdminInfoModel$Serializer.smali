.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1632400
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1632401
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1632416
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1632403
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1632404
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1632405
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1632406
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1632407
    if-eqz v2, :cond_0

    .line 1632408
    const-string p0, "boosted_local_awareness_promotions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1632409
    invoke-static {v1, v2, p1, p2}, LX/AAA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1632410
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1632411
    if-eqz v2, :cond_1

    .line 1632412
    const-string p0, "messaging_enabled"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1632413
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1632414
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1632415
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1632402
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel;LX/0nX;LX/0my;)V

    return-void
.end method
