.class public final Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1641236
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1641237
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1641238
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1641239
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1641240
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1641241
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1641242
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1641243
    if-eqz v2, :cond_1

    .line 1641244
    const-string p0, "ads_targeting"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1641245
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1641246
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1641247
    if-eqz p0, :cond_0

    .line 1641248
    const-string v0, "locations"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1641249
    invoke-static {v1, p0, p1, p2}, LX/ABe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1641250
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1641251
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1641252
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1641253
    check-cast p1, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
