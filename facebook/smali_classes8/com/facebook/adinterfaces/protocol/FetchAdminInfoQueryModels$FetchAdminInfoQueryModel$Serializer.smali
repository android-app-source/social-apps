.class public final Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1640433
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1640434
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1640435
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1640436
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1640437
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x6

    .line 1640438
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640439
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640440
    if-eqz v2, :cond_0

    .line 1640441
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640442
    invoke-static {v1, v2, p1}, LX/4aS;->a(LX/15i;ILX/0nX;)V

    .line 1640443
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640444
    if-eqz v2, :cond_1

    .line 1640445
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640446
    invoke-static {v1, v2, p1, p2}, LX/A9X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1640447
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640448
    if-eqz v2, :cond_4

    .line 1640449
    const-string v3, "all_phones"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640450
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1640451
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 1640452
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v5

    .line 1640453
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640454
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1640455
    if-eqz v6, :cond_2

    .line 1640456
    const-string p0, "phone_number"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640457
    invoke-static {v1, v6, p1}, LX/AAD;->a(LX/15i;ILX/0nX;)V

    .line 1640458
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640459
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1640460
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1640461
    :cond_4
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640462
    if-eqz v2, :cond_5

    .line 1640463
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640464
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1640465
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640466
    if-eqz v2, :cond_6

    .line 1640467
    const-string v3, "page_thumbnail_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640468
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1640469
    :cond_6
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1640470
    if-eqz v2, :cond_7

    .line 1640471
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640472
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1640473
    :cond_7
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1640474
    if-eqz v2, :cond_8

    .line 1640475
    const-string v2, "websites"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640476
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1640477
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640478
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1640479
    check-cast p1, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
