.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7dc12387
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633830
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1633829
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1633827
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1633828
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1633817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633818
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1633819
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1633820
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x6f98a5b

    invoke-static {v3, v2, v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1633821
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1633822
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1633823
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1633824
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1633825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633826
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1633797
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1633798
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1633799
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 1633800
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1633801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    .line 1633802
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 1633803
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1633804
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 1633805
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1633806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    .line 1633807
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 1633808
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1633809
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6f98a5b

    invoke-static {v2, v0, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1633810
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1633811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    .line 1633812
    iput v3, v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->g:I

    move-object v1, v0

    .line 1633813
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1633814
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1633815
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 1633816
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1633796
    new-instance v0, LX/A9H;

    invoke-direct {v0, p1}, LX/A9H;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633794
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 1633795
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1633831
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1633832
    const/4 v0, 0x2

    const v1, 0x6f98a5b

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->g:I

    .line 1633833
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1633792
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1633793
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1633791
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1633788
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;-><init>()V

    .line 1633789
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1633790
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1633787
    const v0, -0x6126fa0d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1633786
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633784
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 1633785
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPostPromotionInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1633782
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1633783
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
