.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1633755
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1633756
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1633757
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1633758
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1633759
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1633760
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1633761
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1633762
    if-eqz v2, :cond_0

    .line 1633763
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1633764
    invoke-static {v1, v2, p1, p2}, LX/AAP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1633765
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1633766
    if-eqz v2, :cond_1

    .line 1633767
    const-string p0, "insights"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1633768
    invoke-static {v1, v2, p1}, LX/AAQ;->a(LX/15i;ILX/0nX;)V

    .line 1633769
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1633770
    if-eqz v2, :cond_3

    .line 1633771
    const-string p0, "post_promotion_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1633772
    const/4 v0, 0x0

    .line 1633773
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1633774
    invoke-virtual {v1, v2, v0}, LX/15i;->g(II)I

    move-result p0

    .line 1633775
    if-eqz p0, :cond_2

    .line 1633776
    const-string p0, "boosting_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1633777
    invoke-virtual {v1, v2, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1633778
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1633779
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1633780
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1633781
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;LX/0nX;LX/0my;)V

    return-void
.end method
