.class public final Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1641042
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1641043
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1641040
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1641041
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1641021
    if-nez p1, :cond_0

    move v0, v1

    .line 1641022
    :goto_0
    return v0

    .line 1641023
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1641024
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1641025
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1641026
    const v2, -0x1dbf61e9

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1641027
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1641028
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1641029
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1641030
    :sswitch_1
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1641031
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1641032
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1641033
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1641034
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1641035
    :sswitch_2
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    .line 1641036
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1641037
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1641038
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1641039
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7cb417df -> :sswitch_0
        -0x1dbf61e9 -> :sswitch_1
        0x5484f619 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1641020
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1641016
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1641017
    if-eqz v0, :cond_0

    .line 1641018
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1641019
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1641011
    if-eqz p0, :cond_0

    .line 1641012
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1641013
    if-eq v0, p0, :cond_0

    .line 1641014
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1641015
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1641002
    sparse-switch p2, :sswitch_data_0

    .line 1641003
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1641004
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1641005
    const v1, -0x1dbf61e9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1641006
    :goto_0
    return-void

    .line 1641007
    :sswitch_1
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1641008
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1641009
    :sswitch_2
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    .line 1641010
    invoke-static {v0, p3}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7cb417df -> :sswitch_0
        -0x1dbf61e9 -> :sswitch_1
        0x5484f619 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1640996
    if-eqz p1, :cond_0

    .line 1640997
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1640998
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;

    .line 1640999
    if-eq v0, v1, :cond_0

    .line 1641000
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1641001
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1640995
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1640993
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1640994
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1640962
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1640963
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1640964
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1640965
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->b:I

    .line 1640966
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1640992
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1640991
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1640988
    iget v0, p0, LX/1vt;->c:I

    .line 1640989
    move v0, v0

    .line 1640990
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1640985
    iget v0, p0, LX/1vt;->c:I

    .line 1640986
    move v0, v0

    .line 1640987
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1640982
    iget v0, p0, LX/1vt;->b:I

    .line 1640983
    move v0, v0

    .line 1640984
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1640979
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1640980
    move-object v0, v0

    .line 1640981
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1640970
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1640971
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1640972
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1640973
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1640974
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1640975
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1640976
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1640977
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1640978
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1640967
    iget v0, p0, LX/1vt;->c:I

    .line 1640968
    move v0, v0

    .line 1640969
    return v0
.end method
