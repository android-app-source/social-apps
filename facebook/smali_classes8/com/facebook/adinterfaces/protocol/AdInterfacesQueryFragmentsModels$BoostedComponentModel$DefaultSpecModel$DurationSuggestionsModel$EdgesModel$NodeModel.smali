.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3569bfbb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1630310
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1630311
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1630289
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1630290
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1630308
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1630309
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1630301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1630302
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1630303
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1630304
    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1630305
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1630306
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1630307
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1630312
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1630313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1630314
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1630298
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1630299
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->e:I

    .line 1630300
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1630295
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;-><init>()V

    .line 1630296
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1630297
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1630294
    const v0, -0x79ac9f81

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1630293
    const v0, 0x3f9bedd3

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630291
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1630292
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method
