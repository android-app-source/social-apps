.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageBaseFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1632446
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageBaseFieldsModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageBaseFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageBaseFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1632447
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1632448
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1632449
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1632450
    const/4 v2, 0x0

    .line 1632451
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 1632452
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1632453
    :goto_0
    move v1, v2

    .line 1632454
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1632455
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1632456
    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageBaseFieldsModel;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageBaseFieldsModel;-><init>()V

    .line 1632457
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1632458
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1632459
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1632460
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1632461
    :cond_0
    return-object v1

    .line 1632462
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1632463
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_a

    .line 1632464
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1632465
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1632466
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 1632467
    const-string p0, "about"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1632468
    invoke-static {p1, v0}, LX/AAB;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1632469
    :cond_3
    const-string p0, "cover_photo"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1632470
    invoke-static {p1, v0}, LX/A9V;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1632471
    :cond_4
    const-string p0, "id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1632472
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1632473
    :cond_5
    const-string p0, "name"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1632474
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1632475
    :cond_6
    const-string p0, "page_thumbnail_uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1632476
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1632477
    :cond_7
    const-string p0, "profile_picture"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1632478
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1632479
    :cond_8
    const-string p0, "url"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1632480
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1632481
    :cond_9
    const-string p0, "websites"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1632482
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1632483
    :cond_a
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1632484
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1632485
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1632486
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1632487
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1632488
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1632489
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1632490
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1632491
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1632492
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
