.class public final Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1638630
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1638631
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1638632
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1638633
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1638634
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v4, 0xe

    .line 1638635
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638636
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638637
    if-eqz v2, :cond_0

    .line 1638638
    const-string v3, "about"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638639
    invoke-static {v1, v2, p1}, LX/AAB;->a(LX/15i;ILX/0nX;)V

    .line 1638640
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638641
    if-eqz v2, :cond_1

    .line 1638642
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638643
    invoke-static {v1, v2, p1}, LX/4aS;->a(LX/15i;ILX/0nX;)V

    .line 1638644
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638645
    if-eqz v2, :cond_2

    .line 1638646
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638647
    invoke-static {v1, v2, p1, p2}, LX/A9X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1638648
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638649
    if-eqz v2, :cond_5

    .line 1638650
    const-string v3, "all_phones"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638651
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1638652
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v5

    if-ge v3, v5, :cond_4

    .line 1638653
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v5

    .line 1638654
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638655
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1638656
    if-eqz v6, :cond_3

    .line 1638657
    const-string p0, "phone_number"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638658
    invoke-static {v1, v6, p1}, LX/AAD;->a(LX/15i;ILX/0nX;)V

    .line 1638659
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638660
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1638661
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1638662
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638663
    if-eqz v2, :cond_6

    .line 1638664
    const-string v3, "commerce_store"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638665
    invoke-static {v1, v2, p1, p2}, LX/AAK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1638666
    :cond_6
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638667
    if-eqz v2, :cond_7

    .line 1638668
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638669
    invoke-static {v1, v2, p1, p2}, LX/A9V;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1638670
    :cond_7
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1638671
    if-eqz v2, :cond_8

    .line 1638672
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638673
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638674
    :cond_8
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638675
    if-eqz v2, :cond_9

    .line 1638676
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638677
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1638678
    :cond_9
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1638679
    if-eqz v2, :cond_a

    .line 1638680
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638681
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638682
    :cond_a
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638683
    if-eqz v2, :cond_f

    .line 1638684
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638685
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1638686
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638687
    invoke-virtual {v1, v2, v5}, LX/15i;->g(II)I

    move-result v3

    .line 1638688
    if-eqz v3, :cond_b

    .line 1638689
    const-string v3, "ads_cta_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638690
    invoke-virtual {v1, v2, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638691
    :cond_b
    invoke-virtual {v1, v2, v6}, LX/15i;->g(II)I

    move-result v3

    .line 1638692
    if-eqz v3, :cond_c

    .line 1638693
    const-string v3, "cta_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638694
    invoke-virtual {v1, v2, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638695
    :cond_c
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1638696
    if-eqz v3, :cond_d

    .line 1638697
    const-string v5, "desktop_uri"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638698
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638699
    :cond_d
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1638700
    if-eqz v3, :cond_e

    .line 1638701
    const-string v5, "label"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638702
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638703
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638704
    :cond_f
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638705
    if-eqz v2, :cond_10

    .line 1638706
    const-string v3, "page_thumbnail_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638707
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1638708
    :cond_10
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638709
    if-eqz v2, :cond_11

    .line 1638710
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638711
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1638712
    :cond_11
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638713
    if-eqz v2, :cond_12

    .line 1638714
    const-string v3, "timeline_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638715
    invoke-static {v1, v2, p1, p2}, LX/AAr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1638716
    :cond_12
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1638717
    if-eqz v2, :cond_13

    .line 1638718
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638719
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1638720
    :cond_13
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1638721
    if-eqz v2, :cond_14

    .line 1638722
    const-string v2, "websites"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638723
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1638724
    :cond_14
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638725
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1638726
    check-cast p1, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
