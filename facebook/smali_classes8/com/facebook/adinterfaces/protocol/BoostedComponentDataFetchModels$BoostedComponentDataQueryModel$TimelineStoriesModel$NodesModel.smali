.class public final Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x8f2d602
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1638763
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1638762
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1638794
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1638795
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1638792
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->e:Ljava/lang/String;

    .line 1638793
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPostPromotionInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1638790
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1638791
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1638782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1638783
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1638784
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1638785
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1638786
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1638787
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1638788
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1638789
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1638774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1638775
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1638776
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1638777
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1638778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;

    .line 1638779
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1638780
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1638781
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1638773
    new-instance v0, LX/AAm;

    invoke-direct {v0, p1}, LX/AAm;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1638772
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1638770
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1638771
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1638769
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1638766
    new-instance v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel$TimelineStoriesModel$NodesModel;-><init>()V

    .line 1638767
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1638768
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1638765
    const v0, -0x5cf9142b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1638764
    const v0, 0x4c808d5

    return v0
.end method
