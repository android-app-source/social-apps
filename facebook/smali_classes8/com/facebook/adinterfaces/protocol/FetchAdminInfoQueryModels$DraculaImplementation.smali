.class public final Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1640366
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1640367
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1640364
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1640365
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1640355
    if-nez p1, :cond_0

    .line 1640356
    :goto_0
    return v0

    .line 1640357
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1640358
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1640359
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1640360
    const v2, 0x677befbf

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1640361
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1640362
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1640363
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x663a7ee2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1640346
    if-nez p0, :cond_0

    move v0, v1

    .line 1640347
    :goto_0
    return v0

    .line 1640348
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1640349
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1640350
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1640351
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1640352
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1640353
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1640354
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1640339
    const/4 v7, 0x0

    .line 1640340
    const/4 v1, 0x0

    .line 1640341
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1640342
    invoke-static {v2, v3, v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1640343
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1640344
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1640345
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1640338
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1640333
    packed-switch p2, :pswitch_data_0

    .line 1640334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1640335
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1640336
    const v1, 0x677befbf

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1640337
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x663a7ee2
        :pswitch_0
    .end packed-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1640327
    if-eqz p1, :cond_0

    .line 1640328
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1640329
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    .line 1640330
    if-eq v0, v1, :cond_0

    .line 1640331
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1640332
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1640326
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1640293
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1640294
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1640321
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1640322
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1640323
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1640324
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->b:I

    .line 1640325
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1640320
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1640319
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1640316
    iget v0, p0, LX/1vt;->c:I

    .line 1640317
    move v0, v0

    .line 1640318
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1640313
    iget v0, p0, LX/1vt;->c:I

    .line 1640314
    move v0, v0

    .line 1640315
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1640310
    iget v0, p0, LX/1vt;->b:I

    .line 1640311
    move v0, v0

    .line 1640312
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1640307
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1640308
    move-object v0, v0

    .line 1640309
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1640298
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1640299
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1640300
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1640301
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1640302
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1640303
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1640304
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1640305
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1640306
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1640295
    iget v0, p0, LX/1vt;->c:I

    .line 1640296
    move v0, v0

    .line 1640297
    return v0
.end method
