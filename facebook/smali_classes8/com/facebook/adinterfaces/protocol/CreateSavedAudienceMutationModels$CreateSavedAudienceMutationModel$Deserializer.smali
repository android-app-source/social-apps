.class public final Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1639669
    const-class v0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1639670
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1639671
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1639672
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1639673
    const/4 v2, 0x0

    .line 1639674
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1639675
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1639676
    :goto_0
    move v1, v2

    .line 1639677
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1639678
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1639679
    new-instance v1, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;-><init>()V

    .line 1639680
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1639681
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1639682
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1639683
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1639684
    :cond_0
    return-object v1

    .line 1639685
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1639686
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1639687
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1639688
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1639689
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1639690
    const-string v4, "ad_account_ad_audiences_edge"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1639691
    const/4 v3, 0x0

    .line 1639692
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1639693
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1639694
    :goto_2
    move v1, v3

    .line 1639695
    goto :goto_1

    .line 1639696
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1639697
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1639698
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1639699
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1639700
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1639701
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1639702
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1639703
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1639704
    const-string p0, "node"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1639705
    invoke-static {p1, v0}, LX/A9U;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 1639706
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1639707
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1639708
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3
.end method
