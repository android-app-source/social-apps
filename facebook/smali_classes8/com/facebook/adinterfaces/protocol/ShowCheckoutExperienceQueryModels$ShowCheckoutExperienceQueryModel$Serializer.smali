.class public final Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1642697
    const-class v0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1642698
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1642699
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1642700
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1642701
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1642702
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1642703
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1642704
    if-eqz p0, :cond_0

    .line 1642705
    const-string p2, "show_checkout_experience"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1642706
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1642707
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1642708
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1642709
    check-cast p1, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
