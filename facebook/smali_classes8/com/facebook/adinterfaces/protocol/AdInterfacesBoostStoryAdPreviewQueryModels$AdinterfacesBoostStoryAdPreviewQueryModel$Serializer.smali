.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1628479
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1628480
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1628478
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1628461
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1628462
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1628463
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1628464
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1628465
    if-eqz v2, :cond_0

    .line 1628466
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628467
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1628468
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628469
    if-eqz v2, :cond_1

    .line 1628470
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628471
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628472
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1628473
    if-eqz v2, :cond_2

    .line 1628474
    const-string p0, "promotion_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628475
    invoke-static {v1, v2, p1, p2}, LX/BYT;->a$redex0(LX/15i;ILX/0nX;LX/0my;)V

    .line 1628476
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1628477
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1628460
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesBoostStoryAdPreviewQueryModels$AdinterfacesBoostStoryAdPreviewQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
