.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55872a67
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1628589
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1628588
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1628586
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1628587
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1628578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1628579
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1628580
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1628581
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1628582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1628583
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1628590
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1628591
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1628592
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628584
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;->e:Ljava/lang/String;

    .line 1628585
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1628575
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;-><init>()V

    .line 1628576
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1628577
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1628574
    const v0, 0x1d85d89d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1628573
    const v0, -0x419661d

    return v0
.end method
