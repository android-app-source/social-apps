.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x51c5e775
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1629983
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1629982
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1629959
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1629960
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1629976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1629977
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1629978
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1629979
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1629980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1629981
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1629968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1629969
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1629970
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    .line 1629971
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1629972
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;

    .line 1629973
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    .line 1629974
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1629975
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1629966
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    .line 1629967
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1629963
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel$PromotedObjectModel;-><init>()V

    .line 1629964
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1629965
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1629962
    const v0, -0x6a84ae17

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1629961
    const v0, -0xd954fef

    return v0
.end method
