.class public final Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3340b003
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642033
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1642032
    const-class v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642030
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1642031
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642028
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->e:Ljava/lang/String;

    .line 1642029
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642026
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->h:Ljava/lang/String;

    .line 1642027
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1642015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642016
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1642017
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1642018
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1642019
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1642020
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1642021
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1642022
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1642023
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1642024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642025
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1642012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1642013
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1642014
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642011
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1642001
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1642002
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->f:J

    .line 1642003
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1642008
    new-instance v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;-><init>()V

    .line 1642009
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1642010
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1642007
    const v0, 0x5913bbf2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1642006
    const v0, 0x4892a3c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642004
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->g:Ljava/lang/String;

    .line 1642005
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->g:Ljava/lang/String;

    return-object v0
.end method
