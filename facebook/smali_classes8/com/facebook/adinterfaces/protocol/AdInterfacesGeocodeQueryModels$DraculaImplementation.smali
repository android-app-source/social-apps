.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1628835
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1628836
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1628833
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1628834
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1628807
    if-nez p1, :cond_0

    .line 1628808
    :goto_0
    return v0

    .line 1628809
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1628810
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1628811
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1628812
    const v2, -0x49251d43

    const/4 v5, 0x0

    .line 1628813
    if-nez v1, :cond_1

    move v3, v5

    .line 1628814
    :goto_1
    move v1, v3

    .line 1628815
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1628816
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1628817
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1628818
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1628819
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1628820
    const v3, 0x7b2ce237

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1628821
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1628822
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1628823
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1628824
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1628825
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 1628826
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 1628827
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1628828
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1628829
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 1628830
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1628831
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 1628832
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x49251d43 -> :sswitch_1
        -0x12aaf2a3 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1628806
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1628793
    sparse-switch p2, :sswitch_data_0

    .line 1628794
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1628795
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1628796
    const v1, -0x49251d43

    .line 1628797
    if-eqz v0, :cond_0

    .line 1628798
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1628799
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1628800
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1628801
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1628802
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1628803
    :cond_0
    :goto_1
    return-void

    .line 1628804
    :sswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1628805
    const v1, 0x7b2ce237

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x49251d43 -> :sswitch_1
        -0x12aaf2a3 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1628787
    if-eqz p1, :cond_0

    .line 1628788
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1628789
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;

    .line 1628790
    if-eq v0, v1, :cond_0

    .line 1628791
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1628792
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1628753
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1628785
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1628786
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1628780
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1628781
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1628782
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1628783
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->b:I

    .line 1628784
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1628779
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1628778
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1628775
    iget v0, p0, LX/1vt;->c:I

    .line 1628776
    move v0, v0

    .line 1628777
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1628772
    iget v0, p0, LX/1vt;->c:I

    .line 1628773
    move v0, v0

    .line 1628774
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1628769
    iget v0, p0, LX/1vt;->b:I

    .line 1628770
    move v0, v0

    .line 1628771
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1628766
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1628767
    move-object v0, v0

    .line 1628768
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1628757
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1628758
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1628759
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1628760
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1628761
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1628762
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1628763
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1628764
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1628765
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1628754
    iget v0, p0, LX/1vt;->c:I

    .line 1628755
    move v0, v0

    .line 1628756
    return v0
.end method
