.class public final Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1640652
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1640653
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1640654
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1640655
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1640656
    const/4 v2, 0x0

    .line 1640657
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 1640658
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640659
    :goto_0
    move v1, v2

    .line 1640660
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1640661
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1640662
    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;-><init>()V

    .line 1640663
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1640664
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1640665
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1640666
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1640667
    :cond_0
    return-object v1

    .line 1640668
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640669
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1640670
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1640671
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640672
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 1640673
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1640674
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 1640675
    :cond_4
    const-string v6, "admin_info"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1640676
    const/4 v5, 0x0

    .line 1640677
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_d

    .line 1640678
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640679
    :goto_2
    move v3, v5

    .line 1640680
    goto :goto_1

    .line 1640681
    :cond_5
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1640682
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1640683
    :cond_6
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1640684
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1640685
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1640686
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1640687
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1

    .line 1640688
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640689
    :cond_9
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_c

    .line 1640690
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1640691
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640692
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_9

    if-eqz v8, :cond_9

    .line 1640693
    const-string v9, "boosted_cta_promotions"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1640694
    const/4 v8, 0x0

    .line 1640695
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_11

    .line 1640696
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640697
    :goto_4
    move v7, v8

    .line 1640698
    goto :goto_3

    .line 1640699
    :cond_a
    const-string v9, "boosted_page_like_promotions"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1640700
    const/4 v8, 0x0

    .line 1640701
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_15

    .line 1640702
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640703
    :goto_5
    move v6, v8

    .line 1640704
    goto :goto_3

    .line 1640705
    :cond_b
    const-string v9, "boosted_website_promotions"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1640706
    const/4 v8, 0x0

    .line 1640707
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v9, :cond_19

    .line 1640708
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640709
    :goto_6
    move v3, v8

    .line 1640710
    goto :goto_3

    .line 1640711
    :cond_c
    const/4 v8, 0x3

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1640712
    invoke-virtual {v0, v5, v7}, LX/186;->b(II)V

    .line 1640713
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 1640714
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 1640715
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_d
    move v3, v5

    move v6, v5

    move v7, v5

    goto :goto_3

    .line 1640716
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640717
    :cond_f
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_10

    .line 1640718
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1640719
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640720
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v9, :cond_f

    .line 1640721
    const-string p0, "nodes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1640722
    invoke-static {p1, v0}, LX/A9Y;->b(LX/15w;LX/186;)I

    move-result v7

    goto :goto_7

    .line 1640723
    :cond_10
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1640724
    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1640725
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_4

    :cond_11
    move v7, v8

    goto :goto_7

    .line 1640726
    :cond_12
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640727
    :cond_13
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_14

    .line 1640728
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1640729
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640730
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_13

    if-eqz v9, :cond_13

    .line 1640731
    const-string p0, "nodes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 1640732
    invoke-static {p1, v0}, LX/A9Y;->b(LX/15w;LX/186;)I

    move-result v6

    goto :goto_8

    .line 1640733
    :cond_14
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1640734
    invoke-virtual {v0, v8, v6}, LX/186;->b(II)V

    .line 1640735
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_15
    move v6, v8

    goto :goto_8

    .line 1640736
    :cond_16
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1640737
    :cond_17
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_18

    .line 1640738
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1640739
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1640740
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_17

    if-eqz v9, :cond_17

    .line 1640741
    const-string p0, "nodes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 1640742
    invoke-static {p1, v0}, LX/A9Y;->b(LX/15w;LX/186;)I

    move-result v3

    goto :goto_9

    .line 1640743
    :cond_18
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1640744
    invoke-virtual {v0, v8, v3}, LX/186;->b(II)V

    .line 1640745
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_6

    :cond_19
    move v3, v8

    goto :goto_9
.end method
