.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1628201
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1628202
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1628200
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1628203
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1628204
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1628205
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1628206
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1628207
    if-eqz v2, :cond_0

    .line 1628208
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628209
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1628210
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1628211
    if-eqz v2, :cond_1

    .line 1628212
    const-string p0, "feed_unit_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628213
    invoke-static {v1, v2, p1, p2}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1628214
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1628215
    if-eqz v2, :cond_2

    .line 1628216
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1628217
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1628218
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1628219
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1628199
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
