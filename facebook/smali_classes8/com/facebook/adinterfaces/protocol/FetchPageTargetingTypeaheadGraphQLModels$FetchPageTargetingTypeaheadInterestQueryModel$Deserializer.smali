.class public final Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1641044
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1641045
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1641046
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1641047
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1641048
    const/4 v2, 0x0

    .line 1641049
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1641050
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1641051
    :goto_0
    move v1, v2

    .line 1641052
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1641053
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1641054
    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;-><init>()V

    .line 1641055
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1641056
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1641057
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1641058
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1641059
    :cond_0
    return-object v1

    .line 1641060
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1641061
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1641062
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1641063
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1641064
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1641065
    const-string v4, "ads_targeting"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1641066
    const/4 v3, 0x0

    .line 1641067
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1641068
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1641069
    :goto_2
    move v1, v3

    .line 1641070
    goto :goto_1

    .line 1641071
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1641072
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1641073
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1641074
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1641075
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1641076
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1641077
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1641078
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, p0, :cond_6

    if-eqz v4, :cond_6

    .line 1641079
    const-string v5, "interests"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1641080
    const/4 v4, 0x0

    .line 1641081
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 1641082
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1641083
    :goto_4
    move v1, v4

    .line 1641084
    goto :goto_3

    .line 1641085
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1641086
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1641087
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 1641088
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1641089
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_b

    .line 1641090
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1641091
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1641092
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v5, :cond_a

    .line 1641093
    const-string p0, "nodes"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1641094
    invoke-static {p1, v0}, LX/AA8;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_5

    .line 1641095
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1641096
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1641097
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_c
    move v1, v4

    goto :goto_5
.end method
