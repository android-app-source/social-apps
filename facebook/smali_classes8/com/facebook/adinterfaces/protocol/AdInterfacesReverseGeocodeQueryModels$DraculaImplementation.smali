.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1638179
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1638180
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1638181
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1638182
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1638113
    if-nez p1, :cond_0

    .line 1638114
    :goto_0
    return v0

    .line 1638115
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1638116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1638117
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1638118
    const v2, 0x6158e9ce

    const/4 v4, 0x0

    .line 1638119
    if-nez v1, :cond_1

    move v3, v4

    .line 1638120
    :goto_1
    move v1, v3

    .line 1638121
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1638122
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1638123
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1638124
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 1638125
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 1638126
    :goto_2
    if-ge v4, p1, :cond_3

    .line 1638127
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result p2

    .line 1638128
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v4

    .line 1638129
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1638130
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 1638131
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x51be21a5
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1638178
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1638167
    packed-switch p2, :pswitch_data_0

    .line 1638168
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1638169
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1638170
    const v1, 0x6158e9ce

    .line 1638171
    if-eqz v0, :cond_0

    .line 1638172
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1638173
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1638174
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1638175
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1638176
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1638177
    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x51be21a5
        :pswitch_0
    .end packed-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1638161
    if-eqz p1, :cond_0

    .line 1638162
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1638163
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;

    .line 1638164
    if-eq v0, v1, :cond_0

    .line 1638165
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1638166
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1638160
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1638158
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1638159
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1638183
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1638184
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1638185
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1638186
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->b:I

    .line 1638187
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1638157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1638156
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1638153
    iget v0, p0, LX/1vt;->c:I

    .line 1638154
    move v0, v0

    .line 1638155
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1638150
    iget v0, p0, LX/1vt;->c:I

    .line 1638151
    move v0, v0

    .line 1638152
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1638147
    iget v0, p0, LX/1vt;->b:I

    .line 1638148
    move v0, v0

    .line 1638149
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1638144
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1638145
    move-object v0, v0

    .line 1638146
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1638135
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1638136
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1638137
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1638138
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1638139
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1638140
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1638141
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1638142
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1638143
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1638132
    iget v0, p0, LX/1vt;->c:I

    .line 1638133
    move v0, v0

    .line 1638134
    return v0
.end method
