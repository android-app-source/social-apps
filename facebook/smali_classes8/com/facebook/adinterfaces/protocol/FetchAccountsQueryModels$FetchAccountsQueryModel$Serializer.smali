.class public final Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1640074
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1640075
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1640076
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1640077
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1640078
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1640079
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640080
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1640081
    if-eqz v2, :cond_1

    .line 1640082
    const-string p0, "admin_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640083
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1640084
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1640085
    if-eqz p0, :cond_0

    .line 1640086
    const-string v0, "ad_accounts"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1640087
    invoke-static {v1, p0, p1, p2}, LX/A9T;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1640088
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640089
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1640090
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1640091
    check-cast p1, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
