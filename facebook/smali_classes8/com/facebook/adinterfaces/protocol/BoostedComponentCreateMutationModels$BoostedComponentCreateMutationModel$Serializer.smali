.class public final Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1638391
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;

    new-instance v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1638392
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1638393
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1638394
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1638395
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1638396
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1638397
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638398
    if-eqz v2, :cond_0

    .line 1638399
    const-string p0, "boosted_component"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638400
    invoke-static {v1, v2, p1, p2}, LX/A9t;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1638401
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1638402
    if-eqz v2, :cond_2

    .line 1638403
    const-string p0, "validation_messages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1638404
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1638405
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1638406
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/A9u;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1638407
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1638408
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1638409
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1638410
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1638411
    check-cast p1, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Serializer;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
