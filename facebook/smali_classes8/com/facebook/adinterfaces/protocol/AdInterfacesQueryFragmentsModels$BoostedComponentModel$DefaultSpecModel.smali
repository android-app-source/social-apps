.class public final Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6da87598
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1630477
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1630476
    const-class v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1630474
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1630475
    return-void
.end method

.method private q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630472
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    .line 1630473
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1630451
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1630452
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1630453
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->c(Ljava/util/List;)I

    move-result v1

    .line 1630454
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x1288323e

    invoke-static {v3, v2, v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1630455
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1630456
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x3df6c4e8

    invoke-static {v5, v4, v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1630457
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1630458
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1630459
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->p()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1630460
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1630461
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1630462
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1630463
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1630464
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->h:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 1630465
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1630466
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1630467
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1630468
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1630469
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1630470
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1630471
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1630415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1630416
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1630417
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    .line 1630418
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1630419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630420
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    .line 1630421
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1630422
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1288323e

    invoke-static {v2, v0, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1630423
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1630424
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630425
    iput v3, v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->g:I

    move-object v1, v0

    .line 1630426
    :cond_1
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1630427
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    .line 1630428
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1630429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630430
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$CallToActionModel;

    .line 1630431
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1630432
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3df6c4e8

    invoke-static {v2, v0, v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1630433
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1630434
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630435
    iput v3, v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->j:I

    move-object v1, v0

    .line 1630436
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1630437
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 1630438
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1630439
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630440
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 1630441
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1630442
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    .line 1630443
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1630444
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630445
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->l:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    .line 1630446
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1630447
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 1630448
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1630449
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_6
    move-object p0, v1

    .line 1630450
    goto :goto_0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630413
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    .line 1630414
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$AdAccountModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1630408
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1630409
    const/4 v0, 0x2

    const v1, -0x1288323e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->g:I

    .line 1630410
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->h:I

    .line 1630411
    const/4 v0, 0x5

    const v1, 0x3df6c4e8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->j:I

    .line 1630412
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1630478
    new-instance v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;-><init>()V

    .line 1630479
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1630480
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1630392
    const v0, 0x1f250170

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1630393
    const v0, 0x489ccee1

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1630394
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->f:Ljava/util/List;

    .line 1630395
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAvailableAudiences"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630396
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1630397
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1630398
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1630399
    iget v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->h:I

    return v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDefaultCreativeSpec"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630400
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1630401
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDefaultTargetSpec"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630402
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 1630403
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630404
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->l:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->l:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    .line 1630405
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->l:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1630406
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 1630407
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    return-object v0
.end method
