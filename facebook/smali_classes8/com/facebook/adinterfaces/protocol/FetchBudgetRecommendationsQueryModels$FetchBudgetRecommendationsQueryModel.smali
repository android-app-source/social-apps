.class public final Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x13d21055
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1640936
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1640935
    const-class v0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1640908
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1640909
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1640929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1640930
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1640931
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1640932
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1640933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1640934
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1640921
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1640922
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1640923
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    .line 1640924
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1640925
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;

    .line 1640926
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    .line 1640927
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1640928
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1640920
    new-instance v0, LX/ABX;

    invoke-direct {v0, p1}, LX/ABX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1640918
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    .line 1640919
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1640916
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1640917
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1640915
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1640912
    new-instance v0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;-><init>()V

    .line 1640913
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1640914
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1640911
    const v0, 0x2313fe69

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1640910
    const v0, 0x25d6af

    return v0
.end method
