.class public final Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1642739
    new-instance v0, LX/ACS;

    invoke-direct {v0}, LX/ACS;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1642740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1642741
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->a:Ljava/lang/String;

    .line 1642742
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->b:Ljava/lang/String;

    .line 1642743
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1642744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1642745
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->a:Ljava/lang/String;

    .line 1642746
    iput-object p2, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->b:Ljava/lang/String;

    .line 1642747
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1642748
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1642749
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1642750
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1642751
    return-void
.end method
