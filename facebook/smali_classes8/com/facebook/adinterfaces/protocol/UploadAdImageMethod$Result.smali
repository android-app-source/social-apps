.class public final Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1642767
    new-instance v0, LX/ACT;

    invoke-direct {v0}, LX/ACT;-><init>()V

    sput-object v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1642763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1642764
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->a:Ljava/lang/String;

    .line 1642765
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->b:Ljava/lang/String;

    .line 1642766
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1642755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1642756
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->a:Ljava/lang/String;

    .line 1642757
    iput-object p2, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->b:Ljava/lang/String;

    .line 1642758
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1642762
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1642759
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1642760
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1642761
    return-void
.end method
