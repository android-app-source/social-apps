.class public final Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x680e7f50
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1638437
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1638436
    const-class v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1638434
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1638435
    return-void
.end method

.method private j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBoostedComponent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1638432
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1638433
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1638438
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1638439
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1638440
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1638441
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1638442
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1638443
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1638444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1638445
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1638430
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->f:Ljava/util/List;

    .line 1638431
    iget-object v0, p0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1638417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1638418
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1638419
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1638420
    invoke-direct {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1638421
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;

    .line 1638422
    iput-object v0, v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 1638423
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1638424
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1638425
    if-eqz v2, :cond_1

    .line 1638426
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;

    .line 1638427
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1638428
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1638429
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1638414
    new-instance v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;-><init>()V

    .line 1638415
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1638416
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1638412
    const v0, -0x6f6dd629

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1638413
    const v0, 0x57914c76

    return v0
.end method
