.class public final Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1642505
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1642506
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1642507
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1642508
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1642509
    if-nez p1, :cond_0

    .line 1642510
    :goto_0
    return v0

    .line 1642511
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1642512
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1642513
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1642514
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1642515
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1642516
    const v3, -0x44f349f5

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1642517
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1642518
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1642519
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1642520
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1642521
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1642522
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1642523
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1642524
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1642525
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x44f349f5 -> :sswitch_1
        -0x3668373f -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1642526
    new-instance v0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1642533
    sparse-switch p2, :sswitch_data_0

    .line 1642534
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1642535
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1642536
    const v1, -0x44f349f5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1642537
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x44f349f5 -> :sswitch_1
        -0x3668373f -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1642527
    if-eqz p1, :cond_0

    .line 1642528
    invoke-static {p0, p1, p2}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;

    move-result-object v1

    .line 1642529
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;

    .line 1642530
    if-eq v0, v1, :cond_0

    .line 1642531
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1642532
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1642538
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1642503
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1642504
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1642472
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1642473
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1642474
    :cond_0
    iput-object p1, p0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a:LX/15i;

    .line 1642475
    iput p2, p0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->b:I

    .line 1642476
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1642502
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1642501
    new-instance v0, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1642498
    iget v0, p0, LX/1vt;->c:I

    .line 1642499
    move v0, v0

    .line 1642500
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1642495
    iget v0, p0, LX/1vt;->c:I

    .line 1642496
    move v0, v0

    .line 1642497
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1642492
    iget v0, p0, LX/1vt;->b:I

    .line 1642493
    move v0, v0

    .line 1642494
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1642489
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1642490
    move-object v0, v0

    .line 1642491
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1642480
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1642481
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1642482
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1642483
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1642484
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1642485
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1642486
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1642487
    invoke-static {v3, v9, v2}, Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1642488
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1642477
    iget v0, p0, LX/1vt;->c:I

    .line 1642478
    move v0, v0

    .line 1642479
    return v0
.end method
