.class public final Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3f5f2347
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645047
    const-class v0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645048
    const-class v0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1645049
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1645050
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1645051
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1645052
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1645053
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1645054
    iput-boolean p1, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->e:Z

    .line 1645055
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1645056
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1645057
    if-eqz v0, :cond_0

    .line 1645058
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1645059
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645060
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->f:Ljava/lang/String;

    .line 1645061
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1645062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645063
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1645064
    invoke-virtual {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1645065
    invoke-virtual {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1645066
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1645067
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 1645068
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1645069
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1645070
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1645071
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645072
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1645073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645075
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1645045
    new-instance v0, LX/ADg;

    invoke-direct {v0, p1}, LX/ADg;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645046
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1645042
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1645043
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->e:Z

    .line 1645044
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1645036
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645037
    invoke-virtual {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1645038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1645039
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1645040
    :goto_0
    return-void

    .line 1645041
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1645033
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645034
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->a(Z)V

    .line 1645035
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1645030
    new-instance v0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;-><init>()V

    .line 1645031
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1645032
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1645022
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1645023
    iget-boolean v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->e:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645028
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->g:Ljava/lang/String;

    .line 1645029
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645026
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->h:Ljava/lang/String;

    .line 1645027
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1645025
    const v0, -0x8e25132

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1645024
    const v0, 0x2df91497

    return v0
.end method
