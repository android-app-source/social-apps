.class public final Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x174a6094
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645299
    const-class v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645298
    const-class v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1645296
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1645297
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645294
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->e:Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->e:Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    .line 1645295
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->e:Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1645288
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645289
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->a()Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1645290
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1645291
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1645292
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1645280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645281
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->a()Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1645282
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->a()Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    .line 1645283
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->a()Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1645284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;

    .line 1645285
    iput-object v0, v1, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;->e:Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    .line 1645286
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645287
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1645277
    new-instance v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel;-><init>()V

    .line 1645278
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1645279
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1645276
    const v0, 0x77867436

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1645275
    const v0, 0x407dd604

    return v0
.end method
