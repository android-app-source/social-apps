.class public final Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3f5f2347
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645246
    const-class v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645247
    const-class v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1645248
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1645249
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1645250
    iput-boolean p1, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->e:Z

    .line 1645251
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1645252
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1645253
    if-eqz v0, :cond_0

    .line 1645254
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1645255
    :cond_0
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1645256
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1645257
    iget-boolean v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->e:Z

    return v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645258
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->f:Ljava/lang/String;

    .line 1645259
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645260
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->g:Ljava/lang/String;

    .line 1645261
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645233
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->h:Ljava/lang/String;

    .line 1645234
    iget-object v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1645235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645236
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1645237
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1645238
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1645239
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1645240
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 1645241
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1645242
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1645243
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1645244
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645245
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1645230
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645231
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645232
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1645229
    new-instance v0, LX/ADk;

    invoke-direct {v0, p1}, LX/ADk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645228
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1645225
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1645226
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->e:Z

    .line 1645227
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1645219
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645220
    invoke-direct {p0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1645221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1645222
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1645223
    :goto_0
    return-void

    .line 1645224
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1645216
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645217
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;->a(Z)V

    .line 1645218
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1645213
    new-instance v0, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/leadgen/LeadGenShareUserInfoWithSignedRequestMutationModels$LeadGenUserInfoCoreMutationFieldsModel$LeadGenUserStatusModel;-><init>()V

    .line 1645214
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1645215
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1645211
    const v0, 0x6a52bc51

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1645212
    const v0, 0x372290f1

    return v0
.end method
