.class public final Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1291540c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645521
    const-class v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645555
    const-class v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1645553
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1645554
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1645546
    iput-object p1, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1645547
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1645548
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1645549
    if-eqz v0, :cond_0

    .line 1645550
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1645551
    :cond_0
    return-void

    .line 1645552
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645544
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1645545
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1645532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645533
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1645534
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1645535
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1645536
    invoke-direct {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1645537
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1645538
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1645539
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1645540
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1645541
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1645542
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645543
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1645524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645525
    invoke-direct {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1645526
    invoke-direct {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1645527
    invoke-direct {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1645528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;

    .line 1645529
    iput-object v0, v1, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1645530
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645531
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1645523
    new-instance v0, LX/ADs;

    invoke-direct {v0, p1}, LX/ADs;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645522
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1645556
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645557
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1645558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1645559
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1645560
    :goto_0
    return-void

    .line 1645561
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1645506
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645507
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1645508
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645509
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1645510
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1645511
    new-instance v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;-><init>()V

    .line 1645512
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1645513
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645514
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->f:Ljava/lang/String;

    .line 1645515
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645516
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->g:Ljava/lang/String;

    .line 1645517
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1645518
    const v0, 0x20e93ea8

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645519
    invoke-direct {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1645520
    const v0, 0x285feb

    return v0
.end method
