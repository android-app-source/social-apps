.class public final Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x124cdba7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645596
    const-class v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1645597
    const-class v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1645606
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1645607
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1645598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645599
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1645600
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x6bded9b4

    invoke-static {v2, v1, v3}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1645601
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1645602
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1645603
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1645604
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645605
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1645594
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->e:Ljava/util/List;

    .line 1645595
    iget-object v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1645569
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1645570
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1645571
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1645572
    if-eqz v1, :cond_2

    .line 1645573
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;

    .line 1645574
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1645575
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1645576
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6bded9b4

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1645577
    invoke-virtual {p0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1645578
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;

    .line 1645579
    iput v3, v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->f:I

    move-object v1, v0

    .line 1645580
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1645581
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 1645582
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 1645583
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1645591
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1645592
    const/4 v0, 0x1

    const v1, 0x6bded9b4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->f:I

    .line 1645593
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1645588
    new-instance v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;-><init>()V

    .line 1645589
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1645590
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1645587
    const v0, 0x7ba110dd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1645586
    const v0, 0x10b6b624

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1645584
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1645585
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
