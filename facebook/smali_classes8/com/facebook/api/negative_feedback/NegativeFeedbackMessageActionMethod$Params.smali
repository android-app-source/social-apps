.class public final Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1422865
    new-instance v0, LX/8wg;

    invoke-direct {v0}, LX/8wg;-><init>()V

    sput-object v0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8wh;)V
    .locals 1

    .prologue
    .line 1422871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422872
    iget-object v0, p1, LX/8wh;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->a:Ljava/lang/String;

    .line 1422873
    iget-object v0, p1, LX/8wh;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->b:Ljava/lang/String;

    .line 1422874
    iget-object v0, p1, LX/8wh;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->c:Ljava/lang/String;

    .line 1422875
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1422876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422877
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->a:Ljava/lang/String;

    .line 1422878
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->b:Ljava/lang/String;

    .line 1422879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->c:Ljava/lang/String;

    .line 1422880
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1422870
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1422866
    iget-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1422867
    iget-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1422868
    iget-object v0, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1422869
    return-void
.end method
