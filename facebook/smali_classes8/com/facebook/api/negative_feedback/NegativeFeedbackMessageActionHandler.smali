.class public Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8wi;


# direct methods
.method public constructor <init>(LX/0Or;LX/8wi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/8wi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1422853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422854
    iput-object p1, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;->a:LX/0Or;

    .line 1422855
    iput-object p2, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;->b:LX/8wi;

    .line 1422856
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1422857
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1422858
    const-string v1, "negativeFeedbackMessageActionParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;

    .line 1422859
    iget-object v1, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;->b:LX/8wi;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1422860
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1422861
    return-object v0
.end method
