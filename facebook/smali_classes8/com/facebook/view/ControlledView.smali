.class public Lcom/facebook/view/ControlledView;
.super Landroid/view/View;
.source ""


# instance fields
.field public final a:Lcom/facebook/view/ViewController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 1413614
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413615
    sget-object v0, LX/03r;->ControlledView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1413616
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1413617
    const-string v2, "You must provide a controller class name in the \'controller\' attribute of a ControlledView"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1413618
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1413619
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1413620
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/view/ControlledView;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 1413621
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/view/ViewController;

    iput-object v0, p0, Lcom/facebook/view/ControlledView;->a:Lcom/facebook/view/ViewController;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1413622
    return-void

    .line 1413623
    :catch_0
    move-exception v0

    .line 1413624
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not find controller class"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1413625
    :catch_1
    move-exception v0

    .line 1413626
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not find controller constructor"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1413627
    :catch_2
    move-exception v0

    .line 1413628
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not instantiate controller"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1413629
    :catch_3
    move-exception v0

    .line 1413630
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not instantiate controller"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1413631
    :catch_4
    move-exception v0

    .line 1413632
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not instantiate controller"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public getController()Lcom/facebook/view/ViewController;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/facebook/view/ViewController;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 1413633
    iget-object v0, p0, Lcom/facebook/view/ControlledView;->a:Lcom/facebook/view/ViewController;

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1413634
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 1413635
    iget-object v0, p0, Lcom/facebook/view/ControlledView;->a:Lcom/facebook/view/ViewController;

    invoke-virtual {v0}, Lcom/facebook/view/ViewController;->a()V

    .line 1413636
    return-void
.end method
