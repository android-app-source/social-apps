.class public final Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6ef6802c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468204
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468203
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1468201
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1468202
    return-void
.end method

.method private a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1468148
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    .line 1468149
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1468195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468196
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1468197
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1468198
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1468199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1468187
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468188
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1468189
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    .line 1468190
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1468191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    .line 1468192
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    .line 1468193
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468194
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1468186
    new-instance v0, LX/9KT;

    invoke-direct {v0, p1}, LX/9KT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1468172
    const-string v0, "group_owner_authored_stories.available_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1468173
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    .line 1468174
    if-eqz v0, :cond_1

    .line 1468175
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1468176
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1468177
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1468178
    :goto_0
    return-void

    .line 1468179
    :cond_0
    const-string v0, "group_owner_authored_stories.total_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1468180
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    .line 1468181
    if-eqz v0, :cond_1

    .line 1468182
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1468183
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1468184
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1468185
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1468155
    const-string v0, "group_owner_authored_stories.available_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1468156
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    .line 1468157
    if-eqz v0, :cond_0

    .line 1468158
    if-eqz p3, :cond_1

    .line 1468159
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    .line 1468160
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->a(I)V

    .line 1468161
    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    .line 1468162
    :cond_0
    :goto_0
    return-void

    .line 1468163
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->a(I)V

    goto :goto_0

    .line 1468164
    :cond_2
    const-string v0, "group_owner_authored_stories.total_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1468165
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    .line 1468166
    if-eqz v0, :cond_0

    .line 1468167
    if-eqz p3, :cond_3

    .line 1468168
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    .line 1468169
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->b(I)V

    .line 1468170
    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    goto :goto_0

    .line 1468171
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->b(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1468152
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;-><init>()V

    .line 1468153
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1468154
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1468151
    const v0, -0x66126ed0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1468150
    const v0, 0x41e065f

    return v0
.end method
