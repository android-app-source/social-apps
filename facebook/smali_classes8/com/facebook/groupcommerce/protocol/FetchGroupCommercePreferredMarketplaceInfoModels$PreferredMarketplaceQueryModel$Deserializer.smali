.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466112
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1466113
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466114
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1466056
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1466057
    const/4 v2, 0x0

    .line 1466058
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1466059
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466060
    :goto_0
    move v1, v2

    .line 1466061
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1466062
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1466063
    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;-><init>()V

    .line 1466064
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1466065
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1466066
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1466067
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1466068
    :cond_0
    return-object v1

    .line 1466069
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466070
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1466071
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1466072
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466073
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1466074
    const-string v4, "preferred_marketplaces"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1466075
    const/4 v3, 0x0

    .line 1466076
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1466077
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466078
    :goto_2
    move v1, v3

    .line 1466079
    goto :goto_1

    .line 1466080
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1466081
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1466082
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1466083
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466084
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1466085
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1466086
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466087
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1466088
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1466089
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1466090
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1466091
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1466092
    const/4 v5, 0x0

    .line 1466093
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1466094
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466095
    :goto_5
    move v4, v5

    .line 1466096
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1466097
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1466098
    goto :goto_3

    .line 1466099
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1466100
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1466101
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 1466102
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466103
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 1466104
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1466105
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466106
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 1466107
    const-string p0, "node"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1466108
    invoke-static {p1, v0}, LX/9Jq;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 1466109
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1466110
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1466111
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6
.end method
