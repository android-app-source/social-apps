.class public final Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1470661
    const-class v0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1470662
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1470681
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1470664
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1470665
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1470666
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1470667
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1470668
    if-eqz p0, :cond_0

    .line 1470669
    const-string p2, "object"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470670
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1470671
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1470672
    if-eqz p0, :cond_1

    .line 1470673
    const-string p2, "price"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470674
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1470675
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1470676
    if-eqz p0, :cond_2

    .line 1470677
    const-string p2, "sale_intercept_eligible"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470678
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1470679
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1470680
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1470663
    check-cast p1, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
