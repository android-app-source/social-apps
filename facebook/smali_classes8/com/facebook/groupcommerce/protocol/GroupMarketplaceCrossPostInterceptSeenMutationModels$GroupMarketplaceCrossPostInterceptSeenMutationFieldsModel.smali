.class public final Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x33bcb37f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468757
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468756
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1468754
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1468755
    return-void
.end method

.method private a()Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1468752
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    .line 1468753
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1468746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468747
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1468748
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1468749
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1468750
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468751
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1468733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468734
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1468735
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    .line 1468736
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1468737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;

    .line 1468738
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel$GroupModel;

    .line 1468739
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468740
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1468743
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/GroupMarketplaceCrossPostInterceptSeenMutationModels$GroupMarketplaceCrossPostInterceptSeenMutationFieldsModel;-><init>()V

    .line 1468744
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1468745
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1468742
    const v0, -0x4857c4fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1468741
    const v0, 0xb61b912

    return v0
.end method
