.class public final Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466636
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1466637
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466638
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1466639
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1466640
    const/4 v2, 0x0

    .line 1466641
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1466642
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466643
    :goto_0
    move v1, v2

    .line 1466644
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1466645
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1466646
    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;-><init>()V

    .line 1466647
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1466648
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1466649
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1466650
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1466651
    :cond_0
    return-object v1

    .line 1466652
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466653
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1466654
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1466655
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466656
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1466657
    const-string v4, "group_sell_config"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1466658
    const/4 v3, 0x0

    .line 1466659
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1466660
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466661
    :goto_2
    move v1, v3

    .line 1466662
    goto :goto_1

    .line 1466663
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1466664
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1466665
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1466666
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466667
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1466668
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1466669
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466670
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1466671
    const-string p0, "edges"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1466672
    invoke-static {p1, v0}, LX/9K2;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 1466673
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1466674
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1466675
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3
.end method
