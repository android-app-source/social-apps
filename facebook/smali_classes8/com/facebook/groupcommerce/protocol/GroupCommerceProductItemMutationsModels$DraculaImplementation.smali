.class public final Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1468009
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1468010
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1467993
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1467994
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1467995
    if-nez p1, :cond_0

    .line 1467996
    :goto_0
    return v0

    .line 1467997
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1467998
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1467999
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1468000
    const v2, 0x2febb431

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1468001
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1468002
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1468003
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1468004
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1468005
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1468006
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1468007
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1468008
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1323ecc6 -> :sswitch_0
        0x2febb431 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1468011
    if-nez p0, :cond_0

    move v0, v1

    .line 1468012
    :goto_0
    return v0

    .line 1468013
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1468014
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1468015
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1468016
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1468017
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1468018
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1468019
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1468032
    const/4 v7, 0x0

    .line 1468033
    const/4 v1, 0x0

    .line 1468034
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1468035
    invoke-static {v2, v3, v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1468036
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1468037
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1468038
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1468020
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1468021
    sparse-switch p2, :sswitch_data_0

    .line 1468022
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1468023
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1468024
    const v1, 0x2febb431

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1468025
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1323ecc6 -> :sswitch_0
        0x2febb431 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1468026
    if-eqz p1, :cond_0

    .line 1468027
    invoke-static {p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    move-result-object v1

    .line 1468028
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    .line 1468029
    if-eq v0, v1, :cond_0

    .line 1468030
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1468031
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1467990
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1467991
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1467992
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1467985
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1467986
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1467987
    :cond_0
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 1467988
    iput p2, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->b:I

    .line 1467989
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1467984
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1467983
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1467980
    iget v0, p0, LX/1vt;->c:I

    .line 1467981
    move v0, v0

    .line 1467982
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1467977
    iget v0, p0, LX/1vt;->c:I

    .line 1467978
    move v0, v0

    .line 1467979
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1467974
    iget v0, p0, LX/1vt;->b:I

    .line 1467975
    move v0, v0

    .line 1467976
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1467959
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1467960
    move-object v0, v0

    .line 1467961
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1467965
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1467966
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1467967
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1467968
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1467969
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1467970
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1467971
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1467972
    invoke-static {v3, v9, v2}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1467973
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1467962
    iget v0, p0, LX/1vt;->c:I

    .line 1467963
    move v0, v0

    .line 1467964
    return v0
.end method
