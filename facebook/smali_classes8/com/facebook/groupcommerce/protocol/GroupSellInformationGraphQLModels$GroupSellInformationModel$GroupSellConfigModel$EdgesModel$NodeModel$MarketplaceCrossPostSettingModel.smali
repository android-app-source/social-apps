.class public final Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6e467e52
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1469661
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1469662
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1469663
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1469664
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1469665
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1469666
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1469667
    return-void
.end method

.method public static a(Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;
    .locals 2

    .prologue
    .line 1469668
    if-nez p0, :cond_0

    .line 1469669
    const/4 p0, 0x0

    .line 1469670
    :goto_0
    return-object p0

    .line 1469671
    :cond_0
    instance-of v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    if-eqz v0, :cond_1

    .line 1469672
    check-cast p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    goto :goto_0

    .line 1469673
    :cond_1
    new-instance v0, LX/9L4;

    invoke-direct {v0}, LX/9L4;-><init>()V

    .line 1469674
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->a:Ljava/lang/String;

    .line 1469675
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->b:Ljava/lang/String;

    .line 1469676
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->c:Ljava/lang/String;

    .line 1469677
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/9L4;->d:Z

    .line 1469678
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/9L4;->e:Z

    .line 1469679
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->hy_()Z

    move-result v1

    iput-boolean v1, v0, LX/9L4;->f:Z

    .line 1469680
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->hz_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->g:Ljava/lang/String;

    .line 1469681
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->j()Z

    move-result v1

    iput-boolean v1, v0, LX/9L4;->h:Z

    .line 1469682
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/9L4;->i:Z

    .line 1469683
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->j:Ljava/lang/String;

    .line 1469684
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->k:Ljava/lang/String;

    .line 1469685
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->l:Ljava/lang/String;

    .line 1469686
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->m:Ljava/lang/String;

    .line 1469687
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->n:Ljava/lang/String;

    .line 1469688
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->o:Ljava/lang/String;

    .line 1469689
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->p:Ljava/lang/String;

    .line 1469690
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9L4;->q:Ljava/lang/String;

    .line 1469691
    invoke-virtual {v0}, LX/9L4;->a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 1469692
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1469693
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1469694
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1469695
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1469696
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->hz_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1469697
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1469698
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1469699
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1469700
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1469701
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->p()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1469702
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->q()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1469703
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->r()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1469704
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->s()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1469705
    const/16 v12, 0x11

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 1469706
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 1469707
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1469708
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1469709
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1469710
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1469711
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1469712
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1469713
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1469714
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1469715
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1469716
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1469717
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1469718
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1469719
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1469720
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1469721
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1469722
    const/16 v0, 0x10

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1469723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1469724
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1469727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1469728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1469729
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469725
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->e:Ljava/lang/String;

    .line 1469726
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1469734
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1469735
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->h:Z

    .line 1469736
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->i:Z

    .line 1469737
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->j:Z

    .line 1469738
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->l:Z

    .line 1469739
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->m:Z

    .line 1469740
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1469741
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;-><init>()V

    .line 1469742
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1469743
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469732
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->f:Ljava/lang/String;

    .line 1469733
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469730
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->g:Ljava/lang/String;

    .line 1469731
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1469658
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1469659
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1469660
    const v0, -0x44e96d0f

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1469656
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1469657
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1469655
    const v0, -0x5e59ab76

    return v0
.end method

.method public final hy_()Z
    .locals 2

    .prologue
    .line 1469653
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1469654
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->j:Z

    return v0
.end method

.method public final hz_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469651
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->k:Ljava/lang/String;

    .line 1469652
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1469649
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1469650
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->l:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1469647
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1469648
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469645
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->n:Ljava/lang/String;

    .line 1469646
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469643
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->o:Ljava/lang/String;

    .line 1469644
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469641
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->p:Ljava/lang/String;

    .line 1469642
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469639
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->q:Ljava/lang/String;

    .line 1469640
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469637
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->r:Ljava/lang/String;

    .line 1469638
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469635
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->s:Ljava/lang/String;

    .line 1469636
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469633
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->t:Ljava/lang/String;

    .line 1469634
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1469631
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->u:Ljava/lang/String;

    .line 1469632
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->u:Ljava/lang/String;

    return-object v0
.end method
