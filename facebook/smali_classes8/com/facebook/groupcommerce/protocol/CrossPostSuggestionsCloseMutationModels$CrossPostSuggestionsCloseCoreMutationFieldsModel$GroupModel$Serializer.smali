.class public final Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1465609
    const-class v0, Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1465610
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1465611
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1465612
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1465613
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1}, LX/9Je;->a(LX/15i;ILX/0nX;)V

    .line 1465614
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1465615
    check-cast p1, Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/CrossPostSuggestionsCloseMutationModels$CrossPostSuggestionsCloseCoreMutationFieldsModel$GroupModel;LX/0nX;LX/0my;)V

    return-void
.end method
