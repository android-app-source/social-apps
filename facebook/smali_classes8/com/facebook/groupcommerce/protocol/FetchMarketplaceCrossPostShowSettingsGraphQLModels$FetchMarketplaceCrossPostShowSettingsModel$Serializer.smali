.class public final Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466678
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1466679
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466680
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1466681
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1466682
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1466683
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466684
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1466685
    if-eqz v2, :cond_7

    .line 1466686
    const-string v3, "group_sell_config"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466687
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466688
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1466689
    if-eqz v3, :cond_6

    .line 1466690
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466691
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1466692
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_5

    .line 1466693
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1466694
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466695
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1466696
    if-eqz v0, :cond_4

    .line 1466697
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466698
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466699
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1466700
    if-eqz v2, :cond_3

    .line 1466701
    const-string p0, "marketplace_cross_post_setting"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466702
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466703
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1466704
    if-eqz p0, :cond_0

    .line 1466705
    const-string v0, "is_enabled"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466706
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1466707
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1466708
    if-eqz p0, :cond_1

    .line 1466709
    const-string v0, "should_show_intercept"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466710
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1466711
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1466712
    if-eqz p0, :cond_2

    .line 1466713
    const-string v0, "should_show_nux"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466714
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1466715
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466716
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466717
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466718
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1466719
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1466720
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466721
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466722
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1466723
    check-cast p1, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;LX/0nX;LX/0my;)V

    return-void
.end method
