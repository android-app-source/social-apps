.class public final Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1466877
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1466878
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1466907
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1466908
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1466879
    if-nez p1, :cond_0

    .line 1466880
    :goto_0
    return v0

    .line 1466881
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1466882
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1466883
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1466884
    const v2, -0x1257f5f

    const/4 v5, 0x0

    .line 1466885
    if-nez v1, :cond_1

    move v4, v5

    .line 1466886
    :goto_1
    move v1, v4

    .line 1466887
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1466888
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466889
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466890
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1466891
    const v2, 0xe595943

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1466892
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1466893
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466894
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466895
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1466896
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1466897
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1466898
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466899
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 1466900
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1466901
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1466902
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1466903
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 1466904
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1466905
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1466906
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1257f5f -> :sswitch_1
        0xe595943 -> :sswitch_2
        0x514ecec8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1466828
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1466864
    sparse-switch p2, :sswitch_data_0

    .line 1466865
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1466866
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1466867
    const v1, -0x1257f5f

    .line 1466868
    if-eqz v0, :cond_0

    .line 1466869
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1466870
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1466871
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1466872
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1466873
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1466874
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1466875
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1466876
    const v1, 0xe595943

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x1257f5f -> :sswitch_2
        0xe595943 -> :sswitch_1
        0x514ecec8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1466858
    if-eqz p1, :cond_0

    .line 1466859
    invoke-static {p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1466860
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;

    .line 1466861
    if-eq v0, v1, :cond_0

    .line 1466862
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1466863
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1466857
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1466855
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1466856
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1466909
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1466910
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1466911
    :cond_0
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1466912
    iput p2, p0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->b:I

    .line 1466913
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1466854
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1466853
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1466850
    iget v0, p0, LX/1vt;->c:I

    .line 1466851
    move v0, v0

    .line 1466852
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1466847
    iget v0, p0, LX/1vt;->c:I

    .line 1466848
    move v0, v0

    .line 1466849
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1466844
    iget v0, p0, LX/1vt;->b:I

    .line 1466845
    move v0, v0

    .line 1466846
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466841
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1466842
    move-object v0, v0

    .line 1466843
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1466832
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1466833
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1466834
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1466835
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1466836
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1466837
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1466838
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1466839
    invoke-static {v3, v9, v2}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1466840
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1466829
    iget v0, p0, LX/1vt;->c:I

    .line 1466830
    move v0, v0

    .line 1466831
    return v0
.end method
