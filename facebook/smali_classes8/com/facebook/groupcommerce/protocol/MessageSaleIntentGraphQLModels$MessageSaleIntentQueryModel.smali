.class public final Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x87660b4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1470708
    const-class v0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1470709
    const-class v0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1470706
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1470707
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1470697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1470698
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1470699
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1470700
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1470701
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1470702
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1470703
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1470704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1470705
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1470694
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1470695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1470696
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1470710
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->e:Ljava/lang/String;

    .line 1470711
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1470691
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1470692
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->g:Z

    .line 1470693
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1470688
    new-instance v0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;-><init>()V

    .line 1470689
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1470690
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1470687
    const v0, -0x17ccb3d9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1470686
    const v0, 0x4aaeedde    # 5732079.0f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1470682
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->f:Ljava/lang/String;

    .line 1470683
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1470684
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1470685
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;->g:Z

    return v0
.end method
