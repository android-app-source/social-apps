.class public final Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471010
    const-class v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471009
    const-class v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1471007
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1471008
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1471004
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1471005
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1471006
    return-void
.end method

.method public static a(Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;)Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;
    .locals 9

    .prologue
    .line 1470945
    if-nez p0, :cond_0

    .line 1470946
    const/4 p0, 0x0

    .line 1470947
    :goto_0
    return-object p0

    .line 1470948
    :cond_0
    instance-of v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    if-eqz v0, :cond_1

    .line 1470949
    check-cast p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    goto :goto_0

    .line 1470950
    :cond_1
    new-instance v0, LX/9LS;

    invoke-direct {v0}, LX/9LS;-><init>()V

    .line 1470951
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9LS;->a:Ljava/lang/String;

    .line 1470952
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9LS;->b:Ljava/lang/String;

    .line 1470953
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1470954
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1470955
    iget-object v3, v0, LX/9LS;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1470956
    iget-object v5, v0, LX/9LS;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1470957
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1470958
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1470959
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1470960
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1470961
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1470962
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1470963
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1470964
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1470965
    new-instance v3, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    invoke-direct {v3, v2}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;-><init>(LX/15i;)V

    .line 1470966
    move-object p0, v3

    .line 1470967
    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1470998
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->f:Ljava/lang/String;

    .line 1470999
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1471000
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1471001
    if-eqz v0, :cond_0

    .line 1471002
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1471003
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1470990
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1470991
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1470992
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1470993
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1470994
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1470995
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1470996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1470997
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1470987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1470988
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1470989
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1471011
    new-instance v0, LX/9LT;

    invoke-direct {v0, p1}, LX/9LT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1470986
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1470980
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1470981
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1470982
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1470983
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1470984
    :goto_0
    return-void

    .line 1470985
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1470977
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1470978
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->a(Ljava/lang/String;)V

    .line 1470979
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1470974
    new-instance v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;-><init>()V

    .line 1470975
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1470976
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1470972
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->e:Ljava/lang/String;

    .line 1470973
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1470970
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->f:Ljava/lang/String;

    .line 1470971
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1470969
    const v0, -0x5f8c1c08

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1470968
    const v0, 0x41e065f

    return v0
.end method
