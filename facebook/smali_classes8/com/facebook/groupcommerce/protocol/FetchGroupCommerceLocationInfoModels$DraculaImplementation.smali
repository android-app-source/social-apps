.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1465748
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1465749
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1465764
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1465765
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1465754
    if-nez p1, :cond_0

    .line 1465755
    :goto_0
    return v1

    .line 1465756
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1465757
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1465758
    :pswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1465759
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1465760
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 1465761
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1465762
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1465763
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x73c10995
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1465753
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1465750
    packed-switch p0, :pswitch_data_0

    .line 1465751
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1465752
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x73c10995
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1465715
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1465746
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;->b(I)V

    .line 1465747
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1465741
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1465742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1465743
    :cond_0
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;->a:LX/15i;

    .line 1465744
    iput p2, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;->b:I

    .line 1465745
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1465766
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1465740
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1465737
    iget v0, p0, LX/1vt;->c:I

    .line 1465738
    move v0, v0

    .line 1465739
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1465734
    iget v0, p0, LX/1vt;->c:I

    .line 1465735
    move v0, v0

    .line 1465736
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1465731
    iget v0, p0, LX/1vt;->b:I

    .line 1465732
    move v0, v0

    .line 1465733
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1465728
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1465729
    move-object v0, v0

    .line 1465730
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1465719
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1465720
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1465721
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1465722
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1465723
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1465724
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1465725
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1465726
    invoke-static {v3, v9, v2}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1465727
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1465716
    iget v0, p0, LX/1vt;->c:I

    .line 1465717
    move v0, v0

    .line 1465718
    return v0
.end method
