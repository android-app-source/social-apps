.class public final Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x11781041
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1467813
    const-class v0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1467812
    const-class v0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1467810
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1467811
    return-void
.end method

.method private a()Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1467808
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    .line 1467809
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1467814
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1467815
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1467816
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1467817
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1467818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1467819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1467800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1467801
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1467802
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    .line 1467803
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1467804
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;

    .line 1467805
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel$StoryModel;

    .line 1467806
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1467807
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1467795
    new-instance v0, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/ForSalePostCreateXPostsMutationModels$ForSalePostCreateXPostsCoreMutationFieldsModel;-><init>()V

    .line 1467796
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1467797
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1467799
    const v0, -0x730b5ae6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1467798
    const v0, -0x5cc0fcda

    return v0
.end method
