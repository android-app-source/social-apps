.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466160
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1466161
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466162
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1466163
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1466164
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1466165
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466166
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1466167
    if-eqz v2, :cond_3

    .line 1466168
    const-string v3, "preferred_marketplaces"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466169
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466170
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1466171
    if-eqz v3, :cond_2

    .line 1466172
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466173
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1466174
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_1

    .line 1466175
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1466176
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466177
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1466178
    if-eqz v0, :cond_0

    .line 1466179
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466180
    invoke-static {v1, v0, p1}, LX/9Jq;->a(LX/15i;ILX/0nX;)V

    .line 1466181
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466182
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1466183
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1466184
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466185
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1466186
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1466187
    check-cast p1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
