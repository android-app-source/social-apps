.class public final Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x26272ccb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468132
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468131
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1468129
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1468130
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1468127
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1468128
    iget v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1468121
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468122
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1468123
    iget v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 1468124
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1468125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468126
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1468118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468120
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1468133
    iput p1, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->e:I

    .line 1468134
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1468135
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1468136
    if-eqz v0, :cond_0

    .line 1468137
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1468138
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1468114
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1468115
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->e:I

    .line 1468116
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->f:I

    .line 1468117
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1468111
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;-><init>()V

    .line 1468112
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1468113
    return-object v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 1468101
    iput p1, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->f:I

    .line 1468102
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1468103
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1468104
    if-eqz v0, :cond_0

    .line 1468105
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1468106
    :cond_0
    return-void
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1468110
    const v0, 0x15743ac8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1468109
    const v0, -0x7eb557ed

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1468107
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1468108
    iget v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel$GroupOwnerAuthoredStoriesModel;->f:I

    return v0
.end method
