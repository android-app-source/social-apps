.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47d4d6a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1466413
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1466412
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1466410
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1466411
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466408
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 1466409
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466406
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->f:Ljava/lang/String;

    .line 1466407
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1466396
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1466397
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->k()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1466398
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1466399
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x4879458d

    invoke-static {v3, v2, v4}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1466400
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1466401
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1466402
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1466403
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1466404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1466405
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1466386
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1466387
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1466388
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4879458d

    invoke-static {v2, v0, v3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1466389
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1466390
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    .line 1466391
    iput v3, v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->g:I

    .line 1466392
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1466393
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1466394
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1466395
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1466385
    new-instance v0, LX/9Ju;

    invoke-direct {v0, p1}, LX/9Ju;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466414
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1466382
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1466383
    const/4 v0, 0x2

    const v1, 0x4879458d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->g:I

    .line 1466384
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1466380
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1466381
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1466379
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1466376
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;-><init>()V

    .line 1466377
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1466378
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1466375
    const v0, 0x4b1508d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1466374
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466372
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1466373
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
