.class public final Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1467317
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1467318
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1467319
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1467304
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1467305
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1467306
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1467307
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1467308
    if-eqz v2, :cond_0

    .line 1467309
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467310
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1467311
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1467312
    if-eqz v2, :cond_1

    .line 1467313
    const-string p0, "groups"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467314
    invoke-static {v1, v2, p1, p2}, LX/9KF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1467315
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1467316
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1467303
    check-cast p1, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
