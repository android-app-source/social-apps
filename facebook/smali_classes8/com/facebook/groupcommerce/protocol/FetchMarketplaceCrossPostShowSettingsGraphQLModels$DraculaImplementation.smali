.class public final Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1466627
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1466628
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1466629
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1466630
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1466590
    if-nez p1, :cond_0

    .line 1466591
    :goto_0
    return v0

    .line 1466592
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1466593
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1466594
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1466595
    const v2, -0x245fec8f

    const/4 v4, 0x0

    .line 1466596
    if-nez v1, :cond_1

    move v3, v4

    .line 1466597
    :goto_1
    move v1, v3

    .line 1466598
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1466599
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466600
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466601
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1466602
    const v2, -0x70505d83

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1466603
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1466604
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466605
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466606
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1466607
    const v2, -0x5ec49b33

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1466608
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1466609
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466610
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466611
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1466612
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 1466613
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 1466614
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1466615
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1466616
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 1466617
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 1466618
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466619
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v6

    .line 1466620
    if-nez v6, :cond_2

    const/4 v3, 0x0

    .line 1466621
    :goto_2
    if-ge v4, v6, :cond_3

    .line 1466622
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result p1

    .line 1466623
    invoke-static {p0, p1, v2, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p1

    aput p1, v3, v4

    .line 1466624
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1466625
    :cond_2
    new-array v3, v6, [I

    goto :goto_2

    .line 1466626
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x70505d83 -> :sswitch_2
        -0x60fd50a0 -> :sswitch_0
        -0x5ec49b33 -> :sswitch_3
        -0x245fec8f -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1466589
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1466574
    sparse-switch p2, :sswitch_data_0

    .line 1466575
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1466576
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1466577
    const v1, -0x245fec8f

    .line 1466578
    if-eqz v0, :cond_0

    .line 1466579
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1466580
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1466581
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1466582
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1466583
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1466584
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1466585
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1466586
    const v1, -0x70505d83

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 1466587
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1466588
    const v1, -0x5ec49b33

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x70505d83 -> :sswitch_3
        -0x60fd50a0 -> :sswitch_0
        -0x5ec49b33 -> :sswitch_1
        -0x245fec8f -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1466568
    if-eqz p1, :cond_0

    .line 1466569
    invoke-static {p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1466570
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;

    .line 1466571
    if-eq v0, v1, :cond_0

    .line 1466572
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1466573
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1466567
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1466565
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1466566
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1466631
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1466632
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1466633
    :cond_0
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1466634
    iput p2, p0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->b:I

    .line 1466635
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1466564
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1466563
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1466560
    iget v0, p0, LX/1vt;->c:I

    .line 1466561
    move v0, v0

    .line 1466562
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1466557
    iget v0, p0, LX/1vt;->c:I

    .line 1466558
    move v0, v0

    .line 1466559
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1466554
    iget v0, p0, LX/1vt;->b:I

    .line 1466555
    move v0, v0

    .line 1466556
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466551
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1466552
    move-object v0, v0

    .line 1466553
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1466539
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1466540
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1466541
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1466542
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1466543
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1466544
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1466545
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1466546
    invoke-static {v3, v9, v2}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1466547
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1466548
    iget v0, p0, LX/1vt;->c:I

    .line 1466549
    move v0, v0

    .line 1466550
    return v0
.end method
