.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4882c9b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1466452
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1466429
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1466450
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1466451
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1466444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1466445
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1466446
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1466447
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1466448
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1466449
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1466436
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1466437
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1466438
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    .line 1466439
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1466440
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;

    .line 1466441
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    .line 1466442
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1466443
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupCommerceSuggestedLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466434
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    .line 1466435
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1466431
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;-><init>()V

    .line 1466432
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1466433
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1466430
    const v0, -0x194dae62

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1466428
    const v0, -0x6747e1ce

    return v0
.end method
