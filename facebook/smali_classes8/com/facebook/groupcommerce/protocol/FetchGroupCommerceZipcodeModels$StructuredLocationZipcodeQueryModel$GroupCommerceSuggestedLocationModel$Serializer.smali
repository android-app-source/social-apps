.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466365
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1466366
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466371
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1466368
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1466369
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/9Jw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1466370
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1466367
    check-cast p1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;LX/0nX;LX/0my;)V

    return-void
.end method
