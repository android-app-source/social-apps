.class public final Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x18ae3fd5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471186
    const-class v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471185
    const-class v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1471183
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1471184
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1471180
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1471181
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1471182
    return-void
.end method

.method public static a(Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;)Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;
    .locals 8

    .prologue
    .line 1471160
    if-nez p0, :cond_0

    .line 1471161
    const/4 p0, 0x0

    .line 1471162
    :goto_0
    return-object p0

    .line 1471163
    :cond_0
    instance-of v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;

    if-eqz v0, :cond_1

    .line 1471164
    check-cast p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;

    goto :goto_0

    .line 1471165
    :cond_1
    new-instance v0, LX/9LW;

    invoke-direct {v0}, LX/9LW;-><init>()V

    .line 1471166
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->a()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;->a(Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;)Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/9LW;->a:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    .line 1471167
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1471168
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1471169
    iget-object v3, v0, LX/9LW;->a:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1471170
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1471171
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1471172
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1471173
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1471174
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1471175
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1471176
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1471177
    new-instance v3, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;

    invoke-direct {v3, v2}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;-><init>(LX/15i;)V

    .line 1471178
    move-object p0, v3

    .line 1471179
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1471154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1471155
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1471156
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1471157
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1471158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1471159
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1471187
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1471188
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1471189
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    .line 1471190
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1471191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;

    .line 1471192
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->e:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    .line 1471193
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1471194
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471153
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1471150
    new-instance v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;-><init>()V

    .line 1471151
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1471152
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1471149
    const v0, -0x157e4744

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1471148
    const v0, 0x261131e8

    return v0
.end method

.method public final j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471146
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->e:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->e:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    .line 1471147
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->e:Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    return-object v0
.end method
