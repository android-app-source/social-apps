.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1466159
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1466158
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1466156
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1466157
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1466148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1466149
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1466150
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1466151
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1466152
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1466153
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1466154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1466155
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1466145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1466146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1466147
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466144
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1466135
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;-><init>()V

    .line 1466136
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1466137
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1466143
    const v0, 0x1bb6fb8b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1466142
    const v0, 0x7559322b    # 2.753286E32f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466140
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1466141
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466138
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1466139
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method
