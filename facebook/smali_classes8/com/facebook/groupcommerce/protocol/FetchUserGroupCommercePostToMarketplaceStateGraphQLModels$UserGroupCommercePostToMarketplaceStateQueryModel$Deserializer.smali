.class public final Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466914
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1466915
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466916
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1466917
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1466918
    const/4 v2, 0x0

    .line 1466919
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1466920
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466921
    :goto_0
    move v1, v2

    .line 1466922
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1466923
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1466924
    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;-><init>()V

    .line 1466925
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1466926
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1466927
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1466928
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1466929
    :cond_0
    return-object v1

    .line 1466930
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466931
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1466932
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1466933
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466934
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1466935
    const-string v4, "group_sell_config"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1466936
    const/4 v3, 0x0

    .line 1466937
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1466938
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466939
    :goto_2
    move v1, v3

    .line 1466940
    goto :goto_1

    .line 1466941
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1466942
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1466943
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1466944
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466945
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1466946
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1466947
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466948
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1466949
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1466950
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1466951
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1466952
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1466953
    const/4 v5, 0x0

    .line 1466954
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1466955
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466956
    :goto_5
    move v4, v5

    .line 1466957
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1466958
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1466959
    goto :goto_3

    .line 1466960
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1466961
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1466962
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 1466963
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466964
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 1466965
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1466966
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466967
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 1466968
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1466969
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1466970
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_12

    .line 1466971
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1466972
    :goto_7
    move v4, v6

    .line 1466973
    goto :goto_6

    .line 1466974
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1466975
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1466976
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6

    .line 1466977
    :cond_e
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_10

    .line 1466978
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1466979
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1466980
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v9, :cond_e

    .line 1466981
    const-string p0, "user_group_commerce_post_to_marketplace_state"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1466982
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v7

    goto :goto_8

    .line 1466983
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_8

    .line 1466984
    :cond_10
    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1466985
    if-eqz v4, :cond_11

    .line 1466986
    invoke-virtual {v0, v6, v8}, LX/186;->a(IZ)V

    .line 1466987
    :cond_11
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_7

    :cond_12
    move v4, v6

    move v8, v6

    goto :goto_8
.end method
