.class public final Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x349fd2ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468394
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468395
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1468374
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1468375
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1468408
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->e:Ljava/util/List;

    .line 1468409
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1468396
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->e:Ljava/util/List;

    .line 1468397
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1468398
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1468399
    if-eqz v0, :cond_0

    .line 1468400
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 1468401
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1468402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468403
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1468404
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1468405
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1468406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468407
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1468385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468386
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1468387
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1468388
    if-eqz v1, :cond_0

    .line 1468389
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    .line 1468390
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->e:Ljava/util/List;

    .line 1468391
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468392
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1468393
    new-instance v0, LX/9KW;

    invoke-direct {v0, p1}, LX/9KW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1468383
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1468384
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1468380
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1468381
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;->a(Ljava/util/List;)V

    .line 1468382
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1468379
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1468376
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;-><init>()V

    .line 1468377
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1468378
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1468373
    const v0, 0x717a7182

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1468372
    const v0, 0x4c808d5

    return v0
.end method
