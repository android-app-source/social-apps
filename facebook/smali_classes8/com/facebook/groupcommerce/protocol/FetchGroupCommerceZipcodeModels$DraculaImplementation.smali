.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1466293
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1466294
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1466295
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1466296
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1466297
    if-nez p1, :cond_0

    .line 1466298
    :goto_0
    return v0

    .line 1466299
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1466300
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1466301
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1466302
    const v2, 0x1bcaae0e

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1466303
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1466304
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466305
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1466306
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1466307
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1466308
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1466309
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1466310
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1bcaae0e -> :sswitch_1
        0x4879458d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1466323
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1466311
    sparse-switch p2, :sswitch_data_0

    .line 1466312
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1466313
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1466314
    const v1, 0x1bcaae0e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1466315
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1bcaae0e -> :sswitch_1
        0x4879458d -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1466316
    if-eqz p1, :cond_0

    .line 1466317
    invoke-static {p0, p1, p2}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    move-result-object v1

    .line 1466318
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    .line 1466319
    if-eq v0, v1, :cond_0

    .line 1466320
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1466321
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1466322
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1466286
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1466287
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1466288
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1466289
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1466290
    :cond_0
    iput-object p1, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a:LX/15i;

    .line 1466291
    iput p2, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->b:I

    .line 1466292
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1466285
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1466284
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1466281
    iget v0, p0, LX/1vt;->c:I

    .line 1466282
    move v0, v0

    .line 1466283
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1466278
    iget v0, p0, LX/1vt;->c:I

    .line 1466279
    move v0, v0

    .line 1466280
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1466275
    iget v0, p0, LX/1vt;->b:I

    .line 1466276
    move v0, v0

    .line 1466277
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1466272
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1466273
    move-object v0, v0

    .line 1466274
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1466263
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1466264
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1466265
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1466266
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1466267
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1466268
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1466269
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1466270
    invoke-static {v3, v9, v2}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1466271
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1466260
    iget v0, p0, LX/1vt;->c:I

    .line 1466261
    move v0, v0

    .line 1466262
    return v0
.end method
