.class public final Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1470628
    const-class v0, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1470629
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1470630
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1470631
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1470632
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1470633
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 1470634
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1470635
    :goto_0
    move v1, v2

    .line 1470636
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1470637
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1470638
    new-instance v1, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/MessageSaleIntentGraphQLModels$MessageSaleIntentQueryModel;-><init>()V

    .line 1470639
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1470640
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1470641
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1470642
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1470643
    :cond_0
    return-object v1

    .line 1470644
    :cond_1
    const-string p0, "sale_intercept_eligible"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1470645
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    .line 1470646
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 1470647
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1470648
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1470649
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 1470650
    const-string p0, "object"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1470651
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1470652
    :cond_3
    const-string p0, "price"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1470653
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1470654
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1470655
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1470656
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1470657
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1470658
    if-eqz v1, :cond_6

    .line 1470659
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1470660
    :cond_6
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_1
.end method
