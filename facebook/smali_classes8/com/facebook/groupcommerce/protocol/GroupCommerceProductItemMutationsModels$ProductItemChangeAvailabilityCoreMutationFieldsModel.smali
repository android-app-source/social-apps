.class public final Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4e71f84d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468443
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1468442
    const-class v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1468440
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1468441
    return-void
.end method

.method private a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1468438
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    .line 1468439
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    return-object v0
.end method

.method private j()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1468436
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->f:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->f:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    .line 1468437
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->f:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1468428
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468429
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1468430
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->j()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1468431
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1468432
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1468433
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1468434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468435
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1468415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1468416
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1468417
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    .line 1468418
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->a()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1468419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;

    .line 1468420
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->e:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$GroupModel;

    .line 1468421
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->j()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1468422
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->j()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    .line 1468423
    invoke-direct {p0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->j()Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1468424
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;

    .line 1468425
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;->f:Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel;

    .line 1468426
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1468427
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1468410
    new-instance v0, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel;-><init>()V

    .line 1468411
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1468412
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1468414
    const v0, -0x5e59ed16

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1468413
    const v0, 0x26b7500

    return v0
.end method
