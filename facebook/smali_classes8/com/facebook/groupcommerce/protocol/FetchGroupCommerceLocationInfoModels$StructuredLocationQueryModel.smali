.class public final Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4e6afa18
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1465895
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1465894
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1465892
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1465893
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1465871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1465872
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1465873
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1465874
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1465875
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1465876
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1465884
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1465885
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1465886
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    .line 1465887
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1465888
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;

    .line 1465889
    iput-object v0, v1, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    .line 1465890
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1465891
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupCommerceSuggestedLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1465882
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    iput-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    .line 1465883
    iget-object v0, p0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->e:Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1465879
    new-instance v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;-><init>()V

    .line 1465880
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1465881
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1465878
    const v0, -0x2500677a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1465877
    const v0, -0x6747e1ce

    return v0
.end method
