.class public final Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1466990
    const-class v0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;

    new-instance v1, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1466991
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466992
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1466993
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1466994
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1466995
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1466996
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1466997
    if-eqz v2, :cond_4

    .line 1466998
    const-string v3, "group_sell_config"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1466999
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1467000
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1467001
    if-eqz v3, :cond_3

    .line 1467002
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467003
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1467004
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_2

    .line 1467005
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1467006
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1467007
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1467008
    if-eqz v0, :cond_1

    .line 1467009
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467010
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1467011
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1467012
    if-eqz v2, :cond_0

    .line 1467013
    const-string p0, "user_group_commerce_post_to_marketplace_state"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467014
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1467015
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1467016
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1467017
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1467018
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1467019
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1467020
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1467021
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1467022
    check-cast p1, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel$Serializer;->a(Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
