.class public Lcom/facebook/user/tiles/UserTileDrawableController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/user/tiles/UserTileDrawableController;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:I


# instance fields
.field public A:Ljava/lang/String;

.field public B:LX/8t6;

.field public C:LX/8t8;

.field public D:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/3Rb;

.field public final f:LX/1HI;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Yb;

.field private final j:LX/0Zb;

.field public final k:LX/3el;

.field public final l:LX/0Uh;

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:Landroid/graphics/drawable/ShapeDrawable;

.field private o:LX/8ug;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/8ui;

.field private q:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I

.field private s:Z

.field public t:LX/8t9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Z

.field private v:LX/1bf;

.field private w:LX/1bf;

.field public x:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public z:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xee

    .line 1412242
    const-class v0, Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412243
    sput-object v0, Lcom/facebook/user/tiles/UserTileDrawableController;->a:Ljava/lang/Class;

    const-string v1, "profile_user_tile_view"

    const-string v2, "user_tile"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/user/tiles/UserTileDrawableController;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1412244
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/facebook/user/tiles/UserTileDrawableController;->c:I

    return-void
.end method

.method public constructor <init>(LX/0Zb;Landroid/content/res/Resources;LX/3Rb;LX/1HI;Ljava/util/concurrent/Executor;LX/0Or;LX/0Xl;LX/3el;LX/0Uh;)V
    .locals 4
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/widget/tiles/annotations/IsProfilePictureDiskCacheEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Landroid/content/res/Resources;",
            "LX/3Rb;",
            "LX/1HI;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Xl;",
            "LX/3el;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1412246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->u:Z

    .line 1412247
    iput-object p2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->d:Landroid/content/res/Resources;

    .line 1412248
    iput-object p3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->e:LX/3Rb;

    .line 1412249
    iput-object p4, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->f:LX/1HI;

    .line 1412250
    iput-object p5, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->g:Ljava/util/concurrent/Executor;

    .line 1412251
    iput-object p6, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->h:LX/0Or;

    .line 1412252
    iput-object p1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->j:LX/0Zb;

    .line 1412253
    iput-object p8, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->k:LX/3el;

    .line 1412254
    iput-object p9, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->l:LX/0Uh;

    .line 1412255
    invoke-interface {p7}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.users.ACTION_USERS_UPDATED"

    new-instance v2, LX/8t5;

    invoke-direct {v2, p0}, LX/8t5;-><init>(Lcom/facebook/user/tiles/UserTileDrawableController;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->i:LX/0Yb;

    .line 1412256
    return-void
.end method

.method private a(LX/1bf;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1412257
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v1, :cond_0

    .line 1412258
    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->e:LX/3Rb;

    iget-object v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    invoke-virtual {v2, v3}, LX/3Rb;->a(LX/8t9;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1412259
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->v:LX/1bf;

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1412260
    :goto_1
    return-void

    .line 1412261
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1412262
    :cond_2
    if-nez p1, :cond_3

    .line 1412263
    invoke-direct {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->g()V

    goto :goto_1

    .line 1412264
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->s:Z

    if-nez v0, :cond_4

    .line 1412265
    invoke-direct {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->g()V

    .line 1412266
    :cond_4
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->C:LX/8t8;

    sget-object v1, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412267
    iget-object v1, v0, LX/8t9;->f:Ljava/lang/String;

    move-object v0, v1

    .line 1412268
    :goto_2
    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->A:Ljava/lang/String;

    .line 1412269
    iput-object p1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->w:LX/1bf;

    iput-object p1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->v:LX/1bf;

    .line 1412270
    invoke-static {p1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    .line 1412271
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->A:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1412272
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412273
    iget-object v3, v2, LX/8t9;->g:Lcom/facebook/user/model/Name;

    move-object v2, v3

    .line 1412274
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8ui;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1412275
    invoke-direct {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->i()V

    .line 1412276
    :cond_5
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1412277
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1412278
    sget-object v1, LX/1bb;->SMALL:LX/1bb;

    .line 1412279
    iput-object v1, v0, LX/1bX;->f:LX/1bb;

    .line 1412280
    :cond_6
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->l:LX/0Uh;

    const/16 v2, 0x16b

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1412281
    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    if-lez v1, :cond_8

    .line 1412282
    new-instance v1, LX/1o9;

    iget v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    iget v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1412283
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1412284
    :goto_3
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->l:LX/0Uh;

    const/16 v2, 0x555

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1412285
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1412286
    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->f:LX/1HI;

    .line 1412287
    iget-object v3, v2, LX/1HI;->e:LX/1Fh;

    move-object v2, v3

    .line 1412288
    iget-object v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->f:LX/1HI;

    .line 1412289
    iget-object v4, v3, LX/1HI;->i:LX/1Ao;

    move-object v3, v4

    .line 1412290
    invoke-virtual {v3, v1, v5}, LX/1Ao;->a(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v1

    .line 1412291
    invoke-static {v1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1412292
    iput-object v5, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    .line 1412293
    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {v2}, LX/8ui;->a()V

    .line 1412294
    invoke-static {p0, v1, p1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1FJ;LX/1bf;)V

    .line 1412295
    :goto_4
    goto/16 :goto_1

    .line 1412296
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1412297
    :cond_8
    new-instance v1, LX/1o9;

    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/ShapeDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1412298
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1412299
    goto :goto_3

    .line 1412300
    :cond_9
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->k:LX/3el;

    invoke-virtual {v1, p1}, LX/3el;->a(LX/1bf;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 1412301
    new-instance v2, Lcom/facebook/user/tiles/UserTileDrawableController$1;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/facebook/user/tiles/UserTileDrawableController$1;-><init>(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1bX;LX/1bf;Z)V

    const v3, 0x753779eb

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->z:Ljava/util/concurrent/Future;

    goto :goto_4

    .line 1412302
    :cond_a
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->f:LX/1HI;

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 1412303
    sget-object v3, Lcom/facebook/user/tiles/UserTileDrawableController;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1412304
    invoke-virtual {v1, v2, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    .line 1412305
    new-instance v1, LX/8t4;

    invoke-direct {v1, p0, p1, p2}, LX/8t4;-><init>(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1bf;Z)V

    .line 1412306
    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    iget-object v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->g:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v1, v3}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_4
.end method

.method private a(Landroid/content/Context;ZIILX/8ug;LX/8ui;F)V
    .locals 3
    .param p5    # LX/8ug;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/8ui;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1412307
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    .line 1412308
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 1412309
    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1412310
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1412311
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1412312
    const/4 v0, 0x0

    cmpl-float v0, p7, v0

    if-lez v0, :cond_2

    .line 1412313
    const/4 v2, 0x0

    .line 1412314
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p7, v0, v1

    const/4 v1, 0x1

    aput p7, v0, v1

    const/4 v1, 0x2

    aput p7, v0, v1

    const/4 v1, 0x3

    aput p7, v0, v1

    const/4 v1, 0x4

    aput p7, v0, v1

    const/4 v1, 0x5

    aput p7, v0, v1

    const/4 v1, 0x6

    aput p7, v0, v1

    const/4 v1, 0x7

    aput p7, v0, v1

    .line 1412315
    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v1, v0, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-static {p0, v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Lcom/facebook/user/tiles/UserTileDrawableController;Landroid/graphics/drawable/shapes/Shape;)V

    .line 1412316
    :goto_0
    if-nez p6, :cond_3

    .line 1412317
    new-instance v0, LX/8ui;

    invoke-direct {v0}, LX/8ui;-><init>()V

    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    .line 1412318
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    const v1, 0x7f0a003d

    invoke-virtual {v0, p1, v1}, LX/8ui;->a(Landroid/content/Context;I)V

    .line 1412319
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0052

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/res/Resources;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/8ui;->a(F)V

    .line 1412320
    :goto_1
    iput-object p5, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->o:LX/8ug;

    .line 1412321
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1412322
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1412323
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 1412324
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1412325
    :cond_0
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1412326
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->o:LX/8ug;

    if-eqz v1, :cond_1

    .line 1412327
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->o:LX/8ug;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1412328
    :cond_1
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    .line 1412329
    if-lez p3, :cond_4

    :goto_2
    invoke-virtual {p0, p3}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(I)V

    .line 1412330
    return-void

    .line 1412331
    :cond_2
    invoke-virtual {p0, p2}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Z)V

    goto :goto_0

    .line 1412332
    :cond_3
    iput-object p6, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    goto :goto_1

    .line 1412333
    :cond_4
    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result p3

    goto :goto_2
.end method

.method public static a(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1FJ;LX/1bf;)V
    .locals 5

    .prologue
    .line 1412360
    iput-object p1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->y:LX/1FJ;

    .line 1412361
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    .line 1412362
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, LX/7U7;

    const/4 v3, 0x0

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/7U7;-><init>(LX/7U6;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1412363
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1412364
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    .line 1412365
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412366
    iget-object v2, v1, LX/8t9;->e:LX/8ue;

    move-object v1, v2

    .line 1412367
    invoke-static {v1, p2}, LX/3Rd;->a(LX/8ue;LX/1bf;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1412368
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->e:LX/3Rb;

    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    invoke-virtual {v1, v2}, LX/3Rb;->a(LX/8t9;)I

    move-result v1

    iput v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    .line 1412369
    :cond_0
    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    if-eqz v1, :cond_2

    .line 1412370
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    iget v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/ShapeDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1412371
    :goto_0
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    if-eqz v1, :cond_1

    .line 1412372
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    invoke-interface {v1}, LX/8t6;->a()V

    .line 1412373
    :cond_1
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->j:LX/0Zb;

    .line 1412374
    sget-object v2, Lcom/facebook/user/tiles/UserTileDrawableController;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 1412375
    iget-object v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/widget/tiles/InstrumentTile;->a(LX/0Zb;Lcom/facebook/common/callercontext/CallerContext;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)V

    .line 1412376
    return-void

    .line 1412377
    :cond_2
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/user/tiles/UserTileDrawableController;Landroid/graphics/drawable/shapes/Shape;)V
    .locals 1

    .prologue
    .line 1412334
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1412335
    return-void
.end method

.method public static a$redex0(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1ca;LX/1bf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1bf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1412336
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1412337
    :goto_0
    return-void

    .line 1412338
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    .line 1412339
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {v0}, LX/8ui;->a()V

    .line 1412340
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1412341
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->w:LX/1bf;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->w:LX/1bf;

    invoke-virtual {p2, v1}, LX/1bf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1412342
    invoke-static {p0, v0, p2}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1FJ;LX/1bf;)V

    goto :goto_0

    .line 1412343
    :cond_1
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/user/tiles/UserTileDrawableController;
    .locals 10

    .prologue
    .line 1412344
    new-instance v0, Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v3

    check-cast v3, LX/3Rb;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    const/16 v6, 0x15a0

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {p0}, LX/3el;->a(LX/0QB;)LX/3el;

    move-result-object v8

    check-cast v8, LX/3el;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/user/tiles/UserTileDrawableController;-><init>(LX/0Zb;Landroid/content/res/Resources;LX/3Rb;LX/1HI;Ljava/util/concurrent/Executor;LX/0Or;LX/0Xl;LX/3el;LX/0Uh;)V

    .line 1412345
    return-object v0
.end method

.method public static b(Lcom/facebook/user/tiles/UserTileDrawableController;Z)V
    .locals 2

    .prologue
    .line 1412346
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getShaderFactory()Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1412347
    :cond_0
    :goto_0
    return-void

    .line 1412348
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412349
    iget-object v1, v0, LX/8t9;->b:LX/8t8;

    move-object v0, v1

    .line 1412350
    sget-object v1, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    if-ne v0, v1, :cond_0

    .line 1412351
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412352
    iget-object p1, v1, LX/8t9;->g:Lcom/facebook/user/model/Name;

    move-object v1, p1

    .line 1412353
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8ui;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1412354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->v:LX/1bf;

    .line 1412355
    invoke-direct {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->i()V

    .line 1412356
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    if-eqz v0, :cond_0

    .line 1412357
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    invoke-interface {v0}, LX/8t6;->a()V

    goto :goto_0

    .line 1412358
    :cond_2
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    invoke-static {v0, v1}, LX/3Rb;->a(LX/8t9;I)LX/1bf;

    move-result-object v0

    .line 1412359
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(LX/1bf;Z)V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/user/tiles/UserTileDrawableController;)V
    .locals 7

    .prologue
    .line 1412203
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->C:LX/8t8;

    .line 1412204
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412205
    iget-object v2, v0, LX/8t9;->b:LX/8t8;

    move-object v0, v2

    .line 1412206
    :goto_0
    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->C:LX/8t8;

    .line 1412207
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->o:LX/8ug;

    if-eqz v0, :cond_0

    .line 1412208
    iget-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->o:LX/8ug;

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412209
    iget-object v3, v0, LX/8t9;->e:LX/8ue;

    move-object v0, v3

    .line 1412210
    :goto_1
    invoke-virtual {v2, v0}, LX/8ug;->a(LX/8ue;)V

    .line 1412211
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->C:LX/8t8;

    sget-object v2, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    if-ne v0, v2, :cond_4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1412212
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412213
    iget-object v4, v0, LX/8t9;->g:Lcom/facebook/user/model/Name;

    move-object v0, v4

    .line 1412214
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    .line 1412215
    iget-object v4, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    .line 1412216
    iget-object v5, v4, LX/8ui;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1412217
    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {v4, v0}, LX/8ui;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v3

    .line 1412218
    :goto_2
    iget v4, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    iget-object v5, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->e:LX/3Rb;

    iget-object v6, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    invoke-virtual {v5, v6}, LX/3Rb;->a(LX/8t9;)I

    move-result v5

    if-ne v4, v5, :cond_6

    move v4, v3

    .line 1412219
    :goto_3
    iget-object v5, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412220
    iget-object v6, v5, LX/8t9;->f:Ljava/lang/String;

    move-object v5, v6

    .line 1412221
    iget-object v6, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->C:LX/8t8;

    if-ne v1, v6, :cond_7

    iget-object v6, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->A:Ljava/lang/String;

    invoke-static {v6, v5}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    :goto_4
    move v0, v3

    .line 1412222
    if-eqz v0, :cond_4

    .line 1412223
    :goto_5
    return-void

    .line 1412224
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1412225
    :cond_3
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    goto :goto_1

    .line 1412226
    :cond_4
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->e:LX/3Rb;

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    iget v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    iget v3, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    invoke-virtual {v0, v1, v2, v3}, LX/3Rb;->a(LX/8t9;II)LX/1bf;

    move-result-object v0

    .line 1412227
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(LX/1bf;Z)V

    goto :goto_5

    :cond_5
    move v0, v2

    .line 1412228
    goto :goto_2

    :cond_6
    move v4, v2

    .line 1412229
    goto :goto_3

    :cond_7
    move v3, v2

    .line 1412230
    goto :goto_4
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1412231
    iput-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->v:LX/1bf;

    .line 1412232
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    if-eqz v0, :cond_0

    .line 1412233
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 1412234
    iput-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->x:LX/1ca;

    .line 1412235
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->y:LX/1FJ;

    if-eqz v0, :cond_1

    .line 1412236
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->y:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1412237
    iput-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->y:LX/1FJ;

    .line 1412238
    :cond_1
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->z:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_2

    .line 1412239
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->z:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1412240
    iput-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->z:Ljava/util/concurrent/Future;

    .line 1412241
    :cond_2
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1412196
    invoke-direct {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->f()V

    .line 1412197
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1412198
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1412199
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1412200
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {v0}, LX/8ui;->a()V

    .line 1412201
    iput-object v2, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->A:Ljava/lang/String;

    .line 1412202
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1412189
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412190
    iget-object v1, v0, LX/8t9;->e:LX/8ue;

    move-object v0, v1

    .line 1412191
    sget-object v1, LX/8ue;->SMS:LX/8ue;

    if-ne v0, v1, :cond_0

    .line 1412192
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a0048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/8ui;->b(I)V

    .line 1412193
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->e:LX/3Rb;

    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    invoke-virtual {v0, v1}, LX/3Rb;->a(LX/8t9;)I

    move-result v0

    iput v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    .line 1412194
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->D:I

    invoke-virtual {v0, v1}, LX/8ui;->c(I)V

    .line 1412195
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1412181
    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1412182
    iget v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    if-eq v0, p1, :cond_0

    .line 1412183
    iput p1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    .line 1412184
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    .line 1412185
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->r:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    .line 1412186
    invoke-static {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->e(Lcom/facebook/user/tiles/UserTileDrawableController;)V

    .line 1412187
    :cond_0
    return-void

    .line 1412188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1412177
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    sub-int v1, p1, p5

    sub-int v2, p2, p6

    invoke-virtual {v0, p3, p4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1412178
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1412179
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3, v3, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1412180
    :cond_0
    return-void
.end method

.method public final a(LX/8t9;)V
    .locals 0

    .prologue
    .line 1412174
    iput-object p1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412175
    invoke-static {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->e(Lcom/facebook/user/tiles/UserTileDrawableController;)V

    .line 1412176
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1412160
    sget-object v0, LX/03r;->UserTileView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1412161
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    .line 1412162
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1412163
    sget-object v0, LX/03r;->UserTileDrawable:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1412164
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 1412165
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 1412166
    const/16 v1, 0x4

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v7

    .line 1412167
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->s:Z

    .line 1412168
    const/16 v1, 0x0

    sget v4, Lcom/facebook/user/tiles/UserTileDrawableController;->c:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 1412169
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1412170
    new-instance v5, LX/8ug;

    invoke-direct {v5, p1, p2, p3}, LX/8ug;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412171
    new-instance v6, LX/8ui;

    invoke-direct {v6, p1, p2, p3}, LX/8ui;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v0, p0

    move-object v1, p1

    .line 1412172
    invoke-direct/range {v0 .. v7}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Landroid/content/Context;ZIILX/8ug;LX/8ui;F)V

    .line 1412173
    return-void
.end method

.method public final a(Landroid/content/Context;ZILX/8ug;)V
    .locals 8
    .param p4    # LX/8ug;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1412145
    sget v4, Lcom/facebook/user/tiles/UserTileDrawableController;->c:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Landroid/content/Context;ZIILX/8ug;LX/8ui;F)V

    .line 1412146
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1412157
    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Lcom/facebook/user/tiles/UserTileDrawableController;Landroid/graphics/drawable/shapes/Shape;)V

    .line 1412158
    return-void

    .line 1412159
    :cond_0
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1412152
    iget-boolean v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->u:Z

    if-nez v0, :cond_0

    .line 1412153
    :goto_0
    return-void

    .line 1412154
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->u:Z

    .line 1412155
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1412156
    invoke-static {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->e(Lcom/facebook/user/tiles/UserTileDrawableController;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1412147
    iget-boolean v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->u:Z

    if-eqz v0, :cond_0

    .line 1412148
    :goto_0
    return-void

    .line 1412149
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->u:Z

    .line 1412150
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileDrawableController;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1412151
    invoke-direct {p0}, Lcom/facebook/user/tiles/UserTileDrawableController;->f()V

    goto :goto_0
.end method
