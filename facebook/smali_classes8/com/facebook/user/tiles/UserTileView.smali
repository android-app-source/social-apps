.class public Lcom/facebook/user/tiles/UserTileView;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Lcom/facebook/user/tiles/UserTileDrawableController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1412094
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1412095
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/user/tiles/UserTileView;->a(Landroid/util/AttributeSet;I)V

    .line 1412096
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1412091
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1412092
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/user/tiles/UserTileView;->a(Landroid/util/AttributeSet;I)V

    .line 1412093
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1412088
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412089
    invoke-direct {p0, p2, p3}, Lcom/facebook/user/tiles/UserTileView;->a(Landroid/util/AttributeSet;I)V

    .line 1412090
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1412082
    const-class v0, Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0, p0}, Lcom/facebook/user/tiles/UserTileView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1412083
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412084
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412085
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1412086
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1412087
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/user/tiles/UserTileView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->b(LX/0QB;)Lcom/facebook/user/tiles/UserTileDrawableController;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileDrawableController;

    iput-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 7

    .prologue
    .line 1412077
    iget-boolean v0, p0, Lcom/facebook/user/tiles/UserTileView;->b:Z

    if-eqz v0, :cond_0

    .line 1412078
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getPaddingRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getPaddingBottom()I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(IIIIII)V

    .line 1412079
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/user/tiles/UserTileView;->b:Z

    .line 1412080
    :cond_0
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1412081
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1412072
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 1412073
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412074
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1412075
    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1412076
    return-void
.end method

.method public getUserTileDrawable()Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    .line 1412064
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412065
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, v1

    .line 1412066
    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1412067
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1412068
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1412069
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1412070
    invoke-virtual {p0, v3, v0}, Lcom/facebook/user/tiles/UserTileView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 1412071
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/facebook/user/tiles/UserTileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 1412035
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 1412036
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412037
    iget-object p0, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, p0

    .line 1412038
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 1412039
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x470b0ddd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1412061
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1412062
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->c()V

    .line 1412063
    const/16 v1, 0x2d

    const v2, -0x7a02ef7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x39b941ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1412058
    iget-object v1, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->d()V

    .line 1412059
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1412060
    const/16 v1, 0x2d

    const v2, -0x7cef78fa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1412053
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1412054
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412055
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1412056
    invoke-virtual {p0, p1, v0}, Lcom/facebook/user/tiles/UserTileView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 1412057
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1412050
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 1412051
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/user/tiles/UserTileView;->b:Z

    .line 1412052
    return-void
.end method

.method public setOnUserTileUpdatedListener(LX/8t6;)V
    .locals 1

    .prologue
    .line 1412047
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412048
    iput-object p1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    .line 1412049
    return-void
.end method

.method public setParams(LX/8t9;)V
    .locals 1

    .prologue
    .line 1412045
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v0, p1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(LX/8t9;)V

    .line 1412046
    return-void
.end method

.method public setTileSizePx(I)V
    .locals 1

    .prologue
    .line 1412043
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v0, p1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(I)V

    .line 1412044
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    .prologue
    .line 1412040
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412041
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1412042
    if-eq p1, v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
