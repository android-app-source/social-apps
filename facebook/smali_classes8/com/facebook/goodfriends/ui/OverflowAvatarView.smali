.class public Lcom/facebook/goodfriends/ui/OverflowAvatarView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1464357
    const-class v0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1464358
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464359
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1464360
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464361
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1464362
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464363
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->a(Landroid/content/Context;)V

    .line 1464364
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1464365
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1464366
    const v1, 0x7f0307c6

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1464367
    const v0, 0x7f0d149d

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1464368
    const v0, 0x7f0d149e

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1464369
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/goodfriends/data/FriendData;I)V
    .locals 3

    .prologue
    .line 1464370
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1464371
    const/4 v0, 0x1

    if-le p2, v0, :cond_1

    .line 1464372
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->c:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, "+"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1464373
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f02199f

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 1464374
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1464375
    :goto_1
    return-void

    .line 1464376
    :cond_0
    iget-object v0, p1, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 1464377
    goto :goto_0

    .line 1464378
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 1464379
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1464380
    return-void
.end method
