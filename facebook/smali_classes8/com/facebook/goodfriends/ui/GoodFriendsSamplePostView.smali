.class public Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Lcom/facebook/widget/FbImageView;

.field private c:Lcom/facebook/widget/FbImageView;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1464304
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464305
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1464306
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464307
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1464308
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464309
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->a(Landroid/content/Context;)V

    .line 1464310
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1464311
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1464312
    const v1, 0x7f0307cb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1464313
    const v0, 0x7f0d14a5

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1464314
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0214f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1464315
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1464316
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1464317
    const v0, 0x7f0d14a3

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->b:Lcom/facebook/widget/FbImageView;

    .line 1464318
    const v0, 0x7f0d14a6

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->c:Lcom/facebook/widget/FbImageView;

    .line 1464319
    const v0, 0x7f0d14a4

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->d:Landroid/view/View;

    .line 1464320
    return-void
.end method

.method public static c(Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;)V
    .locals 4

    .prologue
    const v1, 0x3fa66666    # 1.3f

    .line 1464321
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v1}, Landroid/view/animation/BounceInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/9J6;

    invoke-direct {v1, p0}, LX/9J6;-><init>(Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1464322
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x190

    const-wide/16 v2, 0xc8

    const v1, 0x3e99999a    # 0.3f

    .line 1464323
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 1464324
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->c:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 1464325
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->b:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/9J5;

    invoke-direct {v1, p0}, LX/9J5;-><init>(Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1464326
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1464327
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1464328
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->c:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1464329
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsSamplePostView;->b:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1464330
    return-void
.end method
