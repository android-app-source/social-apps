.class public Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/goodfriends/ui/OverflowAvatarView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1464249
    const-class v0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1464250
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464251
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1464252
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464253
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1464254
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1464255
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1464256
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->a(Landroid/content/Context;)V

    .line 1464257
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1464258
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1464259
    const v1, 0x7f0307c5

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1464260
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    const v0, 0x7f0d149c

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v0, v1, v2

    .line 1464261
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v0, 0x7f0d149a

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v0, v1, v3

    .line 1464262
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x2

    const v0, 0x7f0d149b

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v0, v1, v2

    .line 1464263
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x3

    const v0, 0x7f0d1498

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v0, v1, v2

    .line 1464264
    const v0, 0x7f0d1499

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/ui/OverflowAvatarView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->c:Lcom/facebook/goodfriends/ui/OverflowAvatarView;

    .line 1464265
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1464266
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1464267
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aget-object v3, v0, v1

    .line 1464268
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1464269
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/data/FriendData;

    .line 1464270
    iget-object v4, v0, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    move-object v0, v4

    .line 1464271
    sget-object v4, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1464272
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1464273
    :cond_0
    sget-object v0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 1464274
    :cond_1
    if-lez p2, :cond_2

    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    array-length v0, v0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/data/FriendData;

    .line 1464275
    :goto_2
    iget-object v1, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->c:Lcom/facebook/goodfriends/ui/OverflowAvatarView;

    invoke-virtual {v1, v0, p2}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->a(Lcom/facebook/goodfriends/data/FriendData;I)V

    .line 1464276
    return-void

    :cond_2
    move-object v0, v2

    .line 1464277
    goto :goto_2
.end method

.method public setAvatarData(Lcom/facebook/goodfriends/data/FriendData;)V
    .locals 3

    .prologue
    .line 1464278
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 1464279
    iget-object v1, p1, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    move-object v1, v1

    .line 1464280
    sget-object v2, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1464281
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const v1, 0x7f020a57

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1464282
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const v1, 0x7f020a59

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1464283
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->b:[Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    const v1, 0x7f020a5b

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1464284
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->c:Lcom/facebook/goodfriends/ui/OverflowAvatarView;

    const v1, 0x7f020a5d

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/ui/OverflowAvatarView;->setImageResource(I)V

    .line 1464285
    return-void
.end method
