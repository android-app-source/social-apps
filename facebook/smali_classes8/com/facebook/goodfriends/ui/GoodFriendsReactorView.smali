.class public Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1464286
    const-class v0, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1464287
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1464288
    const/4 v2, 0x1

    .line 1464289
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1464290
    const v1, 0x7f0307aa

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1464291
    const v0, 0x7f0d146b

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1464292
    const v0, 0x7f0d146c

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1464293
    new-instance v1, LX/4Ab;

    invoke-direct {v1}, LX/4Ab;-><init>()V

    .line 1464294
    iput-boolean v2, v1, LX/4Ab;->b:Z

    .line 1464295
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/4Ab;)V

    .line 1464296
    iget-object v0, p0, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/4Ab;)V

    .line 1464297
    return-void
.end method
