.class public Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;
.super Landroid/view/View;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/1aX;

.field private final d:LX/1Uo;

.field private final e:I

.field private final f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Z

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1452334
    const-class v0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

    const-string v1, "feedback_reactions"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1452337
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1452338
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1452335
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1452336
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1452311
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1452312
    iput v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->k:I

    .line 1452313
    iput v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    .line 1452314
    const-class v0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1452315
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1452316
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    .line 1452317
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    .line 1452318
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1452319
    move-object v1, v1

    .line 1452320
    iput-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->d:LX/1Uo;

    .line 1452321
    new-instance v1, LX/1aX;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->d:LX/1Uo;

    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1aX;-><init>(LX/1aY;)V

    iput-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    .line 1452322
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a:LX/1Ad;

    sget-object v2, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1452323
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->f:Landroid/graphics/Paint;

    .line 1452324
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1452325
    sget-object v1, LX/03r;->ReactorsFaceView:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1452326
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->k:I

    .line 1452327
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->i:Z

    .line 1452328
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->f:Landroid/graphics/Paint;

    const/16 v3, 0x3

    const v4, 0x7f0a00d5

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1452329
    const/16 v2, 0x2

    const v3, 0x7f0b0fd8

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->j:I

    .line 1452330
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->f:Landroid/graphics/Paint;

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->j:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1452331
    const/16 v2, 0x4

    const v3, 0x7f0b0fc7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    .line 1452332
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1452333
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1452304
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingLeft()I

    move-result v0

    .line 1452305
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingTop()I

    move-result v1

    .line 1452306
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 1452307
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1452308
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 1452309
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    add-int/2addr v0, v5

    iget v5, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    add-int/2addr v0, v5

    iget v5, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    add-int/2addr v1, v5

    iget v5, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    add-int/2addr v1, v5

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1452310
    :cond_1
    return-void
.end method

.method private a(LX/1zt;)V
    .locals 1

    .prologue
    .line 1452298
    sget-object v0, LX/1zt;->c:LX/1zt;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p1, v0, :cond_2

    .line 1452299
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    .line 1452300
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1452301
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1452302
    :cond_1
    return-void

    .line 1452303
    :cond_2
    invoke-virtual {p1}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 2

    .prologue
    .line 1452288
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1452289
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1452290
    if-eqz v0, :cond_0

    .line 1452291
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a:LX/1Ad;

    invoke-virtual {v1, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1452292
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1452293
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    .line 1452294
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1452295
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1452296
    :cond_1
    return-void

    .line 1452297
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0203b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a:LX/1Ad;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLActor;LX/1zt;)V
    .locals 0

    .prologue
    .line 1452339
    invoke-direct {p0, p1}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a(Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 1452340
    invoke-direct {p0, p2}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a(LX/1zt;)V

    .line 1452341
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->invalidate()V

    .line 1452342
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->requestLayout()V

    .line 1452343
    return-void
.end method

.method public getFaceSize()I
    .locals 1

    .prologue
    .line 1452250
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xba8d9e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1452251
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1452252
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1452253
    const/16 v1, 0x2d

    const v2, 0x4147b684

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4159e3b1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1452254
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1452255
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1452256
    const/16 v1, 0x2d

    const v2, 0x70ff6dbb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1452257
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1452258
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1452259
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1452260
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 1452261
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->i:Z

    if-eqz v0, :cond_1

    .line 1452262
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->j:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1452263
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1452264
    :cond_2
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1452265
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1452266
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1452267
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1452268
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 1452269
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->k:I

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 1452270
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    .line 1452271
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->i:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->j:I

    .line 1452272
    :goto_1
    iget v1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    add-int/2addr v1, v2

    add-int/2addr v1, v0

    invoke-static {v1, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    iget v2, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->e:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->setMeasuredDimension(II)V

    .line 1452273
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a()V

    .line 1452274
    return-void

    .line 1452275
    :cond_0
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->k:I

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->l:I

    goto :goto_0

    .line 1452276
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1452285
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1452286
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1452287
    return-void
.end method

.method public setFaceSize(I)V
    .locals 1

    .prologue
    .line 1452277
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->k:I

    if-eq v0, p1, :cond_0

    .line 1452278
    iput p1, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->k:I

    .line 1452279
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->invalidate()V

    .line 1452280
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->requestLayout()V

    .line 1452281
    :cond_0
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1452282
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->g:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->h:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_1

    .line 1452283
    :cond_0
    const/4 v0, 0x1

    .line 1452284
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
