.class public Lcom/facebook/feedback/reactions/ui/ReactorsRowView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/2do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/8rk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/9XP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Wc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/5Ou;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8rv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

.field private t:Landroid/widget/TextView;

.field private u:Lcom/facebook/friends/ui/SmartButtonLite;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/friends/ui/SmartButtonLite;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:LX/2Ip;

.field private x:Z

.field private y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1451328
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1451329
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->d()V

    .line 1451330
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1451325
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1451326
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->d()V

    .line 1451327
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1451322
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1451323
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->d()V

    .line 1451324
    return-void
.end method

.method private static a(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;LX/2do;LX/8rk;LX/1zf;LX/9XP;Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;LX/0Ot;LX/0ad;LX/5Ou;LX/8rv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/reactions/ui/ReactorsRowView;",
            "LX/2do;",
            "LX/8rk;",
            "LX/1zf;",
            "LX/9XP;",
            "Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;",
            "LX/0Ot",
            "<",
            "LX/9Wc;",
            ">;",
            "LX/0ad;",
            "LX/5Ou;",
            "LX/8rv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1451321
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->j:LX/2do;

    iput-object p2, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->k:LX/8rk;

    iput-object p3, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->l:LX/1zf;

    iput-object p4, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->m:LX/9XP;

    iput-object p5, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->n:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    iput-object p6, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->o:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->p:LX/0ad;

    iput-object p8, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->q:LX/5Ou;

    iput-object p9, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->r:LX/8rv;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;

    invoke-static {v9}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v1

    check-cast v1, LX/2do;

    invoke-static {v9}, LX/8rk;->b(LX/0QB;)LX/8rk;

    move-result-object v2

    check-cast v2, LX/8rk;

    invoke-static {v9}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v3

    check-cast v3, LX/1zf;

    new-instance v7, LX/9XP;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v9}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v9}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-direct {v7, v4, v5, v6}, LX/9XP;-><init>(LX/0tX;LX/0Sh;LX/0kL;)V

    move-object v4, v7

    check-cast v4, LX/9XP;

    invoke-static {v9}, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->b(LX/0QB;)Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    const/16 v6, 0x2b4b

    invoke-static {v9, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v9}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v9}, LX/5Ou;->a(LX/0QB;)LX/5Ou;

    move-result-object v8

    check-cast v8, LX/5Ou;

    invoke-static {v9}, LX/8rv;->a(LX/0QB;)LX/8rv;

    move-result-object v9

    check-cast v9, LX/8rv;

    invoke-static/range {v0 .. v9}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;LX/2do;LX/8rk;LX/1zf;LX/9XP;Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;LX/0Ot;LX/0ad;LX/5Ou;LX/8rv;)V

    return-void
.end method

.method private a(ZLcom/facebook/friends/ui/SmartButtonLite;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1451305
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->n:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    .line 1451306
    iget-object v2, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1451307
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1451308
    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->f:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1451309
    if-eqz v0, :cond_1

    .line 1451310
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1451311
    :goto_1
    return-void

    .line 1451312
    :cond_1
    invoke-virtual {p2, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1451313
    invoke-virtual {p2, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setFocusable(Z)V

    .line 1451314
    if-nez p1, :cond_2

    invoke-virtual {p2}, Lcom/facebook/friends/ui/SmartButtonLite;->getTag()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/9Wh;->BANNED:LX/9Wh;

    if-ne v0, v1, :cond_3

    .line 1451315
    :cond_2
    invoke-static {p0, p2}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a$redex0(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Lcom/facebook/friends/ui/SmartButtonLite;)V

    goto :goto_1

    .line 1451316
    :cond_3
    sget-object v0, LX/9Wh;->BANNABLE:LX/9Wh;

    invoke-virtual {p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setTag(Ljava/lang/Object;)V

    .line 1451317
    const v0, 0x7f0101e3

    invoke-virtual {p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1451318
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 1451319
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 1451320
    new-instance v0, LX/9B5;

    invoke-direct {v0, p0, p3, p4, p2}, LX/9B5;-><init>(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/friends/ui/SmartButtonLite;)V

    invoke-virtual {p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Lcom/facebook/friends/ui/SmartButtonLite;)V
    .locals 2

    .prologue
    .line 1451298
    if-eqz p1, :cond_0

    .line 1451299
    sget-object v0, LX/9Wh;->BANNED:LX/9Wh;

    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setTag(Ljava/lang/Object;)V

    .line 1451300
    const v0, 0x7f0101e3

    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1451301
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080fb8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 1451302
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 1451303
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1451304
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Lcom/facebook/friends/ui/SmartButtonLite;Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1451272
    invoke-virtual {p1, v6}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1451273
    const v0, 0x7f0101e3

    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1451274
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1451275
    invoke-virtual {p1}, Lcom/facebook/friends/ui/SmartButtonLite;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/friends/ui/SmartButtonLite;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 1451276
    :goto_0
    sget-object v4, LX/9B6;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    move v1, v3

    .line 1451277
    :goto_1
    if-eqz v1, :cond_1

    .line 1451278
    invoke-virtual {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setTag(Ljava/lang/Object;)V

    .line 1451279
    invoke-virtual {p1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->setFocusable(Z)V

    .line 1451280
    invoke-virtual {p1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1451281
    :goto_2
    return-void

    :cond_0
    move-object v0, p2

    .line 1451282
    goto :goto_0

    .line 1451283
    :pswitch_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a00e1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 1451284
    new-instance v1, LX/9B4;

    invoke-direct {v1, p0, p3, p4, p1}, LX/9B4;-><init>(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/friends/ui/SmartButtonLite;)V

    invoke-virtual {p1, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1451285
    invoke-virtual {p1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->setClickable(Z)V

    .line 1451286
    invoke-virtual {p1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    move v1, v2

    .line 1451287
    goto :goto_1

    .line 1451288
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f080fc1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 1451289
    invoke-virtual {p1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setClickable(Z)V

    .line 1451290
    invoke-virtual {p1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    move v1, v2

    .line 1451291
    goto :goto_1

    .line 1451292
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f081781

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 1451293
    invoke-virtual {p1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setClickable(Z)V

    .line 1451294
    invoke-virtual {p1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    move v1, v2

    .line 1451295
    goto :goto_1

    .line 1451296
    :cond_1
    invoke-virtual {p1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setFocusable(Z)V

    .line 1451297
    invoke-virtual {p1, v6}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1451199
    const-class v0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1451200
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->p:LX/0ad;

    sget-short v1, LX/2ez;->q:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    .line 1451201
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->n:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    .line 1451202
    iget-object v1, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1451203
    iget-boolean v0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v0

    .line 1451204
    move v0, v1

    .line 1451205
    iput-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->y:Z

    .line 1451206
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getLayoutRes()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1451207
    const v0, 0x7f0d293d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->s:Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

    .line 1451208
    const v0, 0x7f0d293e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->t:Landroid/widget/TextView;

    .line 1451209
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->k:LX/8rk;

    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->getProfileListFriendingButtonBinder()LX/8rg;

    move-result-object v2

    const v0, 0x7f0d11ce

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0d293f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1, v2, v3, v0}, LX/8rk;->a(LX/8rg;Landroid/view/View;Landroid/widget/TextView;)V

    .line 1451210
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    if-nez v0, :cond_0

    .line 1451211
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->k:LX/8rk;

    invoke-virtual {v0}, LX/8rk;->a()LX/8rj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->w:LX/2Ip;

    .line 1451212
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->e()V

    .line 1451213
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1451268
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->y:Z

    if-nez v0, :cond_0

    .line 1451269
    :goto_0
    return-void

    .line 1451270
    :cond_0
    const v0, 0x7f0d2431

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->u:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 1451271
    const v0, 0x7f0d23ff

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->v:Lcom/facebook/friends/ui/SmartButtonLite;

    goto :goto_0
.end method

.method private getLayoutRes()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 1451262
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->y:Z

    if-eqz v0, :cond_0

    .line 1451263
    const v0, 0x7f03118b

    .line 1451264
    :goto_0
    return v0

    .line 1451265
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->q:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1451266
    const v0, 0x7f031189

    goto :goto_0

    .line 1451267
    :cond_1
    const v0, 0x7f03118a

    goto :goto_0
.end method

.method private getProfileListFriendingButtonBinder()LX/8rg;
    .locals 1

    .prologue
    .line 1451261
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->q:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, LX/8ry;

    invoke-direct {v0}, LX/8ry;-><init>()V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->r:LX/8rv;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/9Al;Ljava/lang/String;ZZLX/8s1;Lcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;)V
    .locals 7
    .param p6    # Lcom/facebook/ipc/feed/ViewPermalinkParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/82m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v6, 0x8

    .line 1451232
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->s:Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;

    .line 1451233
    iget-object v1, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 1451234
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->l:LX/1zf;

    .line 1451235
    iget-object v3, p1, LX/9Al;->b:Ljava/lang/Integer;

    move-object v3, v3

    .line 1451236
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, LX/1zf;->a(I)LX/1zt;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feedback/reactions/ui/widget/ReactorsFaceView;->a(Lcom/facebook/graphql/model/GraphQLActor;LX/1zt;)V

    .line 1451237
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->t:Landroid/widget/TextView;

    .line 1451238
    iget-object v1, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 1451239
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1451240
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->k:LX/8rk;

    .line 1451241
    iget-object v1, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 1451242
    invoke-static {v1}, LX/8rY;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/36N;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    if-nez v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    move-object v2, p5

    move-object v4, p6

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, LX/8rk;->a(LX/36N;LX/8s1;ZLcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;)V

    .line 1451243
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->y:Z

    if-nez v0, :cond_1

    .line 1451244
    :goto_1
    return-void

    .line 1451245
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 1451246
    :cond_1
    if-eqz p3, :cond_2

    .line 1451247
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->u:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 1451248
    iget-object v1, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 1451249
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aK()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v1

    .line 1451250
    iget-object v2, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v2, v2

    .line 1451251
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2, p2}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a$redex0(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Lcom/facebook/friends/ui/SmartButtonLite;Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;Ljava/lang/String;Ljava/lang/String;)V

    .line 1451252
    :goto_2
    if-eqz p4, :cond_3

    .line 1451253
    iget-object v0, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 1451254
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->I()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->v:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 1451255
    iget-object v2, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v2, v2

    .line 1451256
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    .line 1451257
    iget-object v3, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v3, v3

    .line 1451258
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a(ZLcom/facebook/friends/ui/SmartButtonLite;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1451259
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->u:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v6}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto :goto_2

    .line 1451260
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->v:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v6}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3fe81479

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1451227
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onAttachedToWindow()V

    .line 1451228
    iget-boolean v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    if-nez v1, :cond_0

    .line 1451229
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->j:LX/2do;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->w:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1451230
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->k:LX/8rk;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/8rk;->a(Z)V

    .line 1451231
    const/16 v1, 0x2d

    const v2, 0x63f75c0a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6ebc96e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1451222
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->k:LX/8rk;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/8rk;->a(Z)V

    .line 1451223
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDetachedFromWindow()V

    .line 1451224
    iget-boolean v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    if-nez v1, :cond_0

    .line 1451225
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->j:LX/2do;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->w:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1451226
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x68fc33ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 2

    .prologue
    .line 1451218
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onFinishTemporaryDetach()V

    .line 1451219
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    if-nez v0, :cond_0

    .line 1451220
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->j:LX/2do;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->w:LX/2Ip;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1451221
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 1451214
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onStartTemporaryDetach()V

    .line 1451215
    iget-boolean v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->x:Z

    if-nez v0, :cond_0

    .line 1451216
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->j:LX/2do;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->w:LX/2Ip;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1451217
    :cond_0
    return-void
.end method
