.class public Lcom/facebook/feedback/reactions/ui/VectorGraphicConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
.end annotation


# instance fields
.field private mCanvasSize:[F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "canvas_size"
    .end annotation
.end field

.field private mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field private mPaintMapConfig:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "paint_map"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mStates:[Lcom/facebook/feedback/reactions/ui/VectorGraphicConfig$State;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "states"
    .end annotation
.end field

.field private mVectorCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "vector_count"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1451643
    const-class v0, Lcom/facebook/feedback/reactions/ui/VectorGraphicConfigDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1451644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451645
    return-void
.end method
