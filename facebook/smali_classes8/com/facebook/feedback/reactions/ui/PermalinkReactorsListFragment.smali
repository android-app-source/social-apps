.class public Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;
.super Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/9Am;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1450737
    const-class v0, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;

    const-string v1, "permalink_reactors_list"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1450738
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1450727
    const-string v0, "permalink_reactors_list"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1450723
    invoke-super {p0, p1}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Landroid/os/Bundle;)V

    .line 1450724
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;

    new-instance v1, LX/9Am;

    invoke-static {p1}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v0

    check-cast v0, LX/0zG;

    invoke-direct {v1, v0}, LX/9Am;-><init>(LX/0zG;)V

    move-object p1, v1

    check-cast p1, LX/9Am;

    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;->a:LX/9Am;

    .line 1450725
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1450728
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1450729
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1450730
    if-eqz v0, :cond_0

    .line 1450731
    const-string v2, "profileListParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450732
    if-eqz v0, :cond_0

    .line 1450733
    const-string v2, "feedback_id"

    .line 1450734
    iget-object p0, v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1450735
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450736
    :cond_0
    return-object v1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1450726
    const v0, 0x7f0306c0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1450722
    const v0, 0x7f0306be

    return v0
.end method

.method public final gi_()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1450721
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1450711
    invoke-super {p0, p1, p2}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1450712
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450713
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1450714
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450715
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1450716
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;->a:LX/9Am;

    .line 1450717
    iget-object p0, v1, LX/9Am;->a:LX/0zG;

    invoke-interface {p0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1450718
    iget-object p0, v1, LX/9Am;->a:LX/0zG;

    invoke-interface {p0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0h5;

    invoke-interface {p0, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1450719
    :cond_0
    return-void

    .line 1450720
    :cond_1
    const v0, 0x7f080fe7

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
