.class public abstract Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public A:Lcom/facebook/widget/listview/BetterListView;

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/44w",
            "<",
            "LX/1zt;",
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public C:Z

.field public D:Z

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:Z

.field public final a:Ljava/util/Comparator;

.field public b:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/82m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1CW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/9B3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/9Av;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/9BL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/9BT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/9B7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0oz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/9Aj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/9Ak;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

.field public q:Landroid/support/v4/view/ViewPager;

.field public r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public s:LX/9BI;

.field private t:Landroid/view/View;

.field public u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public v:Ljava/lang/String;

.field public w:Landroid/content/res/Resources;

.field public x:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public y:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public z:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x19

    const/4 v1, 0x1

    .line 1450702
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1450703
    new-instance v0, LX/9BA;

    invoke-direct {v0, p0}, LX/9BA;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a:Ljava/util/Comparator;

    .line 1450704
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->z:Landroid/util/SparseArray;

    .line 1450705
    iput v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->E:I

    .line 1450706
    iput v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->F:I

    .line 1450707
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->G:I

    .line 1450708
    iput v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->H:I

    .line 1450709
    iput-boolean v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->I:Z

    .line 1450710
    return-void
.end method

.method public static a(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;Z)V
    .locals 4

    .prologue
    .line 1450683
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 1450684
    :cond_0
    :goto_0
    return-void

    .line 1450685
    :cond_1
    invoke-static {p0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    .line 1450686
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 1450687
    if-eqz v1, :cond_0

    .line 1450688
    const v0, 0x7f0d124b

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1450689
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    instance-of v2, v2, LX/9B9;

    if-eqz v2, :cond_2

    .line 1450690
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, LX/9B9;

    .line 1450691
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    .line 1450692
    iget-object v3, v2, LX/9B3;->l:LX/1zt;

    if-eqz v3, :cond_3

    .line 1450693
    iget v3, p1, LX/1zt;->e:I

    move v3, v3

    .line 1450694
    iget-object p0, v2, LX/9B3;->l:LX/1zt;

    .line 1450695
    iget p1, p0, LX/1zt;->e:I

    move p0, p1

    .line 1450696
    if-ne v3, p0, :cond_3

    iget-object v3, v2, LX/9B3;->m:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    :goto_1
    move-object v2, v3

    .line 1450697
    iput-object v2, v0, LX/9B9;->b:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 1450698
    const v2, 0x1fa78d22

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1450699
    :cond_2
    if-eqz p2, :cond_0

    .line 1450700
    const v0, 0x7f0d124a

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1450701
    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1nG;LX/82m;LX/1CW;LX/9B3;LX/9Av;LX/9BL;LX/1zf;LX/9BT;LX/9B7;LX/0oz;LX/9Aj;LX/9Ak;LX/17W;)V
    .locals 0

    .prologue
    .line 1450682
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->c:LX/1nG;

    iput-object p3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    iput-object p4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->e:LX/1CW;

    iput-object p5, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    iput-object p6, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->g:LX/9Av;

    iput-object p7, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->h:LX/9BL;

    iput-object p8, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->i:LX/1zf;

    iput-object p9, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    iput-object p10, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    iput-object p11, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->l:LX/0oz;

    iput-object p12, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->m:LX/9Aj;

    iput-object p13, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->n:LX/9Ak;

    iput-object p14, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->o:LX/17W;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-static {v14}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v14}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v2

    check-cast v2, LX/1nG;

    invoke-static {v14}, LX/82m;->b(LX/0QB;)LX/82m;

    move-result-object v3

    check-cast v3, LX/82m;

    invoke-static {v14}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v4

    check-cast v4, LX/1CW;

    invoke-static {v14}, LX/9B3;->b(LX/0QB;)LX/9B3;

    move-result-object v5

    check-cast v5, LX/9B3;

    invoke-static {v14}, LX/9Av;->b(LX/0QB;)LX/9Av;

    move-result-object v6

    check-cast v6, LX/9Av;

    invoke-static {v14}, LX/9BL;->a(LX/0QB;)LX/9BL;

    move-result-object v7

    check-cast v7, LX/9BL;

    invoke-static {v14}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v8

    check-cast v8, LX/1zf;

    invoke-static {v14}, LX/9BT;->b(LX/0QB;)LX/9BT;

    move-result-object v9

    check-cast v9, LX/9BT;

    invoke-static {v14}, LX/9B7;->a(LX/0QB;)LX/9B7;

    move-result-object v10

    check-cast v10, LX/9B7;

    invoke-static {v14}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v11

    check-cast v11, LX/0oz;

    invoke-static {v14}, LX/9Aj;->a(LX/0QB;)LX/9Aj;

    move-result-object v12

    check-cast v12, LX/9Aj;

    invoke-static {v14}, LX/9Ak;->a(LX/0QB;)LX/9Ak;

    move-result-object v13

    check-cast v13, LX/9Ak;

    invoke-static {v14}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v14

    check-cast v14, LX/17W;

    invoke-static/range {v0 .. v14}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1nG;LX/82m;LX/1CW;LX/9B3;LX/9Av;LX/9BL;LX/1zf;LX/9BT;LX/9B7;LX/0oz;LX/9Aj;LX/9Ak;LX/17W;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;I)I
    .locals 2

    .prologue
    .line 1450679
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    if-nez v0, :cond_0

    .line 1450680
    const/4 v0, 0x0

    .line 1450681
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-static {v1, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9B3;->b(LX/1zt;)I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/55h;)LX/1Cw;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;)",
            "LX/1Cw;"
        }
    .end annotation

    .prologue
    .line 1450668
    new-instance v0, LX/9B9;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450669
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1450670
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450671
    iget-boolean v3, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->h:Z

    move v3, v3

    .line 1450672
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450673
    iget-boolean v4, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->i:Z

    move v4, v4

    .line 1450674
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450675
    iget-object v5, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    move-object v5, v5

    .line 1450676
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450677
    iget-object v6, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    move-object v6, v6

    .line 1450678
    iget-object v7, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    iget-object v8, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->o:LX/17W;

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, LX/9B9;-><init>(LX/55h;Ljava/lang/String;ZZLX/8s1;Lcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;LX/17W;)V

    return-object v0
.end method

.method public static c$redex0(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;)V
    .locals 3

    .prologue
    .line 1450653
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    .line 1450654
    if-eqz p1, :cond_1

    invoke-virtual {v0}, LX/9B7;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1450655
    if-eqz v0, :cond_0

    .line 1450656
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    invoke-virtual {v0, p1}, LX/9B3;->b(LX/1zt;)I

    move-result v1

    .line 1450657
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    .line 1450658
    iget v2, p1, LX/1zt;->e:I

    move v2, v2

    .line 1450659
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1450660
    if-ne v0, v1, :cond_2

    .line 1450661
    :cond_0
    :goto_1
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;Z)V

    .line 1450662
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1450663
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    .line 1450664
    iget v2, p1, LX/1zt;->e:I

    move v2, v2

    .line 1450665
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450666
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    if-eqz v0, :cond_0

    .line 1450667
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    goto :goto_1
.end method

.method public static r(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1450613
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    if-eqz v0, :cond_1

    .line 1450614
    :cond_0
    :goto_0
    return-void

    .line 1450615
    :cond_1
    const/4 v3, 0x0

    .line 1450616
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1450617
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    invoke-virtual {v0}, LX/9B7;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v3

    :goto_1
    if-ge v1, v6, :cond_5

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 1450618
    iget-object v7, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    .line 1450619
    invoke-static {v7, v0}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v8

    .line 1450620
    invoke-static {v7, v8}, LX/9B3;->a(LX/9B3;I)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1450621
    const/4 v8, 0x0

    .line 1450622
    :goto_2
    move-object v7, v8

    .line 1450623
    if-eqz v7, :cond_4

    .line 1450624
    iget v8, v0, LX/1zt;->e:I

    move v8, v8

    .line 1450625
    iget-object v9, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    .line 1450626
    sget-object v10, LX/1zt;->c:LX/1zt;

    move-object v10, v10

    .line 1450627
    iget v11, v10, LX/1zt;->e:I

    move v10, v11

    .line 1450628
    if-ne v8, v10, :cond_2

    .line 1450629
    if-eqz v9, :cond_9

    .line 1450630
    sget-object v10, LX/1zt;->c:LX/1zt;

    move-object v10, v10

    .line 1450631
    iget v11, v10, LX/1zt;->e:I

    move v10, v11

    .line 1450632
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    const/4 v10, 0x1

    :goto_3
    move v10, v10

    .line 1450633
    if-nez v10, :cond_3

    :cond_2
    invoke-static {v8, v9}, LX/9B7;->b(ILjava/util/HashMap;)I

    move-result v10

    if-lez v10, :cond_8

    :cond_3
    const/4 v10, 0x1

    :goto_4
    move v8, v10

    .line 1450634
    if-eqz v8, :cond_4

    .line 1450635
    new-instance v8, LX/44w;

    invoke-direct {v8, v0, v7}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1450636
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1450637
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1450638
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a:Ljava/util/Comparator;

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1450639
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1450640
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, LX/1zt;

    .line 1450641
    iget-object v3, v1, LX/82m;->b:LX/0if;

    sget-object v6, LX/82m;->a:LX/0ih;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "tabs_count_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1450642
    iget-object v3, v1, LX/82m;->b:LX/0if;

    sget-object v6, LX/82m;->a:LX/0ih;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "first_tab_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1450643
    iget v8, v0, LX/1zt;->e:I

    move v8, v8

    .line 1450644
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1450645
    :cond_6
    move-object v0, v4

    .line 1450646
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->B:Ljava/util/List;

    .line 1450647
    new-instance v0, LX/9BI;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->B:Ljava/util/List;

    invoke-direct {v0, p0, v1}, LX/9BI;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;Ljava/util/List;)V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    .line 1450648
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->H:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1450649
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->h:LX/9BL;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/view/ViewPager;->a(ZLX/3sI;)V

    .line 1450650
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1450651
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1450652
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-static {v1, v2}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v1

    invoke-static {v1}, LX/9B7;->c(LX/1zt;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineColor(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v9, v7, LX/9B3;->k:[LX/55h;

    aget-object v8, v9, v8

    goto/16 :goto_2

    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v10, 0x0

    goto/16 :goto_3
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 1450569
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1450570
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1450571
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    .line 1450572
    iget-object v1, v0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x820001

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1450573
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1450574
    const-string v1, "profileListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450575
    new-instance v0, LX/9BJ;

    invoke-direct {v0, p0}, LX/9BJ;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    .line 1450576
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->g:LX/9Av;

    .line 1450577
    new-instance v2, LX/9BB;

    invoke-direct {v2, p0}, LX/9BB;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    move-object v2, v2

    .line 1450578
    iput-object v2, v1, LX/9Av;->d:LX/9BB;

    .line 1450579
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->g:LX/9Av;

    .line 1450580
    iput-object v0, v1, LX/9Av;->e:LX/9BJ;

    .line 1450581
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    new-instance v2, LX/9BK;

    invoke-direct {v2, p0}, LX/9BK;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    .line 1450582
    iput-object v2, v1, LX/9B3;->q:LX/9BK;

    .line 1450583
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    .line 1450584
    iput-object v0, v1, LX/9B3;->r:LX/9BJ;

    .line 1450585
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    const/4 v2, 0x0

    .line 1450586
    iget-object v1, v0, LX/9B3;->j:LX/0Px;

    move-object v1, v1

    .line 1450587
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    move v1, v1

    .line 1450588
    new-array v3, v1, [Z

    iput-object v3, v0, LX/9B3;->n:[Z

    .line 1450589
    new-array v3, v1, [LX/55h;

    iput-object v3, v0, LX/9B3;->k:[LX/55h;

    .line 1450590
    new-array v1, v1, [I

    iput-object v1, v0, LX/9B3;->p:[I

    move v1, v2

    .line 1450591
    :goto_0
    iget-object v3, v0, LX/9B3;->k:[LX/55h;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 1450592
    new-instance v3, LX/55h;

    sget-object p1, LX/9B3;->a:LX/0QK;

    invoke-direct {v3, p1}, LX/55h;-><init>(LX/0QK;)V

    .line 1450593
    invoke-static {v0, v1}, LX/9B3;->a(LX/9B3;I)Z

    move-result p1

    if-nez p1, :cond_2

    .line 1450594
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1450595
    :cond_0
    iget-object v1, v0, LX/9B3;->f:LX/0ad;

    sget-short v3, LX/2ez;->q:S

    invoke-interface {v1, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    iput-boolean v1, v0, LX/9B3;->s:Z

    .line 1450596
    iget-object v1, v0, LX/9B3;->f:LX/0ad;

    sget-short v3, LX/1sg;->k:S

    invoke-interface {v1, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    iput-boolean v1, v0, LX/9B3;->t:Z

    .line 1450597
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->w:Landroid/content/res/Resources;

    .line 1450598
    const/16 v5, 0x19

    const/4 v6, 0x1

    .line 1450599
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->m:LX/9Aj;

    .line 1450600
    iget-object v7, v4, LX/9Aj;->a:LX/0Uh;

    const/16 v8, 0x4b1

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, LX/0Uh;->a(IZ)Z

    move-result v7

    move v4, v7

    .line 1450601
    if-eqz v4, :cond_1

    .line 1450602
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->n:LX/9Ak;

    invoke-virtual {v4, v5}, LX/9Ak;->a(I)I

    move-result v4

    iput v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->E:I

    .line 1450603
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->n:LX/9Ak;

    invoke-virtual {v4, v5}, LX/9Ak;->a(I)I

    move-result v4

    iput v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->F:I

    .line 1450604
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->n:LX/9Ak;

    const/4 v5, 0x0

    .line 1450605
    iget-object v7, v4, LX/9Ak;->a:LX/0uf;

    sget-wide v9, LX/0X5;->dd:J

    int-to-long v11, v5

    invoke-virtual {v7, v9, v10, v11, v12}, LX/0uf;->a(JJ)J

    move-result-wide v7

    long-to-int v7, v7

    move v4, v7

    .line 1450606
    iput v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->G:I

    .line 1450607
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->n:LX/9Ak;

    .line 1450608
    iget-object v7, v4, LX/9Ak;->a:LX/0uf;

    sget-wide v9, LX/0X5;->de:J

    int-to-long v11, v6

    invoke-virtual {v7, v9, v10, v11, v12}, LX/0uf;->a(JJ)J

    move-result-wide v7

    long-to-int v7, v7

    move v4, v7

    .line 1450609
    iput v4, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->H:I

    .line 1450610
    iput-boolean v6, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->D:Z

    .line 1450611
    :cond_1
    return-void

    .line 1450612
    :cond_2
    iget-object p1, v0, LX/9B3;->k:[LX/55h;

    aput-object v3, p1, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1450561
    check-cast p1, LX/9Al;

    .line 1450562
    iget-object v0, p1, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 1450563
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->c:LX/1nG;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1450564
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1450565
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x285feb

    if-ne v3, v4, :cond_0

    .line 1450566
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1450567
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 1450568
    return-void
.end method

.method public abstract c()I
.end method

.method public abstract d()I
.end method

.method public abstract gi_()Lcom/facebook/common/callercontext/CallerContext;
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1ddfa737

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1450548
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1450549
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450550
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1450551
    iput-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->v:Ljava/lang/String;

    .line 1450552
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1450553
    iget-object v4, v2, Lcom/facebook/ufiservices/flyout/ProfileListParams;->l:Ljava/lang/String;

    move-object v2, v4

    .line 1450554
    iget-object v4, v1, LX/82m;->b:LX/0if;

    sget-object p1, LX/82m;->a:LX/0ih;

    invoke-virtual {v4, p1}, LX/0if;->a(LX/0ih;)V

    .line 1450555
    iget-object v4, v1, LX/82m;->b:LX/0if;

    sget-object p1, LX/82m;->a:LX/0ih;

    invoke-virtual {v4, p1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1450556
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->l:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    .line 1450557
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "connection_status:"

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1450558
    iget-object v4, v2, LX/82m;->b:LX/0if;

    sget-object p1, LX/82m;->a:LX/0ih;

    invoke-virtual {v4, p1, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1450559
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->g:LX/9Av;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/9Av;->a(Z)V

    .line 1450560
    const/16 v1, 0x2b

    const v2, -0x1c3d6ea6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x45f9e25a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1450536
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1450537
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->t:Landroid/view/View;

    .line 1450538
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->t:Landroid/view/View;

    const v2, 0x7f0d1246

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1450539
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->t:Landroid/view/View;

    const v2, 0x7f0d1249

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    .line 1450540
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->t:Landroid/view/View;

    const v2, 0x7f0d1248

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1450541
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1450542
    new-instance v2, LX/9BD;

    invoke-direct {v2, p0}, LX/9BD;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    move-object v2, v2

    .line 1450543
    iput-object v2, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->m:LX/6Uh;

    .line 1450544
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1450545
    new-instance v2, LX/9BE;

    invoke-direct {v2, p0}, LX/9BE;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    move-object v2, v2

    .line 1450546
    iput-object v2, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 1450547
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->t:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x7b9f0176

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x40f1c487

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1450524
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1450525
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    .line 1450526
    iget-object v2, v1, LX/9B3;->h:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1450527
    iget-boolean v2, v1, LX/9B3;->s:Z

    if-eqz v2, :cond_0

    .line 1450528
    iget-object v2, v1, LX/9B3;->b:LX/3H7;

    .line 1450529
    iget-object v4, v2, LX/3H7;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1My;

    invoke-virtual {v4}, LX/1My;->a()V

    .line 1450530
    iget-object v2, v1, LX/9B3;->e:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 1450531
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->g:LX/9Av;

    .line 1450532
    iget-object v2, v1, LX/9Av;->a:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1450533
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    .line 1450534
    iget-object v2, v1, LX/82m;->b:LX/0if;

    sget-object v4, LX/82m;->a:LX/0ih;

    invoke-virtual {v2, v4}, LX/0if;->c(LX/0ih;)V

    .line 1450535
    const/16 v1, 0x2b

    const v2, -0x1ae27af5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x7a8e9184

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1450509
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-static {p0, v2}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a$redex0(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;I)I

    move-result v2

    const v8, 0x820001

    const/4 v7, 0x4

    .line 1450510
    if-lez v2, :cond_0

    .line 1450511
    iget-object v5, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v6, 0x2

    invoke-interface {v5, v8, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1450512
    :goto_0
    iget-object v5, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x820009

    invoke-interface {v5, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(IS)V

    .line 1450513
    iget-object v5, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x820008

    invoke-interface {v5, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(IS)V

    .line 1450514
    iget-object v5, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x820006

    invoke-interface {v5, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(IS)V

    .line 1450515
    iput-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    .line 1450516
    iput-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1450517
    iput-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    .line 1450518
    iput-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->t:Landroid/view/View;

    .line 1450519
    iput-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1450520
    iput-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->A:Lcom/facebook/widget/listview/BetterListView;

    .line 1450521
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1450522
    const/16 v1, 0x2b

    const v2, 0x4e308720    # 7.4041139E8f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1450523
    :cond_0
    iget-object v5, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v5, v8, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xf840a9e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1450505
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1450506
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    .line 1450507
    iget-object v2, v1, LX/82m;->b:LX/0if;

    sget-object v4, LX/82m;->a:LX/0ih;

    const-string p0, "fragment_pause"

    invoke-virtual {v2, v4, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1450508
    const/16 v1, 0x2b

    const v2, 0x13980ef4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x42d936e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1450501
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1450502
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    .line 1450503
    iget-object v2, v1, LX/82m;->b:LX/0if;

    sget-object v4, LX/82m;->a:LX/0ih;

    const-string p0, "fragment_resume"

    invoke-virtual {v2, v4, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1450504
    const/16 v1, 0x2b

    const v2, 0x7445f2e8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
