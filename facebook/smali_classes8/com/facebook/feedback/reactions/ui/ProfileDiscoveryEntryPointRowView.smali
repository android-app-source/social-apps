.class public Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1450759
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1450760
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;->d()V

    .line 1450761
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1450762
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1450763
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;->d()V

    .line 1450764
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1450765
    const v0, 0x7f031187

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1450766
    const v0, 0x7f0d293e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1450767
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0819a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1450768
    const v0, 0x7f0d293f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1450769
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0819a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1450770
    return-void
.end method
