.class public Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;
.super Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8qC;


# static fields
.field private static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0hG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1450983
    const-class v0, Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;

    const-string v1, "flyout_reactors_list"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1450981
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;-><init>()V

    .line 1450982
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1450980
    const-string v0, "flyout_reactors_list"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1450976
    invoke-super {p0, p1}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Landroid/os/Bundle;)V

    .line 1450977
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1450978
    check-cast v0, LX/0hG;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;->a:LX/0hG;

    .line 1450979
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1450975
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1450955
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    if-nez v1, :cond_4

    .line 1450956
    :cond_0
    const/4 v1, 0x0

    .line 1450957
    :goto_0
    move-object v1, v1

    .line 1450958
    if-nez v1, :cond_2

    .line 1450959
    :cond_1
    :goto_1
    return v0

    .line 1450960
    :cond_2
    sget-object v2, LX/9Ay;->a:[I

    invoke-virtual {p3}, LX/31M;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 1450961
    :pswitch_0
    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->isAtBottom()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1450962
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    if-nez v1, :cond_6

    .line 1450963
    :cond_3
    const/4 v1, 0x1

    .line 1450964
    :goto_2
    move v1, v1

    .line 1450965
    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    .line 1450966
    :pswitch_1
    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->a()Z

    move-result v0

    goto :goto_1

    .line 1450967
    :cond_4
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->A:Lcom/facebook/widget/listview/BetterListView;

    if-nez v1, :cond_5

    .line 1450968
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v2, v3}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 1450969
    if-eqz v1, :cond_5

    .line 1450970
    const v2, 0x7f0d124b

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    iput-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->A:Lcom/facebook/widget/listview/BetterListView;

    .line 1450971
    :cond_5
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->A:Lcom/facebook/widget/listview/BetterListView;

    goto :goto_0

    .line 1450972
    :cond_6
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->B:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/44w;

    iget-object v1, v1, LX/44w;->b:Ljava/lang/Object;

    check-cast v1, LX/55h;

    .line 1450973
    iget-boolean v2, v1, LX/55h;->g:Z

    move v1, v2

    .line 1450974
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1450938
    const v0, 0x7f0306c0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1450954
    const v0, 0x7f0306bd

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1450953
    const-string v0, "flyout_reactors_animation_perf"

    return-object v0
.end method

.method public final gi_()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1450952
    sget-object v0, Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final gj_()V
    .locals 0

    .prologue
    .line 1450951
    return-void
.end method

.method public final gk_()V
    .locals 0

    .prologue
    .line 1450950
    return-void
.end method

.method public final hu_()Z
    .locals 1

    .prologue
    .line 1450949
    const/4 v0, 0x0

    return v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 1450948
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 1

    .prologue
    .line 1450947
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 1450939
    invoke-super {p0, p1, p2}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1450940
    const v0, 0x7f0d1245

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    .line 1450941
    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowFocusable(Z)V

    .line 1450942
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowVisibility(I)V

    .line 1450943
    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderTextFocusable(Z)V

    .line 1450944
    const v1, 0x7f080fe7

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderText(I)V

    .line 1450945
    new-instance v1, LX/9Ax;

    invoke-direct {v1, p0}, LX/9Ax;-><init>(Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1450946
    return-void
.end method
