.class public final Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1zl;

.field private final b:LX/22E;


# direct methods
.method public constructor <init>(LX/1zl;LX/22E;)V
    .locals 0

    .prologue
    .line 1450877
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->a:LX/1zl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450878
    iput-object p2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->b:LX/22E;

    .line 1450879
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1450880
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->b:LX/22E;

    .line 1450881
    iget-object v1, v0, LX/22E;->p:LX/22F;

    move-object v0, v1

    .line 1450882
    iget-object v1, v0, LX/22F;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1450883
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1450884
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->a:LX/1zl;

    const-string v1, "apk_faces_"

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->b:LX/22E;

    .line 1450885
    iget v3, v2, LX/22E;->l:I

    move v2, v3

    .line 1450886
    invoke-static {v1, v2}, LX/1zl;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/1zl;->c(LX/1zl;Ljava/lang/String;)V

    .line 1450887
    :goto_0
    return-void

    .line 1450888
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->a:LX/1zl;

    iget-object v1, v1, LX/1zl;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 1450889
    const-string v3, "/"

    const-string v4, "_"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1450890
    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 1450891
    invoke-static {v0}, LX/3JK;->a(Ljava/io/InputStream;)LX/3K3;

    move-result-object v1

    .line 1450892
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 1450893
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->a:LX/1zl;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1450894
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->a:LX/1zl;

    iget-object v0, v0, LX/1zl;->e:LX/0YU;

    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->b:LX/22E;

    .line 1450895
    iget v4, v3, LX/22E;->l:I

    move v3, v4

    .line 1450896
    invoke-virtual {v0, v3, v1}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 1450897
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1450898
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->a:LX/1zl;

    const-string v1, "apk_faces_"

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;->b:LX/22E;

    .line 1450899
    iget v3, v2, LX/22E;->l:I

    move v2, v3

    .line 1450900
    invoke-static {v1, v2}, LX/1zl;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/1zl;->c(LX/1zl;Ljava/lang/String;)V

    goto :goto_0

    .line 1450901
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    goto :goto_1
.end method
