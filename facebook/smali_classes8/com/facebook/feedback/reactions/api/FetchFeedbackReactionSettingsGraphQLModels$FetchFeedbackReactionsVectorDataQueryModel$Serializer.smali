.class public final Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1450393
    const-class v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;

    new-instance v1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1450394
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1450395
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1450396
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1450397
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1450398
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1450399
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1450400
    if-eqz v2, :cond_2

    .line 1450401
    const-string p0, "feedback_reaction_settings"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1450402
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1450403
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1450404
    if-eqz p0, :cond_1

    .line 1450405
    const-string v0, "reaction_infos"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1450406
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1450407
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1450408
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/3gz;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1450409
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1450410
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1450411
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1450412
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1450413
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1450414
    check-cast p1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$Serializer;->a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
