.class public final Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1450331
    const-class v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel;

    new-instance v1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1450332
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1450333
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1450334
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1450335
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1450336
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1450337
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1450338
    if-eqz v2, :cond_2

    .line 1450339
    const-string p0, "feedback_reaction_settings"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1450340
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1450341
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1450342
    if-eqz p0, :cond_1

    .line 1450343
    const-string v0, "reaction_infos"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1450344
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1450345
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1450346
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/3fH;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1450347
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1450348
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1450349
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1450350
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1450351
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1450352
    check-cast p1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$Serializer;->a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
