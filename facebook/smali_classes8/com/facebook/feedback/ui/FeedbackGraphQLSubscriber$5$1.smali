.class public final Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

.field public final synthetic b:LX/9DY;


# direct methods
.method public constructor <init>(LX/9DY;Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;)V
    .locals 0

    .prologue
    .line 1455730
    iput-object p1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->b:LX/9DY;

    iput-object p2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1455731
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1455732
    :cond_0
    :goto_0
    return-void

    .line 1455733
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1455734
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->b:LX/9DY;

    iget-object v1, v1, LX/9DY;->a:LX/9Da;

    iget-object v1, v1, LX/9Da;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1455735
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->b:LX/9DY;

    iget-object v1, v1, LX/9DY;->a:LX/9Da;

    iget-object v1, v1, LX/9Da;->b:LX/1K9;

    new-instance v2, LX/8q0;

    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/8q0;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1455736
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;->b:LX/9DY;

    iget-object v0, v0, LX/9DY;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->d:LX/3iR;

    invoke-virtual {v0}, LX/3iR;->a()V

    goto :goto_0
.end method
