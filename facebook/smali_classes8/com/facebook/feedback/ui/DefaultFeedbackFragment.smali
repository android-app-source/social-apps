.class public Lcom/facebook/feedback/ui/DefaultFeedbackFragment;
.super Lcom/facebook/feedback/ui/BaseFeedbackFragment;
.source ""

# interfaces
.implements LX/8qD;


# instance fields
.field public B:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/1nK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/9Dh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0tH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:Ljava/lang/String;

.field public G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

.field private H:LX/195;

.field private I:Landroid/view/ContextThemeWrapper;

.field private J:LX/9EH;

.field public K:LX/9EG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:Z

.field public M:Z

.field private final N:LX/0fu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1455446
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;-><init>()V

    .line 1455447
    new-instance v0, LX/9DI;

    invoke-direct {v0, p0}, LX/9DI;-><init>(Lcom/facebook/feedback/ui/DefaultFeedbackFragment;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->N:LX/0fu;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1455445
    const-string v0, "story_feedback_flyout"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1455441
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;

    const-class v3, LX/193;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/193;

    invoke-static {v0}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v4

    check-cast v4, LX/1nK;

    const-class v5, LX/9Dh;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9Dh;

    invoke-static {v0}, LX/9EG;->a(LX/0QB;)LX/9EG;

    move-result-object v6

    check-cast v6, LX/9EG;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v0

    check-cast v0, LX/0tH;

    iput-object v3, v2, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->B:LX/193;

    iput-object v4, v2, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    iput-object v5, v2, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->D:LX/9Dh;

    iput-object v6, v2, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->K:LX/9EG;

    iput-object v0, v2, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->E:LX/0tH;

    .line 1455442
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Landroid/os/Bundle;)V

    .line 1455443
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->B:LX/193;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "default_feedback_scroll_perf"

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->H:LX/195;

    .line 1455444
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 1

    .prologue
    .line 1455438
    invoke-super {p0, p1, p2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 1455439
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0, p1}, LX/1nK;->a(Ljava/lang/Throwable;)V

    .line 1455440
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1455378
    iput-object p1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->F:Ljava/lang/String;

    .line 1455379
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 1455428
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1455429
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->h()V

    .line 1455430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->M:Z

    .line 1455431
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1455432
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->G()V

    .line 1455433
    :goto_0
    return-void

    .line 1455434
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1455435
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    iget-object p1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->F:Ljava/lang/String;

    invoke-virtual {v0, p1}, LX/9DG;->a(Ljava/lang/String;)V

    .line 1455436
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->F:Ljava/lang/String;

    .line 1455437
    :cond_1
    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 1455419
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1455420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->L:Z

    .line 1455421
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1455422
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->f()V

    .line 1455423
    :cond_1
    :goto_0
    return-void

    .line 1455424
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->e()V

    .line 1455425
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->g()V

    .line 1455426
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1455427
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->F()V

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1455418
    const-string v0, "flyout_feedback_animation_perf"

    return-object v0
.end method

.method public final gj_()V
    .locals 1

    .prologue
    .line 1455415
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->A()V

    .line 1455416
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->x()V

    .line 1455417
    return-void
.end method

.method public final gk_()V
    .locals 1

    .prologue
    .line 1455411
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->gk_()V

    .line 1455412
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->y()V

    .line 1455413
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->B()V

    .line 1455414
    return-void
.end method

.method public final m()LX/21B;
    .locals 1

    .prologue
    .line 1455410
    sget-object v0, LX/21B;->DEFAULT_FEEDBACK:LX/21B;

    return-object v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1455408
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->i()V

    .line 1455409
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5985452e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1455404
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->n()V

    .line 1455405
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1455406
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->o()V

    .line 1455407
    const/16 v1, 0x2b

    const v2, 0x7f823665

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x250c2f9f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1455399
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->j()V

    .line 1455400
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->r()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1455401
    const v2, 0x7f030655

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1455402
    iget-object v2, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v2}, LX/1nK;->k()V

    .line 1455403
    const/16 v2, 0x2b

    const v3, -0x53f34e1c    # -1.9993946E-12f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x1f50f16

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1455391
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onDestroyView()V

    .line 1455392
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    if-eqz v1, :cond_0

    .line 1455393
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1455394
    iput-object v2, v1, Lcom/facebook/feedback/ui/FeedbackHeaderView;->k:LX/9Df;

    .line 1455395
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b(LX/21l;)V

    .line 1455396
    iput-object v2, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1455397
    iput-object v2, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->J:LX/9EH;

    .line 1455398
    const/16 v1, 0x2b

    const v2, -0x492eefb4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x205ef696

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1455387
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->C()V

    .line 1455388
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->H:LX/195;

    invoke-virtual {v1}, LX/195;->b()V

    .line 1455389
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onPause()V

    .line 1455390
    const/16 v1, 0x2b

    const v2, 0x160209dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a32ba63

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1455380
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->p()V

    .line 1455381
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->w()V

    .line 1455382
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onResume()V

    .line 1455383
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->q()V

    .line 1455384
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->r()V

    .line 1455385
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->z()V

    .line 1455386
    const/16 v1, 0x2b

    const v2, -0x5fb96f30

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1455356
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->l()V

    .line 1455357
    const v0, 0x7f0d118f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1455358
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->D:LX/9Dh;

    invoke-virtual {v1, p0}, LX/9Dh;->a(Lcom/facebook/base/fragment/FbFragment;)LX/9Dg;

    move-result-object v1

    .line 1455359
    iput-object v1, v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->k:LX/9Df;

    .line 1455360
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(LX/21l;)V

    .line 1455361
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1455362
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1455363
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1455364
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1455365
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1455366
    if-eqz v0, :cond_0

    .line 1455367
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1455368
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1455369
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1455370
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455371
    invoke-static {v0}, LX/0sa;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m:LX/0ad;

    sget-short v2, LX/9Io;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1455372
    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->G:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-static {v0}, LX/0sa;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1455373
    iput-object v0, v1, Lcom/facebook/feedback/ui/FeedbackHeaderView;->l:Ljava/lang/String;

    .line 1455374
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1455375
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->N:LX/0fu;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fu;)V

    .line 1455376
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    new-instance v1, LX/2je;

    iget-object v2, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->H:LX/195;

    invoke-direct {v1, v2}, LX/2je;-><init>(LX/195;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 1455377
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1455354
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->C:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->m()V

    .line 1455355
    return-void
.end method

.method public final r()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1455349
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->I:Landroid/view/ContextThemeWrapper;

    if-nez v0, :cond_0

    .line 1455350
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1455351
    const v2, 0x7f0e0606

    move v2, v2

    .line 1455352
    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->I:Landroid/view/ContextThemeWrapper;

    .line 1455353
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;->I:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1455348
    const/4 v0, 0x0

    return v0
.end method
