.class public Lcom/facebook/feedback/ui/FeedbackHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/21l",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:LX/14w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1za;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9EK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/widget/TextView;

.field private g:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feedback/ui/ReactorsDescriptionView;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private j:Landroid/view/View$OnClickListener;

.field public k:LX/9Df;

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1456023
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1456024
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1456021
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1456018
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456019
    invoke-direct {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a()V

    .line 1456020
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1456010
    const-class v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1456011
    const v0, 0x7f030659

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1456012
    const v0, 0x7f0d1197

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    .line 1456013
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1199

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->g:LX/0zw;

    .line 1456014
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1198

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->h:LX/0zw;

    .line 1456015
    new-instance v0, LX/9De;

    invoke-direct {v0, p0}, LX/9De;-><init>(Lcom/facebook/feedback/ui/FeedbackHeaderView;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->j:Landroid/view/View$OnClickListener;

    .line 1456016
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setVisibility(I)V

    .line 1456017
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1456001
    if-eqz p1, :cond_0

    .line 1456002
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456003
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1456004
    invoke-direct {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->b()V

    .line 1456005
    if-nez p1, :cond_1

    .line 1456006
    :goto_1
    return-void

    .line 1456007
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1456008
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1456009
    invoke-direct {p0, v0, p1}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 1455920
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->c:LX/9EK;

    invoke-virtual {v0, p1, p2}, LX/9EK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->d:LX/0ad;

    sget-short v1, LX/227;->a:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1455921
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1455922
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->setVisibility(I)V

    .line 1455923
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1455924
    :cond_1
    :goto_0
    return-void

    .line 1455925
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1455926
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->c:LX/9EK;

    .line 1455927
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1455928
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455929
    new-instance v2, LX/9EJ;

    invoke-direct {v2, v0, v1}, LX/9EJ;-><init>(LX/9EK;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v1, v2

    .line 1455930
    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->c:LX/9EK;

    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;

    .line 1455931
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455932
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455933
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    .line 1455934
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1455935
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455936
    iget-object v6, v2, LX/9EK;->d:LX/14w;

    invoke-virtual {v6, v3}, LX/14w;->l(Lcom/facebook/graphql/model/GraphQLStory;)LX/0am;

    move-result-object v3

    .line 1455937
    if-eqz v3, :cond_5

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1455938
    iget-object v6, v2, LX/9EK;->c:LX/1Uf;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v3}, LX/1eD;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    move-result-object v3

    const/4 v7, 0x1

    invoke-virtual {v6, v3, v7, v5}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v3

    .line 1455939
    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1455940
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1455941
    :goto_1
    move-object v2, v3

    .line 1455942
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->c:LX/9EK;

    .line 1455943
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1455944
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->h:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedback/ui/ReactorsDescriptionView;

    const/4 p2, 0x0

    .line 1455945
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v7

    .line 1455946
    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1455947
    invoke-static {}, LX/9Iy;->newBuilder()LX/9Iw;

    move-result-object v8

    .line 1455948
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v9, :cond_3

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    .line 1455949
    iget-object v10, v3, LX/9EK;->g:LX/1zf;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result p1

    invoke-virtual {v10, p1}, LX/1zf;->a(I)LX/1zt;

    move-result-object v10

    .line 1455950
    invoke-virtual {v10}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 1455951
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1455952
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5, v10, p2, p2}, LX/9Iw;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;)LX/9Iw;

    .line 1455953
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 1455954
    :cond_3
    iput-object v2, v8, LX/9Iw;->b:Ljava/lang/CharSequence;

    .line 1455955
    invoke-virtual {v8}, LX/9Iw;->a()LX/9Iy;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->setFacepileModel(LX/9Iy;)V

    .line 1455956
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;

    .line 1455957
    if-lez v4, :cond_6

    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->setBottomDividerVisibility(I)V

    .line 1455958
    goto/16 :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080fc6

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1455959
    :cond_6
    const/16 v1, 0x8

    goto :goto_3
.end method

.method private static a(Lcom/facebook/feedback/ui/FeedbackHeaderView;LX/14w;LX/1za;LX/9EK;LX/0ad;LX/0hL;)V
    .locals 0

    .prologue
    .line 1456000
    iput-object p1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a:LX/14w;

    iput-object p2, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->b:LX/1za;

    iput-object p3, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->c:LX/9EK;

    iput-object p4, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->d:LX/0ad;

    iput-object p5, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->e:LX/0hL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-static {v5}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v1

    check-cast v1, LX/14w;

    invoke-static {v5}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v2

    check-cast v2, LX/1za;

    invoke-static {v5}, LX/9EK;->a(LX/0QB;)LX/9EK;

    move-result-object v3

    check-cast v3, LX/9EK;

    invoke-static {v5}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v5}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v5

    check-cast v5, LX/0hL;

    invoke-static/range {v0 .. v5}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a(Lcom/facebook/feedback/ui/FeedbackHeaderView;LX/14w;LX/1za;LX/9EK;LX/0ad;LX/0hL;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1455990
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a:LX/14w;

    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v2}, LX/14w;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0am;

    move-result-object v2

    .line 1455991
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1455992
    :goto_0
    if-eqz v0, :cond_2

    .line 1455993
    invoke-direct {p0, v2}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setActorText(LX/0am;)V

    .line 1455994
    invoke-direct {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->e()V

    .line 1455995
    invoke-direct {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->g()V

    .line 1455996
    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setVisibility(I)V

    .line 1455997
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1455998
    goto :goto_0

    .line 1455999
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setVisibility(I)V

    goto :goto_1
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1455918
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->b:LX/1za;

    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;)V

    .line 1455919
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1455960
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1455961
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 1455962
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 1455963
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setRightArrowCompoundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1455964
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1455965
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1455966
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1455967
    :goto_0
    return-void

    .line 1455968
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1455969
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setActorText(LX/0am;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1455970
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1455971
    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2}, LX/16z;->r(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1455972
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080fc6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1455973
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1455974
    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1455975
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 1455976
    goto :goto_0

    .line 1455977
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1455978
    const v1, 0x7f0219d6

    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->setBackgroundResource(I)V

    .line 1455979
    invoke-direct {p0}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f()V

    .line 1455980
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private setRightArrowCompoundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1455981
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->e:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455982
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1455983
    :goto_0
    return-void

    .line 1455984
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, p1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1455985
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/FeedbackHeaderView;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-void
.end method

.method public setListener(LX/9Df;)V
    .locals 0

    .prologue
    .line 1455986
    iput-object p1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->k:LX/9Df;

    .line 1455987
    return-void
.end method

.method public setProfileVideoViewCount(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1455988
    iput-object p1, p0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->l:Ljava/lang/String;

    .line 1455989
    return-void
.end method
