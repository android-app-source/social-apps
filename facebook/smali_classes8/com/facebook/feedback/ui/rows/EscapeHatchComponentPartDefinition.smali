.class public Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/9Gu;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9IU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1460027
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1460028
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1460029
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->d:LX/0Ot;

    .line 1460030
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1460031
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->e:LX/0Ot;

    .line 1460032
    return-void
.end method

.method private a(LX/1De;LX/9Gu;LX/9FA;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/9Gu;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1460033
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9IU;

    const/4 v1, 0x0

    .line 1460034
    new-instance v2, LX/9IT;

    invoke-direct {v2, v0}, LX/9IT;-><init>(LX/9IU;)V

    .line 1460035
    sget-object p0, LX/9IU;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9IS;

    .line 1460036
    if-nez p0, :cond_0

    .line 1460037
    new-instance p0, LX/9IS;

    invoke-direct {p0}, LX/9IS;-><init>()V

    .line 1460038
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/9IS;->a$redex0(LX/9IS;LX/1De;IILX/9IT;)V

    .line 1460039
    move-object v2, p0

    .line 1460040
    move-object v1, v2

    .line 1460041
    move-object v1, v1

    .line 1460042
    iget-object v0, p2, LX/9Gu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1460043
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1460044
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460045
    iget-object v2, v1, LX/9IS;->a:LX/9IT;

    iput-object v0, v2, LX/9IT;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460046
    iget-object v2, v1, LX/9IS;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1460047
    move-object v0, v1

    .line 1460048
    iget-object v1, p2, LX/9Gu;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1460049
    iget-object v2, v0, LX/9IS;->a:LX/9IT;

    iput-object v1, v2, LX/9IT;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1460050
    iget-object v2, v0, LX/9IS;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1460051
    move-object v0, v0

    .line 1460052
    iget-object v1, p3, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 1460053
    iget-object v2, v0, LX/9IS;->a:LX/9IT;

    iput-object v1, v2, LX/9IT;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1460054
    iget-object v2, v0, LX/9IS;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1460055
    move-object v0, v0

    .line 1460056
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;
    .locals 5

    .prologue
    .line 1460057
    const-class v1, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;

    monitor-enter v1

    .line 1460058
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460059
    sput-object v2, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460060
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460061
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460062
    new-instance v4, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3}, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1460063
    const/16 v3, 0x1032

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x1ded

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1460064
    iput-object v3, v4, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->d:LX/0Ot;

    iput-object p0, v4, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->e:LX/0Ot;

    .line 1460065
    move-object v0, v4

    .line 1460066
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460067
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460068
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460069
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1460070
    check-cast p2, LX/9Gu;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->a(LX/1De;LX/9Gu;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1460071
    check-cast p2, LX/9Gu;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->a(LX/1De;LX/9Gu;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1460072
    check-cast p1, LX/9Gu;

    const/4 v1, 0x0

    .line 1460073
    invoke-static {p1}, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->a(LX/9Gu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0wn;->k:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1460074
    check-cast p1, LX/9Gu;

    .line 1460075
    iget-object v0, p1, LX/9Gu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
