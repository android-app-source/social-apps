.class public Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "LX/9Fh;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/3Wq;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field private final e:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

.field public final f:LX/3iM;

.field private final g:LX/9Hj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1458924
    const-class v0, LX/3Wq;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;LX/3iM;LX/9Hj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458925
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1458926
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1458927
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 1458928
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    .line 1458929
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->f:LX/3iM;

    .line 1458930
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->g:LX/9Hj;

    .line 1458931
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;
    .locals 9

    .prologue
    .line 1458932
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;

    monitor-enter v1

    .line 1458933
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1458934
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1458935
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1458936
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1458937
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    invoke-static {v0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v7

    check-cast v7, LX/3iM;

    invoke-static {v0}, LX/9Hj;->a(LX/0QB;)LX/9Hj;

    move-result-object v8

    check-cast v8, LX/9Hj;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;LX/3iM;LX/9Hj;)V

    .line 1458938
    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    .line 1458939
    iput-object v4, v3, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->a:Ljava/lang/Boolean;

    .line 1458940
    move-object v0, v3

    .line 1458941
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1458942
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458943
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1458944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1458945
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458946
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1458947
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1458948
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1458949
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->g:LX/9Hj;

    invoke-virtual {v2, v0}, LX/9Hj;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1458950
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9HA;->NO_OFFSET:LX/9HA;

    .line 1458951
    :goto_0
    const v3, 0x7f0d09cb

    iget-object v4, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1458952
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v3, LX/9GN;

    invoke-direct {v3, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1458953
    const v0, 0x7f0d2fe0

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    invoke-interface {p1, v0, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1458954
    new-instance v0, LX/9Fh;

    invoke-direct {v0, v1}, LX/9Fh;-><init>(Landroid/net/Uri;)V

    return-object v0

    .line 1458955
    :cond_0
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x45b8a0e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1458956
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/9Fh;

    check-cast p3, LX/9FA;

    check-cast p4, LX/3Wq;

    const/4 v4, 0x0

    .line 1458957
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1458958
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1458959
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {v2}, LX/33N;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    iget-object v5, p2, LX/9Fh;->a:Landroid/net/Uri;

    sget-object v6, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v2, v5, v6}, LX/3Wq;->a(Lcom/facebook/graphql/model/GraphQLProfile;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1458960
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->B()Z

    move-result v2

    invoke-virtual {p4, v2}, LX/3Wq;->setVerifiedBadgeVisibility(Z)V

    .line 1458961
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->D()Z

    move-result v2

    invoke-virtual {p4, v2}, LX/3Wq;->setPinnedIconVisibility(Z)V

    .line 1458962
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1458963
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->U()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->S()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    .line 1458964
    :goto_0
    iput-boolean v2, p4, LX/3Wq;->z:Z

    .line 1458965
    if-eqz v2, :cond_0

    .line 1458966
    invoke-static {p4}, LX/3Wq;->j(LX/3Wq;)Landroid/graphics/drawable/Drawable;

    .line 1458967
    :cond_0
    invoke-static {p4}, LX/3Wq;->g(LX/3Wq;)V

    .line 1458968
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x0

    .line 1458969
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1458970
    sget-object v5, LX/3Wq;->D:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_2

    .line 1458971
    iget-object v5, p4, LX/3Wq;->l:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0wM;

    const v6, 0x7f0208d6

    const p2, -0x413d37

    invoke-virtual {v5, v6, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    sput-object v5, LX/3Wq;->D:Landroid/graphics/drawable/Drawable;

    .line 1458972
    :cond_2
    iget-object v5, p4, LX/3Wq;->v:Lcom/facebook/resources/ui/FbTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1458973
    iget-object v5, p4, LX/3Wq;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1458974
    iget-object v5, p4, LX/3Wq;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1458975
    iget-object v5, p4, LX/3Wq;->v:Lcom/facebook/resources/ui/FbTextView;

    sget-object v6, LX/3Wq;->D:Landroid/graphics/drawable/Drawable;

    invoke-static {v5, v6, v7, v7, v7}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1458976
    :goto_1
    iget-object v2, p4, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v2}, Lcom/facebook/translation/ui/TranslatableTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1458977
    if-eqz v2, :cond_5

    const/16 v2, 0x30

    .line 1458978
    :goto_3
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1458979
    sget-object v2, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p1}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1458980
    iget-object v2, p3, LX/9FA;->m:LX/9FD;

    move-object v2, v2

    .line 1458981
    iget v5, v2, LX/9FD;->a:I

    move v5, v5

    .line 1458982
    iget-object v2, p3, LX/9FA;->m:LX/9FD;

    move-object v2, v2

    .line 1458983
    iget v6, v2, LX/9FD;->e:I

    move v2, v6

    .line 1458984
    :goto_4
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1458985
    invoke-virtual {p4}, LX/3Wq;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p4}, LX/3Wq;->getPaddingRight()I

    move-result v6

    invoke-virtual {p4, v2, v5, v6, v4}, LX/3Wq;->setPadding(IIII)V

    .line 1458986
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->f:LX/3iM;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v2, :cond_3

    .line 1458987
    const v1, 0x7f0d09cf

    invoke-static {p4, v1}, LX/1aP;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1458988
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1458989
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1458990
    :cond_3
    const/16 v1, 0x1f

    const v2, 0x514e1bb0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_4
    move v2, v4

    .line 1458991
    goto/16 :goto_0

    .line 1458992
    :cond_5
    const/16 v2, 0x10

    goto :goto_3

    .line 1458993
    :cond_6
    iget-object v2, p3, LX/9FA;->m:LX/9FD;

    move-object v2, v2

    .line 1458994
    iget v5, v2, LX/9FD;->b:I

    move v5, v5

    .line 1458995
    iget-object v2, p3, LX/9FA;->m:LX/9FD;

    move-object v2, v2

    .line 1458996
    iget v6, v2, LX/9FD;->f:I

    move v2, v6

    .line 1458997
    goto :goto_4

    .line 1458998
    :cond_7
    iget-object v5, p4, LX/3Wq;->v:Lcom/facebook/resources/ui/FbTextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method
