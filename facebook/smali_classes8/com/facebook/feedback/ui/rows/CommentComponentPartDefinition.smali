.class public Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Hg;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tH;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tF;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1DR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1458758
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1458759
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458760
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->d:LX/0Ot;

    .line 1458761
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458762
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->e:LX/0Ot;

    .line 1458763
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458764
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->f:LX/0Ot;

    .line 1458765
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458766
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->g:LX/0Ot;

    .line 1458767
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458768
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->h:LX/0Ot;

    .line 1458769
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1458734
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Hg;

    const/4 v1, 0x0

    .line 1458735
    new-instance v2, LX/9Hf;

    invoke-direct {v2, v0}, LX/9Hf;-><init>(LX/9Hg;)V

    .line 1458736
    iget-object p0, v0, LX/9Hg;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9He;

    .line 1458737
    if-nez p0, :cond_0

    .line 1458738
    new-instance p0, LX/9He;

    invoke-direct {p0, v0}, LX/9He;-><init>(LX/9Hg;)V

    .line 1458739
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/9He;->a$redex0(LX/9He;LX/1De;IILX/9Hf;)V

    .line 1458740
    move-object v2, p0

    .line 1458741
    move-object v1, v2

    .line 1458742
    move-object v0, v1

    .line 1458743
    iget-object v1, v0, LX/9He;->a:LX/9Hf;

    iput-object p2, v1, LX/9Hf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458744
    iget-object v1, v0, LX/9He;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1458745
    move-object v0, v0

    .line 1458746
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1458747
    iget-object v2, v0, LX/9He;->a:LX/9Hf;

    iput-object v1, v2, LX/9Hf;->b:LX/9FD;

    .line 1458748
    iget-object v2, v0, LX/9He;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1458749
    move-object v0, v0

    .line 1458750
    iget-object v1, p3, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 1458751
    iget-object v2, v0, LX/9He;->a:LX/9Hf;

    iput-object v1, v2, LX/9Hf;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458752
    iget-object v2, v0, LX/9He;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1458753
    move-object v0, v0

    .line 1458754
    iget-object v1, v0, LX/9He;->a:LX/9Hf;

    iput-object p3, v1, LX/9Hf;->d:LX/1Pr;

    .line 1458755
    iget-object v1, v0, LX/9He;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1458756
    move-object v0, v0

    .line 1458757
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;
    .locals 8

    .prologue
    .line 1458721
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    monitor-enter v1

    .line 1458722
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1458723
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1458724
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1458725
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1458726
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1458727
    const/16 v4, 0x1dd6

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1032

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x79f

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x79a

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x78b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1458728
    iput-object v4, v3, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->d:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->e:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->f:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->g:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->h:LX/0Ot;

    .line 1458729
    move-object v0, v3

    .line 1458730
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1458731
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458732
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1458733
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1458714
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1458715
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f0105a6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    .line 1458716
    if-nez v0, :cond_0

    .line 1458717
    const/4 v0, -0x1

    .line 1458718
    :goto_0
    return v0

    .line 1458719
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 1458720
    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1458713
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1458691
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 1458694
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458695
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tH;

    invoke-virtual {v0}, LX/0tH;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    const/4 v2, 0x0

    .line 1458696
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1458697
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1458698
    const/4 v1, 0x1

    .line 1458699
    :goto_0
    move v0, v1

    .line 1458700
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1458701
    :cond_1
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1458702
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1458703
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1458704
    sget-object p0, LX/9Hm;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ordinal()I

    move-result v1

    aget v1, p0, v1

    packed-switch v1, :pswitch_data_0

    .line 1458705
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1458706
    :pswitch_0
    sget-short v1, LX/0wn;->g:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0

    .line 1458707
    :pswitch_1
    sget-short v1, LX/0wn;->h:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0

    .line 1458708
    :pswitch_2
    sget-short v1, LX/0wn;->i:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0

    .line 1458709
    :pswitch_3
    sget-short v1, LX/0wn;->j:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0

    :pswitch_4
    move v1, v2

    .line 1458710
    goto :goto_0

    .line 1458711
    :pswitch_5
    sget-short v1, LX/0wn;->f:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1458712
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1458692
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458693
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
