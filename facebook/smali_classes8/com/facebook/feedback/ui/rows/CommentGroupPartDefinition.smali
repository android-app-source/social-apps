.class public Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "LX/9Fg;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

.field private final b:Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

.field private final e:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

.field private final f:LX/9CG;

.field public final g:LX/8sB;

.field public final h:Landroid/content/Context;

.field public final i:LX/3iR;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;LX/9CG;LX/8sB;LX/3iR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458910
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1458911
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->h:Landroid/content/Context;

    .line 1458912
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

    .line 1458913
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;

    .line 1458914
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

    .line 1458915
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

    .line 1458916
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    .line 1458917
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->f:LX/9CG;

    .line 1458918
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->g:LX/8sB;

    .line 1458919
    iput-object p9, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->i:LX/3iR;

    .line 1458920
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;
    .locals 13

    .prologue
    .line 1458899
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    monitor-enter v1

    .line 1458900
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1458901
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1458902
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1458903
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1458904
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    invoke-static {v0}, LX/9CG;->a(LX/0QB;)LX/9CG;

    move-result-object v10

    check-cast v10, LX/9CG;

    invoke-static {v0}, LX/8sB;->b(LX/0QB;)LX/8sB;

    move-result-object v11

    check-cast v11, LX/8sB;

    invoke-static {v0}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v12

    check-cast v12, LX/3iR;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;LX/9CG;LX/8sB;LX/3iR;)V

    .line 1458905
    move-object v0, v3

    .line 1458906
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1458907
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458908
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1458909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1458892
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    .line 1458893
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1458894
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentAttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1458895
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

    .line 1458896
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1458897
    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1458898
    new-instance v0, LX/9Fg;

    invoke-direct {v0, p0, p2, p3}, LX/9Fg;-><init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 4

    .prologue
    .line 1458873
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/9Fg;

    check-cast p3, LX/9FA;

    .line 1458874
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    .line 1458875
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1458876
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1458877
    iget-object v2, p3, LX/9FA;->f:LX/9Ce;

    move-object p4, v2

    .line 1458878
    if-nez p4, :cond_1

    .line 1458879
    const/4 v2, 0x0

    .line 1458880
    :goto_0
    move v2, v2

    .line 1458881
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->g:LX/8sB;

    invoke-virtual {v2, v0}, LX/8sB;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1458882
    iget-object v0, p2, LX/9Fg;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1458883
    invoke-virtual {v1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1458884
    invoke-virtual {v1, p2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1458885
    :cond_0
    return-void

    .line 1458886
    :cond_1
    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1458887
    check-cast v2, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1458888
    invoke-static {p1}, LX/5Op;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    .line 1458889
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    .line 1458890
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2, v3}, LX/9Ce;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    .line 1458891
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1458872
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 2

    .prologue
    .line 1458865
    check-cast p2, LX/9Fg;

    const/4 v1, 0x0

    .line 1458866
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    .line 1458867
    iget-object p0, p2, LX/9Fg;->e:Ljava/util/List;

    invoke-interface {p0, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1458868
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1458869
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1458870
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/9CG;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1458871
    return-void
.end method
