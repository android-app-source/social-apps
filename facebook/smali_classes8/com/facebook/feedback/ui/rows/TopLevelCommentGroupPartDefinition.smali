.class public Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9Gv;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

.field private final b:Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460441
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460442
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    .line 1460443
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    .line 1460444
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    .line 1460445
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->d:Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;

    .line 1460446
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;
    .locals 7

    .prologue
    .line 1460447
    const-class v1, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;

    monitor-enter v1

    .line 1460448
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460449
    sput-object v2, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460450
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460451
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460452
    new-instance p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;)V

    .line 1460453
    move-object v0, p0

    .line 1460454
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460455
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460456
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460457
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1460458
    check-cast p2, LX/9Gv;

    .line 1460459
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    iget-object v1, p2, LX/9Gv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    iget-object v2, p2, LX/9Gv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1460460
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->d:Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;

    new-instance v1, LX/9Gj;

    iget-object v2, p2, LX/9Gv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/9Gv;->d:LX/21y;

    invoke-direct {v1, v2, v3}, LX/9Gj;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21y;)V

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    new-instance v2, LX/9Gt;

    iget-object v3, p2, LX/9Gv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9Gv;->d:LX/21y;

    invoke-direct {v2, v3, v4}, LX/9Gt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21y;)V

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1460461
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460462
    const/4 v0, 0x1

    return v0
.end method
