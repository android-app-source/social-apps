.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9GG;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/8xz;

.field public final c:LX/189;

.field public final d:LX/20j;

.field public final e:LX/0bH;

.field public final f:LX/1K9;

.field public final g:LX/0kL;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xz;LX/189;LX/20j;LX/0bH;LX/1K9;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459666
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1459667
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459668
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->b:LX/8xz;

    .line 1459669
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->c:LX/189;

    .line 1459670
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->d:LX/20j;

    .line 1459671
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->e:LX/0bH;

    .line 1459672
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->f:LX/1K9;

    .line 1459673
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->g:LX/0kL;

    .line 1459674
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;
    .locals 11

    .prologue
    .line 1459644
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    monitor-enter v1

    .line 1459645
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459646
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459647
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459648
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459649
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459650
    new-instance v8, LX/8xz;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v6

    check-cast v6, LX/0sa;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-direct {v8, v5, v6, v7}, LX/8xz;-><init>(LX/0tX;LX/0sa;LX/1Ck;)V

    .line 1459651
    move-object v5, v8

    .line 1459652
    check-cast v5, LX/8xz;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v6

    check-cast v6, LX/189;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v7

    check-cast v7, LX/20j;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v9

    check-cast v9, LX/1K9;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xz;LX/189;LX/20j;LX/0bH;LX/1K9;LX/0kL;)V

    .line 1459653
    move-object v0, v3

    .line 1459654
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459655
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459656
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459657
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1459658
    check-cast p2, LX/9GG;

    check-cast p3, LX/9FA;

    .line 1459659
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459660
    iget-object v2, p2, LX/9GG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    .line 1459661
    iget-boolean v2, p2, LX/9GG;->c:Z

    if-eqz v2, :cond_0

    const-string v6, "CONFIRMED_PLACE"

    .line 1459662
    :goto_0
    new-instance v2, LX/9GF;

    move-object v3, p0

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, LX/9GF;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;LX/9GG;Ljava/lang/String;Ljava/lang/String;LX/9FA;)V

    move-object v1, v2

    .line 1459663
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459664
    const/4 v0, 0x0

    return-object v0

    .line 1459665
    :cond_0
    const-string v6, "PENDING_PLACE"

    goto :goto_0
.end method
