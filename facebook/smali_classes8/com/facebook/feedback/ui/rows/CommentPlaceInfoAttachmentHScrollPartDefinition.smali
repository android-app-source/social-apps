.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9G6;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

.field public final c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

.field public final d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

.field public final e:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

.field public final f:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

.field public final g:Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

.field public final h:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

.field public final i:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

.field private final j:LX/9D3;

.field private final k:LX/2dq;

.field public final l:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;LX/9D3;LX/2dq;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459393
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1459394
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 1459395
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    .line 1459396
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    .line 1459397
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

    .line 1459398
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    .line 1459399
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    .line 1459400
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->g:Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    .line 1459401
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->h:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    .line 1459402
    iput-object p9, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->i:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    .line 1459403
    iput-object p10, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->j:LX/9D3;

    .line 1459404
    iput-object p11, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->k:LX/2dq;

    .line 1459405
    iput-object p12, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->l:LX/0ad;

    .line 1459406
    return-void
.end method

.method public static a(Z)I
    .locals 1
    .annotation build Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition$AttachmentStylingTypes;
    .end annotation

    .prologue
    .line 1459442
    if-eqz p0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(LX/9G6;Lcom/facebook/graphql/model/GraphQLComment;)LX/2eJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9G6;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1459434
    iget-object v0, p1, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1459435
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1459436
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1459437
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v2

    .line 1459438
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v5

    .line 1459439
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v4

    .line 1459440
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v6

    .line 1459441
    new-instance v0, LX/9G5;

    move-object v1, p0

    move-object v3, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/9G5;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;LX/0Px;LX/9G6;LX/0Px;LX/0Px;LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;
    .locals 3

    .prologue
    .line 1459426
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    monitor-enter v1

    .line 1459427
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459428
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459429
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459430
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->b(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459431
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459432
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;
    .locals 15

    .prologue
    .line 1459421
    new-instance v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    .line 1459422
    new-instance v14, LX/9D3;

    const-class v10, Landroid/content/Context;

    invoke-interface {p0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    invoke-static {p0}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v12

    check-cast v12, LX/0gy;

    invoke-static {p0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-direct {v14, v10, v11, v12, v13}, LX/9D3;-><init>(Landroid/content/Context;LX/0hB;LX/0gy;Ljava/lang/Boolean;)V

    .line 1459423
    move-object v10, v14

    .line 1459424
    check-cast v10, LX/9D3;

    invoke-static {p0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v11

    check-cast v11, LX/2dq;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;LX/9D3;LX/2dq;LX/0ad;)V

    .line 1459425
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1459420
    sget-object v0, LX/2eA;->d:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1459408
    check-cast p2, LX/9G6;

    check-cast p3, LX/9FA;

    const/4 v7, 0x0

    .line 1459409
    iget-object v0, p2, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 1459410
    if-nez v5, :cond_0

    .line 1459411
    :goto_0
    return-object v7

    .line 1459412
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->j:LX/9D3;

    iget-object v1, p2, LX/9G6;->c:LX/9HA;

    .line 1459413
    iget-object v2, p3, LX/9FA;->m:LX/9FD;

    move-object v2, v2

    .line 1459414
    iget-object v3, v0, LX/9D3;->d:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/9D3;->c:LX/0gy;

    invoke-virtual {v3}, LX/0gy;->c()I

    move-result v3

    .line 1459415
    :goto_1
    iget-object v4, v0, LX/9D3;->a:Landroid/content/Context;

    int-to-float v3, v3

    invoke-static {v4, v3}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    .line 1459416
    iget-object v4, v0, LX/9D3;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, LX/9FD;->a(LX/9HA;)I

    move-result v6

    int-to-float v6, v6

    invoke-static {v4, v6}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v4

    int-to-float v4, v4

    .line 1459417
    sub-float/2addr v3, v4

    const v4, 0x3f6147ae    # 0.88f

    mul-float/2addr v3, v4

    move v1, v3

    .line 1459418
    iget-object v6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->k:LX/2dq;

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, p2, v5}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(LX/9G6;Lcom/facebook/graphql/model/GraphQLComment;)LX/2eJ;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 1459419
    :cond_1
    iget-object v3, v0, LX/9D3;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459407
    const/4 v0, 0x1

    return v0
.end method
