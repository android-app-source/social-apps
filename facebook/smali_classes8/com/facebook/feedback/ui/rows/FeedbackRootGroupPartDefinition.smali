.class public Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9Gk;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/TopLevelCommentGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460076
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460077
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, LX/9Gv;

    invoke-virtual {v0, v1, p3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/9Gp;

    invoke-virtual {v0, v1, p2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/9Gl;

    invoke-virtual {v0, v1, p1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;->a:LX/1T5;

    .line 1460078
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;
    .locals 6

    .prologue
    .line 1460079
    const-class v1, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;

    monitor-enter v1

    .line 1460080
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460081
    sput-object v2, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460084
    new-instance v3, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;

    const/16 v4, 0x1dc7

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1dc9

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1dcc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1460085
    move-object v0, v3

    .line 1460086
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460087
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460088
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1460090
    check-cast p2, LX/9Gk;

    .line 1460091
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 1460092
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460093
    const/4 v0, 0x1

    return v0
.end method
