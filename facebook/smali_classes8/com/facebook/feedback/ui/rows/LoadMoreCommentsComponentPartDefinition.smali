.class public Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/9Gl;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/9IY;

.field private final e:LX/9HW;

.field private final f:LX/0ad;

.field private final g:LX/0kb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9IY;LX/9HW;LX/0ad;LX/0kb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1460202
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1460203
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->d:LX/9IY;

    .line 1460204
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->e:LX/9HW;

    .line 1460205
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->f:LX/0ad;

    .line 1460206
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->g:LX/0kb;

    .line 1460207
    return-void
.end method

.method private a(LX/1De;LX/9Gl;LX/9FA;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/9Gl;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1460208
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->d:LX/9IY;

    const/4 v1, 0x0

    .line 1460209
    new-instance v2, LX/9IX;

    invoke-direct {v2, v0}, LX/9IX;-><init>(LX/9IY;)V

    .line 1460210
    sget-object v3, LX/9IY;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9IW;

    .line 1460211
    if-nez v3, :cond_0

    .line 1460212
    new-instance v3, LX/9IW;

    invoke-direct {v3}, LX/9IW;-><init>()V

    .line 1460213
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/9IW;->a$redex0(LX/9IW;LX/1De;IILX/9IX;)V

    .line 1460214
    move-object v2, v3

    .line 1460215
    move-object v1, v2

    .line 1460216
    move-object v0, v1

    .line 1460217
    iget-object v1, p2, LX/9Gl;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1460218
    iget-object v2, v0, LX/9IW;->a:LX/9IX;

    iput-object v1, v2, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1460219
    iget-object v2, v0, LX/9IW;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460220
    move-object v0, v0

    .line 1460221
    iget-object v1, p2, LX/9Gl;->b:LX/5Gs;

    .line 1460222
    iget-object v2, v0, LX/9IW;->a:LX/9IX;

    iput-object v1, v2, LX/9IX;->b:LX/5Gs;

    .line 1460223
    iget-object v2, v0, LX/9IW;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460224
    move-object v0, v0

    .line 1460225
    iget-object v1, p2, LX/9Gl;->c:LX/9Fi;

    .line 1460226
    iget-object v2, v0, LX/9IW;->a:LX/9IX;

    iput-object v1, v2, LX/9IX;->f:LX/9Fi;

    .line 1460227
    iget-object v2, v0, LX/9IW;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460228
    move-object v0, v0

    .line 1460229
    iget v1, p2, LX/9Gl;->e:I

    .line 1460230
    iget-object v2, v0, LX/9IW;->a:LX/9IX;

    iput v1, v2, LX/9IX;->c:I

    .line 1460231
    move-object v0, v0

    .line 1460232
    iget-object v1, p2, LX/9Gl;->d:Ljava/lang/String;

    .line 1460233
    iget-object v2, v0, LX/9IW;->a:LX/9IX;

    iput-object v1, v2, LX/9IX;->d:Ljava/lang/String;

    .line 1460234
    move-object v0, v0

    .line 1460235
    iget-object v1, v0, LX/9IW;->a:LX/9IX;

    iput-object p3, v1, LX/9IX;->e:LX/9FA;

    .line 1460236
    iget-object v1, v0, LX/9IW;->d:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1460237
    move-object v0, v0

    .line 1460238
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1460239
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->e:LX/9HW;

    invoke-virtual {v1, p1}, LX/9HW;->c(LX/1De;)LX/9HU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/9HU;->a(LX/1X1;)LX/9HU;

    move-result-object v0

    .line 1460240
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1460241
    invoke-virtual {v0, v1}, LX/9HU;->a(LX/9FD;)LX/9HU;

    move-result-object v1

    iget-object v0, p2, LX/9Gl;->c:LX/9Fi;

    sget-object v2, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-virtual {v0, v2}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/9HA;->NO_OFFSET:LX/9HA;

    :goto_0
    invoke-virtual {v1, v0}, LX/9HU;->a(LX/9HA;)LX/9HU;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;
    .locals 9

    .prologue
    .line 1460257
    const-class v1, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    monitor-enter v1

    .line 1460258
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460259
    sput-object v2, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460260
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460261
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460262
    new-instance v3, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/9IY;->a(LX/0QB;)LX/9IY;

    move-result-object v5

    check-cast v5, LX/9IY;

    invoke-static {v0}, LX/9HW;->a(LX/0QB;)LX/9HW;

    move-result-object v6

    check-cast v6, LX/9HW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;-><init>(Landroid/content/Context;LX/9IY;LX/9HW;LX/0ad;LX/0kb;)V

    .line 1460263
    move-object v0, v3

    .line 1460264
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460265
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460266
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/9Gl;LX/1dV;LX/9FA;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 6

    .prologue
    .line 1460242
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1460243
    iget-object v0, p1, LX/9Gl;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p1, LX/9Gl;->b:LX/5Gs;

    iget-object v2, p1, LX/9Gl;->c:LX/9Fi;

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->g:LX/0kb;

    const/4 v5, 0x0

    .line 1460244
    sget-object v4, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-static {v0}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object p2

    invoke-virtual {v4, p2}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    .line 1460245
    :goto_0
    if-ne v4, v1, :cond_4

    .line 1460246
    sget-object v4, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    invoke-virtual {v1, v4}, LX/5Gs;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    .line 1460247
    :goto_1
    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-static {v0, v4}, LX/9Ii;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-virtual {v2, v4}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    .line 1460248
    :goto_2
    move v0, v4

    .line 1460249
    if-eqz v0, :cond_0

    .line 1460250
    new-instance v0, LX/9Gm;

    iget-object v1, p1, LX/9Gl;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p1, LX/9Gl;->b:LX/5Gs;

    invoke-direct {v0, v1, v2}, LX/9Gm;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)V

    new-instance v1, LX/4Vj;

    iget-object v2, p1, LX/9Gl;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4Vj;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Gn;

    .line 1460251
    new-instance v1, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition$1;

    invoke-direct {v1, p0, p1, p3, v0}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition$1;-><init>(Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;LX/9Gl;LX/9FA;LX/9Gn;)V

    invoke-virtual {p4, v1}, Lcom/facebook/components/feed/FeedComponentView;->post(Ljava/lang/Runnable;)Z

    .line 1460252
    :cond_0
    return-void

    .line 1460253
    :cond_1
    sget-object v4, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    goto :goto_0

    .line 1460254
    :cond_2
    sget-object v4, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    goto :goto_1

    :cond_3
    move v4, v5

    .line 1460255
    goto :goto_2

    :cond_4
    move v4, v5

    .line 1460256
    goto :goto_2
.end method

.method public static a(LX/9Gl;)Z
    .locals 2

    .prologue
    .line 1460200
    iget-object v0, p0, LX/9Gl;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, LX/9Gl;->b:LX/5Gs;

    invoke-static {v0, v1}, LX/9Ii;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1460201
    check-cast p2, LX/9Gl;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/1De;LX/9Gl;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1460199
    check-cast p2, LX/9Gl;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/1De;LX/9Gl;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 1460198
    check-cast p1, LX/9Gl;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/9Gl;LX/1dV;LX/9FA;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x44a93016

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1460197
    check-cast p1, LX/9Gl;

    check-cast p2, LX/1dV;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/9Gl;LX/1dV;LX/9FA;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x6ee55761

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460196
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 2

    .prologue
    .line 1460194
    check-cast p1, LX/9Gl;

    .line 1460195
    new-instance v0, LX/4Vj;

    iget-object v1, p1, LX/9Gl;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4Vj;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
