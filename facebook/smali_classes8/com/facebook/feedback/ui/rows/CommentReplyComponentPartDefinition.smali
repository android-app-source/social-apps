.class public Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/9GM;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/9IA;

.field private final f:LX/9HW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1459812
    const-class v0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/9IA;LX/9HW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459808
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1459809
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->e:LX/9IA;

    .line 1459810
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->f:LX/9HW;

    .line 1459811
    return-void
.end method

.method private a(LX/1De;LX/9GM;LX/9FA;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/9GM;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1459785
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->e:LX/9IA;

    const/4 v1, 0x0

    .line 1459786
    new-instance v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    invoke-direct {v2, v0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;-><init>(LX/9IA;)V

    .line 1459787
    sget-object v3, LX/9IA;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9I9;

    .line 1459788
    if-nez v3, :cond_0

    .line 1459789
    new-instance v3, LX/9I9;

    invoke-direct {v3}, LX/9I9;-><init>()V

    .line 1459790
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/9I9;->a$redex0(LX/9I9;LX/1De;IILcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;)V

    .line 1459791
    move-object v2, v3

    .line 1459792
    move-object v1, v2

    .line 1459793
    move-object v0, v1

    .line 1459794
    iget-object v1, v0, LX/9I9;->a:Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    iput-object p2, v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    .line 1459795
    iget-object v1, v0, LX/9I9;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1459796
    move-object v0, v0

    .line 1459797
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1459798
    iget-object v2, v0, LX/9I9;->a:Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->b:LX/9FD;

    .line 1459799
    iget-object v2, v0, LX/9I9;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1459800
    move-object v0, v0

    .line 1459801
    sget-object v1, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1459802
    iget-object v2, v0, LX/9I9;->a:Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1459803
    move-object v0, v0

    .line 1459804
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1459805
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->f:LX/9HW;

    invoke-virtual {v1, p1}, LX/9HW;->c(LX/1De;)LX/9HU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/9HU;->a(LX/1X1;)LX/9HU;

    move-result-object v0

    .line 1459806
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1459807
    invoke-virtual {v0, v1}, LX/9HU;->a(LX/9FD;)LX/9HU;

    move-result-object v0

    sget-object v1, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    invoke-virtual {v0, v1}, LX/9HU;->a(LX/9HA;)LX/9HU;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;
    .locals 6

    .prologue
    .line 1459813
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    monitor-enter v1

    .line 1459814
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459815
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459818
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/9IA;->a(LX/0QB;)LX/9IA;

    move-result-object v4

    check-cast v4, LX/9IA;

    invoke-static {v0}, LX/9HW;->a(LX/0QB;)LX/9HW;

    move-result-object v5

    check-cast v5, LX/9HW;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;-><init>(Landroid/content/Context;LX/9IA;LX/9HW;)V

    .line 1459819
    move-object v0, p0

    .line 1459820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1459784
    check-cast p2, LX/9GM;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->a(LX/1De;LX/9GM;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1459783
    check-cast p2, LX/9GM;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->a(LX/1De;LX/9GM;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459780
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1459781
    check-cast p1, LX/9GM;

    .line 1459782
    iget-object v0, p1, LX/9GM;->a:Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method
