.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9GJ;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/8y0;

.field public final c:LX/20j;

.field public final d:LX/189;

.field public final e:LX/0bH;

.field public final f:LX/1K9;

.field public final g:LX/0kL;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8y0;LX/20j;LX/189;LX/0bH;LX/1K9;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459730
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1459731
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459732
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->b:LX/8y0;

    .line 1459733
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->c:LX/20j;

    .line 1459734
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->d:LX/189;

    .line 1459735
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->e:LX/0bH;

    .line 1459736
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->f:LX/1K9;

    .line 1459737
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->g:LX/0kL;

    .line 1459738
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;
    .locals 11

    .prologue
    .line 1459719
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    monitor-enter v1

    .line 1459720
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459721
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459722
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459723
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459724
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/8y0;->b(LX/0QB;)LX/8y0;

    move-result-object v5

    check-cast v5, LX/8y0;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v6

    check-cast v6, LX/20j;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v9

    check-cast v9, LX/1K9;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8y0;LX/20j;LX/189;LX/0bH;LX/1K9;LX/0kL;)V

    .line 1459725
    move-object v0, v3

    .line 1459726
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459727
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459728
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459729
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1459714
    check-cast p2, LX/9GJ;

    check-cast p3, LX/9FA;

    .line 1459715
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459716
    new-instance v1, LX/9GI;

    invoke-direct {v1, p0, p2, p3}, LX/9GI;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;LX/9GJ;LX/9FA;)V

    move-object v1, v1

    .line 1459717
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459718
    const/4 v0, 0x0

    return-object v0
.end method
