.class public Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/9Gw;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/9Ig;

.field private final e:LX/9HW;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9Ig;LX/9HW;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1460508
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1460509
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->d:LX/9Ig;

    .line 1460510
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->e:LX/9HW;

    .line 1460511
    return-void
.end method

.method private a(LX/1De;LX/9Gw;LX/9FA;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/9Gw;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1460512
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->d:LX/9Ig;

    const/4 v1, 0x0

    .line 1460513
    new-instance v2, LX/9If;

    invoke-direct {v2, v0}, LX/9If;-><init>(LX/9Ig;)V

    .line 1460514
    sget-object v3, LX/9Ig;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9Ie;

    .line 1460515
    if-nez v3, :cond_0

    .line 1460516
    new-instance v3, LX/9Ie;

    invoke-direct {v3}, LX/9Ie;-><init>()V

    .line 1460517
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/9Ie;->a$redex0(LX/9Ie;LX/1De;IILX/9If;)V

    .line 1460518
    move-object v2, v3

    .line 1460519
    move-object v1, v2

    .line 1460520
    move-object v0, v1

    .line 1460521
    iget-object v1, p2, LX/9Gw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460522
    iget-object v2, v0, LX/9Ie;->a:LX/9If;

    iput-object v1, v2, LX/9If;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460523
    iget-object v2, v0, LX/9Ie;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460524
    move-object v0, v0

    .line 1460525
    iget-object v1, p2, LX/9Gw;->b:LX/0Px;

    .line 1460526
    iget-object v2, v0, LX/9Ie;->a:LX/9If;

    iput-object v1, v2, LX/9If;->b:LX/0Px;

    .line 1460527
    iget-object v2, v0, LX/9Ie;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460528
    move-object v0, v0

    .line 1460529
    iget-boolean v1, p2, LX/9Gw;->c:Z

    .line 1460530
    iget-object v2, v0, LX/9Ie;->a:LX/9If;

    iput-boolean v1, v2, LX/9If;->c:Z

    .line 1460531
    iget-object v2, v0, LX/9Ie;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460532
    move-object v0, v0

    .line 1460533
    iget-object v1, p2, LX/9Gw;->d:Landroid/view/View$OnClickListener;

    .line 1460534
    iget-object v2, v0, LX/9Ie;->a:LX/9If;

    iput-object v1, v2, LX/9If;->d:Landroid/view/View$OnClickListener;

    .line 1460535
    iget-object v2, v0, LX/9Ie;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1460536
    move-object v0, v0

    .line 1460537
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1460538
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->e:LX/9HW;

    invoke-virtual {v1, p1}, LX/9HW;->c(LX/1De;)LX/9HU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/9HU;->a(LX/1X1;)LX/9HU;

    move-result-object v0

    .line 1460539
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1460540
    invoke-virtual {v0, v1}, LX/9HU;->a(LX/9FD;)LX/9HU;

    move-result-object v0

    sget-object v1, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    invoke-virtual {v0, v1}, LX/9HU;->a(LX/9HA;)LX/9HU;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;
    .locals 6

    .prologue
    .line 1460496
    const-class v1, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

    monitor-enter v1

    .line 1460497
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460498
    sput-object v2, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460499
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460500
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460501
    new-instance p0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/9Ig;->a(LX/0QB;)LX/9Ig;

    move-result-object v4

    check-cast v4, LX/9Ig;

    invoke-static {v0}, LX/9HW;->a(LX/0QB;)LX/9HW;

    move-result-object v5

    check-cast v5, LX/9HW;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;-><init>(Landroid/content/Context;LX/9Ig;LX/9HW;)V

    .line 1460502
    move-object v0, p0

    .line 1460503
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460504
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460505
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1460507
    check-cast p2, LX/9Gw;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->a(LX/1De;LX/9Gw;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1460495
    check-cast p2, LX/9Gw;

    check-cast p3, LX/9FA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->a(LX/1De;LX/9Gw;LX/9FA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1460489
    check-cast p1, LX/9Gw;

    const/4 v0, 0x0

    .line 1460490
    iget-object v1, p1, LX/9Gw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1460491
    if-nez v1, :cond_1

    .line 1460492
    :cond_0
    :goto_0
    return v0

    .line 1460493
    :cond_1
    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 1460494
    iget-object v2, p1, LX/9Gw;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1460487
    check-cast p1, LX/9Gw;

    .line 1460488
    iget-object v0, p1, LX/9Gw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method
