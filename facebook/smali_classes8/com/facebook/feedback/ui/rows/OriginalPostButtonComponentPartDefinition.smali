.class public Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/9Gp;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public d:Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/9EG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/0ad;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1460308
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1460309
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->f:LX/0ad;

    .line 1460310
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->f:LX/0ad;

    sget-short v1, LX/0wn;->aA:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->g:Z

    .line 1460311
    return-void
.end method

.method private a(LX/1De;LX/9Gp;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/9Gp;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1460294
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;

    const/4 v1, 0x0

    .line 1460295
    new-instance v2, LX/9Ic;

    invoke-direct {v2, v0}, LX/9Ic;-><init>(Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;)V

    .line 1460296
    sget-object p0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9Ib;

    .line 1460297
    if-nez p0, :cond_0

    .line 1460298
    new-instance p0, LX/9Ib;

    invoke-direct {p0}, LX/9Ib;-><init>()V

    .line 1460299
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/9Ib;->a$redex0(LX/9Ib;LX/1De;IILX/9Ic;)V

    .line 1460300
    move-object v2, p0

    .line 1460301
    move-object v1, v2

    .line 1460302
    move-object v0, v1

    .line 1460303
    iget-object v1, p2, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1460304
    iget-object v2, v0, LX/9Ib;->a:LX/9Ic;

    iput-object v1, v2, LX/9Ic;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1460305
    iget-object v2, v0, LX/9Ib;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1460306
    move-object v0, v0

    .line 1460307
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;
    .locals 5

    .prologue
    .line 1460281
    const-class v1, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;

    monitor-enter v1

    .line 1460282
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460283
    sput-object v2, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460284
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460285
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460286
    new-instance p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;)V

    .line 1460287
    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;

    invoke-static {v0}, LX/9EG;->a(LX/0QB;)LX/9EG;

    move-result-object v4

    check-cast v4, LX/9EG;

    .line 1460288
    iput-object v3, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;

    iput-object v4, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->e:LX/9EG;

    .line 1460289
    move-object v0, p0

    .line 1460290
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460291
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460292
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1460280
    check-cast p2, LX/9Gp;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->a(LX/1De;LX/9Gp;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1460279
    check-cast p2, LX/9Gp;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->a(LX/1De;LX/9Gp;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1460275
    check-cast p1, LX/9Gp;

    .line 1460276
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->e:LX/9EG;

    iget-object v1, p1, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/9EG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1460277
    check-cast p1, LX/9Gp;

    .line 1460278
    iget-object v0, p1, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
