.class public Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9Gt;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

.field private final b:Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

.field public final d:LX/3iR;

.field private final e:LX/0tF;

.field private final f:LX/0ad;

.field private final g:LX/9FH;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;LX/3iR;LX/0tF;LX/0ad;LX/9FH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460360
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460361
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    .line 1460362
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    .line 1460363
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

    .line 1460364
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->d:LX/3iR;

    .line 1460365
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->e:LX/0tF;

    .line 1460366
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->f:LX/0ad;

    .line 1460367
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->g:LX/9FH;

    .line 1460368
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;
    .locals 11

    .prologue
    .line 1460369
    const-class v1, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    monitor-enter v1

    .line 1460370
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460371
    sput-object v2, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460372
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460373
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460374
    new-instance v3, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

    invoke-static {v0}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v7

    check-cast v7, LX/3iR;

    invoke-static {v0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v8

    check-cast v8, LX/0tF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const-class v10, LX/9FH;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/9FH;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;LX/3iR;LX/0tF;LX/0ad;LX/9FH;)V

    .line 1460375
    move-object v0, v3

    .line 1460376
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460377
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460378
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460379
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/9Gt;)Z
    .locals 2

    .prologue
    .line 1460380
    iget-object v0, p0, LX/9Gt;->d:LX/21y;

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v0}, LX/36l;->g(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1460381
    check-cast p2, LX/9Gt;

    check-cast p3, LX/9FA;

    .line 1460382
    const/4 v3, 0x0

    .line 1460383
    invoke-static {p2}, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->c(LX/9Gt;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1460384
    iget-object v0, p2, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1460385
    :goto_0
    move-object v2, v0

    .line 1460386
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->g:LX/9FH;

    .line 1460387
    iget-object v1, p3, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 1460388
    iget-wide v9, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v4, v9

    .line 1460389
    invoke-virtual {v0, v4, v5}, LX/9FH;->a(J)LX/9FG;

    move-result-object v0

    .line 1460390
    new-instance v1, LX/9Gw;

    iget-object v3, p2, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p2}, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->c(LX/9Gt;)Z

    move-result v4

    new-instance v5, LX/9Gr;

    invoke-direct {v5, p0, v0, p3, p2}, LX/9Gr;-><init>(Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;LX/9FG;LX/9FA;LX/9Gt;)V

    invoke-direct {v1, v3, v2, v4, v5}, LX/9Gw;-><init>(Lcom/facebook/graphql/model/GraphQLComment;LX/0Px;ZLandroid/view/View$OnClickListener;)V

    .line 1460391
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/ViewMoreRepliesComponentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1460392
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->e:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->a()Z

    move-result v3

    .line 1460393
    new-instance v4, LX/9Gs;

    invoke-direct {v4, p0, p3, p2}, LX/9Gs;-><init>(Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;LX/9FA;LX/9Gt;)V

    .line 1460394
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460395
    if-eqz v3, :cond_0

    .line 1460396
    iget-object v6, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    iget-object v7, p2, LX/9Gt;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v8, p2, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1460397
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1460398
    :cond_0
    new-instance v6, LX/9GM;

    iget-object v7, p2, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    sget-object v8, LX/9Fi;->THREADED:LX/9Fi;

    invoke-direct {v6, v0, v7, v8, v4}, LX/9GM;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;LX/9Fi;LX/9Gs;)V

    .line 1460399
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentReplyComponentPartDefinition;

    invoke-virtual {p1, v0, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    .line 1460400
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 1460401
    :cond_2
    iget-object v0, p2, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v1

    .line 1460402
    sget-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    iget-object v2, p2, LX/9Gt;->d:LX/21y;

    invoke-virtual {v0, v2}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1460403
    :goto_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-gt v2, v0, :cond_4

    move-object v0, v1

    .line 1460404
    :goto_4
    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1460405
    :cond_3
    const/4 v0, 0x3

    goto :goto_3

    .line 1460406
    :cond_4
    invoke-virtual {v1, v3, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460407
    check-cast p1, LX/9Gt;

    .line 1460408
    iget-object v0, p1, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
