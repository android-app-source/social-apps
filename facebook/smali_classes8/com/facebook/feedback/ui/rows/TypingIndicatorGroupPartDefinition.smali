.class public Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460463
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460464
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;->a:LX/0Ot;

    .line 1460465
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;
    .locals 4

    .prologue
    .line 1460466
    const-class v1, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;

    monitor-enter v1

    .line 1460467
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460468
    sput-object v2, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460469
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460470
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460471
    new-instance v3, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;

    const/16 p0, 0x7d5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;-><init>(LX/0Ot;)V

    .line 1460472
    move-object v0, v3

    .line 1460473
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460474
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460475
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460476
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1460477
    check-cast p2, Ljava/lang/Integer;

    .line 1460478
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 1460479
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460480
    const/4 v0, 0x1

    return v0
.end method
