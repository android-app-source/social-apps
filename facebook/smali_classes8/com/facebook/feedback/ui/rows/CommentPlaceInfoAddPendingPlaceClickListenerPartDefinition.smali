.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9G4;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/8xt;

.field public final c:LX/189;

.field public final d:LX/20j;

.field public final e:LX/0bH;

.field public final f:LX/1K9;

.field public final g:LX/0kL;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xt;LX/189;LX/20j;LX/0bH;LX/1K9;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459288
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1459289
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459290
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->b:LX/8xt;

    .line 1459291
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->c:LX/189;

    .line 1459292
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->d:LX/20j;

    .line 1459293
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->e:LX/0bH;

    .line 1459294
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->f:LX/1K9;

    .line 1459295
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->g:LX/0kL;

    .line 1459296
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;
    .locals 11

    .prologue
    .line 1459297
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    monitor-enter v1

    .line 1459298
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459299
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459302
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/8xt;->b(LX/0QB;)LX/8xt;

    move-result-object v5

    check-cast v5, LX/8xt;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v6

    check-cast v6, LX/189;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v7

    check-cast v7, LX/20j;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v9

    check-cast v9, LX/1K9;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xt;LX/189;LX/20j;LX/0bH;LX/1K9;LX/0kL;)V

    .line 1459303
    move-object v0, v3

    .line 1459304
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459305
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459306
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1459308
    check-cast p2, LX/9G4;

    check-cast p3, LX/9FA;

    .line 1459309
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459310
    iget-object v1, p2, LX/9G4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 1459311
    new-instance v2, LX/9G3;

    invoke-direct {v2, p0, p2, v1, p3}, LX/9G3;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;LX/9G4;Ljava/lang/String;LX/9FA;)V

    move-object v1, v2

    .line 1459312
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459313
    const/4 v0, 0x0

    return-object v0
.end method
