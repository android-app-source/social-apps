.class public Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "LX/9Ff;",
        "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/3iM;


# direct methods
.method public constructor <init>(LX/3iM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458822
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1458823
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;->a:LX/3iM;

    .line 1458824
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;
    .locals 4

    .prologue
    .line 1458811
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    monitor-enter v1

    .line 1458812
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1458813
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1458814
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1458815
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1458816
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    invoke-static {v0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v3

    check-cast v3, LX/3iM;

    invoke-direct {p0, v3}, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;-><init>(LX/3iM;)V

    .line 1458817
    move-object v0, p0

    .line 1458818
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1458819
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458820
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1458821
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1458825
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    .line 1458826
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1458827
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1458828
    invoke-static {p2}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1458829
    new-instance v2, LX/9Ff;

    new-instance v3, LX/9Fd;

    invoke-direct {v3, p0, p3, v0, v1}, LX/9Fd;-><init>(Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-direct {v2, v3}, LX/9Ff;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3ddd8645

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1458802
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/9Ff;

    .line 1458803
    if-nez p4, :cond_0

    .line 1458804
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x12f88cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1458805
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;->a:LX/3iM;

    .line 1458806
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1458807
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_1

    .line 1458808
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1458809
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1458810
    iget-object v1, p2, LX/9Ff;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1458798
    if-nez p4, :cond_0

    .line 1458799
    :goto_0
    return-void

    .line 1458800
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1458801
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
