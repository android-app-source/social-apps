.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9GB;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H8;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9H8;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1459561
    new-instance v0, LX/9GA;

    invoke-direct {v0}, LX/9GA;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459562
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1459563
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1459564
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 1459565
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;
    .locals 5

    .prologue
    .line 1459566
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

    monitor-enter v1

    .line 1459567
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459568
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459569
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459570
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459571
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;)V

    .line 1459572
    move-object v0, p0

    .line 1459573
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459574
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459575
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459576
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1459577
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1459578
    check-cast p2, LX/9GB;

    .line 1459579
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v1, LX/9GK;

    iget v2, p2, LX/9GB;->b:I

    iget-object v3, p2, LX/9GB;->c:LX/9HA;

    invoke-direct {v1, v2, v3}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459580
    const v0, 0x7f0d09e1

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/9GB;->a:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1459581
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459582
    const/4 v0, 0x1

    return v0
.end method
