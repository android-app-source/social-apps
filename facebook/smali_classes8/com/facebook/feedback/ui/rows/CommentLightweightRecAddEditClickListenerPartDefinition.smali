.class public Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9Fk;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Or;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459047
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1459048
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459049
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->b:LX/0Or;

    .line 1459050
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->c:Landroid/content/Context;

    .line 1459051
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;
    .locals 6

    .prologue
    .line 1459036
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    monitor-enter v1

    .line 1459037
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459038
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459039
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459040
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459041
    new-instance v5, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const/16 v4, 0x455

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Or;Landroid/content/Context;)V

    .line 1459042
    move-object v0, v5

    .line 1459043
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459044
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459045
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1459052
    check-cast p2, LX/9Fk;

    .line 1459053
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459054
    new-instance v1, LX/9Fj;

    invoke-direct {v1, p0, p2}, LX/9Fj;-><init>(Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;LX/9Fk;)V

    move-object v1, v1

    .line 1459055
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459056
    const/4 v0, 0x0

    return-object v0
.end method
