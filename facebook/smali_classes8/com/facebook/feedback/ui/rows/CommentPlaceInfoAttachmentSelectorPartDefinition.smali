.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static l:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

.field private final e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

.field private final f:Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

.field private final g:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

.field private final h:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

.field private final i:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

.field private final j:LX/0ad;

.field private final k:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1459474
    const-class v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;LX/03V;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459462
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1459463
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    .line 1459464
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    .line 1459465
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    .line 1459466
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    .line 1459467
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    .line 1459468
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->h:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    .line 1459469
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->i:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    .line 1459470
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->g:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    .line 1459471
    iput-object p9, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->k:LX/03V;

    .line 1459472
    iput-object p10, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->j:LX/0ad;

    .line 1459473
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;
    .locals 14

    .prologue
    .line 1459451
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;

    monitor-enter v1

    .line 1459452
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459453
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459454
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459455
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459456
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;LX/03V;LX/0ad;)V

    .line 1459457
    move-object v0, v3

    .line 1459458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/Void;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 1459475
    invoke-static/range {p2 .. p2}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v10

    .line 1459476
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1459477
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->k:LX/03V;

    sget-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->a:Ljava/lang/String;

    const-string v3, "CommentPlaceInfoAttachment does not reference a valid comment"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459478
    const/4 v1, 0x0

    .line 1459479
    :goto_0
    return-object v1

    .line 1459480
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v11

    .line 1459481
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v12

    .line 1459482
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v13

    .line 1459483
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v14

    .line 1459484
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v15

    .line 1459485
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v16

    .line 1459486
    invoke-static {v11}, LX/8y3;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;

    move-result-object v17

    .line 1459487
    sget-object v1, LX/9Fi;->THREADED:LX/9Fi;

    invoke-static/range {p2 .. p2}, LX/9Fi;->getCommentLevelFromAttachment(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    move-object v9, v1

    .line 1459488
    :goto_1
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v3

    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v4

    invoke-virtual/range {v17 .. v17}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v6

    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v7

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v8

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v8}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->a(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;IIIIIIZ)Z

    move-result v1

    .line 1459489
    if-eqz v1, :cond_4

    .line 1459490
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    new-instance v2, LX/9G6;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v3

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v3, v9}, LX/9G6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZLX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1459491
    :cond_2
    :goto_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1459492
    :cond_3
    sget-object v1, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    move-object v9, v1

    goto :goto_1

    .line 1459493
    :cond_4
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 1459494
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    new-instance v3, LX/9G8;

    const/4 v1, 0x0

    invoke-virtual {v12, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v1, v4, v9}, LX/9G8;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    .line 1459495
    :cond_5
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 1459496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    new-instance v3, LX/9GC;

    const/4 v1, 0x0

    invoke-virtual {v13, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v1, v4, v9}, LX/9GC;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    .line 1459497
    :cond_6
    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 1459498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    new-instance v3, LX/9Fb;

    const/4 v1, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v1, v4, v9}, LX/9Fb;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    .line 1459499
    :cond_7
    invoke-virtual/range {v17 .. v17}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->b(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1459500
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    new-instance v3, LX/9GD;

    const/4 v1, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v1, v4, v9}, LX/9GD;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1459501
    :cond_8
    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 1459502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->h:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    new-instance v3, LX/9Fn;

    const/4 v1, 0x0

    invoke-virtual {v14, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v1, v4, v9}, LX/9Fn;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1459503
    :cond_9
    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    .line 1459504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->i:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    new-instance v3, LX/9Fz;

    const/4 v1, 0x0

    invoke-virtual {v15, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v1, v4, v9}, LX/9Fz;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1459505
    :cond_a
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1459506
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->g:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    new-instance v2, LX/9FT;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v3, v4, v9}, LX/9FT;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ILX/9HA;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;IIIIIIZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1459446
    add-int v1, p1, p2

    add-int/2addr v1, p3

    add-int/2addr v1, p5

    add-int/2addr v1, p6

    .line 1459447
    if-lez p4, :cond_0

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->b(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1459448
    add-int/2addr v1, p4

    .line 1459449
    :cond_0
    if-le v1, v0, :cond_2

    .line 1459450
    :cond_1
    :goto_0
    return v0

    :cond_2
    if-ne v1, v0, :cond_3

    if-nez p7, :cond_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;)Z
    .locals 3

    .prologue
    .line 1459445
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->j:LX/0ad;

    sget-short v1, LX/5HH;->h:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1459444
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459443
    const/4 v0, 0x1

    return v0
.end method
