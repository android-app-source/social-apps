.class public Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9Gp;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1460323
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460324
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;
    .locals 5

    .prologue
    .line 1460325
    const-class v1, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;

    monitor-enter v1

    .line 1460326
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460327
    sput-object v2, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460328
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460329
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460330
    new-instance p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;

    invoke-direct {p0}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;-><init>()V

    .line 1460331
    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;

    .line 1460332
    iput-object v3, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;->a:Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;

    iput-object v4, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;->b:Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;

    .line 1460333
    move-object v0, p0

    .line 1460334
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460335
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460336
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460337
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1460338
    check-cast p2, LX/9Gp;

    .line 1460339
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;->b:Lcom/facebook/feedback/ui/rows/OriginalPostButtonComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonSelectorPartDefinition;->a:Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1460340
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460341
    const/4 v0, 0x1

    return v0
.end method
