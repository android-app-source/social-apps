.class public Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Fu;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final d:LX/9Ep;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1459148
    new-instance v0, LX/9Ft;

    invoke-direct {v0}, LX/9Ft;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/9Ep;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459149
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1459150
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459151
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1459152
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->d:LX/9Ep;

    .line 1459153
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;
    .locals 9

    .prologue
    .line 1459154
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    monitor-enter v1

    .line 1459155
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459156
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459157
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459158
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459159
    new-instance v6, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1459160
    new-instance p0, LX/9Ep;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/8xu;->b(LX/0QB;)LX/8xu;

    move-result-object v8

    check-cast v8, LX/8xu;

    invoke-direct {p0, v5, v7, v8}, LX/9Ep;-><init>(LX/17W;Landroid/content/Context;LX/8xu;)V

    .line 1459161
    move-object v5, p0

    .line 1459162
    check-cast v5, LX/9Ep;

    invoke-direct {v6, v3, v4, v5}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/9Ep;)V

    .line 1459163
    move-object v0, v6

    .line 1459164
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459165
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459166
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459167
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1459168
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1459169
    check-cast p2, LX/9Fu;

    .line 1459170
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->d:LX/9Ep;

    iget-object v2, p2, LX/9Fu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/9Fu;->b:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1459171
    new-instance p3, LX/9Eo;

    invoke-direct {p3, v1, v2, v3}, LX/9Eo;-><init>(LX/9Ep;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)V

    move-object v1, p3

    .line 1459172
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459173
    const v0, 0x7f0d09d9

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    iget-object v2, p2, LX/9Fu;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1459174
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3c33905c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1459175
    check-cast p1, LX/9Fu;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;

    .line 1459176
    const v1, 0x7f02111f

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;->setThumbnailPlaceholderResource(I)V

    .line 1459177
    iget-object v1, p1, LX/9Fu;->b:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1459178
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1459179
    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentPeopleCardView;->a(Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 1459180
    const/16 v1, 0x1f

    const v2, -0x721373bd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459181
    const/4 v0, 0x1

    return v0
.end method
