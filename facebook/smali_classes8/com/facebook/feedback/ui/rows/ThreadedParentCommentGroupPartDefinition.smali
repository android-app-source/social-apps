.class public Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9Gu;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/EscapeHatchComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460413
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460414
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->a:LX/0Ot;

    .line 1460415
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->b:LX/0Ot;

    .line 1460416
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->c:LX/0Ot;

    .line 1460417
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;
    .locals 6

    .prologue
    .line 1460418
    const-class v1, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;

    monitor-enter v1

    .line 1460419
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460420
    sput-object v2, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460421
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460422
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460423
    new-instance v3, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;

    const/16 v4, 0x7d0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1dc3

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1db2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1460424
    move-object v0, v3

    .line 1460425
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460426
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460427
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1460429
    check-cast p2, LX/9Gu;

    .line 1460430
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1460431
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    iget-object v1, p2, LX/9Gu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1460432
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460433
    const/4 v0, 0x1

    return v0
.end method
