.class public Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/9Gj;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

.field private final b:Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

.field private final e:LX/0tF;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;LX/0tF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460141
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1460142
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    .line 1460143
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    .line 1460144
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    .line 1460145
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->d:Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    .line 1460146
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->e:LX/0tF;

    .line 1460147
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;
    .locals 9

    .prologue
    .line 1460173
    const-class v1, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;

    monitor-enter v1

    .line 1460174
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1460175
    sput-object v2, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1460176
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460177
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1460178
    new-instance v3, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    invoke-static {v0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v8

    check-cast v8, LX/0tF;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;LX/0tF;)V

    .line 1460179
    move-object v0, v3

    .line 1460180
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1460181
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1460182
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1460183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1460150
    check-cast p2, LX/9Gj;

    check-cast p3, LX/9FA;

    const/4 v8, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1460151
    iget-object v0, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1460152
    iget-object v0, p2, LX/9Gj;->d:LX/21y;

    sget-object v3, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v0, v3}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1460153
    new-instance v0, LX/9GZ;

    iget-object v3, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, LX/9GZ;-><init>(Ljava/lang/String;)V

    new-instance v3, LX/4Vj;

    iget-object v4, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/4Vj;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ga;

    .line 1460154
    iget-boolean v0, v0, LX/9Ga;->a:Z

    .line 1460155
    :goto_0
    iget-object v3, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-static {v3}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    .line 1460156
    if-eqz v0, :cond_5

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-le v0, v2, :cond_5

    .line 1460157
    invoke-virtual {v3, v6, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    move v7, v2

    .line 1460158
    :goto_1
    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v9

    .line 1460159
    invoke-static {v1}, LX/16z;->i(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v5, v8

    .line 1460160
    :goto_2
    new-instance v0, LX/9Gl;

    iget-object v2, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    sget-object v4, LX/9Fi;->THREADED:LX/9Fi;

    invoke-direct/range {v0 .. v5}, LX/9Gl;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/5Gs;LX/9Fi;I)V

    .line 1460161
    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/9Gl;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v7, :cond_1

    .line 1460162
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1460163
    :cond_1
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    :goto_3
    if-ge v6, v2, :cond_3

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460164
    iget-object v3, p2, LX/9Gj;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1460165
    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentComponentPartDefinition;

    invoke-static {p1, v3, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    invoke-virtual {v3, v4, v0}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1460166
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1460167
    :cond_2
    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    sub-int v5, v0, v2

    goto :goto_2

    .line 1460168
    :cond_3
    new-instance v0, LX/9Gl;

    iget-object v2, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    sget-object v4, LX/9Fi;->THREADED:LX/9Fi;

    move v5, v8

    invoke-direct/range {v0 .. v5}, LX/9Gl;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/5Gs;LX/9Fi;I)V

    .line 1460169
    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/9Gl;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1460170
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->c:Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1460171
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->d:Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    new-instance v1, LX/9Gc;

    iget-object v2, p2, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p2, LX/9Gj;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {v1, v2, v3}, LX/9Gc;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1460172
    const/4 v0, 0x0

    return-object v0

    :cond_5
    move v7, v6

    move-object v0, v3

    goto/16 :goto_1

    :cond_6
    move v0, v6

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1460148
    check-cast p1, LX/9Gj;

    .line 1460149
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyThreadedCommentListGroupPartDefinition;->e:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/9Gj;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
