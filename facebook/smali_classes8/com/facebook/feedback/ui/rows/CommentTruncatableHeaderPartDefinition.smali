.class public Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "LX/9GQ;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1Uf;

.field private final b:LX/9Hj;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;


# direct methods
.method public constructor <init>(LX/1Uf;LX/9Hj;Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459867
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1459868
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->a:LX/1Uf;

    .line 1459869
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->b:LX/9Hj;

    .line 1459870
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;

    .line 1459871
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;
    .locals 6

    .prologue
    .line 1459872
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

    monitor-enter v1

    .line 1459873
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459874
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459875
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459876
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459877
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v3

    check-cast v3, LX/1Uf;

    invoke-static {v0}, LX/9Hj;->a(LX/0QB;)LX/9Hj;

    move-result-object v4

    check-cast v4, LX/9Hj;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;-><init>(LX/1Uf;LX/9Hj;Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;)V

    .line 1459878
    move-object v0, p0

    .line 1459879
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459880
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459881
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459882
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1459883
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->p:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1459884
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    .line 1459885
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1459886
    move-object v5, v0

    check-cast v5, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1459887
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentHeaderBasePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459888
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->b:LX/9Hj;

    .line 1459889
    iget-object v1, p3, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 1459890
    invoke-virtual {v0, v5, v1}, LX/9Hj;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1459891
    invoke-static {p2}, LX/9Hj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1459892
    const/4 v0, 0x0

    .line 1459893
    :goto_0
    move-object v3, v0

    .line 1459894
    invoke-static {v5}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v4

    .line 1459895
    new-instance v0, LX/9GQ;

    new-instance v1, LX/9GP;

    invoke-direct {v1, p0, p3}, LX/9GP;-><init>(Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;LX/9FA;)V

    if-eqz v3, :cond_0

    .line 1459896
    new-instance p0, LX/9FZ;

    invoke-direct {p0, v5}, LX/9FZ;-><init>(Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {p3, p0, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9Fa;

    move-object v5, p0

    .line 1459897
    :goto_1
    invoke-direct/range {v0 .. v5}, LX/9GQ;-><init>(LX/9GO;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;LX/9Fa;)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 1459898
    :cond_1
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->q:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 1459899
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentTruncatableHeaderPartDefinition;->a:LX/1Uf;

    invoke-virtual {v1, v2, v0}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x70f8856

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1459900
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/9GQ;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    .line 1459901
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1459902
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1459903
    iget-object v2, p2, LX/9GQ;->a:Ljava/lang/CharSequence;

    iget-object v4, p2, LX/9GQ;->e:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object p0

    iget-object p3, p2, LX/9GQ;->b:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {p4, v2, v4, p0, p3}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V

    .line 1459904
    iget-boolean v2, p2, LX/9GQ;->f:Z

    if-eqz v2, :cond_0

    .line 1459905
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->g()V

    .line 1459906
    :cond_0
    iget-object v2, p2, LX/9GQ;->c:LX/9Fa;

    if-eqz v2, :cond_1

    iget-object v2, p2, LX/9GQ;->c:LX/9Fa;

    .line 1459907
    iget-boolean v4, v2, LX/9Fa;->a:Z

    move v2, v4

    .line 1459908
    if-eqz v2, :cond_1

    .line 1459909
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->i()Z

    .line 1459910
    :cond_1
    invoke-static {v1}, LX/8sC;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1459911
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/3Wq;->a(LX/9CP;)V

    .line 1459912
    :cond_2
    iget-object v1, p2, LX/9GQ;->d:LX/9GO;

    .line 1459913
    iput-object v1, p4, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    .line 1459914
    const/16 v1, 0x1f

    const v2, -0x67553a9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459915
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1459916
    check-cast p2, LX/9GQ;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    const/4 v2, 0x0

    .line 1459917
    iput-object v2, p4, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    .line 1459918
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->h()Z

    move-result v0

    .line 1459919
    if-eqz v0, :cond_0

    .line 1459920
    iput-object v2, p2, LX/9GQ;->e:Ljava/lang/CharSequence;

    .line 1459921
    :cond_0
    iget-object v1, p2, LX/9GQ;->c:LX/9Fa;

    if-eqz v1, :cond_1

    .line 1459922
    iget-object v1, p2, LX/9GQ;->c:LX/9Fa;

    .line 1459923
    iput-boolean v0, v1, LX/9Fa;->a:Z

    .line 1459924
    :cond_1
    iget-object v0, p4, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v0}, Lcom/facebook/translation/ui/TranslatableTextView;->c()Z

    move-result v0

    move v0, v0

    .line 1459925
    iput-boolean v0, p2, LX/9GQ;->f:Z

    .line 1459926
    invoke-virtual {p4, v2}, LX/3Wq;->setBody(Ljava/lang/CharSequence;)V

    .line 1459927
    return-void
.end method
