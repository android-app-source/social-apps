.class public Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Ib;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Id;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1463412
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9Id;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463409
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1463410
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->b:LX/0Ot;

    .line 1463411
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;
    .locals 4

    .prologue
    .line 1463398
    const-class v1, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;

    monitor-enter v1

    .line 1463399
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463400
    sput-object v2, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463401
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463402
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463403
    new-instance v3, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;

    const/16 p0, 0x1df2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;-><init>(LX/0Ot;)V

    .line 1463404
    move-object v0, v3

    .line 1463405
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463406
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463407
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1463413
    const v0, 0x57644d41

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1463384
    check-cast p2, LX/9Ic;

    .line 1463385
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Id;

    iget-object v1, p2, LX/9Ic;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/16 p2, 0x10

    const/16 p0, 0xc

    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v6, 0x2

    .line 1463386
    iget-object v2, v0, LX/9Id;->b:LX/9EG;

    invoke-virtual {v2, v1}, LX/9EG;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1463387
    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1463388
    const/4 v2, 0x0

    .line 1463389
    if-eqz v4, :cond_0

    .line 1463390
    invoke-static {v4}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v4

    .line 1463391
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1463392
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1463393
    :cond_0
    iget-object v4, v0, LX/9Id;->b:LX/9EG;

    invoke-virtual {v4, p1, v1, v3}, LX/9EG;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 1463394
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/16 v6, 0x34

    invoke-interface {v3, v6}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7, v8}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x3

    invoke-interface {v3, v6, p0}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    iget-boolean v3, v0, LX/9Id;->c:Z

    if-eqz v3, :cond_1

    const v3, 0x7f021627

    :goto_0
    invoke-interface {v6, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    .line 1463395
    const v6, 0x57644d41

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1463396
    invoke-interface {v3, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    iget-object v6, v0, LX/9Id;->a:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v2

    const v6, 0x7f02111f

    invoke-virtual {v2, v6}, LX/1nw;->h(I)LX/1nw;

    move-result-object v2

    const-class v6, LX/9Id;

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/16 v6, 0x1c

    invoke-interface {v2, v6}, LX/1Di;->j(I)LX/1Di;

    move-result-object v2

    const/16 v6, 0x1c

    invoke-interface {v2, v6}, LX/1Di;->r(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    const v6, 0x7f0e0601

    invoke-static {p1, v3, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    const v4, 0x7f0200b6

    invoke-virtual {v3, v4}, LX/1o5;->h(I)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1463397
    return-object v0

    :cond_1
    const v3, 0x7f021626

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1463373
    invoke-static {}, LX/1dS;->b()V

    .line 1463374
    iget v0, p1, LX/1dQ;->b:I

    .line 1463375
    packed-switch v0, :pswitch_data_0

    .line 1463376
    :goto_0
    return-object v2

    .line 1463377
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1463378
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1463379
    check-cast v1, LX/9Ic;

    .line 1463380
    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/views/specs/OriginalPostButtonComponent;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9Id;

    iget-object p1, v1, LX/9Ic;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1463381
    iget-object p2, v3, LX/9Id;->b:LX/9EG;

    invoke-virtual {p2, p1}, LX/9EG;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    .line 1463382
    iget-object p0, v3, LX/9Id;->b:LX/9EG;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p1, p2}, LX/9EG;->b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1463383
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x57644d41
        :pswitch_0
    .end packed-switch
.end method
