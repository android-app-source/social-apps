.class public Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasLoggingParams;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasInlineReplyActions;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static t:LX/0Xm;


# instance fields
.field private final a:LX/9HS;

.field private final b:LX/9Ha;

.field private final c:LX/9Hp;

.field private final d:LX/9Hx;

.field private final e:LX/9I1;

.field private final f:LX/9IH;

.field private final g:Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

.field private final h:LX/9Ht;

.field private final i:LX/9IL;

.field private final j:LX/1nu;

.field public final k:LX/8qo;

.field private final l:LX/9Hj;

.field private final m:LX/9CG;

.field private final n:LX/3Cq;

.field private final o:LX/1vg;

.field private final p:LX/3iM;

.field private final q:LX/8sB;

.field private final r:LX/9Dz;

.field private final s:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(LX/9HS;LX/9Ha;LX/9Hp;LX/9Hx;LX/9I1;LX/9IH;Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;LX/9Ht;LX/9IL;LX/1nu;LX/8qo;LX/9Hj;LX/9CG;LX/3Cq;LX/1vg;LX/3iM;LX/8sB;LX/9Dz;Ljava/lang/Boolean;)V
    .locals 1
    .param p19    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1462069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462070
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a:LX/9HS;

    .line 1462071
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->b:LX/9Ha;

    .line 1462072
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->c:LX/9Hp;

    .line 1462073
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->d:LX/9Hx;

    .line 1462074
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->e:LX/9I1;

    .line 1462075
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->f:LX/9IH;

    .line 1462076
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->g:Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    .line 1462077
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->h:LX/9Ht;

    .line 1462078
    iput-object p9, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->i:LX/9IL;

    .line 1462079
    iput-object p10, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->j:LX/1nu;

    .line 1462080
    iput-object p11, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->k:LX/8qo;

    .line 1462081
    iput-object p12, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->l:LX/9Hj;

    .line 1462082
    iput-object p13, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->m:LX/9CG;

    .line 1462083
    iput-object p14, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->n:LX/3Cq;

    .line 1462084
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->o:LX/1vg;

    .line 1462085
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->p:LX/3iM;

    .line 1462086
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->q:LX/8sB;

    .line 1462087
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->r:LX/9Dz;

    .line 1462088
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->s:Ljava/lang/Boolean;

    .line 1462089
    return-void
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/graphics/drawable/Drawable;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1461980
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461981
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1461982
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v4

    .line 1461983
    :goto_0
    return-object v0

    .line 1461984
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1461985
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v2

    move-object v3, v4

    :goto_1
    if-ge v5, v7, :cond_2

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1461986
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v2, v8, :cond_3

    .line 1461987
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->e:LX/9I1;

    const/4 v3, 0x0

    .line 1461988
    new-instance v5, LX/9I0;

    invoke-direct {v5, v2}, LX/9I0;-><init>(LX/9I1;)V

    .line 1461989
    sget-object v6, LX/9I1;->a:LX/0Zi;

    invoke-virtual {v6}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/9Hz;

    .line 1461990
    if-nez v6, :cond_1

    .line 1461991
    new-instance v6, LX/9Hz;

    invoke-direct {v6}, LX/9Hz;-><init>()V

    .line 1461992
    :cond_1
    invoke-static {v6, p1, v3, v3, v5}, LX/9Hz;->a$redex0(LX/9Hz;LX/1De;IILX/9I0;)V

    .line 1461993
    move-object v5, v6

    .line 1461994
    move-object v3, v5

    .line 1461995
    move-object v2, v3

    .line 1461996
    iget-object v3, v2, LX/9Hz;->a:LX/9I0;

    iput-object v1, v3, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1461997
    iget-object v3, v2, LX/9Hz;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/BitSet;->set(I)V

    .line 1461998
    move-object v1, v2

    .line 1461999
    invoke-static {p0, v0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->c(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    .line 1462000
    iget-object v2, v1, LX/9Hz;->a:LX/9I0;

    iput-boolean v0, v2, LX/9I0;->b:Z

    .line 1462001
    iget-object v2, v1, LX/9Hz;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1462002
    move-object v0, v1

    .line 1462003
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1462004
    :cond_2
    :goto_2
    if-nez v3, :cond_b

    move-object v0, v4

    .line 1462005
    goto :goto_0

    .line 1462006
    :cond_3
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v2, v8, :cond_5

    .line 1462007
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->g:Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    const/4 v2, 0x0

    .line 1462008
    new-instance v3, LX/9ID;

    invoke-direct {v3, v0}, LX/9ID;-><init>(Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;)V

    .line 1462009
    sget-object v5, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9IC;

    .line 1462010
    if-nez v5, :cond_4

    .line 1462011
    new-instance v5, LX/9IC;

    invoke-direct {v5}, LX/9IC;-><init>()V

    .line 1462012
    :cond_4
    invoke-static {v5, p1, v2, v2, v3}, LX/9IC;->a$redex0(LX/9IC;LX/1De;IILX/9ID;)V

    .line 1462013
    move-object v3, v5

    .line 1462014
    move-object v2, v3

    .line 1462015
    move-object v0, v2

    .line 1462016
    iget-object v2, v0, LX/9IC;->a:LX/9ID;

    iput-object v1, v2, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1462017
    iget-object v2, v0, LX/9IC;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1462018
    move-object v0, v0

    .line 1462019
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    goto :goto_2

    .line 1462020
    :cond_5
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v2, v8, :cond_7

    .line 1462021
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->f:LX/9IH;

    const/4 v2, 0x0

    .line 1462022
    new-instance v3, LX/9IG;

    invoke-direct {v3, v0}, LX/9IG;-><init>(LX/9IH;)V

    .line 1462023
    sget-object v5, LX/9IH;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9IF;

    .line 1462024
    if-nez v5, :cond_6

    .line 1462025
    new-instance v5, LX/9IF;

    invoke-direct {v5}, LX/9IF;-><init>()V

    .line 1462026
    :cond_6
    invoke-static {v5, p1, v2, v2, v3}, LX/9IF;->a$redex0(LX/9IF;LX/1De;IILX/9IG;)V

    .line 1462027
    move-object v3, v5

    .line 1462028
    move-object v2, v3

    .line 1462029
    move-object v0, v2

    .line 1462030
    invoke-virtual {p3, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1462031
    iget-object v2, v0, LX/9IF;->a:LX/9IG;

    iput-object v1, v2, LX/9IG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1462032
    iget-object v2, v0, LX/9IF;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1462033
    move-object v0, v0

    .line 1462034
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    goto :goto_2

    .line 1462035
    :cond_7
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v2, v8, :cond_9

    .line 1462036
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->h:LX/9Ht;

    const/4 v2, 0x0

    .line 1462037
    new-instance v3, LX/9Hs;

    invoke-direct {v3, v0}, LX/9Hs;-><init>(LX/9Ht;)V

    .line 1462038
    sget-object v5, LX/9Ht;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9Hr;

    .line 1462039
    if-nez v5, :cond_8

    .line 1462040
    new-instance v5, LX/9Hr;

    invoke-direct {v5}, LX/9Hr;-><init>()V

    .line 1462041
    :cond_8
    invoke-static {v5, p1, v2, v2, v3}, LX/9Hr;->a$redex0(LX/9Hr;LX/1De;IILX/9Hs;)V

    .line 1462042
    move-object v3, v5

    .line 1462043
    move-object v2, v3

    .line 1462044
    move-object v0, v2

    .line 1462045
    iget-object v2, v0, LX/9Hr;->a:LX/9Hs;

    iput-object v1, v2, LX/9Hs;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1462046
    iget-object v2, v0, LX/9Hr;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1462047
    move-object v0, v0

    .line 1462048
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    goto/16 :goto_2

    .line 1462049
    :cond_9
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v2, v8, :cond_c

    .line 1462050
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->i:LX/9IL;

    const/4 v3, 0x0

    .line 1462051
    new-instance v8, LX/9IK;

    invoke-direct {v8, v2}, LX/9IK;-><init>(LX/9IL;)V

    .line 1462052
    sget-object v9, LX/9IL;->a:LX/0Zi;

    invoke-virtual {v9}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/9IJ;

    .line 1462053
    if-nez v9, :cond_a

    .line 1462054
    new-instance v9, LX/9IJ;

    invoke-direct {v9}, LX/9IJ;-><init>()V

    .line 1462055
    :cond_a
    invoke-static {v9, p1, v3, v3, v8}, LX/9IJ;->a$redex0(LX/9IJ;LX/1De;IILX/9IK;)V

    .line 1462056
    move-object v8, v9

    .line 1462057
    move-object v3, v8

    .line 1462058
    move-object v2, v3

    .line 1462059
    iget-object v3, v2, LX/9IJ;->a:LX/9IK;

    iput-object v1, v3, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1462060
    iget-object v3, v2, LX/9IJ;->d:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Ljava/util/BitSet;->set(I)V

    .line 1462061
    move-object v2, v2

    .line 1462062
    invoke-static {p0, v0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->c(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v3

    .line 1462063
    iget-object v8, v2, LX/9IJ;->a:LX/9IK;

    iput-boolean v3, v8, LX/9IK;->b:Z

    .line 1462064
    iget-object v8, v2, LX/9IJ;->d:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 1462065
    move-object v2, v2

    .line 1462066
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 1462067
    :goto_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto/16 :goto_1

    .line 1462068
    :cond_b
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {v3, v0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-static {p1, p2}, LX/9Hg;->a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->c(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-static {p1}, LX/9Hg;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    move-object v2, v3

    goto :goto_3
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Dg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/graphics/drawable/Drawable;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1461950
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->p:LX/3iM;

    .line 1461951
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461952
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_1

    .line 1461953
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->d:LX/9Hx;

    const/4 v1, 0x0

    .line 1461954
    new-instance p0, LX/9Hw;

    invoke-direct {p0, v0}, LX/9Hw;-><init>(LX/9Hx;)V

    .line 1461955
    sget-object p2, LX/9Hx;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/9Hv;

    .line 1461956
    if-nez p2, :cond_0

    .line 1461957
    new-instance p2, LX/9Hv;

    invoke-direct {p2}, LX/9Hv;-><init>()V

    .line 1461958
    :cond_0
    invoke-static {p2, p1, v1, v1, p0}, LX/9Hv;->a$redex0(LX/9Hv;LX/1De;IILX/9Hw;)V

    .line 1461959
    move-object p0, p2

    .line 1461960
    move-object v1, p0

    .line 1461961
    move-object v0, v1

    .line 1461962
    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    .line 1461963
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a:LX/9HS;

    const/4 v1, 0x0

    .line 1461964
    new-instance v2, LX/9HR;

    invoke-direct {v2, v0}, LX/9HR;-><init>(LX/9HS;)V

    .line 1461965
    iget-object p0, v0, LX/9HS;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9HQ;

    .line 1461966
    if-nez p0, :cond_2

    .line 1461967
    new-instance p0, LX/9HQ;

    invoke-direct {p0, v0}, LX/9HQ;-><init>(LX/9HS;)V

    .line 1461968
    :cond_2
    invoke-static {p0, p1, v1, v1, v2}, LX/9HQ;->a$redex0(LX/9HQ;LX/1De;IILX/9HR;)V

    .line 1461969
    move-object v2, p0

    .line 1461970
    move-object v1, v2

    .line 1461971
    move-object v0, v1

    .line 1461972
    iget-object v1, v0, LX/9HQ;->a:LX/9HR;

    iput-object p3, v1, LX/9HR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1461973
    iget-object v1, v0, LX/9HQ;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1461974
    move-object v0, v0

    .line 1461975
    check-cast p4, LX/9FA;

    .line 1461976
    iget-object v1, v0, LX/9HQ;->a:LX/9HR;

    iput-object p4, v1, LX/9HR;->b:LX/9FA;

    .line 1461977
    iget-object v1, v0, LX/9HQ;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1461978
    move-object v0, v0

    .line 1461979
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-static {p1, p2}, LX/9Hg;->a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->c(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-static {p1}, LX/9Hg;->onClick(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-static {p1}, LX/9Hg;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLComment;ZLX/9FD;)LX/1Dg;
    .locals 3

    .prologue
    .line 1461943
    if-eqz p4, :cond_0

    .line 1461944
    iget v0, p5, LX/9FD;->f:I

    move v0, v0

    .line 1461945
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->j:LX/1nu;

    invoke-virtual {v1, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {v2}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {v2}, LX/1xl;->e(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1nw;->b(Ljava/lang/String;)LX/1nw;

    move-result-object v1

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v1

    const-class v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v1

    const v2, 0x7f02111f

    invoke-virtual {v1, v2}, LX/1nw;->h(I)LX/1nw;

    move-result-object v1

    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v2}, LX/1nw;->b(LX/1Up;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b08bd

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-static {p1, p2}, LX/9Hg;->a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->c(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1461946
    const v2, -0x455704a9

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v1, p0, p2

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v1, v2

    .line 1461947
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-static {p1}, LX/9Hg;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 1461948
    :cond_0
    iget v0, p5, LX/9FD;->e:I

    move v0, v0

    .line 1461949
    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLComment;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1Pr;)LX/1Dg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/graphics/drawable/Drawable;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Z",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1461847
    invoke-static {p3}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461848
    const/4 v0, 0x0

    .line 1461849
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->b:LX/9Ha;

    const/4 v1, 0x0

    .line 1461850
    new-instance v2, LX/9HZ;

    invoke-direct {v2, v0}, LX/9HZ;-><init>(LX/9Ha;)V

    .line 1461851
    iget-object p0, v0, LX/9Ha;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9HY;

    .line 1461852
    if-nez p0, :cond_1

    .line 1461853
    new-instance p0, LX/9HY;

    invoke-direct {p0, v0}, LX/9HY;-><init>(LX/9Ha;)V

    .line 1461854
    :cond_1
    invoke-static {p0, p1, v1, v1, v2}, LX/9HY;->a$redex0(LX/9HY;LX/1De;IILX/9HZ;)V

    .line 1461855
    move-object v2, p0

    .line 1461856
    move-object v1, v2

    .line 1461857
    move-object v0, v1

    .line 1461858
    iget-object v1, v0, LX/9HY;->a:LX/9HZ;

    iput-object p3, v1, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1461859
    iget-object v1, v0, LX/9HY;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1461860
    move-object v0, v0

    .line 1461861
    iget-object v1, v0, LX/9HY;->a:LX/9HZ;

    iput-boolean p4, v1, LX/9HZ;->b:Z

    .line 1461862
    iget-object v1, v0, LX/9HY;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1461863
    move-object v0, v0

    .line 1461864
    iget-object v1, v0, LX/9HY;->a:LX/9HZ;

    iput-object p5, v1, LX/9HZ;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1461865
    iget-object v1, v0, LX/9HY;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1461866
    move-object v0, v0

    .line 1461867
    check-cast p6, LX/1Pq;

    .line 1461868
    iget-object v1, v0, LX/9HY;->a:LX/9HZ;

    iput-object p6, v1, LX/9HZ;->d:LX/1Pq;

    .line 1461869
    iget-object v1, v0, LX/9HY;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1461870
    move-object v0, v0

    .line 1461871
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-static {p1, p2}, LX/9Hg;->a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->c(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-static {p1}, LX/9Hg;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1461936
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->p:LX/3iM;

    .line 1461937
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461938
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    .line 1461939
    const/4 v0, 0x0

    .line 1461940
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const v1, 0x7f0219cd

    invoke-virtual {v0, v1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b08bb

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b08bb

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    .line 1461941
    const v1, -0x4f67f4ec

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 1461942
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLComment;)LX/1Dg;
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x2

    .line 1461931
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461932
    const/4 v0, 0x0

    .line 1461933
    :goto_0
    return-object v0

    .line 1461934
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->o:LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v1, 0x7f0208d6

    invoke-virtual {v0, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    const v1, -0x413d37

    invoke-virtual {v0, v1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1461935
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->j(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->r(I)LX/1Di;

    move-result-object v0

    const/4 v2, 0x5

    const v3, 0x7f0b02a9

    invoke-interface {v0, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    const v2, 0x7f0b0af8

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;
    .locals 3

    .prologue
    .line 1461923
    const-class v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    monitor-enter v1

    .line 1461924
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461925
    sput-object v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461926
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461927
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->b(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461928
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461929
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461930
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/9Ce;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1461917
    if-eqz p0, :cond_0

    .line 1461918
    iget-object v1, p0, LX/9Ce;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1461919
    if-nez v1, :cond_1

    .line 1461920
    :cond_0
    :goto_0
    return v0

    .line 1461921
    :cond_1
    iget-object v1, p0, LX/9Ce;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1461922
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLComment;ZLX/9FD;)LX/1Dg;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1461907
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->l:LX/9Hj;

    invoke-virtual {v0, p3}, LX/9Hj;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1461908
    invoke-static {p3}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1461909
    if-eqz p4, :cond_0

    .line 1461910
    iget v0, p5, LX/9FD;->f:I

    move v0, v0

    .line 1461911
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1, p2}, LX/9Hg;->a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->f(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/9Hg;->onClick(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/9Hg;->d(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->e(LX/1dQ;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e0601

    invoke-static {p1, v1, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a042b

    invoke-virtual {v1, v3}, LX/1ne;->v(I)LX/1ne;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLComment;->B()Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, v2

    :goto_1
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLComment;->D()Z

    move-result v1

    if-nez v1, :cond_2

    move-object v1, v2

    :goto_2
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    const/16 v3, 0x0

    if-nez v3, :cond_3

    :goto_3
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->L(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 1461912
    :cond_0
    iget v0, p5, LX/9FD;->e:I

    move v0, v0

    .line 1461913
    goto :goto_0

    .line 1461914
    :cond_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v4, 0x7f021a25

    invoke-virtual {v1, v4}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v4, 0x7f020996

    invoke-virtual {v1, v4}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v3

    .line 1461915
    const/4 v5, 0x0

    move-object v4, v5

    .line 1461916
    invoke-virtual {v3, v4}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v2

    goto :goto_3

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;
    .locals 20

    .prologue
    .line 1461905
    new-instance v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    invoke-static/range {p0 .. p0}, LX/9HS;->a(LX/0QB;)LX/9HS;

    move-result-object v1

    check-cast v1, LX/9HS;

    invoke-static/range {p0 .. p0}, LX/9Ha;->a(LX/0QB;)LX/9Ha;

    move-result-object v2

    check-cast v2, LX/9Ha;

    invoke-static/range {p0 .. p0}, LX/9Hp;->a(LX/0QB;)LX/9Hp;

    move-result-object v3

    check-cast v3, LX/9Hp;

    invoke-static/range {p0 .. p0}, LX/9Hx;->a(LX/0QB;)LX/9Hx;

    move-result-object v4

    check-cast v4, LX/9Hx;

    invoke-static/range {p0 .. p0}, LX/9I1;->a(LX/0QB;)LX/9I1;

    move-result-object v5

    check-cast v5, LX/9I1;

    invoke-static/range {p0 .. p0}, LX/9IH;->a(LX/0QB;)LX/9IH;

    move-result-object v6

    check-cast v6, LX/9IH;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    invoke-static/range {p0 .. p0}, LX/9Ht;->a(LX/0QB;)LX/9Ht;

    move-result-object v8

    check-cast v8, LX/9Ht;

    invoke-static/range {p0 .. p0}, LX/9IL;->a(LX/0QB;)LX/9IL;

    move-result-object v9

    check-cast v9, LX/9IL;

    invoke-static/range {p0 .. p0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v10

    check-cast v10, LX/1nu;

    invoke-static/range {p0 .. p0}, LX/AjZ;->a(LX/0QB;)LX/AjZ;

    move-result-object v11

    check-cast v11, LX/8qo;

    invoke-static/range {p0 .. p0}, LX/9Hj;->a(LX/0QB;)LX/9Hj;

    move-result-object v12

    check-cast v12, LX/9Hj;

    invoke-static/range {p0 .. p0}, LX/9CG;->a(LX/0QB;)LX/9CG;

    move-result-object v13

    check-cast v13, LX/9CG;

    invoke-static/range {p0 .. p0}, LX/3Cq;->a(LX/0QB;)LX/3Cq;

    move-result-object v14

    check-cast v14, LX/3Cq;

    invoke-static/range {p0 .. p0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v15

    check-cast v15, LX/1vg;

    invoke-static/range {p0 .. p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v16

    check-cast v16, LX/3iM;

    invoke-static/range {p0 .. p0}, LX/8sB;->a(LX/0QB;)LX/8sB;

    move-result-object v17

    check-cast v17, LX/8sB;

    invoke-static/range {p0 .. p0}, LX/9Dz;->a(LX/0QB;)LX/9Dz;

    move-result-object v18

    check-cast v18, LX/9Dz;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v19}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;-><init>(LX/9HS;LX/9Ha;LX/9Hp;LX/9Hx;LX/9I1;LX/9IH;Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;LX/9Ht;LX/9IL;LX/1nu;LX/8qo;LX/9Hj;LX/9CG;LX/3Cq;LX/1vg;LX/3iM;LX/8sB;LX/9Dz;Ljava/lang/Boolean;)V

    .line 1461906
    return-object v0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 1

    .prologue
    .line 1461904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 2

    .prologue
    .line 1461902
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->p:LX/3iM;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 1461903
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FD;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1Pr;)LX/1Dg;
    .locals 12
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/9FD;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "LX/9FD;",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1461885
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1461886
    invoke-static {p2}, LX/5Op;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    .line 1461887
    sget-object v1, LX/9Fi;->THREADED:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1461888
    invoke-static {p2}, LX/9Hj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v7

    move-object/from16 v1, p5

    .line 1461889
    check-cast v1, LX/9FA;

    invoke-virtual {v1}, LX/9FA;->g()LX/9Ce;

    move-result-object v1

    invoke-static {v1, v4, v2}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(LX/9Ce;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v8

    .line 1461890
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->m:LX/9CG;

    invoke-virtual {v1, p1}, LX/9CG;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1461891
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->r:LX/9Dz;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9Dz;->a(Ljava/lang/String;)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 1461892
    if-eqz v1, :cond_0

    .line 1461893
    invoke-static {v3, v1}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 1461894
    :cond_0
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 1461895
    if-eqz v5, :cond_1

    sget-object v1, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    :goto_0
    invoke-virtual {p3, v1}, LX/9FD;->a(LX/9HA;)I

    move-result v6

    .line 1461896
    invoke-virtual {p3}, LX/9FD;->c()I

    move-result v9

    .line 1461897
    if-eqz v5, :cond_2

    invoke-virtual {p3}, LX/9FD;->b()I

    move-result v1

    .line 1461898
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    const/4 v11, 0x2

    invoke-interface {v10, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v2}, LX/1Dh;->b(LX/1dc;)LX/1Dh;

    move-result-object v2

    invoke-static {p1, v3}, LX/9Hg;->a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;

    move-result-object v10

    invoke-interface {v2, v10}, LX/1Dh;->f(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/9Hg;->d(LX/1De;)LX/1dQ;

    move-result-object v10

    invoke-interface {v2, v10}, LX/1Dh;->e(LX/1dQ;)LX/1Dh;

    move-result-object v2

    const/4 v10, 0x4

    invoke-interface {v2, v10, v6}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    const/4 v6, 0x5

    invoke-interface {v2, v6, v9}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    const/4 v6, 0x7

    invoke-interface {v2, v6, v1}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v9

    move-object v1, p0

    move-object v2, p1

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLComment;ZLX/9FD;)LX/1Dg;

    move-result-object v1

    invoke-interface {v9, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v10

    move-object v1, p0

    move-object v2, p1

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->b(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLComment;ZLX/9FD;)LX/1Dg;

    move-result-object v1

    invoke-interface {v10, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p0, p1, v4}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLComment;)LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v10

    move-object v1, p0

    move-object v2, p1

    move v5, v7

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-static/range {v1 .. v7}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLComment;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1Pr;)LX/1Dg;

    move-result-object v1

    invoke-interface {v10, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p0, p1, v3, p2}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {p0, p1, v3, p2, v0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Landroid/graphics/drawable/Drawable;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v9, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p0, p1, p2}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    if-nez v8, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 1461899
    :cond_1
    sget-object v1, LX/9HA;->NO_OFFSET:LX/9HA;

    goto/16 :goto_0

    .line 1461900
    :cond_2
    invoke-virtual {p3}, LX/9FD;->a()I

    move-result v1

    goto/16 :goto_1

    .line 1461901
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->c:LX/9Hp;

    invoke-virtual {v1, p1}, LX/9Hp;->c(LX/1De;)LX/9Hn;

    move-result-object v1

    check-cast p5, LX/9FA;

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, LX/9Hn;->a(LX/9FA;)LX/9Hn;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)Z
    .locals 9
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 1461872
    new-instance v1, LX/9G0;

    .line 1461873
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461874
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {v1, v0}, LX/9G0;-><init>(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1461875
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461876
    check-cast v0, LX/0jW;

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/9G1;

    .line 1461877
    iget-boolean v0, v7, LX/9G1;->a:Z

    move v0, v0

    .line 1461878
    if-eqz v0, :cond_0

    .line 1461879
    :goto_0
    return v8

    .line 1461880
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->q:LX/8sB;

    .line 1461881
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1461882
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast p3, LX/9FA;

    invoke-static {v2, p2, p3}, LX/9Hj;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)LX/9Hi;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, LX/9Hk;

    invoke-direct {v6, p0, v7}, LX/9Hk;-><init>(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/9G1;)V

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, LX/8sB;->a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;Landroid/content/Context;LX/9Hi;LX/2yQ;LX/2dD;)Z

    .line 1461883
    iput-boolean v8, v7, LX/9G1;->a:Z

    .line 1461884
    goto :goto_0
.end method
