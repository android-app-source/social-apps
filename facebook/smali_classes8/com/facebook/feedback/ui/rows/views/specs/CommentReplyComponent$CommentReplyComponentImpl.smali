.class public final Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9IA;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/9GM;

.field public b:LX/9FD;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic d:LX/9IA;


# direct methods
.method public constructor <init>(LX/9IA;)V
    .locals 1

    .prologue
    .line 1462563
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->d:LX/9IA;

    .line 1462564
    move-object v0, p1

    .line 1462565
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1462566
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462545
    const-string v0, "CommentReplyComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1462546
    if-ne p0, p1, :cond_1

    .line 1462547
    :cond_0
    :goto_0
    return v0

    .line 1462548
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1462549
    goto :goto_0

    .line 1462550
    :cond_3
    check-cast p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    .line 1462551
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1462552
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1462553
    if-eq v2, v3, :cond_0

    .line 1462554
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    iget-object v3, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1462555
    goto :goto_0

    .line 1462556
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    if-nez v2, :cond_4

    .line 1462557
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->b:LX/9FD;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->b:LX/9FD;

    iget-object v3, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->b:LX/9FD;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1462558
    goto :goto_0

    .line 1462559
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->b:LX/9FD;

    if-nez v2, :cond_7

    .line 1462560
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1462561
    goto :goto_0

    .line 1462562
    :cond_a
    iget-object v2, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
