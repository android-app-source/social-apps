.class public Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3Wr;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:LX/9HC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final l:LX/9HB;

.field private final m:LX/9HK;

.field private final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1461087
    const-class v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1461106
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461107
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1461098
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461099
    const-class v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1461100
    const v0, 0x7f030915

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1461101
    const v0, 0x7f0d174b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1461102
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->k:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->l:LX/9HB;

    .line 1461103
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->l:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1461104
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->m:LX/9HK;

    .line 1461105
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v1, p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->k:LX/9HC;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1461096
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->m:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1461097
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1461094
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->m:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1461095
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1461093
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->m:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1461090
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1461091
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->m:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1461092
    return-void
.end method

.method public setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1461088
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1461089
    return-void
.end method
