.class public Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Gi;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/9HC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final m:LX/9HB;

.field public final n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

.field public final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Landroid/widget/ImageView;

.field public final q:Lcom/facebook/feedback/ui/CommentComposerPostButton;

.field private final r:Lcom/facebook/resources/ui/FbTextView;

.field private final s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final t:I

.field private u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1461175
    const-class v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1461173
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461174
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1461158
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461159
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1461160
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->k:LX/0Ot;

    .line 1461161
    const-class v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1461162
    const v0, 0x7f030916

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1461163
    const v0, 0x7f0d174a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1461164
    const v0, 0x7f0d174d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1461165
    const v0, 0x7f0d174e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1461166
    const v0, 0x7f0d1751

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->p:Landroid/widget/ImageView;

    .line 1461167
    const v0, 0x7f0d1752

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->q:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    .line 1461168
    const v0, 0x7f0d1750

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1461169
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->t:I

    .line 1461170
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->l:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->m:LX/9HB;

    .line 1461171
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->m:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1461172
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    const/16 v2, 0x1dc5

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v2, p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->k:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->l:LX/9HC;

    return-void
.end method

.method public static f(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;II)Z
    .locals 2

    .prologue
    .line 1461154
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->x:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 1461155
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->x:Landroid/graphics/Rect;

    .line 1461156
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1461157
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method public static i(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)Z
    .locals 1

    .prologue
    .line 1461153
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->w:Lcom/facebook/ipc/media/MediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)Z
    .locals 2

    .prologue
    .line 1461152
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->t:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setImagePreviewVisible(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1461144
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 1461145
    const v0, 0x7f0d174f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1461146
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1461147
    :cond_0
    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1461148
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->o:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1461149
    return-void

    :cond_1
    move v0, v2

    .line 1461150
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1461151
    goto :goto_1
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 1461141
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1461142
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 1461143
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1461116
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1461117
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    invoke-static {p0, v0, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->f(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;II)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->j(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1461118
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->requestDisallowInterceptTouchEvent(Z)V

    .line 1461119
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1461139
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1461140
    return-void
.end method

.method public setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 5
    .param p1    # Lcom/facebook/ipc/media/MediaItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1461127
    if-nez p1, :cond_1

    .line 1461128
    const/4 v1, 0x0

    .line 1461129
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setImagePreviewVisible(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;Z)V

    .line 1461130
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->w:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    .line 1461131
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1461132
    :cond_0
    iput-object v1, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->w:Lcom/facebook/ipc/media/MediaItem;

    .line 1461133
    :goto_0
    return-void

    .line 1461134
    :cond_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setImagePreviewVisible(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;Z)V

    .line 1461135
    const v0, 0x7f0d09d3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1461136
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9Gi;

    sget-object v2, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->j:Lcom/facebook/common/callercontext/CallerContext;

    new-instance v3, Ljava/io/File;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/9Gi;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/net/Uri;)V

    .line 1461137
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->o:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/9HN;

    invoke-direct {v1, p0}, LX/9HN;-><init>(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1461138
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->w:Lcom/facebook/ipc/media/MediaItem;

    goto :goto_0
.end method

.method public setMediaPickerListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1461125
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1461126
    return-void
.end method

.method public setPostButtonListener(LX/21m;)V
    .locals 1

    .prologue
    .line 1461122
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->q:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    .line 1461123
    iput-object p1, v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->e:LX/21m;

    .line 1461124
    return-void
.end method

.method public setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1461120
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1461121
    return-void
.end method
