.class public Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9IC;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9IE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462690
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9IE;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462691
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462692
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->b:LX/0Ot;

    .line 1462693
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;
    .locals 4

    .prologue
    .line 1462694
    const-class v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    monitor-enter v1

    .line 1462695
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462696
    sput-object v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462697
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462698
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462699
    new-instance v3, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    const/16 p0, 0x1de6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;-><init>(LX/0Ot;)V

    .line 1462700
    move-object v0, v3

    .line 1462701
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462702
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462703
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1462705
    const v0, -0x6a82c685

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1462706
    check-cast p2, LX/9ID;

    .line 1462707
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9IE;

    iget-object v1, p2, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 1462708
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/16 v4, 0x8

    const v5, 0x7f0b18a7

    invoke-interface {v2, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v4, 0x7f0200c1

    invoke-interface {v2, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 1462709
    const v4, -0x6a82c685

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1462710
    invoke-interface {v2, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v2, v3

    :goto_0
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e015e

    invoke-static {p1, p0, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->d()LX/1X1;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-nez v5, :cond_1

    :goto_1
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0e0160

    invoke-static {p1, p0, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-static {v1}, LX/1VO;->y(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1462711
    return-object v0

    :cond_0
    iget-object v2, v0, LX/9IE;->a:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v2

    const-class v5, LX/9IE;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v5, 0x7f0b18a9

    invoke-interface {v2, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v5, 0x7f0b18a9

    invoke-interface {v2, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const/4 v5, 0x5

    const v6, 0x7f0b18a8

    invoke-interface {v2, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    :cond_1
    const v3, 0x7f0e015f

    invoke-static {p1, p0, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    iget-object v5, v0, LX/9IE;->b:LX/1Uf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {v6}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1Uf;->a(LX/1eE;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1462712
    invoke-static {}, LX/1dS;->b()V

    .line 1462713
    iget v0, p1, LX/1dQ;->b:I

    .line 1462714
    packed-switch v0, :pswitch_data_0

    .line 1462715
    :goto_0
    return-object v2

    .line 1462716
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1462717
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462718
    check-cast v1, LX/9ID;

    .line 1462719
    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9IE;

    iget-object p1, v1, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1462720
    iget-object p2, v3, LX/9IE;->c:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, p0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1462721
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x6a82c685
        :pswitch_0
    .end packed-switch
.end method
