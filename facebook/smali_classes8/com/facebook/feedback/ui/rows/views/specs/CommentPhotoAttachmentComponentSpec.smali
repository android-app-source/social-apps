.class public Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/26J;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1nu;

.field public final d:LX/1ev;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462423
    const-class v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1ev;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1462424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462425
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->c:LX/1nu;

    .line 1462426
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->d:LX/1ev;

    .line 1462427
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;
    .locals 5

    .prologue
    .line 1462428
    const-class v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;

    monitor-enter v1

    .line 1462429
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462430
    sput-object v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462431
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462432
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462433
    new-instance p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v4

    check-cast v4, LX/1ev;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;-><init>(LX/1nu;LX/1ev;)V

    .line 1462434
    const/16 v3, 0x15c

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 1462435
    iput-object v3, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->a:LX/0Or;

    .line 1462436
    move-object v0, p0

    .line 1462437
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462438
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462439
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
