.class public Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3Wr;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Gi;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/9HC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final m:LX/9HK;

.field public final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Lcom/facebook/resources/ui/FbTextView;

.field private final p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1461191
    const-class v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1461205
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461206
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1461192
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461193
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1461194
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->k:LX/0Ot;

    .line 1461195
    const-class v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1461196
    const v0, 0x7f030917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1461197
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->l:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    .line 1461198
    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1461199
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f0a04a4

    invoke-static {p2, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p2

    sget-object p3, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p2, p3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1461200
    const v0, 0x7f0d174d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1461201
    const v0, 0x7f0d174a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1461202
    const v0, 0x7f0d1753

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1461203
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->m:LX/9HK;

    .line 1461204
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;

    const/16 v2, 0x1dc5

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v2, p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->k:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->l:LX/9HC;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1461189
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->m:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1461190
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1461187
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->m:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1461188
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1461186
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->m:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1461183
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1461184
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->m:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1461185
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1461180
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 1461181
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1461182
    :cond_0
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1461178
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1461179
    return-void
.end method

.method public setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1461176
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1461177
    return-void
.end method
