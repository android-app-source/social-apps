.class public Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1463069
    const-class v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1AV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463071
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;->b:LX/1AV;

    .line 1463072
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;
    .locals 4

    .prologue
    .line 1463073
    const-class v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;

    monitor-enter v1

    .line 1463074
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463075
    sput-object v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463076
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463077
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463078
    new-instance p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v3

    check-cast v3, LX/1AV;

    invoke-direct {p0, v3}, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;-><init>(LX/1AV;)V

    .line 1463079
    move-object v0, p0

    .line 1463080
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463081
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463082
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463083
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
