.class public Lcom/facebook/feedback/ui/rows/views/LoadingTextView;
.super Landroid/widget/TextView;
.source ""


# instance fields
.field private a:LX/9HO;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1461233
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1461234
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1461231
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1461232
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1461224
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461225
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 1461226
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->a:LX/9HO;

    if-nez v0, :cond_0

    .line 1461227
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 1461228
    new-instance v1, LX/9HO;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->getTextSize()F

    move-result v2

    invoke-direct {v1, v2, v0}, LX/9HO;-><init>(FF)V

    iput-object v1, p0, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->a:LX/9HO;

    .line 1461229
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/LoadingTextView;->a:LX/9HO;

    invoke-virtual {v0, p1}, LX/9HO;->draw(Landroid/graphics/Canvas;)V

    .line 1461230
    return-void
.end method
