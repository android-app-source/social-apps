.class public Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3Wr;
.implements LX/3Ws;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/9HC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/9HB;

.field private final d:LX/9HK;

.field public final e:Lcom/facebook/video/player/RichVideoPlayer;

.field public final f:Landroid/widget/TextView;

.field public g:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1460777
    const-class v0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1460778
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460779
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1460759
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460760
    const-class v0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1460761
    const v0, 0x7f0300e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1460762
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setOrientation(I)V

    .line 1460763
    const v0, 0x7f0d053b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1460764
    const v0, 0x7f0d16ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    .line 1460765
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1460766
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1460767
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1460768
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1460769
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/3Ig;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3Ig;-><init>(Landroid/content/Context;)V

    .line 1460770
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1460771
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    const v1, 0x7f020b52

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1460772
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b18ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b18ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p2, 0x7f0b18ac

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b18ae

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-virtual {v0, v1, v2, v3, p2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1460773
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->c:LX/9HB;

    .line 1460774
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->c:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1460775
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->d:LX/9HK;

    .line 1460776
    return-void
.end method

.method public static a(II)F
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1460754
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 1460755
    int-to-float v0, p0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    int-to-float v2, p1

    div-float/2addr v0, v2

    .line 1460756
    :goto_0
    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 1460757
    const v0, 0x3faaaaab

    .line 1460758
    :cond_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v1, p1, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a:LX/9HC;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460752
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->d:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460753
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460780
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->c:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/animation/ValueAnimator;)V

    .line 1460781
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460750
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->d:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460751
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460749
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->d:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460746
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460747
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->d:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460748
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460745
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->c:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1460738
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1460739
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1460740
    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->g:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 1460741
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->measureChild(Landroid/view/View;II)V

    .line 1460742
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->measureChild(Landroid/view/View;II)V

    .line 1460743
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setMeasuredDimension(II)V

    .line 1460744
    return-void
.end method

.method public setBottomTextClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1460732
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1460733
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1460734
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1460735
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->c:LX/9HB;

    .line 1460736
    iput-object p1, v0, LX/9HB;->c:Landroid/view/View$OnTouchListener;

    .line 1460737
    return-void
.end method
