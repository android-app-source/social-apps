.class public Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460891
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1460892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1460889
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460890
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1460885
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460886
    const v0, 0x7f0302d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1460887
    const v0, 0x7f0d09e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1460888
    return-void
.end method


# virtual methods
.method public setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1460879
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1460880
    return-void
.end method

.method public setThumbnailPlaceholderResource(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1460883
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 1460884
    return-void
.end method

.method public setThumbnailUri(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1460881
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1460882
    return-void
.end method
