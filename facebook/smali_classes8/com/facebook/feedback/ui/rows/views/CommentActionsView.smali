.class public Lcom/facebook/feedback/ui/rows/views/CommentActionsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/3Wr;
.implements LX/3Ws;


# instance fields
.field public a:LX/9CG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/9HK;

.field private final c:Lcom/facebook/widget/accessibility/AccessibleTextView;

.field private d:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460597
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1460598
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1460569
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460570
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460588
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460589
    const-class v0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1460590
    const v0, 0x7f0302c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1460591
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->a:LX/9CG;

    invoke-virtual {v0, p1}, LX/9CG;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1460592
    const v0, 0x7f0d09ba

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/accessibility/AccessibleTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->c:Lcom/facebook/widget/accessibility/AccessibleTextView;

    .line 1460593
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->c:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1460594
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->c:Lcom/facebook/widget/accessibility/AccessibleTextView;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b08cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setTextSize(IF)V

    .line 1460595
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->b:LX/9HK;

    .line 1460596
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;

    invoke-static {v0}, LX/9CG;->a(LX/0QB;)LX/9CG;

    move-result-object v0

    check-cast v0, LX/9CG;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->a:LX/9CG;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460586
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->b:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460587
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460584
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0, p1}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 1460585
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460582
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->b:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460583
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460581
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->b:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460578
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460579
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->b:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460580
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460574
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1460575
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->d:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 1460576
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->d:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1460577
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setMetadataText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1460571
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->c:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1460572
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->c:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1460573
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 1460566
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1460567
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->d:Landroid/view/View$OnTouchListener;

    .line 1460568
    return-void
.end method
