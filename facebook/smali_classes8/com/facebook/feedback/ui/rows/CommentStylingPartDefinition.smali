.class public Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9GN;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/HasCommentStylingResolver;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459833
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1459834
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;
    .locals 3

    .prologue
    .line 1459835
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    monitor-enter v1

    .line 1459836
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459837
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459838
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459839
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1459840
    new-instance v0, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;-><init>()V

    .line 1459841
    move-object v0, v0

    .line 1459842
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459843
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459844
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459845
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1459846
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x45ea3aae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1459847
    check-cast p1, LX/9GN;

    check-cast p3, LX/9FA;

    .line 1459848
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1459849
    iget-object v2, p1, LX/9GN;->a:LX/9HA;

    invoke-virtual {v1, v2}, LX/9FD;->a(LX/9HA;)I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result p0

    .line 1459850
    iget p2, v1, LX/9FD;->d:I

    move v1, p2

    .line 1459851
    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result p2

    invoke-static {p4, v2, p0, v1, p2}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1459852
    const/16 v1, 0x1f

    const v2, 0x395115c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
