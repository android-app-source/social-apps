.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPage;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final e:LX/9Er;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1459509
    new-instance v0, LX/9G7;

    invoke-direct {v0}, LX/9G7;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/9Er;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459510
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1459511
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1459512
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1459513
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->d:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1459514
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->e:LX/9Er;

    .line 1459515
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;
    .locals 7

    .prologue
    .line 1459516
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    monitor-enter v1

    .line 1459517
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459518
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459521
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, LX/9Er;->b(LX/0QB;)LX/9Er;

    move-result-object v6

    check-cast v6, LX/9Er;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/9Er;)V

    .line 1459522
    move-object v0, p0

    .line 1459523
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459524
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459525
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1459527
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1459528
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x0

    .line 1459529
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->e:LX/9Er;

    .line 1459530
    new-instance v3, LX/9Eq;

    invoke-direct {v3, v2, p2}, LX/9Eq;-><init>(LX/9Er;Lcom/facebook/graphql/model/GraphQLPage;)V

    move-object v2, v3

    .line 1459531
    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459532
    const v0, 0x7f0d09e1

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1459533
    const v0, 0x7f0d09e2

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->d:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->e:LX/9Er;

    const/4 v4, 0x0

    invoke-virtual {v3, p2, v4}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPage;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1459534
    const v2, 0x7f0d09e3

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->d:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->k()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1459535
    return-object v1

    :cond_0
    move-object v0, v1

    .line 1459536
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4f14d54c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1459537
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPage;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;

    .line 1459538
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1459539
    const v1, 0x7f0202a5

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->setThumbnailPlaceholderResource(I)V

    .line 1459540
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1459541
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x3fd52281

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1459542
    :cond_0
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1459543
    const v2, 0x7f0202a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentPlaceInfoCardView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1459544
    const/4 v0, 0x1

    return v0
.end method
