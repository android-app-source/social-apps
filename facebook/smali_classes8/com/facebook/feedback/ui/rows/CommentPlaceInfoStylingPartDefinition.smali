.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/9GK;",
        "LX/9GL;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1459746
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1459747
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 1459748
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;
    .locals 4

    .prologue
    .line 1459749
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    monitor-enter v1

    .line 1459750
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1459751
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1459752
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459753
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1459754
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;)V

    .line 1459755
    move-object v0, p0

    .line 1459756
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1459757
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1459758
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1459759
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1459760
    check-cast p2, LX/9GK;

    check-cast p3, LX/9FA;

    const/4 v3, 0x0

    .line 1459761
    iget v0, p2, LX/9GK;->a:I

    if-nez v0, :cond_0

    .line 1459762
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v1, LX/9GN;

    iget-object v2, p2, LX/9GK;->b:LX/9HA;

    invoke-direct {v1, v2}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1459763
    new-instance v0, LX/9GL;

    invoke-direct {v0, v3}, LX/9GL;-><init>(I)V

    .line 1459764
    :goto_0
    return-object v0

    .line 1459765
    :cond_0
    iget v0, p2, LX/9GK;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1459766
    new-instance v0, LX/9GL;

    invoke-direct {v0, v3}, LX/9GL;-><init>(I)V

    goto :goto_0

    .line 1459767
    :cond_1
    new-instance v0, LX/9GL;

    .line 1459768
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 1459769
    iget-object v2, p2, LX/9GK;->b:LX/9HA;

    invoke-virtual {v1, v2}, LX/9FD;->a(LX/9HA;)I

    move-result v1

    invoke-direct {v0, v1}, LX/9GL;-><init>(I)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7700c1f5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1459770
    check-cast p1, LX/9GK;

    check-cast p2, LX/9GL;

    .line 1459771
    iget v1, p1, LX/9GK;->a:I

    if-eqz v1, :cond_0

    iget v1, p1, LX/9GK;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1459772
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x71fa0b69

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1459773
    :cond_1
    iget v1, p2, LX/9GL;->a:I

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    const/4 p0, 0x0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result p3

    invoke-static {p4, v1, v2, p0, p3}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1459774
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget p0, p2, LX/9GL;->a:I

    add-int/2addr v2, p0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1459775
    check-cast p1, LX/9GK;

    check-cast p2, LX/9GL;

    const/4 v2, 0x0

    .line 1459776
    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    invoke-static {p4, v2, v0, v2, v1}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1459777
    iget v0, p1, LX/9GK;->a:I

    if-eqz v0, :cond_0

    iget v0, p1, LX/9GK;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1459778
    :cond_0
    :goto_0
    return-void

    .line 1459779
    :cond_1
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, p2, LX/9GL;->a:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method
