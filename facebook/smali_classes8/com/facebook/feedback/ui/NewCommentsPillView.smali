.class public Lcom/facebook/feedback/ui/NewCommentsPillView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/facepile/FacepileView;

.field private b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1456649
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/NewCommentsPillView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1456650
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1456651
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/NewCommentsPillView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456652
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1456653
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456654
    invoke-direct {p0}, Lcom/facebook/feedback/ui/NewCommentsPillView;->a()V

    .line 1456655
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1456644
    const v0, 0x7f030be9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1456645
    const v0, 0x7f0d1d9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1456646
    const v0, 0x7f0d1d9c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1456647
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/NewCommentsPillView;->setGravity(I)V

    .line 1456648
    return-void
.end method


# virtual methods
.method public getPillText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1456631
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setPillProfilePictures(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1456634
    if-nez p1, :cond_0

    .line 1456635
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1456636
    :goto_0
    return-void

    .line 1456637
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-gt v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1456638
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1456639
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1456640
    new-instance v4, LX/6UY;

    invoke-direct {v4, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move v0, v1

    .line 1456641
    goto :goto_1

    .line 1456642
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1456643
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPillText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1456632
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsPillView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1456633
    return-void
.end method
