.class public Lcom/facebook/feedback/ui/DeferredConsumptionController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/9Bw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile l:Lcom/facebook/feedback/ui/DeferredConsumptionController;


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/ufiservices/flyout/FeedbackParams;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field public final e:Landroid/app/NotificationManager;

.field public final f:LX/0Xl;

.field public final g:LX/0kb;

.field public final h:Lcom/facebook/feedback/ui/SingletonFeedbackController;

.field public final i:LX/0hy;

.field public j:I

.field public k:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1455454
    const-class v0, Lcom/facebook/feedback/ui/DeferredConsumptionController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0hy;Landroid/content/Context;LX/0Xl;LX/0kb;Lcom/facebook/feedback/ui/SingletonFeedbackController;)V
    .locals 2
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1455503
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b:Ljava/util/Map;

    .line 1455504
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    .line 1455505
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->j:I

    .line 1455506
    iput-object p1, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->i:LX/0hy;

    .line 1455507
    iput-object p2, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->d:Landroid/content/Context;

    .line 1455508
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->d:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->e:Landroid/app/NotificationManager;

    .line 1455509
    iput-object p3, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->f:LX/0Xl;

    .line 1455510
    iput-object p4, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->g:LX/0kb;

    .line 1455511
    iput-object p5, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->h:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    .line 1455512
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/DeferredConsumptionController;
    .locals 9

    .prologue
    .line 1455489
    sget-object v0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->l:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    if-nez v0, :cond_1

    .line 1455490
    const-class v1, Lcom/facebook/feedback/ui/DeferredConsumptionController;

    monitor-enter v1

    .line 1455491
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->l:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1455492
    if-eqz v2, :cond_0

    .line 1455493
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1455494
    new-instance v3, Lcom/facebook/feedback/ui/DeferredConsumptionController;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v4

    check-cast v4, LX/0hy;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {v0}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(LX/0QB;)Lcom/facebook/feedback/ui/SingletonFeedbackController;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/SingletonFeedbackController;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedback/ui/DeferredConsumptionController;-><init>(LX/0hy;Landroid/content/Context;LX/0Xl;LX/0kb;Lcom/facebook/feedback/ui/SingletonFeedbackController;)V

    .line 1455495
    move-object v0, v3

    .line 1455496
    sput-object v0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->l:Lcom/facebook/feedback/ui/DeferredConsumptionController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1455497
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1455498
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1455499
    :cond_1
    sget-object v0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->l:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    return-object v0

    .line 1455500
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1455501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/ufiservices/flyout/FeedbackParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1455486
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1455487
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v0

    .line 1455488
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 3

    .prologue
    .line 1455513
    invoke-static {p2}, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b(Lcom/facebook/ufiservices/flyout/FeedbackParams;)Ljava/lang/String;

    move-result-object v1

    .line 1455514
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    .line 1455515
    iget-object v2, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v2, v2

    .line 1455516
    invoke-virtual {v0, v2}, LX/1nY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455517
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1455518
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 1455519
    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 1455520
    iget-object v2, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455521
    :cond_0
    :goto_1
    return-void

    .line 1455522
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1455523
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 3

    .prologue
    .line 1455481
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->k:LX/0Yb;

    if-eqz v0, :cond_0

    .line 1455482
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b:Ljava/util/Map;

    invoke-static {p1}, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b(Lcom/facebook/ufiservices/flyout/FeedbackParams;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455483
    return-void

    .line 1455484
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/9DJ;

    invoke-direct {v2, p0}, LX/9DJ;-><init>(Lcom/facebook/feedback/ui/DeferredConsumptionController;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->k:LX/0Yb;

    .line 1455485
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->k:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 7

    .prologue
    .line 1455456
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1455457
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1455458
    :goto_0
    move-object v1, v0

    .line 1455459
    iget-object v0, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1455460
    if-eqz v0, :cond_0

    .line 1455461
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 1455462
    if-nez v2, :cond_1

    .line 1455463
    :cond_0
    :goto_1
    return-void

    .line 1455464
    :cond_1
    iget-object v2, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455465
    iget-object v2, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455466
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1455467
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1455468
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p1, 0x1

    .line 1455469
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1455470
    if-nez v1, :cond_3

    .line 1455471
    :goto_2
    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1455472
    :cond_3
    new-instance v2, LX/2HB;

    iget-object v3, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->d:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0218e4

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->d:Landroid/content/Context;

    const v4, 0x7f081264

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->d:Landroid/content/Context;

    const v4, 0x7f08126b

    new-array v5, p1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    .line 1455473
    new-instance v2, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-direct {v2, v0}, Lcom/facebook/ipc/feed/ViewPermalinkParams;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1455474
    const/4 v3, 0x1

    .line 1455475
    iput-boolean v3, v2, Lcom/facebook/ipc/feed/ViewPermalinkParams;->e:Z

    .line 1455476
    iget-object v3, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->d:Landroid/content/Context;

    iget v4, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->j:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->j:I

    iget-object v5, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->i:LX/0hy;

    const/4 v6, 0x0

    invoke-interface {v5, v6, v2}, LX/0hy;->a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/ViewPermalinkParams;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v5, 0x8000000

    invoke-static {v3, v4, v2, v5}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    move-object v2, v2

    .line 1455477
    iput-object v2, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1455478
    move-object v1, v1

    .line 1455479
    invoke-virtual {v1, p1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v1

    .line 1455480
    iget-object v2, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->e:Landroid/app/NotificationManager;

    iget v3, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->j:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/facebook/feedback/ui/DeferredConsumptionController;->j:I

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_2
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1455455
    return-void
.end method
