.class public final Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

.field public final synthetic b:LX/9DZ;


# direct methods
.method public constructor <init>(LX/9DZ;Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;)V
    .locals 0

    .prologue
    .line 1455742
    iput-object p1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iput-object p2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1455743
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_4

    .line 1455744
    :cond_1
    :goto_1
    return-void

    .line 1455745
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 1455746
    if-nez v1, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_0

    .line 1455747
    :cond_4
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1455748
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iget-object v3, v3, LX/9DZ;->a:LX/9Da;

    iget-object v3, v3, LX/9Da;->f:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1455749
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iget-object v3, v3, LX/9DZ;->a:LX/9Da;

    iget-object v3, v3, LX/9Da;->l:LX/0ad;

    sget-short v4, LX/0wn;->aH:S

    invoke-interface {v3, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1455750
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->j(II)I

    move-result v0

    .line 1455751
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iget-object v2, v2, LX/9DZ;->a:LX/9Da;

    iget-object v2, v2, LX/9Da;->b:LX/1K9;

    new-instance v3, LX/8q1;

    invoke-direct {v3, v1, v0}, LX/8q1;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, LX/1K9;->a(LX/1KJ;)V

    .line 1455752
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iget-object v1, v1, LX/9DZ;->a:LX/9Da;

    iget-object v1, v1, LX/9Da;->u:LX/9DA;

    if-eqz v1, :cond_1

    .line 1455753
    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iget-object v1, v1, LX/9DZ;->a:LX/9Da;

    iget-object v1, v1, LX/9Da;->u:LX/9DA;

    invoke-interface {v1, v0}, LX/9DA;->a(I)V

    goto :goto_1

    .line 1455754
    :cond_6
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->b:LX/9DZ;

    iget-object v3, v3, LX/9DZ;->a:LX/9Da;

    iget-object v3, v3, LX/9Da;->l:LX/0ad;

    sget-short v4, LX/0wn;->aI:S

    invoke-interface {v3, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1455755
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$6$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    goto :goto_2
.end method
