.class public Lcom/facebook/feedback/ui/SingletonFeedbackController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:Lcom/facebook/feedback/ui/SingletonFeedbackController;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/3H7;

.field public final c:LX/1Ck;

.field public final d:LX/0tn;

.field public final e:LX/9Ah;

.field private final f:LX/0pf;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/3H7;LX/1Ck;LX/0tn;LX/9Ah;LX/0pf;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1457493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457494
    iput-object p2, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->b:LX/3H7;

    .line 1457495
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a:Ljava/util/concurrent/Executor;

    .line 1457496
    iput-object p3, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->c:LX/1Ck;

    .line 1457497
    iput-object p4, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->d:LX/0tn;

    .line 1457498
    iput-object p5, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->e:LX/9Ah;

    .line 1457499
    iput-object p6, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->f:LX/0pf;

    .line 1457500
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/SingletonFeedbackController;
    .locals 10

    .prologue
    .line 1457477
    sget-object v0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->g:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    if-nez v0, :cond_1

    .line 1457478
    const-class v1, Lcom/facebook/feedback/ui/SingletonFeedbackController;

    monitor-enter v1

    .line 1457479
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->g:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1457480
    if-eqz v2, :cond_0

    .line 1457481
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1457482
    new-instance v3, Lcom/facebook/feedback/ui/SingletonFeedbackController;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v5

    check-cast v5, LX/3H7;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0tn;->a(LX/0QB;)LX/0tn;

    move-result-object v7

    check-cast v7, LX/0tn;

    .line 1457483
    new-instance v9, LX/9Ah;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct {v9, v8}, LX/9Ah;-><init>(LX/0Uh;)V

    .line 1457484
    move-object v8, v9

    .line 1457485
    check-cast v8, LX/9Ah;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v9

    check-cast v9, LX/0pf;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedback/ui/SingletonFeedbackController;-><init>(Ljava/util/concurrent/Executor;LX/3H7;LX/1Ck;LX/0tn;LX/9Ah;LX/0pf;)V

    .line 1457486
    move-object v0, v3

    .line 1457487
    sput-object v0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->g:Lcom/facebook/feedback/ui/SingletonFeedbackController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1457488
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1457489
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1457490
    :cond_1
    sget-object v0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->g:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    return-object v0

    .line 1457491
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1457492
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/FeedbackParams;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1457457
    iget-object v1, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v1, v1

    .line 1457458
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->f:LX/0pf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->f:LX/0pf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v2

    .line 1457459
    iget-object v3, v2, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v3

    .line 1457460
    if-nez v2, :cond_1

    .line 1457461
    :cond_0
    :goto_0
    return-object v0

    .line 1457462
    :cond_1
    iget-object v2, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v2, v2

    .line 1457463
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 1457464
    if-eqz v2, :cond_0

    .line 1457465
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->f:LX/0pf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v0

    .line 1457466
    iget-object v1, v0, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v1

    .line 1457467
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v6

    .line 1457468
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v1

    .line 1457469
    iget-boolean v0, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v4, v0

    .line 1457470
    new-instance v5, LX/9Ej;

    invoke-direct {v5, p0, v2, v6}, LX/9Ej;-><init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/google/common/util/concurrent/SettableFuture;)V

    move-object v0, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;ZLX/451;)V

    move-object v0, v6

    .line 1457471
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;ZLX/451;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Z",
            "LX/451",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1457472
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->b:LX/3H7;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v3, LX/21y;->DEFAULT_ORDER:LX/21y;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v6, v5

    move v7, p4

    move-object v8, p3

    move-object v9, p2

    invoke-virtual/range {v0 .. v9}, LX/3H7;->a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1457473
    invoke-virtual {p5}, LX/451;->a()V

    .line 1457474
    if-eqz v0, :cond_0

    .line 1457475
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->c:LX/1Ck;

    const-string v2, "fetch_feedback_with_viewer_context"

    invoke-virtual {v1, v2, v0, p5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1457476
    :cond_0
    return-void
.end method

.method public static varargs a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)LX/0Ve;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/FeedbackParams;",
            "[",
            "LX/9Bw;",
            ")",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1457456
    new-instance v0, LX/9Em;

    invoke-direct {v0, p0, p2, p1}, LX/9Em;-><init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;[LX/9Bw;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/0Px;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1457444
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1457445
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1457446
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1457447
    :goto_1
    return-object v0

    .line 1457448
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1457449
    if-eqz v0, :cond_1

    .line 1457450
    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    .line 1457451
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1457452
    invoke-static {p0, v0, p2}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/0Px;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1457453
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 1457454
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1457455
    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method

.method private varargs b(Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;[LX/9Bw;)V
    .locals 12

    .prologue
    .line 1457387
    new-instance v0, LX/9Ek;

    invoke-direct {v0, p0, p1, p3}, LX/9Ek;-><init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)V

    .line 1457388
    invoke-virtual {v0}, LX/451;->a()V

    .line 1457389
    invoke-static {p0, p1, p2}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1457390
    if-eqz v1, :cond_0

    .line 1457391
    invoke-static {p0, p1, p3}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)LX/0Ve;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1457392
    :goto_0
    return-void

    .line 1457393
    :cond_0
    iget-object v1, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    move-object v1, v1

    .line 1457394
    if-nez v1, :cond_1

    .line 1457395
    iget-boolean v1, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    move v1, v1

    .line 1457396
    if-eqz v1, :cond_2

    .line 1457397
    :cond_1
    iget-object v3, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->d:LX/0tn;

    .line 1457398
    iget-boolean v4, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v4, v4

    .line 1457399
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0tn;->a(ZZ)LX/0up;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v3, v4, v0, p2, v5}, LX/0up;->a(Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 1457400
    iget-object v3, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->b:LX/3H7;

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 1457401
    iget-object v6, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->o:LX/21y;

    move-object v6, v6

    .line 1457402
    iget-boolean v7, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    move v7, v7

    .line 1457403
    iget-object v8, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v8, v8

    .line 1457404
    iget-object v9, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    move-object v9, v9

    .line 1457405
    iget-boolean v10, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v10, v10

    .line 1457406
    move-object v11, p2

    invoke-virtual/range {v3 .. v11}, LX/3H7;->a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1457407
    iget-object v4, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->c:LX/1Ck;

    const-string v5, "fetch_focused_feedback"

    invoke-static {p0, p1, p3}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)LX/0Ve;

    move-result-object v6

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1457408
    goto :goto_0

    .line 1457409
    :cond_2
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1457410
    new-instance v6, LX/9El;

    invoke-direct {v6, p0, p1, p3}, LX/9El;-><init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)V

    .line 1457411
    invoke-virtual {v6}, LX/451;->a()V

    .line 1457412
    iget-object v7, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->d:LX/0tn;

    .line 1457413
    iget-boolean v3, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v8, v3

    .line 1457414
    iget-object v3, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v3, v3

    .line 1457415
    if-eqz v3, :cond_4

    move v3, v4

    :goto_1
    invoke-virtual {v7, v8, v3}, LX/0tn;->a(ZZ)LX/0up;

    move-result-object v3

    .line 1457416
    const/4 v7, 0x0

    .line 1457417
    iget-object v8, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v8, v8

    .line 1457418
    if-eqz v8, :cond_6

    .line 1457419
    iget-object v7, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->d:LX/0tn;

    .line 1457420
    iget-boolean v8, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v8, v8

    .line 1457421
    iget-object v9, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v9, v9

    .line 1457422
    if-eqz v9, :cond_5

    .line 1457423
    :goto_2
    invoke-static {v7, v8, v4}, LX/0tn;->c(LX/0tn;ZZ)LX/0ui;

    move-result-object v5

    iget-object v5, v5, LX/0ui;->a:LX/0uj;

    move-object v4, v5

    .line 1457424
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v5

    .line 1457425
    iget-object v7, p1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v7, v7

    .line 1457426
    invoke-virtual {v4, v5, v7}, LX/0uj;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v4

    .line 1457427
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a:Ljava/util/concurrent/Executor;

    move-object v5, v0

    move-object v7, p2

    invoke-virtual/range {v3 .. v8}, LX/0up;->a(Ljava/lang/String;LX/0TF;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 1457428
    if-eqz v9, :cond_3

    .line 1457429
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, LX/0uj;->c(Ljava/lang/String;)V

    .line 1457430
    :cond_3
    goto/16 :goto_0

    :cond_4
    move v3, v5

    .line 1457431
    goto :goto_1

    :cond_5
    move v4, v5

    .line 1457432
    goto :goto_2

    :cond_6
    move-object v9, v7

    goto :goto_3
.end method


# virtual methods
.method public final varargs a(Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;[LX/9Bw;)V
    .locals 7

    .prologue
    .line 1457433
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1457434
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->b(Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;[LX/9Bw;)V

    .line 1457435
    :cond_0
    :goto_0
    return-void

    .line 1457436
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1457437
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->b:LX/3H7;

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/21y;->DEFAULT_ORDER:LX/21y;

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/3H7;->a(Ljava/lang/String;LX/21y;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1457438
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->c:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_feedback_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingletonFeedbackController;->b:LX/3H7;

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/21y;->DEFAULT_ORDER:LX/21y;

    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h()Ljava/lang/String;

    move-result-object p2

    invoke-static {v6, p2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, LX/3H7;->a(Ljava/lang/String;LX/21y;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/9En;

    invoke-direct {v3, p0, p3, p1}, LX/9En;-><init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;[LX/9Bw;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1457439
    goto :goto_0

    .line 1457440
    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Feedback id and legacy api post id are null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    .line 1457441
    array-length v2, p3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, p3, v0

    .line 1457442
    invoke-interface {v3, v1, p1}, LX/9Bw;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 1457443
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
