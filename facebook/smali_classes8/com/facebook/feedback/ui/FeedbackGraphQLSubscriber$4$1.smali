.class public final Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

.field public final synthetic b:LX/9DX;


# direct methods
.method public constructor <init>(LX/9DX;Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;)V
    .locals 0

    .prologue
    .line 1455724
    iput-object p1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iput-object p2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1455700
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1455701
    :cond_0
    :goto_0
    return-void

    .line 1455702
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1455703
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455704
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    invoke-static {v0, v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v2

    .line 1455705
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1455706
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v3, v3, LX/9DX;->a:LX/9Da;

    iget-object v3, v3, LX/9Da;->s:LX/3PH;

    invoke-virtual {v3}, LX/3PH;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1455707
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1455708
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->p:LX/0ti;

    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/3PG;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)LX/69y;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1455709
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    .line 1455710
    new-instance v2, LX/9DW;

    invoke-direct {v2, p0}, LX/9DW;-><init>(Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;)V

    .line 1455711
    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v3, v3, LX/9DX;->a:LX/9Da;

    iget-object v3, v3, LX/9Da;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1455712
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->b:LX/1K9;

    new-instance v2, LX/8pz;

    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    invoke-direct {v2, v3, v1}, LX/8pz;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1455713
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->r:LX/8yF;

    iget-object v1, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v2, v2, LX/9DX;->a:LX/9Da;

    iget-object v2, v2, LX/9Da;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455714
    if-nez v2, :cond_6

    .line 1455715
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->d:LX/3iR;

    invoke-virtual {v0}, LX/3iR;->a()V

    goto/16 :goto_0

    .line 1455716
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v0, v0, LX/9DX;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->p:LX/0ti;

    iget-object v3, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->b:LX/9DX;

    iget-object v3, v3, LX/9DX;->a:LX/9Da;

    iget-object v3, v3, LX/9Da;->q:LX/3PF;

    iget-object v4, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$4$1;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/3PF;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)LX/69x;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0ti;->a(LX/3Bq;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 1455717
    :cond_6
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1455718
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v4, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1455719
    invoke-static {v1}, LX/36l;->i(Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v4

    .line 1455720
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1455721
    iget-object v5, v0, LX/8yF;->a:LX/8yE;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 1455722
    iget-object v0, v5, LX/8yE;->a:LX/0ti;

    invoke-static {v3, v1, v4}, LX/3Ki;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;LX/0Px;)LX/6A8;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1455723
    goto :goto_2
.end method
