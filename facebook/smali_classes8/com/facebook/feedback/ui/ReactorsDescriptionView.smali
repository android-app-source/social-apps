.class public Lcom/facebook/feedback/ui/ReactorsDescriptionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fig/facepile/FigFacepileView;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/9Iy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1456912
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1456913
    invoke-direct {p0}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a()V

    .line 1456914
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1456948
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1456949
    invoke-direct {p0}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a()V

    .line 1456950
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1456945
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456946
    invoke-direct {p0}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a()V

    .line 1456947
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1456940
    const v0, 0x7f031342

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1456941
    const v0, 0x7f0d09c7

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/facepile/FigFacepileView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    .line 1456942
    const v0, 0x7f0d07fd

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->b:Landroid/view/View;

    .line 1456943
    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->setFaceSize(I)V

    .line 1456944
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x728bac4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1456936
    iget-object v1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->d:LX/9Iy;

    if-eqz v1, :cond_0

    .line 1456937
    iget-object v1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    iget-object v2, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->d:LX/9Iy;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/facepile/FigFacepileView;->setModel(LX/9Iy;)V

    .line 1456938
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1456939
    const/16 v1, 0x2d

    const v2, -0x3082bb1b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x716711be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1456933
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1456934
    iget-object v1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-static {}, LX/9Iy;->newBuilder()LX/9Iw;

    move-result-object v2

    invoke-virtual {v2}, LX/9Iw;->a()LX/9Iy;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/facepile/FigFacepileView;->setModel(LX/9Iy;)V

    .line 1456935
    const/16 v1, 0x2d

    const v2, -0x2dfef727

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1456927
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 1456928
    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    .line 1456929
    iget-object v1, v0, LX/6WN;->g:LX/6WT;

    move-object v0, v1

    .line 1456930
    if-eqz v0, :cond_0

    .line 1456931
    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1456932
    :cond_0
    return-void
.end method

.method public setBottomDividerVisibility(I)V
    .locals 1

    .prologue
    .line 1456925
    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1456926
    return-void
.end method

.method public setFacepileModel(LX/9Iy;)V
    .locals 2

    .prologue
    .line 1456921
    iput-object p1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->d:LX/9Iy;

    .line 1456922
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456923
    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->d:LX/9Iy;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->setModel(LX/9Iy;)V

    .line 1456924
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456915
    iput-object p1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->c:Landroid/view/View$OnClickListener;

    .line 1456916
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    .line 1456917
    iget-object v1, v0, LX/6WN;->g:LX/6WT;

    move-object v0, v1

    .line 1456918
    if-eqz v0, :cond_0

    .line 1456919
    iget-object v0, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->a:Lcom/facebook/fig/facepile/FigFacepileView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/ReactorsDescriptionView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1456920
    :cond_0
    return-void
.end method
