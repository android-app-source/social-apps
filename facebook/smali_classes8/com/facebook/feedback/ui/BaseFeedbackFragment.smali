.class public abstract Lcom/facebook/feedback/ui/BaseFeedbackFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/21l;
.implements LX/9Bw;
.implements LX/8qC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/0hF;",
        "LX/0fk;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;",
        "LX/9Bw;",
        "LX/8qC;"
    }
.end annotation


# static fields
.field private static final B:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/9DG;

.field private C:LX/5Ot;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private D:LX/0pf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private E:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/9FH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/21n;

.field public H:Landroid/view/ViewGroup;

.field public I:LX/62C;

.field public J:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field public K:LX/9Dj;

.field public L:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public M:Landroid/view/ViewStub;

.field private N:Z

.field public O:Z

.field public P:Z

.field public Q:Z

.field public final R:LX/9Bu;

.field private S:Ljava/lang/Runnable;

.field private T:Z

.field public a:LX/9DH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/feedback/ui/SingletonFeedbackController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9Dk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8qM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3E1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/9DO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/3iR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/common/perftest/PerfTestConfig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3lt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/9Ee;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/2tj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9Dd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/feedback/ui/DeferredConsumptionController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0tF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/3H7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

.field public w:LX/0g8;

.field public x:LX/0hG;

.field public y:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public z:Lcom/facebook/graphql/model/GraphQLFeedback;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1452780
    const-class v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->B:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1452781
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1452782
    new-instance v0, LX/9Bu;

    invoke-direct {v0, p0}, LX/9Bu;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->R:LX/9Bu;

    .line 1452783
    return-void
.end method

.method private static a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;LX/9DH;Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/9Dk;LX/8qM;LX/0lC;LX/3E1;LX/9DO;LX/3iR;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Ot;LX/03V;Landroid/os/Handler;LX/0ad;LX/9Ee;LX/0if;LX/2tj;LX/9Dd;Lcom/facebook/feedback/ui/DeferredConsumptionController;LX/0tF;LX/3H7;Ljava/util/concurrent/Executor;LX/5Ot;LX/0pf;LX/0SI;LX/9FH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/BaseFeedbackFragment;",
            "LX/9DH;",
            "Lcom/facebook/feedback/ui/SingletonFeedbackController;",
            "LX/9Dk;",
            "LX/8qM;",
            "LX/0lC;",
            "LX/3E1;",
            "LX/9DO;",
            "LX/3iR;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0Ot",
            "<",
            "LX/3lt;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/os/Handler;",
            "LX/0ad;",
            "LX/9Ee;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/2tj;",
            "LX/9Dd;",
            "Lcom/facebook/feedback/ui/DeferredConsumptionController;",
            "LX/0tF;",
            "LX/3H7;",
            "Ljava/util/concurrent/Executor;",
            "LX/5Ot;",
            "LX/0pf;",
            "LX/0SI;",
            "LX/9FH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1452784
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a:LX/9DH;

    iput-object p2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iput-object p3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->c:LX/9Dk;

    iput-object p4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->d:LX/8qM;

    iput-object p5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->e:LX/0lC;

    iput-object p6, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->f:LX/3E1;

    iput-object p7, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->g:LX/9DO;

    iput-object p8, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->h:LX/3iR;

    iput-object p9, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->i:Lcom/facebook/common/perftest/PerfTestConfig;

    iput-object p10, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->j:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->k:LX/03V;

    iput-object p12, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    iput-object p13, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m:LX/0ad;

    iput-object p14, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->n:LX/9Ee;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o:LX/0if;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->p:LX/2tj;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->q:LX/9Dd;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->r:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->s:LX/0tF;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->t:LX/3H7;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->u:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->C:LX/5Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->D:LX/0pf;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->E:LX/0SI;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->F:LX/9FH;

    return-void
.end method

.method public static a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Z)V
    .locals 3

    .prologue
    .line 1452743
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452744
    iget-boolean v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    move v0, v1

    .line 1452745
    if-eqz v0, :cond_1

    .line 1452746
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    .line 1452747
    iget-object v1, v0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-object v0, v1

    .line 1452748
    iget-object v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    move-object v0, v1

    .line 1452749
    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    if-eq v0, v1, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1452750
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    if-nez v0, :cond_2

    .line 1452751
    :cond_1
    :goto_1
    return-void

    .line 1452752
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m:LX/0ad;

    sget-short v1, LX/0wn;->aE:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1452753
    if-eqz p1, :cond_1

    .line 1452754
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    invoke-interface {v0}, LX/21n;->j()V

    .line 1452755
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x()V

    goto :goto_1

    .line 1452756
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->Q:Z

    if-eqz v0, :cond_1

    .line 1452757
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment$7;

    invoke-direct {v1, p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$7;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    const v2, 0x1baa7f09

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1452758
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x()V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 28

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v26

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    const-class v2, LX/9DH;

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9DH;

    invoke-static/range {v26 .. v26}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(LX/0QB;)Lcom/facebook/feedback/ui/SingletonFeedbackController;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/SingletonFeedbackController;

    const-class v4, LX/9Dk;

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9Dk;

    invoke-static/range {v26 .. v26}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object v5

    check-cast v5, LX/8qM;

    invoke-static/range {v26 .. v26}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v6

    check-cast v6, LX/0lC;

    invoke-static/range {v26 .. v26}, LX/3E1;->a(LX/0QB;)LX/3E1;

    move-result-object v7

    check-cast v7, LX/3E1;

    const-class v8, LX/9DO;

    move-object/from16 v0, v26

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/9DO;

    invoke-static/range {v26 .. v26}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v9

    check-cast v9, LX/3iR;

    invoke-static/range {v26 .. v26}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v10

    check-cast v10, Lcom/facebook/common/perftest/PerfTestConfig;

    const/16 v11, 0x139f

    move-object/from16 v0, v26

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v26 .. v26}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static/range {v26 .. v26}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v13

    check-cast v13, Landroid/os/Handler;

    invoke-static/range {v26 .. v26}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static/range {v26 .. v26}, LX/9Ee;->a(LX/0QB;)LX/9Ee;

    move-result-object v15

    check-cast v15, LX/9Ee;

    invoke-static/range {v26 .. v26}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v16

    check-cast v16, LX/0if;

    invoke-static/range {v26 .. v26}, LX/2tj;->a(LX/0QB;)LX/2tj;

    move-result-object v17

    check-cast v17, LX/2tj;

    invoke-static/range {v26 .. v26}, LX/9Dd;->a(LX/0QB;)LX/9Dd;

    move-result-object v18

    check-cast v18, LX/9Dd;

    invoke-static/range {v26 .. v26}, Lcom/facebook/feedback/ui/DeferredConsumptionController;->a(LX/0QB;)Lcom/facebook/feedback/ui/DeferredConsumptionController;

    move-result-object v19

    check-cast v19, Lcom/facebook/feedback/ui/DeferredConsumptionController;

    invoke-static/range {v26 .. v26}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v20

    check-cast v20, LX/0tF;

    invoke-static/range {v26 .. v26}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v21

    check-cast v21, LX/3H7;

    invoke-static/range {v26 .. v26}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v22

    check-cast v22, Ljava/util/concurrent/Executor;

    invoke-static/range {v26 .. v26}, LX/5Ot;->a(LX/0QB;)LX/5Ot;

    move-result-object v23

    check-cast v23, LX/5Ot;

    invoke-static/range {v26 .. v26}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v24

    check-cast v24, LX/0pf;

    invoke-static/range {v26 .. v26}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v25

    check-cast v25, LX/0SI;

    const-class v27, LX/9FH;

    invoke-interface/range {v26 .. v27}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/9FH;

    invoke-static/range {v1 .. v26}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;LX/9DH;Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/9Dk;LX/8qM;LX/0lC;LX/3E1;LX/9DO;LX/3iR;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Ot;LX/03V;Landroid/os/Handler;LX/0ad;LX/9Ee;LX/0if;LX/2tj;LX/9Dd;Lcom/facebook/feedback/ui/DeferredConsumptionController;LX/0tF;LX/3H7;Ljava/util/concurrent/Executor;LX/5Ot;LX/0pf;LX/0SI;LX/9FH;)V

    return-void
.end method

.method public static b(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1452785
    new-instance v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$8;

    invoke-direct {v0, p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$8;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->S:Ljava/lang/Runnable;

    .line 1452786
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->S:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    const v4, 0x6dd1acf9

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1452787
    return-void
.end method

.method public static d(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1452788
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_1

    .line 1452789
    invoke-static {p1}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v3}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-eq v0, v3, :cond_3

    move v0, v1

    .line 1452790
    :goto_0
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v4}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-eq v3, v4, :cond_4

    move v4, v1

    .line 1452791
    :goto_1
    if-nez v0, :cond_0

    if-eqz v4, :cond_1

    .line 1452792
    :cond_0
    iget-object v5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->h:LX/3iR;

    iget-object v6, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452793
    iget-object v7, v3, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v3, v7

    .line 1452794
    :goto_2
    invoke-virtual {v5, v6, v3, v4, v0}, LX/3iR;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)V

    .line 1452795
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->f(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1452796
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 1452797
    invoke-static {p0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Z)V

    .line 1452798
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    invoke-interface {v0}, LX/21n;->getPhotoButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1452799
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->p:LX/2tj;

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    invoke-interface {v3}, LX/21n;->getPhotoButton()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, LX/2tj;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;)V

    .line 1452800
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->p:LX/2tj;

    invoke-virtual {v0}, LX/2tj;->a()Z

    move-result v0

    .line 1452801
    if-eqz v0, :cond_2

    .line 1452802
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v0}, LX/8qL;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;

    move-result-object v0

    .line 1452803
    iput-boolean v2, v0, LX/8qL;->i:Z

    .line 1452804
    move-object v0, v0

    .line 1452805
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452806
    :cond_2
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y()V

    .line 1452807
    iput-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->N:Z

    .line 1452808
    return-void

    :cond_3
    move v0, v2

    .line 1452809
    goto :goto_0

    :cond_4
    move v4, v2

    .line 1452810
    goto :goto_1

    .line 1452811
    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static synthetic d(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 1452812
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1452813
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1452814
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1452815
    new-instance v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;

    invoke-direct {v0, p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    .line 1452816
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->O:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->P:Z

    if-eqz v1, :cond_0

    .line 1452817
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1452818
    :goto_0
    return-void

    .line 1452819
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    const v2, 0xd8e93c1

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public static f(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 6

    .prologue
    .line 1452820
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->S:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1452821
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->n:LX/9Ee;

    invoke-virtual {v0}, LX/9Ee;->a()V

    .line 1452822
    invoke-virtual {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1452823
    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    div-double v2, v0, v2

    .line 1452824
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    .line 1452825
    long-to-double v4, v0

    cmpg-double v2, v4, v2

    if-gez v2, :cond_0

    .line 1452826
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 1452827
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o:LX/0if;

    sget-object v3, LX/0ig;->W:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "comments_shown_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1452828
    return-void
.end method

.method public static u(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V
    .locals 4

    .prologue
    .line 1452829
    const-string v0, "after_animation"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m:LX/0ad;

    sget-char v2, LX/0wn;->aC:C

    const-string v3, "no_upgrade"

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    if-eqz v0, :cond_0

    .line 1452830
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    invoke-interface {v0}, LX/21n;->l()V

    .line 1452831
    :cond_0
    return-void
.end method

.method public static v(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V
    .locals 6

    .prologue
    .line 1452889
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    if-eqz v0, :cond_0

    .line 1452890
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->a()V

    .line 1452891
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    sget-object v2, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->B:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v3, 0x2

    new-array v3, v3, [LX/9Bw;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->r:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;[LX/9Bw;)V

    .line 1452892
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 1452832
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v0, :cond_0

    .line 1452833
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->h()V

    .line 1452834
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v0}, LX/8qL;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;

    move-result-object v0

    const/4 v1, 0x0

    .line 1452835
    iput-boolean v1, v0, LX/8qL;->i:Z

    .line 1452836
    move-object v0, v0

    .line 1452837
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452838
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1452896
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452897
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v0, v1

    .line 1452898
    if-eqz v0, :cond_1

    .line 1452899
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452900
    iget-object p0, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v1, p0

    .line 1452901
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    .line 1452902
    :cond_0
    :goto_0
    return-void

    .line 1452903
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452904
    iget-boolean v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    move v0, v1

    .line 1452905
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/21y;->isReverseOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1452906
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->f()V

    goto :goto_0
.end method


# virtual methods
.method public a(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/9Bj;
    .locals 1

    .prologue
    .line 1452895
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->g:LX/9DO;

    invoke-virtual {v0, p1, p2}, LX/9DO;->a(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/9DN;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/21l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1452893
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->L:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1452894
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    .line 1452907
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1452908
    if-nez p1, :cond_0

    .line 1452909
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 1452910
    :cond_0
    if-eqz p1, :cond_1

    .line 1452911
    const-string v0, "feedbackParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452912
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m()LX/21B;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a(LX/21B;)V

    .line 1452913
    const-string v0, "loadingState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->J:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1452914
    const-string v0, "feedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "hasFetchedFeedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1452915
    const-string v0, "feedback"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1452916
    const-string v0, "hasFetchedFeedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->N:Z

    .line 1452917
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452918
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452919
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v2

    .line 1452920
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452921
    iget-object v5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452922
    iget-object v6, v5, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v5, v6

    .line 1452923
    iget-object v6, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->F:LX/9FH;

    .line 1452924
    iget-wide v11, v5, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v7, v11

    .line 1452925
    invoke-virtual {v6, v7, v8}, LX/9FH;->a(J)LX/9FG;

    move-result-object v6

    .line 1452926
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->s()Z

    move-result v7

    if-eqz v7, :cond_9

    sget-object v7, LX/9FF;->REPLY:LX/9FF;

    :goto_0
    move-object v7, v7

    .line 1452927
    invoke-virtual {v6, v7}, LX/9FG;->a(LX/9FF;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 1452928
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1452929
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->D:LX/0pf;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v0

    .line 1452930
    if-eqz v0, :cond_2

    .line 1452931
    iget-object v2, v0, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v2

    .line 1452932
    if-eqz v2, :cond_2

    .line 1452933
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->E:LX/0SI;

    .line 1452934
    iget-object v3, v0, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v3

    .line 1452935
    invoke-interface {v2, v0}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 1452936
    iput-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->T:Z

    .line 1452937
    :cond_2
    const/4 v0, 0x0

    .line 1452938
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v2, :cond_4

    .line 1452939
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452940
    iget-object v3, v2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v2, v3

    .line 1452941
    iput-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1452942
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    .line 1452943
    :cond_4
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->N:Z

    if-nez v1, :cond_5

    .line 1452944
    invoke-static {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    .line 1452945
    :cond_5
    if-eqz v0, :cond_6

    .line 1452946
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452947
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1452948
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452949
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1452950
    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->l()Ljava/lang/String;

    move-result-object v0

    .line 1452951
    :goto_2
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->k:LX/03V;

    const-string v2, "BaseFeedbackFragment_FeedbackNullIDs"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Feedback passed to BaseFeedbackFragment has a null id: id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", legacyapipostid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", loggingparams: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452952
    :cond_6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->L:Ljava/util/Set;

    .line 1452953
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;

    invoke-direct {v1, p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    const v2, -0x7b44a373

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1452954
    return-void

    .line 1452955
    :cond_7
    const-string v0, "no logging debug info"

    goto :goto_2

    .line 1452956
    :cond_8
    iget-object v7, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v7}, LX/8qL;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;

    move-result-object v7

    invoke-static {v5}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v5

    .line 1452957
    iget-wide v11, v6, LX/9FG;->b:J

    move-wide v9, v11

    .line 1452958
    iput-wide v9, v5, LX/21A;->j:J

    .line 1452959
    move-object v5, v5

    .line 1452960
    invoke-virtual {v5}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v5

    .line 1452961
    iput-object v5, v7, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1452962
    move-object v5, v7

    .line 1452963
    invoke-virtual {v5}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    goto/16 :goto_1

    :cond_9
    sget-object v7, LX/9FF;->TOP_LEVEL:LX/9FF;

    goto/16 :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1452884
    if-nez p1, :cond_0

    .line 1452885
    :goto_0
    return-void

    .line 1452886
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1452887
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1452888
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->H:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 3

    .prologue
    .line 1452878
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1452879
    :cond_0
    :goto_0
    return-void

    .line 1452880
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->S:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1452881
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->n:LX/9Ee;

    invoke-virtual {v0}, LX/9Ee;->a()V

    .line 1452882
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    if-eqz v0, :cond_0

    .line 1452883
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->R:LX/9Bu;

    invoke-virtual {v0, p1, v1, v2}, LX/9Dj;->a(Lcom/facebook/fbservice/service/ServiceException;Landroid/content/res/Resources;LX/1DI;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1452848
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->D:LX/0pf;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452849
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1452850
    invoke-virtual {v0, v1}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v0

    .line 1452851
    if-eqz v0, :cond_0

    .line 1452852
    iget-object v1, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 1452853
    if-eqz v1, :cond_0

    .line 1452854
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v1

    .line 1452855
    iget-object v2, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v2

    .line 1452856
    invoke-static {v0}, LX/6Wy;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1452857
    iput-object v0, v1, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1452858
    move-object v0, v1

    .line 1452859
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    .line 1452860
    :cond_0
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1452861
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1452862
    if-nez v0, :cond_2

    .line 1452863
    :cond_1
    :goto_0
    return-void

    .line 1452864
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1452865
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452866
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 1452867
    invoke-static {v0, v1}, LX/8qM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1452868
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 1452869
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1452870
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o()LX/1Cw;

    move-result-object v0

    .line 1452871
    instance-of v0, v0, LX/21l;

    if-eqz v0, :cond_1

    .line 1452872
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o()LX/1Cw;

    move-result-object v0

    check-cast v0, LX/21l;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452873
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 1452874
    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452875
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 1452876
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1452877
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_2
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1452847
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1452839
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1452840
    if-nez v1, :cond_1

    .line 1452841
    const/4 v0, 0x1

    .line 1452842
    :cond_0
    :goto_0
    return v0

    .line 1452843
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v1, p1, p2}, LX/9DG;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1452844
    :cond_2
    sget-object v1, LX/9Bq;->a:[I

    invoke-virtual {p3}, LX/31M;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1452845
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v0}, LX/0g8;->l()Z

    move-result v0

    goto :goto_0

    .line 1452846
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v0}, LX/0g8;->n()Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1452759
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1452760
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1452761
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1452762
    :goto_0
    return-object v0

    .line 1452763
    :cond_0
    const-string v2, "feedbackParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452764
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1452765
    goto :goto_0

    .line 1452766
    :cond_1
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v2, v2

    .line 1452767
    if-eqz v2, :cond_4

    .line 1452768
    invoke-static {v2}, LX/21y;->isRanked(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1452769
    const-string v3, "ranked_comments"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452770
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 1452771
    const-string v3, "post_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452772
    :cond_3
    :goto_1
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v2, v2

    .line 1452773
    if-eqz v2, :cond_4

    .line 1452774
    const-string v2, "source_group_id"

    .line 1452775
    iget-object v3, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v0, v3

    .line 1452776
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    .line 1452777
    goto :goto_0

    .line 1452778
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1452779
    const-string v2, "post_id"

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final b(LX/21l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1452632
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->L:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1452633
    return-void
.end method

.method public b(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1452641
    invoke-static {p1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->s:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452642
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->t:LX/3H7;

    invoke-virtual {v0, p1}, LX/3H7;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/9Bm;

    invoke-direct {v1, p0, p1}, LX/9Bm;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->u:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1452643
    :goto_0
    return-void

    .line 1452644
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->d(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method

.method public c(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1452645
    invoke-static {p1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->s:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452646
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->t:LX/3H7;

    invoke-virtual {v0, p1}, LX/3H7;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/9Bn;

    invoke-direct {v1, p0, p1}, LX/9Bn;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->u:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1452647
    :goto_0
    return-void

    .line 1452648
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->e(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1452634
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1452635
    const-string v0, "Feedback Params"

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452636
    const-string v2, "Has Fetched Feedback: "

    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->N:Z

    if-eqz v0, :cond_1

    const-string v0, "True"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452637
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 1452638
    :try_start_0
    const-string v0, "Feedback Object"

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->e:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->g()LX/4ps;

    move-result-object v2

    invoke-virtual {v2}, LX/4ps;->a()LX/4ps;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2, v3}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1452639
    :cond_0
    :goto_1
    return-object v1

    .line 1452640
    :cond_1
    const-string v0, "False"

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public gj_()V
    .locals 0

    .prologue
    .line 1452649
    return-void
.end method

.method public gk_()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1452650
    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->Q:Z

    .line 1452651
    invoke-static {p0, v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Z)V

    .line 1452652
    invoke-static {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->u(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    .line 1452653
    return-void
.end method

.method public final hu_()Z
    .locals 2

    .prologue
    .line 1452654
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9DG;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1452655
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v0, :cond_0

    .line 1452656
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->i()Z

    .line 1452657
    :cond_0
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 1

    .prologue
    .line 1452658
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    if-nez v0, :cond_0

    .line 1452659
    const/4 v0, 0x0

    .line 1452660
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    invoke-interface {v0}, LX/21n;->getSelfAsView()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract m()LX/21B;
.end method

.method public n()V
    .locals 0

    .prologue
    .line 1452661
    return-void
.end method

.method public o()LX/1Cw;
    .locals 1

    .prologue
    .line 1452662
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->q:LX/9Dd;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1452663
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1452664
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 1452665
    :goto_0
    new-instance v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment$1;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$1;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;IILandroid/content/Intent;)V

    .line 1452666
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->O:Z

    if-eqz v0, :cond_1

    .line 1452667
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1452668
    :goto_1
    return-void

    .line 1452669
    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    goto :goto_0

    .line 1452670
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    const v2, -0x2224a818

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1
.end method

.method public final onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1452671
    const/4 v0, 0x0

    .line 1452672
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->d:LX/8qM;

    .line 1452673
    iget-boolean v2, v1, LX/8qM;->a:Z

    move v1, v2

    .line 1452674
    if-nez v1, :cond_1

    .line 1452675
    new-instance v0, LX/9Bk;

    invoke-direct {v0, p0}, LX/9Bk;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    .line 1452676
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1452677
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1452678
    :goto_1
    return-object v0

    .line 1452679
    :cond_1
    if-eqz p3, :cond_0

    .line 1452680
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 1452681
    :cond_2
    new-instance v1, LX/9Bl;

    invoke-direct {v1, p0}, LX/9Bl;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x23e00edb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1452682
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1452683
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v1, :cond_0

    .line 1452684
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->c()V

    .line 1452685
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x:LX/0hG;

    .line 1452686
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->T:Z

    if-eqz v1, :cond_1

    .line 1452687
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->E:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    .line 1452688
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x3f42df32

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x31109329

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1452689
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1452690
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v1, :cond_0

    .line 1452691
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->b()V

    .line 1452692
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    if-eqz v1, :cond_1

    .line 1452693
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v1}, LX/62O;->c()V

    .line 1452694
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->S:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 1452695
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->S:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1452696
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    if-eqz v1, :cond_3

    .line 1452697
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1452698
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->f(Landroid/view/View;)V

    .line 1452699
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v1}, LX/0g8;->w()V

    .line 1452700
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/0fx;)V

    .line 1452701
    iput-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    .line 1452702
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x2ec8ad0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x3a1bb0ec

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1452703
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1452704
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452705
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v2

    .line 1452706
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452707
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v2

    .line 1452708
    iget-object v2, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v0, v2

    .line 1452709
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->f:LX/3E1;

    const-string v3, "story_feedback_flyout"

    invoke-virtual {v2, v0, v3}, LX/3E1;->a(LX/162;Ljava/lang/String;)V

    .line 1452710
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->p:LX/2tj;

    invoke-virtual {v0}, LX/2tj;->b()V

    .line 1452711
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v0, :cond_0

    .line 1452712
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->d()V

    .line 1452713
    :cond_0
    const v0, 0x702337bc

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1452714
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x213970e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1452715
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1452716
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->f:LX/3E1;

    invoke-virtual {v1}, LX/3E1;->a()V

    .line 1452717
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->f:LX/3E1;

    invoke-virtual {v1}, LX/3E1;->b()V

    .line 1452718
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v1, :cond_0

    .line 1452719
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->e()V

    .line 1452720
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1fdc0f98

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1452721
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1452722
    const-string v0, "feedbackParams"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1452723
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    if-eqz v0, :cond_0

    .line 1452724
    const-string v0, "loadingState"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    .line 1452725
    iget-object v2, v1, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-object v1, v2

    .line 1452726
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1452727
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    if-eqz v0, :cond_1

    .line 1452728
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->b(Landroid/os/Bundle;)V

    .line 1452729
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->s:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1452730
    const-string v0, "feedback"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1452731
    const-string v0, "hasFetchedFeedback"

    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1452732
    :cond_2
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1452733
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1452734
    const v0, 0x7f0d009c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1452735
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1452736
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setWillNotDraw(Z)V

    .line 1452737
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    .line 1452738
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->O:Z

    if-eqz v0, :cond_0

    .line 1452739
    new-instance v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->run()V

    .line 1452740
    :goto_0
    return-void

    .line 1452741
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V

    const v2, -0x71448adc

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 1452742
    return-void
.end method

.method public abstract r()Landroid/content/Context;
.end method

.method public abstract s()Z
.end method
