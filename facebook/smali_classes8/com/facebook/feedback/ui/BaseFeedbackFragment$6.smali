.class public final Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V
    .locals 1

    .prologue
    .line 1452496
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1452497
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->b:I

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1452498
    iget v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->b:I

    .line 1452499
    iget v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->b:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    .line 1452500
    :goto_0
    return-void

    .line 1452501
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-boolean v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-boolean v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->P:Z

    if-nez v0, :cond_2

    .line 1452502
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    const v1, 0x4228a135

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1452503
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->f(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1452504
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 1452505
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$6;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    const/4 v1, 0x0

    .line 1452506
    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Z)V

    .line 1452507
    goto :goto_0
.end method
