.class public Lcom/facebook/feedback/ui/NewCommentsLoadingView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ProgressBar;

.field private final b:Lcom/facebook/feedback/ui/NewCommentsPillView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1456615
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/NewCommentsLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1456616
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456617
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/NewCommentsLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456618
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456619
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456620
    const v0, 0x7f030be8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1456621
    const v0, 0x7f0d1d99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->a:Landroid/widget/ProgressBar;

    .line 1456622
    const v0, 0x7f0d1d9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/NewCommentsPillView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->b:Lcom/facebook/feedback/ui/NewCommentsPillView;

    .line 1456623
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1456624
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    const/4 v2, -0x1

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1456625
    iget-object v1, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1456626
    return-void
.end method


# virtual methods
.method public getLoadingView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1456627
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->a:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public getPillView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1456628
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->b:Lcom/facebook/feedback/ui/NewCommentsPillView;

    return-object v0
.end method

.method public setOnPillClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1456629
    iget-object v0, p0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->b:Lcom/facebook/feedback/ui/NewCommentsPillView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedback/ui/NewCommentsPillView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1456630
    return-void
.end method
