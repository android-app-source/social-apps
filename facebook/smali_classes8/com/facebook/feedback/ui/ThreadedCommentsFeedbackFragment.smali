.class public Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;
.super Lcom/facebook/feedback/ui/BaseFeedbackFragment;
.source ""


# instance fields
.field public B:LX/1K9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/9Dr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0tF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

.field private F:LX/9Dq;

.field private G:Landroid/view/ContextThemeWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1457952
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;

    invoke-static {p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v1

    check-cast v1, LX/1K9;

    const-class v2, LX/9Dr;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9Dr;

    invoke-static {p0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object p0

    check-cast p0, LX/0tF;

    iput-object v1, p1, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->B:LX/1K9;

    iput-object v2, p1, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->C:LX/9Dr;

    iput-object p0, p1, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->D:LX/0tF;

    return-void
.end method


# virtual methods
.method public final a(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/9Bj;
    .locals 7

    .prologue
    .line 1457947
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->C:LX/9Dr;

    .line 1457948
    new-instance v1, LX/9Dq;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v0}, LX/1nL;->b(LX/0QB;)LX/1nL;

    move-result-object v6

    check-cast v6, LX/1nL;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, LX/9Dq;-><init>(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1nL;)V

    .line 1457949
    move-object v0, v1

    .line 1457950
    iput-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->F:LX/9Dq;

    .line 1457951
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->F:LX/9Dq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1457946
    const-string v0, "story_feedback_flyout"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1457940
    const-class v0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1457941
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Landroid/os/Bundle;)V

    .line 1457942
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->F:LX/9Dq;

    if-eqz v0, :cond_0

    .line 1457943
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->F:LX/9Dq;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    .line 1457944
    iput-object v1, v0, LX/9Dq;->a:LX/9DG;

    .line 1457945
    :cond_0
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1457939
    const-string v0, "flyout_threaded_comments_feedback_animation_perf"

    return-object v0
.end method

.method public final m()LX/21B;
    .locals 1

    .prologue
    .line 1457910
    sget-object v0, LX/21B;->THREADED_FEEDBACK:LX/21B;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6404af56

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457937
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->r()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1457938
    const v2, 0x7f0314a0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1acde40d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x7edb875    # -1.18676E34f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457930
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onDestroy()V

    .line 1457931
    iget-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->D:LX/0tF;

    invoke-virtual {v1}, LX/0tF;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1457932
    iget-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->B:LX/1K9;

    new-instance v2, LX/82i;

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1457933
    iget-object v6, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    move-object v4, v6

    .line 1457934
    invoke-direct {v2, v3, v4}, LX/82i;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1457935
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->x:LX/0hG;

    .line 1457936
    const/16 v1, 0x2b

    const v2, -0x3d7a5b2d

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x74358ad8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457926
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onDestroyView()V

    .line 1457927
    iget-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    invoke-virtual {v1, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457928
    iget-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    invoke-virtual {v1, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->a(Landroid/view/View$OnClickListener;)V

    .line 1457929
    const/16 v1, 0x2b

    const v2, -0x3ede79a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1457917
    const v0, 0x7f0d2eda

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    .line 1457918
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowFocusable(Z)V

    .line 1457919
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowVisibility(I)V

    .line 1457920
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    const v1, 0x7f080ff0

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderText(I)V

    .line 1457921
    new-instance v0, LX/9F1;

    invoke-direct {v0, p0}, LX/9F1;-><init>(Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;)V

    .line 1457922
    iget-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    invoke-virtual {v1, v0}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457923
    iget-object v1, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->E:Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    invoke-virtual {v1, v0}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->a(Landroid/view/View$OnClickListener;)V

    .line 1457924
    invoke-super {p0, p1, p2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1457925
    return-void
.end method

.method public final r()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1457912
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->G:Landroid/view/ContextThemeWrapper;

    if-nez v0, :cond_0

    .line 1457913
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1457914
    const v2, 0x7f0e0607

    move v2, v2

    .line 1457915
    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->G:Landroid/view/ContextThemeWrapper;

    .line 1457916
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;->G:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1457911
    const/4 v0, 0x1

    return v0
.end method
