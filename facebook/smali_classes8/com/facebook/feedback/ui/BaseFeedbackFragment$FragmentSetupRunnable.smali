.class public final Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V
    .locals 0

    .prologue
    .line 1452532
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1452533
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1452534
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    .line 1452535
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v2

    .line 1452536
    check-cast v0, LX/0hG;

    iput-object v0, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x:LX/0hG;

    .line 1452537
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->c:LX/9Dk;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, v2, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->J:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v3, v3, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->R:LX/9Bu;

    invoke-virtual {v1, v2, v3}, LX/9Dk;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)LX/9Dj;

    move-result-object v1

    .line 1452538
    iput-object v1, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    .line 1452539
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->a()V

    .line 1452540
    iget-object v12, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a:LX/9DH;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->r()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v4, v4, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x:LX/0hG;

    iget-object v5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v5, v5, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/9Bj;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v4, v4, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452541
    iget-object v5, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v5

    .line 1452542
    iget-object v5, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v5, v5, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452543
    iget-object v6, v5, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v5, v6

    .line 1452544
    iget-object v6, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v6}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->s()Z

    move-result v6

    new-instance v9, LX/9Bs;

    invoke-direct {v9, p0}, LX/9Bs;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;)V

    new-instance v11, LX/9Bt;

    invoke-direct {v11, p0}, LX/9Bt;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;)V

    move v10, v8

    invoke-virtual/range {v0 .. v11}, LX/9DH;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;)LX/9DG;

    move-result-object v0

    iput-object v0, v12, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    .line 1452545
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452546
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v0, v1

    .line 1452547
    if-eqz v0, :cond_0

    .line 1452548
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452549
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v1, v2

    .line 1452550
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/Long;)V

    .line 1452551
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->L:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1452552
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1452553
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o()LX/1Cw;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1452554
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o()LX/1Cw;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1452555
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    .line 1452556
    iget-object v2, v1, LX/9DG;->b:LX/9CC;

    move-object v1, v2

    .line 1452557
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1452558
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/62C;->b(Ljava/util/List;)LX/62C;

    move-result-object v0

    .line 1452559
    iput-object v0, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->I:LX/62C;

    .line 1452560
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452561
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->l:Lcom/facebook/ipc/media/MediaItem;

    move-object v1, v2

    .line 1452562
    invoke-virtual {v0, v1}, LX/9DG;->a(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1452563
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->h:LX/3iR;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->v:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1452564
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v2

    .line 1452565
    invoke-virtual {v0, v1}, LX/3iR;->b(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1452566
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    .line 1452567
    iput-boolean v7, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->O:Z

    .line 1452568
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$FragmentSetupRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->n()V

    .line 1452569
    return-void
.end method
