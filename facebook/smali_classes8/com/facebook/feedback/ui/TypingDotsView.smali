.class public Lcom/facebook/feedback/ui/TypingDotsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/A7m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1458014
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/TypingDotsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1458015
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1458012
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/TypingDotsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1458013
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1458006
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1458007
    sget-object v0, LX/03r;->TypingDotsView:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1458008
    const/16 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1458009
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1458010
    invoke-direct {p0, v1}, Lcom/facebook/feedback/ui/TypingDotsView;->c(I)V

    .line 1458011
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1458000
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1458001
    instance-of v1, v0, Landroid/graphics/drawable/ShapeDrawable;

    if-eqz v1, :cond_1

    .line 1458002
    check-cast v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1458003
    :cond_0
    :goto_0
    return-void

    .line 1458004
    :cond_1
    instance-of v1, v0, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v1, :cond_0

    .line 1458005
    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/ui/TypingDotsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedback/ui/TypingDotsView;

    new-instance p1, LX/A7m;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {p1, v1}, LX/A7m;-><init>(Landroid/content/Context;)V

    move-object v0, p1

    check-cast v0, LX/A7m;

    iput-object v0, p0, Lcom/facebook/feedback/ui/TypingDotsView;->a:LX/A7m;

    return-void
.end method

.method private c(I)V
    .locals 14

    .prologue
    .line 1457978
    const-class v0, Lcom/facebook/feedback/ui/TypingDotsView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/TypingDotsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1457979
    const v0, 0x7f031533

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1457980
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/TypingDotsView;->setGravity(I)V

    .line 1457981
    const v0, 0x7f0d1eea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1457982
    const v0, 0x7f0d1eeb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v2

    .line 1457983
    const v0, 0x7f0d1eec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v3

    .line 1457984
    invoke-static {v1, p1}, Lcom/facebook/feedback/ui/TypingDotsView;->a(Landroid/view/View;I)V

    .line 1457985
    invoke-static {v2, p1}, Lcom/facebook/feedback/ui/TypingDotsView;->a(Landroid/view/View;I)V

    .line 1457986
    invoke-static {v3, p1}, Lcom/facebook/feedback/ui/TypingDotsView;->a(Landroid/view/View;I)V

    .line 1457987
    new-instance v0, LX/A7l;

    const/4 v4, 0x6

    const/16 v5, 0x661

    const/16 v6, 0x16f

    invoke-direct/range {v0 .. v6}, LX/A7l;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;III)V

    .line 1457988
    iget-object v1, p0, Lcom/facebook/feedback/ui/TypingDotsView;->a:LX/A7m;

    .line 1457989
    iget-object v7, v1, LX/A7m;->a:Landroid/content/Context;

    iget v8, v0, LX/A7l;->d:I

    int-to-float v8, v8

    invoke-static {v7, v8}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v7

    .line 1457990
    sget-object v8, LX/A7j;->a:[F

    invoke-static {v8, v7}, LX/A7m;->a([FI)[F

    move-result-object v8

    .line 1457991
    sget-object v9, LX/A7j;->b:[F

    invoke-static {v9, v7}, LX/A7m;->a([FI)[F

    move-result-object v9

    .line 1457992
    sget-object v10, LX/A7j;->c:[F

    invoke-static {v10, v7}, LX/A7m;->a([FI)[F

    move-result-object v7

    .line 1457993
    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1457994
    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Animator;

    const/4 v12, 0x0

    iget-object v13, v0, LX/A7l;->a:Landroid/view/View;

    invoke-static {v13, v8}, LX/A7m;->a(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v11, v12

    const/4 v8, 0x1

    iget-object v12, v0, LX/A7l;->b:Landroid/view/View;

    invoke-static {v12, v9}, LX/A7m;->a(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v11, v8

    const/4 v8, 0x2

    iget-object v9, v0, LX/A7l;->c:Landroid/view/View;

    invoke-static {v9, v7}, LX/A7m;->a(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    aput-object v7, v11, v8

    invoke-virtual {v10, v11}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1457995
    iget v7, v0, LX/A7l;->e:I

    int-to-long v7, v7

    invoke-virtual {v10, v7, v8}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1457996
    new-instance v7, LX/A7k;

    invoke-direct {v7, v1, v10, v0}, LX/A7k;-><init>(LX/A7m;Landroid/animation/AnimatorSet;LX/A7l;)V

    invoke-virtual {v10, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1457997
    move-object v0, v10

    .line 1457998
    iput-object v0, p0, Lcom/facebook/feedback/ui/TypingDotsView;->b:Landroid/animation/AnimatorSet;

    .line 1457999
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x49899a84

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457975
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1457976
    iget-object v1, p0, Lcom/facebook/feedback/ui/TypingDotsView;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1457977
    const/16 v1, 0x2d

    const v2, -0x41503784

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x76cfa80d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457972
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1457973
    iget-object v1, p0, Lcom/facebook/feedback/ui/TypingDotsView;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->end()V

    .line 1457974
    const/16 v1, 0x2d

    const v2, 0xc6f4f44

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
