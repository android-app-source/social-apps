.class public final Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/21l;
.implements LX/9Bw;
.implements LX/8qD;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/0hF;",
        "LX/0fk;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;",
        "LX/9Bw;",
        "LX/8qD;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;


# instance fields
.field public A:LX/9FH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Ee;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/DeferredConsumptionController;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9E7;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

.field public G:LX/0g8;

.field public H:LX/0hG;

.field public I:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public J:LX/9DG;

.field public K:LX/21n;

.field private L:Landroid/view/ViewGroup;

.field public M:LX/62C;

.field public N:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field public O:LX/9Dj;

.field public P:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public Q:Landroid/view/ViewStub;

.field public final R:LX/9CA;

.field public S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

.field private T:LX/195;

.field private U:Landroid/view/ContextThemeWrapper;

.field private V:LX/9EH;

.field private W:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private X:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private Z:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;

.field public final ab:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public ac:Z

.field private ad:Z

.field private ae:Z

.field public af:Z

.field private ag:Z

.field public ah:Z

.field public ai:Z

.field private final aj:LX/0fu;

.field public ak:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/9DH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/feedback/ui/SingletonFeedbackController;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/9Dk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/8qM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3E1;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/9DO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/3iR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/2tj;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private o:LX/193;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/1nK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/9Dh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/9EG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0tF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private v:LX/3H7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/9Dd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private x:LX/5Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private y:LX/0pf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private z:LX/0SI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1453093
    const-class v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1453094
    const-class v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1453301
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1453302
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453303
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->g:LX/0Ot;

    .line 1453304
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453305
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->h:LX/0Ot;

    .line 1453306
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453307
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->k:LX/0Ot;

    .line 1453308
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453309
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->t:LX/0Ot;

    .line 1453310
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453311
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->B:LX/0Ot;

    .line 1453312
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453313
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->C:LX/0Ot;

    .line 1453314
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453315
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->D:LX/0Ot;

    .line 1453316
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453317
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->E:LX/0Ot;

    .line 1453318
    new-instance v0, LX/9CA;

    invoke-direct {v0, p0}, LX/9CA;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->R:LX/9CA;

    .line 1453319
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ab:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1453320
    new-instance v0, LX/9C1;

    invoke-direct {v0, p0}, LX/9C1;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->aj:LX/0fu;

    .line 1453321
    return-void
.end method

.method private static a(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;LX/9DH;Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/9Dk;LX/8qM;LX/0Ot;LX/0Ot;LX/9DO;LX/3iR;LX/0Ot;LX/0ad;LX/0if;LX/2tj;LX/193;LX/1nK;LX/9Dh;LX/9EG;LX/0TD;LX/0Ot;LX/0tF;LX/3H7;LX/9Dd;LX/5Ot;LX/0pf;LX/0SI;LX/9FH;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;",
            "LX/9DH;",
            "Lcom/facebook/feedback/ui/SingletonFeedbackController;",
            "LX/9Dk;",
            "LX/8qM;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3E1;",
            ">;",
            "LX/9DO;",
            "LX/3iR;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/2tj;",
            "LX/193;",
            "LX/1nK;",
            "LX/9Dh;",
            "LX/9EG;",
            "LX/0TD;",
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;",
            "LX/0tF;",
            "LX/3H7;",
            "LX/9Dd;",
            "LX/5Ot;",
            "LX/0pf;",
            "LX/0SI;",
            "LX/9FH;",
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9Ee;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/DeferredConsumptionController;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9E7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1453322
    iput-object p1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->c:LX/9DH;

    iput-object p2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->d:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iput-object p3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->e:LX/9Dk;

    iput-object p4, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->f:LX/8qM;

    iput-object p5, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->g:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->h:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->i:LX/9DO;

    iput-object p8, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->j:LX/3iR;

    iput-object p9, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->k:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->l:LX/0ad;

    iput-object p11, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->m:LX/0if;

    iput-object p12, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->n:LX/2tj;

    iput-object p13, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->o:LX/193;

    iput-object p14, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->q:LX/9Dh;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->r:LX/9EG;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->t:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->u:LX/0tF;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->v:LX/3H7;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->w:LX/9Dd;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->x:LX/5Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->y:LX/0pf;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->z:LX/0SI;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->A:LX/9FH;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->B:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->C:LX/0Ot;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->D:LX/0Ot;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->E:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 33

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v31

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    const-class v3, LX/9DH;

    move-object/from16 v0, v31

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/9DH;

    invoke-static/range {v31 .. v31}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(LX/0QB;)Lcom/facebook/feedback/ui/SingletonFeedbackController;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/SingletonFeedbackController;

    const-class v5, LX/9Dk;

    move-object/from16 v0, v31

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9Dk;

    invoke-static/range {v31 .. v31}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object v6

    check-cast v6, LX/8qM;

    const/16 v7, 0x2ba

    move-object/from16 v0, v31

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x5d5

    move-object/from16 v0, v31

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, LX/9DO;

    move-object/from16 v0, v31

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/9DO;

    invoke-static/range {v31 .. v31}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v10

    check-cast v10, LX/3iR;

    const/16 v11, 0x259

    move-object/from16 v0, v31

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v31 .. v31}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {v31 .. v31}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v13

    check-cast v13, LX/0if;

    invoke-static/range {v31 .. v31}, LX/2tj;->a(LX/0QB;)LX/2tj;

    move-result-object v14

    check-cast v14, LX/2tj;

    const-class v15, LX/193;

    move-object/from16 v0, v31

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/193;

    invoke-static/range {v31 .. v31}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v16

    check-cast v16, LX/1nK;

    const-class v17, LX/9Dh;

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/9Dh;

    invoke-static/range {v31 .. v31}, LX/9EG;->a(LX/0QB;)LX/9EG;

    move-result-object v18

    check-cast v18, LX/9EG;

    invoke-static/range {v31 .. v31}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v19

    check-cast v19, LX/0TD;

    const/16 v20, 0x140d

    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {v31 .. v31}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v21

    check-cast v21, LX/0tF;

    invoke-static/range {v31 .. v31}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v22

    check-cast v22, LX/3H7;

    invoke-static/range {v31 .. v31}, LX/9Dd;->a(LX/0QB;)LX/9Dd;

    move-result-object v23

    check-cast v23, LX/9Dd;

    invoke-static/range {v31 .. v31}, LX/5Ot;->a(LX/0QB;)LX/5Ot;

    move-result-object v24

    check-cast v24, LX/5Ot;

    invoke-static/range {v31 .. v31}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v25

    check-cast v25, LX/0pf;

    invoke-static/range {v31 .. v31}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v26

    check-cast v26, LX/0SI;

    const-class v27, LX/9FH;

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/9FH;

    const/16 v28, 0x2a

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x1da5

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const/16 v30, 0x1d97

    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const/16 v32, 0x1da2

    invoke-static/range {v31 .. v32}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    invoke-static/range {v2 .. v31}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;LX/9DH;Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/9Dk;LX/8qM;LX/0Ot;LX/0Ot;LX/9DO;LX/3iR;LX/0Ot;LX/0ad;LX/0if;LX/2tj;LX/193;LX/1nK;LX/9Dh;LX/9EG;LX/0TD;LX/0Ot;LX/0tF;LX/3H7;LX/9Dd;LX/5Ot;LX/0pf;LX/0SI;LX/9FH;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 1453323
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0, p2}, LX/9DG;->a(Landroid/os/Bundle;)V

    .line 1453324
    const v0, 0x7f0d118f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1453325
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->q:LX/9Dh;

    invoke-virtual {v1, p0}, LX/9Dh;->a(Lcom/facebook/base/fragment/FbFragment;)LX/9Dg;

    move-result-object v1

    .line 1453326
    iput-object v1, v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->k:LX/9Df;

    .line 1453327
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1453328
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453329
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1453330
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453331
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1453332
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1453333
    if-eqz v0, :cond_0

    .line 1453334
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453335
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1453336
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1453337
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1453338
    invoke-static {v0}, LX/0sa;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->l:LX/0ad;

    sget-short v2, LX/9Io;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1453339
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-static {v0}, LX/0sa;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1453340
    iput-object v0, v1, Lcom/facebook/feedback/ui/FeedbackHeaderView;->l:Ljava/lang/String;

    .line 1453341
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1453342
    const v0, 0x7f0d009c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1453343
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1453344
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setWillNotDraw(Z)V

    .line 1453345
    move-object v0, v0

    .line 1453346
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    .line 1453347
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->M:LX/62C;

    invoke-virtual {v0, p1, v1, v2}, LX/9DG;->a(Landroid/view/View;LX/0g8;Landroid/widget/BaseAdapter;)V

    .line 1453348
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->M:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1453349
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Z:Landroid/os/Parcelable;

    if-eqz v0, :cond_1

    .line 1453350
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Z:Landroid/os/Parcelable;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/os/Parcelable;)V

    .line 1453351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Z:Landroid/os/Parcelable;

    .line 1453352
    :cond_1
    const v0, 0x7f0d009d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/21n;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    .line 1453353
    const v0, 0x7f0d118d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->L:Landroid/view/ViewGroup;

    .line 1453354
    const v0, 0x7f0d118c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Q:Landroid/view/ViewStub;

    .line 1453355
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->m:LX/0if;

    sget-object v1, LX/0ig;->W:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1453356
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->aj:LX/0fu;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fu;)V

    .line 1453357
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    new-instance v1, LX/2je;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->T:LX/195;

    invoke-direct {v1, v2}, LX/2je;-><init>(LX/195;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 1453358
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->H:LX/0hG;

    invoke-interface {v0}, LX/0hG;->eJ_()V

    .line 1453359
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ad:Z

    .line 1453360
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, v0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->j(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453361
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->aa:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1453362
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    if-eqz v0, :cond_3

    .line 1453363
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, v0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453364
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ae:Z

    if-eqz v0, :cond_2

    .line 1453365
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->t()V

    .line 1453366
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->u()V

    .line 1453367
    :cond_2
    return-void

    .line 1453368
    :cond_3
    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453369
    if-eqz v3, :cond_4

    invoke-static {v3}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-nez v4, :cond_7

    invoke-static {v3}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-lez v4, :cond_7

    :cond_4
    const/4 v4, 0x1

    :goto_2
    move v3, v4

    .line 1453370
    if-nez v3, :cond_6

    .line 1453371
    :goto_3
    goto :goto_1

    .line 1453372
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    .line 1453373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->aa:Ljava/lang/String;

    goto :goto_0

    .line 1453374
    :cond_6
    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Q:Landroid/view/ViewStub;

    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    .line 1453375
    new-instance v6, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment$12;

    invoke-direct {v6, p0, v4}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment$12;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;)V

    iput-object v6, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ak:Ljava/lang/Runnable;

    .line 1453376
    iget-object v6, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    iget-object v7, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ak:Ljava/lang/Runnable;

    const-wide/16 v8, 0x7d0

    const v10, -0x5dd20f

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1453377
    invoke-static {p0, v4}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->d(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;)V

    .line 1453378
    const v3, 0x7f0d1191

    invoke-static {v4, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1453379
    iget-object v5, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v5, v4}, LX/0g8;->f(Landroid/view/View;)V

    .line 1453380
    iget-object v4, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {v4, v3}, LX/62O;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public static d(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 1453381
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 1453382
    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->v(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->l:LX/0ad;

    sget-short v2, LX/0wn;->an:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1453383
    :cond_0
    :goto_0
    return-void

    .line 1453384
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9E7;

    .line 1453385
    iget-object v2, v0, LX/9E7;->a:Landroid/view/View;

    if-nez v2, :cond_2

    .line 1453386
    const v2, 0x7f0d1193

    invoke-static {p1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 1453387
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, LX/9E7;->a:Landroid/view/View;

    .line 1453388
    :cond_2
    iget-object v2, v0, LX/9E7;->a:Landroid/view/View;

    const v3, 0x7f0d1190

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1453389
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f008d

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v5, v6

    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1453390
    goto :goto_0
.end method

.method public static d(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1453391
    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    .line 1453392
    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ai:Z

    .line 1453393
    iput-object p1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453394
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453395
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ad:Z

    if-eqz v0, :cond_0

    .line 1453396
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->j(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453397
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453398
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->h()V

    .line 1453399
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1453400
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->G()V

    .line 1453401
    :cond_1
    return-void
.end method

.method private e(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 2

    .prologue
    .line 1453402
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    invoke-interface {v0}, LX/21n;->getPhotoButton()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1453403
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->w()V

    .line 1453404
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 1453405
    return-void

    .line 1453406
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->n:LX/2tj;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    invoke-interface {v1}, LX/21n;->getPhotoButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/2tj;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;)V

    .line 1453407
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->n:LX/2tj;

    invoke-virtual {v0}, LX/2tj;->a()Z

    move-result v0

    .line 1453408
    if-eqz v0, :cond_0

    .line 1453409
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v0}, LX/8qL;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;

    move-result-object v0

    const/4 v1, 0x0

    .line 1453410
    iput-boolean v1, v0, LX/8qL;->i:Z

    .line 1453411
    move-object v0, v0

    .line 1453412
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    goto :goto_0
.end method

.method private f(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1453413
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_1

    .line 1453414
    :cond_0
    :goto_0
    return-void

    .line 1453415
    :cond_1
    invoke-static {p1}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v3}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-eq v0, v3, :cond_4

    move v0, v1

    .line 1453416
    :goto_1
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v4}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-eq v3, v4, :cond_2

    move v2, v1

    .line 1453417
    :cond_2
    if-nez v0, :cond_3

    if-eqz v2, :cond_0

    .line 1453418
    :cond_3
    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->j:LX/3iR;

    iget-object v4, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453419
    iget-object p0, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, p0

    .line 1453420
    :goto_2
    invoke-virtual {v3, v4, v1, v2, v0}, LX/3iR;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1453421
    goto :goto_1

    .line 1453422
    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static h(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1453423
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ah:Z

    .line 1453424
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1453425
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->f()V

    .line 1453426
    :cond_1
    :goto_0
    return-void

    .line 1453427
    :cond_2
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1453428
    iget-boolean v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    if-nez v2, :cond_3

    if-nez p1, :cond_6

    :cond_3
    move v0, v1

    .line 1453429
    :cond_4
    :goto_1
    move v0, v0

    .line 1453430
    if-eqz v0, :cond_5

    .line 1453431
    iput-object p1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453432
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ad:Z

    if-eqz v0, :cond_5

    .line 1453433
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->j(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453434
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 1453435
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->e()V

    .line 1453436
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->g()V

    .line 1453437
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1453438
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->F()V

    goto :goto_0

    .line 1453439
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v2, :cond_4

    .line 1453440
    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v3}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-gt v2, v3, :cond_4

    move v0, v1

    goto :goto_1
.end method

.method private j(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 6

    .prologue
    .line 1453441
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ak:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1453442
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ee;

    invoke-virtual {v0}, LX/9Ee;->a()V

    .line 1453443
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9E7;

    invoke-virtual {v0}, LX/9E7;->a()V

    .line 1453444
    invoke-virtual {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1453445
    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    div-double v2, v0, v2

    .line 1453446
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    .line 1453447
    long-to-double v4, v0

    cmpg-double v2, v4, v2

    if-gez v2, :cond_0

    .line 1453448
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 1453449
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->m:LX/0if;

    sget-object v3, LX/0ig;->W:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "comments_shown_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1453450
    return-void
.end method

.method public static r(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1453451
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_0

    .line 1453452
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->c()V

    .line 1453453
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1453454
    :cond_0
    iput-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->H:LX/0hG;

    .line 1453455
    iput-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->W:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1453456
    return-void
.end method

.method public static s(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V
    .locals 6

    .prologue
    .line 1453567
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    if-eqz v0, :cond_0

    .line 1453568
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->a()V

    .line 1453569
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->d:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    sget-object v3, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v0, 0x2

    new-array v4, v0, [LX/9Bw;

    const/4 v0, 0x0

    aput-object p0, v4, v0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bw;

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;[LX/9Bw;)V

    .line 1453570
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1453457
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453458
    iget-boolean v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    move v0, v1

    .line 1453459
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    if-nez v0, :cond_1

    .line 1453460
    :cond_0
    :goto_0
    return-void

    .line 1453461
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    invoke-interface {v0}, LX/21n;->j()V

    .line 1453462
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_2

    .line 1453463
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->h()V

    .line 1453464
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v0}, LX/8qL;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;

    move-result-object v0

    const/4 v1, 0x0

    .line 1453465
    iput-boolean v1, v0, LX/8qL;->i:Z

    .line 1453466
    move-object v0, v0

    .line 1453467
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    goto :goto_0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 1453468
    const-string v0, "after_animation"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->l:LX/0ad;

    sget-char v2, LX/0wn;->aC:C

    const-string v3, "no_upgrade"

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    if-eqz v0, :cond_0

    .line 1453469
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    invoke-interface {v0}, LX/21n;->l()V

    .line 1453470
    :cond_0
    return-void
.end method

.method public static v(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)Z
    .locals 2

    .prologue
    .line 1453471
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    if-eqz v0, :cond_0

    sget-object v0, LX/1lD;->LOADING:LX/1lD;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    .line 1453472
    iget-object p0, v1, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-object v1, p0

    .line 1453473
    iget-object p0, v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    move-object v1, p0

    .line 1453474
    invoke-virtual {v0, v1}, LX/1lD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1453475
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453476
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v0, v1

    .line 1453477
    if-eqz v0, :cond_1

    .line 1453478
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453479
    iget-object p0, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v1, p0

    .line 1453480
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    .line 1453481
    :cond_0
    :goto_0
    return-void

    .line 1453482
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453483
    iget-boolean v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    move v0, v1

    .line 1453484
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/21y;->isReverseOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1453485
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->f()V

    goto :goto_0
.end method

.method private x()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1453486
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->U:Landroid/view/ContextThemeWrapper;

    if-nez v0, :cond_0

    .line 1453487
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0606

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->U:Landroid/view/ContextThemeWrapper;

    .line 1453488
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->U:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1453489
    const-string v0, "story_feedback_flyout"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1453490
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1453491
    if-nez p1, :cond_0

    .line 1453492
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 1453493
    :cond_0
    if-eqz p1, :cond_1

    .line 1453494
    const-string v0, "feedbackParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453495
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    sget-object v1, LX/21B;->DEFAULT_FEEDBACK:LX/21B;

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a(LX/21B;)V

    .line 1453496
    const-string v0, "loadingState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->N:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1453497
    const-string v0, "feedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "hasFetchedFeedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1453498
    const-string v0, "feedback"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453499
    const-string v0, "hasFetchedFeedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    .line 1453500
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453501
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453502
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1453503
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453504
    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453505
    iget-object v4, v3, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v3, v4

    .line 1453506
    iget-object v4, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->A:LX/9FH;

    .line 1453507
    iget-wide v9, v3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v5, v9

    .line 1453508
    invoke-virtual {v4, v5, v6}, LX/9FH;->a(J)LX/9FG;

    move-result-object v4

    .line 1453509
    sget-object v5, LX/9FF;->TOP_LEVEL:LX/9FF;

    invoke-virtual {v4, v5}, LX/9FG;->a(LX/9FF;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1453510
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1453511
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->y:LX/0pf;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v0

    .line 1453512
    if-eqz v0, :cond_2

    .line 1453513
    iget-object v1, v0, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 1453514
    if-eqz v1, :cond_2

    .line 1453515
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->z:LX/0SI;

    .line 1453516
    iget-object v3, v0, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v3

    .line 1453517
    invoke-interface {v1, v0}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 1453518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ag:Z

    .line 1453519
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_3

    .line 1453520
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453521
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v1

    .line 1453522
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453523
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    if-nez v0, :cond_4

    .line 1453524
    invoke-static {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    .line 1453525
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    :cond_5
    const/4 v0, 0x1

    .line 1453526
    :goto_1
    if-nez v0, :cond_8

    .line 1453527
    :goto_2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    .line 1453528
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ab:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1453529
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->x()Landroid/content/Context;

    move-result-object v0

    .line 1453530
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->l:LX/0ad;

    sget-short v3, LX/0wn;->a:S

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    .line 1453531
    :goto_3
    new-instance v3, LX/9C2;

    invoke-direct {v3, p0, v0, p1}, LX/9C2;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-interface {v1, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 1453532
    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->W:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1453533
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->o:LX/193;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "default_feedback_scroll_perf"

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->T:LX/195;

    .line 1453534
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->i()V

    .line 1453535
    return-void

    .line 1453536
    :cond_6
    iget-object v5, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-static {v5}, LX/8qL;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;

    move-result-object v5

    invoke-static {v3}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v3

    .line 1453537
    iget-wide v9, v4, LX/9FG;->b:J

    move-wide v7, v9

    .line 1453538
    iput-wide v7, v3, LX/21A;->j:J

    .line 1453539
    move-object v3, v3

    .line 1453540
    invoke-virtual {v3}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v3

    .line 1453541
    iput-object v3, v5, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1453542
    move-object v3, v5

    .line 1453543
    invoke-virtual {v3}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    goto/16 :goto_0

    .line 1453544
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 1453545
    :cond_8
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453546
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1453547
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453548
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1453549
    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->l()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1453550
    :goto_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "BgInflatableFeedbackFragment_FeedbackNullIDs"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Feedback passed to BgInflatableFeedbackFragment has a null id: id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", legacyapipostid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", loggingparams: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1453551
    :cond_9
    const-string v0, "no logging debug info"

    move-object v1, v0

    goto :goto_4

    .line 1453552
    :cond_a
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0TD;

    goto/16 :goto_3
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1453553
    if-nez p1, :cond_0

    .line 1453554
    :goto_0
    return-void

    .line 1453555
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1453556
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1453557
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->L:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 3

    .prologue
    .line 1453558
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1453559
    :goto_0
    return-void

    .line 1453560
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ak:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1453561
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ee;

    invoke-virtual {v0}, LX/9Ee;->a()V

    .line 1453562
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9E7;

    invoke-virtual {v0}, LX/9E7;->a()V

    .line 1453563
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    if-eqz v0, :cond_1

    .line 1453564
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->R:LX/9CA;

    invoke-virtual {v0, p1, v1, v2}, LX/9Dj;->a(Lcom/facebook/fbservice/service/ServiceException;Landroid/content/res/Resources;LX/1DI;)V

    .line 1453565
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0, p1}, LX/1nK;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1453269
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->y:LX/0pf;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453270
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1453271
    invoke-virtual {v0, v1}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v0

    .line 1453272
    if-eqz v0, :cond_0

    .line 1453273
    iget-object v1, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 1453274
    if-eqz v1, :cond_0

    .line 1453275
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v1

    .line 1453276
    iget-object v2, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v2

    .line 1453277
    invoke-static {v0}, LX/6Wy;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1453278
    iput-object v0, v1, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1453279
    move-object v0, v1

    .line 1453280
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    .line 1453281
    :cond_0
    iput-object p1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453282
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ad:Z

    if-eqz v0, :cond_1

    .line 1453283
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1453284
    if-nez v0, :cond_2

    .line 1453285
    :cond_1
    :goto_0
    return-void

    .line 1453286
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1453287
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453288
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 1453289
    invoke-static {v0, v1}, LX/8qM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1453290
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    monitor-enter v2

    .line 1453291
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 1453292
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1453293
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1453294
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->w:LX/9Dd;

    instance-of v0, v0, LX/21l;

    if-eqz v0, :cond_1

    .line 1453295
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->w:LX/9Dd;

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453296
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v2

    .line 1453297
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453298
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v2

    .line 1453299
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1453300
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_2
    invoke-interface {v1, v0}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1453566
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1453091
    iput-object p1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->aa:Ljava/lang/String;

    .line 1453092
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1453095
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1453096
    if-nez v2, :cond_1

    .line 1453097
    :cond_0
    :goto_0
    return v0

    .line 1453098
    :cond_1
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v2, p1, p2}, LX/9DG;->a(FF)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 1453099
    goto :goto_0

    .line 1453100
    :cond_2
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    if-eqz v2, :cond_0

    if-eqz p3, :cond_0

    .line 1453101
    sget-object v0, LX/9C0;->a:[I

    invoke-virtual {p3}, LX/31M;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 1453102
    goto :goto_0

    .line 1453103
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->l()Z

    move-result v0

    goto :goto_0

    .line 1453104
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->n()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1453105
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1453106
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1453107
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1453108
    :goto_0
    return-object v0

    .line 1453109
    :cond_0
    const-string v2, "feedbackParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453110
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1453111
    goto :goto_0

    .line 1453112
    :cond_1
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v2, v2

    .line 1453113
    if-eqz v2, :cond_4

    .line 1453114
    invoke-static {v2}, LX/21y;->isRanked(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1453115
    const-string v3, "ranked_comments"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453116
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 1453117
    const-string v3, "post_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453118
    :cond_3
    :goto_1
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v2, v2

    .line 1453119
    if-eqz v2, :cond_4

    .line 1453120
    const-string v2, "source_group_id"

    .line 1453121
    iget-object v3, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v0, v3

    .line 1453122
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    .line 1453123
    goto :goto_0

    .line 1453124
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1453125
    const-string v2, "post_id"

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1453126
    invoke-static {p1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->u:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1453127
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->v:LX/3H7;

    invoke-virtual {v0, p1}, LX/3H7;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/9Bx;

    invoke-direct {v1, p0, p1}, LX/9Bx;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1453128
    :goto_0
    return-void

    .line 1453129
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->d(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1453130
    invoke-static {p1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->u:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1453131
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->v:LX/3H7;

    invoke-virtual {v0, p1}, LX/3H7;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/9By;

    invoke-direct {v1, p0, p1}, LX/9By;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1453132
    :goto_0
    return-void

    .line 1453133
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->h(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1453134
    const-string v0, "flyout_feedback_animation_perf"

    return-object v0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1453135
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1453136
    const-string v0, "Feedback Params"

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {v2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453137
    const-string v2, "Has Fetched Feedback: "

    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    if-eqz v0, :cond_1

    const-string v0, "True"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453138
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 1453139
    :try_start_0
    const-string v2, "Feedback Object"

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-virtual {v0}, LX/0lC;->g()LX/4ps;

    move-result-object v0

    invoke-virtual {v0}, LX/4ps;->a()LX/4ps;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v3}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1453140
    :cond_0
    :goto_1
    return-object v1

    .line 1453141
    :cond_1
    const-string v0, "False"

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final gj_()V
    .locals 1

    .prologue
    .line 1453142
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->A()V

    .line 1453143
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->x()V

    .line 1453144
    return-void
.end method

.method public final gk_()V
    .locals 1

    .prologue
    .line 1453145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ae:Z

    .line 1453146
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ad:Z

    if-eqz v0, :cond_0

    .line 1453147
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->t()V

    .line 1453148
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->u()V

    .line 1453149
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->y()V

    .line 1453150
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->B()V

    .line 1453151
    return-void
.end method

.method public final hu_()Z
    .locals 2

    .prologue
    .line 1453152
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9DG;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1453153
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_0

    .line 1453154
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->i()Z

    .line 1453155
    :cond_0
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 1

    .prologue
    .line 1453156
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    if-nez v0, :cond_0

    .line 1453157
    const/4 v0, 0x0

    .line 1453158
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    invoke-interface {v0}, LX/21n;->getSelfAsView()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x16e371fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1453159
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->n()V

    .line 1453160
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1453161
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->o()V

    .line 1453162
    const/16 v1, 0x2b

    const v2, 0x51037297

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1453163
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1453164
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/9C7;

    invoke-direct {v1, p0, p1, p2, p3}, LX/9C7;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;IILandroid/content/Intent;)V

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1453165
    return-void
.end method

.method public final onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1453166
    const/4 v0, 0x0

    .line 1453167
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->f:LX/8qM;

    .line 1453168
    iget-boolean v2, v1, LX/8qM;->a:Z

    move v1, v2

    .line 1453169
    if-nez v1, :cond_1

    .line 1453170
    new-instance v0, LX/9C9;

    invoke-direct {v0, p0}, LX/9C9;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    .line 1453171
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1453172
    :cond_0
    :goto_0
    return-object v0

    .line 1453173
    :cond_1
    if-eqz p3, :cond_0

    .line 1453174
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x215169ef

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1453175
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->j()V

    .line 1453176
    iput-boolean v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ac:Z

    .line 1453177
    iput-boolean v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ad:Z

    .line 1453178
    iput-boolean v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ae:Z

    .line 1453179
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->x()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1453180
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string p1, "accessibility"

    invoke-virtual {v2, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    .line 1453181
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {v2}, LX/1d8;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1453182
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    .line 1453183
    :goto_1
    new-instance p1, LX/9C5;

    invoke-direct {p1, p0, v1, p2}, LX/9C5;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    invoke-interface {v2, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 1453184
    iput-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1453185
    const v2, 0x7f030654

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1453186
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v2}, LX/1nK;->k()V

    .line 1453187
    const/16 v2, 0x2b

    const v3, -0x2080cf52

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 1453188
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->t:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0TD;

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6e12e73e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1453189
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1453190
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ag:Z

    if-eqz v1, :cond_0

    .line 1453191
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->z:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    .line 1453192
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ab:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1453193
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->W:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_1

    .line 1453194
    const/16 v1, 0x2b

    const v2, -0x4fa87f0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1453195
    :goto_0
    return-void

    .line 1453196
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->W:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/9C8;

    invoke-direct {v2, p0}, LX/9C8;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    iget-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1453197
    const v1, 0x5710d5d8

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x2fbbf2a0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1453198
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1453199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ac:Z

    .line 1453200
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1453201
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1453202
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_1

    .line 1453203
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->b()V

    .line 1453204
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    if-eqz v0, :cond_2

    .line 1453205
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {v0}, LX/62O;->c()V

    .line 1453206
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ak:Ljava/lang/Runnable;

    invoke-static {v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1453207
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    if-eqz v0, :cond_3

    .line 1453208
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->A()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Z:Landroid/os/Parcelable;

    .line 1453209
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1453210
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->f(Landroid/view/View;)V

    .line 1453211
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->w()V

    .line 1453212
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->a(LX/0fx;)V

    .line 1453213
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->G:LX/0g8;

    .line 1453214
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    if-eqz v0, :cond_4

    .line 1453215
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1453216
    iput-object v3, v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->k:LX/9Df;

    .line 1453217
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1453218
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->S:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1453219
    :cond_4
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->K:LX/21n;

    .line 1453220
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->L:Landroid/view/ViewGroup;

    .line 1453221
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Q:Landroid/view/ViewStub;

    .line 1453222
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->V:LX/9EH;

    .line 1453223
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1453224
    iput-object v3, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1453225
    const/16 v0, 0x2b

    const v2, 0xd5d242

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7da719b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1453226
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->C()V

    .line 1453227
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->T:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 1453228
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1453229
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453230
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1453231
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453232
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 1453233
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v0, v1

    .line 1453234
    move-object v1, v0

    .line 1453235
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3E1;

    const-string v3, "story_feedback_flyout"

    invoke-virtual {v0, v1, v3}, LX/3E1;->a(LX/162;Ljava/lang/String;)V

    .line 1453236
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->n:LX/2tj;

    invoke-virtual {v0}, LX/2tj;->b()V

    .line 1453237
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_0

    .line 1453238
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->d()V

    .line 1453239
    :cond_0
    const v0, -0x2705ee9d

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    .line 1453240
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x34316df0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1453241
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->p()V

    .line 1453242
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->w()V

    .line 1453243
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1453244
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3E1;

    invoke-virtual {v0}, LX/3E1;->a()V

    .line 1453245
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3E1;

    invoke-virtual {v0}, LX/3E1;->b()V

    .line 1453246
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_0

    .line 1453247
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->e()V

    .line 1453248
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->q()V

    .line 1453249
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->r()V

    .line 1453250
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->z()V

    .line 1453251
    const/16 v0, 0x2b

    const v2, 0x42d244e2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1453252
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1453253
    const-string v0, "feedbackParams"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1453254
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    if-eqz v0, :cond_0

    .line 1453255
    const-string v0, "loadingState"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    .line 1453256
    iget-object v2, v1, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-object v1, v2

    .line 1453257
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1453258
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    if-eqz v0, :cond_1

    .line 1453259
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->b(Landroid/os/Bundle;)V

    .line 1453260
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->u:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1453261
    const-string v0, "feedback"

    iget-object v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1453262
    const-string v0, "hasFetchedFeedback"

    iget-boolean v1, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->af:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1453263
    :cond_2
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1453264
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 1453265
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->b:Ljava/lang/String;

    const-string v2, "onViewCreated() view is not a ViewGroup"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1453266
    :goto_0
    return-void

    .line 1453267
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->W:Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1453268
    iget-object v0, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/9C6;

    invoke-direct {v1, p0, p1, p2}, LX/9C6;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->s:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
