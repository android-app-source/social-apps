.class public Lcom/facebook/feedback/ui/SpeechCommentDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public final n:Landroid/text/TextWatcher;

.field public o:Lcom/facebook/resources/ui/FbEditText;

.field public p:Landroid/widget/ImageView;

.field public q:Lcom/facebook/resources/ui/FbButton;

.field public r:Lcom/facebook/resources/ui/FbButton;

.field public s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Landroid/view/View;

.field public u:Landroid/animation/AnimatorSet;

.field public v:LX/9F0;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1457672
    const-class v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1457782
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1457783
    new-instance v0, LX/9Es;

    invoke-direct {v0, p0}, LX/9Es;-><init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->n:Landroid/text/TextWatcher;

    .line 1457784
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1457785
    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->x:LX/0Ot;

    .line 1457786
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1457787
    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->y:LX/0Ot;

    .line 1457788
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedback/ui/SpeechCommentDialog;

    const/16 v2, 0x259

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x798

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, Lcom/facebook/feedback/ui/SpeechCommentDialog;->x:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/feedback/ui/SpeechCommentDialog;->y:LX/0Ot;

    return-void
.end method

.method private n()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x2

    .line 1457771
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->u:Landroid/animation/AnimatorSet;

    .line 1457772
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->t:Landroid/view/View;

    const-string v1, "scaleX"

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1457773
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->t:Landroid/view/View;

    const-string v2, "scaleY"

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1457774
    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1457775
    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1457776
    invoke-virtual {v1, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1457777
    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1457778
    iget-object v2, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->u:Landroid/animation/AnimatorSet;

    new-array v3, v4, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1457779
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->u:Landroid/animation/AnimatorSet;

    new-instance v1, LX/9Ex;

    invoke-direct {v1, p0}, LX/9Ex;-><init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1457780
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3fb9999a    # 1.45f
    .end array-data

    .line 1457781
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3fb9999a    # 1.45f
    .end array-data
.end method

.method public static p(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1457765
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1457766
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1457767
    iget-object v2, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1457768
    return-void

    :cond_0
    move v0, v1

    .line 1457769
    goto :goto_0

    .line 1457770
    :cond_1
    const/4 v1, 0x4

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1457763
    new-instance v0, LX/9Et;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/9Et;-><init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;Landroid/content/Context;I)V

    .line 1457764
    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1457760
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1457761
    invoke-static {p0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    .line 1457762
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1457756
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20q;

    invoke-virtual {v0}, LX/20q;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1457757
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 1457758
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1457759
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1457789
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x77f8b617

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457752
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1457753
    const-class v1, Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-static {v1, p0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 1457754
    const v1, 0x7f0e0613

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1457755
    const/16 v1, 0x2b

    const v2, 0x66d86a0a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6e42e60c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457751
    const v1, 0x7f031393

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x3fbde26a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x53cb6f4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457748
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 1457749
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    .line 1457750
    const/16 v1, 0x2b

    const v2, -0x121fb885

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x2a418667

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457735
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1457736
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457737
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457738
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457739
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1457740
    iget-object v1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->n:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1457741
    iput-object v3, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->q:Lcom/facebook/resources/ui/FbButton;

    .line 1457742
    iput-object v3, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->r:Lcom/facebook/resources/ui/FbButton;

    .line 1457743
    iput-object v3, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p:Landroid/widget/ImageView;

    .line 1457744
    iput-object v3, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->t:Landroid/view/View;

    .line 1457745
    iput-object v3, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1457746
    iput-object v3, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    .line 1457747
    const/16 v1, 0x2b

    const v2, -0x2ff4ad62

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 5

    .prologue
    .line 1457712
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    if-nez v0, :cond_0

    .line 1457713
    :goto_0
    return-void

    .line 1457714
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    .line 1457715
    invoke-static {v0}, LX/9F0;->g(LX/9F0;)V

    .line 1457716
    iget-boolean v1, v0, LX/9F0;->h:Z

    if-nez v1, :cond_2

    .line 1457717
    iget-object v1, v0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v1}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->k()Ljava/lang/String;

    move-result-object v1

    .line 1457718
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1457719
    iget-object v2, v0, LX/9F0;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2, v1}, LX/9F0;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v2

    .line 1457720
    iget-object v1, v0, LX/9F0;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8pb;

    iget-object v3, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, LX/8pb;->a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Z

    .line 1457721
    :cond_1
    iget-object v1, v0, LX/9F0;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9D5;

    .line 1457722
    iget-object v2, v1, LX/9D5;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0if;

    .line 1457723
    sget-object v3, LX/0ig;->M:LX/0ih;

    move-object v3, v3

    .line 1457724
    const-string v4, "user_didn\'t_post_comment_after_speech"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457725
    :cond_2
    const/4 v1, 0x0

    .line 1457726
    iput-object v1, v0, LX/9F0;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1457727
    iput-object v1, v0, LX/9F0;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457728
    iput-object v1, v0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    .line 1457729
    iput-object v1, v0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    .line 1457730
    iget-object v1, v0, LX/9F0;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9D5;

    .line 1457731
    iget-object v2, v1, LX/9D5;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0if;

    .line 1457732
    sget-object v3, LX/0ig;->M:LX/0ih;

    move-object v3, v3

    .line 1457733
    invoke-virtual {v2, v3}, LX/0if;->c(LX/0ih;)V

    .line 1457734
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4add2b0f    # 7247239.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457701
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1457702
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1457703
    if-nez v1, :cond_0

    .line 1457704
    const/16 v1, 0x2b

    const v2, 0x734d600d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1457705
    :goto_0
    return-void

    .line 1457706
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b28

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 1457707
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 1457708
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 1457709
    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Landroid/view/Window;->setGravity(I)V

    .line 1457710
    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    .line 1457711
    const v1, -0x2f90ba7a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1457673
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1457674
    const v0, 0x7f0d2d3b

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    .line 1457675
    const v0, 0x7f0d2d3a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1457676
    const v0, 0x7f0d1b1a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->q:Lcom/facebook/resources/ui/FbButton;

    .line 1457677
    const v0, 0x7f0d2d3c

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->t:Landroid/view/View;

    .line 1457678
    const v0, 0x7f0d067d

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->r:Lcom/facebook/resources/ui/FbButton;

    .line 1457679
    const v0, 0x7f0d2d3d

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p:Landroid/widget/ImageView;

    .line 1457680
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1457681
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object p1, Lcom/facebook/feedback/ui/SpeechCommentDialog;->m:Ljava/lang/String;

    const-string p2, "No title passed into Dialog"

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457682
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object p1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->n:Landroid/text/TextWatcher;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1457683
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->r:Lcom/facebook/resources/ui/FbButton;

    new-instance p1, LX/9Eu;

    invoke-direct {p1, p0}, LX/9Eu;-><init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457684
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->q:Lcom/facebook/resources/ui/FbButton;

    new-instance p1, LX/9Ev;

    invoke-direct {p1, p0}, LX/9Ev;-><init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457685
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p:Landroid/widget/ImageView;

    new-instance p1, LX/9Ew;

    invoke-direct {p1, p0}, LX/9Ew;-><init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1457686
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->n()V

    .line 1457687
    invoke-static {p0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V

    .line 1457688
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    .line 1457689
    invoke-static {v0}, LX/9F0;->e(LX/9F0;)V

    .line 1457690
    iget-object p0, v0, LX/9F0;->k:LX/0i4;

    iget-object p1, v0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p0, p1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object p0

    sget-object p1, LX/9F0;->b:[Ljava/lang/String;

    invoke-virtual {p0, p1}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1457691
    invoke-virtual {v0}, LX/9F0;->d()V

    .line 1457692
    :goto_1
    return-void

    .line 1457693
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, p0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->w:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1457694
    :cond_1
    iget-object p1, v0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    iget-object p0, v0, LX/9F0;->l:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/20q;

    invoke-virtual {p0}, LX/20q;->c()Ljava/lang/String;

    move-result-object p0

    .line 1457695
    iget-object p2, p1, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p2, p0}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1457696
    iget-object p0, v0, LX/9F0;->n:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9D5;

    .line 1457697
    iget-object p1, p0, LX/9D5;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0if;

    .line 1457698
    sget-object p2, LX/0ig;->M:LX/0ih;

    move-object p2, p2

    .line 1457699
    const-string v0, "ask_user_for_permissions"

    invoke-virtual {p1, p2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457700
    goto :goto_1
.end method
