.class public final Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V
    .locals 0

    .prologue
    .line 1452570
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1452571
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->d(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    if-nez v0, :cond_1

    .line 1452572
    :cond_0
    :goto_0
    return-void

    .line 1452573
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->M:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 1452574
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;)V

    .line 1452575
    const v2, 0x7f0d1191

    .line 1452576
    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 1452577
    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1452578
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, v2, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v2, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 1452579
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v1, v0}, LX/62O;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    goto :goto_0
.end method
