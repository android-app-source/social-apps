.class public final Lcom/facebook/feedback/ui/CommentListScrollStateController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/9D1;


# direct methods
.method public constructor <init>(LX/9D1;I)V
    .locals 0

    .prologue
    .line 1454566
    iput-object p1, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iput p2, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1454567
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-object v0, v0, LX/9D1;->c:LX/0g8;

    if-nez v0, :cond_0

    .line 1454568
    :goto_0
    return-void

    .line 1454569
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget v3, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->a:I

    .line 1454570
    iget-object v4, v0, LX/9D1;->c:LX/0g8;

    if-eqz v4, :cond_4

    iget-object v4, v0, LX/9D1;->c:LX/0g8;

    invoke-interface {v4}, LX/0g8;->q()I

    move-result v4

    if-gt v4, v3, :cond_4

    iget-object v4, v0, LX/9D1;->c:LX/0g8;

    invoke-interface {v4}, LX/0g8;->r()I

    move-result v4

    if-lt v4, v3, :cond_4

    const/4 v4, 0x1

    :goto_1
    move v0, v4

    .line 1454571
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 1454572
    :goto_2
    iput-boolean v0, v2, LX/9D1;->f:Z

    .line 1454573
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-object v0, v0, LX/9D1;->k:LX/9Im;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-boolean v0, v0, LX/9D1;->f:Z

    if-eqz v0, :cond_1

    .line 1454574
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-object v0, v0, LX/9D1;->k:LX/9Im;

    .line 1454575
    iget-boolean v2, v0, LX/9Im;->e:Z

    if-nez v2, :cond_5

    .line 1454576
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-boolean v0, v0, LX/9D1;->f:Z

    if-eqz v0, :cond_3

    .line 1454577
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-object v0, v0, LX/9D1;->c:LX/0g8;

    iget v1, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->a:I

    invoke-interface {v0, v1}, LX/0g8;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1454578
    goto :goto_2

    .line 1454579
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->b:LX/9D1;

    iget-object v0, v0, LX/9D1;->c:LX/0g8;

    iget v2, p0, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;->a:I

    invoke-interface {v0, v2, v1}, LX/0g8;->c(II)V

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 1454580
    :cond_5
    iget-object v2, v0, LX/9Im;->a:LX/9Ik;

    invoke-virtual {v2}, LX/9Ik;->c()V

    .line 1454581
    iget-object v2, v0, LX/9Im;->a:LX/9Ik;

    const/4 v3, 0x0

    .line 1454582
    iput-boolean v3, v2, LX/9Ik;->g:Z

    .line 1454583
    iget-object v2, v0, LX/9Im;->c:LX/9Ij;

    invoke-virtual {v2}, LX/9Ij;->a()V

    goto :goto_3
.end method
