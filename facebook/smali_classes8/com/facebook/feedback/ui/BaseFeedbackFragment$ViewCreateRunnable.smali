.class public final Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

.field private final b:Landroid/view/View;

.field private final c:Landroid/os/Bundle;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1452627
    iput-object p1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1452628
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->d:I

    .line 1452629
    iput-object p2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->b:Landroid/view/View;

    .line 1452630
    iput-object p3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->c:Landroid/os/Bundle;

    .line 1452631
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1452588
    iget v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->d:I

    .line 1452589
    iget v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->d:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    .line 1452590
    :cond_0
    :goto_0
    return-void

    .line 1452591
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-boolean v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->O:Z

    if-nez v0, :cond_2

    .line 1452592
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    const v1, -0x46a4d3cd

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1452593
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    if-eqz v0, :cond_0

    .line 1452594
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, LX/9DG;->a(Landroid/os/Bundle;)V

    .line 1452595
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->b:Landroid/view/View;

    const v2, 0x7f0d009d

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/21n;

    .line 1452596
    iput-object v0, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->G:LX/21n;

    .line 1452597
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->b:Landroid/view/View;

    const v3, 0x7f0d118d

    .line 1452598
    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    move-object v0, v5

    .line 1452599
    check-cast v0, Landroid/view/ViewGroup;

    .line 1452600
    iput-object v0, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->H:Landroid/view/ViewGroup;

    .line 1452601
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->A:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, v2, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    iget-object v3, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v3, v3, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->I:LX/62C;

    invoke-virtual {v0, v1, v2, v3}, LX/9DG;->a(Landroid/view/View;LX/0g8;Landroid/widget/BaseAdapter;)V

    .line 1452602
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->I:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1452603
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->b:Landroid/view/View;

    const v3, 0x7f0d118c

    .line 1452604
    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    move-object v0, v5

    .line 1452605
    check-cast v0, Landroid/view/ViewStub;

    .line 1452606
    iput-object v0, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->M:Landroid/view/ViewStub;

    .line 1452607
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m:LX/0ad;

    sget-short v1, LX/0wn;->L:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1452608
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->M:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 1452609
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Landroid/view/View;)V

    .line 1452610
    const v2, 0x7f0d1191

    .line 1452611
    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 1452612
    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1452613
    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v2, v2, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    invoke-interface {v2, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 1452614
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->K:LX/9Dj;

    invoke-virtual {v1, v0}, LX/62O;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 1452615
    :cond_3
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1452616
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lt;

    .line 1452617
    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->w:LX/0g8;

    new-instance v2, LX/9Bv;

    invoke-direct {v2, p0, v0}, LX/9Bv;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;LX/3lt;)V

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 1452618
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->o:LX/0if;

    sget-object v1, LX/0ig;->W:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1452619
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    const/4 v1, 0x1

    .line 1452620
    iput-boolean v1, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->P:Z

    .line 1452621
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1452622
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    .line 1452623
    invoke-static {v0, v4}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Z)V

    .line 1452624
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->m:LX/0ad;

    sget-short v1, LX/0wn;->L:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->y:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->d(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1452625
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->l:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;

    iget-object v2, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-direct {v1, v2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment$LoadingIndicatorRunnable;-><init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;)V

    const v2, 0xbdef29

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1452626
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/ui/BaseFeedbackFragment$ViewCreateRunnable;->a:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->p()V

    goto/16 :goto_0
.end method
