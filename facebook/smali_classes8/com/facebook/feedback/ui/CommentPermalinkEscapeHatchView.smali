.class public Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3Q9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1454702
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1454703
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1454691
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1454692
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1454699
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1454700
    const-class v0, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1454701
    return-void
.end method

.method private static a(Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;Lcom/facebook/content/SecureContextHelper;LX/3Q9;LX/0hy;)V
    .locals 0

    .prologue
    .line 1454698
    iput-object p1, p0, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->b:LX/3Q9;

    iput-object p3, p0, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->c:LX/0hy;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/3Q9;->b(LX/0QB;)LX/3Q9;

    move-result-object v1

    check-cast v1, LX/3Q9;

    invoke-static {v2}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v2

    check-cast v2, LX/0hy;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->a(Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;Lcom/facebook/content/SecureContextHelper;LX/3Q9;LX/0hy;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/api/ufiservices/FetchSingleCommentParams;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 10

    .prologue
    .line 1454693
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->setText(Ljava/lang/CharSequence;)V

    .line 1454694
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->b:LX/3Q9;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v2, -0x1

    const/4 v8, 0x0

    .line 1454695
    iget-object v9, v0, LX/3Q9;->a:LX/0Sh;

    new-instance v3, LX/5QB;

    move-object v4, v0

    move-object v5, p0

    move-object v6, v1

    move v7, v2

    invoke-direct/range {v3 .. v7}, LX/5QB;-><init>(LX/3Q9;Landroid/widget/TextView;Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)V

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v9, v3, v4}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 1454696
    new-instance v0, LX/9D2;

    invoke-direct {v0, p0, p2, p3}, LX/9D2;-><init>(Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;Lcom/facebook/api/ufiservices/FetchSingleCommentParams;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1454697
    return-void
.end method
