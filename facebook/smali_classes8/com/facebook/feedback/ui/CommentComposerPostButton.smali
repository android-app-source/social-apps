.class public Lcom/facebook/feedback/ui/CommentComposerPostButton;
.super Landroid/widget/ImageButton;
.source ""


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I


# instance fields
.field private d:LX/A7L;

.field public e:LX/21m;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1454082
    new-array v0, v2, [I

    sput-object v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->a:[I

    .line 1454083
    new-array v0, v3, [I

    const v1, 0x7f0105b2

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->b:[I

    .line 1454084
    const/4 v0, 0x2

    new-array v0, v0, [I

    const v1, 0x10100a1

    aput v1, v0, v2

    const v1, 0x7f0105b2

    aput v1, v0, v3

    sput-object v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1454085
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1454086
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1454087
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1454088
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->f:Z

    .line 1454089
    invoke-direct {p0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->a()V

    .line 1454090
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1454091
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setSoundEffectsEnabled(Z)V

    .line 1454092
    new-instance v0, LX/A7L;

    invoke-direct {v0}, LX/A7L;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->d:LX/A7L;

    .line 1454093
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021806

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1454094
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02122a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1454095
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1454096
    iget-object v2, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->d:LX/A7L;

    sget-object v3, Lcom/facebook/feedback/ui/CommentComposerPostButton;->c:[I

    const v4, -0xa76f01

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/A7L;->a([ILjava/lang/Integer;Landroid/graphics/drawable/Drawable;)V

    .line 1454097
    iget-object v2, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->d:LX/A7L;

    sget-object v3, Lcom/facebook/feedback/ui/CommentComposerPostButton;->b:[I

    const/high16 v4, 0x4d000000    # 1.34217728E8f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/A7L;->a([ILjava/lang/Integer;Landroid/graphics/drawable/Drawable;)V

    .line 1454098
    iget-object v1, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->d:LX/A7L;

    sget-object v2, Lcom/facebook/feedback/ui/CommentComposerPostButton;->a:[I

    invoke-virtual {v1, v2, v0}, LX/A7L;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1454099
    iget-object v0, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->d:LX/A7L;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1454100
    new-instance v0, LX/9CR;

    invoke-direct {v0, p0}, LX/9CR;-><init>(Lcom/facebook/feedback/ui/CommentComposerPostButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1454101
    return-void
.end method


# virtual methods
.method public getShowSticker()Z
    .locals 1

    .prologue
    .line 1454102
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->f:Z

    return v0
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 1454103
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/ImageButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1454104
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->f:Z

    if-eqz v1, :cond_0

    .line 1454105
    sget-object v1, Lcom/facebook/feedback/ui/CommentComposerPostButton;->b:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    .line 1454106
    :cond_0
    return-object v0
.end method

.method public setListener(LX/21m;)V
    .locals 0

    .prologue
    .line 1454107
    iput-object p1, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->e:LX/21m;

    .line 1454108
    return-void
.end method

.method public setShowSticker(Z)V
    .locals 2

    .prologue
    .line 1454109
    iput-boolean p1, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->f:Z

    .line 1454110
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->f:Z

    if-eqz v0, :cond_0

    .line 1454111
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0809d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1454112
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->refreshDrawableState()V

    .line 1454113
    return-void

    .line 1454114
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08121e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
