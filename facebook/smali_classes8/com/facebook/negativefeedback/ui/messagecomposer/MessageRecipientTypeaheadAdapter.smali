.class public Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/9WZ;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:I


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:LX/9Wa;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1501331
    const-class v0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1501332
    const v0, 0x7f030ad8

    sput v0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/9Wa;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1501368
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1501369
    iput-object p2, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->d:LX/9Wa;

    .line 1501370
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->d:LX/9Wa;

    .line 1501371
    iput-object p0, v0, LX/9Wa;->c:LX/9WZ;

    .line 1501372
    iput-object p1, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->c:Landroid/content/Context;

    .line 1501373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->e:Ljava/util/List;

    .line 1501374
    return-void
.end method

.method public static a(Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 1

    .prologue
    .line 1501375
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1501362
    if-nez p1, :cond_0

    .line 1501363
    :goto_0
    return-void

    .line 1501364
    :cond_0
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1501365
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1501366
    const v0, -0x62cfca71

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1501367
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1501361
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->d:LX/9Wa;

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1501360
    invoke-static {p0, p1}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->a(Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 4

    .prologue
    .line 1501357
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1501358
    iget-wide v2, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v0, v2

    .line 1501359
    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1501333
    if-nez p2, :cond_0

    .line 1501334
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1501335
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->a(Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v1

    .line 1501336
    const v0, 0x7f0d1a16

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1501337
    iget-object v2, v1, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1501338
    if-eqz v2, :cond_4

    .line 1501339
    iget-object v2, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v2, v2

    .line 1501340
    sget-object p3, LX/7Gr;->TEXT:LX/7Gr;

    if-eq v2, p3, :cond_4

    .line 1501341
    iget-object v2, v1, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1501342
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object p3, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1501343
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1501344
    :cond_1
    :goto_0
    iget-object v2, v1, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1501345
    if-nez v2, :cond_2

    .line 1501346
    const/4 v2, 0x0

    sget-object p3, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1501347
    :cond_2
    const v0, 0x7f0d1b9e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1501348
    iget-object v2, v1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v1, v2

    .line 1501349
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    .line 1501350
    if-nez v1, :cond_3

    .line 1501351
    iget-object v1, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;->c:Landroid/content/Context;

    const v2, 0x7f081394

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1501352
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1501353
    return-object p2

    .line 1501354
    :cond_4
    iget-object v2, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v2, v2

    .line 1501355
    sget-object p3, LX/7Gr;->TEXT:LX/7Gr;

    if-ne v2, p3, :cond_1

    .line 1501356
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
