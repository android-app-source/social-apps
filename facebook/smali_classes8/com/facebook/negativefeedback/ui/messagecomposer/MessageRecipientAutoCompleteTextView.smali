.class public Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;
.super Lcom/facebook/resources/ui/FbAutoCompleteTextView;
.source ""


# instance fields
.field public b:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1501314
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1501315
    const-class v0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    invoke-static {v0, p0}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1501316
    new-instance v0, LX/9WY;

    invoke-direct {v0, p0}, LX/9WY;-><init>(Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;)V

    .line 1501317
    new-instance v1, LX/9WW;

    invoke-direct {v1, v0}, LX/9WW;-><init>(LX/9WY;)V

    move-object v1, v1

    .line 1501318
    invoke-virtual {p0, v1}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1501319
    new-instance v1, LX/9WX;

    invoke-direct {v1, v0}, LX/9WX;-><init>(LX/9WY;)V

    move-object v0, v1

    .line 1501320
    invoke-virtual {p0, v0}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1501321
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->b:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;

    invoke-virtual {p0, v0}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1501322
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    new-instance v3, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    new-instance p1, LX/9Wa;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {v0}, LX/9WV;->a(LX/0QB;)LX/9WV;

    move-result-object v4

    check-cast v4, LX/9WV;

    invoke-direct {p1, v2, v4}, LX/9Wa;-><init>(LX/0Sh;LX/9WV;)V

    move-object v2, p1

    check-cast v2, LX/9Wa;

    invoke-direct {v3, v1, v2}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;-><init>(Landroid/content/Context;LX/9Wa;)V

    move-object v0, v3

    check-cast v0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;

    iput-object v0, p0, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->b:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientTypeaheadAdapter;

    return-void
.end method


# virtual methods
.method public getSelectedProfileId()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1501323
    invoke-virtual {p0}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1501324
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, Landroid/text/Annotation;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    .line 1501325
    array-length v1, v0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1501326
    const-string v0, ""

    .line 1501327
    :cond_0
    :goto_0
    return-object v0

    .line 1501328
    :cond_1
    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/text/Annotation;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1501329
    if-nez v0, :cond_0

    .line 1501330
    const-string v0, ""

    goto :goto_0
.end method
