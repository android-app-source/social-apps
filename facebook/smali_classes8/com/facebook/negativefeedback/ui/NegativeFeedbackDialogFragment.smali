.class public Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public m:LX/2EJ;

.field public n:Ljava/lang/String;

.field public o:J

.field public p:Ljava/lang/String;

.field public q:LX/9WC;

.field public r:LX/A7t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1501013
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1500922
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1500923
    const-string v1, "node_token"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500924
    const-string v1, "location"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500925
    const-string v1, "responsible_user"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1500926
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    new-instance v2, LX/9WC;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    const/16 v5, 0x1769

    invoke-static {v1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    new-instance v8, LX/9Ve;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct {v8, v7}, LX/9Ve;-><init>(LX/0Zb;)V

    move-object v7, v8

    check-cast v7, LX/9Ve;

    new-instance v9, LX/9Vf;

    invoke-static {v1}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v9, v8}, LX/9Vf;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    move-object v8, v9

    check-cast v8, LX/9Vf;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    const/16 v10, 0x122d

    invoke-static {v1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-static {v1}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v12

    check-cast v12, LX/0SI;

    invoke-direct/range {v2 .. v12}, LX/9WC;-><init>(LX/03V;LX/0Sh;LX/0Or;LX/0tX;LX/9Ve;LX/9Vf;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/1Ck;LX/0SI;)V

    move-object v0, v2

    check-cast v0, LX/9WC;

    invoke-static {v1}, LX/A7t;->b(LX/0QB;)LX/A7t;

    move-result-object v1

    check-cast v1, LX/A7t;

    iput-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    iput-object v1, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->r:LX/A7t;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1500987
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1500988
    if-eqz v0, :cond_0

    .line 1500989
    const-string v1, "node_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->n:Ljava/lang/String;

    .line 1500990
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->p:Ljava/lang/String;

    .line 1500991
    const-string v1, "responsible_user"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->o:J

    .line 1500992
    const-string v1, "extras"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1500993
    iget-object v1, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    const-string v3, "extras"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1500994
    iput-object v0, v1, LX/9WC;->y:Landroid/os/Bundle;

    .line 1500995
    :cond_0
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0e09a8

    invoke-direct {v0, v1, v3}, LX/31Y;-><init>(Landroid/content/Context;I)V

    .line 1500996
    invoke-virtual {v0, v2}, LX/0ju;->c(Z)LX/0ju;

    .line 1500997
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/view/View;)LX/0ju;

    .line 1500998
    invoke-virtual {v0, v2}, LX/0ju;->a(Z)LX/0ju;

    .line 1500999
    new-instance v1, LX/9WT;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, LX/9WT;-><init>(Landroid/content/Context;)V

    move v3, v2

    move v4, v2

    move v5, v2

    .line 1501000
    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1501001
    new-instance v1, LX/9WH;

    invoke-direct {v1}, LX/9WH;-><init>()V

    .line 1501002
    iget-object v2, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    invoke-virtual {v2, v1}, LX/9WC;->a(LX/9WH;)V

    .line 1501003
    const v2, 0x7f082432

    new-instance v3, LX/9WD;

    invoke-direct {v3, p0}, LX/9WD;-><init>(Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1501004
    const v2, 0x7f082431

    iget-object v3, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1501005
    iget-object v4, v3, LX/9WC;->D:Landroid/content/DialogInterface$OnClickListener;

    move-object v3, v4

    .line 1501006
    invoke-virtual {v0, v2, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1501007
    const v2, 0x7f082433

    iget-object v3, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1501008
    iget-object v4, v3, LX/9WC;->E:Landroid/content/DialogInterface$OnClickListener;

    move-object v3, v4

    .line 1501009
    invoke-virtual {v0, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1501010
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    .line 1501011
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    new-instance v2, LX/9WE;

    invoke-direct {v2, p0, v1}, LX/9WE;-><init>(Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;LX/9WH;)V

    invoke-virtual {v0, v2}, LX/2EJ;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 1501012
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1500986
    const-string v0, "negative_feedback"

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1500984
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1500985
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19288642

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1500981
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1500982
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1500983
    const/16 v1, 0x2b

    const v2, 0x40dcbe5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 1500935
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1500936
    iget-object v1, v0, LX/9WC;->r:LX/9Ve;

    iget-object v2, v0, LX/9WC;->k:LX/9Vx;

    .line 1500937
    iget-object v3, v2, LX/9Vx;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1500938
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "negativefeedback_cancel_flow"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500939
    invoke-static {v1, v3, v2}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500940
    iget-object v1, v0, LX/9WC;->s:LX/9Vf;

    .line 1500941
    iget-object v2, v1, LX/9Vf;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x4d0001

    const/4 p1, 0x4

    invoke-interface {v2, v3, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1500942
    iget-object v1, v0, LX/9WC;->w:Ljava/lang/String;

    sget-object v2, LX/0wD;->MESSENGER_THREAD_ACTION_PANEL:LX/0wD;

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_0

    iget-object v1, v0, LX/9WC;->w:Ljava/lang/String;

    sget-object v2, LX/0wD;->MESSENGER_CONTACT_DETAILS:LX/0wD;

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_4

    .line 1500943
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1500944
    iget-boolean v1, v0, LX/9WC;->C:Z

    move v0, v1

    .line 1500945
    if-nez v0, :cond_2

    .line 1500946
    :cond_1
    :goto_1
    return-void

    .line 1500947
    :cond_2
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1500948
    iget-object v1, v0, LX/9WC;->x:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1500949
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1500950
    invoke-static {v0}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1500951
    :goto_2
    new-instance v1, LX/A7p;

    const-string v2, "NFX_FEEDBACK"

    const-string v3, "FB4A_NFX_DIALOG"

    invoke-direct {v1, v2, v3}, LX/A7p;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500952
    iput-object v0, v1, LX/A7p;->c:Ljava/lang/String;

    .line 1500953
    move-object v0, v1

    .line 1500954
    iget-object v1, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->r:LX/A7t;

    .line 1500955
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 1500956
    invoke-virtual {v1, v0, v2}, LX/A7t;->a(LX/A7p;LX/0gc;)V

    goto :goto_1

    .line 1500957
    :cond_3
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1500958
    iget-object v1, v0, LX/9WC;->m:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    move-object v0, v1

    .line 1500959
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1500960
    iget-object v1, v0, LX/9WC;->m:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    move-object v0, v1

    .line 1500961
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1500962
    iget-object v0, p0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    .line 1500963
    iget-object v1, v0, LX/9WC;->m:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    move-object v0, v1

    .line 1500964
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1500965
    :cond_4
    iget-object v1, v0, LX/9WC;->u:LX/9Vy;

    instance-of v1, v1, LX/9WT;

    if-eqz v1, :cond_5

    .line 1500966
    iget-object v1, v0, LX/9WC;->v:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gt;

    const-string v2, "1415134408797047"

    .line 1500967
    iput-object v2, v1, LX/0gt;->a:Ljava/lang/String;

    .line 1500968
    move-object v1, v1

    .line 1500969
    iget-object v2, v0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v2}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gt;->a(Landroid/content/Context;)V

    .line 1500970
    :cond_5
    iget-object v1, v0, LX/9WC;->u:LX/9Vy;

    instance-of v1, v1, LX/9WP;

    if-eqz v1, :cond_0

    .line 1500971
    iget-object v1, v0, LX/9WC;->B:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1500972
    iget-object v1, v0, LX/9WC;->A:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1500973
    iget-object v1, v0, LX/9WC;->v:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gt;

    const-string v2, "1564957227079130"

    .line 1500974
    iput-object v2, v1, LX/0gt;->a:Ljava/lang/String;

    .line 1500975
    move-object v1, v1

    .line 1500976
    iget-object v2, v0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v2}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gt;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1500977
    :cond_6
    iget-object v1, v0, LX/9WC;->v:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gt;

    const-string v2, "1607155906162890"

    .line 1500978
    iput-object v2, v1, LX/0gt;->a:Ljava/lang/String;

    .line 1500979
    move-object v1, v1

    .line 1500980
    iget-object v2, v0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v2}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gt;->a(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6fc0cfff

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1500927
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1500928
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 1500929
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 1500930
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 1500931
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 1500932
    const/4 v3, -0x1

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1500933
    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1500934
    const/16 v1, 0x2b

    const v2, 0xd2f61c6

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
