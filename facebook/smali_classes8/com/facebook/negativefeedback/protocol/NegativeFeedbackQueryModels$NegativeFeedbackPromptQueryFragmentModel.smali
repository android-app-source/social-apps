.class public final Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x352013ce
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1500128
    const-class v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1500129
    const-class v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1500130
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1500131
    return-void
.end method

.method private j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1500132
    iget-object v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->g:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->g:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    .line 1500133
    iget-object v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->g:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1500134
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1500135
    invoke-virtual {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1500136
    invoke-direct {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1500137
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1500138
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1500139
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1500140
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1500141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1500142
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1500115
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1500116
    invoke-virtual {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1500117
    invoke-virtual {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1500118
    if-eqz v1, :cond_2

    .line 1500119
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    .line 1500120
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1500121
    :goto_0
    invoke-direct {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1500122
    invoke-direct {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    .line 1500123
    invoke-direct {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1500124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    .line 1500125
    iput-object v0, v1, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->g:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    .line 1500126
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1500127
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1500143
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1500144
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->e:Z

    .line 1500145
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1500113
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1500114
    iget-boolean v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->e:Z

    return v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1500111
    iget-object v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->f:Ljava/util/List;

    .line 1500112
    iget-object v0, p0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1500108
    new-instance v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    invoke-direct {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;-><init>()V

    .line 1500109
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1500110
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1500107
    invoke-direct {p0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1500105
    const v0, -0x369cf358    # -929994.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1500106
    const v0, 0x41733cfe

    return v0
.end method
