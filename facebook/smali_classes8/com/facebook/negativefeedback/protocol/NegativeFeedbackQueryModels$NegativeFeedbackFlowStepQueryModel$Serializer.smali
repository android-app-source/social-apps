.class public final Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1499675
    const-class v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel;

    new-instance v1, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1499676
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1499660
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1499662
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1499663
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1499664
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1499665
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1499666
    if-eqz v2, :cond_0

    .line 1499667
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1499668
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1499669
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1499670
    if-eqz v2, :cond_1

    .line 1499671
    const-string p0, "negative_feedback_prompt"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1499672
    invoke-static {v1, v2, p1, p2}, LX/9Vv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1499673
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1499674
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1499661
    check-cast p1, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel$Serializer;->a(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowStepQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
