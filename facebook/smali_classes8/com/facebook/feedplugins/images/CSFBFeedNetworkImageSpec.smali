.class public Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1463846
    const-class v0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;

    const-string v1, "ComponentScript"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463848
    iput-object p1, p0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;->b:LX/1nu;

    .line 1463849
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;
    .locals 4

    .prologue
    .line 1463850
    const-class v1, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;

    monitor-enter v1

    .line 1463851
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463852
    sput-object v2, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463853
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463854
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463855
    new-instance p0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;-><init>(LX/1nu;)V

    .line 1463856
    move-object v0, p0

    .line 1463857
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463858
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463859
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463860
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
