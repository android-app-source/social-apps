.class public Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Lcom/facebook/common/callercontext/CallerContext;

.field private final b:LX/3AP;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/AMh;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/AMf;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/AMk;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;LX/1Ck;LX/AMf;Landroid/os/Handler;)V
    .locals 11
    .param p11    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1666489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666490
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "msqrd_asset_download"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1666491
    new-instance v1, LX/3AP;

    const-string v4, "msqrd"

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    iput-object v1, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->b:LX/3AP;

    .line 1666492
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->c:LX/1Ck;

    .line 1666493
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    .line 1666494
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->e:Ljava/util/Map;

    .line 1666495
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->f:Landroid/os/Handler;

    .line 1666496
    return-void
.end method

.method public static a$redex0(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)V
    .locals 2

    .prologue
    .line 1666443
    iget-object v0, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->e:Ljava/util/Map;

    .line 1666444
    iget-object v1, p1, LX/AMT;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1666445
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1666446
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;
    .locals 13

    .prologue
    .line 1666497
    new-instance v0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v3

    check-cast v3, LX/1Gm;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v5

    check-cast v5, LX/1Gk;

    invoke-static {p0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v6

    check-cast v6, LX/1Gl;

    invoke-static {p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v7

    check-cast v7, LX/1Go;

    invoke-static {p0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v8

    check-cast v8, LX/13t;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    .line 1666498
    new-instance v12, LX/AMf;

    invoke-static {p0}, LX/2Nz;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v10

    check-cast v10, Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-direct {v12, v10, v11}, LX/AMf;-><init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/1Ck;)V

    .line 1666499
    move-object v10, v12

    .line 1666500
    check-cast v10, LX/AMf;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v11

    check-cast v11, Landroid/os/Handler;

    invoke-direct/range {v0 .. v11}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;LX/1Ck;LX/AMf;Landroid/os/Handler;)V

    .line 1666501
    return-object v0
.end method

.method public static b(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1666477
    iget-object v1, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    .line 1666478
    iget-boolean v0, p1, LX/AMT;->e:Z

    move v0, v0

    .line 1666479
    if-eqz v0, :cond_0

    .line 1666480
    iget-object v0, p1, LX/AMT;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1666481
    :goto_0
    invoke-virtual {v1, v0}, LX/AMf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1666482
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1666483
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1666484
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1666485
    :goto_1
    return-object v0

    .line 1666486
    :cond_0
    iget-object v0, p1, LX/AMT;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1666487
    goto :goto_0

    .line 1666488
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0Px;LX/AML;LX/AMK;)V
    .locals 12
    .param p3    # LX/AMK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;",
            "LX/AML;",
            "Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader$OnAssetDownloadProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1666447
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_4

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMT;

    .line 1666448
    invoke-static {p0, v0}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->b(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)Ljava/io/File;

    move-result-object v1

    .line 1666449
    if-eqz v1, :cond_0

    .line 1666450
    invoke-interface {p2, v0, v1}, LX/AML;->a(LX/AMT;Ljava/io/File;)V

    .line 1666451
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1666452
    :cond_0
    iget-object v1, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->e:Ljava/util/Map;

    .line 1666453
    iget-object v4, v0, LX/AMT;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1666454
    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1666455
    iget-object v1, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->e:Ljava/util/Map;

    .line 1666456
    iget-object v4, v0, LX/AMT;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1666457
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMk;

    .line 1666458
    iget-object v1, v0, LX/AMk;->c:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1666459
    if-eqz p3, :cond_2

    .line 1666460
    iget-wide v8, v0, LX/AMk;->e:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    .line 1666461
    iget-object v8, v0, LX/AMk;->b:LX/AMT;

    iget-wide v10, v0, LX/AMk;->e:J

    invoke-virtual {p3, v8, v10, v11}, LX/AMK;->a(LX/AMT;J)V

    .line 1666462
    :cond_1
    iget-object v8, v0, LX/AMk;->d:Ljava/util/Set;

    invoke-interface {v8, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1666463
    :cond_2
    goto :goto_1

    .line 1666464
    :cond_3
    iget-object v1, v0, LX/AMT;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1666465
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1666466
    new-instance v4, LX/AMk;

    invoke-direct {v4, p0, v0, p2, p3}, LX/AMk;-><init>(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;LX/AML;LX/AMK;)V

    .line 1666467
    iget-object v5, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->e:Ljava/util/Map;

    .line 1666468
    iget-object v6, v0, LX/AMT;->d:Ljava/lang/String;

    move-object v6, v6

    .line 1666469
    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1666470
    :try_start_0
    iget-object v5, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->b:LX/3AP;

    new-instance v6, LX/34X;

    iget-object v7, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v6, v1, v4, v7}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v5, v6}, LX/3AP;->b(LX/34X;)LX/1j2;

    move-result-object v1

    .line 1666471
    iget-object v4, v1, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v4

    .line 1666472
    iget-object v4, p0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->c:LX/1Ck;

    sget-object v5, LX/AMh;->DOWNLOAD_MSQRD_MASKS:LX/AMh;

    new-instance v6, LX/AMg;

    invoke-direct {v6, p0, v0, p2}, LX/AMg;-><init>(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;LX/AML;)V

    invoke-virtual {v4, v5, v1, v6}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1666473
    :catch_0
    move-exception v1

    .line 1666474
    invoke-static {p0, v0}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->a$redex0(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)V

    .line 1666475
    invoke-interface {p2, v1}, LX/AML;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1666476
    :cond_4
    return-void
.end method
