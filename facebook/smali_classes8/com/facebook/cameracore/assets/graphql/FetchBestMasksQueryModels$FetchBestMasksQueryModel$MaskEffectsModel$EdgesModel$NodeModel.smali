.class public final Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2bc04393
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1666872
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1666873
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1666876
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1666877
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1666874
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1666875
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1666856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1666857
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1666858
    invoke-direct {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1666859
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1666860
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1666861
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1666862
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1666863
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1666864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1666865
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1666866
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    .line 1666867
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1666868
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    .line 1666869
    iput-object v0, v1, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    .line 1666870
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1666871
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1666855
    invoke-direct {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1666852
    new-instance v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;-><init>()V

    .line 1666853
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1666854
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1666851
    const v0, -0x7cf94e39

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1666850
    const v0, -0x160d5843

    return v0
.end method

.method public final j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1666848
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    iput-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    .line 1666849
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    return-object v0
.end method
