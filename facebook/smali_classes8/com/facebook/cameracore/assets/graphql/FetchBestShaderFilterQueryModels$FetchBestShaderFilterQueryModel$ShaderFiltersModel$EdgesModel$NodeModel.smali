.class public final Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c420a4e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1667192
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1667193
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1667204
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1667205
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1667194
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1667195
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1667196
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1667197
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1667198
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1667199
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1667200
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1667201
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1667202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1667203
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1667186
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1667187
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1667188
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1667185
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1667189
    new-instance v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;-><init>()V

    .line 1667190
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1667191
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1667184
    const v0, 0x2c7c20b7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1667183
    const v0, 0x7ddb43dd

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1667181
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1667182
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1667179
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1667180
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1667177
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1667178
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method
