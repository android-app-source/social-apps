.class public final Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7806a0d6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1667270
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1667271
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1667272
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1667273
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1667274
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1667275
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1667276
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1667277
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1667278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1667279
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1667280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1667281
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1667282
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    .line 1667283
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1667284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;

    .line 1667285
    iput-object v0, v1, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    .line 1667286
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1667287
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1667288
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    iput-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    .line 1667289
    iget-object v0, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->e:Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1667290
    new-instance v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;

    invoke-direct {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;-><init>()V

    .line 1667291
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1667292
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1667293
    const v0, -0x6b2f77fa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1667294
    const v0, -0x6747e1ce

    return v0
.end method
