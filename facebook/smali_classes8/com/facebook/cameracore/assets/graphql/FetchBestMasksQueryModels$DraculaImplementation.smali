.class public final Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1666767
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1666768
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1666765
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1666766
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1666741
    if-nez p1, :cond_0

    move v0, v1

    .line 1666742
    :goto_0
    return v0

    .line 1666743
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1666744
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1666745
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1666746
    const v2, 0x34aae13e

    const/4 v5, 0x0

    .line 1666747
    if-nez v0, :cond_1

    move v4, v5

    .line 1666748
    :goto_1
    move v0, v4

    .line 1666749
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1666750
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1666751
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1666752
    :sswitch_1
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    .line 1666753
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1666754
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1666755
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1666756
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1666757
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 1666758
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1666759
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1666760
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1666761
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 1666762
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1666763
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1666764
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x62c5a67 -> :sswitch_0
        0x34aae13e -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1666740
    new-instance v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1666735
    if-eqz p0, :cond_0

    .line 1666736
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1666737
    if-eq v0, p0, :cond_0

    .line 1666738
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1666739
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1666722
    sparse-switch p2, :sswitch_data_0

    .line 1666723
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1666724
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1666725
    const v1, 0x34aae13e

    .line 1666726
    if-eqz v0, :cond_0

    .line 1666727
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1666728
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1666729
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1666730
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1666731
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1666732
    :cond_0
    :goto_1
    return-void

    .line 1666733
    :sswitch_1
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    .line 1666734
    invoke-static {v0, p3}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x62c5a67 -> :sswitch_0
        0x34aae13e -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1666716
    if-eqz p1, :cond_0

    .line 1666717
    invoke-static {p0, p1, p2}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1666718
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;

    .line 1666719
    if-eq v0, v1, :cond_0

    .line 1666720
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1666721
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1666715
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1666713
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1666714
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1666682
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1666683
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1666684
    :cond_0
    iput-object p1, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1666685
    iput p2, p0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->b:I

    .line 1666686
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1666712
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1666711
    new-instance v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1666708
    iget v0, p0, LX/1vt;->c:I

    .line 1666709
    move v0, v0

    .line 1666710
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1666705
    iget v0, p0, LX/1vt;->c:I

    .line 1666706
    move v0, v0

    .line 1666707
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1666702
    iget v0, p0, LX/1vt;->b:I

    .line 1666703
    move v0, v0

    .line 1666704
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1666699
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1666700
    move-object v0, v0

    .line 1666701
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1666690
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1666691
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1666692
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1666693
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1666694
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1666695
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1666696
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1666697
    invoke-static {v3, v9, v2}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1666698
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1666687
    iget v0, p0, LX/1vt;->c:I

    .line 1666688
    move v0, v0

    .line 1666689
    return v0
.end method
