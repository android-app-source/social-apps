.class public final Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1666878
    const-class v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;

    new-instance v1, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1666879
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1666880
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1666881
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1666882
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1666883
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1666884
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1666885
    if-eqz v2, :cond_3

    .line 1666886
    const-string v3, "mask_effects"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1666887
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1666888
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1666889
    if-eqz v3, :cond_2

    .line 1666890
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1666891
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1666892
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_1

    .line 1666893
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1666894
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1666895
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1666896
    if-eqz v0, :cond_0

    .line 1666897
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1666898
    invoke-static {v1, v0, p1, p2}, LX/AMs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1666899
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1666900
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1666901
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1666902
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1666903
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1666904
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1666905
    check-cast p1, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$Serializer;->a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
