.class public final enum Lcom/facebook/cameracore/ui/CaptureType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/cameracore/ui/CaptureType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/cameracore/ui/CaptureType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/cameracore/ui/CaptureType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

.field public static final enum VIDEO:Lcom/facebook/cameracore/ui/CaptureType;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1668193
    new-instance v0, Lcom/facebook/cameracore/ui/CaptureType;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, Lcom/facebook/cameracore/ui/CaptureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/cameracore/ui/CaptureType;->PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

    .line 1668194
    new-instance v0, Lcom/facebook/cameracore/ui/CaptureType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, Lcom/facebook/cameracore/ui/CaptureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/cameracore/ui/CaptureType;->VIDEO:Lcom/facebook/cameracore/ui/CaptureType;

    .line 1668195
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/cameracore/ui/CaptureType;

    sget-object v1, Lcom/facebook/cameracore/ui/CaptureType;->PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/cameracore/ui/CaptureType;->VIDEO:Lcom/facebook/cameracore/ui/CaptureType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/cameracore/ui/CaptureType;->$VALUES:[Lcom/facebook/cameracore/ui/CaptureType;

    .line 1668196
    new-instance v0, LX/ANe;

    invoke-direct {v0}, LX/ANe;-><init>()V

    sput-object v0, Lcom/facebook/cameracore/ui/CaptureType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1668192
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/cameracore/ui/CaptureType;
    .locals 1

    .prologue
    .line 1668187
    const-class v0, Lcom/facebook/cameracore/ui/CaptureType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/CaptureType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/cameracore/ui/CaptureType;
    .locals 1

    .prologue
    .line 1668191
    sget-object v0, Lcom/facebook/cameracore/ui/CaptureType;->$VALUES:[Lcom/facebook/cameracore/ui/CaptureType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/cameracore/ui/CaptureType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1668190
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1668188
    invoke-virtual {p0}, Lcom/facebook/cameracore/ui/CaptureType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668189
    return-void
.end method
