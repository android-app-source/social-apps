.class public Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;
.super Landroid/view/SurfaceView;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View$OnTouchListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1668346
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1668347
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1668360
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1668361
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1668356
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1668357
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a:Ljava/util/List;

    .line 1668358
    new-instance v0, LX/ANh;

    invoke-direct {v0, p0}, LX/ANh;-><init>(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V

    invoke-super {p0, v0}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1668359
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1668354
    iget-object v0, p0, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668355
    return-void
.end method

.method public final b(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1668352
    iget-object v0, p0, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1668353
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 1668348
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Use removeOnTouchListener to remove a listener"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1668349
    invoke-virtual {p0, p1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 1668350
    return-void

    .line 1668351
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
