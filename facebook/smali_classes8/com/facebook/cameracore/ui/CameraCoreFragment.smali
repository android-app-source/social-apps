.class public Lcom/facebook/cameracore/ui/CameraCoreFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static e:LX/6Jt;

.field public static h:LX/6Ia;


# instance fields
.field public A:LX/ANX;

.field public B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

.field private C:LX/6KN;

.field public D:LX/6KO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:LX/0Zr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public H:LX/ANu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:LX/6KS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public L:LX/1FZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/6KT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public N:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public O:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public P:Lcom/facebook/fbui/glyph/GlyphButton;

.field public Q:LX/ANf;

.field public R:Lcom/facebook/cameracore/ui/FocusView;

.field public S:Lcom/facebook/fbui/glyph/GlyphButton;

.field public T:Landroid/view/ViewGroup;

.field private U:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private V:Landroid/view/ViewGroup;

.field private W:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public X:Z

.field public Y:LX/ANt;

.field private Z:LX/6KB;

.field public aa:Z

.field private ab:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6JF;",
            ">;"
        }
    .end annotation
.end field

.field public final ac:LX/6Ik;

.field private final ad:LX/6JU;

.field public d:Z

.field public f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

.field public g:Lcom/facebook/resources/ui/FbImageButton;

.field public i:Lcom/facebook/fbui/glyph/GlyphButton;

.field public j:Lcom/facebook/fbui/glyph/GlyphButton;

.field public k:Lcom/facebook/fbui/glyph/GlyphButton;

.field public l:Lcom/facebook/fbui/glyph/GlyphButton;

.field private m:Landroid/view/OrientationEventListener;

.field public n:Landroid/view/GestureDetector;

.field public o:Landroid/view/ScaleGestureDetector;

.field public p:Lcom/facebook/cameracore/ui/CounterView;

.field public q:Landroid/view/View;

.field public r:Landroid/view/animation/AlphaAnimation;

.field public s:LX/ANZ;

.field public t:LX/6JF;

.field public u:LX/6JB;

.field public v:Z

.field public w:I

.field public volatile x:LX/6JR;

.field public y:LX/6JC;

.field public z:LX/AMH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1668035
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a:[Ljava/lang/String;

    .line 1668036
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->b:[Ljava/lang/String;

    .line 1668037
    const-class v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1668031
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1668032
    new-instance v0, LX/ANL;

    invoke-direct {v0, p0}, LX/ANL;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ac:LX/6Ik;

    .line 1668033
    new-instance v0, LX/ANP;

    invoke-direct {v0, p0}, LX/ANP;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ad:LX/6JU;

    .line 1668034
    return-void
.end method

.method public static B(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1668001
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1668002
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1668003
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1668004
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(Lcom/facebook/cameracore/ui/CameraCoreFragment;[Landroid/graphics/drawable/Drawable;)V

    .line 1668005
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08277c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1668006
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    sget-object v1, Lcom/facebook/cameracore/ui/CaptureType;->VIDEO:Lcom/facebook/cameracore/ui/CaptureType;

    invoke-virtual {v0, v1}, LX/AMH;->a(Lcom/facebook/cameracore/ui/CaptureType;)Ljava/io/File;

    move-result-object v0

    .line 1668007
    const/4 v3, 0x1

    .line 1668008
    iget-boolean v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->v:Z

    if-eqz v1, :cond_0

    .line 1668009
    :goto_0
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    .line 1668010
    new-instance v2, LX/ANN;

    invoke-direct {v2, p0, v0}, LX/ANN;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/io/File;)V

    move-object v2, v2

    .line 1668011
    invoke-virtual {v1, v0, v2}, LX/6Jt;->a(Ljava/io/File;LX/6JG;)V

    .line 1668012
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1668013
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1668014
    return-void

    .line 1668015
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->w:I

    .line 1668016
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->H(Lcom/facebook/cameracore/ui/CameraCoreFragment;)I

    move-result v1

    .line 1668017
    if-nez v1, :cond_2

    .line 1668018
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1668019
    :cond_1
    :goto_1
    iput-boolean v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->v:Z

    goto :goto_0

    .line 1668020
    :cond_2
    if-ne v1, v3, :cond_3

    .line 1668021
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1668022
    :cond_3
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1668023
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_1
.end method

.method public static C(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 5

    .prologue
    .line 1667782
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->p:Lcom/facebook/cameracore/ui/CounterView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/CounterView;->b()V

    .line 1667783
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->p:Lcom/facebook/cameracore/ui/CounterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/cameracore/ui/CounterView;->setVisibility(I)V

    .line 1667784
    iget-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->v:Z

    if-nez v0, :cond_0

    .line 1667785
    :goto_0
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1667786
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {p0, v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(Lcom/facebook/cameracore/ui/CameraCoreFragment;[Landroid/graphics/drawable/Drawable;)V

    .line 1667787
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08277b    # 1.8098E38f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1667788
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1667789
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1667790
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1667791
    goto :goto_1

    .line 1667792
    :goto_1
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->K(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667793
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667794
    return-void

    .line 1667795
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->w:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1667796
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->v:Z

    goto :goto_0
.end method

.method public static H(Lcom/facebook/cameracore/ui/CameraCoreFragment;)I
    .locals 2

    .prologue
    .line 1668038
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1668039
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method public static K(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 2

    .prologue
    .line 1668040
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ab:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1668041
    :goto_0
    return-void

    .line 1668042
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1668043
    const v0, 0x7f0d085b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->S:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1668044
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->S:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->X:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020818

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 1668045
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->S:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/AND;

    invoke-direct {v1, p0}, LX/AND;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1668046
    const v0, 0x7f0d0891

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->T:Landroid/view/ViewGroup;

    .line 1668047
    const v0, 0x7f0d0892

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->U:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668048
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->U:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1668049
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->U:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1668050
    const v0, 0x7f0d0893

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->V:Landroid/view/ViewGroup;

    .line 1668051
    const v0, 0x7f0d0894

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->W:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668052
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->W:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1668053
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->W:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1668054
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->H:LX/ANu;

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    iget-object v4, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->U:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->V:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->W:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v7, LX/ANE;

    invoke-direct {v7, p0}, LX/ANE;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual/range {v0 .. v7}, LX/ANu;->a(LX/6Jt;Landroid/content/Context;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/ViewGroup;Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/ANE;)LX/ANt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Y:LX/ANt;

    .line 1668055
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Y:LX/ANt;

    invoke-virtual {v0}, LX/ANt;->a()V

    .line 1668056
    return-void

    .line 1668057
    :cond_0
    const v0, 0x7f020919

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/ANZ;)V
    .locals 2

    .prologue
    .line 1668058
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    if-ne v0, p1, :cond_0

    .line 1668059
    :goto_0
    return-void

    .line 1668060
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1668061
    iput-object p1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1668062
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-static {p0, v0, v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/ANZ;LX/ANZ;)V

    .line 1668063
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->K(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1668064
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1668065
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    invoke-static {p0, v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/6JF;)V

    goto :goto_0

    .line 1668066
    :cond_1
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-virtual {v0, v1}, LX/ANf;->a(LX/ANZ;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/cameracore/ui/CameraCoreFragment;[Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1668067
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1668068
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1668069
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1668070
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v1, p1

    check-cast v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;

    const-class v2, LX/6KO;

    invoke-interface {v13, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/6KO;

    invoke-static {v13}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v13}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v4

    check-cast v4, LX/0Zr;

    invoke-static {v13}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const-class v6, LX/ANu;

    invoke-interface {v13, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ANu;

    invoke-static {v13}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v13}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    new-instance v11, LX/6KS;

    invoke-static {v13}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v9

    check-cast v9, Landroid/os/Handler;

    invoke-static {v13}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v11, v9, v10}, LX/6KS;-><init>(Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;)V

    move-object v9, v11

    check-cast v9, LX/6KS;

    invoke-static {v13}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v10

    check-cast v10, LX/1FZ;

    invoke-static {v13}, LX/6KT;->b(LX/0QB;)LX/6KT;

    move-result-object v11

    check-cast v11, LX/6KT;

    invoke-static {v13}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class p0, LX/0i4;

    invoke-interface {v13, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/0i4;

    iput-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->D:LX/6KO;

    iput-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->E:Landroid/os/Handler;

    iput-object v4, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->F:LX/0Zr;

    iput-object v5, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->G:Landroid/content/res/Resources;

    iput-object v6, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->H:LX/ANu;

    iput-object v7, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->I:Ljava/util/concurrent/ExecutorService;

    iput-object v8, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    iput-object v9, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->K:LX/6KS;

    iput-object v10, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->L:LX/1FZ;

    iput-object v11, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->M:LX/6KT;

    iput-object v12, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->N:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v13, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->O:LX/0i4;

    return-void
.end method

.method private static a(LX/0Px;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/cameracore/ui/CaptureType;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1668071
    sget-object v0, Lcom/facebook/cameracore/ui/CaptureType;->VIDEO:Lcom/facebook/cameracore/ui/CaptureType;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1668072
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->b:[Ljava/lang/String;

    .line 1668073
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/6JF;)V
    .locals 4

    .prologue
    .line 1668074
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1668075
    iput-object p1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    .line 1668076
    new-instance v0, LX/ANf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    invoke-direct {v0, v1, v2, v3}, LX/ANf;-><init>(Landroid/content/res/Resources;Lcom/facebook/fbui/glyph/GlyphButton;LX/6JF;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    .line 1668077
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1668078
    :cond_0
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1668079
    :goto_0
    return-void

    .line 1668080
    :cond_1
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    new-instance v1, LX/ANJ;

    invoke-direct {v1, p0}, LX/ANJ;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-interface {v0, v1}, LX/6Ia;->b(LX/6Ik;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/ANZ;LX/ANZ;)V
    .locals 9

    .prologue
    .line 1668081
    sget-object v0, LX/ANZ;->IMAGE:LX/ANZ;

    if-ne p1, v0, :cond_0

    .line 1668082
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1668083
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0767

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 1668084
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0768

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 1668085
    sget-object v1, LX/ANZ;->IMAGE:LX/ANZ;

    if-ne p2, v1, :cond_2

    .line 1668086
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1668087
    const-string v2, "image"

    .line 1668088
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f082779

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move v6, v5

    move v5, v4

    .line 1668089
    :goto_1
    iget-object v7, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v7, v6}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 1668090
    iget-object v6, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v6, v5}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 1668091
    iget-object v5, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v5, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 1668092
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v3, v4, v0

    invoke-static {p0, v4}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(Lcom/facebook/cameracore/ui/CameraCoreFragment;[Landroid/graphics/drawable/Drawable;)V

    .line 1668093
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbImageButton;->setTag(Ljava/lang/Object;)V

    .line 1668094
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1668095
    return-void

    .line 1668096
    :cond_0
    sget-object v0, LX/ANZ;->PHOTO:LX/ANZ;

    if-ne p1, v0, :cond_1

    .line 1668097
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1668098
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1668099
    :cond_2
    sget-object v1, LX/ANZ;->PHOTO:LX/ANZ;

    if-ne p2, v1, :cond_3

    .line 1668100
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1668101
    const-string v2, "photo"

    .line 1668102
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f08277a

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move v6, v4

    goto :goto_1

    .line 1668103
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1668104
    const-string v2, "video"

    .line 1668105
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f08277b    # 1.8098E38f

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move v6, v4

    move v8, v4

    move v4, v5

    move v5, v8

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1668024
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1668025
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1668026
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_finish_with_error"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1668027
    :goto_0
    return-void

    .line 1668028
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08277e

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1668029
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 1668030
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 6

    .prologue
    .line 1667991
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-nez v0, :cond_0

    .line 1667992
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->C:LX/6KN;

    invoke-direct {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l()LX/6J9;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Z:LX/6KB;

    .line 1667993
    iget-object v5, v4, LX/6KB;->a:LX/6KF;

    move-object v4, v5

    .line 1667994
    invoke-static {v0, v1, v2, v3, v4}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;

    move-result-object v0

    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    .line 1667995
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667996
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    move-object v0, v1

    .line 1667997
    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(LX/0Px;)[Ljava/lang/String;

    move-result-object v0

    .line 1667998
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->O:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    .line 1667999
    new-instance v2, LX/ANF;

    invoke-direct {v2, p0}, LX/ANF;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1668000
    return-void
.end method

.method public static k$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 3

    .prologue
    .line 1667983
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667984
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    move-object v0, v1

    .line 1667985
    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(LX/0Px;)[Ljava/lang/String;

    move-result-object v0

    .line 1667986
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->O:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    .line 1667987
    invoke-virtual {v1, v0}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1667988
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    .line 1667989
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    new-instance v1, LX/ANG;

    invoke-direct {v1, p0}, LX/ANG;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-interface {v0, v1}, LX/6Ia;->a(LX/6Ik;)V

    .line 1667990
    :cond_0
    return-void
.end method

.method private l()LX/6J9;
    .locals 1

    .prologue
    .line 1667978
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667979
    iget-boolean p0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    move v0, p0

    .line 1667980
    if-eqz v0, :cond_0

    .line 1667981
    sget-object v0, LX/6J9;->CAMERA1:LX/6J9;

    .line 1667982
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/6J7;->a()LX/6J9;

    move-result-object v0

    goto :goto_0
.end method

.method public static n$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 7

    .prologue
    .line 1667946
    :try_start_0
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x500

    move v1, v0

    .line 1667947
    :goto_0
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getHeight()I

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x2d0

    .line 1667948
    :goto_1
    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v2}, LX/6Ia;->a()LX/6IP;

    move-result-object v2

    .line 1667949
    iget-object v4, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->N:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    sget-object v5, LX/6JF;->FRONT:LX/6JF;

    if-ne v3, v5, :cond_9

    sget-object v3, LX/AN4;->a:LX/0Tn;

    :goto_2
    const-string v5, ""

    invoke-interface {v4, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1667950
    invoke-static {v3}, LX/ANA;->a(Ljava/lang/String;)LX/6JR;

    move-result-object v3

    move-object v3, v3

    .line 1667951
    iput-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    .line 1667952
    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    if-nez v3, :cond_0

    .line 1667953
    invoke-interface {v2}, LX/6IP;->c()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v1, v0}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    .line 1667954
    :cond_0
    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    if-nez v3, :cond_3

    .line 1667955
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid preview size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1667956
    :catch_0
    :goto_3
    return-void

    .line 1667957
    :cond_1
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getWidth()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 1667958
    :cond_2
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getHeight()I

    move-result v0

    goto :goto_1

    .line 1667959
    :cond_3
    invoke-interface {v2}, LX/6IP;->d()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v1, v0}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v3

    .line 1667960
    if-nez v3, :cond_4

    .line 1667961
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid photo size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1667962
    :cond_4
    invoke-interface {v2}, LX/6IP;->e()Ljava/util/List;

    move-result-object v4

    .line 1667963
    invoke-static {v4, v1, v0}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v1

    .line 1667964
    if-nez v1, :cond_5

    .line 1667965
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    move-object v4, v0

    .line 1667966
    :goto_4
    new-instance v0, LX/6JB;

    iget v1, v3, LX/6JR;->a:I

    iget v2, v3, LX/6JR;->b:I

    iget v3, v4, LX/6JR;->a:I

    iget v4, v4, LX/6JR;->b:I

    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->H(Lcom/facebook/cameracore/ui/CameraCoreFragment;)I

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/6JB;-><init>(IIIIILjava/util/List;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->u:LX/6JB;

    .line 1667967
    new-instance v0, LX/6JC;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    .line 1667968
    iget-object v2, v1, LX/ANf;->b:LX/6JN;

    move-object v1, v2

    .line 1667969
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/6JC;-><init>(LX/6JN;LX/6JO;)V

    move-object v0, v0

    .line 1667970
    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->y:LX/6JC;

    goto :goto_3

    .line 1667971
    :cond_5
    const/4 v2, 0x0

    .line 1667972
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JR;

    .line 1667973
    iget-object v5, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    invoke-virtual {v5, v0}, LX/6JR;->a(LX/6JR;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1667974
    const/4 v0, 0x1

    .line 1667975
    :goto_5
    if-nez v0, :cond_7

    .line 1667976
    iput-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    move-object v4, v1

    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_5

    .line 1667977
    :cond_9
    sget-object v3, LX/AN4;->b:LX/0Tn;

    goto/16 :goto_2
.end method

.method public static r(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 3

    .prologue
    .line 1667943
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    invoke-virtual {v0}, LX/6Jt;->a()LX/6K9;

    move-result-object v0

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    if-ne v0, v1, :cond_0

    .line 1667944
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ad:LX/6JU;

    invoke-virtual {v0, v1, v2}, LX/6Jt;->a(LX/6JR;LX/6JU;)V

    .line 1667945
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1667904
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1667905
    const-class v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0, p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1667906
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1667907
    const-string v1, "config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667908
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->D:LX/6KO;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667909
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1667910
    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667911
    iget-object v3, v2, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    move-object v2, v3

    .line 1667912
    invoke-virtual {v0, v1, v2}, LX/6KO;->a(Ljava/lang/String;Ljava/lang/String;)LX/6KN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->C:LX/6KN;

    .line 1667913
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->M:LX/6KT;

    invoke-static {v0}, LX/6KD;->a(LX/6KT;)LX/6KD;

    move-result-object v0

    invoke-virtual {v0}, LX/6KD;->a()LX/6KB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Z:LX/6KB;

    .line 1667914
    sget-object v0, LX/6J7;->a:Ljava/util/Set;

    if-eqz v0, :cond_3

    .line 1667915
    sget-object v0, LX/6J7;->a:Ljava/util/Set;

    .line 1667916
    :goto_0
    move-object v0, v0

    .line 1667917
    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ab:Ljava/util/Set;

    .line 1667918
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667919
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    move-object v0, v1

    .line 1667920
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1667921
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1667922
    :cond_0
    if-eqz p1, :cond_1

    .line 1667923
    const-string v0, "camera_facing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JF;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    .line 1667924
    const-string v0, "camera_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ANZ;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1667925
    :goto_1
    return-void

    .line 1667926
    :cond_1
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667927
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    move-object v0, v1

    .line 1667928
    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    .line 1667929
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ab:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1667930
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ab:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JF;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    .line 1667931
    :cond_2
    sget-object v0, LX/ANZ;->NONE:LX/ANZ;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    goto :goto_1

    .line 1667932
    :cond_3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/6J7;->a:Ljava/util/Set;

    .line 1667933
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    .line 1667934
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1667935
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_6

    .line 1667936
    invoke-static {v0, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1667937
    iget v3, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 1667938
    sget-object v3, LX/6J7;->a:Ljava/util/Set;

    sget-object v4, LX/6JF;->FRONT:LX/6JF;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1667939
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1667940
    :cond_5
    iget v3, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v3, :cond_4

    .line 1667941
    sget-object v3, LX/6J7;->a:Ljava/util/Set;

    sget-object v4, LX/6JF;->BACK:LX/6JF;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1667942
    :cond_6
    sget-object v0, LX/6J7;->a:Ljava/util/Set;

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1667884
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1667885
    :cond_0
    :goto_0
    return-void

    .line 1667886
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1667887
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1667888
    sget-object v1, LX/ANZ;->IMAGE:LX/ANZ;

    iput-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1667889
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-static {p0, v0, v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/ANZ;LX/ANZ;)V

    .line 1667890
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667891
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667892
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1667893
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->K:LX/6KS;

    new-instance v2, LX/ANQ;

    invoke-direct {v2, p0, v0}, LX/ANQ;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Landroid/net/Uri;)V

    .line 1667894
    if-eqz v2, :cond_2

    const/4 p0, 0x1

    :goto_1
    const-string p1, "Null callback means loaded image does not get used"

    invoke-static {p0, p1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1667895
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object p0

    iget-object p1, v1, LX/6KS;->c:LX/6KQ;

    .line 1667896
    iput-object p1, p0, LX/1bX;->j:LX/33B;

    .line 1667897
    move-object p0, p0

    .line 1667898
    invoke-virtual {p0}, LX/1bX;->n()LX/1bf;

    move-result-object p0

    .line 1667899
    invoke-static {}, LX/4AN;->b()LX/1HI;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p0, p2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object p0

    .line 1667900
    new-instance p1, LX/6KP;

    invoke-direct {p1, v1, v2, v0}, LX/6KP;-><init>(LX/6KS;LX/ANQ;Landroid/net/Uri;)V

    .line 1667901
    iget-object p2, v1, LX/6KS;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p0, p1, p2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1667902
    goto :goto_0

    .line 1667903
    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0xb7630b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v9

    .line 1667831
    const v0, 0x7f030238

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 1667832
    const v0, 0x7f0d0681

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 1667833
    const v0, 0x7f0d088d

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->q:Landroid/view/View;

    .line 1667834
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r:Landroid/view/animation/AlphaAnimation;

    .line 1667835
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1667836
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1667837
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r:Landroid/view/animation/AlphaAnimation;

    new-instance v1, LX/ANR;

    invoke-direct {v1, p0}, LX/ANR;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1667838
    const v0, 0x7f0d0859

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    .line 1667839
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbImageButton;->setEnabled(Z)V

    .line 1667840
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    new-instance v1, LX/ANS;

    invoke-direct {v1, p0}, LX/ANS;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1667841
    const v0, 0x7f0d0857

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1667842
    const v0, 0x7f0d0858

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1667843
    const v0, 0x7f0d085a

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1667844
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667845
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    move-object v0, v1

    .line 1667846
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v6, :cond_3

    .line 1667847
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/ANT;

    invoke-direct {v1, p0}, LX/ANT;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1667848
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/ANU;

    invoke-direct {v1, p0}, LX/ANU;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1667849
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/ANV;

    invoke-direct {v1, p0}, LX/ANV;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1667850
    :goto_0
    const v0, 0x7f0d088f

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1667851
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ab:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v6, :cond_4

    .line 1667852
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667853
    :goto_1
    const v0, 0x7f0d0890

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1667854
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667855
    iget-boolean v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    move v1, v2

    .line 1667856
    if-eqz v1, :cond_5

    .line 1667857
    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667858
    new-instance v1, LX/ANB;

    invoke-direct {v1, p0}, LX/ANB;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1667859
    :goto_2
    const v0, 0x7f0d0685

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1667860
    new-instance v0, LX/ANf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    invoke-direct {v0, v1, v2, v3}, LX/ANf;-><init>(Landroid/content/res/Resources;Lcom/facebook/fbui/glyph/GlyphButton;LX/6JF;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    .line 1667861
    const v0, 0x7f0d0682

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/FocusView;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->R:Lcom/facebook/cameracore/ui/FocusView;

    .line 1667862
    const v0, 0x7f0d088e

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/CounterView;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->p:Lcom/facebook/cameracore/ui/CounterView;

    .line 1667863
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    if-nez v0, :cond_0

    .line 1667864
    new-instance v0, LX/6Jt;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->F:LX/0Zr;

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->G:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->E:Landroid/os/Handler;

    iget-object v4, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->I:Ljava/util/concurrent/ExecutorService;

    iget-object v5, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->C:LX/6KN;

    iget-object v6, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Z:LX/6KB;

    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->H(Lcom/facebook/cameracore/ui/CameraCoreFragment;)I

    move-result v7

    .line 1667865
    new-instance v8, LX/ANO;

    invoke-direct {v8, p0}, LX/ANO;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    move-object v8, v8

    .line 1667866
    invoke-direct/range {v0 .. v8}, LX/6Jt;-><init>(LX/0Zr;Landroid/content/res/Resources;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/6KN;LX/6KB;ILX/6Jp;)V

    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    .line 1667867
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v1, LX/ANZ;->IMAGE:LX/ANZ;

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    if-nez v0, :cond_1

    .line 1667868
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667869
    :cond_1
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0, v1}, LX/6Jt;->a(Landroid/view/SurfaceView;)V

    .line 1667870
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/ANY;

    invoke-direct {v2, p0}, LX/ANY;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->n:Landroid/view/GestureDetector;

    .line 1667871
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/ANa;

    invoke-direct {v2, p0}, LX/ANa;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->o:Landroid/view/ScaleGestureDetector;

    .line 1667872
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    new-instance v1, LX/ANH;

    invoke-direct {v1, p0}, LX/ANH;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 1667873
    new-instance v0, LX/ANC;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, p0, v1, v2}, LX/ANC;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->m:Landroid/view/OrientationEventListener;

    .line 1667874
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667875
    iget-boolean v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    move v0, v1

    .line 1667876
    if-eqz v0, :cond_2

    .line 1667877
    invoke-direct {p0, v10}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a(Landroid/view/View;)V

    .line 1667878
    :cond_2
    const v0, -0x5f43ae67

    invoke-static {v0, v9}, LX/02F;->f(II)V

    return-object v10

    .line 1667879
    :cond_3
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667880
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->j:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667881
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 1667882
    :cond_4
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/ANW;

    invoke-direct {v1, p0}, LX/ANW;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 1667883
    :cond_5
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xaf09716

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1667815
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    if-eqz v1, :cond_0

    .line 1667816
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v1, v2}, LX/6Jt;->a(Landroid/view/View;)V

    .line 1667817
    :cond_0
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v1, :cond_1

    .line 1667818
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    .line 1667819
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->b()V

    .line 1667820
    :cond_1
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->m:Landroid/view/OrientationEventListener;

    if-eqz v1, :cond_2

    .line 1667821
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->m:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->disable()V

    .line 1667822
    :cond_2
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Y:LX/ANt;

    if-eqz v1, :cond_3

    .line 1667823
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Y:LX/ANt;

    const/4 v4, 0x0

    .line 1667824
    iget-object v2, v1, LX/ANt;->m:LX/AMO;

    invoke-virtual {v2}, LX/AMO;->a()V

    .line 1667825
    iget-object v2, v1, LX/ANt;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1667826
    iput-object v4, v1, LX/ANt;->q:Ljava/util/List;

    .line 1667827
    iput-object v4, v1, LX/ANt;->r:Ljava/util/Map;

    .line 1667828
    iput-object v4, v1, LX/ANt;->s:Ljava/util/List;

    .line 1667829
    :cond_3
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1667830
    const/16 v1, 0x2b

    const v2, -0xba50a56

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19503c2e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1667808
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v1, :cond_0

    .line 1667809
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    .line 1667810
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->b()V

    .line 1667811
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->p:Lcom/facebook/cameracore/ui/CounterView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/cameracore/ui/CounterView;->setVisibility(I)V

    .line 1667812
    :cond_0
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    invoke-virtual {v1}, LX/6Jt;->c()V

    .line 1667813
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1667814
    const/16 v1, 0x2b

    const v2, -0x38f2eeed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x55ac7a38

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1667801
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1667802
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->m:Landroid/view/OrientationEventListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->m:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1667803
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->m:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->enable()V

    .line 1667804
    :cond_0
    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v2, LX/ANZ;->IMAGE:LX/ANZ;

    if-eq v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v1, :cond_1

    .line 1667805
    invoke-static {p0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->k$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667806
    :cond_1
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    invoke-virtual {v1}, LX/6Jt;->b()V

    .line 1667807
    const/16 v1, 0x2b

    const v2, -0xd701196

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1667797
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1667798
    const-string v0, "camera_facing"

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1667799
    const-string v0, "camera_mode"

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1667800
    return-void
.end method
