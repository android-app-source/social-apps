.class public Lcom/facebook/cameracore/ui/FocusView;
.super Landroid/view/View;
.source ""


# instance fields
.field public final a:Landroid/graphics/Paint;

.field public final b:F

.field private final c:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private final d:Ljava/lang/Runnable;

.field public e:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1668312
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/cameracore/ui/FocusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1668313
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1668314
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/cameracore/ui/FocusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1668315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1668316
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1668317
    new-instance v0, LX/ANg;

    invoke-direct {v0, p0}, LX/ANg;-><init>(Lcom/facebook/cameracore/ui/FocusView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->c:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1668318
    new-instance v0, Lcom/facebook/cameracore/ui/FocusView$2;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/FocusView$2;-><init>(Lcom/facebook/cameracore/ui/FocusView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->d:Ljava/lang/Runnable;

    .line 1668319
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->a:Landroid/graphics/Paint;

    .line 1668320
    iget-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1668321
    iget-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1668322
    iget-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/cameracore/ui/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a42

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1668323
    invoke-virtual {p0}, Lcom/facebook/cameracore/ui/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a43

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/cameracore/ui/FocusView;->b:F

    .line 1668324
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 4

    .prologue
    .line 1668325
    int-to-float v0, p1

    iget v1, p0, Lcom/facebook/cameracore/ui/FocusView;->b:F

    sub-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/ui/FocusView;->setX(F)V

    .line 1668326
    int-to-float v0, p2

    iget v1, p0, Lcom/facebook/cameracore/ui/FocusView;->b:F

    sub-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/ui/FocusView;->setY(F)V

    .line 1668327
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/ui/FocusView;->setVisibility(I)V

    .line 1668328
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1668329
    iget-object v1, p0, Lcom/facebook/cameracore/ui/FocusView;->c:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1668330
    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1668331
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1668332
    iget-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/ui/FocusView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1668333
    iget-object v0, p0, Lcom/facebook/cameracore/ui/FocusView;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/cameracore/ui/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1668334
    return-void

    .line 1668335
    :array_0
    .array-data 4
        0x40400000    # 3.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1668336
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1668337
    iget v0, p0, Lcom/facebook/cameracore/ui/FocusView;->b:F

    iget v1, p0, Lcom/facebook/cameracore/ui/FocusView;->b:F

    iget v2, p0, Lcom/facebook/cameracore/ui/FocusView;->e:F

    iget-object v3, p0, Lcom/facebook/cameracore/ui/FocusView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1668338
    return-void
.end method
