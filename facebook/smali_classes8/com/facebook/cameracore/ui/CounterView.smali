.class public Lcom/facebook/cameracore/ui/CounterView;
.super Landroid/widget/TextView;
.source ""


# instance fields
.field public a:J

.field public b:Z

.field public c:Landroid/os/Handler;

.field public d:Landroid/os/Handler;

.field public final e:Ljava/lang/Runnable;

.field public final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1668228
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1668229
    new-instance v0, Lcom/facebook/cameracore/ui/CounterView$1;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/CounterView$1;-><init>(Lcom/facebook/cameracore/ui/CounterView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->e:Ljava/lang/Runnable;

    .line 1668230
    new-instance v0, Lcom/facebook/cameracore/ui/CounterView$2;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/CounterView$2;-><init>(Lcom/facebook/cameracore/ui/CounterView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->f:Ljava/lang/Runnable;

    .line 1668231
    invoke-direct {p0}, Lcom/facebook/cameracore/ui/CounterView;->c()V

    .line 1668232
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1668208
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1668209
    new-instance v0, Lcom/facebook/cameracore/ui/CounterView$1;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/CounterView$1;-><init>(Lcom/facebook/cameracore/ui/CounterView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->e:Ljava/lang/Runnable;

    .line 1668210
    new-instance v0, Lcom/facebook/cameracore/ui/CounterView$2;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/CounterView$2;-><init>(Lcom/facebook/cameracore/ui/CounterView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->f:Ljava/lang/Runnable;

    .line 1668211
    invoke-direct {p0}, Lcom/facebook/cameracore/ui/CounterView;->c()V

    .line 1668212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1668220
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1668221
    new-instance v0, Lcom/facebook/cameracore/ui/CounterView$1;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/CounterView$1;-><init>(Lcom/facebook/cameracore/ui/CounterView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->e:Ljava/lang/Runnable;

    .line 1668222
    new-instance v0, Lcom/facebook/cameracore/ui/CounterView$2;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/ui/CounterView$2;-><init>(Lcom/facebook/cameracore/ui/CounterView;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->f:Ljava/lang/Runnable;

    .line 1668223
    invoke-direct {p0}, Lcom/facebook/cameracore/ui/CounterView;->c()V

    .line 1668224
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1668225
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->d:Landroid/os/Handler;

    .line 1668226
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->c:Landroid/os/Handler;

    .line 1668227
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1668215
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/ui/CounterView;->setText(Ljava/lang/CharSequence;)V

    .line 1668216
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/cameracore/ui/CounterView;->a:J

    .line 1668217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CounterView;->b:Z

    .line 1668218
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CounterView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/cameracore/ui/CounterView;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, -0x286ae8ca

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1668219
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1668213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CounterView;->b:Z

    .line 1668214
    return-void
.end method
