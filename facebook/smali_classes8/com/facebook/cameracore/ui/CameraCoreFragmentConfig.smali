.class public Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6JF;

.field public final b:Z

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/cameracore/ui/CaptureType;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1668181
    new-instance v0, LX/ANb;

    invoke-direct {v0}, LX/ANb;-><init>()V

    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/ANc;)V
    .locals 1

    .prologue
    .line 1668172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668173
    iget-object v0, p1, LX/ANc;->b:LX/6JF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JF;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    .line 1668174
    iget-boolean v0, p1, LX/ANc;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    .line 1668175
    iget-object v0, p1, LX/ANc;->d:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    .line 1668176
    iget-boolean v0, p1, LX/ANc;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    .line 1668177
    iget-object v0, p1, LX/ANc;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    .line 1668178
    iget-object v0, p1, LX/ANc;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    .line 1668179
    iget-boolean v0, p1, LX/ANc;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    .line 1668180
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1668155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668156
    invoke-static {}, LX/6JF;->values()[LX/6JF;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    .line 1668157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    .line 1668158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Lcom/facebook/cameracore/ui/CaptureType;

    move v0, v2

    .line 1668159
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 1668160
    invoke-static {}, Lcom/facebook/cameracore/ui/CaptureType;->values()[Lcom/facebook/cameracore/ui/CaptureType;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    aget-object v4, v4, v5

    .line 1668161
    aput-object v4, v3, v0

    .line 1668162
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1668163
    goto :goto_0

    .line 1668164
    :cond_1
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    .line 1668165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    .line 1668166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    .line 1668167
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    .line 1668168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    .line 1668169
    return-void

    :cond_2
    move v0, v2

    .line 1668170
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1668171
    goto :goto_3
.end method

.method public static newBuilder()LX/ANc;
    .locals 2

    .prologue
    .line 1668120
    new-instance v0, LX/ANc;

    invoke-direct {v0}, LX/ANc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1668182
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1668136
    if-ne p0, p1, :cond_1

    .line 1668137
    :cond_0
    :goto_0
    return v0

    .line 1668138
    :cond_1
    instance-of v2, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1668139
    goto :goto_0

    .line 1668140
    :cond_2
    check-cast p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1668141
    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    iget-object v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1668142
    goto :goto_0

    .line 1668143
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    iget-boolean v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1668144
    goto :goto_0

    .line 1668145
    :cond_4
    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    iget-object v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1668146
    goto :goto_0

    .line 1668147
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    iget-boolean v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1668148
    goto :goto_0

    .line 1668149
    :cond_6
    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1668150
    goto :goto_0

    .line 1668151
    :cond_7
    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1668152
    goto :goto_0

    .line 1668153
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    iget-boolean v3, p1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1668154
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1668135
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1668121
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    invoke-virtual {v0}, LX/6JF;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668122
    iget-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668123
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668124
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/CaptureType;

    .line 1668125
    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/CaptureType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668126
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1668127
    goto :goto_0

    .line 1668128
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668129
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1668130
    iget-object v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1668131
    iget-boolean v0, p0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->g:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1668132
    return-void

    :cond_2
    move v0, v2

    .line 1668133
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1668134
    goto :goto_3
.end method
