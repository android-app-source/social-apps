.class public final Lcom/facebook/cameracore/ui/CounterView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CounterView;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CounterView;)V
    .locals 0

    .prologue
    .line 1668202
    iput-object p1, p0, Lcom/facebook/cameracore/ui/CounterView$2;->a:Lcom/facebook/cameracore/ui/CounterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1668203
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/cameracore/ui/CounterView$2;->a:Lcom/facebook/cameracore/ui/CounterView;

    iget-wide v2, v2, Lcom/facebook/cameracore/ui/CounterView;->a:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    .line 1668204
    div-int/lit8 v1, v0, 0x3c

    .line 1668205
    rem-int/lit8 v0, v0, 0x3c

    .line 1668206
    iget-object v2, p0, Lcom/facebook/cameracore/ui/CounterView$2;->a:Lcom/facebook/cameracore/ui/CounterView;

    const-string v3, "%02d:%02d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/cameracore/ui/CounterView;->setText(Ljava/lang/CharSequence;)V

    .line 1668207
    return-void
.end method
