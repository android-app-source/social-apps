.class public Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final p:Ljava/lang/String;


# instance fields
.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1666034
    const-class v0, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1666035
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1666036
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1666037
    iput-object v0, p0, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->q:LX/0Ot;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1666038
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1666039
    const-string v1, "config"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1666040
    return-object v0
.end method

.method private a()Lcom/facebook/cameracore/ui/CameraCoreFragment;
    .locals 4

    .prologue
    .line 1666041
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;

    .line 1666042
    if-eqz v0, :cond_0

    .line 1666043
    :goto_0
    return-object v0

    .line 1666044
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "config"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1666045
    new-instance v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;-><init>()V

    .line 1666046
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1666047
    const-string v3, "config"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1666048
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1666049
    move-object v0, v1

    .line 1666050
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1666051
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->b()Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->q:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1666052
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1666053
    invoke-static {p0, p0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1666054
    const v0, 0x7f030232

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->setContentView(I)V

    .line 1666055
    new-instance v1, Ljava/io/File;

    sget-object v0, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const-string v2, "Facebook"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1666056
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1666057
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1666058
    iget-object v0, p0, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->p:Ljava/lang/String;

    const-string v3, "Can not create directory to store new photos"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1666059
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->setResult(I)V

    .line 1666060
    invoke-virtual {p0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->finish()V

    .line 1666061
    :cond_0
    invoke-direct {p0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->a()Lcom/facebook/cameracore/ui/CameraCoreFragment;

    move-result-object v0

    .line 1666062
    new-instance v2, LX/AMH;

    invoke-direct {v2, p0, v1}, LX/AMH;-><init>(Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;Ljava/io/File;)V

    .line 1666063
    iput-object v2, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    .line 1666064
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 3

    .prologue
    .line 1666065
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onWindowFocusChanged(Z)V

    .line 1666066
    if-eqz p1, :cond_1

    .line 1666067
    const/16 v0, 0x404

    .line 1666068
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 1666069
    const/16 v0, 0x1606

    .line 1666070
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1666071
    :cond_1
    return-void
.end method
