.class public Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:LX/6Ia;

.field private c:LX/6Ia;

.field private d:LX/6KB;

.field private e:LX/4or;

.field private f:LX/4or;

.field public g:LX/6KT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1667506
    const-class v0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1667505
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;

    invoke-static {v0}, LX/6KT;->b(LX/0QB;)LX/6KT;

    move-result-object v0

    check-cast v0, LX/6KT;

    iput-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->g:LX/6KT;

    return-void
.end method

.method private static a(LX/6Ia;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1667491
    :try_start_0
    invoke-interface {p0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    .line 1667492
    invoke-interface {v0}, LX/6IP;->c()Ljava/util/List;

    move-result-object v4

    .line 1667493
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1667494
    :goto_0
    return-object v0

    .line 1667495
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 1667496
    array-length v5, v2

    .line 1667497
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_1

    .line 1667498
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JR;
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    .line 1667499
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget p0, v0, LX/6JR;->a:I

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string p0, " x "

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget p0, v0, LX/6JR;->b:I

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/6JJ; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v6

    .line 1667500
    :goto_2
    move-object v0, v6

    .line 1667501
    aput-object v0, v2, v3
    :try_end_2
    .catch LX/6JJ; {:try_start_2 .. :try_end_2} :catch_0

    .line 1667502
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 1667503
    goto :goto_0

    .line 1667504
    :catch_0
    move-object v0, v1

    goto :goto_0

    :catch_1
    const-string v6, ""

    goto :goto_2
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1667483
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->d:LX/6KB;

    .line 1667484
    iget-object v3, v1, LX/6KB;->a:LX/6KF;

    move-object v1, v3

    .line 1667485
    invoke-static {p0, v0, v2, v1}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6KF;)LX/6Ia;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    .line 1667486
    sget-object v0, LX/6JF;->BACK:LX/6JF;

    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->d:LX/6KB;

    .line 1667487
    iget-object v3, v1, LX/6KB;->a:LX/6KF;

    move-object v1, v3

    .line 1667488
    invoke-static {p0, v0, v2, v1}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6KF;)LX/6Ia;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    .line 1667489
    invoke-direct {p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->d()V

    .line 1667490
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1667481
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    new-instance v1, LX/AN7;

    invoke-direct {v1, p0}, LX/AN7;-><init>(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V

    invoke-interface {v0, v1}, LX/6Ia;->a(LX/6Ik;)V

    .line 1667482
    return-void
.end method

.method public static e(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V
    .locals 3

    .prologue
    .line 1667507
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    invoke-static {v0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->a(LX/6Ia;)[Ljava/lang/String;

    move-result-object v0

    .line 1667508
    if-eqz v0, :cond_0

    .line 1667509
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    invoke-virtual {v1, v0}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 1667510
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    invoke-virtual {v1, v0}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1667511
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 1667512
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    new-instance v1, LX/AN8;

    invoke-direct {v1, p0}, LX/AN8;-><init>(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V

    invoke-interface {v0, v1}, LX/6Ia;->b(LX/6Ik;)V

    .line 1667513
    return-void
.end method

.method public static f(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V
    .locals 2

    .prologue
    .line 1667479
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    new-instance v1, LX/AN9;

    invoke-direct {v1, p0}, LX/AN9;-><init>(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V

    invoke-interface {v0, v1}, LX/6Ia;->a(LX/6Ik;)V

    .line 1667480
    return-void
.end method

.method public static g(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V
    .locals 3

    .prologue
    .line 1667472
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    invoke-static {v0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->a(LX/6Ia;)[Ljava/lang/String;

    move-result-object v0

    .line 1667473
    if-eqz v0, :cond_0

    .line 1667474
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    invoke-virtual {v1, v0}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 1667475
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    invoke-virtual {v1, v0}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1667476
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 1667477
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->b()V

    .line 1667478
    return-void
.end method

.method public static h(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V
    .locals 1

    .prologue
    .line 1667469
    invoke-direct {p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1667470
    invoke-virtual {p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->finish()V

    .line 1667471
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 1667437
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1667450
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1667451
    invoke-static {p0, p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1667452
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->g:LX/6KT;

    invoke-static {v0}, LX/6KD;->a(LX/6KT;)LX/6KD;

    move-result-object v0

    invoke-virtual {v0}, LX/6KD;->a()LX/6KB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->d:LX/6KB;

    .line 1667453
    invoke-virtual {p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1667454
    invoke-virtual {p0, v0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1667455
    new-instance v1, LX/4or;

    invoke-direct {v1, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    .line 1667456
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    const-string v2, "Change the front camera preview size"

    invoke-virtual {v1, v2}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 1667457
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    const-string v2, "Front Camera Preview Size"

    invoke-virtual {v1, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 1667458
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    const-string v2, "Choose Preferred Size"

    invoke-virtual {v1, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1667459
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    sget-object v2, LX/AN4;->a:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4or;->a(LX/0Tn;)V

    .line 1667460
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->e:LX/4or;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1667461
    new-instance v1, LX/4or;

    invoke-direct {v1, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    .line 1667462
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    const-string v2, "Change the back camera preview size"

    invoke-virtual {v1, v2}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 1667463
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    const-string v2, "Back Camera Preview size"

    invoke-virtual {v1, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 1667464
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    const-string v2, "Select Preferred Size"

    invoke-virtual {v1, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1667465
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    sget-object v2, LX/AN4;->b:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4or;->a(LX/0Tn;)V

    .line 1667466
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->f:LX/4or;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1667467
    invoke-direct {p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b()V

    .line 1667468
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1667444
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1667445
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    new-instance v1, LX/AN5;

    invoke-direct {v1, p0}, LX/AN5;-><init>(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V

    invoke-interface {v0, v1}, LX/6Ia;->b(LX/6Ik;)V

    .line 1667446
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1667447
    iget-object v0, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    new-instance v1, LX/AN6;

    invoke-direct {v1, p0}, LX/AN6;-><init>(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V

    invoke-interface {v0, v1}, LX/6Ia;->b(LX/6Ik;)V

    .line 1667448
    :cond_1
    invoke-static {p0}, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->h(Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;)V

    .line 1667449
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x3df9faa1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1667438
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1667439
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->b:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->b()V

    .line 1667440
    :cond_0
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1667441
    iget-object v1, p0, Lcom/facebook/cameracore/camerasettings/CameraSettingsPreferenceActivity;->c:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->b()V

    .line 1667442
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onPause()V

    .line 1667443
    const/16 v1, 0x23

    const v2, -0x6333942b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
