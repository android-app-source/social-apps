.class public final Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1495792
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1495793
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1495850
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1495851
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1495819
    if-nez p1, :cond_0

    move v0, v1

    .line 1495820
    :goto_0
    return v0

    .line 1495821
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1495822
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1495823
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1495824
    const v2, -0x30db1c42

    const/4 v5, 0x0

    .line 1495825
    if-nez v0, :cond_1

    move v3, v5

    .line 1495826
    :goto_1
    move v0, v3

    .line 1495827
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1495828
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1495829
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1495830
    :sswitch_1
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;

    .line 1495831
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1495832
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1495833
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1495834
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1495835
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1495836
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1495837
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1495838
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1495839
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1495840
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1495841
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1495842
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 1495843
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 1495844
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1495845
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1495846
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 1495847
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1495848
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 1495849
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3578b42a -> :sswitch_0
        -0x30db1c42 -> :sswitch_1
        -0x21916223 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1495818
    new-instance v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1495813
    if-eqz p0, :cond_0

    .line 1495814
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1495815
    if-eq v0, p0, :cond_0

    .line 1495816
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1495817
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1495800
    sparse-switch p2, :sswitch_data_0

    .line 1495801
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1495802
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1495803
    const v1, -0x30db1c42

    .line 1495804
    if-eqz v0, :cond_0

    .line 1495805
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1495806
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1495807
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1495808
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1495809
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1495810
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1495811
    :sswitch_2
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;

    .line 1495812
    invoke-static {v0, p3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3578b42a -> :sswitch_0
        -0x30db1c42 -> :sswitch_2
        -0x21916223 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1495794
    if-eqz p1, :cond_0

    .line 1495795
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1495796
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    .line 1495797
    if-eq v0, v1, :cond_0

    .line 1495798
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1495799
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1495791
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1495852
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1495853
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1495786
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1495787
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1495788
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1495789
    iput p2, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->b:I

    .line 1495790
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1495785
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1495760
    new-instance v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1495782
    iget v0, p0, LX/1vt;->c:I

    .line 1495783
    move v0, v0

    .line 1495784
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1495779
    iget v0, p0, LX/1vt;->c:I

    .line 1495780
    move v0, v0

    .line 1495781
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1495776
    iget v0, p0, LX/1vt;->b:I

    .line 1495777
    move v0, v0

    .line 1495778
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495773
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1495774
    move-object v0, v0

    .line 1495775
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1495764
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1495765
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1495766
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1495767
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1495768
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1495769
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1495770
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1495771
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1495772
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1495761
    iget v0, p0, LX/1vt;->c:I

    .line 1495762
    move v0, v0

    .line 1495763
    return v0
.end method
