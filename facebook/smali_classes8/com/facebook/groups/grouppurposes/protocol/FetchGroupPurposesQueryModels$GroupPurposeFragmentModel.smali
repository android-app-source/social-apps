.class public final Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9TS;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4587ee51
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1496141
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1496140
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1496138
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1496139
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1496135
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1496136
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1496137
    return-void
.end method

.method public static a(LX/9TS;)Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;
    .locals 13

    .prologue
    .line 1496100
    if-nez p0, :cond_0

    .line 1496101
    const/4 p0, 0x0

    .line 1496102
    :goto_0
    return-object p0

    .line 1496103
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    if-eqz v0, :cond_1

    .line 1496104
    check-cast p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    goto :goto_0

    .line 1496105
    :cond_1
    new-instance v0, LX/9TU;

    invoke-direct {v0}, LX/9TU;-><init>()V

    .line 1496106
    invoke-interface {p0}, LX/9TS;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9TU;->a:Ljava/lang/String;

    .line 1496107
    invoke-interface {p0}, LX/9TS;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9TU;->b:Ljava/lang/String;

    .line 1496108
    invoke-interface {p0}, LX/9TS;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v1

    iput-object v1, v0, LX/9TU;->c:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 1496109
    invoke-interface {p0}, LX/9TS;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9TU;->d:Ljava/lang/String;

    .line 1496110
    invoke-interface {p0}, LX/9TS;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9TU;->e:Ljava/lang/String;

    .line 1496111
    invoke-interface {p0}, LX/9TS;->hC_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    iput-object v1, v0, LX/9TU;->f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1496112
    const/4 v6, 0x1

    const/4 v12, 0x0

    const/4 v4, 0x0

    .line 1496113
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1496114
    iget-object v3, v0, LX/9TU;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1496115
    iget-object v5, v0, LX/9TU;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1496116
    iget-object v7, v0, LX/9TU;->c:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {v2, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1496117
    iget-object v8, v0, LX/9TU;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1496118
    iget-object v9, v0, LX/9TU;->e:Ljava/lang/String;

    invoke-virtual {v2, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1496119
    iget-object v10, v0, LX/9TU;->f:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v2, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1496120
    const/4 v11, 0x6

    invoke-virtual {v2, v11}, LX/186;->c(I)V

    .line 1496121
    invoke-virtual {v2, v12, v3}, LX/186;->b(II)V

    .line 1496122
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1496123
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1496124
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1496125
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1496126
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1496127
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1496128
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1496129
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1496130
    invoke-virtual {v3, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1496131
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1496132
    new-instance v3, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;-><init>(LX/15i;)V

    .line 1496133
    move-object p0, v3

    .line 1496134
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1496084
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1496085
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1496086
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1496087
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1496088
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1496089
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1496090
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->hC_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1496091
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1496092
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1496093
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1496094
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1496095
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1496096
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1496097
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1496098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1496099
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1496081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1496082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1496083
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496142
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->e:Ljava/lang/String;

    .line 1496143
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1496078
    new-instance v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-direct {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;-><init>()V

    .line 1496079
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1496080
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496076
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->f:Ljava/lang/String;

    .line 1496077
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496074
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 1496075
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496066
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->h:Ljava/lang/String;

    .line 1496067
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1496073
    const v0, 0x3d718142

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496071
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->i:Ljava/lang/String;

    .line 1496072
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1496070
    const v0, -0x182c8a41

    return v0
.end method

.method public final hC_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496068
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1496069
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
