.class public final Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1495987
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel;

    new-instance v1, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1495988
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1495989
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1495990
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1495991
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1495992
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1495993
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1495994
    if-eqz v2, :cond_3

    .line 1495995
    const-string v3, "groups_app_group_purposes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1495996
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1495997
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1495998
    if-eqz v3, :cond_2

    .line 1495999
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1496000
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1496001
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_1

    .line 1496002
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1496003
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1496004
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1496005
    if-eqz v0, :cond_0

    .line 1496006
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1496007
    invoke-static {v1, v0, p1, p2}, LX/9TW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1496008
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1496009
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1496010
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1496011
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1496012
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1496013
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1496014
    check-cast p1, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$Serializer;->a(Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
