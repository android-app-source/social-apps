.class public final Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9TS;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x83dcb9d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495958
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495953
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1495954
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1495955
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPurposeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495956
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1495957
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1495969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495970
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1495971
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1495972
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1495973
    invoke-direct {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x21916223

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1495974
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1495975
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1495976
    invoke-virtual {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->hC_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1495977
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1495978
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1495979
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1495980
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1495981
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1495982
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1495983
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1495984
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1495985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495986
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1495959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495960
    invoke-direct {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1495961
    invoke-direct {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x21916223

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1495962
    invoke-direct {p0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1495963
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;

    .line 1495964
    iput v3, v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->h:I

    .line 1495965
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495966
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1495967
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1495968
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495948
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1495949
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1495950
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1495951
    const/4 v0, 0x3

    const v1, -0x21916223

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->h:I

    .line 1495952
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1495945
    new-instance v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;-><init>()V

    .line 1495946
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1495947
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495943
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1495944
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495941
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 1495942
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495933
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 1495934
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1495940
    const v0, -0x694e8cc5

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495938
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1495939
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1495937
    const v0, -0x182c8a41

    return v0
.end method

.method public final hC_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495935
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1495936
    iget-object v0, p0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$FetchGroupPurposesQueryModel$GroupsAppGroupPurposesModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
