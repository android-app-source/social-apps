.class public final Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x67a8a495
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1493909
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1493908
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1493923
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1493924
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1493925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1493926
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1493927
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1493928
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1493929
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1493930
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1493931
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1493932
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1493934
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1493935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1493936
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1493933
    new-instance v0, LX/9SN;

    invoke-direct {v0, p1}, LX/9SN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1493920
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1493921
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1493922
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1493919
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1493916
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;-><init>()V

    .line 1493917
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1493918
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1493915
    const v0, 0x60f4a618

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1493914
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1493912
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->e:Ljava/lang/String;

    .line 1493913
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1493910
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->f:Ljava/lang/String;

    .line 1493911
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method
