.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4525ec95
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1472826
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1472825
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1472823
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1472824
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1472789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1472790
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x6cca0325

    invoke-static {v1, v0, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1472791
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1472792
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1472793
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1472794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1472795
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1472813
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1472814
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1472815
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6cca0325

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1472816
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1472817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    .line 1472818
    iput v3, v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->f:I

    .line 1472819
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1472820
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1472821
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1472822
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1472812
    new-instance v0, LX/9M8;

    invoke-direct {v0, p1}, LX/9M8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1472808
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1472809
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->e:Z

    .line 1472810
    const/4 v0, 0x1

    const v1, 0x6cca0325

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->f:I

    .line 1472811
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1472806
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1472807
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1472805
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1472803
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1472804
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1472800
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;-><init>()V

    .line 1472801
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1472802
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1472799
    const v0, 0x4de96b36    # 4.89514688E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1472798
    const v0, 0x41e065f

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupPostTopics"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1472796
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1472797
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
