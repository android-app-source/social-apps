.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x43eaa89
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1473728
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1473727
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1473725
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1473726
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1473723
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    .line 1473724
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    return-object v0
.end method

.method private a(Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;)V
    .locals 3
    .param p1    # Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1473717
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    .line 1473718
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1473719
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1473720
    if-eqz v0, :cond_0

    .line 1473721
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1473722
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1473691
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1473692
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1473693
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1473694
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1473695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1473696
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1473709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1473710
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1473711
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    .line 1473712
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1473713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;

    .line 1473714
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    .line 1473715
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1473716
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1473708
    new-instance v0, LX/9MN;

    invoke-direct {v0, p1}, LX/9MN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1473706
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1473707
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1473703
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1473704
    check-cast p2, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel$MessageModel;)V

    .line 1473705
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1473702
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1473699
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel$NodesModel;-><init>()V

    .line 1473700
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1473701
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1473698
    const v0, -0x354b03f7    # -5930500.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1473697
    const v0, 0x4c808d5

    return v0
.end method
