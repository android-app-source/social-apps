.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46f0e52b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1473813
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1473814
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1473817
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1473818
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1473815
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    .line 1473816
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1473799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1473800
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1473801
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1473802
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1473803
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1473804
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1473805
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1473806
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1473807
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    .line 1473808
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1473809
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;

    .line 1473810
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel$GroupStoriesModel;

    .line 1473811
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1473812
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1473798
    new-instance v0, LX/9MO;

    invoke-direct {v0, p1}, LX/9MO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1473796
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1473797
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1473795
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1473792
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsPostsModels$GroupDiscussionTopicPostsModel;-><init>()V

    .line 1473793
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1473794
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1473791
    const v0, -0x58f3278a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1473790
    const v0, 0x41e065f

    return v0
.end method
