.class public final Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d6f6471
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495258
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495288
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1495286
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1495287
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495284
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->e:Ljava/lang/String;

    .line 1495285
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495282
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 1495283
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495289
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->g:Ljava/lang/String;

    .line 1495290
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1495272
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495273
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1495274
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v1

    .line 1495275
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1495276
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1495277
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1495278
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1495279
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1495280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495281
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1495264
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495265
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1495266
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1495267
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1495268
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;

    .line 1495269
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 1495270
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495271
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1495261
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel$EdgesModel;-><init>()V

    .line 1495262
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1495263
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1495260
    const v0, -0x4722cfb1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1495259
    const v0, 0x368cefa6

    return v0
.end method
