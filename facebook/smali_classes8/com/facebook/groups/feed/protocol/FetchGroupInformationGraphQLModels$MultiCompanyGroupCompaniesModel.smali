.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/9N7;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6249b7f0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1484415
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1484414
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1484412
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1484413
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1484410
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    .line 1484411
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1484387
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1484388
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1484389
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1484390
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1484391
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1484392
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1484402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1484403
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1484404
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    .line 1484405
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1484406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;

    .line 1484407
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    .line 1484408
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1484409
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1484401
    new-instance v0, LX/9PB;

    invoke-direct {v0, p1}, LX/9PB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1484399
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1484400
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1484398
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1484395
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel;-><init>()V

    .line 1484396
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1484397
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1484394
    const v0, 0x76dc176f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1484393
    const v0, 0x41e065f

    return v0
.end method
