.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1480987
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1480986
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1480951
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1480952
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1480983
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1480984
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1480985
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;
    .locals 8

    .prologue
    .line 1480964
    if-nez p0, :cond_0

    .line 1480965
    const/4 p0, 0x0

    .line 1480966
    :goto_0
    return-object p0

    .line 1480967
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    if-eqz v0, :cond_1

    .line 1480968
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    goto :goto_0

    .line 1480969
    :cond_1
    new-instance v0, LX/9OV;

    invoke-direct {v0}, LX/9OV;-><init>()V

    .line 1480970
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;->a()I

    move-result v1

    iput v1, v0, LX/9OV;->a:I

    .line 1480971
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1480972
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1480973
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1480974
    iget v3, v0, LX/9OV;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1480975
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1480976
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1480977
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1480978
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1480979
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1480980
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;-><init>(LX/15i;)V

    .line 1480981
    move-object p0, v3

    .line 1480982
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1480962
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480963
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1480988
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1480989
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1480990
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1480991
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1480992
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1480959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1480960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1480961
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1480956
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1480957
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;->e:I

    .line 1480958
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1480953
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;-><init>()V

    .line 1480954
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1480955
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1480950
    const v0, -0x41fc389d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1480949
    const v0, 0x494a92f2

    return v0
.end method
