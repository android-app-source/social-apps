.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1472603
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1472604
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1472605
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1472606
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1472607
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1472608
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_6

    .line 1472609
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1472610
    :goto_0
    move v1, v2

    .line 1472611
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1472612
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1472613
    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;-><init>()V

    .line 1472614
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1472615
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1472616
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1472617
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1472618
    :cond_0
    return-object v1

    .line 1472619
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1472620
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1472621
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1472622
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1472623
    const-string v7, "can_viewer_change_post_topics"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1472624
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 1472625
    :cond_2
    const-string v7, "group_post_topics"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1472626
    const/4 v6, 0x0

    .line 1472627
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_c

    .line 1472628
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1472629
    :goto_2
    move v4, v6

    .line 1472630
    goto :goto_1

    .line 1472631
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1472632
    :cond_4
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1472633
    if-eqz v1, :cond_5

    .line 1472634
    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 1472635
    :cond_5
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1472636
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_6
    move v1, v2

    move v4, v2

    move v5, v2

    goto :goto_1

    .line 1472637
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1472638
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1472639
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1472640
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1472641
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_8

    if-eqz v8, :cond_8

    .line 1472642
    const-string v9, "nodes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1472643
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1472644
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_9

    .line 1472645
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1472646
    invoke-static {p1, v0}, LX/9MB;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1472647
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1472648
    :cond_9
    invoke-static {v7, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 1472649
    goto :goto_3

    .line 1472650
    :cond_a
    const-string v9, "page_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1472651
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1472652
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v10, :cond_12

    .line 1472653
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1472654
    :goto_5
    move v4, v8

    .line 1472655
    goto :goto_3

    .line 1472656
    :cond_b
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1472657
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1472658
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v4}, LX/186;->b(II)V

    .line 1472659
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_c
    move v4, v6

    move v7, v6

    goto :goto_3

    .line 1472660
    :cond_d
    const-string p0, "has_next_page"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 1472661
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v10, v4

    move v4, v9

    .line 1472662
    :cond_e
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_10

    .line 1472663
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1472664
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1472665
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v12, :cond_e

    .line 1472666
    const-string p0, "end_cursor"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 1472667
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_6

    .line 1472668
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1472669
    :cond_10
    const/4 v12, 0x2

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1472670
    invoke-virtual {v0, v8, v11}, LX/186;->b(II)V

    .line 1472671
    if-eqz v4, :cond_11

    .line 1472672
    invoke-virtual {v0, v9, v10}, LX/186;->a(IZ)V

    .line 1472673
    :cond_11
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_5

    :cond_12
    move v4, v8

    move v10, v8

    move v11, v8

    goto :goto_6
.end method
