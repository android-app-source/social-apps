.class public final Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x436adcc0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1490925
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1490924
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1490922
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1490923
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1490916
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->f:Ljava/lang/String;

    .line 1490917
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1490918
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1490919
    if-eqz v0, :cond_0

    .line 1490920
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1490921
    :cond_0
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1490914
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->e:Ljava/lang/String;

    .line 1490915
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1490912
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->f:Ljava/lang/String;

    .line 1490913
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1490900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1490901
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1490902
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1490903
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1490904
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1490905
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1490906
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1490907
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1490908
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1490909
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1490910
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1490911
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1490887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1490888
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1490889
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    .line 1490890
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1490891
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    .line 1490892
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->g:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    .line 1490893
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1490894
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    .line 1490895
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1490896
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    .line 1490897
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    .line 1490898
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1490899
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1490926
    new-instance v0, LX/9RP;

    invoke-direct {v0, p1}, LX/9RP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1490886
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1490880
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1490881
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1490882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1490883
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1490884
    :goto_0
    return-void

    .line 1490885
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1490877
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1490878
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->a(Ljava/lang/String;)V

    .line 1490879
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1490874
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;-><init>()V

    .line 1490875
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1490876
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1490873
    const v0, 0x23fa3326

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1490872
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1490868
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->g:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->g:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    .line 1490869
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->g:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRelatedGroups"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1490870
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    .line 1490871
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    return-object v0
.end method
