.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1473037
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1473038
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1473035
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1473036
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1473009
    if-nez p1, :cond_0

    .line 1473010
    :goto_0
    return v0

    .line 1473011
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1473012
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1473013
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1473014
    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;

    invoke-virtual {p0, p1, v4, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 1473015
    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1473016
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1473017
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1473018
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1473019
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1473020
    :sswitch_1
    const-class v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1473021
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1473022
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1473023
    const v3, -0x49835a33

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1473024
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1473025
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1473026
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1473027
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1473028
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1473029
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1473030
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 1473031
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1473032
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1473033
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 1473034
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5e4dc4ce -> :sswitch_0
        -0x49835a33 -> :sswitch_2
        0x7b0ef869 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1473008
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1473004
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1473005
    if-eqz v0, :cond_0

    .line 1473006
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1473007
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1472995
    sparse-switch p2, :sswitch_data_0

    .line 1472996
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1472997
    :sswitch_0
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1472998
    invoke-static {v0, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1472999
    :goto_0
    :sswitch_1
    return-void

    .line 1473000
    :sswitch_2
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1473001
    invoke-static {v0, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1473002
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1473003
    const v1, -0x49835a33

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5e4dc4ce -> :sswitch_0
        -0x49835a33 -> :sswitch_1
        0x7b0ef869 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1472989
    if-eqz p1, :cond_0

    .line 1472990
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    move-result-object v1

    .line 1472991
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    .line 1472992
    if-eq v0, v1, :cond_0

    .line 1472993
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1472994
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1472988
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1472986
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1472987
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1472955
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1472956
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1472957
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a:LX/15i;

    .line 1472958
    iput p2, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->b:I

    .line 1472959
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1472985
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1472984
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1472981
    iget v0, p0, LX/1vt;->c:I

    .line 1472982
    move v0, v0

    .line 1472983
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1472978
    iget v0, p0, LX/1vt;->c:I

    .line 1472979
    move v0, v0

    .line 1472980
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1472975
    iget v0, p0, LX/1vt;->b:I

    .line 1472976
    move v0, v0

    .line 1472977
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1472972
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1472973
    move-object v0, v0

    .line 1472974
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1472963
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1472964
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1472965
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1472966
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1472967
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1472968
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1472969
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1472970
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1472971
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1472960
    iget v0, p0, LX/1vt;->c:I

    .line 1472961
    move v0, v0

    .line 1472962
    return v0
.end method
