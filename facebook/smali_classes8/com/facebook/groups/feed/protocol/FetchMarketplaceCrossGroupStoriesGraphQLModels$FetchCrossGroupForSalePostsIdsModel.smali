.class public final Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7594cef1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495190
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495189
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1495174
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1495175
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1495183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495184
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1495185
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1495186
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1495187
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495188
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1495191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495192
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1495193
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    .line 1495194
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1495195
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;

    .line 1495196
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    .line 1495197
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495198
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495181
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    .line 1495182
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1495178
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;-><init>()V

    .line 1495179
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1495180
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1495177
    const v0, 0x11765e44

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1495176
    const v0, -0x6747e1ce

    return v0
.end method
