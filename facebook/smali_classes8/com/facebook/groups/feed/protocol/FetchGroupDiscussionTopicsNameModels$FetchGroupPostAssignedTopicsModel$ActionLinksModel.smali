.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4543ad86
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1473134
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1473135
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1473132
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1473133
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1473124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1473125
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1473126
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x5e4dc4ce

    invoke-static {v2, v1, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1473127
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1473128
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1473129
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1473130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1473131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1473114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1473115
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1473116
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5e4dc4ce

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1473117
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1473118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;

    .line 1473119
    iput v3, v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->f:I

    .line 1473120
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1473121
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1473122
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1473123
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1473136
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1473137
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1473138
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1473111
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1473112
    const/4 v0, 0x1

    const v1, -0x5e4dc4ce

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->f:I

    .line 1473113
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1473108
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;-><init>()V

    .line 1473109
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1473110
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1473107
    const v0, -0x7ae1996e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1473104
    const v0, -0x6829c9fb

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupStoryTopics"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1473105
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1473106
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
