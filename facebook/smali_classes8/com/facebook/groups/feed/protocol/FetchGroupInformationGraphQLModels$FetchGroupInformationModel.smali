.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/9N6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1df75c57
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Serializer;
.end annotation


# instance fields
.field private A:I

.field private B:Z

.field private C:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Z

.field private v:Z

.field private w:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1480756
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1480757
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1480758
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1480759
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1480760
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1480761
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1480762
    return-void
.end method

.method public static a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1480763
    if-nez p0, :cond_0

    .line 1480764
    const/4 p0, 0x0

    .line 1480765
    :goto_0
    return-object p0

    .line 1480766
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    .line 1480767
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    goto :goto_0

    .line 1480768
    :cond_1
    new-instance v0, LX/9OQ;

    invoke-direct {v0}, LX/9OQ;-><init>()V

    .line 1480769
    invoke-interface {p0}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 1480770
    invoke-interface {p0}, LX/9N1;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->b:Z

    .line 1480771
    invoke-interface {p0}, LX/9N1;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->c:Z

    .line 1480772
    invoke-interface {p0}, LX/9N1;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->d:Z

    .line 1480773
    invoke-interface {p0}, LX/9N6;->z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    .line 1480774
    invoke-interface {p0}, LX/9N1;->e()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1480775
    invoke-interface {p0}, LX/9N1;->hQ_()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1480776
    invoke-interface {p0}, LX/9N1;->hP_()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    .line 1480777
    invoke-interface {p0}, LX/9N1;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    .line 1480778
    invoke-interface {p0}, LX/9N6;->A()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/9OQ;->j:LX/15i;

    iput v1, v0, LX/9OQ;->k:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1480779
    invoke-interface {p0}, LX/9N1;->k()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a(Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->l:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1480780
    invoke-interface {p0}, LX/9N6;->B()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;->a(Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->m:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1480781
    invoke-interface {p0}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->n:Ljava/lang/String;

    .line 1480782
    invoke-interface {p0}, LX/9N1;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->o:Z

    .line 1480783
    invoke-interface {p0}, LX/9N6;->C()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/9OQ;->p:LX/15i;

    iput v1, v0, LX/9OQ;->q:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1480784
    invoke-interface {p0}, LX/9N1;->n()LX/175;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1480785
    invoke-interface {p0}, LX/9N1;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->s:Z

    .line 1480786
    invoke-interface {p0}, LX/9N1;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->t:Z

    .line 1480787
    invoke-interface {p0}, LX/9N6;->D()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->u:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    .line 1480788
    invoke-interface {p0}, LX/9N1;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->v:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1480789
    invoke-interface {p0}, LX/9N6;->E()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, v0, LX/9OQ;->w:LX/15i;

    iput v1, v0, LX/9OQ;->x:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1480790
    invoke-interface {p0}, LX/9N6;->F()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->y:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    .line 1480791
    invoke-interface {p0}, LX/9N1;->r()I

    move-result v1

    iput v1, v0, LX/9OQ;->z:I

    .line 1480792
    invoke-interface {p0}, LX/9N1;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->A:Z

    .line 1480793
    invoke-interface {p0}, LX/9N1;->t()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->B:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    .line 1480794
    invoke-interface {p0}, LX/9N1;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->C:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1480795
    invoke-interface {p0}, LX/9N1;->v()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->D:Ljava/lang/String;

    .line 1480796
    invoke-interface {p0}, LX/9N6;->G()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->E:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 1480797
    invoke-interface {p0}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1480798
    invoke-interface {p0}, LX/9N1;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->G:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1480799
    invoke-virtual {v0}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object p0

    goto/16 :goto_0

    .line 1480800
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1480801
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1480802
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1480803
    iput p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A:I

    .line 1480804
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1480805
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1480806
    if-eqz v0, :cond_0

    .line 1480807
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1480808
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1480809
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->G:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1480810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1480811
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1480812
    if-eqz v0, :cond_0

    .line 1480813
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x1c

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1480814
    :cond_0
    return-void

    .line 1480815
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1480816
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1480817
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1480818
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1480819
    if-eqz v0, :cond_0

    .line 1480820
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x13

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1480821
    :cond_0
    return-void

    .line 1480822
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupPostTopics"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1480823
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480824
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupSellConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480825
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    return-object v0
.end method

.method public final C()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLearningCourseUnit"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480826
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480827
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->s:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubgroupsFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480828
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final E()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480829
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480830
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTipsChannel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480645
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic G()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerInviteToGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480831
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminAwareGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480834
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 1480835
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    return-object v0
.end method

.method public final I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommunityFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480912
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    .line 1480913
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    return-object v0
.end method

.method public final J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480910
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1480911
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    return-object v0
.end method

.method public final K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480908
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1480909
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    return-object v0
.end method

.method public final L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480906
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    .line 1480907
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    return-object v0
.end method

.method public final M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480904
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    .line 1480905
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    return-object v0
.end method

.method public final N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480902
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->o:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->o:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1480903
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->o:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    return-object v0
.end method

.method public final O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupSellConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480900
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->p:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->p:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1480901
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->p:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480832
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1480833
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubgroupsFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480898
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    .line 1480899
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    return-object v0
.end method

.method public final R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTipsChannel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480896
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->z:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->z:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    .line 1480897
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->z:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    return-object v0
.end method

.method public final S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480894
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    .line 1480895
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    return-object v0
.end method

.method public final T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerInviteToGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480892
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->F:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->F:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 1480893
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->F:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 26

    .prologue
    .line 1480836
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1480837
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1480838
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1480839
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1480840
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1480841
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1480842
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1480843
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    const v11, -0x4d1fbe34

    invoke-static {v10, v9, v11}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1480844
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1480845
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1480846
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1480847
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v13

    iget-object v14, v13, LX/1vs;->a:LX/15i;

    iget v13, v13, LX/1vs;->b:I

    const v15, 0x722fec04

    invoke-static {v14, v13, v15}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1480848
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1480849
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1480850
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 1480851
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    const v19, 0x6da0c94

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1480852
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1480853
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1480854
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1480855
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 1480856
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1480857
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    .line 1480858
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1480859
    const/16 v25, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1480860
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1480861
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->f:Z

    move/from16 v25, v0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1480862
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->g:Z

    move/from16 v25, v0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1480863
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->h:Z

    move/from16 v25, v0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1480864
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1480865
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1480866
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1480867
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1480868
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1480869
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1480870
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1480871
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1480872
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1480873
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1480874
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1480875
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1480876
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1480877
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1480878
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1480879
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480880
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480881
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480882
    const/16 v3, 0x16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1480883
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->B:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1480884
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480885
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480886
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480887
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480888
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480889
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1480890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1480891
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1480668
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1480669
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1480670
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 1480671
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1480672
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480673
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 1480674
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1480675
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    .line 1480676
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1480677
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480678
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    .line 1480679
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1480680
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1480681
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1480682
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480683
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1480684
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1480685
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1480686
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1480687
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480688
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1480689
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1480690
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    .line 1480691
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1480692
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480693
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    .line 1480694
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1480695
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    .line 1480696
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1480697
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480698
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    .line 1480699
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1480700
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4d1fbe34

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1480701
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1480702
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480703
    iput v3, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->n:I

    move-object v1, v0

    .line 1480704
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1480705
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1480706
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1480707
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480708
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->o:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1480709
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1480710
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1480711
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1480712
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480713
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->p:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1480714
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 1480715
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x722fec04

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1480716
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1480717
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480718
    iput v3, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->s:I

    move-object v1, v0

    .line 1480719
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1480720
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1480721
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1480722
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480723
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1480724
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1480725
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    .line 1480726
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1480727
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480728
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    .line 1480729
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_c

    .line 1480730
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6da0c94

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1480731
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1480732
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480733
    iput v3, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->y:I

    move-object v1, v0

    .line 1480734
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1480735
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    .line 1480736
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1480737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480738
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->z:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    .line 1480739
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1480740
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    .line 1480741
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1480742
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480743
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    .line 1480744
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1480745
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 1480746
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 1480747
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1480748
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->F:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 1480749
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1480750
    if-nez v1, :cond_10

    :goto_0
    return-object p0

    .line 1480751
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1480752
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1480753
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_10
    move-object p0, v1

    .line 1480754
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1480755
    new-instance v0, LX/9OR;

    invoke-direct {v0, p1}, LX/9OR;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480640
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1480627
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1480628
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->f:Z

    .line 1480629
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->g:Z

    .line 1480630
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->h:Z

    .line 1480631
    const/16 v0, 0x9

    const v1, -0x4d1fbe34

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->n:I

    .line 1480632
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r:Z

    .line 1480633
    const/16 v0, 0xe

    const v1, 0x722fec04

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->s:I

    .line 1480634
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u:Z

    .line 1480635
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v:Z

    .line 1480636
    const/16 v0, 0x14

    const v1, 0x6da0c94

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->y:I

    .line 1480637
    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A:I

    .line 1480638
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->B:Z

    .line 1480639
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1480613
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1480614
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1480615
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1480616
    const/16 v0, 0x13

    iput v0, p2, LX/18L;->c:I

    .line 1480617
    :goto_0
    return-void

    .line 1480618
    :cond_0
    const-string v0, "unread_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1480619
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1480620
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1480621
    const/16 v0, 0x16

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1480622
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1480623
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1480624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1480625
    const/16 v0, 0x1c

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1480626
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1480591
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1480592
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1480593
    :cond_0
    :goto_0
    return-void

    .line 1480594
    :cond_1
    const-string v0, "unread_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1480595
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(I)V

    goto :goto_0

    .line 1480596
    :cond_2
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1480597
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1480610
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;-><init>()V

    .line 1480611
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1480612
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1480608
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480609
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1480606
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480607
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1480604
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480605
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1480590
    const v0, -0x3df831c4

    return v0
.end method

.method public final synthetic e()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480598
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1480599
    const v0, 0x41e065f

    return v0
.end method

.method public final synthetic hP_()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480600
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic hQ_()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480601
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480602
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480603
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480641
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q:Ljava/lang/String;

    .line 1480642
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1480643
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480644
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r:Z

    return v0
.end method

.method public final synthetic n()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480646
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1480647
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480648
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u:Z

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 1480649
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480650
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480651
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1480652
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 1480653
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480654
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A:I

    return v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 1480655
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1480656
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->B:Z

    return v0
.end method

.method public final synthetic t()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480657
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480658
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->D:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->D:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1480659
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->D:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480660
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E:Ljava/lang/String;

    .line 1480661
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480662
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->G:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->G:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1480663
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->G:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480664
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1480665
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminAwareGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480666
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommunityFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1480667
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    return-object v0
.end method
