.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1472599
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1472600
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1472597
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1472598
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1472565
    if-nez p1, :cond_0

    .line 1472566
    :goto_0
    return v0

    .line 1472567
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1472568
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1472569
    :sswitch_0
    const-class v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1472570
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1472571
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 1472572
    const v3, -0x22889111

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1472573
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1472574
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1472575
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1472576
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1472577
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1472578
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1472579
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1472580
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1472581
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1472582
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1472583
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1472584
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 1472585
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1472586
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1472587
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1472588
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1472589
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1472590
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1472591
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1472592
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 1472593
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1472594
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1472595
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 1472596
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x22889111 -> :sswitch_3
        0x4a376de7 -> :sswitch_1
        0x6cca0325 -> :sswitch_0
        0x72f26670 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1472564
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1472560
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1472561
    if-eqz v0, :cond_0

    .line 1472562
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1472563
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1472553
    sparse-switch p2, :sswitch_data_0

    .line 1472554
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1472555
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1472556
    invoke-static {v0, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1472557
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1472558
    const v1, -0x22889111

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1472559
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x22889111 -> :sswitch_1
        0x4a376de7 -> :sswitch_1
        0x6cca0325 -> :sswitch_0
        0x72f26670 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1472547
    if-eqz p1, :cond_0

    .line 1472548
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1472549
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    .line 1472550
    if-eq v0, v1, :cond_0

    .line 1472551
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1472552
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1472546
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1472601
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1472602
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1472515
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1472516
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1472517
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1472518
    iput p2, p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->b:I

    .line 1472519
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1472520
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1472521
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1472522
    iget v0, p0, LX/1vt;->c:I

    .line 1472523
    move v0, v0

    .line 1472524
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1472525
    iget v0, p0, LX/1vt;->c:I

    .line 1472526
    move v0, v0

    .line 1472527
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1472528
    iget v0, p0, LX/1vt;->b:I

    .line 1472529
    move v0, v0

    .line 1472530
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1472543
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1472544
    move-object v0, v0

    .line 1472545
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1472531
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1472532
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1472533
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1472534
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1472535
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1472536
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1472537
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1472538
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1472539
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1472540
    iget v0, p0, LX/1vt;->c:I

    .line 1472541
    move v0, v0

    .line 1472542
    return v0
.end method
