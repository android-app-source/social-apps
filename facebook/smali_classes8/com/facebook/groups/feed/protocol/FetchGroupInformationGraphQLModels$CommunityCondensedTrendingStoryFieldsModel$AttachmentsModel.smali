.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4160dfb7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1475051
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1475050
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1475048
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1475049
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1475045
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1475046
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1475047
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;
    .locals 13

    .prologue
    .line 1475012
    if-nez p0, :cond_0

    .line 1475013
    const/4 p0, 0x0

    .line 1475014
    :goto_0
    return-object p0

    .line 1475015
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    if-eqz v0, :cond_1

    .line 1475016
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    goto :goto_0

    .line 1475017
    :cond_1
    new-instance v1, LX/9NC;

    invoke-direct {v1}, LX/9NC;-><init>()V

    .line 1475018
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v0

    iput-object v0, v1, LX/9NC;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    .line 1475019
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v0

    iput-object v0, v1, LX/9NC;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    .line 1475020
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1475021
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1475022
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1475023
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1475024
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/9NC;->c:LX/0Px;

    .line 1475025
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/9NC;->d:Ljava/lang/String;

    .line 1475026
    const/4 v8, 0x1

    const/4 v12, 0x0

    const/4 v6, 0x0

    .line 1475027
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1475028
    iget-object v5, v1, LX/9NC;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1475029
    iget-object v7, v1, LX/9NC;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1475030
    iget-object v9, v1, LX/9NC;->c:LX/0Px;

    invoke-virtual {v4, v9}, LX/186;->c(Ljava/util/List;)I

    move-result v9

    .line 1475031
    iget-object v10, v1, LX/9NC;->d:Ljava/lang/String;

    invoke-virtual {v4, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1475032
    const/4 v11, 0x4

    invoke-virtual {v4, v11}, LX/186;->c(I)V

    .line 1475033
    invoke-virtual {v4, v12, v5}, LX/186;->b(II)V

    .line 1475034
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1475035
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1475036
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1475037
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1475038
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1475039
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1475040
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1475041
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1475042
    new-instance v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;-><init>(LX/15i;)V

    .line 1475043
    move-object p0, v5

    .line 1475044
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475010
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    .line 1475011
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    return-object v0
.end method

.method private k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1474995
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    .line 1474996
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1474998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1474999
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1475000
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1475001
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->c(Ljava/util/List;)I

    move-result v2

    .line 1475002
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1475003
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1475004
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1475005
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1475006
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1475007
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1475008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1475009
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1475052
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1475053
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1475054
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    .line 1475055
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1475056
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    .line 1475057
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    .line 1475058
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1475059
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    .line 1475060
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1475061
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    .line 1475062
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    .line 1475063
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1475064
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1474997
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1474992
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;-><init>()V

    .line 1474993
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1474994
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1474991
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1474989
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->g:Ljava/util/List;

    .line 1474990
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1474987
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->h:Ljava/lang/String;

    .line 1474988
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1474986
    const v0, 0x25fa1567

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1474985
    const v0, -0x4b900828

    return v0
.end method
