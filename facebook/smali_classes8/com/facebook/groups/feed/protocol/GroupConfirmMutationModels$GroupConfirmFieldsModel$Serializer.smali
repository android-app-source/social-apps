.class public final Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1495580
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1495581
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1495582
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1495583
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1495584
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1495585
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1495586
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1495587
    if-eqz v2, :cond_0

    .line 1495588
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1495589
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1495590
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1495591
    if-eqz v2, :cond_1

    .line 1495592
    const-string p0, "group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1495593
    invoke-static {v1, v2, p1}, LX/9TJ;->a(LX/15i;ILX/0nX;)V

    .line 1495594
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1495595
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1495596
    check-cast p1, Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
