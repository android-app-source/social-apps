.class public final Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5803d6c9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471782
    const-class v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471781
    const-class v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1471779
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1471780
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1471773
    iput-boolean p1, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->e:Z

    .line 1471774
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1471775
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1471776
    if-eqz v0, :cond_0

    .line 1471777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1471778
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1471767
    iput-boolean p1, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->g:Z

    .line 1471768
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1471769
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1471770
    if-eqz v0, :cond_0

    .line 1471771
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1471772
    :cond_0
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471765
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->h:Ljava/lang/String;

    .line 1471766
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471723
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->i:Ljava/lang/String;

    .line 1471724
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1471753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1471754
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1471755
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1471756
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1471757
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1471758
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 1471759
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1471760
    const/4 v0, 0x2

    iget-boolean v3, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->g:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1471761
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1471762
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1471763
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1471764
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1471745
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1471746
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1471747
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1471748
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1471749
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;

    .line 1471750
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1471751
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1471752
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1471744
    new-instance v0, LX/9Lo;

    invoke-direct {v0, p1}, LX/9Lo;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471783
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1471740
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1471741
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->e:Z

    .line 1471742
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->g:Z

    .line 1471743
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1471730
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1471731
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1471732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1471733
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1471734
    :goto_0
    return-void

    .line 1471735
    :cond_0
    const-string v0, "have_comments_been_disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1471736
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1471737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1471738
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1471739
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1471725
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1471726
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->a(Z)V

    .line 1471727
    :cond_0
    :goto_0
    return-void

    .line 1471728
    :cond_1
    const-string v0, "have_comments_been_disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1471729
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1471720
    new-instance v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;-><init>()V

    .line 1471721
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1471722
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1471719
    const v0, -0x69895040

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1471718
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1471716
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1471717
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->e:Z

    return v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471714
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1471715
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1471712
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1471713
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->g:Z

    return v0
.end method
