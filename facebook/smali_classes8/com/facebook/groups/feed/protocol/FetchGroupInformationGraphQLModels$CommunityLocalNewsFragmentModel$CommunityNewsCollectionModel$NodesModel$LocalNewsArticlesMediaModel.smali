.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5eaf74b2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478400
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478440
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1478438
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478439
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1478435
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478436
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478437
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;
    .locals 8

    .prologue
    .line 1478415
    if-nez p0, :cond_0

    .line 1478416
    const/4 p0, 0x0

    .line 1478417
    :goto_0
    return-object p0

    .line 1478418
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    if-eqz v0, :cond_1

    .line 1478419
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    goto :goto_0

    .line 1478420
    :cond_1
    new-instance v0, LX/9O8;

    invoke-direct {v0}, LX/9O8;-><init>()V

    .line 1478421
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v1

    iput-object v1, v0, LX/9O8;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    .line 1478422
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1478423
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1478424
    iget-object v3, v0, LX/9O8;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1478425
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1478426
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1478427
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1478428
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1478429
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1478430
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1478431
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1478432
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;-><init>(LX/15i;)V

    .line 1478433
    move-object p0, v3

    .line 1478434
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1478409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478410
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1478411
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1478412
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1478413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478414
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1478441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478442
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1478443
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    .line 1478444
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1478445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    .line 1478446
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    .line 1478447
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478448
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478408
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1478405
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;-><init>()V

    .line 1478406
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478407
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1478404
    const v0, 0x7b545831

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1478403
    const v0, 0x745df5c6

    return v0
.end method

.method public final j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478401
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    .line 1478402
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    return-object v0
.end method
