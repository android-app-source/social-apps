.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x38498cdc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1481053
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1481052
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1481050
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1481051
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1481047
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1481048
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1481049
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;
    .locals 8

    .prologue
    .line 1481027
    if-nez p0, :cond_0

    .line 1481028
    const/4 p0, 0x0

    .line 1481029
    :goto_0
    return-object p0

    .line 1481030
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    if-eqz v0, :cond_1

    .line 1481031
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    goto :goto_0

    .line 1481032
    :cond_1
    new-instance v0, LX/9OU;

    invoke-direct {v0}, LX/9OU;-><init>()V

    .line 1481033
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OU;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    .line 1481034
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1481035
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1481036
    iget-object v3, v0, LX/9OU;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1481037
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1481038
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1481039
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1481040
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1481041
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1481042
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1481043
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1481044
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;-><init>(LX/15i;)V

    .line 1481045
    move-object p0, v3

    .line 1481046
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1481021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1481022
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1481023
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1481024
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1481025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1481026
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1481013
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1481014
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1481015
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    .line 1481016
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1481017
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    .line 1481018
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    .line 1481019
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1481020
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1481054
    new-instance v0, LX/9OW;

    invoke-direct {v0, p1}, LX/9OW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1481012
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1481002
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1481003
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1481011
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1481008
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;-><init>()V

    .line 1481009
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1481010
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1481007
    const v0, -0x31491704

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1481006
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1481004
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    .line 1481005
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    return-object v0
.end method
