.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1480015
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1480016
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1480017
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1480018
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1480019
    if-nez p1, :cond_0

    move v0, v6

    .line 1480020
    :goto_0
    return v0

    .line 1480021
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1480022
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1480023
    :sswitch_0
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480024
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480025
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480026
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480027
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1480028
    :sswitch_1
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1480029
    const v2, -0x588d55e

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1480030
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480031
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480032
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1480033
    :sswitch_2
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480034
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480035
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480036
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480037
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1480038
    :sswitch_3
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1480039
    const v2, -0x31e8a21

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1480040
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480041
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480042
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1480043
    :sswitch_4
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1480044
    const v2, -0x7bbc1960

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1480045
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480046
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480047
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1480048
    :sswitch_5
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480049
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480050
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480051
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480052
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480053
    :sswitch_6
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480054
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480055
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480056
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480057
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480058
    :sswitch_7
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480059
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480060
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480061
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480062
    :sswitch_8
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480063
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480064
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1480065
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1480066
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480067
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480068
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1480069
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480070
    :sswitch_9
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480071
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480072
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480073
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480074
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480075
    :sswitch_a
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1480076
    const v2, 0x14954af1

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1480077
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480078
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480079
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480080
    :sswitch_b
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480081
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480082
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    move-result-object v2

    .line 1480083
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1480084
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480085
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480086
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1480087
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480088
    :sswitch_c
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1480089
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1480090
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1480091
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1480092
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480093
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480094
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1480095
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480096
    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480097
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480098
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480099
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1480100
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480101
    :sswitch_e
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480102
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480103
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480104
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480105
    :sswitch_f
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480106
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480107
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480108
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480109
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480110
    :sswitch_10
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480111
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480112
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v2

    .line 1480113
    const v3, -0xc745248

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1480114
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480115
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480116
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1480117
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480118
    :sswitch_11
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480119
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480120
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1480121
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480122
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    move-object v0, p3

    .line 1480123
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1480124
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480125
    :sswitch_12
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    .line 1480126
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1480127
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480128
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480129
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480130
    :sswitch_13
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480131
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480132
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480133
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480134
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480135
    :sswitch_14
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480136
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480137
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480138
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480139
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480140
    :sswitch_15
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480141
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480142
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480143
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480144
    :sswitch_16
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480145
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480146
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480147
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480148
    :sswitch_17
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480149
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480150
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480151
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480152
    :sswitch_18
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480153
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480154
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480155
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480156
    :sswitch_19
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1480157
    const v2, -0x145860f9

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1480158
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1480159
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1480160
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480161
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480162
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1480163
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480164
    :sswitch_1a
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480165
    invoke-virtual {p0, p1, v1, v6}, LX/15i;->a(III)I

    move-result v2

    .line 1480166
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480167
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480168
    invoke-virtual {p3, v1, v2, v6}, LX/186;->a(III)V

    .line 1480169
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480170
    :sswitch_1b
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480171
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480172
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480173
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480174
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480175
    :sswitch_1c
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1480176
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1480177
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1480178
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1480179
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1480180
    :sswitch_1d
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v0

    .line 1480181
    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 1480182
    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1480183
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1480184
    invoke-virtual {p3, v6, v0, v6}, LX/186;->a(III)V

    .line 1480185
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1480186
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7bbc1960 -> :sswitch_5
        -0x73721085 -> :sswitch_19
        -0x69b2f13c -> :sswitch_0
        -0x59f9285f -> :sswitch_15
        -0x5323d551 -> :sswitch_3
        -0x4d1fbe34 -> :sswitch_e
        -0x4a0261f8 -> :sswitch_10
        -0x4959043f -> :sswitch_a
        -0x4553d923 -> :sswitch_8
        -0x4020ab03 -> :sswitch_16
        -0x3a311122 -> :sswitch_d
        -0x2dcee0eb -> :sswitch_f
        -0x296bbbdf -> :sswitch_12
        -0x22c19c9e -> :sswitch_13
        -0x20692170 -> :sswitch_17
        -0x145860f9 -> :sswitch_1a
        -0x144dcd62 -> :sswitch_18
        -0xc745248 -> :sswitch_11
        -0x588d55e -> :sswitch_2
        -0x31e8a21 -> :sswitch_4
        0x12e2a8a6 -> :sswitch_14
        0x14954af1 -> :sswitch_b
        0x1cb69aae -> :sswitch_c
        0x2dab8fe6 -> :sswitch_9
        0x38e3eeb1 -> :sswitch_6
        0x544c82e3 -> :sswitch_1
        0x5f7e61dd -> :sswitch_7
        0x6496211f -> :sswitch_1c
        0x722fec04 -> :sswitch_1d
        0x7e98ef71 -> :sswitch_1b
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1480187
    if-nez p0, :cond_0

    move v0, v1

    .line 1480188
    :goto_0
    return v0

    .line 1480189
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1480190
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1480191
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1480192
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1480193
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1480194
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1480195
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1480196
    const/4 v7, 0x0

    .line 1480197
    const/4 v1, 0x0

    .line 1480198
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1480199
    invoke-static {v2, v3, v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1480200
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1480201
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1480202
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1480203
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1480204
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1480205
    if-eqz v0, :cond_0

    .line 1480206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1480207
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1480208
    if-eqz p0, :cond_0

    .line 1480209
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1480210
    if-eq v0, p0, :cond_0

    .line 1480211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1480212
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1480213
    sparse-switch p2, :sswitch_data_0

    .line 1480214
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1480215
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1480216
    const v1, -0x588d55e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1480217
    :goto_0
    :sswitch_1
    return-void

    .line 1480218
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1480219
    const v1, -0x31e8a21

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1480220
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1480221
    const v1, -0x7bbc1960

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1480222
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1480223
    const v1, 0x14954af1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1480224
    :sswitch_5
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1480225
    invoke-static {v0, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1480226
    :sswitch_6
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1480227
    const v1, -0xc745248

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1480228
    :sswitch_7
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    .line 1480229
    invoke-static {v0, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1480230
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1480231
    const v1, -0x145860f9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1480232
    :sswitch_9
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1480233
    invoke-static {v0, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7bbc1960 -> :sswitch_1
        -0x73721085 -> :sswitch_8
        -0x69b2f13c -> :sswitch_1
        -0x59f9285f -> :sswitch_1
        -0x5323d551 -> :sswitch_2
        -0x4d1fbe34 -> :sswitch_1
        -0x4a0261f8 -> :sswitch_6
        -0x4959043f -> :sswitch_4
        -0x4553d923 -> :sswitch_1
        -0x4020ab03 -> :sswitch_1
        -0x3a311122 -> :sswitch_1
        -0x2dcee0eb -> :sswitch_1
        -0x296bbbdf -> :sswitch_7
        -0x22c19c9e -> :sswitch_1
        -0x20692170 -> :sswitch_1
        -0x145860f9 -> :sswitch_1
        -0x144dcd62 -> :sswitch_1
        -0xc745248 -> :sswitch_1
        -0x588d55e -> :sswitch_1
        -0x31e8a21 -> :sswitch_3
        0x12e2a8a6 -> :sswitch_1
        0x14954af1 -> :sswitch_1
        0x1cb69aae -> :sswitch_5
        0x2dab8fe6 -> :sswitch_1
        0x38e3eeb1 -> :sswitch_1
        0x544c82e3 -> :sswitch_0
        0x5f7e61dd -> :sswitch_1
        0x6496211f -> :sswitch_1
        0x722fec04 -> :sswitch_9
        0x7e98ef71 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1480234
    if-nez p1, :cond_0

    move v0, v1

    .line 1480235
    :goto_0
    return v0

    .line 1480236
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1480237
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1480238
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1480239
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1480240
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1480241
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1480242
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1480243
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1480008
    if-eqz p1, :cond_0

    .line 1480009
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1480010
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1480011
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1480012
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1480013
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1480014
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1480244
    if-eqz p1, :cond_0

    .line 1480245
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1480246
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    .line 1480247
    if-eq v0, v1, :cond_0

    .line 1480248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1480249
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1480007
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1480005
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1480006
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1480000
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1480001
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1480002
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1480003
    iput p2, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->b:I

    .line 1480004
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1479999
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1479998
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1479995
    iget v0, p0, LX/1vt;->c:I

    .line 1479996
    move v0, v0

    .line 1479997
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1479992
    iget v0, p0, LX/1vt;->c:I

    .line 1479993
    move v0, v0

    .line 1479994
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1479989
    iget v0, p0, LX/1vt;->b:I

    .line 1479990
    move v0, v0

    .line 1479991
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1479986
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1479987
    move-object v0, v0

    .line 1479988
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1479977
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1479978
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1479979
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1479980
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1479981
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1479982
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1479983
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1479984
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1479985
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1479974
    iget v0, p0, LX/1vt;->c:I

    .line 1479975
    move v0, v0

    .line 1479976
    return v0
.end method
