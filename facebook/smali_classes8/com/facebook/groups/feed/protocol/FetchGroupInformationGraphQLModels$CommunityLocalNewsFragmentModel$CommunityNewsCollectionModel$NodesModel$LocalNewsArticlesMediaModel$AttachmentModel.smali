.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xdcefbdb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478377
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478376
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1478374
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478375
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1478371
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478372
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478373
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;
    .locals 11

    .prologue
    .line 1478342
    if-nez p0, :cond_0

    .line 1478343
    const/4 p0, 0x0

    .line 1478344
    :goto_0
    return-object p0

    .line 1478345
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    if-eqz v0, :cond_1

    .line 1478346
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    goto :goto_0

    .line 1478347
    :cond_1
    new-instance v0, LX/9O3;

    invoke-direct {v0}, LX/9O3;-><init>()V

    .line 1478348
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v1

    iput-object v1, v0, LX/9O3;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    .line 1478349
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v1

    iput-object v1, v0, LX/9O3;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    .line 1478350
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9O3;->c:Ljava/lang/String;

    .line 1478351
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9O3;->d:Ljava/lang/String;

    .line 1478352
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1478353
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1478354
    iget-object v3, v0, LX/9O3;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1478355
    iget-object v5, v0, LX/9O3;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1478356
    iget-object v7, v0, LX/9O3;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1478357
    iget-object v8, v0, LX/9O3;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1478358
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1478359
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1478360
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1478361
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1478362
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1478363
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1478364
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1478365
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1478366
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1478367
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1478368
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;-><init>(LX/15i;)V

    .line 1478369
    move-object p0, v3

    .line 1478370
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1478330
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478331
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1478332
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1478333
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1478334
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1478335
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1478336
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1478337
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1478338
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1478339
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1478340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478341
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1478317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478318
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1478319
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    .line 1478320
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1478321
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    .line 1478322
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    .line 1478323
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1478324
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    .line 1478325
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1478326
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    .line 1478327
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    .line 1478328
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478329
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478378
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1478314
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;-><init>()V

    .line 1478315
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478316
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478313
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478311
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->g:Ljava/lang/String;

    .line 1478312
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478309
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->h:Ljava/lang/String;

    .line 1478310
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1478303
    const v0, 0x6b12b228

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1478308
    const v0, -0x4b900828

    return v0
.end method

.method public final j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478306
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    .line 1478307
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478304
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    .line 1478305
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    return-object v0
.end method
