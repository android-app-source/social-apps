.class public final Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1491634
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1491635
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1491633
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1491619
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1491620
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1491621
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1491622
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1491623
    if-eqz v2, :cond_0

    .line 1491624
    const-string p0, "group_trending_stories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491625
    invoke-static {v1, v2, p1, p2}, LX/9SR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1491626
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1491627
    if-eqz v2, :cond_1

    .line 1491628
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491629
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1491630
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1491631
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1491632
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchCommunityTrendingStoriesPostIdsModel;LX/0nX;LX/0my;)V

    return-void
.end method
