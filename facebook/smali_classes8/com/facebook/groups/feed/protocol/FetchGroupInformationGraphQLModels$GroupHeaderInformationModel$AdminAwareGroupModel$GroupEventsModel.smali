.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1079d2ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1481563
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1481564
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1481565
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1481566
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1481567
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1481568
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1481569
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;
    .locals 10

    .prologue
    .line 1481570
    if-nez p0, :cond_0

    .line 1481571
    const/4 p0, 0x0

    .line 1481572
    :goto_0
    return-object p0

    .line 1481573
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    if-eqz v0, :cond_1

    .line 1481574
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    goto :goto_0

    .line 1481575
    :cond_1
    new-instance v2, LX/9Oc;

    invoke-direct {v2}, LX/9Oc;-><init>()V

    .line 1481576
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1481577
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1481578
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1481579
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1481580
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9Oc;->a:LX/0Px;

    .line 1481581
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1481582
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1481583
    iget-object v5, v2, LX/9Oc;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1481584
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1481585
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1481586
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1481587
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1481588
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1481589
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1481590
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1481591
    new-instance v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;-><init>(LX/15i;)V

    .line 1481592
    move-object p0, v5

    .line 1481593
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1481594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1481595
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1481596
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1481597
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1481598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1481599
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1481600
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->e:Ljava/util/List;

    .line 1481601
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1481602
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1481603
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1481604
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1481605
    if-eqz v1, :cond_0

    .line 1481606
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    .line 1481607
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->e:Ljava/util/List;

    .line 1481608
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1481609
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1481610
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;-><init>()V

    .line 1481611
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1481612
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1481613
    const v0, 0x1b8fd46f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1481614
    const v0, 0x6a6e1d56

    return v0
.end method
