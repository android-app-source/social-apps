.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x59628c4a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1483001
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1483000
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1482942
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1482943
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1482997
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1482998
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1482999
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
    .locals 10

    .prologue
    .line 1482971
    if-nez p0, :cond_0

    .line 1482972
    const/4 p0, 0x0

    .line 1482973
    :goto_0
    return-object p0

    .line 1482974
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    if-eqz v0, :cond_1

    .line 1482975
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    goto :goto_0

    .line 1482976
    :cond_1
    new-instance v2, LX/9Os;

    invoke-direct {v2}, LX/9Os;-><init>()V

    .line 1482977
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->a()I

    move-result v0

    iput v0, v2, LX/9Os;->a:I

    .line 1482978
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1482979
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1482980
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1482981
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1482982
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9Os;->b:LX/0Px;

    .line 1482983
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 1482984
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1482985
    iget-object v5, v2, LX/9Os;->b:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1482986
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, LX/186;->c(I)V

    .line 1482987
    iget v7, v2, LX/9Os;->a:I

    invoke-virtual {v4, v9, v7, v9}, LX/186;->a(III)V

    .line 1482988
    invoke-virtual {v4, v8, v5}, LX/186;->b(II)V

    .line 1482989
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1482990
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1482991
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1482992
    invoke-virtual {v5, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1482993
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1482994
    new-instance v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;-><init>(LX/15i;)V

    .line 1482995
    move-object p0, v5

    .line 1482996
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1482969
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1482970
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1482962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1482963
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1482964
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1482965
    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1482966
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1482967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1482968
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1482954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1482955
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1482956
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1482957
    if-eqz v1, :cond_0

    .line 1482958
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    .line 1482959
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->f:Ljava/util/List;

    .line 1482960
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1482961
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1482951
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1482952
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->e:I

    .line 1482953
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1482949
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->f:Ljava/util/List;

    .line 1482950
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1482946
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;-><init>()V

    .line 1482947
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1482948
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1482945
    const v0, -0x5757cbe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1482944
    const v0, -0xce5e2c6

    return v0
.end method
