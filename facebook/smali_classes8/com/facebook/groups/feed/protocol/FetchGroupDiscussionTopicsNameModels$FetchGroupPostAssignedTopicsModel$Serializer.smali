.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1473176
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1473177
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1473196
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1473179
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1473180
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1473181
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1473182
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1473183
    if-eqz v2, :cond_0

    .line 1473184
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473185
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1473186
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1473187
    if-eqz v2, :cond_2

    .line 1473188
    const-string p0, "action_links"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473189
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1473190
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1473191
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/9MK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1473192
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1473193
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1473194
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1473195
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1473178
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;LX/0nX;LX/0my;)V

    return-void
.end method
