.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1477093
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1477094
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1477095
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1477096
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1477097
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/9Pk;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1477098
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1477099
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
