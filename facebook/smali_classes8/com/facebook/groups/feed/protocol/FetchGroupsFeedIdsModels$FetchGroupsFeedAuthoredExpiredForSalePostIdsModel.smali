.class public final Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5623454f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1492017
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1492016
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1492014
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1492015
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1492012
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->f:Ljava/lang/String;

    .line 1492013
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1492004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1492005
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1492006
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1492007
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1492008
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1492009
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1492010
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1492011
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1491996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1491997
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1491998
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    .line 1491999
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1492000
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;

    .line 1492001
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    .line 1492002
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1492003
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1491984
    new-instance v0, LX/9S3;

    invoke-direct {v0, p1}, LX/9S3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1491995
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1491993
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1491994
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1491992
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1491989
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;-><init>()V

    .line 1491990
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1491991
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1491988
    const v0, -0x382b5708

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1491987
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1491985
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    .line 1491986
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedAuthoredExpiredForSalePostIdsModel$GroupOwnerAuthoredStoriesModel;

    return-object v0
.end method
