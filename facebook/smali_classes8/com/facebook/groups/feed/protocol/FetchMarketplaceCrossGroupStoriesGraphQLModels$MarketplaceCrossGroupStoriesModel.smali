.class public final Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xbe6e0dc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495369
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1495368
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1495366
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1495367
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1495364
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    .line 1495365
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1495345
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495346
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1495347
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1495348
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1495349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495350
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1495356
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1495357
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1495358
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    .line 1495359
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1495360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;

    .line 1495361
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;->e:Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel$MarketplaceCrossGroupStoriesMarketplaceCrossGroupStoriesModel;

    .line 1495362
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1495363
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1495353
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$MarketplaceCrossGroupStoriesModel;-><init>()V

    .line 1495354
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1495355
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1495352
    const v0, 0x7147d39f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1495351
    const v0, -0x6747e1ce

    return v0
.end method
