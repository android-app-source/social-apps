.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2c1abdb5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1484083
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1484084
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1484100
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1484101
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1484085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1484086
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1484087
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1484088
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1484089
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1484090
    iget-boolean v3, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->e:Z

    invoke-virtual {p1, v4, v3}, LX/186;->a(IZ)V

    .line 1484091
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1484092
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1484093
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1484094
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->i:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 1484095
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1484096
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1484097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1484098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1484099
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1484073
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1484076
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1484077
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->e:Z

    .line 1484078
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->i:I

    .line 1484079
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1484080
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;-><init>()V

    .line 1484081
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1484082
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1484075
    const v0, -0x6780822b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1484074
    const v0, -0x330519e3

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1484071
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1484072
    iget-boolean v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->e:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1484069
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1484070
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1484067
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1484068
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1484065
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 1484066
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 1484063
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1484064
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->i:I

    return v0
.end method
