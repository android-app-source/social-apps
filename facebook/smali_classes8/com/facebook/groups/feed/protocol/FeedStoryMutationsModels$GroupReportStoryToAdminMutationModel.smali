.class public final Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471951
    const-class v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1471952
    const-class v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1471953
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1471954
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1471955
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;->e:Ljava/lang/String;

    .line 1471956
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1471957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1471958
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1471959
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1471960
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1471961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1471962
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1471963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1471964
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1471965
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1471966
    new-instance v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$GroupReportStoryToAdminMutationModel;-><init>()V

    .line 1471967
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1471968
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1471969
    const v0, -0x218522e0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1471970
    const v0, 0x194d90bb

    return v0
.end method
