.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x87fc8cf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:D

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1474872
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1474904
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1474902
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1474903
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1474899
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1474900
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1474901
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;
    .locals 13

    .prologue
    .line 1474873
    if-nez p0, :cond_0

    .line 1474874
    const/4 p0, 0x0

    .line 1474875
    :goto_0
    return-object p0

    .line 1474876
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;

    if-eqz v0, :cond_1

    .line 1474877
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;

    goto :goto_0

    .line 1474878
    :cond_1
    new-instance v0, LX/9NF;

    invoke-direct {v0}, LX/9NF;-><init>()V

    .line 1474879
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->a()I

    move-result v1

    iput v1, v0, LX/9NF;->a:I

    .line 1474880
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/9NF;->b:D

    .line 1474881
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9NF;->c:Ljava/lang/String;

    .line 1474882
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->d()I

    move-result v1

    iput v1, v0, LX/9NF;->d:I

    .line 1474883
    const/4 v5, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 1474884
    new-instance v4, LX/186;

    const/16 v6, 0x80

    invoke-direct {v4, v6}, LX/186;-><init>(I)V

    .line 1474885
    iget-object v6, v0, LX/9NF;->c:Ljava/lang/String;

    invoke-virtual {v4, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1474886
    const/4 v6, 0x4

    invoke-virtual {v4, v6}, LX/186;->c(I)V

    .line 1474887
    iget v6, v0, LX/9NF;->a:I

    invoke-virtual {v4, v11, v6, v11}, LX/186;->a(III)V

    .line 1474888
    iget-wide v6, v0, LX/9NF;->b:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1474889
    const/4 v6, 0x2

    invoke-virtual {v4, v6, v10}, LX/186;->b(II)V

    .line 1474890
    const/4 v6, 0x3

    iget v7, v0, LX/9NF;->d:I

    invoke-virtual {v4, v6, v7, v11}, LX/186;->a(III)V

    .line 1474891
    invoke-virtual {v4}, LX/186;->d()I

    move-result v6

    .line 1474892
    invoke-virtual {v4, v6}, LX/186;->d(I)V

    .line 1474893
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1474894
    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1474895
    new-instance v6, LX/15i;

    move-object v8, v12

    move-object v9, v12

    move v10, v5

    move-object v11, v12

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1474896
    new-instance v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;

    invoke-direct {v4, v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;-><init>(LX/15i;)V

    .line 1474897
    move-object p0, v4

    .line 1474898
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1474870
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1474871
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1474861
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1474862
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1474863
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1474864
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->e:I

    invoke-virtual {p1, v7, v0, v7}, LX/186;->a(III)V

    .line 1474865
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1474866
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1474867
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->h:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1474868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1474869
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1474905
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1474906
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1474907
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1474856
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1474857
    invoke-virtual {p1, p2, v4, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->e:I

    .line 1474858
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->f:D

    .line 1474859
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->h:I

    .line 1474860
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1474854
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1474855
    iget-wide v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1474845
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;-><init>()V

    .line 1474846
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1474847
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1474852
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->g:Ljava/lang/String;

    .line 1474853
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1474850
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1474851
    iget v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->h:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1474849
    const v0, 0x72bd0558

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1474848
    const v0, 0x437b93b

    return v0
.end method
