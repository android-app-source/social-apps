.class public final Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1490843
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1490844
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1490845
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1490846
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1490847
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1490848
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1490849
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1490850
    if-eqz v2, :cond_0

    .line 1490851
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1490852
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1490853
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1490854
    if-eqz v2, :cond_1

    .line 1490855
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1490856
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1490857
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1490858
    if-eqz v2, :cond_2

    .line 1490859
    const-string p0, "parent_group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1490860
    invoke-static {v1, v2, p1, p2}, LX/9RX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1490861
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1490862
    if-eqz v2, :cond_3

    .line 1490863
    const-string p0, "related_groups"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1490864
    invoke-static {v1, v2, p1, p2}, LX/9RZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1490865
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1490866
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1490867
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;LX/0nX;LX/0my;)V

    return-void
.end method
