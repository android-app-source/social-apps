.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5a4cdf79
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1475767
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1475766
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1475764
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1475765
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1475761
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1475762
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1475763
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;
    .locals 13

    .prologue
    .line 1475728
    if-nez p0, :cond_0

    .line 1475729
    const/4 p0, 0x0

    .line 1475730
    :goto_0
    return-object p0

    .line 1475731
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    if-eqz v0, :cond_1

    .line 1475732
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    goto :goto_0

    .line 1475733
    :cond_1
    new-instance v2, LX/9NM;

    invoke-direct {v2}, LX/9NM;-><init>()V

    .line 1475734
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    iput-object v0, v2, LX/9NM;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    .line 1475735
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v0

    iput-object v0, v2, LX/9NM;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    .line 1475736
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1475737
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1475738
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1475739
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1475740
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9NM;->c:LX/0Px;

    .line 1475741
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->d()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v0

    iput-object v0, v2, LX/9NM;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    .line 1475742
    const/4 v8, 0x1

    const/4 v12, 0x0

    const/4 v6, 0x0

    .line 1475743
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1475744
    iget-object v5, v2, LX/9NM;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1475745
    iget-object v7, v2, LX/9NM;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1475746
    iget-object v9, v2, LX/9NM;->c:LX/0Px;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 1475747
    iget-object v10, v2, LX/9NM;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    invoke-static {v4, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1475748
    const/4 v11, 0x4

    invoke-virtual {v4, v11}, LX/186;->c(I)V

    .line 1475749
    invoke-virtual {v4, v12, v5}, LX/186;->b(II)V

    .line 1475750
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1475751
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1475752
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1475753
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1475754
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1475755
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1475756
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1475757
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1475758
    new-instance v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;-><init>(LX/15i;)V

    .line 1475759
    move-object p0, v5

    .line 1475760
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1475716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1475717
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1475718
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1475719
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1475720
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1475721
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1475722
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1475723
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1475724
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1475725
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1475726
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1475727
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1475680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1475681
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1475682
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    .line 1475683
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1475684
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    .line 1475685
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    .line 1475686
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1475687
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    .line 1475688
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1475689
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    .line 1475690
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    .line 1475691
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1475692
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1475693
    if-eqz v2, :cond_2

    .line 1475694
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    .line 1475695
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 1475696
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1475697
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    .line 1475698
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1475699
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    .line 1475700
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    .line 1475701
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1475702
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475715
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1475768
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;-><init>()V

    .line 1475769
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1475770
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475714
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1475712
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->g:Ljava/util/List;

    .line 1475713
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475711
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1475710
    const v0, 0x1575c709

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1475709
    const v0, 0x20ed736c

    return v0
.end method

.method public final j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475707
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    .line 1475708
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475705
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    .line 1475706
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1475703
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    .line 1475704
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    return-object v0
.end method
