.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x702256b1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478515
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478560
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1478558
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478559
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1478555
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478556
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478557
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;
    .locals 10

    .prologue
    .line 1478531
    if-nez p0, :cond_0

    .line 1478532
    const/4 p0, 0x0

    .line 1478533
    :goto_0
    return-object p0

    .line 1478534
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    if-eqz v0, :cond_1

    .line 1478535
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    goto :goto_0

    .line 1478536
    :cond_1
    new-instance v2, LX/9O1;

    invoke-direct {v2}, LX/9O1;-><init>()V

    .line 1478537
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1478538
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1478539
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;

    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1478540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1478541
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9O1;->a:LX/0Px;

    .line 1478542
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1478543
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1478544
    iget-object v5, v2, LX/9O1;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1478545
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1478546
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1478547
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1478548
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1478549
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1478550
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1478551
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1478552
    new-instance v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;-><init>(LX/15i;)V

    .line 1478553
    move-object p0, v5

    .line 1478554
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1478561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478562
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1478563
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1478564
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1478565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1478529
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->e:Ljava/util/List;

    .line 1478530
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1478521
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478522
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1478523
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1478524
    if-eqz v1, :cond_0

    .line 1478525
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    .line 1478526
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->e:Ljava/util/List;

    .line 1478527
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478528
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1478518
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;-><init>()V

    .line 1478519
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478520
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1478517
    const v0, 0x358133c8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1478516
    const v0, -0x20382388

    return v0
.end method
