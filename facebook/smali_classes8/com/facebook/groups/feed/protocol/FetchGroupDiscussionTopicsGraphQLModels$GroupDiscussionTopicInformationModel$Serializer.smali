.class public final Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1472787
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1472788
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1472750
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1472752
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1472753
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1472754
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1472755
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1472756
    if-eqz v2, :cond_0

    .line 1472757
    const-string v3, "can_viewer_change_post_topics"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472758
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1472759
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1472760
    if-eqz v2, :cond_6

    .line 1472761
    const-string v3, "group_post_topics"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472762
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1472763
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1472764
    if-eqz v3, :cond_2

    .line 1472765
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472766
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1472767
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1472768
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/9MB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1472769
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1472770
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1472771
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1472772
    if-eqz v3, :cond_5

    .line 1472773
    const-string p0, "page_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472774
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1472775
    const/4 p0, 0x0

    invoke-virtual {v1, v3, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1472776
    if-eqz p0, :cond_3

    .line 1472777
    const-string v0, "end_cursor"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472778
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1472779
    :cond_3
    const/4 p0, 0x1

    invoke-virtual {v1, v3, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1472780
    if-eqz p0, :cond_4

    .line 1472781
    const-string v0, "has_next_page"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472782
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1472783
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1472784
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1472785
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1472786
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1472751
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;LX/0nX;LX/0my;)V

    return-void
.end method
