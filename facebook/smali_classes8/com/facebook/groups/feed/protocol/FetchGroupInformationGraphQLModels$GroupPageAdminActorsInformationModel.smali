.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/9Mu;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x139667f4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1482823
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1482822
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1482820
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1482821
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1482795
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1482796
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1482814
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1482815
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1482816
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1482817
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1482818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1482819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1482806
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1482807
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1482808
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1482809
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1482810
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;

    .line 1482811
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1482812
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1482813
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1482805
    new-instance v0, LX/9Or;

    invoke-direct {v0, p1}, LX/9Or;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1482803
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1482804
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1482802
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1482799
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel;-><init>()V

    .line 1482800
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1482801
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1482798
    const v0, -0x16e69faf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1482797
    const v0, 0x41e065f

    return v0
.end method
