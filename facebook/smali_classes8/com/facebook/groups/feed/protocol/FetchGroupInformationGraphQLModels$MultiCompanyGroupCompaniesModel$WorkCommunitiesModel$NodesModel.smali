.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x104a4aa6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1484284
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1484316
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1484314
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1484315
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1484311
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1484312
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1484313
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;
    .locals 8

    .prologue
    .line 1484291
    if-nez p0, :cond_0

    .line 1484292
    const/4 p0, 0x0

    .line 1484293
    :goto_0
    return-object p0

    .line 1484294
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;

    if-eqz v0, :cond_1

    .line 1484295
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;

    goto :goto_0

    .line 1484296
    :cond_1
    new-instance v0, LX/9PD;

    invoke-direct {v0}, LX/9PD;-><init>()V

    .line 1484297
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9PD;->a:Ljava/lang/String;

    .line 1484298
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1484299
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1484300
    iget-object v3, v0, LX/9PD;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1484301
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1484302
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1484303
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1484304
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1484305
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1484306
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1484307
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1484308
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;-><init>(LX/15i;)V

    .line 1484309
    move-object p0, v3

    .line 1484310
    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1484285
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->e:Ljava/lang/String;

    .line 1484286
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1484287
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1484288
    if-eqz v0, :cond_0

    .line 1484289
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1484290
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1484278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1484279
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1484280
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1484281
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1484282
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1484283
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1484317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1484318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1484319
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1484277
    new-instance v0, LX/9PE;

    invoke-direct {v0, p1}, LX/9PE;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1484261
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->e:Ljava/lang/String;

    .line 1484262
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1484271
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1484272
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1484273
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1484274
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1484275
    :goto_0
    return-void

    .line 1484276
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1484268
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1484269
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;->a(Ljava/lang/String;)V

    .line 1484270
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1484265
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel$NodesModel;-><init>()V

    .line 1484266
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1484267
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1484264
    const v0, 0x5ed12ad9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1484263
    const v0, 0x41e065f

    return v0
.end method
