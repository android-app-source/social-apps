.class public final Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ee21312
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1489043
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1489044
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1489051
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1489052
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1489045
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1489046
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1489047
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1489048
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1489049
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1489050
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1489034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1489035
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1489036
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    .line 1489037
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1489038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;

    .line 1489039
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    .line 1489040
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1489041
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1489042
    new-instance v0, LX/9Qz;

    invoke-direct {v0, p1}, LX/9Qz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroups"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1489024
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    .line 1489025
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1489026
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1489027
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1489028
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1489029
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;-><init>()V

    .line 1489030
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1489031
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1489033
    const v0, -0x123b8dbd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1489032
    const v0, 0x285feb

    return v0
.end method
