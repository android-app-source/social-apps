.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd87b8a7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1476184
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1476221
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1476219
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1476220
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1476216
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1476217
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1476218
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;
    .locals 9

    .prologue
    .line 1476193
    if-nez p0, :cond_0

    .line 1476194
    const/4 p0, 0x0

    .line 1476195
    :goto_0
    return-object p0

    .line 1476196
    :cond_0
    instance-of v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;

    if-eqz v0, :cond_1

    .line 1476197
    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;

    goto :goto_0

    .line 1476198
    :cond_1
    new-instance v0, LX/9Na;

    invoke-direct {v0}, LX/9Na;-><init>()V

    .line 1476199
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9Na;->a:Ljava/lang/String;

    .line 1476200
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9Na;->b:Ljava/lang/String;

    .line 1476201
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1476202
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1476203
    iget-object v3, v0, LX/9Na;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1476204
    iget-object v5, v0, LX/9Na;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1476205
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1476206
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1476207
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1476208
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1476209
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1476210
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1476211
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1476212
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1476213
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;-><init>(LX/15i;)V

    .line 1476214
    move-object p0, v3

    .line 1476215
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1476185
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1476186
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1476187
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1476188
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1476189
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1476190
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1476191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1476192
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1476168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1476169
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1476170
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1476183
    new-instance v0, LX/9Nb;

    invoke-direct {v0, p1}, LX/9Nb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1476222
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1476181
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1476182
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1476180
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1476177
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;-><init>()V

    .line 1476178
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1476179
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1476175
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->e:Ljava/lang/String;

    .line 1476176
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1476173
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->f:Ljava/lang/String;

    .line 1476174
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1476172
    const v0, -0x4b9b788b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1476171
    const v0, 0x4c808d5

    return v0
.end method
