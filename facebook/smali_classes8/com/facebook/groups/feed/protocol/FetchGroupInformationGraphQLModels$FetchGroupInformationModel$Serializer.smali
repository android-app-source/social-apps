.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1480493
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1480494
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1480366
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1480367
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1480368
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x1d

    const/16 v7, 0x1c

    const/16 v6, 0x19

    const/16 v5, 0x13

    const/4 v4, 0x0

    .line 1480369
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1480370
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1480371
    if-eqz v2, :cond_0

    .line 1480372
    const-string v3, "admin_aware_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480373
    invoke-static {v1, v2, p1, p2}, LX/9QY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480374
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480375
    if-eqz v2, :cond_1

    .line 1480376
    const-string v3, "can_viewer_change_cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480377
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480378
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480379
    if-eqz v2, :cond_2

    .line 1480380
    const-string v3, "can_viewer_pin_post"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480381
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480382
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480383
    if-eqz v2, :cond_3

    .line 1480384
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480385
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480386
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480387
    if-eqz v2, :cond_4

    .line 1480388
    const-string v3, "communityFields"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480389
    invoke-static {v1, v2, p1, p2}, LX/9Q5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480390
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480391
    if-eqz v2, :cond_5

    .line 1480392
    const-string v3, "group_admined_page_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480393
    invoke-static {v1, v2, p1, p2}, LX/9Qb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480394
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480395
    if-eqz v2, :cond_6

    .line 1480396
    const-string v3, "group_configs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480397
    invoke-static {v1, v2, p1, p2}, LX/5A9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480398
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480399
    if-eqz v2, :cond_7

    .line 1480400
    const-string v3, "group_member_profiles"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480401
    invoke-static {v1, v2, p1, p2}, LX/9Qm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480402
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480403
    if-eqz v2, :cond_8

    .line 1480404
    const-string v3, "group_pinned_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480405
    invoke-static {v1, v2, p1, p2}, LX/9Qd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480406
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480407
    if-eqz v2, :cond_9

    .line 1480408
    const-string v3, "group_post_topics"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480409
    invoke-static {v1, v2, p1}, LX/9QG;->a(LX/15i;ILX/0nX;)V

    .line 1480410
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480411
    if-eqz v2, :cond_a

    .line 1480412
    const-string v3, "group_purposes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480413
    invoke-static {v1, v2, p1, p2}, LX/9Tf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480414
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480415
    if-eqz v2, :cond_b

    .line 1480416
    const-string v3, "group_sell_config"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480417
    invoke-static {v1, v2, p1, p2}, LX/9LH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480418
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1480419
    if-eqz v2, :cond_c

    .line 1480420
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480421
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1480422
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480423
    if-eqz v2, :cond_d

    .line 1480424
    const-string v3, "is_viewer_unconfirmed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480425
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480426
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480427
    if-eqz v2, :cond_e

    .line 1480428
    const-string v3, "learning_course_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480429
    invoke-static {v1, v2, p1, p2}, LX/9Qp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480430
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480431
    if-eqz v2, :cond_f

    .line 1480432
    const-string v3, "prefill_group_welcome_prompt_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480433
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480434
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480435
    if-eqz v2, :cond_10

    .line 1480436
    const-string v3, "should_show_notif_settings_transition_nux"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480437
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480438
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480439
    if-eqz v2, :cond_11

    .line 1480440
    const-string v3, "should_show_rooms_secondary_nux"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480441
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480442
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480443
    if-eqz v2, :cond_12

    .line 1480444
    const-string v3, "subgroupsFields"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480445
    invoke-static {v1, v2, p1, p2}, LX/9QC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480446
    :cond_12
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1480447
    if-eqz v2, :cond_13

    .line 1480448
    const-string v2, "subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480449
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1480450
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480451
    if-eqz v2, :cond_14

    .line 1480452
    const-string v3, "suggested_purpose"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480453
    invoke-static {v1, v2, p1}, LX/9Tg;->a(LX/15i;ILX/0nX;)V

    .line 1480454
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480455
    if-eqz v2, :cond_15

    .line 1480456
    const-string v3, "tips_channel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480457
    invoke-static {v1, v2, p1, p2}, LX/9Qg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480458
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1480459
    if-eqz v2, :cond_16

    .line 1480460
    const-string v3, "unread_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480461
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1480462
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1480463
    if-eqz v2, :cond_17

    .line 1480464
    const-string v3, "user_might_be_selling"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480465
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1480466
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480467
    if-eqz v2, :cond_18

    .line 1480468
    const-string v3, "viewer_added_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480469
    invoke-static {v1, v2, p1}, LX/9Qh;->a(LX/15i;ILX/0nX;)V

    .line 1480470
    :cond_18
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1480471
    if-eqz v2, :cond_19

    .line 1480472
    const-string v2, "viewer_admin_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480473
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1480474
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1480475
    if-eqz v2, :cond_1a

    .line 1480476
    const-string v3, "viewer_invite_message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480477
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1480478
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1480479
    if-eqz v2, :cond_1b

    .line 1480480
    const-string v3, "viewer_invite_to_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480481
    invoke-static {v1, v2, p1, p2}, LX/9Qj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1480482
    :cond_1b
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1480483
    if-eqz v2, :cond_1c

    .line 1480484
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480485
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1480486
    :cond_1c
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1480487
    if-eqz v2, :cond_1d

    .line 1480488
    const-string v2, "viewer_post_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1480489
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1480490
    :cond_1d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1480491
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1480492
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/0nX;LX/0my;)V

    return-void
.end method
