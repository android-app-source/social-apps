.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1476601
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1476602
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1476604
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1476605
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1476606
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1476607
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1476608
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1476609
    if-eqz v2, :cond_0

    .line 1476610
    const-string p0, "community_forsale_stories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1476611
    invoke-static {v1, v2, p1, p2}, LX/9Pd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1476612
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1476613
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1476603
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
