.class public final Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/9Mk;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4c0f710d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478041
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1478040
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1478038
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1478039
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478036
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->e:Ljava/lang/String;

    .line 1478037
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestedMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478034
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    .line 1478035
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1478026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478027
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1478028
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1478029
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1478030
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1478031
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1478032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478033
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1478008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1478009
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1478010
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    .line 1478011
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1478012
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;

    .line 1478013
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    .line 1478014
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1478015
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1478025
    new-instance v0, LX/9Nq;

    invoke-direct {v0, p1}, LX/9Nq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1478024
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1478022
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1478023
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1478021
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1478018
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel;-><init>()V

    .line 1478019
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1478020
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1478017
    const v0, 0x40ef5f85

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1478016
    const v0, 0x41e065f

    return v0
.end method
