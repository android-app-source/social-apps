.class public final Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x26d6039f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1488859
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1488858
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1488856
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1488857
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1488850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1488851
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1488852
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1488853
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1488854
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1488855
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1488835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1488836
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1488837
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    .line 1488838
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1488839
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;

    .line 1488840
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    .line 1488841
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1488842
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1488848
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    .line 1488849
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->e:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1488845
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;-><init>()V

    .line 1488846
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1488847
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1488844
    const v0, 0x153fef4b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1488843
    const v0, 0x1da3a91b

    return v0
.end method
