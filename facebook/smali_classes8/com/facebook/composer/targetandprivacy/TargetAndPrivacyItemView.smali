.class public final Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;
.super Landroid/widget/RelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Landroid/view/View;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1673075
    const-class v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1673073
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1673074
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1673035
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1673036
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1673050
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1673051
    const v0, 0x7f030fcc

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1673052
    const v0, 0x7f0d0503

    invoke-virtual {p0, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1673053
    const v0, 0x7f0d0281

    invoke-virtual {p0, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1673054
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1673055
    const v0, 0x7f0d261f

    invoke-virtual {p0, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->c:Landroid/view/View;

    .line 1673056
    if-eqz p2, :cond_2

    .line 1673057
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->PlatformComposerTargetPrivacyItemView:[I

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1673058
    :try_start_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1673059
    const/16 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1673060
    const/16 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 1673061
    const/16 v4, 0x3

    invoke-static {p1, v1, v4}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1673062
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1673063
    if-eqz v0, :cond_0

    .line 1673064
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1673065
    :cond_0
    if-eqz v2, :cond_1

    .line 1673066
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 1673067
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1673068
    :cond_1
    :goto_0
    invoke-virtual {p0, v3}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setIsSelected(Z)V

    .line 1673069
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673070
    :cond_2
    return-void

    .line 1673071
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 1673072
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final getTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1673049
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setIsSelected(Z)V
    .locals 2

    .prologue
    .line 1673046
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1673047
    return-void

    .line 1673048
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final setProfilePictureUrl(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1673039
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1673040
    if-nez p1, :cond_0

    .line 1673041
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1673042
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1673043
    :goto_0
    return-void

    .line 1673044
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1673045
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1673037
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673038
    return-void
.end method
