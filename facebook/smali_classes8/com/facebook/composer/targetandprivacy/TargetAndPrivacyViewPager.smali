.class public Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;
.super Landroid/support/v4/view/ViewPager;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1673302
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 1673303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    .line 1673304
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1673299
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1673300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    .line 1673301
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1673296
    iget-boolean v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    if-nez v0, :cond_0

    .line 1673297
    const/4 v0, 0x0

    .line 1673298
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1aea76e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1673291
    iget-boolean v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    if-nez v0, :cond_0

    .line 1673292
    const/4 v0, 0x0

    const v2, -0x321bcd0a

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1673293
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x7c60217d

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setPagingEnabled(Z)V
    .locals 0

    .prologue
    .line 1673294
    iput-boolean p1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    .line 1673295
    return-void
.end method
