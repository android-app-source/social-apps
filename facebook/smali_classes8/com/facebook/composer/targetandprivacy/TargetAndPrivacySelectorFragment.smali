.class public Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public A:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public B:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/93W;

.field public D:I

.field public E:LX/0gG;

.field private F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

.field public G:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/9UI;

.field public I:LX/9U7;

.field public J:LX/ARk;

.field private K:Z

.field public L:Lcom/facebook/resources/ui/FbTextView;

.field public M:Z

.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field public s:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field public t:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field public u:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field private v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

.field public w:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

.field public x:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

.field public y:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

.field public z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1673221
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1673222
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;

    const/16 v1, 0x12cb

    invoke-static {v3, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-static {v3}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static {v3}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    iput-object p0, p1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->m:LX/0Or;

    iput-object v1, p1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->n:LX/0if;

    iput-object v2, p1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->o:Ljava/lang/Boolean;

    iput-object v3, p1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->p:Landroid/view/inputmethod/InputMethodManager;

    return-void
.end method

.method public static b(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;I)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1673268
    new-instance v0, LX/ARh;

    invoke-direct {v0, p0, p1}, LX/ARh;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;I)V

    return-object v0
.end method

.method private b()V
    .locals 12

    .prologue
    .line 1673230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->q:Ljava/util/ArrayList;

    .line 1673231
    new-instance v0, LX/ARm;

    invoke-direct {v0, p0}, LX/ARm;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    move-object v1, v0

    .line 1673232
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    .line 1673233
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1673234
    iget-object v2, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v2, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setProfilePictureUrl(Landroid/net/Uri;)V

    .line 1673235
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673236
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673237
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->t:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673238
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->u:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673239
    iget-object v3, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a()Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673240
    iget-object v6, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->w:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    if-nez v6, :cond_0

    .line 1673241
    new-instance v6, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-direct {v6}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;-><init>()V

    iput-object v6, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->w:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    .line 1673242
    iget-object v6, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->w:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    new-instance v7, LX/ARi;

    invoke-direct {v7, p0}, LX/ARi;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 1673243
    iput-object v7, v6, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->l:LX/9U7;

    .line 1673244
    iget-object v7, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->w:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v6, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->o:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_3

    const v6, 0x7f081344

    :goto_0
    invoke-static {p0, v6}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->b(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;I)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 1673245
    iget-object v8, v7, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->o:LX/8SB;

    invoke-virtual {v8, v6}, LX/8SB;->a(Landroid/view/View$OnClickListener;)V

    .line 1673246
    :cond_0
    iget-object v6, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->w:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    move-object v6, v6

    .line 1673247
    iget-object v7, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->t:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673248
    iget-object v8, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->x:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    if-nez v8, :cond_1

    .line 1673249
    new-instance v8, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    invoke-direct {v8}, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;-><init>()V

    iput-object v8, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->x:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    .line 1673250
    iget-object v8, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->x:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    new-instance v9, LX/ARj;

    invoke-direct {v9, p0}, LX/ARj;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 1673251
    iput-object v9, v8, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->d:LX/9UI;

    .line 1673252
    iget-object v8, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->x:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    const v9, 0x7f081345

    invoke-static {p0, v9}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->b(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;I)Landroid/view/View$OnClickListener;

    move-result-object v9

    .line 1673253
    iget-object v10, v8, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->h:LX/8SB;

    invoke-virtual {v10, v9}, LX/8SB;->a(Landroid/view/View$OnClickListener;)V

    .line 1673254
    :cond_1
    iget-object v8, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->x:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    move-object v8, v8

    .line 1673255
    iget-object v9, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->u:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673256
    iget-object v10, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->y:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    if-nez v10, :cond_2

    .line 1673257
    new-instance v10, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-direct {v10}, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;-><init>()V

    iput-object v10, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->y:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    .line 1673258
    iget-object v10, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->y:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    new-instance v11, LX/ARl;

    invoke-direct {v11, p0}, LX/ARl;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 1673259
    iput-object v11, v10, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->j:LX/ARk;

    .line 1673260
    :cond_2
    iget-object v10, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->y:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    move-object v10, v10

    .line 1673261
    invoke-static/range {v3 .. v10}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->A:LX/0P1;

    .line 1673262
    iget-object v3, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v4, LX/2rw;->UNDIRECTED:LX/2rw;

    iget-object v5, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v6, LX/2rw;->USER:LX/2rw;

    iget-object v7, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->t:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v8, LX/2rw;->GROUP:LX/2rw;

    iget-object v9, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->u:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v10, LX/2rw;->PAGE:LX/2rw;

    invoke-static/range {v3 .. v10}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->B:LX/0P1;

    .line 1673263
    new-instance v0, LX/ARf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/ARf;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->E:LX/0gG;

    .line 1673264
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->E:LX/0gG;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1673265
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    new-instance v1, LX/ARg;

    invoke-direct {v1, p0}, LX/ARg;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1673266
    return-void

    .line 1673267
    :cond_3
    const v6, 0x7f081343

    goto/16 :goto_0
.end method

.method public static c(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1673223
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1673224
    const v2, 0x7f0d2e89

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1673225
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    if-nez p1, :cond_0

    const/4 v1, 0x1

    .line 1673226
    :cond_0
    iput-boolean v1, v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    .line 1673227
    iput-boolean p1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->M:Z

    .line 1673228
    return-void

    :cond_1
    move v0, v1

    .line 1673229
    goto :goto_0
.end method

.method public static r(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;
    .locals 2

    .prologue
    .line 1673135
    iget v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->A:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1673136
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    .line 1673137
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->A:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    iget v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    return-object v0
.end method

.method public static s(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1673216
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->A:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673217
    invoke-virtual {v0, v2}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setIsSelected(Z)V

    .line 1673218
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1673219
    :cond_0
    invoke-static {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setIsSelected(Z)V

    .line 1673220
    return-void
.end method

.method public static t(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V
    .locals 2

    .prologue
    .line 1673213
    iget v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1673214
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    iget v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1673215
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1673269
    iget-object v2, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-static {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 1673270
    invoke-virtual {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a()Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->S_()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 1673271
    :cond_0
    :goto_0
    return v0

    .line 1673272
    :cond_1
    iget-boolean v2, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->M:Z

    if-eqz v2, :cond_2

    .line 1673273
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f081342    # 1.80875E38f

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673274
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->p:Landroid/view/inputmethod/InputMethodManager;

    .line 1673275
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1673276
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1673277
    invoke-static {p0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->c(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;Z)V

    move v0, v1

    .line 1673278
    goto :goto_0

    .line 1673279
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a()Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1673280
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 1673281
    if-eqz v2, :cond_0

    .line 1673282
    iget-object v2, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->C:LX/93W;

    iget-object v2, v2, LX/93W;->b:LX/93X;

    invoke-interface {v2, v1}, LX/93X;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto :goto_0

    .line 1673283
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->M:Z

    if-eqz v2, :cond_0

    .line 1673284
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->A:LX/0P1;

    invoke-static {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1673285
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f081342    # 1.80875E38f

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673286
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->p:Landroid/view/inputmethod/InputMethodManager;

    .line 1673287
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1673288
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1673289
    invoke-static {p0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->c(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;Z)V

    move v0, v1

    .line 1673290
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1673212
    new-instance v0, LX/ARd;

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/ARd;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a()Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;
    .locals 2

    .prologue
    .line 1673205
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-nez v0, :cond_1

    .line 1673206
    new-instance v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-direct {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1673207
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->C:LX/93W;

    iget-object v1, v1, LX/93W;->a:LX/8Q5;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8Q5;)V

    .line 1673208
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->C:LX/93W;

    iget-object v0, v0, LX/93W;->c:LX/8Rm;

    if-eqz v0, :cond_0

    .line 1673209
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->C:LX/93W;

    iget-object v1, v1, LX/93W;->c:LX/8Rm;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8Rm;)V

    .line 1673210
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    const v1, 0x7f08131c

    invoke-static {p0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->b(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Landroid/view/View$OnClickListener;)V

    .line 1673211
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->v:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    return-object v0
.end method

.method public final a(LX/2rw;)V
    .locals 3

    .prologue
    .line 1673198
    sget-object v0, LX/ARc;->a:[I

    invoke-virtual {p1}, LX/2rw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1673199
    :goto_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected target type \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1673200
    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    .line 1673201
    :goto_1
    return-void

    .line 1673202
    :pswitch_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    goto :goto_1

    .line 1673203
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    goto :goto_1

    .line 1673204
    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->D:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1bb12333

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1673193
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1673194
    const-class v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;

    invoke-static {v1, p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1673195
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->z:Ljava/util/ArrayList;

    .line 1673196
    const v1, 0x7f0e0632

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1673197
    const/16 v1, 0x2b

    const v2, -0x2019f2d2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x4595468d

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1673160
    if-eqz p3, :cond_3

    .line 1673161
    const-string v1, "target and privacy selector fragment tags"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->q:Ljava/util/ArrayList;

    .line 1673162
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->q:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 1673163
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    .line 1673164
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1673165
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1673166
    if-eqz v0, :cond_0

    .line 1673167
    invoke-virtual {v3, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1673168
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1673169
    :cond_1
    invoke-virtual {v3}, LX/0hH;->b()I

    .line 1673170
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1673171
    :cond_2
    const/4 v0, 0x0

    const v1, -0x16ca295

    invoke-static {v1, v2}, LX/02F;->f(II)V

    .line 1673172
    :goto_1
    return-object v0

    .line 1673173
    :cond_3
    const v1, 0x7f031472

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1673174
    const-class v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;

    invoke-static {v1, p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1673175
    const v1, 0x7f0d1602

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->r:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673176
    const v1, 0x7f0d2622

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673177
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1673178
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    const v3, 0x7f08133f

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setTitleText(Ljava/lang/String;)V

    .line 1673179
    :cond_4
    const v1, 0x7f0d2623

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->t:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673180
    const v1, 0x7f0d1af3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->u:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673181
    const v1, 0x7f0d2e88

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1673182
    new-instance v3, LX/ARe;

    invoke-direct {v3, p0}, LX/ARe;-><init>(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    invoke-virtual {v1, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1673183
    const v3, 0x7f081342    # 1.80875E38f

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1673184
    const v3, 0x7f031473

    invoke-virtual {v1, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->d_(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    .line 1673185
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->L:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081342    # 1.80875E38f

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673186
    const v1, 0x7f0d15b3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    iput-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    .line 1673187
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    iget-boolean v3, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->K:Z

    invoke-virtual {v1, v3}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->setSaveEnabled(Z)V

    .line 1673188
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1673189
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    const/4 v3, 0x1

    .line 1673190
    iput-boolean v3, v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->a:Z

    .line 1673191
    invoke-direct {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->b()V

    .line 1673192
    const v1, -0x3634bd9

    invoke-static {v1, v2}, LX/02F;->f(II)V

    goto/16 :goto_1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4a1f062e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1673157
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1673158
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->F:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;

    iget-boolean v2, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->K:Z

    invoke-virtual {v1, v2}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyViewPager;->setSaveEnabled(Z)V

    .line 1673159
    const/16 v1, 0x2b

    const v2, -0x4388f062

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0xdacf0b1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1673141
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1673142
    iget-boolean v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->K:Z

    if-nez v1, :cond_2

    .line 1673143
    invoke-static {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->t(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 1673144
    invoke-static {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 1673145
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->z:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1673146
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->B:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1673147
    iget-object v5, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->G:LX/0Rf;

    iget-object v6, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->B:LX/0P1;

    invoke-virtual {v6, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1673148
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setVisibility(I)V

    .line 1673149
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1673150
    :cond_0
    iget-object v5, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->z:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1673151
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->E:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 1673152
    :goto_2
    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->p:Landroid/view/inputmethod/InputMethodManager;

    .line 1673153
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1673154
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1673155
    const v1, 0xadecd3f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1673156
    :cond_2
    invoke-static {p0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->s(Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    goto :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1673138
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1673139
    const-string v0, "target and privacy selector fragment tags"

    iget-object v1, p0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1673140
    return-void
.end method
