.class public Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1670915
    const-class v0, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1670914
    const-class v0, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1670916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static c()Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;
    .locals 1

    .prologue
    .line 1670913
    new-instance v0, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    invoke-direct {v0}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1670912
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1670911
    const-string v0, "ComposerHintExperimentPlugin"

    return-object v0
.end method
