.class public Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1670922
    const-class v0, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    new-instance v1, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1670923
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1670924
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1670925
    if-nez p0, :cond_0

    .line 1670926
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1670927
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1670928
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1670929
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1670930
    check-cast p1, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfigSerializer;->a(Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
