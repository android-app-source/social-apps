.class public Lcom/facebook/composer/metatext/TagsTextViewContainer;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;

.field public static final d:Ljava/lang/String;


# instance fields
.field public a:LX/8zA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/widget/TextView;

.field private f:LX/8zN;

.field public g:LX/8zD;

.field public h:Z

.field public i:LX/8z7;

.field public j:LX/8zF;

.field public k:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/8zL;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1427248
    const-class v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1427249
    const-class v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1427250
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1427251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    .line 1427252
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1427253
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1427254
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1427255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    .line 1427256
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1427257
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1427258
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1427259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    .line 1427260
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1427261
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1427262
    const-class v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-static {v0, p0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1427263
    new-instance v0, LX/8zC;

    invoke-direct {v0}, LX/8zC;-><init>()V

    invoke-virtual {v0}, LX/8zC;->a()LX/8zD;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    .line 1427264
    new-instance v0, LX/8zF;

    invoke-virtual {p0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8zF;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->j:LX/8zF;

    .line 1427265
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1427266
    new-instance v1, LX/1aX;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1aX;-><init>(LX/1aY;)V

    iput-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    .line 1427267
    new-instance v0, LX/63G;

    invoke-direct {v0, p1}, LX/63G;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    .line 1427268
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1427269
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinksClickable(Z)V

    .line 1427270
    if-eqz p2, :cond_0

    .line 1427271
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1427272
    :cond_0
    new-instance v0, LX/8zN;

    invoke-direct {v0, p0}, LX/8zN;-><init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;)V

    iput-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->f:LX/8zN;

    .line 1427273
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1427274
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->f:LX/8zN;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1427275
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;LX/8zD;)V
    .locals 3

    .prologue
    .line 1427276
    iget-object v0, p2, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_1

    .line 1427277
    :cond_0
    :goto_0
    return-void

    .line 1427278
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/metatext/TagsTextViewContainer;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    .line 1427279
    iget-object v2, v1, LX/1aX;->f:LX/1aZ;

    move-object v1, v2

    .line 1427280
    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p2, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    new-instance v1, LX/8zK;

    invoke-direct {v1, p0, p2, p1}, LX/8zK;-><init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1427281
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1427282
    invoke-virtual {p0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1427283
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zA;LX/1Ad;)V
    .locals 0

    .prologue
    .line 1427343
    iput-object p1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a:LX/8zA;

    iput-object p2, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->b:LX/1Ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-static {v1}, LX/8zA;->b(LX/0QB;)LX/8zA;

    move-result-object v0

    check-cast v0, LX/8zA;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zA;LX/1Ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;LX/8z9;I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1427284
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    invoke-virtual {p1, v1}, LX/8zD;->a(LX/8zD;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    iget-object v1, v1, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v1, :cond_1

    .line 1427285
    :cond_0
    :goto_0
    return-void

    .line 1427286
    :cond_1
    iput-object p1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    .line 1427287
    iput-boolean v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    .line 1427288
    iget-object v2, p1, LX/8zD;->a:Ljava/lang/CharSequence;

    .line 1427289
    iget-object v3, p1, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1427290
    iget-object v4, p1, LX/8zD;->c:LX/0Px;

    .line 1427291
    iget-object v5, p1, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1427292
    if-nez v2, :cond_5

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object v2, v1

    .line 1427293
    :goto_1
    if-nez v3, :cond_3

    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    if-eqz v5, :cond_9

    .line 1427294
    :cond_3
    new-instance v1, LX/8z6;

    invoke-direct {v1}, LX/8z6;-><init>()V

    .line 1427295
    iput-boolean v6, v1, LX/8z6;->f:Z

    .line 1427296
    move-object v1, v1

    .line 1427297
    iput-boolean v6, v1, LX/8z6;->g:Z

    .line 1427298
    move-object v1, v1

    .line 1427299
    iput-object p2, v1, LX/8z6;->h:LX/8z9;

    .line 1427300
    move-object v6, v1

    .line 1427301
    if-eqz v3, :cond_4

    .line 1427302
    iput-object v3, v6, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1427303
    :cond_4
    if-eqz v4, :cond_7

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1427304
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v8, :cond_a

    .line 1427305
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    move v1, v0

    .line 1427306
    :goto_2
    if-ge v1, v8, :cond_6

    .line 1427307
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1427308
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1427309
    :cond_5
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v2, v1

    goto :goto_1

    .line 1427310
    :cond_6
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/8z6;->a(LX/0Px;)LX/8z6;

    .line 1427311
    :goto_3
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    .line 1427312
    iput v0, v6, LX/8z6;->e:I

    .line 1427313
    :cond_7
    if-eqz v5, :cond_8

    .line 1427314
    iput-object v5, v6, LX/8z6;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1427315
    :cond_8
    invoke-virtual {v6}, LX/8z6;->a()LX/8z5;

    move-result-object v0

    .line 1427316
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->i:LX/8z7;

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a:LX/8zA;

    invoke-virtual {v1, v0}, LX/8zA;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1427317
    :goto_4
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1427318
    :cond_9
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->f:LX/8zN;

    invoke-virtual {v0, p1, p2, p3, v2}, LX/8zN;->a(LX/8zD;LX/8z9;ILandroid/text/SpannableStringBuilder;)V

    .line 1427319
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1427320
    if-eqz v3, :cond_0

    .line 1427321
    invoke-direct {p0, v2, p1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(Landroid/text/SpannableStringBuilder;LX/8zD;)V

    goto/16 :goto_0

    .line 1427322
    :cond_a
    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v0}, LX/8z6;->b(Ljava/lang/String;)LX/8z6;

    goto :goto_3

    .line 1427323
    :cond_b
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a:LX/8zA;

    iget-object v4, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->i:LX/8z7;

    invoke-virtual {v1, v0, v4}, LX/8zA;->a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_4
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 1427324
    sget-object v0, LX/03r;->MinutiaeTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1427325
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1427326
    const/16 v1, 0x3

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1427327
    iget-object v2, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1427328
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1427329
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1427330
    if-eqz v1, :cond_1

    .line 1427331
    iget-object v2, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1427332
    :cond_1
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1427333
    const/16 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1427334
    iget-object v2, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1427335
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1427336
    return-void
.end method

.method public static b(Lcom/facebook/composer/metatext/TagsTextViewContainer;)V
    .locals 1

    .prologue
    .line 1427337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    .line 1427338
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    .line 1427339
    return-void
.end method


# virtual methods
.method public final a(LX/8zD;LX/8z9;Z)V
    .locals 1

    .prologue
    .line 1427340
    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, p1, p2, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a$redex0(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;LX/8z9;I)V

    .line 1427341
    return-void

    .line 1427342
    :cond_0
    const/16 v0, 0x8c

    goto :goto_0
.end method

.method public final canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 1427246
    const/4 v0, 0x1

    return v0
.end method

.method public final canScrollVertically(I)Z
    .locals 1

    .prologue
    .line 1427247
    const/4 v0, 0x1

    return v0
.end method

.method public getLayoutHeight()I
    .locals 1

    .prologue
    .line 1427214
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v0

    return v0
.end method

.method public getMetaTextModel()LX/8zD;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1427215
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    return-object v0
.end method

.method public getMinutiaeController()LX/1aZ;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1427216
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    .line 1427217
    iget-object p0, v0, LX/1aX;->f:LX/1aZ;

    move-object v0, p0

    .line 1427218
    return-object v0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1427219
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7519bed3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1427220
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1427221
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1427222
    const/16 v1, 0x2d

    const v2, 0x2aeb41f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x75b7d9b9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1427223
    iget-object v1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1427224
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1427225
    const/16 v1, 0x2d

    const v2, -0x12c1bc7e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1427226
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishTemporaryDetach()V

    .line 1427227
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1427228
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1427229
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1427230
    invoke-super {p0}, Landroid/widget/FrameLayout;->onStartTemporaryDetach()V

    .line 1427231
    return-void
.end method

.method public setHighlightColor(I)V
    .locals 1

    .prologue
    .line 1427232
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHighlightColor(I)V

    .line 1427233
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 1

    .prologue
    .line 1427234
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxHeight(I)V

    .line 1427235
    return-void
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 1427236
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1427237
    return-void
.end method

.method public setMovementMethod(Landroid/text/method/MovementMethod;)V
    .locals 1

    .prologue
    .line 1427238
    iget-object v0, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1427239
    return-void
.end method

.method public setSeeMoreClickSpanListener(LX/8zL;)V
    .locals 0
    .param p1    # LX/8zL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1427240
    iput-object p1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->l:LX/8zL;

    .line 1427241
    return-void
.end method

.method public setSuffixStyleParams(LX/8z7;)V
    .locals 0

    .prologue
    .line 1427242
    iput-object p1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->i:LX/8z7;

    .line 1427243
    return-void
.end method

.method public setTextView(Landroid/widget/TextView;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1427244
    iput-object p1, p0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    .line 1427245
    return-void
.end method
