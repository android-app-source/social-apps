.class public Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1671016
    const-class v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    new-instance v1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1671017
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1671015
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1671004
    if-nez p0, :cond_0

    .line 1671005
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1671006
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1671007
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;->b(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;LX/0nX;LX/0my;)V

    .line 1671008
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1671009
    return-void
.end method

.method private static b(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1671011
    const-string v0, "is_collapsible"

    invoke-virtual {p0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isCollapsible()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1671012
    const-string v0, "is_facecast_nux_showing"

    invoke-virtual {p0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isFacecastNuxShowing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1671013
    const-string v0, "is_inline_sprouts_open"

    invoke-virtual {p0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1671014
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1671010
    check-cast p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;LX/0nX;LX/0my;)V

    return-void
.end method
