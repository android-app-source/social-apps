.class public final Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1670936
    const-class v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1670937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670938
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)V
    .locals 1

    .prologue
    .line 1670939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670940
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1670941
    instance-of v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    if-eqz v0, :cond_0

    .line 1670942
    check-cast p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 1670943
    iget-boolean v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a:Z

    .line 1670944
    iget-boolean v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->b:Z

    .line 1670945
    iget-boolean v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->c:Z

    .line 1670946
    :goto_0
    return-void

    .line 1670947
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isCollapsible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a:Z

    .line 1670948
    invoke-virtual {p1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isFacecastNuxShowing()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->b:Z

    .line 1670949
    invoke-virtual {p1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;
    .locals 2

    .prologue
    .line 1670950
    new-instance v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    invoke-direct {v0, p0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;-><init>(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;)V

    return-object v0
.end method

.method public setIsCollapsible(Z)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_collapsible"
    .end annotation

    .prologue
    .line 1670951
    iput-boolean p1, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a:Z

    .line 1670952
    return-object p0
.end method

.method public setIsFacecastNuxShowing(Z)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_facecast_nux_showing"
    .end annotation

    .prologue
    .line 1670953
    iput-boolean p1, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->b:Z

    .line 1670954
    return-object p0
.end method

.method public setIsInlineSproutsOpen(Z)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_inline_sprouts_open"
    .end annotation

    .prologue
    .line 1670955
    iput-boolean p1, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->c:Z

    .line 1670956
    return-object p0
.end method
