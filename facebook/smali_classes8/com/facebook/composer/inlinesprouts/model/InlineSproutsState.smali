.class public Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1671002
    const-class v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1670963
    const-class v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1671001
    new-instance v0, LX/AQC;

    invoke-direct {v0}, LX/AQC;-><init>()V

    sput-object v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1670993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670994
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    .line 1670995
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    .line 1670996
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    .line 1670997
    return-void

    :cond_0
    move v0, v2

    .line 1670998
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1670999
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1671000
    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;)V
    .locals 1

    .prologue
    .line 1670988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670989
    iget-boolean v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    .line 1670990
    iget-boolean v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    .line 1670991
    iget-boolean v0, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    .line 1670992
    return-void
.end method

.method public static a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
    .locals 2

    .prologue
    .line 1670987
    new-instance v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;-><init>(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
    .locals 2

    .prologue
    .line 1670986
    new-instance v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1671003
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1670975
    if-ne p0, p1, :cond_1

    .line 1670976
    :cond_0
    :goto_0
    return v0

    .line 1670977
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    if-nez v2, :cond_2

    move v0, v1

    .line 1670978
    goto :goto_0

    .line 1670979
    :cond_2
    check-cast p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 1670980
    iget-boolean v2, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    iget-boolean v3, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1670981
    goto :goto_0

    .line 1670982
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    iget-boolean v3, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1670983
    goto :goto_0

    .line 1670984
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    iget-boolean v3, p1, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1670985
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1670974
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isCollapsible()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_collapsible"
    .end annotation

    .prologue
    .line 1670973
    iget-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    return v0
.end method

.method public isFacecastNuxShowing()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_facecast_nux_showing"
    .end annotation

    .prologue
    .line 1670972
    iget-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    return v0
.end method

.method public isInlineSproutsOpen()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_inline_sprouts_open"
    .end annotation

    .prologue
    .line 1670971
    iget-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1670964
    iget-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1670965
    iget-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1670966
    iget-boolean v0, p0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->c:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1670967
    return-void

    :cond_0
    move v0, v2

    .line 1670968
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1670969
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1670970
    goto :goto_2
.end method
