.class public Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final p:Ljava/lang/String;


# instance fields
.field public q:Landroid/view/View;

.field public r:Lcom/facebook/widget/error/GenericErrorView;

.field private s:LX/0tX;

.field private t:LX/0Sh;

.field private u:LX/91H;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1431786
    const-class v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1431787
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1431788
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_composer_session_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0tX;LX/0Sh;LX/91H;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1431789
    iput-object p1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->s:LX/0tX;

    .line 1431790
    iput-object p2, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->t:LX/0Sh;

    .line 1431791
    iput-object p3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->u:LX/91H;

    .line 1431792
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v2}, LX/91H;->a(LX/0QB;)LX/91H;

    move-result-object v2

    check-cast v2, LX/91H;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a(LX/0tX;LX/0Sh;LX/91H;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 4

    .prologue
    .line 1431793
    new-instance v0, LX/0w7;

    invoke-direct {v0}, LX/0w7;-><init>()V

    .line 1431794
    const-string v1, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0w7;

    .line 1431795
    const-string v1, "minutiae_image_size_large"

    const-string v2, "32"

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    .line 1431796
    const-string v1, "taggable_activity_id"

    iget-object v2, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    .line 1431797
    new-instance v1, LX/5L5;

    invoke-direct {v1}, LX/5L5;-><init>()V

    move-object v1, v1

    .line 1431798
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/32 v2, 0x127500

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 1431799
    iget-object v1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->s:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1431800
    iget-object v1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->t:LX/0Sh;

    new-instance v2, LX/920;

    invoke-direct {v2, p0, p1}, LX/920;-><init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1431801
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Ljava/util/ArrayList;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaeIcon;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 1431802
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1431803
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 1431804
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-direct {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a()Ljava/lang/String;

    move-result-object v2

    .line 1431805
    new-instance v4, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-direct {v4}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;-><init>()V

    .line 1431806
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1431807
    const-string v6, "custom_icons"

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-static {v5, v6, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1431808
    const-string v6, "minutiae_object"

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1431809
    const-string v3, "session_id"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1431810
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1431811
    move-object v2, v4

    .line 1431812
    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 1431813
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1431814
    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1431815
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1431816
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1431817
    invoke-static {p0, p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1431818
    const v0, 0x7f03033c

    invoke-virtual {p0, v0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->setContentView(I)V

    .line 1431819
    const v0, 0x7f0d0446

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->q:Landroid/view/View;

    .line 1431820
    const v0, 0x7f0d0ab3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    .line 1431821
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1431822
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 1431823
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1431824
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1431825
    const v1, 0x7f08146a

    invoke-virtual {p0, v1}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1431826
    new-instance v1, LX/91x;

    invoke-direct {v1, p0}, LX/91x;-><init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1431827
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "is_skippable"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1431828
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1431829
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1431830
    move-object v1, v1

    .line 1431831
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1431832
    new-instance v1, LX/91y;

    invoke-direct {v1, p0}, LX/91y;-><init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1431833
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "icons"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1431834
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "minutiae_object"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431835
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1431836
    invoke-static {p0, v0, v1}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a$redex0(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Ljava/util/ArrayList;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 1431837
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->u:LX/91H;

    invoke-direct {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 1431838
    const-string p0, "iconpicker_started"

    invoke-static {p0, v2}, LX/91H;->a(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    .line 1431839
    iget-object p1, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p0, p1

    .line 1431840
    iget-object p1, v0, LX/91H;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1431841
    return-void

    .line 1431842
    :cond_1
    invoke-static {p0, v1}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a$redex0(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    .line 1431843
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1431844
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "minutiae_object"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431845
    iget-object v1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->u:LX/91H;

    invoke-direct {p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1431846
    const-string v4, "iconpicker_tapped_back"

    invoke-static {v4, v2}, LX/91H;->a(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    .line 1431847
    iget-object p0, v4, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, p0

    .line 1431848
    iget-object p0, v1, LX/91H;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1431849
    return-void
.end method
