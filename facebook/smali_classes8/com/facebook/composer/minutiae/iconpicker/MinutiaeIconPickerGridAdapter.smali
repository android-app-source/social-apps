.class public Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaeIcon;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/1Ad;

.field private final e:LX/924;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1431924
    const-class v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroid/content/Context;LX/1Ad;LX/924;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaeIcon;",
            ">;",
            "Landroid/content/Context;",
            "LX/1Ad;",
            "LX/924;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1431940
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1431941
    iput-object p1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->b:Ljava/util/List;

    .line 1431942
    iput-object p2, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->c:Landroid/content/Context;

    .line 1431943
    iput-object p3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->d:LX/1Ad;

    .line 1431944
    iput-object p4, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    .line 1431945
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1431933
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1431934
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1431935
    new-instance v1, LX/1Uo;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 1431936
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1431937
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    iget v2, v2, LX/924;->a:I

    iget-object v3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    iget v3, v3, LX/924;->a:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1431938
    iget-object v1, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    iget v1, v1, LX/924;->b:I

    iget-object v2, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    iget v2, v2, LX/924;->b:I

    iget-object v3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    iget v3, v3, LX/924;->b:I

    iget-object v4, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->e:LX/924;

    iget v4, v4, LX/924;->b:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1431939
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1431928
    check-cast p3, Lcom/facebook/drawee/view/DraweeView;

    .line 1431929
    check-cast p2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 1431930
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->d:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;->c()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1431931
    invoke-virtual {p3, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1431932
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1431927
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1431926
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1431925
    int-to-long v0, p1

    return-wide v0
.end method
