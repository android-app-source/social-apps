.class public Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaeIcon;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public d:Lcom/facebook/widget/gridview/BetterGridView;

.field private e:Landroid/widget/TextView;

.field public f:Landroid/widget/LinearLayout;

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public h:LX/8z3;

.field public i:LX/1Ad;

.field public j:LX/91H;

.field public k:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;

.field public l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1431891
    const-class v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1431892
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1431893
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->l:I

    .line 1431894
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-static {p0}, LX/8zJ;->b(LX/0QB;)LX/8zJ;

    move-result-object v1

    check-cast v1, LX/8zJ;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {p0}, LX/91H;->a(LX/0QB;)LX/91H;

    move-result-object p0

    check-cast p0, LX/91H;

    iput-object v1, p1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->h:LX/8z3;

    iput-object v2, p1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->i:LX/1Ad;

    iput-object p0, p1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->j:LX/91H;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1431895
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1431896
    const-class v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1431897
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1431898
    const-string v1, "custom_icons"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->b:Ljava/util/List;

    .line 1431899
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1431900
    const-string v1, "minutiae_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431901
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/16 v0, 0x2a

    const v1, 0x421e2cc5

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1431902
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1431903
    const v0, 0x7f030335

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1431904
    const v0, 0x7f0d0aab

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->e:Landroid/widget/TextView;

    .line 1431905
    const v0, 0x7f0d0aac

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/gridview/BetterGridView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    .line 1431906
    const v0, 0x7f0d0aa9

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->f:Landroid/widget/LinearLayout;

    .line 1431907
    const v0, 0x7f0d0aaa

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1431908
    new-instance v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->b:Ljava/util/List;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->i:LX/1Ad;

    new-instance v6, LX/924;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0c6a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0c6b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-direct {v6, v7, v8}, LX/924;-><init>(II)V

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;-><init>(Ljava/util/List;Landroid/content/Context;LX/1Ad;LX/924;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->k:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;

    .line 1431909
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->k:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/gridview/BetterGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1431910
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->h:LX/8z3;

    new-instance v4, LX/8z6;

    invoke-direct {v4}, LX/8z6;-><init>()V

    iget-object v5, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431911
    iput-object v5, v4, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431912
    move-object v4, v4

    .line 1431913
    invoke-virtual {v4}, LX/8z6;->a()LX/8z5;

    move-result-object v4

    invoke-interface {v3, v4}, LX/8z3;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1431914
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v0}, Lcom/facebook/widget/gridview/BetterGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v3, LX/921;

    invoke-direct {v3, p0}, LX/921;-><init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1431915
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    new-instance v3, LX/922;

    invoke-direct {v3, p0}, LX/922;-><init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/gridview/BetterGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1431916
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    new-instance v3, LX/923;

    invoke-direct {v3, p0}, LX/923;-><init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/gridview/BetterGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1431917
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1431918
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1431919
    :cond_1
    const/16 v0, 0x2b

    const v3, 0x26892b42

    invoke-static {v10, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
