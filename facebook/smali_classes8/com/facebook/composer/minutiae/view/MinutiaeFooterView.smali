.class public Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/view/View;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:Landroid/widget/TextView;

.field public f:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1433619
    const-class v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1433620
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1433621
    const v0, 0x7f030334

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1433622
    const v0, 0x7f0d0aa8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->b:Landroid/widget/ImageView;

    .line 1433623
    const v0, 0x7f0d0aa6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->c:Landroid/view/View;

    .line 1433624
    const v0, 0x7f0d0aa5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1433625
    const v0, 0x7f0d0aa7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->e:Landroid/widget/TextView;

    .line 1433626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->f:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    .line 1433627
    return-void
.end method
