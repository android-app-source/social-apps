.class public Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/1aX;

.field private final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1433704
    const-class v0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1433726
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1433727
    const-class v0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;

    invoke-static {v0, p0}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1433728
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1433729
    new-instance v1, LX/1aX;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1aX;-><init>(LX/1aY;)V

    iput-object v1, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    .line 1433730
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c59

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->d:I

    .line 1433731
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->b:LX/1Ad;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1433717
    iget-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    .line 1433718
    iget-object v2, v1, LX/1aX;->f:LX/1aZ;

    move-object v1, v2

    .line 1433719
    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1433720
    iget-object v1, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1433721
    iget-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1433722
    if-eqz v0, :cond_0

    .line 1433723
    iget v1, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->d:I

    iget v2, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->d:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1433724
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {p0, v0, v4, v1, v4}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1433725
    :cond_0
    return-object p0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x328b4f64

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1433714
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onAttachedToWindow()V

    .line 1433715
    iget-object v1, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1433716
    const/16 v1, 0x2d

    const v2, -0x720e34d3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x47d75b54

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1433711
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onDetachedFromWindow()V

    .line 1433712
    iget-object v1, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1433713
    const/16 v1, 0x2d

    const v2, 0x5bb721aa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1433708
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onFinishTemporaryDetach()V

    .line 1433709
    iget-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1433710
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1433705
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onStartTemporaryDetach()V

    .line 1433706
    iget-object v0, p0, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1433707
    return-void
.end method
