.class public Lcom/facebook/composer/minutiae/view/stubs/MinutiaeFooterViewStub;
.super LX/4nv;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1433741
    invoke-direct {p0, p1}, LX/4nv;-><init>(Landroid/content/Context;)V

    .line 1433742
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1433743
    invoke-direct {p0, p1, p2}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1433744
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1433745
    invoke-direct {p0, p1, p2, p3}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1433746
    return-void
.end method


# virtual methods
.method public getInflatedLayout()Landroid/view/View;
    .locals 3

    .prologue
    .line 1433747
    new-instance v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/view/stubs/MinutiaeFooterViewStub;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method
