.class public Lcom/facebook/composer/minutiae/view/stubs/MinutiaeNuxViewStub;
.super LX/4nv;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1433758
    invoke-direct {p0, p1}, LX/4nv;-><init>(Landroid/content/Context;)V

    .line 1433759
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1433763
    invoke-direct {p0, p1, p2}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1433764
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1433761
    invoke-direct {p0, p1, p2, p3}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1433762
    return-void
.end method


# virtual methods
.method public getInflatedLayout()Landroid/view/View;
    .locals 3

    .prologue
    .line 1433760
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/view/stubs/MinutiaeNuxViewStub;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030339

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
