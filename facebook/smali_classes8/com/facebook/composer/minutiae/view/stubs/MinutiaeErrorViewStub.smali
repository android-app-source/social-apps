.class public Lcom/facebook/composer/minutiae/view/stubs/MinutiaeErrorViewStub;
.super LX/4nv;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1433732
    invoke-direct {p0, p1}, LX/4nv;-><init>(Landroid/content/Context;)V

    .line 1433733
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1433737
    invoke-direct {p0, p1, p2}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1433738
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1433739
    invoke-direct {p0, p1, p2, p3}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1433740
    return-void
.end method


# virtual methods
.method public getInflatedLayout()Landroid/view/View;
    .locals 2

    .prologue
    .line 1433734
    new-instance v0, Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/view/stubs/MinutiaeErrorViewStub;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;-><init>(Landroid/content/Context;)V

    .line 1433735
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1433736
    return-object v0
.end method
