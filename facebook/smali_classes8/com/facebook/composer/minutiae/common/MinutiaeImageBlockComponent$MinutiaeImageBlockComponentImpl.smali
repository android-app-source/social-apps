.class public final Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91M;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/CharSequence;

.field public c:I

.field public d:I

.field public e:I

.field public f:Ljava/lang/CharSequence;

.field public g:Ljava/lang/CharSequence;

.field public h:LX/1dQ;

.field public i:LX/1dQ;

.field public j:LX/1dQ;

.field public k:I

.field public l:Lcom/facebook/common/callercontext/CallerContext;

.field public m:LX/4Ab;

.field public n:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public o:Landroid/net/Uri;

.field public p:I

.field public q:I

.field public r:I

.field public s:Ljava/lang/CharSequence;

.field public t:Z

.field public final synthetic u:LX/91M;


# direct methods
.method public constructor <init>(LX/91M;)V
    .locals 1

    .prologue
    .line 1430470
    iput-object p1, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->u:LX/91M;

    .line 1430471
    move-object v0, p1

    .line 1430472
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1430473
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->k:I

    .line 1430474
    sget-object v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 1430475
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430476
    const-string v0, "MinutiaeImageBlockComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1430477
    if-ne p0, p1, :cond_1

    .line 1430478
    :cond_0
    :goto_0
    return v0

    .line 1430479
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1430480
    goto :goto_0

    .line 1430481
    :cond_3
    check-cast p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    .line 1430482
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1430483
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1430484
    if-eq v2, v3, :cond_0

    .line 1430485
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1430486
    goto :goto_0

    .line 1430487
    :cond_5
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 1430488
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1430489
    goto :goto_0

    .line 1430490
    :cond_8
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1430491
    :cond_9
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->c:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1430492
    goto :goto_0

    .line 1430493
    :cond_a
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->d:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1430494
    goto :goto_0

    .line 1430495
    :cond_b
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->e:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->e:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1430496
    goto :goto_0

    .line 1430497
    :cond_c
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->f:Ljava/lang/CharSequence;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->f:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1430498
    goto :goto_0

    .line 1430499
    :cond_e
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->f:Ljava/lang/CharSequence;

    if-nez v2, :cond_d

    .line 1430500
    :cond_f
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->g:Ljava/lang/CharSequence;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->g:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1430501
    goto :goto_0

    .line 1430502
    :cond_11
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_10

    .line 1430503
    :cond_12
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->h:LX/1dQ;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->h:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->h:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1430504
    goto/16 :goto_0

    .line 1430505
    :cond_14
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->h:LX/1dQ;

    if-nez v2, :cond_13

    .line 1430506
    :cond_15
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->i:LX/1dQ;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->i:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->i:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 1430507
    goto/16 :goto_0

    .line 1430508
    :cond_17
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->i:LX/1dQ;

    if-nez v2, :cond_16

    .line 1430509
    :cond_18
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->j:LX/1dQ;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->j:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->j:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 1430510
    goto/16 :goto_0

    .line 1430511
    :cond_1a
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->j:LX/1dQ;

    if-nez v2, :cond_19

    .line 1430512
    :cond_1b
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->k:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->k:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 1430513
    goto/16 :goto_0

    .line 1430514
    :cond_1c
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 1430515
    goto/16 :goto_0

    .line 1430516
    :cond_1e
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_1d

    .line 1430517
    :cond_1f
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->m:LX/4Ab;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->m:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->m:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 1430518
    goto/16 :goto_0

    .line 1430519
    :cond_21
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->m:LX/4Ab;

    if-nez v2, :cond_20

    .line 1430520
    :cond_22
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->n:LX/1dc;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->n:LX/1dc;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->n:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 1430521
    goto/16 :goto_0

    .line 1430522
    :cond_24
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->n:LX/1dc;

    if-nez v2, :cond_23

    .line 1430523
    :cond_25
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->o:Landroid/net/Uri;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->o:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->o:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    :cond_26
    move v0, v1

    .line 1430524
    goto/16 :goto_0

    .line 1430525
    :cond_27
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->o:Landroid/net/Uri;

    if-nez v2, :cond_26

    .line 1430526
    :cond_28
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->p:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->p:I

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 1430527
    goto/16 :goto_0

    .line 1430528
    :cond_29
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->q:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->q:I

    if-eq v2, v3, :cond_2a

    move v0, v1

    .line 1430529
    goto/16 :goto_0

    .line 1430530
    :cond_2a
    iget v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->r:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->r:I

    if-eq v2, v3, :cond_2b

    move v0, v1

    .line 1430531
    goto/16 :goto_0

    .line 1430532
    :cond_2b
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->s:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->s:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->s:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    :cond_2c
    move v0, v1

    .line 1430533
    goto/16 :goto_0

    .line 1430534
    :cond_2d
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->s:Ljava/lang/CharSequence;

    if-nez v2, :cond_2c

    .line 1430535
    :cond_2e
    iget-boolean v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->t:Z

    iget-boolean v3, p1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->t:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1430536
    goto/16 :goto_0
.end method
