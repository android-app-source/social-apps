.class public Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1430422
    const-class v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1430423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430424
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;
    .locals 3

    .prologue
    .line 1430425
    const-class v1, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;

    monitor-enter v1

    .line 1430426
    :try_start_0
    sget-object v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1430427
    sput-object v2, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1430428
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430429
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1430430
    new-instance v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;

    invoke-direct {v0}, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;-><init>()V

    .line 1430431
    move-object v0, v0

    .line 1430432
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1430433
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430434
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1430435
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
