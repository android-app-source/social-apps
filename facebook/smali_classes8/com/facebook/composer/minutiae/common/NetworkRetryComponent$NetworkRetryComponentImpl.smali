.class public final Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91S;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/CharSequence;

.field public c:I

.field public d:Lcom/facebook/common/callercontext/CallerContext;

.field public e:LX/4Ab;

.field public f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1430757
    invoke-static {}, LX/91S;->q()LX/91S;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1430758
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430759
    const-string v0, "NetworkRetryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1430760
    if-ne p0, p1, :cond_1

    .line 1430761
    :cond_0
    :goto_0
    return v0

    .line 1430762
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1430763
    goto :goto_0

    .line 1430764
    :cond_3
    check-cast p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;

    .line 1430765
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1430766
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1430767
    if-eq v2, v3, :cond_0

    .line 1430768
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->a:LX/1dc;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->a:LX/1dc;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->a:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1430769
    goto :goto_0

    .line 1430770
    :cond_5
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->a:LX/1dc;

    if-nez v2, :cond_4

    .line 1430771
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1430772
    goto :goto_0

    .line 1430773
    :cond_8
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1430774
    :cond_9
    iget v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->c:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1430775
    goto :goto_0

    .line 1430776
    :cond_a
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1430777
    goto :goto_0

    .line 1430778
    :cond_c
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_b

    .line 1430779
    :cond_d
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->e:LX/4Ab;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->e:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->e:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1430780
    goto :goto_0

    .line 1430781
    :cond_f
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->e:LX/4Ab;

    if-nez v2, :cond_e

    .line 1430782
    :cond_10
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->f:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->f:Landroid/view/View$OnClickListener;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1430783
    goto :goto_0

    .line 1430784
    :cond_11
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->f:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
