.class public final Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91K;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/CharSequence;

.field public d:I

.field public e:Lcom/facebook/common/callercontext/CallerContext;

.field public f:LX/4Ab;

.field public g:Landroid/view/View$OnClickListener;

.field public final synthetic h:LX/91K;


# direct methods
.method public constructor <init>(LX/91K;)V
    .locals 1

    .prologue
    .line 1430341
    iput-object p1, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->h:LX/91K;

    .line 1430342
    move-object v0, p1

    .line 1430343
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1430344
    sget-object v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1430345
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430346
    const-string v0, "GenericErrorComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1430347
    if-ne p0, p1, :cond_1

    .line 1430348
    :cond_0
    :goto_0
    return v0

    .line 1430349
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1430350
    goto :goto_0

    .line 1430351
    :cond_3
    check-cast p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    .line 1430352
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1430353
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1430354
    if-eq v2, v3, :cond_0

    .line 1430355
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1430356
    goto :goto_0

    .line 1430357
    :cond_5
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1430358
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->b:LX/1dc;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->b:LX/1dc;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->b:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1430359
    goto :goto_0

    .line 1430360
    :cond_8
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->b:LX/1dc;

    if-nez v2, :cond_7

    .line 1430361
    :cond_9
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1430362
    goto :goto_0

    .line 1430363
    :cond_b
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 1430364
    :cond_c
    iget v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->d:I

    iget v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->d:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1430365
    goto :goto_0

    .line 1430366
    :cond_d
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1430367
    goto :goto_0

    .line 1430368
    :cond_f
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_e

    .line 1430369
    :cond_10
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->f:LX/4Ab;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->f:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->f:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 1430370
    goto :goto_0

    .line 1430371
    :cond_12
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->f:LX/4Ab;

    if-nez v2, :cond_11

    .line 1430372
    :cond_13
    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->g:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->g:Landroid/view/View$OnClickListener;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1430373
    goto/16 :goto_0

    .line 1430374
    :cond_14
    iget-object v2, p1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->g:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
