.class public Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1430601
    const-class v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430566
    iput-object p1, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->b:LX/0Or;

    .line 1430567
    return-void
.end method

.method private static a(LX/1De;II)I
    .locals 1

    .prologue
    .line 1430598
    if-nez p1, :cond_0

    .line 1430599
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int p1, v0

    .line 1430600
    :cond_0
    return p1
.end method

.method private static a(LX/1De;Landroid/net/Uri;LX/1dc;IIILX/1dQ;Ljava/lang/CharSequence;ZLX/1Ad;)LX/1Dg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/net/Uri;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;III",
            "LX/1dQ;",
            "Ljava/lang/CharSequence;",
            "Z",
            "LX/1Ad;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    .line 1430589
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 1430590
    const/4 v0, 0x0

    .line 1430591
    :goto_0
    return-object v0

    .line 1430592
    :cond_0
    if-eqz p1, :cond_1

    .line 1430593
    invoke-static {p0}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v0

    invoke-virtual {p9, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3, p5}, LX/1Di;->a(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p7}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 1430594
    :cond_1
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3, p5}, LX/1Di;->a(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p7}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    .line 1430595
    if-eqz p8, :cond_2

    .line 1430596
    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 1430597
    :cond_2
    invoke-interface {v0, p4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;
    .locals 4

    .prologue
    .line 1430578
    const-class v1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;

    monitor-enter v1

    .line 1430579
    :try_start_0
    sget-object v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1430580
    sput-object v2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1430581
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430582
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1430583
    new-instance v3, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;-><init>(LX/0Or;)V

    .line 1430584
    move-object v0, v3

    .line 1430585
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1430586
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430587
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1430588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Landroid/net/Uri;Ljava/lang/CharSequence;IIILjava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dQ;LX/1dQ;LX/1dQ;ILcom/facebook/common/callercontext/CallerContext;LX/4Ab;LX/1dc;Landroid/net/Uri;IIILjava/lang/CharSequence;Z)LX/1Dg;
    .locals 12
    .param p2    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p7    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p8    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p9    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # LX/4Ab;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p16    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p18    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p19    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p20    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p21    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/net/Uri;",
            "Ljava/lang/CharSequence;",
            "III",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/4Ab;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/net/Uri;",
            "III",
            "Ljava/lang/CharSequence;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1430568
    iget-object v1, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/1Ad;

    .line 1430569
    if-eqz p16, :cond_0

    if-eqz p15, :cond_0

    .line 1430570
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Only one of accessoryUri and accessoryDrawable can be used at the same time."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1430571
    :cond_0
    const v1, 0x7f0b1534

    move/from16 v0, p4

    invoke-static {p1, v0, v1}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v1

    .line 1430572
    const v2, 0x7f0b1534

    move/from16 v0, p5

    invoke-static {p1, v0, v2}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v2

    .line 1430573
    const v3, 0x7f0b1536

    move/from16 v0, p6

    invoke-static {p1, v0, v3}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v3

    .line 1430574
    const v4, 0x7f0b1535

    move/from16 v0, p17

    invoke-static {p1, v0, v4}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v5

    .line 1430575
    const v4, 0x7f0b1535

    move/from16 v0, p18

    invoke-static {p1, v0, v4}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v4

    .line 1430576
    const v6, 0x7f0b1536

    move/from16 v0, p19

    invoke-static {p1, v0, v6}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v6

    .line 1430577
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    move/from16 v0, p12

    invoke-interface {v7, v0}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v8

    invoke-virtual {v10, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v9

    move-object/from16 v0, p13

    invoke-virtual {v9, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v9

    invoke-virtual {v9}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v8

    move-object/from16 v0, p14

    invoke-virtual {v8, v0}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v8, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {v1, v2, v3}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-interface {v1, v0}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-interface {v1, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v7, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v7, 0x7f0b0050

    invoke-virtual {v3, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v7, 0x2

    const v8, 0x7f0b1536

    invoke-interface {v3, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-interface {v1, v0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v11

    move-object v1, p1

    move-object/from16 v2, p16

    move-object/from16 v3, p15

    move-object/from16 v7, p10

    move-object/from16 v8, p20

    move/from16 v9, p21

    invoke-static/range {v1 .. v10}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;Landroid/net/Uri;LX/1dc;IIILX/1dQ;Ljava/lang/CharSequence;ZLX/1Ad;)LX/1Dg;

    move-result-object v1

    invoke-interface {v11, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v7, 0x7f0b004e

    invoke-virtual {v1, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v7, 0x7f0a010e

    invoke-virtual {v1, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v7, 0x2

    const v8, 0x7f0b1536

    invoke-interface {v1, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    goto :goto_0
.end method
