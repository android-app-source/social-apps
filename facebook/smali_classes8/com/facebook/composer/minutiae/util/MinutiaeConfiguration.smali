.class public Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/93C;


# instance fields
.field public final b:LX/939;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/93E;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Z

.field public final g:Z

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/93G;

.field public final m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:LX/93E;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1433484
    new-instance v0, LX/936;

    invoke-direct {v0}, LX/936;-><init>()V

    sput-object v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1433485
    new-instance v0, LX/93C;

    invoke-direct {v0}, LX/93C;-><init>()V

    sput-object v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a:LX/93C;

    return-void
.end method

.method public constructor <init>(LX/937;)V
    .locals 3

    .prologue
    .line 1433453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433454
    iget-object v0, p1, LX/937;->c:LX/939;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    .line 1433455
    iget-object v0, p1, LX/937;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1433456
    iget-object v0, p1, LX/937;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1433457
    iget-object v0, p1, LX/937;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    .line 1433458
    iget-boolean v0, p1, LX/937;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    .line 1433459
    iget-boolean v0, p1, LX/937;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    .line 1433460
    iget-object v0, p1, LX/937;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    .line 1433461
    iget-object v0, p1, LX/937;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    .line 1433462
    iget-object v0, p1, LX/937;->k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1433463
    iget-object v0, p1, LX/937;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    .line 1433464
    iget-object v0, p1, LX/937;->m:LX/93G;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93G;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    .line 1433465
    iget-object v0, p1, LX/937;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 1433466
    iget-object v0, p1, LX/937;->o:LX/93E;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    .line 1433467
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1433468
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v0

    .line 1433469
    if-eqz v0, :cond_1

    .line 1433470
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v0

    .line 1433471
    sget-object p1, LX/93E;->UNKNOWN:LX/93E;

    if-eq v0, p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1433472
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v0

    .line 1433473
    sget-object p1, LX/93E;->OBJECT_PICKER:LX/93E;

    if-ne v0, p1, :cond_0

    .line 1433474
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433475
    if-eqz v0, :cond_2

    .line 1433476
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433477
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1433478
    :goto_1
    iget-object p1, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object p1, p1

    .line 1433479
    if-eqz p1, :cond_3

    :goto_2
    xor-int/2addr v0, v1

    const-string v1, "Exactly one of taggable activity and taggable activity suggestions must be provided"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1433480
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1433481
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1433482
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1433483
    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1433411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433412
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1433413
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    .line 1433414
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1433415
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1433416
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1433417
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1433418
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1433419
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    .line 1433420
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_5

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    .line 1433421
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_6

    :goto_5
    iput-boolean v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    .line 1433422
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 1433423
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    .line 1433424
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    .line 1433425
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    .line 1433426
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 1433427
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1433428
    :goto_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    .line 1433429
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    .line 1433430
    :goto_9
    invoke-static {}, LX/93G;->values()[LX/93G;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    .line 1433431
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    .line 1433432
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 1433433
    :goto_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    .line 1433434
    iput-object v6, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    .line 1433435
    :goto_b
    return-void

    .line 1433436
    :cond_0
    invoke-static {}, LX/939;->values()[LX/939;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    goto :goto_0

    .line 1433437
    :cond_1
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    goto :goto_1

    .line 1433438
    :cond_2
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    goto :goto_2

    .line 1433439
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [LX/93E;

    move v0, v1

    .line 1433440
    :goto_c
    array-length v4, v3

    if-ge v0, v4, :cond_4

    .line 1433441
    invoke-static {}, LX/93E;->values()[LX/93E;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    aget-object v4, v4, v5

    .line 1433442
    aput-object v4, v3, v0

    .line 1433443
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1433444
    :cond_4
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 1433445
    goto/16 :goto_4

    :cond_6
    move v2, v1

    .line 1433446
    goto/16 :goto_5

    .line 1433447
    :cond_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    goto/16 :goto_6

    .line 1433448
    :cond_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    goto/16 :goto_7

    .line 1433449
    :cond_9
    sget-object v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    goto/16 :goto_8

    .line 1433450
    :cond_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    goto/16 :goto_9

    .line 1433451
    :cond_b
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    goto :goto_a

    .line 1433452
    :cond_c
    invoke-static {}, LX/93E;->values()[LX/93E;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    goto/16 :goto_b
.end method

.method public static a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;
    .locals 2

    .prologue
    .line 1433410
    new-instance v0, LX/937;

    invoke-direct {v0, p0}, LX/937;-><init>(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V

    return-object v0
.end method

.method public static newBuilder()LX/937;
    .locals 2

    .prologue
    .line 1433486
    new-instance v0, LX/937;

    invoke-direct {v0}, LX/937;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1433409
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1433378
    if-ne p0, p1, :cond_1

    .line 1433379
    :cond_0
    :goto_0
    return v0

    .line 1433380
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 1433381
    goto :goto_0

    .line 1433382
    :cond_2
    check-cast p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1433383
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1433384
    goto :goto_0

    .line 1433385
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1433386
    goto :goto_0

    .line 1433387
    :cond_4
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1433388
    goto :goto_0

    .line 1433389
    :cond_5
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1433390
    goto :goto_0

    .line 1433391
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    iget-boolean v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1433392
    goto :goto_0

    .line 1433393
    :cond_7
    iget-boolean v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    iget-boolean v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1433394
    goto :goto_0

    .line 1433395
    :cond_8
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1433396
    goto :goto_0

    .line 1433397
    :cond_9
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1433398
    goto :goto_0

    .line 1433399
    :cond_a
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1433400
    goto :goto_0

    .line 1433401
    :cond_b
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 1433402
    goto :goto_0

    .line 1433403
    :cond_c
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1433404
    goto/16 :goto_0

    .line 1433405
    :cond_d
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 1433406
    goto/16 :goto_0

    .line 1433407
    :cond_e
    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    iget-object v3, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1433408
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1433377
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1433328
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    if-nez v0, :cond_1

    .line 1433329
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433330
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v0, :cond_2

    .line 1433331
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433332
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v0, :cond_3

    .line 1433333
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433334
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    if-nez v0, :cond_4

    .line 1433335
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433336
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433337
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433338
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1433339
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433340
    :goto_5
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 1433341
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433342
    :goto_6
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_9

    .line 1433343
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433344
    :goto_7
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 1433345
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433346
    :goto_8
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    invoke-virtual {v0}, LX/93G;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433347
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    if-nez v0, :cond_b

    .line 1433348
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433349
    :goto_9
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    if-nez v0, :cond_c

    .line 1433350
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433351
    :goto_a
    return-void

    .line 1433352
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433353
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    invoke-virtual {v0}, LX/939;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 1433354
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433355
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_1

    .line 1433356
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433357
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_2

    .line 1433358
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433359
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433360
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_b
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93E;

    .line 1433361
    invoke-virtual {v0}, LX/93E;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433362
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    :cond_5
    move v0, v2

    .line 1433363
    goto/16 :goto_3

    :cond_6
    move v0, v2

    .line 1433364
    goto/16 :goto_4

    .line 1433365
    :cond_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433366
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1433367
    :cond_8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433368
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1433369
    :cond_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433370
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_7

    .line 1433371
    :cond_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433372
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1433373
    :cond_b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433374
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto/16 :goto_9

    .line 1433375
    :cond_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1433376
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    invoke-virtual {v0}, LX/93E;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_a
.end method
