.class public Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/932;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:LX/91C;

.field public d:LX/90U;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1433196
    const-class v0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1433197
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1433198
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    new-instance v0, LX/91C;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    invoke-direct {v0, p0}, LX/91C;-><init>(LX/0Zb;)V

    move-object v1, v0

    check-cast v1, LX/91C;

    iput-object v1, p1, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->c:LX/91C;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1433199
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1433200
    const-class v0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1433201
    iget-object v0, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->d:LX/90U;

    invoke-interface {v0}, LX/90U;->b()Ljava/lang/String;

    move-result-object v0

    .line 1433202
    iget-object v1, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->d:LX/90U;

    invoke-interface {v1}, LX/90U;->fS_()Z

    move-result v1

    .line 1433203
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1433204
    iget-object v2, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->c:LX/91C;

    .line 1433205
    iput-object v0, v2, LX/91C;->b:Ljava/lang/String;

    .line 1433206
    if-nez p1, :cond_0

    .line 1433207
    iget-object v0, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->c:LX/91C;

    .line 1433208
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/91A;->STICKER_PICKER_OPENED:LX/91A;

    invoke-virtual {p0}, LX/91A;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "composer"

    .line 1433209
    iput-object p0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1433210
    move-object v2, v2

    .line 1433211
    sget-object p0, LX/91B;->IS_FROM_COMPOSER:LX/91B;

    invoke-virtual {p0}, LX/91B;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1433212
    invoke-static {v0, v2}, LX/91C;->a(LX/91C;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1433213
    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1433214
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1433215
    check-cast p1, LX/90U;

    iput-object p1, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->d:LX/90U;

    .line 1433216
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2a16558b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1433217
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0995

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1433218
    new-instance v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {v2, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;-><init>(Landroid/content/Context;)V

    .line 1433219
    sget-object v1, LX/4m4;->POSTS:LX/4m4;

    invoke-virtual {v2, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->setInterface(LX/4m4;)V

    .line 1433220
    new-instance v1, LX/932;

    invoke-direct {v1, p0}, LX/932;-><init>(Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;)V

    iput-object v1, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->b:LX/932;

    .line 1433221
    iget-object v1, p0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->b:LX/932;

    .line 1433222
    iput-object v1, v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    .line 1433223
    invoke-virtual {v2}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->requestFocus()Z

    .line 1433224
    const/16 v1, 0x2b

    const v3, -0x628bb347

    invoke-static {v4, v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
