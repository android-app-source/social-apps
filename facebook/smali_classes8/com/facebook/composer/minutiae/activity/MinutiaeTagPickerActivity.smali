.class public Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/90Z;
.implements LX/90a;
.implements LX/90b;


# instance fields
.field private A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/925;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcom/facebook/widget/OverlayLayout;

.field private C:LX/0h5;

.field private D:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/nux/ui/NuxBubbleView;",
            ">;"
        }
    .end annotation
.end field

.field public E:Ljava/lang/String;

.field private F:LX/9ju;

.field private G:LX/9jn;

.field public H:LX/9jN;

.field private I:LX/0y2;

.field private final J:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/9jN;",
            ">;"
        }
    .end annotation
.end field

.field private final K:LX/90X;

.field public p:LX/0Px;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/93E;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private r:LX/935;

.field public s:LX/91G;

.field private t:LX/93E;

.field private u:LX/919;

.field private v:LX/92E;

.field private w:LX/92C;

.field private x:LX/1Kf;

.field private y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/92o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1429134
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1429135
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->D:LX/0am;

    .line 1429136
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1429137
    new-instance v0, LX/90W;

    invoke-direct {v0, p0}, LX/90W;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->J:LX/0TF;

    .line 1429138
    new-instance v0, LX/90Y;

    invoke-direct {v0, p0}, LX/90Y;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->K:LX/90X;

    return-void
.end method

.method private a(LX/90u;)V
    .locals 1

    .prologue
    .line 1429139
    instance-of v0, p1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1429140
    check-cast v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    .line 1429141
    iput-object p0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->u:LX/90a;

    .line 1429142
    :cond_0
    :goto_0
    invoke-interface {p1, p0}, LX/90u;->a(LX/90b;)V

    .line 1429143
    return-void

    .line 1429144
    :cond_1
    instance-of v0, p1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1429145
    check-cast v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    .line 1429146
    iput-object p0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a:LX/90Z;

    .line 1429147
    goto :goto_0
.end method

.method private a(LX/935;LX/91G;LX/919;LX/92E;LX/92C;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Kf;LX/9jn;LX/9ju;LX/0y2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/935;",
            "LX/91G;",
            "LX/919;",
            "LX/92E;",
            "LX/92C;",
            "LX/0Ot",
            "<",
            "LX/92o;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/925;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/1Kf;",
            "LX/9jn;",
            "LX/9ju;",
            "LX/0y2;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1429148
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->r:LX/935;

    .line 1429149
    iput-object p2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->s:LX/91G;

    .line 1429150
    iput-object p3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->u:LX/919;

    .line 1429151
    iput-object p4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->v:LX/92E;

    .line 1429152
    iput-object p5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->w:LX/92C;

    .line 1429153
    iput-object p6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->z:LX/0Ot;

    .line 1429154
    iput-object p7, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->A:LX/0Ot;

    .line 1429155
    iput-object p8, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->y:LX/0Ot;

    .line 1429156
    iput-object p9, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->x:LX/1Kf;

    .line 1429157
    iput-object p10, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->G:LX/9jn;

    .line 1429158
    iput-object p11, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->F:LX/9ju;

    .line 1429159
    iput-object p12, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->I:LX/0y2;

    .line 1429160
    return-void
.end method

.method private a(LX/93E;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1429161
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1429162
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1429163
    iput-object v1, v0, LX/937;->f:LX/0Px;

    .line 1429164
    move-object v0, v0

    .line 1429165
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429166
    return-void
.end method

.method private a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 3

    .prologue
    .line 1429167
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v0

    sget-object v1, LX/9jG;->CHECKIN:LX/9jG;

    .line 1429168
    iput-object v1, v0, LX/9jF;->q:LX/9jG;

    .line 1429169
    move-object v0, v0

    .line 1429170
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429171
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v2

    .line 1429172
    iput-object v1, v0, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1429173
    move-object v0, v0

    .line 1429174
    iput-object p1, v0, LX/9jF;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1429175
    move-object v0, v0

    .line 1429176
    const/4 v1, 0x1

    .line 1429177
    iput-boolean v1, v0, LX/9jF;->y:Z

    .line 1429178
    move-object v0, v0

    .line 1429179
    invoke-virtual {v0}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v0

    invoke-static {p0, v0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3f4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1429180
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    new-instance v3, LX/935;

    invoke-static {v12}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {v12}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v1, v2}, LX/935;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    move-object v1, v3

    check-cast v1, LX/935;

    new-instance v3, LX/91G;

    invoke-static {v12}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v2}, LX/91G;-><init>(LX/0Zb;)V

    move-object v2, v3

    check-cast v2, LX/91G;

    invoke-static {v12}, LX/919;->a(LX/0QB;)LX/919;

    move-result-object v3

    check-cast v3, LX/919;

    invoke-static {v12}, LX/92E;->a(LX/0QB;)LX/92E;

    move-result-object v4

    check-cast v4, LX/92E;

    invoke-static {v12}, LX/92C;->a(LX/0QB;)LX/92C;

    move-result-object v5

    check-cast v5, LX/92C;

    const/16 v6, 0x19b7

    invoke-static {v12, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x19a8

    invoke-static {v12, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x455

    invoke-static {v12, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v12}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v9

    check-cast v9, LX/1Kf;

    invoke-static {v12}, LX/9jn;->a(LX/0QB;)LX/9jn;

    move-result-object v10

    check-cast v10, LX/9jn;

    invoke-static {v12}, LX/9ju;->b(LX/0QB;)LX/9ju;

    move-result-object v11

    check-cast v11, LX/9ju;

    invoke-static {v12}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v12

    check-cast v12, LX/0y2;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(LX/935;LX/91G;LX/919;LX/92E;LX/92C;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Kf;LX/9jn;LX/9ju;LX/0y2;)V

    return-void
.end method

.method private b()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1429181
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1429182
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    sget-object v1, LX/93E;->UNKNOWN:LX/93E;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1429183
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    invoke-direct {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(LX/93E;)V

    .line 1429184
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->m()V

    .line 1429185
    return-void

    .line 1429186
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/5LG;)V
    .locals 2

    .prologue
    .line 1429187
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->a(LX/5LG;)Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v1

    .line 1429188
    iput-object v1, v0, LX/937;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 1429189
    move-object v0, v0

    .line 1429190
    sget-object v1, LX/93E;->OBJECT_PICKER:LX/93E;

    .line 1429191
    iput-object v1, v0, LX/937;->o:LX/93E;

    .line 1429192
    move-object v0, v0

    .line 1429193
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429194
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429195
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v1

    .line 1429196
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    .line 1429197
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    invoke-direct {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(LX/93E;)V

    .line 1429198
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->m()V

    .line 1429199
    return-void
.end method

.method private b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1429261
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->H:LX/9jN;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 1429262
    :cond_1
    :goto_0
    return v2

    .line 1429263
    :cond_2
    iget-object v4, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 1429264
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;->c()Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->NORMAL:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    if-eq v0, v1, :cond_1

    .line 1429265
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->H:LX/9jN;

    .line 1429266
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 1429267
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1429268
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 1429269
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 1429270
    :goto_2
    if-nez v1, :cond_1

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_4
    move v1, v3

    .line 1429271
    goto :goto_1

    :cond_5
    move v0, v3

    .line 1429272
    goto :goto_2

    :cond_6
    move v2, v3

    .line 1429273
    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1429200
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429201
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1429202
    if-eqz v0, :cond_0

    .line 1429203
    const-string v1, "extra_place"

    invoke-static {p1, v1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1429204
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1429205
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->s:LX/91G;

    sget-object v2, LX/91E;->SELECT:LX/91E;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429206
    iget-object p0, v4, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v4, p0

    .line 1429207
    invoke-virtual {v1, v2, v3, v0, v4}, LX/91G;->a(LX/91E;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1429208
    :cond_0
    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1429209
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1429210
    iput-object v1, v0, LX/937;->f:LX/0Px;

    .line 1429211
    move-object v0, v0

    .line 1429212
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429213
    const-string v0, "bundle_config"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1429214
    return-void
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1429215
    const-string v0, "bundle_config"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429216
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429217
    iget-object p1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    move-object v0, p1

    .line 1429218
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1429219
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1429220
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429221
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v1

    .line 1429222
    sget-object v1, LX/93E;->VERB_PICKER:LX/93E;

    if-ne v0, v1, :cond_0

    .line 1429223
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->v:LX/92E;

    .line 1429224
    const v1, 0x430001

    const-string p0, "minutiae_verb_picker_time_to_init"

    invoke-virtual {v0, v1, p0}, LX/92A;->b(ILjava/lang/String;)V

    .line 1429225
    const v1, 0x430006

    const-string p0, "minutiae_verb_picker_tti"

    invoke-virtual {v0, v1, p0}, LX/92A;->c(ILjava/lang/String;)V

    .line 1429226
    :goto_0
    return-void

    .line 1429227
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->w:LX/92C;

    .line 1429228
    const v1, 0x420001

    const-string p0, "minutiae_object_picker_time_to_init"

    invoke-virtual {v0, v1, p0}, LX/92A;->b(ILjava/lang/String;)V

    .line 1429229
    goto :goto_0
.end method

.method private m()V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1429238
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    sget-object v1, LX/93E;->VERB_PICKER:LX/93E;

    if-ne v0, v1, :cond_1

    .line 1429239
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429240
    new-instance v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;-><init>()V

    .line 1429241
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1429242
    const-string v3, "minutiae_configuration"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1429243
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1429244
    move-object v1, v1

    .line 1429245
    :goto_0
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->l()V

    move-object v0, v1

    .line 1429246
    check-cast v0, LX/90u;

    invoke-direct {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(LX/90u;)V

    .line 1429247
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->n()V

    .line 1429248
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1429249
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 1429250
    const-string v2, "back_stack"

    invoke-virtual {v0, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v2

    const v3, 0x7f0400d6

    const v4, 0x7f0400e1

    const v5, 0x7f0400d5

    const v6, 0x7f0400e2

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0hH;->a(IIII)LX/0hH;

    .line 1429251
    :cond_0
    const v2, 0x7f0d002f

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    invoke-virtual {v3}, LX/93E;->getFragmentName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1429252
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1429253
    return-void

    .line 1429254
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429255
    new-instance v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;-><init>()V

    .line 1429256
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1429257
    const-string v3, "minutiae_configuration"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1429258
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1429259
    move-object v1, v1

    .line 1429260
    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 1429132
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->r:LX/935;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->C:LX/0h5;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-virtual {v0, v1, v2}, LX/935;->a(LX/0h5;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V

    .line 1429133
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1429230
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, LX/93E;->VERB_PICKER:LX/93E;

    invoke-virtual {v1}, LX/93E;->getFragmentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1429231
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/90u;

    if-eqz v1, :cond_0

    .line 1429232
    check-cast v0, LX/90u;

    invoke-direct {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(LX/90u;)V

    .line 1429233
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, LX/93E;->OBJECT_PICKER:LX/93E;

    invoke-virtual {v1}, LX/93E;->getFragmentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1429234
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/90u;

    if-eqz v1, :cond_1

    .line 1429235
    check-cast v0, LX/90u;

    invoke-direct {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(LX/90u;)V

    .line 1429236
    :cond_1
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->n()V

    .line 1429237
    return-void
.end method

.method public static p(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)LX/90u;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1428950
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1428951
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    invoke-virtual {v1}, LX/93E;->getFragmentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/90u;

    .line 1428952
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1428962
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1428963
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)LX/90u;

    move-result-object v0

    .line 1428964
    if-eqz v0, :cond_0

    .line 1428965
    invoke-interface {v0}, LX/90u;->a()V

    .line 1428966
    :cond_0
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->r()LX/93E;

    .line 1428967
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1428968
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1428969
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->s:LX/91G;

    sget-object v1, LX/91E;->HIT_BACK:LX/91E;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v2}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428970
    iget-object v4, v3, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v3, v4

    .line 1428971
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/91G;->a(LX/91E;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1428972
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(I)V

    .line 1428973
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    .line 1428974
    :goto_0
    return-void

    .line 1428975
    :cond_3
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->s()LX/93E;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    .line 1428976
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    .line 1428977
    iput-object v1, v0, LX/937;->o:LX/93E;

    .line 1428978
    move-object v0, v0

    .line 1428979
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428980
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->r:LX/935;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->C:LX/0h5;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-virtual {v0, v1, v2}, LX/935;->a(LX/0h5;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V

    goto :goto_0
.end method

.method private r()LX/93E;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1428953
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428954
    const/4 v0, 0x0

    .line 1428955
    :goto_0
    return-object v0

    .line 1428956
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93E;

    .line 1428957
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1428958
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    .line 1428959
    iput-object v2, v1, LX/937;->f:LX/0Px;

    .line 1428960
    move-object v1, v1

    .line 1428961
    invoke-virtual {v1}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    goto :goto_0
.end method

.method private s()LX/93E;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1428981
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428982
    const/4 v0, 0x0

    .line 1428983
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93E;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1428984
    const-string v0, "minutiae_tag_picker"

    return-object v0
.end method

.method public final a(LX/5LG;)V
    .locals 0

    .prologue
    .line 1428985
    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->b(LX/5LG;)V

    .line 1428986
    return-void
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;I)V
    .locals 3

    .prologue
    .line 1428987
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/925;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428988
    iget-object v0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v0

    .line 1428989
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v1, p1, v2}, LX/925;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0Px;)Landroid/content/Intent;

    move-result-object v1

    .line 1428990
    if-eqz v1, :cond_0

    .line 1428991
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1428992
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1428993
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428994
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1428995
    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428996
    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 1428997
    :goto_0
    return-void

    .line 1428998
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428999
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    move-object v0, v1

    .line 1429000
    sget-object v1, LX/939;->LAUNCH_COMPOSER:LX/939;

    if-ne v0, v1, :cond_1

    .line 1429001
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429002
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v1

    .line 1429003
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429004
    iget-object v3, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v2, v3

    .line 1429005
    invoke-virtual {v1, v2}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v1

    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1429006
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->x:LX/1Kf;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429007
    iget-object v3, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, v3

    .line 1429008
    const/16 v3, 0x4d8

    invoke-interface {v1, v2, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0

    .line 1429009
    :cond_1
    if-nez p2, :cond_2

    .line 1429010
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 1429011
    :cond_2
    const-string v0, "minutiae_object"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1429012
    invoke-direct {p0, p2}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->c(Landroid/content/Intent;)V

    .line 1429013
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p2}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1429014
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1429015
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1429016
    invoke-static {p0, p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1429017
    if-nez p1, :cond_2

    .line 1429018
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LX/93D;->a(Landroid/content/Intent;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429019
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->I:LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 1429020
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429021
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    move-object v1, v2

    .line 1429022
    sget-object v2, LX/939;->LAUNCH_COMPOSER:LX/939;

    if-ne v1, v2, :cond_3

    if-eqz v0, :cond_3

    .line 1429023
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->F:LX/9ju;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->K:LX/90X;

    .line 1429024
    iput-object v2, v1, LX/9ju;->j:LX/90X;

    .line 1429025
    new-instance v1, LX/9jo;

    invoke-direct {v1}, LX/9jo;-><init>()V

    const-string v2, ""

    .line 1429026
    iput-object v2, v1, LX/9jo;->a:Ljava/lang/String;

    .line 1429027
    move-object v1, v1

    .line 1429028
    sget-object v2, LX/9jG;->CHECKIN:LX/9jG;

    .line 1429029
    iput-object v2, v1, LX/9jo;->c:LX/9jG;

    .line 1429030
    move-object v1, v1

    .line 1429031
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    .line 1429032
    iput-object v0, v1, LX/9jo;->b:Landroid/location/Location;

    .line 1429033
    move-object v0, v1

    .line 1429034
    iput-boolean v3, v0, LX/9jo;->d:Z

    .line 1429035
    move-object v0, v0

    .line 1429036
    sget-object v1, LX/9jC;->Status:LX/9jC;

    .line 1429037
    iput-object v1, v0, LX/9jo;->g:LX/9jC;

    .line 1429038
    move-object v0, v0

    .line 1429039
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->F:LX/9ju;

    invoke-virtual {v1, v0, v3}, LX/9ju;->a(LX/9jo;Z)V

    .line 1429040
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429041
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1429042
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->E:Ljava/lang/String;

    .line 1429043
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->l()V

    .line 1429044
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429045
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v1

    .line 1429046
    sget-object v1, LX/93E;->VERB_PICKER:LX/93E;

    if-ne v0, v1, :cond_0

    .line 1429047
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92o;

    invoke-virtual {v0}, LX/92o;->a()V

    .line 1429048
    :cond_0
    const v0, 0x7f030333

    invoke-virtual {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setContentView(I)V

    .line 1429049
    const v0, 0x7f0d0aa4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/OverlayLayout;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->B:Lcom/facebook/widget/OverlayLayout;

    .line 1429050
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1429051
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->C:LX/0h5;

    .line 1429052
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->r:LX/935;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->C:LX/0h5;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-virtual {v0, v1, v2}, LX/935;->a(LX/0h5;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V

    .line 1429053
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->u:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->E:Ljava/lang/String;

    .line 1429054
    const-string v2, "activity_picker_started"

    invoke-static {v2, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1429055
    iget-object v3, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, v3

    .line 1429056
    iget-object v3, v0, LX/919;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429057
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1429058
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->s:LX/91G;

    sget-object v1, LX/91E;->SEEN:LX/91E;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v2}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429059
    iget-object v4, v3, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v3, v4

    .line 1429060
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/91G;->a(LX/91E;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1429061
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->r:LX/935;

    new-instance v1, LX/90V;

    invoke-direct {v1, p0}, LX/90V;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)V

    .line 1429062
    iput-object v1, v0, LX/935;->c:LX/90V;

    .line 1429063
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429064
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v1

    .line 1429065
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->t:LX/93E;

    .line 1429066
    if-nez p1, :cond_4

    .line 1429067
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->b()V

    .line 1429068
    :goto_2
    return-void

    .line 1429069
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->e(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1429070
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->G:LX/9jn;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->J:LX/0TF;

    invoke-virtual {v0, v1}, LX/9jn;->a(LX/0TF;)V

    goto/16 :goto_1

    .line 1429071
    :cond_4
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->o()V

    goto :goto_2
.end method

.method public final b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;I)V
    .locals 3

    .prologue
    .line 1429072
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/925;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429073
    iget-object v0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v0

    .line 1429074
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object v2

    .line 1429075
    const/4 v0, 0x1

    invoke-static {p0, v1, p1, v2, v0}, LX/925;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0Px;Z)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 1429076
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1429077
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1429078
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->D:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429079
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->D:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->e()V

    .line 1429080
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->D:LX/0am;

    .line 1429081
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 1429082
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1429083
    sparse-switch p1, :sswitch_data_0

    .line 1429084
    :cond_0
    :goto_0
    return-void

    .line 1429085
    :sswitch_0
    invoke-virtual {p0, p2, p3}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1429086
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    goto :goto_0

    .line 1429087
    :sswitch_1
    if-ne p2, v3, :cond_1

    .line 1429088
    invoke-virtual {p0, p2, p3}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1429089
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    goto :goto_0

    .line 1429090
    :cond_1
    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    const-string v1, "composer_cancelled"

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1429091
    invoke-virtual {p0, v0, p3}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1429092
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    goto :goto_0

    .line 1429093
    :sswitch_2
    if-ne p2, v3, :cond_0

    .line 1429094
    const-string v0, "minutiae_object"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {p0, v0, p3}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Landroid/content/Intent;)V

    goto :goto_0

    .line 1429095
    :sswitch_3
    if-eqz p2, :cond_0

    .line 1429096
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429097
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    move-object v1, v2

    .line 1429098
    sget-object v2, LX/939;->LAUNCH_COMPOSER:LX/939;

    if-ne v1, v2, :cond_4

    .line 1429099
    const-string v1, "extra_tagged_profiles"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1429100
    if-nez v3, :cond_3

    .line 1429101
    const/4 v0, 0x0

    move-object v2, v0

    .line 1429102
    :goto_1
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1429103
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429104
    iget-object v3, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v3

    .line 1429105
    invoke-static {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v1, "minutiae_object"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v3, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1429106
    if-eqz v2, :cond_2

    .line 1429107
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1429108
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->x:LX/1Kf;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429109
    iget-object v3, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, v3

    .line 1429110
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v3, 0x4d8

    invoke-interface {v1, v2, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto/16 :goto_0

    .line 1429111
    :cond_3
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1429112
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1429113
    iget-wide v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v6, v7}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1429114
    iput-object v6, v5, LX/5Rc;->b:Ljava/lang/String;

    .line 1429115
    move-object v5, v5

    .line 1429116
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1429117
    iput-object v0, v5, LX/5Rc;->c:Ljava/lang/String;

    .line 1429118
    move-object v0, v5

    .line 1429119
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1429120
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1429121
    :cond_4
    invoke-virtual {p0, v3, p3}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1429122
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    goto/16 :goto_0

    :cond_5
    move-object v2, v1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x3f2 -> :sswitch_1
        0x3f3 -> :sswitch_2
        0x3f4 -> :sswitch_3
        0x4d8 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 1429123
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q()V

    .line 1429124
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1429125
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1429126
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1429127
    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->d(Landroid/os/Bundle;)V

    .line 1429128
    return-void
.end method

.method public final setIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1429129
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->setIntent(Landroid/content/Intent;)V

    .line 1429130
    invoke-static {p1}, LX/93D;->a(Landroid/content/Intent;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429131
    return-void
.end method
