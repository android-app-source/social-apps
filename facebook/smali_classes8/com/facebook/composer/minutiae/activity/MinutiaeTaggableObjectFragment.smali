.class public Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/90u;


# instance fields
.field private A:LX/91I;

.field public a:LX/90Z;

.field public b:LX/90b;

.field public c:LX/92Y;

.field public d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/90t;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/03V;

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public g:LX/0Zb;

.field private h:LX/92X;

.field public i:LX/92t;

.field public j:LX/919;

.field public k:LX/92C;

.field public l:LX/0y2;

.field public m:Lcom/facebook/widget/listview/BetterListView;

.field public n:Landroid/widget/ProgressBar;

.field public o:Lcom/facebook/widget/CustomLinearLayout;

.field public p:Lcom/facebook/ui/search/SearchEditText;

.field public q:LX/4nw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4nw",
            "<",
            "Lcom/facebook/widget/error/GenericErrorView;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:LX/90G;

.field public t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

.field public u:LX/90j;

.field public v:LX/5LG;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1429789
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1429790
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;Ljava/lang/String;LX/0ta;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1429737
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->o:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v0, :cond_0

    .line 1429738
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->o:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1429739
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_5

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1429740
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1429741
    :goto_1
    const/4 v3, 0x0

    .line 1429742
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v0}, LX/5LG;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1429743
    new-instance v0, LX/5LL;

    invoke-direct {v0}, LX/5LL;-><init>()V

    .line 1429744
    iput-object p2, v0, LX/5LL;->d:Ljava/lang/String;

    .line 1429745
    move-object v0, v0

    .line 1429746
    iput-boolean v2, v0, LX/5LL;->h:Z

    .line 1429747
    move-object v0, v0

    .line 1429748
    new-instance v3, LX/4aM;

    invoke-direct {v3}, LX/4aM;-><init>()V

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v4}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1429749
    iput-object v4, v3, LX/4aM;->b:Ljava/lang/String;

    .line 1429750
    move-object v3, v3

    .line 1429751
    invoke-virtual {v3}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    .line 1429752
    iput-object v3, v0, LX/5LL;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1429753
    move-object v0, v0

    .line 1429754
    new-instance v3, LX/5Lc;

    invoke-direct {v3}, LX/5Lc;-><init>()V

    .line 1429755
    iput-object p2, v3, LX/5Lc;->d:Ljava/lang/String;

    .line 1429756
    move-object v3, v3

    .line 1429757
    iput-boolean v2, v3, LX/5Lc;->c:Z

    .line 1429758
    move-object v3, v3

    .line 1429759
    new-instance v4, LX/4aM;

    invoke-direct {v4}, LX/4aM;-><init>()V

    iget-object v5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v5}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1429760
    iput-object v5, v4, LX/4aM;->b:Ljava/lang/String;

    .line 1429761
    move-object v4, v4

    .line 1429762
    invoke-virtual {v4}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    .line 1429763
    iput-object v4, v3, LX/5Lc;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1429764
    move-object v3, v3

    .line 1429765
    invoke-virtual {v3}, LX/5Lc;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    .line 1429766
    iput-object v3, v0, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 1429767
    move-object v0, v0

    .line 1429768
    invoke-virtual {v0}, LX/5LL;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v3

    .line 1429769
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v3, :cond_2

    .line 1429770
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    const v4, 0x7f08146e

    invoke-virtual {v0, v4}, Lcom/facebook/widget/error/GenericErrorView;->setCustomErrorMessage(I)V

    .line 1429771
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->d(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429772
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->m:Lcom/facebook/widget/listview/BetterListView;

    new-instance v4, LX/90s;

    invoke-direct {v4, p0, p3}, LX/90s;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;LX/0ta;)V

    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/BetterListView;->setOnDrawListenerTo(LX/0fu;)V

    .line 1429773
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    .line 1429774
    iput-object v4, v0, LX/90j;->b:LX/5LG;

    .line 1429775
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v0}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v0}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v0

    const-string v4, "668012816568345"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v5, v1

    .line 1429776
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    move-object v1, p2

    move-object v2, p1

    move-object v4, p4

    .line 1429777
    iget-object p0, v0, LX/90j;->h:LX/5Lv;

    .line 1429778
    iget-object p1, p0, LX/5Lv;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 1429779
    iget-object p1, p0, LX/5Lv;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 1429780
    invoke-virtual {p0, v2, v4}, LX/5Lv;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;Ljava/lang/String;)V

    .line 1429781
    iput-object v1, v0, LX/90j;->m:Ljava/lang/String;

    .line 1429782
    iput-object v3, v0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1429783
    iget-object p0, v0, LX/90j;->f:LX/93N;

    .line 1429784
    iput-boolean v5, p0, LX/93N;->a:Z

    .line 1429785
    const p0, -0x1b1bea6b

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1429786
    return-void

    .line 1429787
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_4
    move v5, v2

    .line 1429788
    goto :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_1
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V
    .locals 3
    .param p0    # Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1429726
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    .line 1429727
    const v1, 0x420002

    const-string v2, "minutiae_object_picker_time_to_fetch_start"

    invoke-virtual {v0, v1, v2}, LX/92A;->b(ILjava/lang/String;)V

    .line 1429728
    const v1, 0x420005

    const-string v2, "minutiae_object_picker_fetch_time"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 1429729
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1429730
    invoke-static {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->c$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    .line 1429731
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1429732
    :cond_0
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    .line 1429733
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->A:LX/91I;

    invoke-virtual {v0}, LX/91I;->a()LX/91I;

    .line 1429734
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->A:LX/91I;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/91I;->a(Ljava/lang/String;)LX/91I;

    .line 1429735
    :cond_1
    return-void

    .line 1429736
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1429722
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    .line 1429723
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->d(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429724
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->e:LX/03V;

    const-string v1, "minutiae_taggable_object_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1429725
    return-void
.end method

.method public static b$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V
    .locals 4
    .param p0    # Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1429716
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    if-eqz v0, :cond_0

    .line 1429717
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->c()V

    .line 1429718
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1429719
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k()V

    .line 1429720
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->h:LX/92X;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    new-instance v3, LX/90o;

    invoke-direct {v3, p0, v0}, LX/90o;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1, v0, v3}, LX/92X;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)V

    .line 1429721
    return-void
.end method

.method private static c(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 2

    .prologue
    .line 1429713
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->A:LX/91I;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->g:LX/0Zb;

    if-eqz v0, :cond_0

    .line 1429714
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->g:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->A:LX/91I;

    invoke-virtual {v1}, LX/91I;->b()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429715
    :cond_0
    return-void
.end method

.method public static c$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1429687
    new-instance v0, LX/92O;

    invoke-direct {v0}, LX/92O;-><init>()V

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1429688
    iput-object v1, v0, LX/92O;->a:Ljava/lang/String;

    .line 1429689
    move-object v0, v0

    .line 1429690
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429691
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 1429692
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    .line 1429693
    iput-object v1, v0, LX/92O;->b:Ljava/lang/String;

    .line 1429694
    move-object v0, v0

    .line 1429695
    iput-object p1, v0, LX/92O;->c:Ljava/lang/String;

    .line 1429696
    move-object v0, v0

    .line 1429697
    iget v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    .line 1429698
    iput v1, v0, LX/92O;->d:I

    .line 1429699
    move-object v0, v0

    .line 1429700
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1429701
    iput-object v1, v0, LX/92O;->e:Ljava/lang/String;

    .line 1429702
    move-object v0, v0

    .line 1429703
    const-string v1, "place"

    .line 1429704
    iput-object v1, v0, LX/92O;->f:Ljava/lang/String;

    .line 1429705
    move-object v0, v0

    .line 1429706
    const/4 v1, 0x0

    .line 1429707
    iput-object v1, v0, LX/92O;->g:Landroid/location/Location;

    .line 1429708
    move-object v0, v0

    .line 1429709
    invoke-virtual {v0}, LX/92O;->a()LX/92P;

    move-result-object v0

    .line 1429710
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k()V

    .line 1429711
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->d:LX/1Ck;

    sget-object v2, LX/90t;->SEARCH:LX/90t;

    new-instance v3, LX/90p;

    invoke-direct {v3, p0, v0}, LX/90p;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;LX/92P;)V

    new-instance v4, LX/90q;

    invoke-direct {v4, p0, v0}, LX/90q;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;LX/92P;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1429712
    return-void
.end method

.method public static d(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 2

    .prologue
    .line 1429683
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->d()V

    .line 1429684
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setClickable(Z)V

    .line 1429685
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    new-instance v1, LX/90r;

    invoke-direct {v1, p0}, LX/90r;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429686
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1429680
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1429681
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->n:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1429682
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 2

    .prologue
    .line 1429677
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1429678
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1429679
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 1429666
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429667
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429668
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v3}, LX/90j;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v4}, LX/90j;->d()LX/0Px;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429669
    iget v6, v5, LX/90G;->a:I

    move v5, v6

    .line 1429670
    iget-object v6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    if-nez v6, :cond_0

    const-string v6, ""

    :goto_0
    iget v7, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v8}, LX/90j;->c()Ljava/lang/String;

    move-result-object v8

    .line 1429671
    const-string v9, "activity_picker_tapped_back"

    invoke-static {v9, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/8yQ;->b(I)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v6}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    .line 1429672
    iget-object v10, v9, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v9, v10

    .line 1429673
    iget-object v10, v0, LX/919;->a:LX/0Zb;

    invoke-interface {v10, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429674
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->c(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429675
    return-void

    .line 1429676
    :cond_0
    iget-object v6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(LX/90b;)V
    .locals 0

    .prologue
    .line 1429523
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b:LX/90b;

    .line 1429524
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1429625
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1429626
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/92t;->a(LX/0QB;)LX/92t;

    move-result-object v4

    check-cast v4, LX/92t;

    const-class v5, LX/92Y;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/92Y;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/919;->a(LX/0QB;)LX/919;

    move-result-object v9

    check-cast v9, LX/919;

    invoke-static {v0}, LX/92C;->a(LX/0QB;)LX/92C;

    move-result-object v10

    check-cast v10, LX/92C;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v0

    check-cast v0, LX/0y2;

    iput-object v3, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->d:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->i:LX/92t;

    iput-object v6, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->e:LX/03V;

    iput-object v7, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v8, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->g:LX/0Zb;

    iput-object v5, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->c:LX/92Y;

    iput-object v9, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iput-object v10, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    iput-object v0, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->l:LX/0y2;

    .line 1429627
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1429628
    const-string v1, "minutiae_configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429629
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429630
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object v0, v1

    .line 1429631
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    .line 1429632
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    .line 1429633
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->w:Ljava/lang/String;

    .line 1429634
    new-instance v0, LX/91I;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429635
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429636
    invoke-direct {v0, v1}, LX/91I;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->A:LX/91I;

    .line 1429637
    if-eqz p1, :cond_3

    .line 1429638
    const-string v0, "search_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    .line 1429639
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    .line 1429640
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429641
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1429642
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429643
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1429644
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    .line 1429645
    :goto_3
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->c:LX/92Y;

    iget v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429646
    iget-object v4, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v1, v4

    .line 1429647
    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429648
    iget-object v4, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v1, v4

    .line 1429649
    :goto_4
    invoke-virtual {v2, v3, v0, v1}, LX/92Y;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)LX/92X;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->h:LX/92X;

    .line 1429650
    new-instance v0, LX/90j;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->h:LX/92X;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->e:LX/03V;

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, LX/90j;-><init>(Landroid/content/Context;LX/92X;LX/03V;LX/92C;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    .line 1429651
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "composer_location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1429652
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->h:LX/92X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "composer_location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 1429653
    iput-object v0, v1, LX/92X;->g:Landroid/location/Location;

    .line 1429654
    :cond_0
    :goto_5
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    .line 1429655
    return-void

    .line 1429656
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v0}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1429657
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v0}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1429658
    :cond_3
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    goto/16 :goto_2

    .line 1429659
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 1429660
    :cond_5
    const-string v1, "composer"

    goto :goto_4

    .line 1429661
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->l:LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 1429662
    if-eqz v0, :cond_0

    .line 1429663
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->h:LX/92X;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    .line 1429664
    iput-object v0, v1, LX/92X;->g:Landroid/location/Location;

    .line 1429665
    goto :goto_5
.end method

.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 1429584
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429585
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429586
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v4}, LX/90j;->b()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v5}, LX/90j;->d()LX/0Px;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429587
    iget v7, v6, LX/90G;->a:I

    move v6, v7

    .line 1429588
    iget-object v7, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v7, p1}, LX/90j;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)I

    move-result v7

    iget-object v8, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    if-nez v8, :cond_4

    const-string v8, ""

    :goto_0
    iget v9, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    .line 1429589
    iget-object v11, v10, LX/90j;->h:LX/5Lv;

    invoke-virtual {v11, p1}, LX/5Lv;->b(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Ljava/lang/String;

    move-result-object v11

    move-object v10, v11

    .line 1429590
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ag_()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v0 .. v11}, LX/919;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Px;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1429591
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->A:LX/91I;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v1, p1}, LX/90j;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/91I;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;I)LX/91I;

    .line 1429592
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1429593
    :goto_1
    new-instance v1, LX/2s1;

    invoke-direct {v1}, LX/2s1;-><init>()V

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    .line 1429594
    iput-object v2, v1, LX/2s1;->a:LX/5LG;

    .line 1429595
    move-object v1, v1

    .line 1429596
    iput-object p1, v1, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1429597
    move-object v1, v1

    .line 1429598
    iput-object v0, v1, LX/2s1;->d:Ljava/lang/String;

    .line 1429599
    move-object v0, v1

    .line 1429600
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    move-result-object v1

    .line 1429601
    iput-object v1, v0, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 1429602
    move-object v0, v0

    .line 1429603
    invoke-virtual {v0}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 1429604
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->c(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429605
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1429606
    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v3, p1}, LX/90j;->b(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Z

    move-result v4

    .line 1429607
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v2

    .line 1429608
    :goto_2
    if-nez v4, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    move v1, v1

    .line 1429609
    if-eqz v1, :cond_6

    .line 1429610
    const/16 v2, 0x3f3

    .line 1429611
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b:LX/90b;

    if-eqz v1, :cond_2

    .line 1429612
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1429613
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b:LX/90b;

    invoke-interface {v1, v0, v2}, LX/90b;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;I)V

    .line 1429614
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    if-eqz v0, :cond_3

    .line 1429615
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    .line 1429616
    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 1429617
    :cond_3
    return-void

    .line 1429618
    :cond_4
    iget-object v8, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    move-object v0, v12

    .line 1429619
    goto :goto_1

    .line 1429620
    :cond_6
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a:LX/90Z;

    if-eqz v1, :cond_7

    .line 1429621
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a:LX/90Z;

    invoke-interface {v1, v0, v12}, LX/90Z;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Landroid/content/Intent;)V

    .line 1429622
    :cond_7
    goto :goto_3

    :cond_8
    move v3, v1

    .line 1429623
    goto :goto_2

    .line 1429624
    :cond_9
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b:LX/90b;

    invoke-interface {v1, v0, v2}, LX/90b;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;I)V

    goto :goto_3
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1429577
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429578
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429579
    iget v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v3}, LX/90j;->c()Ljava/lang/String;

    move-result-object v3

    .line 1429580
    const-string v4, "activity_picker_object_skipped"

    invoke-static {v4, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    .line 1429581
    iget-object p0, v4, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, p0

    .line 1429582
    iget-object p0, v0, LX/919;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429583
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x138516c9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1429554
    new-instance v0, LX/90G;

    invoke-direct {v0}, LX/90G;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429555
    const v0, 0x7f03033a

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1429556
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->m:Lcom/facebook/widget/listview/BetterListView;

    .line 1429557
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->m:Lcom/facebook/widget/listview/BetterListView;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1429558
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->m:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/90k;

    invoke-direct {v3, p0}, LX/90k;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1429559
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->m:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1429560
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429561
    iget-boolean v3, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    move v0, v3

    .line 1429562
    if-nez v0, :cond_1

    .line 1429563
    const v0, 0x7f0d0aaf

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1429564
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1429565
    const v0, 0x7f0d0ab1

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->o:Lcom/facebook/widget/CustomLinearLayout;

    .line 1429566
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->o:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1429567
    const v0, 0x7f0d0ab7

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->n:Landroid/widget/ProgressBar;

    .line 1429568
    const v0, 0x7f0d0ab6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    .line 1429569
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    new-instance v3, LX/90l;

    invoke-direct {v3, p0}, LX/90l;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1429570
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    new-instance v3, LX/90m;

    invoke-direct {v3, p0}, LX/90m;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1429571
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1429572
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1429573
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->w:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1429574
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->o:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v3, LX/90n;

    invoke-direct {v3, p0}, LX/90n;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 1429575
    :cond_1
    const v0, 0x7f0d0ab2

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/4nv;

    invoke-static {v0}, LX/4nw;->a(LX/4nv;)LX/4nw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    .line 1429576
    const/16 v0, 0x2b

    const v3, 0x73391d50

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x2dbd6446

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429549
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->e:LX/03V;

    .line 1429550
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a:LX/90Z;

    .line 1429551
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b:LX/90b;

    .line 1429552
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1429553
    const/16 v1, 0x2b

    const v2, 0x6ed1e0aa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x79c75245

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429543
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->m:Lcom/facebook/widget/listview/BetterListView;

    .line 1429544
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1429545
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->h:LX/92X;

    .line 1429546
    iget-object v2, v1, LX/92X;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1429547
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1429548
    const/16 v1, 0x2b

    const v2, -0x5932e41b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6aab25b7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429533
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1429534
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    .line 1429535
    const v2, 0x420001

    const-string p0, "minutiae_object_picker_time_to_init"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429536
    const v2, 0x420002

    const-string p0, "minutiae_object_picker_time_to_fetch_start"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429537
    const v2, 0x420003

    const-string p0, "minutiae_object_picker_time_to_fetch_end"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429538
    const v2, 0x420004

    const-string p0, "minutiae_object_picker_time_to_results_shown"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429539
    const v2, 0x420005

    const-string p0, "minutiae_object_picker_fetch_time"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429540
    const v2, 0x420006

    const-string p0, "minutiae_object_picker_rendering_time"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429541
    const v2, 0x42000b

    const-string p0, "minutiae_object_picker_time_to_scroll_load"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429542
    const/16 v1, 0x2b

    const v2, -0x38dbe1cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7b1a58de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429529
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1429530
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1429531
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 1429532
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3f8eb619

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1429525
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1429526
    const-string v0, "search_query"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429527
    const-string v0, "session_id"

    iget v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1429528
    return-void
.end method
