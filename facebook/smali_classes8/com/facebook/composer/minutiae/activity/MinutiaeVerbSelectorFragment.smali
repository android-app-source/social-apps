.class public Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/90F;


# instance fields
.field public a:LX/90T;

.field public b:Lcom/facebook/components/ComponentView;

.field public c:LX/8zj;

.field public d:LX/8zs;

.field public e:Ljava/lang/String;

.field public f:LX/1dV;

.field public g:LX/1De;

.field public final h:LX/903;

.field public final i:LX/8zz;

.field public j:LX/8zk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/8zt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/8zw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/925;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/918;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/92D;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1430116
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1430117
    new-instance v0, LX/914;

    invoke-direct {v0, p0}, LX/914;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)V

    move-object v0, v0

    .line 1430118
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->h:LX/903;

    .line 1430119
    new-instance v0, LX/910;

    invoke-direct {v0, p0}, LX/910;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->i:LX/8zz;

    .line 1430120
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1430121
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->n:LX/0Ot;

    .line 1430122
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1430123
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->p:LX/0Ot;

    .line 1430124
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;LX/8zx;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 1430111
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->f:LX/1dV;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->l:LX/8zw;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->g:LX/1De;

    invoke-virtual {v1, v2}, LX/8zw;->c(LX/1De;)LX/8zu;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->c:LX/8zj;

    invoke-virtual {v1, v2}, LX/8zu;->a(LX/8zj;)LX/8zu;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/8zu;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/8zu;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/8zu;->b(Ljava/lang/String;)LX/8zu;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->i:LX/8zz;

    invoke-virtual {v1, v2}, LX/8zu;->a(LX/8zz;)LX/8zu;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/8zu;->a(LX/8zx;)LX/8zu;

    move-result-object v1

    .line 1430112
    iget-object v2, v1, LX/8zu;->a:LX/8zv;

    iput-object p2, v2, LX/8zv;->d:Landroid/view/View$OnClickListener;

    .line 1430113
    move-object v1, v1

    .line 1430114
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1dV;->b(LX/1X1;)V

    .line 1430115
    return-void
.end method

.method public static e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
    .locals 1

    .prologue
    .line 1430110
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->a:LX/90T;

    invoke-interface {v0}, LX/90T;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1430104
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->o:LX/918;

    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1430105
    iget-object p0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, p0

    .line 1430106
    const-string v2, "activities_selector_tapped_cancel"

    invoke-static {v2, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1430107
    iget-object p0, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, p0

    .line 1430108
    iget-object p0, v0, LX/918;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430109
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1430125
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1430126
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    const-class v3, LX/8zk;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8zk;

    const-class v4, LX/8zt;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/8zt;

    invoke-static {p1}, LX/8zw;->a(LX/0QB;)LX/8zw;

    move-result-object v5

    check-cast v5, LX/8zw;

    invoke-static {p1}, LX/925;->a(LX/0QB;)LX/925;

    move-result-object v6

    check-cast v6, LX/925;

    const/16 v7, 0x455

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p1}, LX/918;->a(LX/0QB;)LX/918;

    move-result-object v8

    check-cast v8, LX/918;

    const/16 v0, 0x19ad

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->j:LX/8zk;

    iput-object v4, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->k:LX/8zt;

    iput-object v5, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->l:LX/8zw;

    iput-object v6, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->m:LX/925;

    iput-object v7, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->n:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->o:LX/918;

    iput-object p1, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->p:LX/0Ot;

    .line 1430127
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, -0x1

    .line 1430098
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1430099
    const/16 v0, 0xb

    if-ne p1, v0, :cond_0

    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    .line 1430100
    const-string v0, "minutiae_object"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1430101
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "minutiae_object"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1430102
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1430103
    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1430059
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1430060
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/90T;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1430061
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/90T;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->a:LX/90T;

    .line 1430062
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1d8bab1a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1430097
    const v1, 0x7f031464

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x11b40cc7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1430071
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1430072
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1430073
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1430074
    new-instance v2, LX/62T;

    const/4 v3, 0x2

    invoke-direct {v2, v0, v3}, LX/62T;-><init>(Landroid/content/Context;I)V

    .line 1430075
    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->j:LX/8zk;

    new-instance v4, LX/915;

    invoke-direct {v4, p0}, LX/915;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)V

    .line 1430076
    new-instance v7, LX/8zj;

    invoke-static {v3}, LX/8zn;->a(LX/0QB;)LX/8zn;

    move-result-object v5

    check-cast v5, LX/8zn;

    const-class v6, Landroid/content/Context;

    invoke-interface {v3, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {v7, v2, v4, v5, v6}, LX/8zj;-><init>(LX/62T;LX/915;LX/8zn;Landroid/content/Context;)V

    .line 1430077
    move-object v2, v7

    .line 1430078
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->c:LX/8zj;

    .line 1430079
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->k:LX/8zt;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->c:LX/8zj;

    .line 1430080
    iget-object v4, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v4, v4

    .line 1430081
    new-instance v5, LX/913;

    invoke-direct {v5, p0}, LX/913;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)V

    move-object v5, v5

    .line 1430082
    new-instance v6, LX/8zs;

    invoke-static {v2}, LX/92o;->a(LX/0QB;)LX/92o;

    move-result-object v10

    check-cast v10, LX/92o;

    invoke-static {v2}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v11

    check-cast v11, LX/0W9;

    invoke-static {v2}, LX/918;->a(LX/0QB;)LX/918;

    move-result-object v12

    check-cast v12, LX/918;

    move-object v7, v3

    move-object v8, v4

    move-object v9, v5

    invoke-direct/range {v6 .. v12}, LX/8zs;-><init>(LX/8zj;Ljava/lang/String;LX/909;LX/92o;LX/0W9;LX/918;)V

    .line 1430083
    move-object v2, v6

    .line 1430084
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->d:LX/8zs;

    .line 1430085
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->d:LX/8zs;

    invoke-virtual {v2}, LX/8zs;->a()V

    .line 1430086
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081423

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e:Ljava/lang/String;

    .line 1430087
    new-instance v2, LX/1De;

    invoke-direct {v2, v0}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->g:LX/1De;

    .line 1430088
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->g:LX/1De;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->l:LX/8zw;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->g:LX/1De;

    invoke-virtual {v2, v3}, LX/8zw;->c(LX/1De;)LX/8zu;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->c:LX/8zj;

    invoke-virtual {v2, v3}, LX/8zu;->a(LX/8zj;)LX/8zu;

    move-result-object v2

    .line 1430089
    iget-object v3, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v3

    .line 1430090
    invoke-virtual {v2, v1}, LX/8zu;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/8zu;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/8zu;->b(Ljava/lang/String;)LX/8zu;

    move-result-object v1

    sget-object v2, LX/8zx;->NORMAL:LX/8zx;

    invoke-virtual {v1, v2}, LX/8zu;->a(LX/8zx;)LX/8zu;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->h:LX/903;

    .line 1430091
    iget-object v3, v1, LX/8zu;->a:LX/8zv;

    iput-object v2, v3, LX/8zv;->f:LX/903;

    .line 1430092
    move-object v1, v1

    .line 1430093
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->i:LX/8zz;

    invoke-virtual {v1, v2}, LX/8zu;->a(LX/8zz;)LX/8zu;

    move-result-object v1

    invoke-static {v0, v1}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->f:LX/1dV;

    .line 1430094
    const v0, 0x7f0d2e70

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->b:Lcom/facebook/components/ComponentView;

    .line 1430095
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->b:Lcom/facebook/components/ComponentView;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->f:LX/1dV;

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1430096
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 1430063
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 1430064
    if-eqz p1, :cond_0

    .line 1430065
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->o:LX/918;

    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1430066
    iget-object p0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, p0

    .line 1430067
    const-string p0, "activities_selector_focused"

    invoke-static {p0, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    .line 1430068
    iget-object p1, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p0, p1

    .line 1430069
    iget-object p1, v0, LX/918;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430070
    :cond_0
    return-void
.end method
