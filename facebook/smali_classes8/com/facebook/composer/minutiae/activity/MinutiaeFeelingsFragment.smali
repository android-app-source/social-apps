.class public Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/90F;


# instance fields
.field public a:LX/90T;

.field private b:Lcom/facebook/components/ComponentView;

.field public c:LX/91v;

.field private d:LX/1dV;

.field private e:LX/1De;

.field public f:LX/91k;

.field private g:LX/1OX;

.field public h:LX/90G;

.field private i:Ljava/lang/String;

.field private final j:LX/903;

.field private final k:LX/8zz;

.field public l:I

.field public m:LX/91l;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/91w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/91f;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/925;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/91D;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/92B;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1428555
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1428556
    new-instance v0, LX/904;

    invoke-direct {v0, p0}, LX/904;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V

    move-object v0, v0

    .line 1428557
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->j:LX/903;

    .line 1428558
    new-instance v0, LX/900;

    invoke-direct {v0, p0}, LX/900;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->k:LX/8zz;

    .line 1428559
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;LX/91g;Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    .line 1428560
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1428561
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->d:LX/1dV;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->o:LX/91f;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->e:LX/1De;

    invoke-virtual {v2, v3}, LX/91f;->c(LX/1De;)LX/91d;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    invoke-virtual {v2, v3}, LX/91d;->a(LX/91k;)LX/91d;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->g:LX/1OX;

    invoke-virtual {v2, v3}, LX/91d;->a(LX/1OX;)LX/91d;

    move-result-object v2

    .line 1428562
    iget-boolean v3, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    move v3, v3

    .line 1428563
    if-eqz v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, LX/91d;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/91d;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->j:LX/903;

    invoke-virtual {v0, v2}, LX/91d;->a(LX/903;)LX/91d;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/91d;->b(Ljava/lang/String;)LX/91d;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->k:LX/8zz;

    invoke-virtual {v0, v2}, LX/91d;->a(LX/8zz;)LX/91d;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/91d;->a(LX/91g;)LX/91d;

    move-result-object v0

    .line 1428564
    iget-object v2, v0, LX/91d;->a:LX/91e;

    iput-object p2, v2, LX/91e;->d:Landroid/view/View$OnClickListener;

    .line 1428565
    move-object v0, v0

    .line 1428566
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1dV;->b(LX/1X1;)V

    .line 1428567
    return-void

    .line 1428568
    :cond_0
    iget-object v3, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v3

    .line 1428569
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 4

    .prologue
    .line 1428549
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v2

    .line 1428550
    iget-object v0, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, v0

    .line 1428551
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object v3

    invoke-static {v1, v2, p1, v3}, LX/925;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0Px;)Landroid/content/Intent;

    move-result-object v0

    .line 1428552
    if-eqz v0, :cond_0

    .line 1428553
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->q:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1428554
    :cond_0
    return-void
.end method

.method private b()V
    .locals 14

    .prologue
    .line 1428512
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 1428513
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v2

    .line 1428514
    const v0, 0x7f0d2e70

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->b:Lcom/facebook/components/ComponentView;

    .line 1428515
    new-instance v0, LX/901;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v3, 0x2

    invoke-direct {v0, p0, v1, v3}, LX/901;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;Landroid/content/Context;I)V

    move-object v7, v0

    .line 1428516
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->m:LX/91l;

    new-instance v1, LX/90C;

    invoke-direct {v1, p0}, LX/90C;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V

    new-instance v3, LX/90E;

    invoke-direct {v3, p0}, LX/90E;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V

    .line 1428517
    new-instance v8, LX/91k;

    invoke-static {v0}, LX/91o;->a(LX/0QB;)LX/91o;

    move-result-object v12

    check-cast v12, LX/91o;

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Context;

    move-object v9, v7

    move-object v10, v1

    move-object v11, v3

    invoke-direct/range {v8 .. v13}, LX/91k;-><init>(LX/62T;LX/90B;LX/90D;LX/91o;Landroid/content/Context;)V

    .line 1428518
    move-object v0, v8

    .line 1428519
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    .line 1428520
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->n:LX/91w;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    .line 1428521
    new-instance v3, LX/906;

    invoke-direct {v3, p0}, LX/906;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V

    move-object v3, v3

    .line 1428522
    new-instance v4, LX/90A;

    invoke-direct {v4, p0}, LX/90A;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V

    move-object v4, v4

    .line 1428523
    iget v5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l:I

    invoke-virtual/range {v0 .. v5}, LX/91w;->a(LX/91k;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;LX/905;LX/909;I)LX/91v;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    .line 1428524
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    invoke-virtual {v0}, LX/91v;->a()V

    .line 1428525
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    .line 1428526
    iget-object v1, v0, LX/91v;->g:LX/5LG;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/91v;->g:LX/5LG;

    invoke-interface {v1}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1428527
    iget-object v1, v0, LX/91v;->g:LX/5LG;

    invoke-interface {v1}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v1

    .line 1428528
    :goto_0
    move-object v0, v1

    .line 1428529
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->i:Ljava/lang/String;

    .line 1428530
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->i:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->i:Ljava/lang/String;

    .line 1428531
    new-instance v0, LX/1De;

    invoke-direct {v0, v6}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->e:LX/1De;

    .line 1428532
    new-instance v0, LX/902;

    invoke-direct {v0, p0, v7}, LX/902;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;LX/62T;)V

    move-object v0, v0

    .line 1428533
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->g:LX/1OX;

    .line 1428534
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->e:LX/1De;

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->o:LX/91f;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->e:LX/1De;

    invoke-virtual {v0, v3}, LX/91f;->c(LX/1De;)LX/91d;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    invoke-virtual {v0, v3}, LX/91d;->a(LX/91k;)LX/91d;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->g:LX/1OX;

    invoke-virtual {v0, v3}, LX/91d;->a(LX/1OX;)LX/91d;

    move-result-object v3

    .line 1428535
    iget-boolean v0, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    move v0, v0

    .line 1428536
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v3, v0}, LX/91d;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/91d;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->j:LX/903;

    invoke-virtual {v0, v2}, LX/91d;->a(LX/903;)LX/91d;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/91d;->b(Ljava/lang/String;)LX/91d;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->k:LX/8zz;

    invoke-virtual {v0, v2}, LX/91d;->a(LX/8zz;)LX/91d;

    move-result-object v0

    sget-object v2, LX/91g;->LOADED:LX/91g;

    invoke-virtual {v0, v2}, LX/91d;->a(LX/91g;)LX/91d;

    move-result-object v0

    invoke-static {v1, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    const/4 v1, 0x1

    .line 1428537
    iput-boolean v1, v0, LX/1me;->d:Z

    .line 1428538
    move-object v0, v0

    .line 1428539
    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->d:LX/1dV;

    .line 1428540
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->b:Lcom/facebook/components/ComponentView;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->d:LX/1dV;

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1428541
    return-void

    .line 1428542
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081423

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1428543
    :cond_1
    iget-object v0, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v0

    .line 1428544
    goto :goto_2

    .line 1428545
    :cond_2
    iget-object v1, v0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    .line 1428546
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1428547
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1428548
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
    .locals 1

    .prologue
    .line 1428511
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->a:LX/90T;

    invoke-interface {v0}, LX/90T;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 1428499
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->r:LX/91D;

    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1428500
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428501
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    .line 1428502
    iget-object v3, v2, LX/91v;->g:LX/5LG;

    move-object v2, v3

    .line 1428503
    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    invoke-virtual {v3}, LX/91k;->e()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    invoke-virtual {v4}, LX/91k;->m()LX/0Px;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    .line 1428504
    iget v6, v5, LX/90G;->a:I

    move v5, v6

    .line 1428505
    iget-object v6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    invoke-virtual {v6}, LX/91v;->d()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    .line 1428506
    iget-object p0, v8, LX/91v;->q:Ljava/lang/String;

    move-object v8, p0

    .line 1428507
    const-string v9, "feeling_selector_tapped_back"

    invoke-static {v9, v1}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/8yQ;->b(I)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v6}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    .line 1428508
    iget-object p0, v9, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v9, p0

    .line 1428509
    iget-object p0, v0, LX/91D;->a:LX/0Zb;

    invoke-interface {p0, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428510
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1428570
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1428571
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    const-class v3, LX/91l;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/91l;

    const-class v4, LX/91w;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/91w;

    invoke-static {v0}, LX/91f;->a(LX/0QB;)LX/91f;

    move-result-object v5

    check-cast v5, LX/91f;

    invoke-static {v0}, LX/925;->a(LX/0QB;)LX/925;

    move-result-object v6

    check-cast v6, LX/925;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/91D;->a(LX/0QB;)LX/91D;

    move-result-object v8

    check-cast v8, LX/91D;

    invoke-static {v0}, LX/92B;->a(LX/0QB;)LX/92B;

    move-result-object v0

    check-cast v0, LX/92B;

    iput-object v3, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->m:LX/91l;

    iput-object v4, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->n:LX/91w;

    iput-object v5, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->o:LX/91f;

    iput-object v6, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->p:LX/925;

    iput-object v7, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->q:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->r:LX/91D;

    iput-object v0, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->s:LX/92B;

    .line 1428572
    if-eqz p1, :cond_0

    .line 1428573
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l:I

    .line 1428574
    :goto_0
    return-void

    .line 1428575
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l:I

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1428494
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1428495
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 1428496
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1428497
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1428498
    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1428490
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1428491
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/90T;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1428492
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/90T;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->a:LX/90T;

    .line 1428493
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x700e7340

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1428488
    new-instance v1, LX/90G;

    invoke-direct {v1}, LX/90G;-><init>()V

    iput-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    .line 1428489
    const v1, 0x7f031464

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x68c91298

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3945fe33

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1428479
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1428480
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->s:LX/92B;

    .line 1428481
    const v2, 0xc50001

    const-string p0, "minutiae_feelings_selector_time_to_init"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428482
    const v2, 0xc50002

    const-string p0, "minutiae_feelings_selector_time_to_fetch_start"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428483
    const v2, 0xc50003

    const-string p0, "minutiae_feelings_selector_time_to_fetch_end"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428484
    const v2, 0xc50004

    const-string p0, "minutiae_feelings_selector_time_to_results_shown"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428485
    const v2, 0xc50005

    const-string p0, "minutiae_feelings_selector_fetch_time"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428486
    const v2, 0xc5000b

    const-string p0, "minutiae_feelings_selector_time_to_scroll_load"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428487
    const/16 v1, 0x2b

    const v2, 0x5ba8a28c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1428476
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1428477
    const-string v0, "session_id"

    iget v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1428478
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1428464
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1428465
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1428466
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->b()V

    .line 1428467
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 1428468
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 1428469
    if-eqz p1, :cond_0

    .line 1428470
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->r:LX/91D;

    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1428471
    iget-object p0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, p0

    .line 1428472
    const-string p0, "feeling_selector_focused"

    invoke-static {p0, v1}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    .line 1428473
    iget-object p1, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p0, p1

    .line 1428474
    iget-object p1, v0, LX/91D;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428475
    :cond_0
    return-void
.end method
