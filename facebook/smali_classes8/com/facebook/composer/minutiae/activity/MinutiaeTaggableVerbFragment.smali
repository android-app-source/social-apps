.class public Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/90u;
.implements LX/8zq;


# instance fields
.field public final a:LX/0Tn;

.field public b:LX/92o;

.field public c:LX/03V;

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/01T;

.field public f:LX/8z3;

.field public g:LX/919;

.field public h:LX/92E;

.field public i:LX/92C;

.field public j:LX/92Q;

.field public k:Lcom/facebook/widget/listview/BetterListView;

.field private l:LX/4nw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4nw",
            "<",
            "Lcom/facebook/widget/error/GenericErrorView;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/4nw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4nw",
            "<",
            "Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/4nw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4nw",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field private p:LX/90x;

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

.field private s:Z

.field public t:LX/0ta;

.field public u:LX/90a;

.field public v:LX/90b;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1429982
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1429983
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "minutiae_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->a:LX/0Tn;

    .line 1429984
    iput-boolean v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->o:Z

    .line 1429985
    iput-boolean v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->s:Z

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1429980
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->k:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->p:LX/90x;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1429981
    return-void
.end method

.method public static e(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;)V
    .locals 1

    .prologue
    .line 1429977
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->c()V

    .line 1429978
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->b:LX/92o;

    invoke-virtual {v0, p0}, LX/92o;->a(LX/8zq;)V

    .line 1429979
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1429971
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->g:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429972
    iget-object p0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, p0

    .line 1429973
    const-string v2, "activity_picker_tapped_cancel"

    invoke-static {v2, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1429974
    iget-object p0, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, p0

    .line 1429975
    iget-object p0, v0, LX/919;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429976
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1429968
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 1429969
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1429970
    return-void
.end method

.method public final a(LX/0Px;LX/0ta;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1429922
    iput-object p2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->t:LX/0ta;

    .line 1429923
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->q:LX/0Px;

    .line 1429924
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->p:LX/90x;

    if-eqz v0, :cond_0

    .line 1429925
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->p:LX/90x;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->q:LX/0Px;

    invoke-virtual {v0, v1}, LX/90x;->a(LX/0Px;)V

    .line 1429926
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->h:LX/92E;

    .line 1429927
    const v1, 0x430003

    const-string p1, "minutiae_verb_picker_time_to_fetch_end"

    invoke-virtual {v0, v1, p1, p2}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1429928
    const v1, 0x430004

    const-string p1, "minutiae_verb_picker_fetch_time"

    invoke-virtual {v0, v1, p1, p2}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1429929
    const v1, 0x430005

    const-string p1, "minutiae_verb_picker_rendering_time"

    invoke-virtual {v0, v1, p1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1429930
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->k:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_2

    .line 1429931
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->k:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/90y;

    invoke-direct {v1, p0}, LX/90y;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnDrawListenerTo(LX/0fu;)V

    .line 1429932
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->o:Z

    if-nez v0, :cond_1

    .line 1429933
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->g:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429934
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429935
    const-string v2, "activity_picker_verbs_loaded"

    invoke-static {v2, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1429936
    iget-object v3, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, v3

    .line 1429937
    iget-object v3, v0, LX/919;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429938
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->o:Z

    .line 1429939
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429940
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v1

    .line 1429941
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429942
    iget-boolean v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    move v0, v1

    .line 1429943
    if-nez v0, :cond_3

    .line 1429944
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429945
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 1429946
    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->f:LX/8z3;

    const/4 p2, 0x0

    .line 1429947
    new-instance v3, LX/93I;

    invoke-direct {v3, v0}, LX/93I;-><init>(Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;)V

    .line 1429948
    iget-object v4, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v5

    sget-object p1, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1429949
    iget-object v4, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v5, LX/93J;

    invoke-direct {v5, v0, v1}, LX/93J;-><init>(Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429950
    new-instance v4, LX/8z6;

    invoke-direct {v4}, LX/8z6;-><init>()V

    .line 1429951
    iput-boolean p2, v4, LX/8z6;->g:Z

    .line 1429952
    move-object v4, v4

    .line 1429953
    iput-boolean p2, v4, LX/8z6;->f:Z

    .line 1429954
    move-object v4, v4

    .line 1429955
    iput-object v1, v4, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1429956
    move-object v4, v4

    .line 1429957
    invoke-virtual {v4}, LX/8z6;->a()LX/8z5;

    move-result-object v4

    .line 1429958
    iget-object v5, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->e:Landroid/widget/TextView;

    invoke-interface {v2, v4}, LX/8z3;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1429959
    iget-object v4, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->c:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429960
    iget-object v3, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->b:Landroid/widget/ImageView;

    new-instance v4, LX/93K;

    invoke-direct {v4, v0}, LX/93K;-><init>(Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429961
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->setVisibility(I)V

    .line 1429962
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    .line 1429963
    iput-object p0, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->f:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    .line 1429964
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->bringToFront()V

    .line 1429965
    :goto_0
    return-void

    .line 1429966
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->s:Z

    goto :goto_0

    .line 1429967
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->c()V

    goto :goto_0
.end method

.method public final a(LX/90b;)V
    .locals 0

    .prologue
    .line 1429920
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->v:LX/90b;

    .line 1429921
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1429986
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1429987
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    invoke-static {v0}, LX/92o;->a(LX/0QB;)LX/92o;

    move-result-object v3

    check-cast v3, LX/92o;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    invoke-static {v0}, LX/8zJ;->b(LX/0QB;)LX/8zJ;

    move-result-object v7

    check-cast v7, LX/8zJ;

    invoke-static {v0}, LX/919;->a(LX/0QB;)LX/919;

    move-result-object v8

    check-cast v8, LX/919;

    invoke-static {v0}, LX/92E;->a(LX/0QB;)LX/92E;

    move-result-object v9

    check-cast v9, LX/92E;

    invoke-static {v0}, LX/92C;->a(LX/0QB;)LX/92C;

    move-result-object p1

    check-cast p1, LX/92C;

    invoke-static {v0}, LX/92Q;->a(LX/0QB;)LX/92Q;

    move-result-object v0

    check-cast v0, LX/92Q;

    iput-object v3, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->b:LX/92o;

    iput-object v4, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->c:LX/03V;

    iput-object v5, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v6, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->e:LX/01T;

    iput-object v7, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->f:LX/8z3;

    iput-object v8, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->g:LX/919;

    iput-object v9, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->h:LX/92E;

    iput-object p1, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->i:LX/92C;

    iput-object v0, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->j:LX/92Q;

    .line 1429988
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1429989
    const-string v1, "minutiae_configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429990
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1429991
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1429848
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->g:LX/919;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429849
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429850
    const-string v2, "activity_picker_load_failed"

    invoke-static {v2, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1429851
    iget-object v3, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, v3

    .line 1429852
    iget-object v3, v0, LX/919;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429853
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->c:LX/03V;

    const-string v1, "minutiae_taggable_activity_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1429854
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->d()V

    .line 1429855
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 1429856
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    .line 1429857
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    new-instance v1, LX/90z;

    invoke-direct {v1, p0}, LX/90z;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429858
    return-void

    .line 1429859
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1429860
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x48254729

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1429890
    new-instance v0, LX/90x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p1, p0}, LX/90x;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->p:LX/90x;

    .line 1429891
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->q:LX/0Px;

    if-eqz v0, :cond_0

    .line 1429892
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->p:LX/90x;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->q:LX/0Px;

    invoke-virtual {v0, v2}, LX/90x;->a(LX/0Px;)V

    .line 1429893
    :cond_0
    const v0, 0x7f03033e

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1429894
    const v0, 0x7f0d0ab9

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/view/stubs/MinutiaeListViewStub;

    .line 1429895
    invoke-static {v0}, LX/4nw;->a(LX/4nv;)LX/4nw;

    move-result-object v0

    .line 1429896
    invoke-virtual {v0}, LX/4nw;->d()V

    .line 1429897
    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    const v3, 0x102000a

    invoke-static {v0, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->k:Lcom/facebook/widget/listview/BetterListView;

    .line 1429898
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->k:Lcom/facebook/widget/listview/BetterListView;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1429899
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->c()V

    .line 1429900
    const v0, 0x7f0d0ab2

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/4nv;

    invoke-static {v0}, LX/4nw;->a(LX/4nv;)LX/4nw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    .line 1429901
    const v0, 0x7f0d0abb

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/4nv;

    invoke-static {v0}, LX/4nw;->a(LX/4nv;)LX/4nw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    .line 1429902
    const v0, 0x7f0d0ab8

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/4nv;

    invoke-static {v0}, LX/4nw;->a(LX/4nv;)LX/4nw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->n:LX/4nw;

    .line 1429903
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1429904
    iget-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->e:LX/01T;

    sget-object p2, LX/01T;->PAA:LX/01T;

    if-ne p1, p2, :cond_4

    .line 1429905
    :cond_1
    :goto_0
    move v0, v0

    .line 1429906
    if-eqz v0, :cond_2

    .line 1429907
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->n:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->d()V

    .line 1429908
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->j:LX/92Q;

    .line 1429909
    iget-object v5, v0, LX/92Q;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/92F;->f:LX/0Tn;

    iget-object v7, v0, LX/92Q;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-interface {v5, v6, v7, v8}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 1429910
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->s:Z

    if-eqz v0, :cond_3

    .line 1429911
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->s:Z

    .line 1429912
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->c()V

    .line 1429913
    :cond_3
    const/16 v0, 0x2b

    const v3, -0x732b953e

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2

    .line 1429914
    :cond_4
    iget-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object p2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->a:LX/0Tn;

    invoke-interface {p1, p2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result p1

    .line 1429915
    if-nez p1, :cond_5

    .line 1429916
    iget-object p2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p2

    .line 1429917
    iget-object p3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->a:LX/0Tn;

    invoke-interface {p2, p3, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 1429918
    invoke-interface {p2}, LX/0hN;->commit()V

    .line 1429919
    :cond_5
    if-nez p1, :cond_1

    move v0, v3

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x5e0f4c40

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429861
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->c:LX/03V;

    .line 1429862
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->b:LX/92o;

    .line 1429863
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->u:LX/90a;

    .line 1429864
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1429865
    const/16 v1, 0x2b

    const v2, -0x49fb2ade

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x7026aa32

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1429866
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->k:Lcom/facebook/widget/listview/BetterListView;

    .line 1429867
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429868
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429869
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1429870
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;

    .line 1429871
    iput-object v2, v0, Lcom/facebook/composer/minutiae/view/MinutiaeFooterView;->f:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    .line 1429872
    :cond_1
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->m:LX/4nw;

    .line 1429873
    iput-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->l:LX/4nw;

    .line 1429874
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->b:LX/92o;

    .line 1429875
    iget-object v2, v0, LX/92o;->l:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1429876
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1429877
    const/16 v0, 0x2b

    const v2, -0x7f569ded

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xb2506f1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429878
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1429879
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->h:LX/92E;

    .line 1429880
    const v2, 0x430001

    const-string p0, "minutiae_verb_picker_time_to_init"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429881
    const v2, 0x430003

    const-string p0, "minutiae_verb_picker_time_to_fetch_end"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429882
    const v2, 0x43000b

    const-string p0, "minutiae_verb_picker_time_to_verbs_shown"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429883
    const v2, 0x430004

    const-string p0, "minutiae_verb_picker_fetch_time"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429884
    const v2, 0x430005

    const-string p0, "minutiae_verb_picker_rendering_time"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429885
    const v2, 0x430006

    const-string p0, "minutiae_verb_picker_tti"

    invoke-virtual {v1, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1429886
    const/16 v1, 0x2b

    const v2, -0x7e5a9ead

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7d681ff7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1429887
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1429888
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;)V

    .line 1429889
    const/16 v1, 0x2b

    const v2, -0x52448f9e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
