.class public Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/90T;
.implements LX/90U;


# instance fields
.field public p:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Landroid/support/v4/view/ViewPager;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public r:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private s:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

.field private t:LX/917;

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/92B;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1428922
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1428923
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1428924
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->u:LX/0Ot;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1428918
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v0

    .line 1428919
    sget-object v1, LX/93E;->OBJECT_PICKER:LX/93E;

    if-ne v0, v1, :cond_0

    .line 1428920
    invoke-static {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 1428921
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private a(LX/93G;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1428915
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1}, LX/93G;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1428916
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->r:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, LX/93G;->mTitleBarResource:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1428917
    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;Landroid/view/inputmethod/InputMethodManager;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/0Ot",
            "<",
            "LX/92B;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1428914
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->p:Landroid/view/inputmethod/InputMethodManager;

    iput-object p2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->u:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    invoke-static {v1}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/16 v2, 0x19ab

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a(Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;Landroid/view/inputmethod/InputMethodManager;LX/0Ot;)V

    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1428867
    const v0, 0x7f0d2e6d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->r:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1428868
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->r:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 1428869
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->r:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/90R;

    invoke-direct {v1, p0}, LX/90R;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1428870
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 1428902
    const v0, 0x7f0d2e6f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->q:Landroid/support/v4/view/ViewPager;

    .line 1428903
    new-instance v0, LX/917;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/917;-><init>(LX/0gc;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->t:LX/917;

    .line 1428904
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->q:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->t:LX/917;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1428905
    const v0, 0x7f0d2e6e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1428906
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1428907
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillParentWidth(Z)V

    .line 1428908
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1428909
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    move-object v1, v2

    .line 1428910
    invoke-direct {p0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a(LX/93G;)V

    .line 1428911
    new-instance v1, LX/90S;

    invoke-direct {v1, p0}, LX/90S;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;)V

    .line 1428912
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 1428913
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
    .locals 1

    .prologue
    .line 1428899
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->s:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    if-nez v0, :cond_0

    .line 1428900
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LX/93D;->a(Landroid/content/Intent;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->s:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428901
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->s:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    return-object v0
.end method

.method public final a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V
    .locals 0

    .prologue
    .line 1428897
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->s:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428898
    return-void
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 3

    .prologue
    .line 1428893
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "sticker_object"

    invoke-static {p1}, LX/5Rb;->a(Lcom/facebook/stickers/model/Sticker;)Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1428894
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1428895
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->finish()V

    .line 1428896
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1428890
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1428891
    iget-object p0, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v0, p0

    .line 1428892
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1428882
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1428883
    invoke-static {p0, p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1428884
    const v0, 0x7f031463

    invoke-virtual {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->setContentView(I)V

    .line 1428885
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->l()V

    .line 1428886
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->m()V

    .line 1428887
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92B;

    .line 1428888
    const p0, 0xc50001

    const-string p1, "minutiae_feelings_selector_time_to_init"

    invoke-virtual {v0, p0, p1}, LX/92A;->b(ILjava/lang/String;)V

    .line 1428889
    return-void
.end method

.method public final fS_()Z
    .locals 1

    .prologue
    .line 1428879
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1428880
    iget-object p0, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, p0

    .line 1428881
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1428871
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1428872
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->t:LX/917;

    .line 1428873
    iget-object v1, v0, LX/917;->b:LX/90F;

    move-object v0, v1

    .line 1428874
    if-eqz v0, :cond_0

    .line 1428875
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->t:LX/917;

    .line 1428876
    iget-object v1, v0, LX/917;->b:LX/90F;

    move-object v0, v1

    .line 1428877
    invoke-interface {v0}, LX/90F;->a()V

    .line 1428878
    :cond_0
    return-void
.end method
