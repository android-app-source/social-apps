.class public Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private final A:LX/8zz;

.field public B:I

.field private C:LX/8za;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private D:LX/8zi;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private E:LX/925;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private F:LX/8zR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private G:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/918;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private I:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/92D;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

.field private q:LX/5LG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/90G;

.field private s:Lcom/facebook/components/ComponentView;

.field private t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public u:LX/8zZ;

.field public v:LX/8zh;

.field private w:LX/1dV;

.field private x:LX/1De;

.field private y:LX/1OX;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1428851
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1428852
    new-instance v0, LX/90H;

    invoke-direct {v0, p0}, LX/90H;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->A:LX/8zz;

    .line 1428853
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1428854
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->G:LX/0Ot;

    .line 1428855
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1428856
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->I:LX/0Ot;

    .line 1428857
    return-void
.end method

.method private a(LX/1P0;)LX/1OX;
    .locals 1

    .prologue
    .line 1428850
    new-instance v0, LX/90K;

    invoke-direct {v0, p0, p1}, LX/90K;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;LX/1P0;)V

    return-object v0
.end method

.method private a(LX/8zS;Landroid/view/View$OnClickListener;)LX/1X1;
    .locals 5

    .prologue
    .line 1428821
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->F:LX/8zR;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->x:LX/1De;

    const/4 v2, 0x0

    .line 1428822
    new-instance v3, LX/8zP;

    invoke-direct {v3, v0}, LX/8zP;-><init>(LX/8zR;)V

    .line 1428823
    sget-object v4, LX/8zR;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8zQ;

    .line 1428824
    if-nez v4, :cond_0

    .line 1428825
    new-instance v4, LX/8zQ;

    invoke-direct {v4}, LX/8zQ;-><init>()V

    .line 1428826
    :cond_0
    invoke-static {v4, v1, v2, v2, v3}, LX/8zQ;->a$redex0(LX/8zQ;LX/1De;IILX/8zP;)V

    .line 1428827
    move-object v3, v4

    .line 1428828
    move-object v2, v3

    .line 1428829
    move-object v0, v2

    .line 1428830
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    .line 1428831
    iget-object v2, v0, LX/8zQ;->a:LX/8zP;

    iput-object v1, v2, LX/8zP;->a:LX/8zZ;

    .line 1428832
    iget-object v2, v0, LX/8zQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1428833
    move-object v0, v0

    .line 1428834
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->y:LX/1OX;

    .line 1428835
    iget-object v2, v0, LX/8zQ;->a:LX/8zP;

    iput-object v1, v2, LX/8zP;->e:LX/1OX;

    .line 1428836
    move-object v0, v0

    .line 1428837
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->z:Ljava/lang/String;

    .line 1428838
    iget-object v2, v0, LX/8zQ;->a:LX/8zP;

    iput-object v1, v2, LX/8zP;->b:Ljava/lang/String;

    .line 1428839
    iget-object v2, v0, LX/8zQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1428840
    move-object v0, v0

    .line 1428841
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->A:LX/8zz;

    .line 1428842
    iget-object v2, v0, LX/8zQ;->a:LX/8zP;

    iput-object v1, v2, LX/8zP;->f:LX/8zz;

    .line 1428843
    move-object v0, v0

    .line 1428844
    iget-object v1, v0, LX/8zQ;->a:LX/8zP;

    iput-object p1, v1, LX/8zP;->c:LX/8zS;

    .line 1428845
    iget-object v1, v0, LX/8zQ;->d:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1428846
    move-object v0, v0

    .line 1428847
    iget-object v1, v0, LX/8zQ;->a:LX/8zP;

    iput-object p2, v1, LX/8zP;->d:Landroid/view/View$OnClickListener;

    .line 1428848
    move-object v0, v0

    .line 1428849
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1428813
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object v0, v0

    .line 1428814
    if-nez v0, :cond_0

    .line 1428815
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1428816
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1428817
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1428818
    const-string v1, "minutiae_configuration"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1428819
    return-object v0

    .line 1428820
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1428804
    const v0, 0x7f0d1c02

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1428805
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1428806
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 1428807
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/90I;

    invoke-direct {v1, p0}, LX/90I;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1428808
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428809
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1428810
    if-eqz v0, :cond_0

    .line 1428811
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->b()V

    .line 1428812
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;LX/8za;LX/8zi;LX/925;LX/8zR;LX/0Ot;LX/918;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;",
            "LX/8za;",
            "LX/8zi;",
            "LX/925;",
            "LX/8zR;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/918;",
            "LX/0Ot",
            "<",
            "LX/92D;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1428803
    iput-object p1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->C:LX/8za;

    iput-object p2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->D:LX/8zi;

    iput-object p3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->E:LX/925;

    iput-object p4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->F:LX/8zR;

    iput-object p5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->G:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->H:LX/918;

    iput-object p7, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->I:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    const-class v1, LX/8za;

    invoke-interface {v7, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8za;

    const-class v2, LX/8zi;

    invoke-interface {v7, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/8zi;

    invoke-static {v7}, LX/925;->a(LX/0QB;)LX/925;

    move-result-object v3

    check-cast v3, LX/925;

    invoke-static {v7}, LX/8zR;->a(LX/0QB;)LX/8zR;

    move-result-object v4

    check-cast v4, LX/8zR;

    const/16 v5, 0x455

    invoke-static {v7, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v7}, LX/918;->a(LX/0QB;)LX/918;

    move-result-object v6

    check-cast v6, LX/918;

    const/16 v8, 0x19ad

    invoke-static {v7, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;LX/8za;LX/8zi;LX/925;LX/8zR;LX/0Ot;LX/918;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 3

    .prologue
    .line 1428797
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428798
    iget-object v0, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v0

    .line 1428799
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v1, p1, v2}, LX/925;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0Px;)Landroid/content/Intent;

    move-result-object v1

    .line 1428800
    if-eqz v1, :cond_0

    .line 1428801
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1428802
    :cond_0
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1428782
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 1428783
    iput v2, v0, LX/108;->a:I

    .line 1428784
    move-object v0, v0

    .line 1428785
    const v1, 0x7f0823cf

    invoke-virtual {p0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1428786
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1428787
    move-object v0, v0

    .line 1428788
    const/4 v1, -0x2

    .line 1428789
    iput v1, v0, LX/108;->h:I

    .line 1428790
    move-object v0, v0

    .line 1428791
    iput-boolean v2, v0, LX/108;->q:Z

    .line 1428792
    move-object v0, v0

    .line 1428793
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1428794
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1428795
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/90J;

    invoke-direct {v1, p0}, LX/90J;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1428796
    return-void
.end method

.method public static b(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;LX/8zS;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1428779
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->w:LX/1dV;

    if-eqz v0, :cond_0

    .line 1428780
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->w:LX/1dV;

    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(LX/8zS;Landroid/view/View$OnClickListener;)LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1dV;->b(LX/1X1;)V

    .line 1428781
    :cond_0
    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1428775
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428776
    const-string v0, "bundle_config"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1428777
    const-string v0, "session_id"

    iget v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1428778
    return-void
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1428671
    const-string v0, "bundle_config"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428672
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    .line 1428673
    return-void
.end method

.method public static l(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V
    .locals 6

    .prologue
    .line 1428760
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->H:LX/918;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428761
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428762
    iget v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    .line 1428763
    iget-object v4, v3, LX/8zh;->n:Ljava/lang/String;

    move-object v3, v4

    .line 1428764
    const-string v4, "activities_selector_object_skipped"

    invoke-static {v4, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    .line 1428765
    iget-object v5, v4, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, v5

    .line 1428766
    iget-object v5, v0, LX/918;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428767
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1428768
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428769
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 1428770
    if-eqz v1, :cond_0

    .line 1428771
    const-string v2, "extra_place"

    invoke-static {v0, v2, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1428772
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 1428773
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->finish()V

    .line 1428774
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1428751
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    invoke-interface {v0}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1428752
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    invoke-interface {v0}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v0

    .line 1428753
    :goto_0
    return-object v0

    .line 1428754
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    .line 1428755
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    .line 1428756
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1428757
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1428758
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1428759
    :cond_2
    const v0, 0x7f08141a

    invoke-virtual {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private n()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1428730
    const v0, 0x7f0d1c03

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->s:Lcom/facebook/components/ComponentView;

    .line 1428731
    new-instance v7, LX/1P0;

    invoke-direct {v7, p0}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 1428732
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->C:LX/8za;

    new-instance v1, LX/90P;

    invoke-direct {v1, p0}, LX/90P;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    new-instance v2, LX/90Q;

    invoke-direct {v2, p0}, LX/90Q;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    invoke-virtual {v0, v7, v1, v2, v3}, LX/8za;->a(LX/1P0;LX/90B;LX/90D;LX/5LG;)LX/8zZ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    .line 1428733
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->D:LX/8zi;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    iget-object v2, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->o()LX/905;

    move-result-object v4

    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p()LX/909;

    move-result-object v5

    iget v6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    invoke-virtual/range {v0 .. v6}, LX/8zi;->a(LX/8zZ;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;LX/5LG;LX/905;LX/909;I)LX/8zh;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    .line 1428734
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    .line 1428735
    iget-object v1, v0, LX/8zh;->i:LX/5LG;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/8zh;->i:LX/5LG;

    invoke-interface {v1}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1428736
    iget-object v1, v0, LX/8zh;->i:LX/5LG;

    invoke-interface {v1}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v1

    .line 1428737
    :goto_0
    move-object v0, v1

    .line 1428738
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->z:Ljava/lang/String;

    .line 1428739
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->z:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->z:Ljava/lang/String;

    .line 1428740
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    invoke-virtual {v0, v8}, LX/8zh;->a(Ljava/lang/String;)V

    .line 1428741
    new-instance v0, LX/1De;

    invoke-direct {v0, p0}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->x:LX/1De;

    .line 1428742
    invoke-direct {p0, v7}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(LX/1P0;)LX/1OX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->y:LX/1OX;

    .line 1428743
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->x:LX/1De;

    sget-object v1, LX/8zS;->LOADED:LX/8zS;

    invoke-direct {p0, v1, v8}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(LX/8zS;Landroid/view/View$OnClickListener;)LX/1X1;

    move-result-object v1

    invoke-static {v0, v1}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->w:LX/1dV;

    .line 1428744
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->s:Lcom/facebook/components/ComponentView;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->w:LX/1dV;

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1428745
    return-void

    .line 1428746
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081423

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1428747
    :cond_1
    iget-object v1, v0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    .line 1428748
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1428749
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1428750
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private o()LX/905;
    .locals 1

    .prologue
    .line 1428729
    new-instance v0, LX/90L;

    invoke-direct {v0, p0}, LX/90L;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    return-object v0
.end method

.method private p()LX/909;
    .locals 1

    .prologue
    .line 1428728
    new-instance v0, LX/90O;

    invoke-direct {v0, p0}, LX/90O;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    return-object v0
.end method

.method public static q(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1428721
    const/4 v0, 0x0

    .line 1428722
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    if-eqz v1, :cond_1

    .line 1428723
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    invoke-interface {v0}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v0

    .line 1428724
    :cond_0
    :goto_0
    return-object v0

    .line 1428725
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    .line 1428726
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1428727
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1428705
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1428706
    invoke-static {p0, p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1428707
    if-nez p1, :cond_0

    .line 1428708
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LX/93D;->a(Landroid/content/Intent;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428709
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    .line 1428710
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428711
    iget-object p1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object v0, p1

    .line 1428712
    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q:LX/5LG;

    .line 1428713
    new-instance v0, LX/90G;

    invoke-direct {v0}, LX/90G;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    .line 1428714
    const v0, 0x7f030b1f

    invoke-virtual {p0, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->setContentView(I)V

    .line 1428715
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a()V

    .line 1428716
    invoke-direct {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->n()V

    .line 1428717
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92D;

    .line 1428718
    const p0, 0xc60001

    const-string p1, "minutiae_objects_selector_time_to_init"

    invoke-virtual {v0, p0, p1}, LX/92A;->b(ILjava/lang/String;)V

    .line 1428719
    return-void

    .line 1428720
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->e(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1428700
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1428701
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 1428702
    invoke-virtual {p0, p2, p3}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 1428703
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->finish()V

    .line 1428704
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 11

    .prologue
    .line 1428686
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1428687
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->H:LX/918;

    iget-object v1, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428688
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428689
    invoke-static {p0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    invoke-virtual {v3}, LX/8zZ;->l()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    invoke-virtual {v4}, LX/8zZ;->n()LX/0Px;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    .line 1428690
    iget v6, v5, LX/90G;->a:I

    move v5, v6

    .line 1428691
    iget-object v6, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    invoke-virtual {v6}, LX/8zh;->b()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    .line 1428692
    iget-object v9, v8, LX/8zZ;->j:LX/5Lv;

    .line 1428693
    iget-object v8, v9, LX/5Lv;->d:Ljava/lang/String;

    move-object v9, v8

    .line 1428694
    move-object v8, v9

    .line 1428695
    const-string v9, "activities_selector_tapped_back"

    invoke-static {v9, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/8yQ;->b(I)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v6}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v9

    .line 1428696
    iget-object v10, v9, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v9, v10

    .line 1428697
    iget-object v10, v0, LX/918;->a:LX/0Zb;

    invoke-interface {v10, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428698
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    invoke-virtual {v0}, LX/8zh;->d()V

    .line 1428699
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x13a6de76

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1428677
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1428678
    iget-object v0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92D;

    .line 1428679
    const v2, 0xc60001

    const-string p0, "minutiae_objects_selector_time_to_init"

    invoke-virtual {v0, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428680
    const v2, 0xc60002

    const-string p0, "minutiae_objects_selector_time_to_fetch_start"

    invoke-virtual {v0, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428681
    const v2, 0xc60003

    const-string p0, "minutiae_objects_selector_time_to_fetch_end"

    invoke-virtual {v0, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428682
    const v2, 0xc60004

    const-string p0, "minutiae_objects_selector_time_to_results_shown"

    invoke-virtual {v0, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428683
    const v2, 0xc60005

    const-string p0, "minutiae_objects_selector_fetch_time"

    invoke-virtual {v0, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428684
    const v2, 0xc60009

    const-string p0, "minutiae_objects_selector_time_to_scroll_load"

    invoke-virtual {v0, v2, p0}, LX/92A;->a(ILjava/lang/String;)V

    .line 1428685
    const/16 v0, 0x23

    const v2, -0x34df89b

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1428674
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1428675
    invoke-direct {p0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->d(Landroid/os/Bundle;)V

    .line 1428676
    return-void
.end method
