.class public Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""

# interfaces
.implements LX/AQy;


# instance fields
.field private j:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1672017
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 1672018
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->g()V

    .line 1672019
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1672014
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1672015
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->g()V

    .line 1672016
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1672011
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1672012
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->g()V

    .line 1672013
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    const/16 v4, 0x11

    const/4 v3, 0x0

    .line 1672002
    new-instance v0, Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    .line 1672003
    new-instance v0, LX/6VC;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/6VC;-><init>(II)V

    .line 1672004
    sget-object v1, LX/6VB;->TITLE:LX/6VB;

    iput-object v1, v0, LX/6VC;->e:LX/6VB;

    .line 1672005
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0, v1, v3, v0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1672006
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1672007
    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0b72

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1672008
    invoke-virtual {p0, v0, v3, v3, v3}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->setPadding(IIII)V

    .line 1672009
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1672010
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 1672000
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1672001
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1671997
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 1671998
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 1671999
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1671996
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public setIconSize(I)V
    .locals 0

    .prologue
    .line 1671994
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1671995
    return-void
.end method

.method public setIconUri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1671988
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1671989
    return-void
.end method

.method public setIsTitleEditable(Z)V
    .locals 1

    .prologue
    .line 1671992
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1671993
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1671990
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1671991
    return-void
.end method
