.class public Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/AQy;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1672072
    const-class v0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1672069
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1672070
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c()V

    .line 1672071
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1672066
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1672067
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c()V

    .line 1672068
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1672063
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1672064
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c()V

    .line 1672065
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1672058
    const v0, 0x7f030332

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1672059
    const v0, 0x7f0d0aa1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1672060
    const v0, 0x7f0d0aa2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    .line 1672061
    const v0, 0x7f0d0aa3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->d:Landroid/widget/TextView;

    .line 1672062
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1672055
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1672056
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1672057
    return-void
.end method

.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 1672073
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1672074
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1672054
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public setDateLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1672037
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1672038
    return-void
.end method

.method public setDatePickerClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1672052
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1672053
    return-void
.end method

.method public setIconSize(I)V
    .locals 1

    .prologue
    .line 1672049
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1672050
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1672051
    return-void
.end method

.method public setIconUri(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1672046
    invoke-static {p1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1672047
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1672048
    return-void
.end method

.method public setIsTitleEditable(Z)V
    .locals 1

    .prologue
    .line 1672044
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1672045
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1672039
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1672040
    :goto_0
    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1672041
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1672042
    :cond_0
    return-void

    .line 1672043
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
