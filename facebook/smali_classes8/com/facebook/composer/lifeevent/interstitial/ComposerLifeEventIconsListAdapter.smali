.class public Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0hB;

.field private final c:Landroid/view/View$OnClickListener;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1671342
    const-class v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View$OnClickListener;LX/0hB;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1671343
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1671344
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671345
    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->d:LX/0Px;

    .line 1671346
    iput-object p1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->c:Landroid/view/View$OnClickListener;

    .line 1671347
    iput-object p2, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->b:LX/0hB;

    .line 1671348
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1671349
    new-instance v0, Landroid/widget/GridLayout;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;)V

    .line 1671350
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 1671351
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1671352
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1671353
    check-cast p2, LX/0Px;

    .line 1671354
    check-cast p3, Landroid/widget/GridLayout;

    .line 1671355
    invoke-virtual {p3}, Landroid/widget/GridLayout;->removeAllViews()V

    .line 1671356
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v0

    .line 1671357
    iget-object v2, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    invoke-virtual {p5}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0b73

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    div-int/2addr v2, v3

    .line 1671358
    invoke-virtual {p3, v2}, Landroid/widget/GridLayout;->setColumnCount(I)V

    .line 1671359
    iget-object v3, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    mul-int/2addr v0, v2

    sub-int v0, v3, v0

    mul-int/lit8 v2, v2, 0x2

    div-int v2, v0, v2

    .line 1671360
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;

    .line 1671361
    new-instance v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1671362
    new-instance v5, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v5}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    .line 1671363
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v6

    iput v6, v5, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 1671364
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v6

    iput v6, v5, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 1671365
    invoke-virtual {v5, v2, v2, v2, v2}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    .line 1671366
    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1671367
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1671368
    invoke-virtual {v4, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTag(Ljava/lang/Object;)V

    .line 1671369
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0812d1

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1671370
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1671371
    invoke-virtual {p3, v4}, Landroid/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 1671372
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1671373
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1671374
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1671375
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1671376
    int-to-long v0, p1

    return-wide v0
.end method
