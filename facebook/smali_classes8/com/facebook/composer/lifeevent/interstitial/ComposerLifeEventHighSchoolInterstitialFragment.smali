.class public Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventHighSchoolInterstitialFragment;
.super Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventEducationInterstitialFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1671183
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventEducationInterstitialFragment;-><init>()V

    .line 1671184
    return-void
.end method


# virtual methods
.method public final a(LX/7l3;Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;)LX/7l3;
    .locals 4

    .prologue
    .line 1671185
    const v0, 0x7f0812c9

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1671186
    iput-object v0, p1, LX/7l3;->a:Ljava/lang/String;

    .line 1671187
    move-object v0, p1

    .line 1671188
    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1671189
    iput-object v1, v0, LX/7l3;->j:Ljava/lang/String;

    .line 1671190
    move-object v0, v0

    .line 1671191
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1671192
    new-instance v0, LX/7lA;

    invoke-direct {v0}, LX/7lA;-><init>()V

    move-object v0, v0

    .line 1671193
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "image_size"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1671194
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1671195
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/AQH;

    invoke-direct {v1, p0}, LX/AQH;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventHighSchoolInterstitialFragment;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1671196
    const v0, 0x7f0812c6

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
