.class public Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/AQP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0h5;

.field public e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

.field public f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public g:Lcom/facebook/widget/listview/BetterListView;

.field public h:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;

.field public i:LX/AQO;

.field public final j:Landroid/text/TextWatcher;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1671336
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1671337
    new-instance v0, LX/AQI;

    invoke-direct {v0, p0}, LX/AQI;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->j:Landroid/text/TextWatcher;

    .line 1671338
    new-instance v0, LX/AQJ;

    invoke-direct {v0, p0}, LX/AQJ;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->k:Landroid/view/View$OnClickListener;

    .line 1671339
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    const-class v1, LX/AQP;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/AQP;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, LX/0Tf;

    iput-object v1, p1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->a:LX/AQP;

    iput-object v2, p1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->b:LX/0tX;

    iput-object p0, p1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->c:LX/0Tf;

    return-void
.end method

.method public static b(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V
    .locals 4

    .prologue
    .line 1671328
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-nez v0, :cond_0

    .line 1671329
    :goto_0
    return-void

    .line 1671330
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1671331
    new-instance v0, LX/7l8;

    invoke-direct {v0}, LX/7l8;-><init>()V

    move-object v0, v0

    .line 1671332
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1671333
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x15180

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 1671334
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/AQK;

    invoke-direct {v1, p0}, LX/AQK;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1671335
    new-instance v1, LX/AQL;

    invoke-direct {v1, p0}, LX/AQL;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->c:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static k(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .locals 2

    .prologue
    .line 1671340
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1671341
    const-string v1, "extra_composer_life_event_model"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1671324
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1671325
    const-class v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-static {v0, p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1671326
    new-instance v0, LX/AQO;

    invoke-direct {v0, p0}, LX/AQO;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->i:LX/AQO;

    .line 1671327
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x76f438f9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1671323
    const v1, 0x7f030327

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x11ae9575

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1671292
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1671293
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->a:LX/AQP;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->k:Landroid/view/View$OnClickListener;

    .line 1671294
    new-instance p2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v2

    check-cast v2, LX/0hB;

    invoke-direct {p2, v1, v2}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;-><init>(Landroid/view/View$OnClickListener;LX/0hB;)V

    .line 1671295
    move-object v0, p2

    .line 1671296
    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->h:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;

    .line 1671297
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1671298
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->d:LX/0h5;

    .line 1671299
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->d:LX/0h5;

    new-instance v1, LX/AQM;

    invoke-direct {v1, p0}, LX/AQM;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1671300
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->d:LX/0h5;

    const v1, 0x7f0812c8

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1671301
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1671302
    const-string v1, "extra_composer_life_event_custom"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1671303
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1671304
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1671305
    move-object v0, v0

    .line 1671306
    const/4 v1, -0x2

    .line 1671307
    iput v1, v0, LX/108;->h:I

    .line 1671308
    move-object v0, v0

    .line 1671309
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1671310
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->d:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1671311
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->d:LX/0h5;

    new-instance v1, LX/AQN;

    invoke-direct {v1, p0}, LX/AQN;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1671312
    :cond_0
    const v0, 0x7f0d0a94

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    .line 1671313
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    invoke-static {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->k(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->j:Landroid/text/TextWatcher;

    const/4 p2, 0x0

    invoke-static {v0, v1, v2, v3, p2}, LX/AQz;->a(Landroid/content/res/Resources;LX/AQy;Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;Landroid/text/TextWatcher;Landroid/view/View$OnClickListener;)V

    .line 1671314
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c5c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1671315
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1671316
    iget-object v2, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    invoke-virtual {v2, v0, v1, v1, v1}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->setPadding(IIII)V

    .line 1671317
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1671318
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;->e()V

    .line 1671319
    :cond_1
    const v0, 0x7f0d0a95

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1671320
    const v0, 0x7f0d0a96

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    .line 1671321
    invoke-static {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->b(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V

    .line 1671322
    return-void
.end method
