.class public Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1671197
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1671198
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1671199
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1671200
    const v0, 0x7f030326

    invoke-virtual {p0, v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsActivity;->setContentView(I)V

    .line 1671201
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0a93

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    .line 1671202
    if-nez v0, :cond_0

    .line 1671203
    new-instance v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;-><init>()V

    .line 1671204
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1671205
    const-string v2, "extra_composer_life_event_model"

    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "extra_composer_life_event_model"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1671206
    const-string v2, "extra_composer_life_event_custom"

    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "extra_composer_life_event_custom"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1671207
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1671208
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0a92

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1671209
    return-void
.end method
