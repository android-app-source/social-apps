.class public abstract Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/AQa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0h5;

.field private h:Lcom/facebook/widget/listview/BetterListView;

.field public i:Landroid/widget/EditText;

.field public j:Landroid/view/View;

.field public k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private l:Landroid/content/Intent;

.field public m:LX/AQZ;

.field public n:LX/AQb;

.field private final o:Landroid/view/View$OnClickListener;

.field private final p:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1671125
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1671126
    new-instance v0, LX/AQQ;

    invoke-direct {v0, p0}, LX/AQQ;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->o:Landroid/view/View$OnClickListener;

    .line 1671127
    new-instance v0, LX/AQS;

    invoke-direct {v0, p0}, LX/AQS;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->p:Landroid/text/TextWatcher;

    .line 1671128
    new-instance v0, LX/AQb;

    invoke-direct {v0}, LX/AQb;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->n:LX/AQb;

    .line 1671129
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)V
    .locals 4

    .prologue
    .line 1671122
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->l:Landroid/content/Intent;

    const-string v1, "extra_composer_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/5RH;

    invoke-direct {v1}, LX/5RH;-><init>()V

    invoke-virtual {v1}, LX/5RH;->a()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1671123
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->f:LX/1Kf;

    const/4 v2, 0x0

    const/16 v3, 0x6dc

    invoke-interface {v1, v2, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 1671124
    return-void
.end method

.method public static k(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .locals 2

    .prologue
    .line 1671121
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->l:Landroid/content/Intent;

    const-string v1, "life_event_model"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    return-object v0
.end method

.method public static l(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V
    .locals 3

    .prologue
    .line 1671118
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->i:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1671119
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1671120
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(LX/7l3;Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;)LX/7l3;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
            ">;>;"
        }
    .end annotation
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1671115
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1671116
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, LX/0Tf;

    const-class p1, LX/AQa;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/AQa;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iput-object v3, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iput-object v4, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->c:LX/0tX;

    iput-object v6, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->d:LX/0Tf;

    iput-object p1, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->e:LX/AQa;

    iput-object v0, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->f:LX/1Kf;

    .line 1671117
    return-void
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Z
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1671112
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1671113
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1671114
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x59459c1d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1671111
    const v1, 0x7f03032a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5d176ef0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1671077
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1671078
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1671079
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->l:Landroid/content/Intent;

    .line 1671080
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->o:Landroid/view/View$OnClickListener;

    .line 1671081
    new-instance v1, LX/AQZ;

    invoke-direct {v1, v0}, LX/AQZ;-><init>(Landroid/view/View$OnClickListener;)V

    .line 1671082
    move-object v0, v1

    .line 1671083
    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->m:LX/AQZ;

    .line 1671084
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1671085
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->g:LX/0h5;

    .line 1671086
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->g:LX/0h5;

    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1671087
    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1671088
    :goto_0
    const v0, 0x7f0d0a95

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1671089
    const v0, 0x7f0d0a9a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    .line 1671090
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->m:LX/AQZ;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1671091
    const v0, 0x7f0d0a98

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->i:Landroid/widget/EditText;

    .line 1671092
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->i:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1671093
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->i:Landroid/widget/EditText;

    new-instance v1, LX/AQT;

    invoke-direct {v1, p0}, LX/AQT;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1671094
    const v0, 0x7f0d0a99

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->j:Landroid/view/View;

    .line 1671095
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->j:Landroid/view/View;

    new-instance v1, LX/AQU;

    invoke-direct {v1, p0}, LX/AQU;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1671096
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/AQV;

    invoke-direct {v1, p0}, LX/AQV;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1671097
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->p:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1671098
    return-void

    .line 1671099
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x1

    .line 1671100
    iput v1, v0, LX/108;->a:I

    .line 1671101
    move-object v0, v0

    .line 1671102
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p2, 0x7f080030

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1671103
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1671104
    move-object v0, v0

    .line 1671105
    const/4 v1, -0x2

    .line 1671106
    iput v1, v0, LX/108;->h:I

    .line 1671107
    move-object v0, v0

    .line 1671108
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1671109
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->g:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1671110
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->g:LX/0h5;

    new-instance v1, LX/AQW;

    invoke-direct {v1, p0}, LX/AQW;-><init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto/16 :goto_0
.end method
