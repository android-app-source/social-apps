.class public Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AQi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Landroid/widget/EditText;

.field public h:Landroid/view/View;

.field public i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public j:LX/AQu;

.field public k:Z

.field public l:Landroid/view/View$OnClickListener;

.field public m:Landroid/view/View$OnClickListener;

.field public n:LX/AQh;

.field public o:LX/AQr;

.field public final p:Landroid/widget/Filter$FilterListener;

.field private final q:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1671866
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1671867
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->k:Z

    .line 1671868
    new-instance v0, LX/AQp;

    invoke-direct {v0, p0}, LX/AQp;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->p:Landroid/widget/Filter$FilterListener;

    .line 1671869
    new-instance v0, LX/AQq;

    invoke-direct {v0, p0}, LX/AQq;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->q:Landroid/text/TextWatcher;

    .line 1671870
    return-void
.end method

.method public static c(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V
    .locals 4

    .prologue
    .line 1671871
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-nez v0, :cond_0

    .line 1671872
    :goto_0
    return-void

    .line 1671873
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1671874
    new-instance v0, LX/7l9;

    invoke-direct {v0}, LX/7l9;-><init>()V

    move-object v0, v0

    .line 1671875
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1671876
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x15180

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 1671877
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/AQn;

    invoke-direct {v1, p0}, LX/AQn;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1671878
    new-instance v1, LX/AQo;

    invoke-direct {v1, p0}, LX/AQo;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->d:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1671879
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1671880
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    const-class v4, LX/AQi;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AQi;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, LX/0Tf;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v3, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iput-object v4, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->b:LX/AQi;

    iput-object v5, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->c:LX/0tX;

    iput-object p1, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->d:LX/0Tf;

    iput-object v0, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1671881
    new-instance v0, LX/AQr;

    invoke-direct {v0, p0}, LX/AQr;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->o:LX/AQr;

    .line 1671882
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0025

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1671883
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1671884
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->g:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1671885
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1671886
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2f871712

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1671887
    const v1, 0x7f03032e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6c7e9b8b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4d0b0e47    # 1.45810544E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1671888
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1671889
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x1a0025

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1671890
    const/16 v1, 0x2b

    const v2, 0x36860fbf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1671891
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1671892
    const v0, 0x7f0d0a95

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1671893
    new-instance v0, LX/AQu;

    invoke-direct {v0}, LX/AQu;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->j:LX/AQu;

    .line 1671894
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->b:LX/AQi;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->j:LX/AQu;

    iget-object v3, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->l:Landroid/view/View$OnClickListener;

    iget-object v4, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/AQi;->a(Landroid/content/Context;LX/AQu;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)LX/AQh;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->n:LX/AQh;

    .line 1671895
    const v0, 0x7f0d0a9f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 1671896
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->n:LX/AQh;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1671897
    const v0, 0x7f0d0a9d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->g:Landroid/widget/EditText;

    .line 1671898
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->g:Landroid/widget/EditText;

    new-instance v1, LX/AQj;

    invoke-direct {v1, p0}, LX/AQj;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1671899
    const v0, 0x7f0d0a9e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->h:Landroid/view/View;

    .line 1671900
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->h:Landroid/view/View;

    new-instance v1, LX/AQk;

    invoke-direct {v1, p0}, LX/AQk;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1671901
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/AQl;

    invoke-direct {v1, p0}, LX/AQl;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1671902
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/AQm;

    invoke-direct {v1, p0}, LX/AQm;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnDrawListenerTo(LX/0fu;)V

    .line 1671903
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->q:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1671904
    invoke-static {p0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->c(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V

    .line 1671905
    return-void
.end method
