.class public final Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private final A:Landroid/view/View$OnClickListener;

.field private final B:LX/63W;

.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/AQF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/0h5;

.field private u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

.field private v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

.field private w:I

.field private x:LX/0Px;

.field private y:LX/0Px;

.field private final z:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1671595
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1671596
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->w:I

    .line 1671597
    new-instance v0, LX/AQe;

    invoke-direct {v0, p0}, LX/AQe;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->z:Landroid/view/View$OnClickListener;

    .line 1671598
    new-instance v0, LX/AQf;

    invoke-direct {v0, p0}, LX/AQf;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->A:Landroid/view/View$OnClickListener;

    .line 1671599
    new-instance v0, LX/AQg;

    invoke-direct {v0, p0}, LX/AQg;-><init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;)V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->B:LX/63W;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1671678
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->t:LX/0h5;

    const v1, 0x7f0812bb

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1671679
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->t:LX/0h5;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->x:LX/0Px;

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1671680
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0a9b

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1671681
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->w:I

    .line 1671682
    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;Lcom/facebook/content/SecureContextHelper;LX/1Kf;LX/AQF;LX/0ad;)V
    .locals 0

    .prologue
    .line 1671677
    iput-object p1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->q:LX/1Kf;

    iput-object p3, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->r:LX/AQF;

    iput-object p4, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->s:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    new-instance v5, LX/AQF;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    const/16 p1, 0x12cb

    invoke-static {v3, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-direct {v5, v2, v4, p1}, LX/AQF;-><init>(LX/0tX;LX/0WJ;LX/0Or;)V

    move-object v2, v5

    check-cast v2, LX/AQF;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->a(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;Lcom/facebook/content/SecureContextHelper;LX/1Kf;LX/AQF;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;LX/AQs;)V
    .locals 4

    .prologue
    .line 1671662
    iget-object v0, p1, LX/AQs;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1671663
    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->t:LX/0h5;

    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1671664
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->t:LX/0h5;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->y:LX/0Px;

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1671665
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1671666
    if-eqz p1, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1671667
    iget-object v1, p1, LX/AQs;->b:LX/0Px;

    move-object v1, v1

    .line 1671668
    if-eqz v1, :cond_2

    :goto_2
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1671669
    iget-object v1, p1, LX/AQs;->b:LX/0Px;

    move-object v1, v1

    .line 1671670
    iput-object v1, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;->c:LX/0Px;

    .line 1671671
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0a9b

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1671672
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->w:I

    .line 1671673
    return-void

    .line 1671674
    :cond_0
    const v0, 0x7f0812bf

    invoke-virtual {p0, v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v1, v3

    .line 1671675
    goto :goto_1

    :cond_2
    move v2, v3

    .line 1671676
    goto :goto_2
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, -0x2

    .line 1671622
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1671623
    invoke-static {p0, p0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1671624
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->r:LX/AQF;

    .line 1671625
    new-instance v1, LX/7l6;

    invoke-direct {v1}, LX/7l6;-><init>()V

    move-object v2, v1

    .line 1671626
    const-string v4, "userId"

    iget-object v1, v0, LX/AQF;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1671627
    iget-object p1, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p1

    .line 1671628
    invoke-virtual {v2, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1671629
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1671630
    iget-object v2, v0, LX/AQF;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/AQE;

    invoke-direct {v2, v0}, LX/AQE;-><init>(LX/AQF;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v1, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1671631
    const v0, 0x7f03032c

    invoke-virtual {p0, v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->setContentView(I)V

    .line 1671632
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0a9c

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    .line 1671633
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    if-nez v0, :cond_0

    .line 1671634
    new-instance v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    .line 1671635
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->z:Landroid/view/View$OnClickListener;

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->A:Landroid/view/View$OnClickListener;

    .line 1671636
    iput-object v1, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->l:Landroid/view/View$OnClickListener;

    .line 1671637
    iput-object v2, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->m:Landroid/view/View$OnClickListener;

    .line 1671638
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0a9f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    .line 1671639
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    if-nez v0, :cond_1

    .line 1671640
    new-instance v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    .line 1671641
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->v:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->A:Landroid/view/View$OnClickListener;

    .line 1671642
    iput-object v1, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;->b:Landroid/view/View$OnClickListener;

    .line 1671643
    :cond_1
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x1

    .line 1671644
    iput v1, v0, LX/108;->a:I

    .line 1671645
    move-object v0, v0

    .line 1671646
    const v1, 0x7f080017

    invoke-virtual {p0, v1}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1671647
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1671648
    move-object v0, v0

    .line 1671649
    iput v3, v0, LX/108;->h:I

    .line 1671650
    move-object v0, v0

    .line 1671651
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->x:LX/0Px;

    .line 1671652
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x2

    .line 1671653
    iput v1, v0, LX/108;->a:I

    .line 1671654
    move-object v0, v0

    .line 1671655
    const v1, 0x7f080027

    invoke-virtual {p0, v1}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1671656
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1671657
    move-object v0, v0

    .line 1671658
    iput v3, v0, LX/108;->h:I

    .line 1671659
    move-object v0, v0

    .line 1671660
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->y:LX/0Px;

    .line 1671661
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1671615
    if-nez p1, :cond_1

    .line 1671616
    if-nez p2, :cond_0

    .line 1671617
    :goto_0
    return-void

    .line 1671618
    :cond_0
    sget-object v0, LX/21D;->TIMELINE:LX/21D;

    const-string v1, "lifeEventAfterIconPicker"

    invoke-static {v0, v1}, LX/1nC;->b(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/5RH;

    invoke-direct {v1}, LX/5RH;-><init>()V

    invoke-virtual {v1}, LX/5RH;->a()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89K;

    invoke-direct {v1}, LX/89K;-><init>()V

    invoke-static {}, LX/BQ4;->c()LX/BQ4;

    move-result-object v1

    invoke-static {v1}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1671619
    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->q:LX/1Kf;

    const/4 v3, 0x0

    const-string v0, "extra_composer_life_event_icon_model"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v1, 0x6dc

    invoke-interface {v2, v3, v0, v1, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0

    .line 1671620
    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->setResult(ILandroid/content/Intent;)V

    .line 1671621
    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->finish()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1671608
    iget v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->w:I

    packed-switch v0, :pswitch_data_0

    .line 1671609
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unrecognized Fragment ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1671610
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->u:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->b()V

    .line 1671611
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->setResult(I)V

    .line 1671612
    invoke-virtual {p0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->finish()V

    .line 1671613
    :goto_0
    return-void

    .line 1671614
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x13803757

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1671600
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1671601
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1671602
    if-eqz v0, :cond_0

    .line 1671603
    invoke-static {v0}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1671604
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->t:LX/0h5;

    .line 1671605
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->t:LX/0h5;

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->B:LX/63W;

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1671606
    invoke-direct {p0}, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->a()V

    .line 1671607
    :cond_0
    const/16 v0, 0x23

    const v2, 0x2665bc4a

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
