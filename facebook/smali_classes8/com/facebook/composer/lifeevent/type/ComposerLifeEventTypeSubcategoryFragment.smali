.class public Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/AQi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1671919
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1671916
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1671917
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;

    const-class v0, LX/AQi;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/AQi;

    iput-object p1, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;->a:LX/AQi;

    .line 1671918
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x10ca56ed

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1671915
    const v1, 0x7f03032f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2425960c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1671906
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1671907
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;->a:LX/AQi;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v3, v3, v2}, LX/AQi;->a(Landroid/content/Context;LX/AQu;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)LX/AQh;

    move-result-object v1

    .line 1671908
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeSubcategoryFragment;->c:LX/0Px;

    .line 1671909
    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, v1, LX/AQh;->f:LX/0Px;

    .line 1671910
    const v0, 0x7f0d0a9f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1671911
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1671912
    return-void

    .line 1671913
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671914
    goto :goto_0
.end method
