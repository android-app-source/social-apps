.class public Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public n:LX/93W;

.field public o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

.field public p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

.field public q:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1433825
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1433826
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->q:LX/0ad;

    iput-object p0, p1, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->r:LX/0Uh;

    return-void
.end method

.method public static b(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1433876
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->r:LX/0Uh;

    const/16 v2, 0x440

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 1433877
    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1433878
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->q:LX/0ad;

    sget-short v2, LX/8Po;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1433865
    invoke-static {p0}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->b(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1433866
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v2}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1433867
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1433868
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v2, v2, LX/93W;->b:LX/93X;

    invoke-interface {v2, v1}, LX/93X;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1433869
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1433870
    goto :goto_0

    .line 1433871
    :cond_2
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-eqz v2, :cond_0

    .line 1433872
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->S_()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1433873
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1433874
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v2, v2, LX/93W;->b:LX/93X;

    invoke-interface {v2, v1}, LX/93X;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1433875
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1433864
    new-instance v0, LX/93T;

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/93T;-><init>(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a(LX/93W;)V
    .locals 2

    .prologue
    .line 1433860
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Typeahead data must be set before starting the fragment!"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1433861
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93W;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    .line 1433862
    return-void

    .line 1433863
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4a7db362

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1433856
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1433857
    const-class v1, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    invoke-static {v1, p0}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1433858
    const v1, 0x7f0e0632

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1433859
    const/16 v1, 0x2b

    const v2, 0x36a8c080

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3097d07e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1433827
    if-eqz p3, :cond_0

    .line 1433828
    const/4 v0, 0x0

    const/16 v2, 0x2b

    const v3, -0x72d0aca3

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1433829
    :goto_0
    return-object v0

    .line 1433830
    :cond_0
    invoke-static {p0}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->b(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f030306

    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1433831
    const v2, 0x7f0d0a5e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->m:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1433832
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->m:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v3, 0x7f08131c

    invoke-virtual {v2, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1433833
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->m:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1433834
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1433835
    invoke-virtual {v2, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1433836
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->m:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v3, LX/93U;

    invoke-direct {v3, p0}, LX/93U;-><init>(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1433837
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    const-string v3, "Audience dialog data was not set!"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433838
    invoke-static {p0}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->b(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1433839
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const v3, 0x7f0d05fa

    invoke-virtual {v2, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iput-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    .line 1433840
    new-instance v2, LX/8QF;

    invoke-direct {v2}, LX/8QF;-><init>()V

    iget-object v3, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v3, v3, LX/93W;->a:LX/8Q5;

    invoke-interface {v3}, LX/8Q5;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v3

    .line 1433841
    iput-object v3, v2, LX/8QF;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1433842
    move-object v2, v2

    .line 1433843
    iget-object v3, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v3, v3, LX/93W;->a:LX/8Q5;

    invoke-interface {v3}, LX/8Q5;->b()Z

    move-result v3

    .line 1433844
    iput-boolean v3, v2, LX/8QF;->b:Z

    .line 1433845
    move-object v2, v2

    .line 1433846
    invoke-virtual {v2}, LX/8QF;->a()Lcom/facebook/privacy/model/AudiencePickerInput;

    move-result-object v2

    .line 1433847
    iget-object v3, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v3, v2}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/privacy/model/AudiencePickerInput;)V

    .line 1433848
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    new-instance v3, LX/93V;

    invoke-direct {v3, p0}, LX/93V;-><init>(Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;)V

    .line 1433849
    iput-object v3, v2, Lcom/facebook/privacy/selector/AudiencePickerFragment;->k:LX/8Qi;

    .line 1433850
    :cond_1
    :goto_2
    const v2, -0x7ba0cdd2

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto/16 :goto_0

    .line 1433851
    :cond_2
    const v0, 0x7f030307

    goto :goto_1

    .line 1433852
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const v3, 0x7f0d05fb

    invoke-virtual {v2, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iput-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1433853
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v3, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v3, v3, LX/93W;->a:LX/8Q5;

    invoke-virtual {v2, v3}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8Q5;)V

    .line 1433854
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v2, v2, LX/93W;->c:LX/8Rm;

    if-eqz v2, :cond_1

    .line 1433855
    iget-object v2, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->o:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v3, p0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->n:LX/93W;

    iget-object v3, v3, LX/93W;->c:LX/8Rm;

    invoke-virtual {v2, v3}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8Rm;)V

    goto :goto_2
.end method
