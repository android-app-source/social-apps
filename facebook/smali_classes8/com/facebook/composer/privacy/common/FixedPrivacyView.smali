.class public Lcom/facebook/composer/privacy/common/FixedPrivacyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/947;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/view/View;

.field public f:Landroid/widget/TextView;

.field public g:LX/0hs;

.field private h:Z

.field private i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1434533
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1434534
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->h:Z

    .line 1434535
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a()V

    .line 1434536
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1434529
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1434530
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->h:Z

    .line 1434531
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a()V

    .line 1434532
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1434525
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1434526
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->h:Z

    .line 1434527
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a()V

    .line 1434528
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1434524
    new-instance v0, LX/945;

    invoke-direct {v0, p0, p1}, LX/945;-><init>(Lcom/facebook/composer/privacy/common/FixedPrivacyView;Ljava/lang/String;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1434518
    const-class v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-static {v0, p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1434519
    const v0, 0x7f03031c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1434520
    const v0, 0x7f0d0a7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->e:Landroid/view/View;

    .line 1434521
    const v0, 0x7f0d0a7c

    invoke-virtual {p0, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    .line 1434522
    new-instance v0, LX/944;

    invoke-direct {v0, p0}, LX/944;-><init>(Lcom/facebook/composer/privacy/common/FixedPrivacyView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1434523
    return-void
.end method

.method private static a(Lcom/facebook/composer/privacy/common/FixedPrivacyView;LX/947;LX/0wM;LX/8Sa;LX/0ad;)V
    .locals 0

    .prologue
    .line 1434517
    iput-object p1, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a:LX/947;

    iput-object p2, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->b:LX/0wM;

    iput-object p3, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->c:LX/8Sa;

    iput-object p4, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->d:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-static {v3}, LX/947;->a(LX/0QB;)LX/947;

    move-result-object v0

    check-cast v0, LX/947;

    invoke-static {v3}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {v3}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v2

    check-cast v2, LX/8Sa;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(Lcom/facebook/composer/privacy/common/FixedPrivacyView;LX/947;LX/0wM;LX/8Sa;LX/0ad;)V

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    .line 1434514
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a:LX/947;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->i:LX/0Px;

    iget-object v3, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/947;->a(Landroid/content/Context;LX/0Px;FIZZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1434515
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1434516
    return-void
.end method

.method private c(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1434479
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->b:LX/0wM;

    const v1, -0x6e685d

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p2    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 1434490
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1434491
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->e:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1434492
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0ba8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1434493
    iget-object v3, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v4, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v3, v4}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    .line 1434494
    iget-object v3, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    .line 1434495
    if-eqz p2, :cond_2

    .line 1434496
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1434497
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v5, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v6, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v5, v6}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1434498
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a010e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1434499
    iget-object v4, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-static {v4, v0, v7, v7, v7}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1434500
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1434501
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b01b1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1434502
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081329

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1434503
    :cond_0
    :goto_1
    if-nez p2, :cond_4

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->d:LX/0ad;

    sget-short v1, LX/1EB;->ar:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1434504
    invoke-virtual {p0, p3}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434505
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    const v1, -0xa76f01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1434506
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 1434507
    goto/16 :goto_0

    .line 1434508
    :cond_2
    if-eqz p1, :cond_3

    .line 1434509
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1434510
    :cond_3
    if-eqz v0, :cond_0

    .line 1434511
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->c(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v1, v0, v7, v7, v7}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1434512
    :cond_4
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434513
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    const v1, -0x6e685d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2
.end method

.method public final a(ZLcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 1
    .param p3    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1434488
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p3, v0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/view/View$OnClickListener;)V

    .line 1434489
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1434484
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->i:LX/0Px;

    if-eqz v0, :cond_0

    .line 1434485
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->h:Z

    .line 1434486
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->b()V

    .line 1434487
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6bbec6f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1434480
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1434481
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->h:Z

    .line 1434482
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->invalidate()V

    .line 1434483
    const/16 v1, 0x2d

    const v2, 0x497f2b39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
