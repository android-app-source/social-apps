.class public Lcom/facebook/composer/privacy/common/SelectablePrivacyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/8Rp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/947;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field public h:Z

.field private i:Z

.field private j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1434576
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1434577
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->l:Z

    .line 1434578
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a()V

    .line 1434579
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1434589
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1434590
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->l:Z

    .line 1434591
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a()V

    .line 1434592
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1434580
    const-class v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-static {v0, p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1434581
    const v0, 0x7f030349

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1434582
    new-instance v0, LX/8QQ;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8QQ;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->c:LX/0Px;

    .line 1434583
    const v0, 0x7f0d0acf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->d:Landroid/view/View;

    .line 1434584
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0ad0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->e:LX/0zw;

    .line 1434585
    const v0, 0x7f0d0acd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->f:Landroid/view/View;

    .line 1434586
    const v0, 0x7f0d0ace

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->g:Landroid/widget/TextView;

    .line 1434587
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->setAsLoading(Z)V

    .line 1434588
    return-void
.end method

.method private static a(Lcom/facebook/composer/privacy/common/SelectablePrivacyView;LX/8Rp;LX/947;)V
    .locals 0

    .prologue
    .line 1434543
    iput-object p1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a:LX/8Rp;

    iput-object p2, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->b:LX/947;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-static {v1}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v0

    check-cast v0, LX/8Rp;

    invoke-static {v1}, LX/947;->a(LX/0QB;)LX/947;

    move-result-object v1

    check-cast v1, LX/947;

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(Lcom/facebook/composer/privacy/common/SelectablePrivacyView;LX/8Rp;LX/947;)V

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1434593
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->b:LX/947;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->k:LX/0Px;

    iget-object v3, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->g:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    move v6, v5

    invoke-virtual/range {v0 .. v6}, LX/947;->a(Landroid/content/Context;LX/0Px;FIZZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1434594
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1434595
    return-void
.end method

.method private setAsLoading(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1434563
    if-eqz p1, :cond_0

    .line 1434564
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->k:LX/0Px;

    .line 1434565
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1434566
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1434567
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->g:Landroid/widget/TextView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1434568
    :goto_0
    iput-boolean p1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->h:Z

    .line 1434569
    iput-boolean v4, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->l:Z

    .line 1434570
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->invalidate()V

    .line 1434571
    return-void

    .line 1434572
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1434573
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1434574
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->g:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1434575
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v3, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->i:Z

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8Rp;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;Landroid/content/res/Resources;ZZ)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->k:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 2

    .prologue
    .line 1434556
    iget-boolean v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Trying to display an invalid ComposerPrivacyData in SelectablePrivacyView"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1434557
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->i:Z

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1434558
    :cond_2
    :goto_1
    return-void

    .line 1434559
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1434560
    :cond_4
    iput-boolean p1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->i:Z

    .line 1434561
    iput-object p2, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1434562
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    invoke-direct {p0, v0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->setAsLoading(Z)V

    goto :goto_1
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1434552
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->l:Z

    if-eqz v0, :cond_0

    .line 1434553
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->l:Z

    .line 1434554
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->b()V

    .line 1434555
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1521f804

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1434548
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1434549
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->l:Z

    .line 1434550
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->invalidate()V

    .line 1434551
    const/16 v1, 0x2d

    const v2, 0x2799419d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1434544
    new-instance v0, LX/946;

    invoke-direct {v0, p0, p1}, LX/946;-><init>(Lcom/facebook/composer/privacy/common/SelectablePrivacyView;Landroid/view/View$OnClickListener;)V

    .line 1434545
    invoke-super {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434546
    iget-object v1, p0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434547
    return-void
.end method
