.class public final Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x738d2785
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1434761
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1434760
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1434758
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1434759
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1434752
    iput-object p1, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->e:Ljava/lang/String;

    .line 1434753
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1434754
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1434755
    if-eqz v0, :cond_0

    .line 1434756
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1434757
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434718
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->e:Ljava/lang/String;

    .line 1434719
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1434744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1434745
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1434746
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1434747
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1434748
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1434749
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1434750
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1434751
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1434736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1434737
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1434738
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1434739
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1434740
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;

    .line 1434741
    iput-object v0, v1, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->f:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1434742
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1434743
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1434762
    new-instance v0, LX/94A;

    invoke-direct {v0, p1}, LX/94A;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434734
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->f:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->f:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1434735
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->f:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1434728
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1434729
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1434730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1434731
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1434732
    :goto_0
    return-void

    .line 1434733
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1434725
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1434726
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a(Ljava/lang/String;)V

    .line 1434727
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1434722
    new-instance v0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;

    invoke-direct {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;-><init>()V

    .line 1434723
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1434724
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1434721
    const v0, 0x48a4fada

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1434720
    const v0, 0x403827a

    return v0
.end method
