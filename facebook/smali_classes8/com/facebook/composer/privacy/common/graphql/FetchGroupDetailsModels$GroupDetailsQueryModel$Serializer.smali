.class public final Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1434997
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    new-instance v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1434998
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1434999
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1435000
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1435001
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    .line 1435002
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1435003
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1435004
    if-eqz v2, :cond_0

    .line 1435005
    const-string v3, "full_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435006
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1435007
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1435008
    if-eqz v2, :cond_1

    .line 1435009
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435010
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1435011
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1435012
    if-eqz v2, :cond_2

    .line 1435013
    const-string v3, "is_multi_company_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435014
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1435015
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1435016
    if-eqz v2, :cond_3

    .line 1435017
    const-string v3, "parent_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435018
    invoke-static {v1, v2, p1}, LX/A82;->a(LX/15i;ILX/0nX;)V

    .line 1435019
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1435020
    if-eqz v2, :cond_4

    .line 1435021
    const-string v3, "posted_item_privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435022
    invoke-static {v1, v2, p1, p2}, LX/A82;->a$redex0(LX/15i;ILX/0nX;LX/0my;)V

    .line 1435023
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1435024
    if-eqz v2, :cond_5

    .line 1435025
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435026
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1435027
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1435028
    if-eqz v2, :cond_7

    .line 1435029
    const-string v3, "work_communities"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435030
    const/4 v3, 0x0

    .line 1435031
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1435032
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1435033
    if-eqz v3, :cond_6

    .line 1435034
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435035
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1435036
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1435037
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1435038
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1435039
    check-cast p1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Serializer;->a(Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
