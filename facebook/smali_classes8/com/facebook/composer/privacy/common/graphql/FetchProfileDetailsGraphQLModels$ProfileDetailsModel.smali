.class public final Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x20fdde91
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1435259
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1435260
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1435261
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1435262
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435265
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1435266
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1435267
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435263
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->f:Ljava/lang/String;

    .line 1435264
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435268
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->g:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->g:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1435269
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->g:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1435241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1435242
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1435243
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1435244
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->l()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1435245
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1435246
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1435247
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1435248
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1435249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1435250
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1435251
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1435252
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->l()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1435253
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->l()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1435254
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->l()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1435255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;

    .line 1435256
    iput-object v0, v1, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->g:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1435257
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1435258
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/2rX;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435240
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->l()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1435239
    new-instance v0, LX/94P;

    invoke-direct {v0, p1}, LX/94P;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1435237
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1435238
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1435236
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1435233
    new-instance v0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;

    invoke-direct {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;-><init>()V

    .line 1435234
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1435235
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1435232
    const v0, 0x721f5527

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1435231
    const v0, 0x50c72189

    return v0
.end method
