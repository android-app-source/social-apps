.class public final Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/2rX;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x49b43084
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1434979
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1434980
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1434981
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1434982
    return-void
.end method

.method private j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434983
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1434984
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1434985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1434986
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1434987
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1434988
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1434989
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1434990
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1434991
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1434992
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1434993
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1434994
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1434995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1434996
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1434969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1434970
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1434971
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1434972
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1434973
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    .line 1434974
    iput-object v0, v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1434975
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1434976
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434977
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->e:Ljava/lang/String;

    .line 1434978
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1434966
    new-instance v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    invoke-direct {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;-><init>()V

    .line 1434967
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1434968
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434964
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->f:Ljava/lang/String;

    .line 1434965
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/1Fd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434963
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1434961
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->h:Ljava/lang/String;

    .line 1434962
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1434960
    const v0, 0x1b4d0e4a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1434959
    const v0, -0x1c648c34

    return v0
.end method
