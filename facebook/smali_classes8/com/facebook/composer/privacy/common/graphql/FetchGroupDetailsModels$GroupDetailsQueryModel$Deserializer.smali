.class public final Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1434829
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    new-instance v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1434830
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1434831
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1434832
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1434833
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1434834
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 1434835
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1434836
    :goto_0
    move v1, v2

    .line 1434837
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1434838
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1434839
    new-instance v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    invoke-direct {v1}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;-><init>()V

    .line 1434840
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1434841
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1434842
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1434843
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1434844
    :cond_0
    return-object v1

    .line 1434845
    :cond_1
    const-string p0, "is_multi_company_group"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1434846
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v3

    .line 1434847
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_9

    .line 1434848
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1434849
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1434850
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1434851
    const-string p0, "full_name"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1434852
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1434853
    :cond_3
    const-string p0, "id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1434854
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1434855
    :cond_4
    const-string p0, "parent_group"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1434856
    invoke-static {p1, v0}, LX/A82;->a$redex0(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1434857
    :cond_5
    const-string p0, "posted_item_privacy_scope"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1434858
    invoke-static {p1, v0}, LX/94J;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1434859
    :cond_6
    const-string p0, "visibility"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1434860
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1434861
    :cond_7
    const-string p0, "work_communities"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1434862
    invoke-static {p1, v0}, LX/94K;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1434863
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1434864
    :cond_9
    const/4 v11, 0x7

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1434865
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1434866
    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1434867
    if-eqz v1, :cond_a

    .line 1434868
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 1434869
    :cond_a
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1434870
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1434871
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1434872
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1434873
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
