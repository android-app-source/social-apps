.class public final Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1435229
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;

    new-instance v1, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1435230
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1435228
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1435210
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1435211
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1435212
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1435213
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1435214
    if-eqz v2, :cond_0

    .line 1435215
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435216
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1435217
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1435218
    if-eqz v2, :cond_1

    .line 1435219
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435220
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1435221
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1435222
    if-eqz v2, :cond_2

    .line 1435223
    const-string p0, "posted_item_privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1435224
    invoke-static {v1, v2, p1, p2}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1435225
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1435226
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1435227
    check-cast p1, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel$Serializer;->a(Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;LX/0nX;LX/0my;)V

    return-void
.end method
