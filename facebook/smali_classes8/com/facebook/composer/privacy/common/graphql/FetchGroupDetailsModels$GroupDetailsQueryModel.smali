.class public final Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x79107477
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1435075
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1435076
    const-class v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1435077
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1435078
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1435064
    iput-object p1, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1435065
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1435066
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1435067
    if-eqz v0, :cond_0

    .line 1435068
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1435069
    :cond_0
    return-void

    .line 1435070
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435079
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->f:Ljava/lang/String;

    .line 1435080
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435081
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->h:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->h:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    .line 1435082
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->h:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    return-object v0
.end method

.method private l()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435083
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->i:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->i:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    .line 1435084
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->i:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1435085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1435086
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1435087
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1435088
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1435089
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->l()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1435090
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1435091
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->go_()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x55463c8

    invoke-static {v6, v5, v7}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1435092
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1435093
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1435094
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1435095
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1435096
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1435097
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1435098
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1435099
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1435100
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1435101
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1435102
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1435103
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1435104
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    .line 1435105
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1435106
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    .line 1435107
    iput-object v0, v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->h:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    .line 1435108
    :cond_0
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->l()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1435109
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->l()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    .line 1435110
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->l()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1435111
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    .line 1435112
    iput-object v0, v1, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->i:Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    .line 1435113
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->go_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1435114
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->go_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x55463c8

    invoke-static {v2, v0, v3}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1435115
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->go_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1435116
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    .line 1435117
    iput v3, v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k:I

    move-object v1, v0

    .line 1435118
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1435119
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1435120
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 1435121
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1435122
    new-instance v0, LX/94G;

    invoke-direct {v0, p1}, LX/94G;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435123
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1435071
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1435072
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->g:Z

    .line 1435073
    const/4 v0, 0x6

    const v1, 0x55463c8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k:I

    .line 1435074
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1435051
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1435052
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1435053
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1435054
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 1435055
    :goto_0
    return-void

    .line 1435056
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1435041
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1435042
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1435043
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1435044
    new-instance v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    invoke-direct {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;-><init>()V

    .line 1435045
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1435046
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435047
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->e:Ljava/lang/String;

    .line 1435048
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1435049
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1435050
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->g:Z

    return v0
.end method

.method public final synthetic d()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435040
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1435057
    const v0, 0x3634b31c

    return v0
.end method

.method public final synthetic e()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435058
    invoke-direct {p0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->l()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1435059
    const v0, 0x41e065f

    return v0
.end method

.method public final gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435060
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1435061
    iget-object v0, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method public final go_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getWorkCommunities"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1435062
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1435063
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
