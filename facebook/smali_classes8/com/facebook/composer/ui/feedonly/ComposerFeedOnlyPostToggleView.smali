.class public Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CompoundButton;

.field private d:Z

.field private e:Landroid/view/animation/AlphaAnimation;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1674215
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1674216
    invoke-direct {p0}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a()V

    .line 1674217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1674218
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1674219
    invoke-direct {p0}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a()V

    .line 1674220
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1674208
    const v0, 0x7f03031b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1674209
    const v0, 0x7f0d0a78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->b:Landroid/widget/TextView;

    .line 1674210
    const v0, 0x7f0d0a79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a:Landroid/widget/TextView;

    .line 1674211
    const v0, 0x7f0d0a7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->c:Landroid/widget/CompoundButton;

    .line 1674212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->d:Z

    .line 1674213
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->e:Landroid/view/animation/AlphaAnimation;

    .line 1674214
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 1674221
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1674222
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1674223
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->e:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1674224
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->e:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1674225
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->e:Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 1674226
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->f:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1674227
    return-void

    .line 1674228
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->g:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1674205
    iput-object p1, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->f:Ljava/lang/String;

    .line 1674206
    iput-object p2, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->g:Ljava/lang/String;

    .line 1674207
    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1674203
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1674204
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1674195
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1674196
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1674197
    return-void
.end method

.method public setToggleChecked(Z)V
    .locals 1

    .prologue
    .line 1674198
    iget-boolean v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->d:Z

    if-eq p1, v0, :cond_0

    .line 1674199
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a(Z)V

    .line 1674200
    iput-boolean p1, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->d:Z

    .line 1674201
    iget-object v0, p0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1674202
    :cond_0
    return-void
.end method
