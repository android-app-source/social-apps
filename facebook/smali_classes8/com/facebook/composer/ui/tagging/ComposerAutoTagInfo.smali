.class public Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1674298
    const-class v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1674299
    const-class v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1674300
    new-instance v0, LX/ASc;

    invoke-direct {v0}, LX/ASc;-><init>()V

    sput-object v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1674305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674306
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    .line 1674307
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    .line 1674308
    return-void

    .line 1674309
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;)V
    .locals 2

    .prologue
    .line 1674301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674302
    iget-boolean v0, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    .line 1674303
    iget-wide v0, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    .line 1674304
    return-void
.end method

.method public static a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;
    .locals 2

    .prologue
    .line 1674279
    new-instance v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;-><init>(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;
    .locals 2

    .prologue
    .line 1674297
    new-instance v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1674296
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1674287
    if-ne p0, p1, :cond_1

    .line 1674288
    :cond_0
    :goto_0
    return v0

    .line 1674289
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 1674290
    goto :goto_0

    .line 1674291
    :cond_2
    check-cast p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 1674292
    iget-boolean v2, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1674293
    goto :goto_0

    .line 1674294
    :cond_3
    iget-wide v2, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    iget-wide v4, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 1674295
    goto :goto_0
.end method

.method public getPostDelayStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_delay_start_time"
    .end annotation

    .prologue
    .line 1674286
    iget-wide v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    return-wide v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1674285
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public shouldForceStopAutoTagging()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "force_stop_auto_tagging"
    .end annotation

    .prologue
    .line 1674284
    iget-boolean v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1674280
    iget-boolean v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1674281
    iget-wide v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1674282
    return-void

    .line 1674283
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
