.class public Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1674321
    const-class v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    new-instance v1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1674322
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1674310
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1674311
    if-nez p0, :cond_0

    .line 1674312
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1674313
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1674314
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;->b(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;LX/0nX;LX/0my;)V

    .line 1674315
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1674316
    return-void
.end method

.method private static b(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1674317
    const-string v0, "force_stop_auto_tagging"

    invoke-virtual {p0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->shouldForceStopAutoTagging()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1674318
    const-string v0, "post_delay_start_time"

    invoke-virtual {p0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->getPostDelayStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1674319
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1674320
    check-cast p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSerializer;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;LX/0nX;LX/0my;)V

    return-void
.end method
