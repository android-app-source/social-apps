.class public final Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1674273
    new-instance v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Deserializer;->a:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1674274
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1674275
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;
    .locals 1

    .prologue
    .line 1674276
    sget-object v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Deserializer;->a:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    .line 1674277
    invoke-virtual {v0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1674278
    invoke-static {p1, p2}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v0

    return-object v0
.end method
