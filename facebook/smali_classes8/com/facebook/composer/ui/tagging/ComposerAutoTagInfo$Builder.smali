.class public final Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Z

.field public b:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1674272
    const-class v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1674270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674271
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)V
    .locals 2

    .prologue
    .line 1674261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674262
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674263
    instance-of v0, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    if-eqz v0, :cond_0

    .line 1674264
    check-cast p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 1674265
    iget-boolean v0, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a:Z

    iput-boolean v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a:Z

    .line 1674266
    iget-wide v0, p1, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->b:J

    iput-wide v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->b:J

    .line 1674267
    :goto_0
    return-void

    .line 1674268
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->shouldForceStopAutoTagging()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a:Z

    .line 1674269
    invoke-virtual {p1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->getPostDelayStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->b:J

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;
    .locals 2

    .prologue
    .line 1674256
    new-instance v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;-><init>(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;)V

    return-object v0
.end method

.method public setForceStopAutoTagging(Z)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "force_stop_auto_tagging"
    .end annotation

    .prologue
    .line 1674259
    iput-boolean p1, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a:Z

    .line 1674260
    return-object p0
.end method

.method public setPostDelayStartTime(J)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_delay_start_time"
    .end annotation

    .prologue
    .line 1674257
    iput-wide p1, p0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->b:J

    .line 1674258
    return-object p0
.end method
