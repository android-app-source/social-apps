.class public Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Landroid/graphics/Path;

.field private f:Landroid/graphics/Paint;

.field private g:Z

.field public h:Z

.field public i:LX/0hs;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1435540
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1435541
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->g:Z

    .line 1435542
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a(Landroid/content/Context;)V

    .line 1435543
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1435536
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1435537
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->g:Z

    .line 1435538
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a(Landroid/content/Context;)V

    .line 1435539
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1435532
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1435533
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->g:Z

    .line 1435534
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a(Landroid/content/Context;)V

    .line 1435535
    return-void
.end method

.method private static a(II)Landroid/graphics/Path;
    .locals 3

    .prologue
    .line 1435527
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 1435528
    int-to-float v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1435529
    int-to-float v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    int-to-float v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1435530
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1435531
    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1435521
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setCursorVisible(Z)V

    .line 1435522
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setFocusable(Z)V

    .line 1435523
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setLines(I)V

    .line 1435524
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->c(Landroid/content/Context;)V

    .line 1435525
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->b(Landroid/content/Context;)V

    .line 1435526
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1435504
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1435505
    iget v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->b:I

    .line 1435506
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getWidth()I

    move-result v1

    .line 1435507
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getHeight()I

    move-result v2

    .line 1435508
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v3

    .line 1435509
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v4

    .line 1435510
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getScrollX()I

    move-result v5

    .line 1435511
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getScrollY()I

    move-result v6

    .line 1435512
    int-to-float v5, v5

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1435513
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getLayout()Landroid/text/Layout;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v5

    float-to-int v5, v5

    .line 1435514
    invoke-static {}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    add-int v0, v5, v3

    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a:I

    add-int/2addr v0, v1

    .line 1435515
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getTotalPaddingTop()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getTotalPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->c:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->d:I

    add-int/2addr v1, v2

    .line 1435516
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1435517
    iget-object v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->e:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1435518
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1435519
    return-void

    .line 1435520
    :cond_0
    sub-int/2addr v1, v5

    sub-int/2addr v1, v4

    sub-int v0, v1, v0

    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private static a()Z
    .locals 1

    .prologue
    .line 1435503
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1435501
    new-instance v0, LX/94a;

    invoke-direct {v0, p0, p1}, LX/94a;-><init>(Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1435502
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1435473
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1435474
    const v1, 0x7f0b0c9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a:I

    .line 1435475
    const v1, 0x7f0b0c9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->b:I

    .line 1435476
    const v1, 0x7f0b0c9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->c:I

    .line 1435477
    const v1, 0x7f0b0c9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->d:I

    .line 1435478
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->f:Landroid/graphics/Paint;

    .line 1435479
    iget-object v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1435480
    iget v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->c:I

    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->b:I

    invoke-static {v0, v1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a(II)Landroid/graphics/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->e:Landroid/graphics/Path;

    .line 1435481
    return-void
.end method


# virtual methods
.method public getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 1435497
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v0

    .line 1435498
    invoke-static {}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1435499
    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->b:I

    add-int/2addr v0, v1

    .line 1435500
    :cond_0
    return v0
.end method

.method public getCompoundPaddingRight()I
    .locals 2

    .prologue
    .line 1435493
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v0

    .line 1435494
    invoke-static {}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1435495
    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->b:I

    add-int/2addr v0, v1

    .line 1435496
    :cond_0
    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1435489
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1435490
    iget-boolean v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->g:Z

    if-eqz v0, :cond_0

    .line 1435491
    invoke-direct {p0, p1}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->a(Landroid/graphics/Canvas;)V

    .line 1435492
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1435486
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 1435487
    invoke-static {p0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->h:Z

    .line 1435488
    return-void
.end method

.method public setShouldShowTriangle(Z)V
    .locals 1

    .prologue
    .line 1435482
    iget-boolean v0, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->g:Z

    if-eq p1, v0, :cond_0

    .line 1435483
    iput-boolean p1, p0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->g:Z

    .line 1435484
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->invalidate()V

    .line 1435485
    :cond_0
    return-void
.end method
