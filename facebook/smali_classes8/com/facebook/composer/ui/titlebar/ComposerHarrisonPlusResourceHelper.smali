.class public Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1435357
    const-class v0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1435358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435359
    return-void
.end method

.method public static a(LX/0zw;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1435360
    if-eqz p1, :cond_1

    .line 1435361
    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1435362
    :cond_0
    :goto_0
    return-void

    .line 1435363
    :cond_1
    invoke-virtual {p0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1435364
    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
