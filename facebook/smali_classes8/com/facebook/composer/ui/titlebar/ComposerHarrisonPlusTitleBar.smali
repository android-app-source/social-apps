.class public Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;
.super LX/94V;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1435365
    invoke-direct {p0, p1}, LX/94V;-><init>(Landroid/content/Context;)V

    .line 1435366
    invoke-direct {p0}, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->a()V

    .line 1435367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1435405
    invoke-direct {p0, p1, p2}, LX/94V;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1435406
    invoke-direct {p0}, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->a()V

    .line 1435407
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1435401
    iget-object v0, p0, LX/94V;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03031e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1435402
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1601

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->c:LX/0zw;

    .line 1435403
    new-instance v1, LX/0zw;

    const v0, 0x7f0d15ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->d:LX/0zw;

    .line 1435404
    return-void
.end method


# virtual methods
.method public final a(LX/94c;)V
    .locals 6

    .prologue
    .line 1435378
    iget-object v0, p0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->c:LX/0zw;

    iget-object v1, p0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->d:LX/0zw;

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1435379
    sget-object v2, LX/94W;->a:[I

    .line 1435380
    iget-object v5, p1, LX/94c;->a:LX/94b;

    move-object v5, v5

    .line 1435381
    invoke-virtual {v5}, LX/94b;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 1435382
    :cond_0
    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v5, 0x7f020697

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    move v2, v3

    .line 1435383
    :goto_0
    invoke-static {v0, v2}, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;->a(LX/0zw;Z)V

    .line 1435384
    if-nez v2, :cond_1

    :goto_1
    invoke-static {v1, v4}, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;->a(LX/0zw;Z)V

    .line 1435385
    return-void

    .line 1435386
    :pswitch_0
    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v5, 0x7f020083

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    move v2, v3

    .line 1435387
    goto :goto_0

    .line 1435388
    :pswitch_1
    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v5, 0x7f020c34

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    move v2, v3

    .line 1435389
    goto :goto_0

    .line 1435390
    :pswitch_2
    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1435391
    iget-object v5, p1, LX/94c;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1435392
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object p0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    move v2, v4

    .line 1435393
    goto :goto_0

    .line 1435394
    :pswitch_3
    iget-object v2, p1, LX/94c;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1435395
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1435396
    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1435397
    iget-object v5, p1, LX/94c;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1435398
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object p0, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusResourceHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    move v2, v4

    .line 1435399
    goto :goto_0

    :cond_1
    move v4, v3

    .line 1435400
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getPrimaryButtonDivider()Landroid/view/View;
    .locals 1

    .prologue
    .line 1435377
    const v0, 0x7f0d1603

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;
    .locals 1

    .prologue
    .line 1435376
    const v0, 0x7f0d1604

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    return-object v0
.end method

.method public getSecondaryButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 1435375
    const v0, 0x7f0d15fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getTitleTextView()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;
    .locals 1

    .prologue
    .line 1435374
    const v0, 0x7f0d0a0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 1435368
    invoke-super {p0, p1}, LX/94V;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1435369
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->getTitleTextView()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    .line 1435370
    invoke-virtual {p0}, Lcom/facebook/composer/ui/titlebar/ComposerHarrisonPlusTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0429

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1435371
    invoke-static {v0, v1}, LX/46y;->a(Landroid/view/View;I)V

    .line 1435372
    invoke-static {p0, v1}, LX/46y;->a(Landroid/view/View;I)V

    .line 1435373
    return-void
.end method
