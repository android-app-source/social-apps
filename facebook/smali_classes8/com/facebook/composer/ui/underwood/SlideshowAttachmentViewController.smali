.class public Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/ASn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0jA;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/ASn;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:Z

.field public final d:LX/ATL;

.field private final e:Landroid/content/Context;

.field private final f:LX/1Ad;

.field public final g:LX/3l1;

.field private final h:LX/ASp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/composer/attachments/ComposerAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1674630
    const-class v0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0il;Ljava/lang/Integer;LX/ATL;Landroid/content/Context;LX/1Ad;LX/BXn;LX/3l1;)V
    .locals 3
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ATL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Ljava/lang/Integer;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "Landroid/content/Context;",
            "LX/1Ad;",
            "LX/BXn;",
            "LX/3l1;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1674614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674615
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    .line 1674616
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->c:Z

    .line 1674617
    iput-object p3, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->d:LX/ATL;

    .line 1674618
    iput-object p4, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->e:Landroid/content/Context;

    .line 1674619
    iput-object p5, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->f:LX/1Ad;

    .line 1674620
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->e:Landroid/content/Context;

    invoke-virtual {p6}, LX/BXn;->a()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->k:I

    .line 1674621
    iput-object p7, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->g:LX/3l1;

    .line 1674622
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->c:Z

    if-eqz v0, :cond_1

    .line 1674623
    new-instance v0, LX/ASp;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/ASp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674624
    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->i:Landroid/view/View;

    .line 1674625
    :goto_1
    return-void

    .line 1674626
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1674627
    :cond_1
    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674628
    new-instance v0, Landroid/view/ViewStub;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->i:Landroid/view/View;

    .line 1674629
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1674596
    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674597
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    if-eqz v0, :cond_0

    .line 1674598
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    const/4 v1, 0x0

    .line 1674599
    iput v1, v0, LX/ASp;->b:F

    .line 1674600
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    invoke-virtual {v0, v2}, LX/ASp;->setController(LX/1aZ;)V

    .line 1674601
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674602
    iget-object v1, v0, LX/ASp;->e:Landroid/widget/ImageView;

    move-object v0, v1

    .line 1674603
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674604
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674605
    iget-object v1, v0, LX/ASp;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1674606
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674607
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674608
    iget-object v1, v0, LX/ASp;->c:Landroid/view/View;

    move-object v0, v1

    .line 1674609
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674610
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674611
    iget-object v1, v0, LX/ASp;->f:Landroid/view/View;

    move-object v0, v1

    .line 1674612
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674613
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1674592
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    if-eqz v0, :cond_0

    .line 1674593
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    invoke-virtual {v0, p1}, LX/ASp;->setScale(F)V

    .line 1674594
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    invoke-virtual {v0, p1}, LX/ASp;->setAlpha(F)V

    .line 1674595
    :cond_0
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1674591
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 5
    .param p1    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1674567
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674568
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1674569
    :cond_0
    :goto_0
    return-void

    .line 1674570
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674571
    iput v1, v0, LX/ASp;->b:F

    .line 1674572
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    invoke-virtual {v0, v1}, LX/ASp;->setScale(F)V

    .line 1674573
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->f:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v2, LX/1o9;

    iget v3, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->k:I

    iget v4, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->k:I

    invoke-direct {v2, v3, v4}, LX/1o9;-><init>(II)V

    .line 1674574
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 1674575
    move-object v1, v1

    .line 1674576
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1674577
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    invoke-virtual {v1, v0}, LX/ASp;->setController(LX/1aZ;)V

    .line 1674578
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674579
    iget-object v1, v0, LX/ASp;->e:Landroid/widget/ImageView;

    move-object v0, v1

    .line 1674580
    new-instance v1, LX/ASq;

    invoke-direct {v1, p0}, LX/ASq;-><init>(Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674581
    new-instance v0, LX/ASr;

    invoke-direct {v0, p0}, LX/ASr;-><init>(Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;)V

    .line 1674582
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674583
    iget-object v2, v1, LX/ASp;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v2

    .line 1674584
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674585
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674586
    iget-object v2, v1, LX/ASp;->c:Landroid/view/View;

    move-object v1, v2

    .line 1674587
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674588
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674589
    iget-object v2, v1, LX/ASp;->f:Landroid/view/View;

    move-object v1, v2

    .line 1674590
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/data/MediaData;Z)V
    .locals 0

    .prologue
    .line 1674566
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1674550
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->i:Landroid/view/View;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 1

    .prologue
    .line 1674564
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1674565
    if-eqz p1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jA;

    invoke-interface {v0}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1674563
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    return-object v0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 0

    .prologue
    .line 1674561
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674562
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1674560
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1674559
    return-void
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1674556
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674557
    iget p0, v0, LX/ASp;->b:F

    move v0, p0

    .line 1674558
    goto :goto_0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1674553
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->h:LX/ASp;

    .line 1674554
    iget p0, v0, LX/ASp;->a:F

    move v0, p0

    .line 1674555
    goto :goto_0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1674552
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1674551
    return-void
.end method
