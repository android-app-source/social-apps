.class public Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/video/player/RichVideoPlayer;

.field public c:F

.field public d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1675260
    const-class v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1675240
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1675241
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1675258
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1675259
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1675246
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1675247
    const v0, 0x7f03156d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1675248
    const v0, 0x7f0d3037

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1675249
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, LX/AT8;

    invoke-direct {p1, p0}, LX/AT8;-><init>(Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1675250
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1675251
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1675252
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p3, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p1, p2, p3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1675253
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1675254
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, LX/3Ig;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, LX/3Ig;-><init>(Landroid/content/Context;)V

    .line 1675255
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1675256
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->c:F

    .line 1675257
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1675242
    iget v0, p0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->d:F

    move v1, v0

    .line 1675243
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    new-instance v2, LX/ATB;

    invoke-direct {v2, p1, p2}, LX/ATB;-><init>(II)V

    invoke-static {v1, v0, v2}, LX/ATC;->a(FLandroid/widget/FrameLayout$LayoutParams;LX/ATB;)LX/ATB;

    move-result-object v0

    .line 1675244
    iget v1, v0, LX/ATB;->a:I

    iget v0, v0, LX/ATB;->b:I

    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1675245
    return-void
.end method
