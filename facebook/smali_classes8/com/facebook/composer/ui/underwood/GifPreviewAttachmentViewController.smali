.class public Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/ASn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j3;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/ASn;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/ASg;

.field private final d:Landroid/content/Context;

.field public final e:LX/ATL;

.field private final f:LX/1Ad;

.field private final g:Landroid/view/View$OnClickListener;

.field private final h:LX/1cC;

.field private final i:LX/0ad;

.field public j:Lcom/facebook/composer/attachments/ComposerAttachment;

.field public k:LX/ASh;

.field public l:LX/ASm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1674510
    const-class v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/ASg;Landroid/content/Context;LX/1Ad;LX/0ad;LX/0il;LX/ATL;)V
    .locals 2
    .param p5    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/ATL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ASg;",
            "Landroid/content/Context;",
            "LX/1Ad;",
            "LX/0ad;",
            "TServices;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1674490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674491
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->c:LX/ASg;

    .line 1674492
    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->d:Landroid/content/Context;

    .line 1674493
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    .line 1674494
    iput-object p6, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->e:LX/ATL;

    .line 1674495
    iput-object p3, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->f:LX/1Ad;

    .line 1674496
    iput-object p4, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->i:LX/0ad;

    .line 1674497
    new-instance v0, LX/ASh;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/ASh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674498
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674499
    iget-object v1, v0, LX/ASh;->b:Landroid/widget/ImageView;

    move-object v0, v1

    .line 1674500
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674501
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674502
    iget-object v1, v0, LX/ASh;->b:Landroid/widget/ImageView;

    move-object v0, v1

    .line 1674503
    new-instance v1, LX/ASi;

    invoke-direct {v1, p0}, LX/ASi;-><init>(Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674504
    sget-object v0, LX/ASm;->PAUSE_ON_CLICK:LX/ASm;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->l:LX/ASm;

    .line 1674505
    new-instance v0, LX/ASk;

    invoke-direct {v0, p0}, LX/ASk;-><init>(Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;)V

    move-object v0, v0

    .line 1674506
    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->g:Landroid/view/View$OnClickListener;

    .line 1674507
    new-instance v0, LX/ASj;

    invoke-direct {v0, p0}, LX/ASj;-><init>(Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;)V

    move-object v0, v0

    .line 1674508
    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->h:LX/1cC;

    .line 1674509
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1674486
    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674487
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/ASh;->setAspectRatio(F)V

    .line 1674488
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    invoke-virtual {v0, v2}, LX/ASh;->setController(LX/1aZ;)V

    .line 1674489
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1674479
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674480
    iput p1, v0, LX/ASh;->c:F

    .line 1674481
    invoke-virtual {v0, p1}, LX/ASh;->setScaleX(F)V

    .line 1674482
    invoke-virtual {v0, p1}, LX/ASh;->setScaleY(F)V

    .line 1674483
    invoke-virtual {v0, p1}, LX/ASh;->setAlpha(F)V

    .line 1674484
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    invoke-virtual {v0, p1}, LX/ASh;->setAlpha(F)V

    .line 1674485
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1674478
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 5

    .prologue
    .line 1674459
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674460
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1674461
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1674462
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v1, v2

    .line 1674463
    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1674464
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v1, v2

    .line 1674465
    if-lez v1, :cond_0

    .line 1674466
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1674467
    iget v3, v2, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v2, v3

    .line 1674468
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 1674469
    iget v4, v3, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v3, v4

    .line 1674470
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v4

    invoke-static {v2, v3, v4}, LX/74d;->a(III)F

    move-result v2

    invoke-virtual {v1, v2}, LX/ASh;->setAspectRatio(F)V

    .line 1674471
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->f:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->h:LX/1cC;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1674472
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    invoke-virtual {v1, v0}, LX/ASh;->setController(LX/1aZ;)V

    .line 1674473
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->g:Landroid/view/View$OnClickListener;

    .line 1674474
    if-nez v1, :cond_1

    .line 1674475
    iget-object v2, v0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setClickable(Z)V

    .line 1674476
    :goto_0
    return-void

    .line 1674477
    :cond_1
    iget-object v2, v0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/data/MediaData;Z)V
    .locals 0

    .prologue
    .line 1674458
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1674442
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    return-object v0
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1674456
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1674457
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1674455
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    return-object v0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 1

    .prologue
    .line 1674453
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674454
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1674452
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1674451
    return-void
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1674448
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674449
    iget p0, v0, LX/ASh;->d:F

    move v0, p0

    .line 1674450
    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1674445
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674446
    iget p0, v0, LX/ASh;->c:F

    move v0, p0

    .line 1674447
    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1674444
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1674443
    return-void
.end method
