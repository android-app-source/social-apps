.class public Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1675135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1675136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1675133
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1675134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1675129
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1675130
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->b:F

    .line 1675131
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a()V

    .line 1675132
    return-void
.end method

.method private a(F)F
    .locals 1

    .prologue
    .line 1675128
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1675085
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a:Landroid/graphics/Paint;

    .line 1675086
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1675087
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1675088
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1675089
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {p0, v1}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1675090
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->g:Landroid/graphics/Path;

    .line 1675091
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFFF)V
    .locals 2

    .prologue
    .line 1675122
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->g:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1675123
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->g:Landroid/graphics/Path;

    invoke-virtual {v0, p4, p3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1675124
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->g:Landroid/graphics/Path;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1675125
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->g:Landroid/graphics/Path;

    invoke-virtual {v0, p2, p5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1675126
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->g:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1675127
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    .line 1675095
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1675096
    iget v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->b:F

    const v1, 0x3d4ccccd    # 0.05f

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-direct {p0, v3}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 1675097
    invoke-direct {p0, v3}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(F)F

    move-result v1

    .line 1675098
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-direct {p0, v2}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(F)F

    move-result v2

    .line 1675099
    add-float v6, v1, v0

    .line 1675100
    add-float v7, v2, v6

    .line 1675101
    iput v6, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    .line 1675102
    iput v7, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    .line 1675103
    iput v6, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    .line 1675104
    iput v7, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    .line 1675105
    iget v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    iget v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    iget v4, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    iget v5, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(Landroid/graphics/Canvas;FFFF)V

    .line 1675106
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    .line 1675107
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v7

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    .line 1675108
    iput v6, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    .line 1675109
    iput v7, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    .line 1675110
    iget v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    iget v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    iget v4, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    iget v5, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(Landroid/graphics/Canvas;FFFF)V

    .line 1675111
    iput v6, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    .line 1675112
    iput v7, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    .line 1675113
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    .line 1675114
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v7

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    .line 1675115
    iget v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    iget v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    iget v4, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    iget v5, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(Landroid/graphics/Canvas;FFFF)V

    .line 1675116
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    .line 1675117
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v7

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    .line 1675118
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    .line 1675119
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v7

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    .line 1675120
    iget v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->c:F

    iget v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->e:F

    iget v4, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->d:F

    iget v5, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->f:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->a(Landroid/graphics/Canvas;FFFF)V

    .line 1675121
    return-void
.end method

.method public setProgress(F)V
    .locals 0

    .prologue
    .line 1675092
    iput p1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->b:F

    .line 1675093
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->invalidate()V

    .line 1675094
    return-void
.end method
