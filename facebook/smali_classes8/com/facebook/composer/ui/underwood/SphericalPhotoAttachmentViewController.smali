.class public Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/ASn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/ASn;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/ASg;

.field private final d:LX/1Ad;

.field public final e:LX/ATL;

.field private final f:LX/7Dh;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0W3;

.field public i:Lcom/facebook/composer/attachments/ComposerAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:LX/AT1;

.field private final k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1675003
    const-class v0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/ATL;Ljava/lang/String;Landroid/content/Context;LX/1Ad;LX/ASg;LX/7Dh;LX/0Ot;LX/0W3;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/ATL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "LX/1Ad;",
            "LX/ASg;",
            "LX/7Dh;",
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1675020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1675021
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    .line 1675022
    iput-object p6, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->c:LX/ASg;

    .line 1675023
    iput-object p5, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->d:LX/1Ad;

    .line 1675024
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-static {p4, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->k:I

    .line 1675025
    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->e:LX/ATL;

    .line 1675026
    iput-object p7, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->f:LX/7Dh;

    .line 1675027
    iput-object p8, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->g:LX/0Ot;

    .line 1675028
    iput-object p9, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->h:LX/0W3;

    .line 1675029
    new-instance v2, LX/AT1;

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->e:LX/ATL;

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v2, p4, v3, p3, v0}, LX/AT1;-><init>(Landroid/content/Context;LX/ATL;Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1675030
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1675031
    iget-object v1, v0, LX/AT1;->j:Landroid/widget/ImageView;

    move-object v0, v1

    .line 1675032
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675033
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1675034
    iget-object v1, v0, LX/AT1;->j:Landroid/widget/ImageView;

    move-object v0, v1

    .line 1675035
    new-instance v1, LX/AT2;

    invoke-direct {v1, p0}, LX/AT2;-><init>(Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1675036
    return-void

    .line 1675037
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1675038
    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675039
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    const/4 v1, 0x0

    .line 1675040
    iput v1, v0, LX/AT1;->F:F

    .line 1675041
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v0, v2}, LX/AT1;->setController(LX/1aZ;)V

    .line 1675042
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1675043
    iget-object v1, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1}, LX/2qW;->d()V

    .line 1675044
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1675045
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v0, p1}, LX/AT1;->setScale(F)V

    .line 1675046
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 5

    .prologue
    .line 1675064
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-nez v0, :cond_1

    .line 1675065
    :cond_0
    :goto_0
    return-void

    .line 1675066
    :cond_1
    sget-object v0, LX/5L2;->ON_USER_POST:LX/5L2;

    if-ne p1, v0, :cond_0

    .line 1675067
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675068
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1675069
    iget-boolean v2, v1, LX/AT1;->A:Z

    move v1, v2

    .line 1675070
    iput-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    .line 1675071
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1675072
    iget-boolean v2, v1, LX/AT1;->A:Z

    move v1, v2

    .line 1675073
    if-eqz v1, :cond_0

    .line 1675074
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v1, v1

    .line 1675075
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v2}, LX/AT1;->getPitch()F

    move-result v2

    float-to-double v2, v2

    .line 1675076
    iput-wide v2, v1, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewPitchDegrees:D

    .line 1675077
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v0, v1

    .line 1675078
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v1}, LX/AT1;->getYaw()F

    move-result v1

    float-to-double v2, v1

    .line 1675079
    iput-wide v2, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewHeadingDegrees:D

    .line 1675080
    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 5
    .param p1    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1675047
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675048
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-nez v0, :cond_0

    .line 1675049
    :goto_0
    return-void

    .line 1675050
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->c:LX/ASg;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ASg;->a(Lcom/facebook/ipc/media/MediaItem;)F

    move-result v1

    .line 1675051
    iput v1, v0, LX/AT1;->F:F

    .line 1675052
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/AT1;->setScale(F)V

    .line 1675053
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->d:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v2, LX/1o9;

    iget v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->k:I

    iget v4, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->k:I

    invoke-direct {v2, v3, v4}, LX/1o9;-><init>(II)V

    .line 1675054
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 1675055
    move-object v1, v1

    .line 1675056
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1675057
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v1, v0}, LX/AT1;->setController(LX/1aZ;)V

    .line 1675058
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    sget-object v1, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1675059
    iput-object p1, v0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675060
    iput-object v1, v0, LX/AT1;->r:Lcom/facebook/common/callercontext/CallerContext;

    .line 1675061
    iget-object v2, v0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v2}, Lcom/facebook/photos/base/media/PhotoItem;->v()Z

    move-result v2

    invoke-static {v0, v2}, LX/AT1;->setSphericalUploadState(LX/AT1;Z)V

    .line 1675062
    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/data/MediaData;Z)V
    .locals 0

    .prologue
    .line 1675063
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1675004
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    return-object v0
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1675005
    invoke-static {p1}, LX/7kq;->d(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->f:LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1675006
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675007
    iget-boolean v2, v0, Lcom/facebook/photos/base/media/PhotoItem;->f:Z

    move v2, v2

    .line 1675008
    if-eqz v2, :cond_0

    .line 1675009
    iget-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v1

    .line 1675010
    :goto_0
    return v0

    .line 1675011
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->h:LX/0W3;

    invoke-static {v2, v3}, LX/7E2;->a(Ljava/lang/String;LX/0W3;)LX/7E1;

    move-result-object v2

    .line 1675012
    iget-boolean v3, v2, LX/7E1;->b:Z

    move v3, v3

    .line 1675013
    if-eqz v3, :cond_1

    .line 1675014
    iput-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    .line 1675015
    iget-object v3, v2, LX/7E1;->a:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v2, v3

    .line 1675016
    iput-object v2, v0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1675017
    move v0, v1

    .line 1675018
    goto :goto_0

    .line 1675019
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1674947
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    return-object v0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 0

    .prologue
    .line 1674948
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1674949
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1674950
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 1674951
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1674952
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1674953
    iget-object v3, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v3}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getMeasuredHeight()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v3}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getMeasuredWidth()I

    move-result v3

    if-nez v3, :cond_5

    :cond_0
    move v1, v2

    .line 1674954
    :cond_1
    :goto_0
    move v2, v1

    .line 1674955
    iget-object v3, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v3, v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setVisibility(I)V

    .line 1674956
    iget-boolean v1, v0, LX/AT1;->D:Z

    if-ne v1, v2, :cond_3

    .line 1674957
    :goto_2
    return-void

    .line 1674958
    :cond_2
    const/4 v1, 0x4

    goto :goto_1

    .line 1674959
    :cond_3
    iput-boolean v2, v0, LX/AT1;->D:Z

    .line 1674960
    if-eqz v2, :cond_4

    .line 1674961
    invoke-static {v0}, LX/AT1;->n(LX/AT1;)V

    goto :goto_2

    .line 1674962
    :cond_4
    iget-object v1, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1}, LX/2qW;->e()V

    .line 1674963
    iget-object v1, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1}, LX/2qW;->c()V

    .line 1674964
    goto :goto_2

    .line 1674965
    :cond_5
    const/4 v3, 0x2

    new-array v3, v3, [I

    iget-object v4, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v4}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getLeft()I

    move-result v4

    aput v4, v3, v2

    iget-object v4, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v4}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getTop()I

    move-result v4

    aput v4, v3, v1

    .line 1674966
    iget-object v4, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v4, v3}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getLocationOnScreen([I)V

    .line 1674967
    aget v4, v3, v1

    iget-object p0, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getMeasuredHeight()I

    move-result p0

    add-int/2addr v4, p0

    .line 1674968
    if-ltz v4, :cond_6

    aget v3, v3, v1

    iget v4, v0, LX/AT1;->H:I

    if-le v3, v4, :cond_1

    :cond_6
    move v1, v2

    .line 1674969
    goto :goto_0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1674970
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1674971
    iget p0, v0, LX/AT1;->F:F

    move v0, p0

    .line 1674972
    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1674973
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1674974
    iget p0, v0, LX/AT1;->E:F

    move v0, p0

    .line 1674975
    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1674976
    return-void
.end method

.method public final i()V
    .locals 7

    .prologue
    .line 1674977
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_1

    .line 1674978
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1674979
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1674980
    iget-boolean v2, v1, LX/AT1;->A:Z

    move v1, v2

    .line 1674981
    iput-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    .line 1674982
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1674983
    iget-boolean v2, v1, LX/AT1;->A:Z

    move v1, v2

    .line 1674984
    if-eqz v1, :cond_0

    .line 1674985
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v1, v1

    .line 1674986
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v2}, LX/AT1;->getPitch()F

    move-result v2

    float-to-double v2, v2

    .line 1674987
    iput-wide v2, v1, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewPitchDegrees:D

    .line 1674988
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v1, v1

    .line 1674989
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    invoke-virtual {v2}, LX/AT1;->getYaw()F

    move-result v2

    float-to-double v2, v2

    .line 1674990
    iput-wide v2, v1, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewHeadingDegrees:D

    .line 1674991
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    .line 1674992
    iget-boolean v2, v1, LX/AT1;->B:Z

    move v1, v2

    .line 1674993
    if-eqz v1, :cond_1

    .line 1674994
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->j:LX/AT1;

    const/16 v3, 0x200

    .line 1674995
    iget-object v2, v1, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v2, v3, v3}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v2, v2

    .line 1674996
    if-eqz v2, :cond_1

    .line 1674997
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Er;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FB_V_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;->i:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v5}, Landroid/graphics/Bitmap$CompressFormat;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v1, v3, v4, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 1674998
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x50

    invoke-static {v2, v3, v4, v1}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V

    .line 1674999
    invoke-virtual {v0, v1}, Lcom/facebook/photos/base/media/PhotoItem;->a(Ljava/io/File;)V
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1675000
    :cond_1
    :goto_0
    return-void

    .line 1675001
    :catchall_0
    move-exception v0

    throw v0

    .line 1675002
    :catch_0
    goto :goto_0
.end method
