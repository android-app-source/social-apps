.class public final Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/AP5;

.field public final e:LX/2by;

.field public final f:Z

.field public final g:Z

.field public final h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1669958
    new-instance v0, LX/AP2;

    invoke-direct {v0}, LX/AP2;-><init>()V

    sput-object v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/AP3;)V
    .locals 1

    .prologue
    .line 1669959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1669960
    iget-object v0, p1, LX/AP3;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a:Ljava/lang/String;

    .line 1669961
    iget-object v0, p1, LX/AP3;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->b:Ljava/lang/String;

    .line 1669962
    iget-object v0, p1, LX/AP3;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->c:Ljava/lang/String;

    .line 1669963
    iget-object v0, p1, LX/AP3;->d:LX/AP5;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->d:LX/AP5;

    .line 1669964
    iget-object v0, p1, LX/AP3;->e:LX/2by;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    .line 1669965
    iget-boolean v0, p1, LX/AP3;->f:Z

    iput-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->f:Z

    .line 1669966
    iget-boolean v0, p1, LX/AP3;->g:Z

    iput-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->g:Z

    .line 1669967
    iget-boolean v0, p1, LX/AP3;->h:Z

    iput-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->h:Z

    .line 1669968
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1669969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1669970
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a:Ljava/lang/String;

    .line 1669971
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->b:Ljava/lang/String;

    .line 1669972
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->c:Ljava/lang/String;

    .line 1669973
    const-class v0, LX/AP5;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AP5;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->d:LX/AP5;

    .line 1669974
    const-class v0, LX/2by;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2by;

    iput-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    .line 1669975
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->f:Z

    .line 1669976
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->g:Z

    .line 1669977
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->h:Z

    .line 1669978
    return-void
.end method


# virtual methods
.method public final a()LX/AP3;
    .locals 1

    .prologue
    .line 1669979
    new-instance v0, LX/AP3;

    invoke-direct {v0, p0}, LX/AP3;-><init>(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1669980
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1669981
    const-class v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "tooltipTitle"

    iget-object v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "tooltipBody"

    iget-object v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "selectedPrivacyName"

    iget-object v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "tagExpansionEducationType"

    iget-object v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->d:LX/AP5;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "educatorType"

    iget-object v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "selectedMoreOptions"

    iget-boolean v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "reshowFlow"

    iget-boolean v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->g:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "usingDefaultAudience"

    iget-boolean v2, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->h:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1669982
    iget-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1669983
    iget-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1669984
    iget-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1669985
    iget-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->d:LX/AP5;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1669986
    iget-object v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1669987
    iget-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1669988
    iget-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1669989
    iget-boolean v0, p0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1669990
    return-void
.end method
