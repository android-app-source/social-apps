.class public Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j0;",
        ":",
        "LX/0jB;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec$SetsStickerData",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:LX/0jK;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:LX/1Ad;

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final f:LX/Hqp;

.field public final g:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<+",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private i:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1672973
    const-class v0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1672974
    const-class v0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->b:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;Landroid/view/ViewStub;LX/Hqp;LX/1Ad;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Hqp;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/view/ViewStub;",
            "Lcom/facebook/composer/stickerpost/controller/ComposerStickerController$StickerTappedCallback;",
            "LX/1Ad;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672979
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672980
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672981
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672982
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->e:Ljava/lang/ref/WeakReference;

    .line 1672983
    new-instance v0, LX/0zw;

    invoke-direct {v0, p2}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    .line 1672984
    iput-object p3, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->f:LX/Hqp;

    .line 1672985
    iput-object p4, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->d:LX/1Ad;

    .line 1672986
    iput-object p5, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->c:Landroid/content/res/Resources;

    .line 1672987
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1672988
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672989
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    check-cast v0, LX/0jB;

    invoke-interface {v0}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    .line 1672990
    if-nez v1, :cond_0

    .line 1672991
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1672992
    :goto_0
    return-void

    .line 1672993
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1672994
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f0d0ae7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1672995
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, LX/ARU;

    invoke-direct {v2, p0}, LX/ARU;-><init>(Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;)V

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1672996
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget-object v2, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->c:Landroid/content/res/Resources;

    const v3, 0x7f02187f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-direct {v0, v2, v3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1672997
    new-instance v2, LX/1Uo;

    iget-object v3, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->c:Landroid/content/res/Resources;

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1672998
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1672999
    move-object v0, v2

    .line 1673000
    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v0, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1673001
    iget-object v2, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1673002
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1673003
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f0d0ae8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->i:Landroid/widget/ImageView;

    .line 1673004
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->i:Landroid/widget/ImageView;

    new-instance v2, LX/ARV;

    invoke-direct {v2, p0}, LX/ARV;-><init>(Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673005
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1673006
    iget-object v0, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->d:LX/1Ad;

    sget-object v2, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1673007
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1673008
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedDiskUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1673009
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedDiskUri()Ljava/lang/String;

    move-result-object v2

    .line 1673010
    :goto_1
    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    move-object v2, v2

    .line 1673011
    invoke-virtual {v0, v2}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1673012
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1673013
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedDiskUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1673014
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedDiskUri()Ljava/lang/String;

    move-result-object v2

    .line 1673015
    :goto_2
    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    move-object v1, v2

    .line 1673016
    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1673017
    iget-object v1, p0, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto/16 :goto_0

    .line 1673018
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1673019
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1673020
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1673021
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1673022
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedWebUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1673023
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedWebUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1673024
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedWebUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1673025
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1673026
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1673027
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1672977
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1672975
    invoke-virtual {p0}, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->a()V

    .line 1672976
    return-void
.end method
