.class public Lcom/facebook/composer/tip/LinearComposerFooterNuxView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:LX/ASJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1673839
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1673840
    invoke-direct {p0}, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->e()V

    .line 1673841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1673842
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1673843
    invoke-direct {p0}, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->e()V

    .line 1673844
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1673845
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1673846
    invoke-direct {p0}, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->e()V

    .line 1673847
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1673835
    const v0, 0x7f0309eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1673836
    const v0, 0x7f0d1920

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->k:Landroid/widget/TextView;

    .line 1673837
    const v0, 0x7f0d1921

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/ASI;

    invoke-direct {v1, p0}, LX/ASI;-><init>(Lcom/facebook/composer/tip/LinearComposerFooterNuxView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673838
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 1673829
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->setVisibility(I)V

    .line 1673830
    return-void
.end method

.method public setOnDismissListener(LX/ASJ;)V
    .locals 0

    .prologue
    .line 1673833
    iput-object p1, p0, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->j:LX/ASJ;

    .line 1673834
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1673831
    iget-object v0, p0, Lcom/facebook/composer/tip/LinearComposerFooterNuxView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673832
    return-void
.end method
