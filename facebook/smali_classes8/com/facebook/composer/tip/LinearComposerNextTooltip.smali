.class public Lcom/facebook/composer/tip/LinearComposerNextTooltip;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private c:Landroid/view/View;

.field private d:LX/ASM;

.field private e:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1673886
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1673887
    invoke-direct {p0}, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->b()V

    .line 1673888
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1673883
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1673884
    invoke-direct {p0}, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->b()V

    .line 1673885
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1673880
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1673881
    invoke-direct {p0}, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->b()V

    .line 1673882
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1673856
    const v0, 0x7f0309ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1673857
    const v0, 0x7f0d1924

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->a:Landroid/widget/TextView;

    .line 1673858
    const v0, 0x7f0d1922

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->c:Landroid/view/View;

    .line 1673859
    const v0, 0x7f0d1925

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1673860
    new-instance v1, LX/ASK;

    invoke-direct {v1, p0}, LX/ASK;-><init>(Lcom/facebook/composer/tip/LinearComposerNextTooltip;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1673861
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->setVisibility(I)V

    .line 1673862
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->setOrientation(I)V

    .line 1673863
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1673874
    const v0, 0x7f0d1923

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1673875
    new-instance v1, LX/2qr;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/2qr;-><init>(Landroid/view/View;I)V

    .line 1673876
    new-instance v2, LX/ASL;

    invoke-direct {v2, p0}, LX/ASL;-><init>(Lcom/facebook/composer/tip/LinearComposerNextTooltip;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1673877
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1673878
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1673879
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x2486aa8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1673868
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1673869
    iget-object v1, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->e:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 1673870
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 1673871
    iget-object v1, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->b:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->e:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1673872
    :cond_0
    :goto_0
    const v1, 0x245f6acb

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 1673873
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->b:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->e:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public setOnDismissListener(LX/ASM;)V
    .locals 0

    .prologue
    .line 1673866
    iput-object p1, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->d:LX/ASM;

    .line 1673867
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1673864
    iget-object v0, p0, Lcom/facebook/composer/tip/LinearComposerNextTooltip;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673865
    return-void
.end method
