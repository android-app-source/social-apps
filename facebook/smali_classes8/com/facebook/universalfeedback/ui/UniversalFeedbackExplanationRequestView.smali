.class public Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;
.super LX/A89;
.source ""


# instance fields
.field public a:LX/A88;

.field private b:Lcom/facebook/resources/ui/FbButton;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/facebook/widget/text/BetterEditTextView;

.field private g:Landroid/widget/ImageView;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1627351
    invoke-direct {p0, p1}, LX/A89;-><init>(Landroid/content/Context;)V

    .line 1627352
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->a(Landroid/content/Context;)V

    .line 1627353
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627314
    invoke-direct {p0, p1, p2}, LX/A89;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1627315
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->a(Landroid/content/Context;)V

    .line 1627316
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627317
    invoke-direct {p0, p1, p2, p3}, LX/A89;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627318
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->a(Landroid/content/Context;)V

    .line 1627319
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1627320
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1627321
    const v0, 0x7f03153d

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1627322
    const v0, 0x7f0d2fd0

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1627323
    const v1, 0x7f0d2fd1

    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    iput-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->b:Lcom/facebook/resources/ui/FbButton;

    .line 1627324
    const v1, 0x7f0d2fd3

    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->g:Landroid/widget/ImageView;

    .line 1627325
    const v1, 0x7f0d2fd4

    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1627326
    const v1, 0x7f0d2fd5

    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1627327
    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->b:Lcom/facebook/resources/ui/FbButton;

    const v3, 0x7f08243f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1627328
    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->b:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/A85;

    invoke-direct {v3, p0}, LX/A85;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;)V

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1627329
    const v1, 0x7f08243d

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1627330
    new-instance v1, LX/A86;

    invoke-direct {v1, p0}, LX/A86;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1627331
    const v0, 0x7f082445

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->d:Ljava/lang/String;

    .line 1627332
    const v0, 0x7f082446

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->e:Ljava/lang/String;

    .line 1627333
    return-void
.end method

.method public static d(Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;)V
    .locals 3

    .prologue
    .line 1627334
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1627335
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1627336
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1627337
    iget-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->f:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/A87;

    invoke-direct {v1, p0}, LX/A87;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1627338
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->setRating(I)V

    .line 1627339
    return-void
.end method

.method public setExplanationListener(LX/A88;)V
    .locals 0

    .prologue
    .line 1627340
    iput-object p1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->a:LX/A88;

    .line 1627341
    return-void
.end method

.method public setRating(I)V
    .locals 3

    .prologue
    .line 1627342
    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    .line 1627343
    iget-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1627344
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f10003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1627345
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1627346
    if-lez v1, :cond_0

    .line 1627347
    iget-object v2, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1627348
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1627349
    return-void

    .line 1627350
    :cond_1
    iget-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
