.class public Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;
.super Lcom/facebook/fbui/popover/PopoverViewFlipper;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1627366
    invoke-direct {p0, p1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;-><init>(Landroid/content/Context;)V

    .line 1627367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1627368
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1627369
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1627370
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627371
    return-void
.end method


# virtual methods
.method public final a()LX/5OU;
    .locals 2

    .prologue
    .line 1627372
    new-instance v0, LX/A8B;

    invoke-direct {v0, p0}, LX/A8B;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;)V

    return-object v0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627373
    if-nez p1, :cond_0

    .line 1627374
    :goto_0
    return-void

    .line 1627375
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getChildCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1627376
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    move-object v0, v0

    .line 1627377
    sget-object v1, LX/5OV;->NONE:LX/5OV;

    if-ne v0, v1, :cond_1

    .line 1627378
    :cond_0
    :goto_0
    return-void

    .line 1627379
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1627380
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->f()V

    goto :goto_0

    .line 1627381
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1627382
    iget v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    move v1, v1

    .line 1627383
    if-ltz v0, :cond_0

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1627384
    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1627385
    new-instance v2, LX/A84;

    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/A84;-><init>(Landroid/content/Context;)V

    .line 1627386
    invoke-virtual {v2, v1}, LX/A84;->a(Landroid/view/View;)V

    .line 1627387
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    .line 1627388
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v2

    .line 1627389
    if-le v1, v0, :cond_0

    .line 1627390
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public setContentViewPreservingLayout(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627391
    if-nez p1, :cond_0

    .line 1627392
    :goto_0
    return-void

    .line 1627393
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->removeAllViews()V

    .line 1627394
    invoke-virtual {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
