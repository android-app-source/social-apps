.class public Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/A8N;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1627221
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4831fc3b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1627222
    const v1, 0x7f03153b

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1627223
    iget-object v2, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;->m:LX/A8N;

    if-eqz v2, :cond_2

    .line 1627224
    iget-object v2, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;->m:LX/A8N;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1627225
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1627226
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    if-nez v4, :cond_3

    move v4, v5

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1627227
    new-instance v4, LX/A8D;

    invoke-direct {v4, v3}, LX/A8D;-><init>(Landroid/content/Context;)V

    iput-object v4, v2, LX/A8N;->b:LX/A8D;

    .line 1627228
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    const/4 p1, -0x2

    .line 1627229
    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    const/4 p2, -0x2

    if-ne p1, p2, :cond_4

    :cond_0
    const/4 p2, 0x1

    :goto_1
    invoke-static {p2}, LX/0PB;->checkArgument(Z)V

    .line 1627230
    iput p1, v4, LX/A8D;->a:I

    .line 1627231
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v2, LX/A8N;->f:Ljava/util/List;

    .line 1627232
    iget-object v4, v2, LX/A8N;->f:Ljava/util/List;

    .line 1627233
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const p2, 0x7f031541

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 1627234
    check-cast p1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    .line 1627235
    invoke-virtual {p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a()V

    .line 1627236
    new-instance p2, LX/A8K;

    invoke-direct {p2, v2}, LX/A8K;-><init>(LX/A8N;)V

    .line 1627237
    iput-object p2, p1, LX/A89;->a:LX/A8A;

    .line 1627238
    iput-object v2, p1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->b:LX/A8H;

    .line 1627239
    move-object p1, p1

    .line 1627240
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627241
    iget-object v4, v2, LX/A8N;->f:Ljava/util/List;

    .line 1627242
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const p2, 0x7f03153c

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 1627243
    check-cast p1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    iput-object p1, v2, LX/A8N;->c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    .line 1627244
    iget-object p1, v2, LX/A8N;->c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    invoke-virtual {p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->a()V

    .line 1627245
    iget-object p1, v2, LX/A8N;->c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    new-instance p2, LX/A8L;

    invoke-direct {p2, v2}, LX/A8L;-><init>(LX/A8N;)V

    .line 1627246
    iput-object p2, p1, LX/A89;->a:LX/A8A;

    .line 1627247
    iget-object p1, v2, LX/A8N;->c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    .line 1627248
    iput-object v2, p1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;->a:LX/A88;

    .line 1627249
    iget-object p1, v2, LX/A8N;->c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    move-object p1, p1

    .line 1627250
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627251
    iget-object v4, v2, LX/A8N;->f:Ljava/util/List;

    .line 1627252
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const p2, 0x7f03153e

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 1627253
    check-cast p1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;

    .line 1627254
    new-instance p2, LX/A8M;

    invoke-direct {p2, v2}, LX/A8M;-><init>(LX/A8N;)V

    .line 1627255
    iput-object p2, p1, LX/A89;->a:LX/A8A;

    .line 1627256
    move-object p1, p1

    .line 1627257
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627258
    iget-object v4, v2, LX/A8N;->f:Ljava/util/List;

    invoke-static {v2, v4}, LX/A8N;->a$redex0(LX/A8N;Ljava/util/List;)V

    .line 1627259
    iget-object p1, v2, LX/A8N;->b:LX/A8D;

    iget-object v4, v2, LX/A8N;->f:Ljava/util/List;

    const/4 p2, 0x0

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {p1, v4}, LX/0ht;->d(Landroid/view/View;)V

    .line 1627260
    iget-object v4, v2, LX/A8N;->f:Ljava/util/List;

    const/4 p1, 0x1

    .line 1627261
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1627262
    if-ltz p1, :cond_5

    const/4 p2, 0x1

    :goto_2
    const-string p3, "number to skip cannot be negative"

    invoke-static {p2, p3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1627263
    instance-of p2, v4, Ljava/util/List;

    if-eqz p2, :cond_6

    .line 1627264
    check-cast v4, Ljava/util/List;

    .line 1627265
    new-instance p2, LX/4yp;

    invoke-direct {p2, v4, p1}, LX/4yp;-><init>(Ljava/util/List;I)V

    .line 1627266
    :goto_3
    move-object v4, p2

    .line 1627267
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 1627268
    iget-object p2, v2, LX/A8N;->b:LX/A8D;

    invoke-virtual {p2, v4}, LX/0ht;->e(Landroid/view/View;)V

    goto :goto_4

    .line 1627269
    :cond_1
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    invoke-virtual {v4, v5}, LX/0ht;->d(Z)V

    .line 1627270
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    sget-object v5, LX/3AV;->CENTER:LX/3AV;

    invoke-virtual {v4, v5}, LX/0ht;->a(LX/3AV;)V

    .line 1627271
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    sget-object v5, LX/5OV;->SLIDE_UP:LX/5OV;

    invoke-virtual {v4, v5}, LX/0ht;->a(LX/5OV;)V

    .line 1627272
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    .line 1627273
    iput-boolean v6, v4, LX/0ht;->x:Z

    .line 1627274
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    .line 1627275
    iput-boolean v6, v4, LX/0ht;->w:Z

    .line 1627276
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    iget-object v5, v2, LX/A8N;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1627277
    iget-object v4, v2, LX/A8N;->b:LX/A8D;

    invoke-virtual {v4, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1627278
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 1627279
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1627280
    :goto_5
    const v2, 0x5a47369f

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 1627281
    :cond_2
    const-class v2, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    const-string v3, "Required UniversalFeedbackUIController not set"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_5

    :cond_3
    move v4, v6

    .line 1627282
    goto/16 :goto_0

    .line 1627283
    :cond_4
    const/4 p2, 0x0

    goto/16 :goto_1

    .line 1627284
    :cond_5
    const/4 p2, 0x0

    goto :goto_2

    .line 1627285
    :cond_6
    new-instance p2, LX/4yl;

    invoke-direct {p2, v4, p1}, LX/4yl;-><init>(Ljava/lang/Iterable;I)V

    goto :goto_3
.end method
