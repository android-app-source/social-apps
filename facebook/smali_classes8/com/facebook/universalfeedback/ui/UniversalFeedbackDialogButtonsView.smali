.class public Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogButtonsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1627218
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1627219
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogButtonsView;->a(Landroid/content/Context;)V

    .line 1627220
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627215
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1627216
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogButtonsView;->a(Landroid/content/Context;)V

    .line 1627217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627210
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627211
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogButtonsView;->a(Landroid/content/Context;)V

    .line 1627212
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1627213
    const v0, 0x7f03153a

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1627214
    return-void
.end method
