.class public Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;
.super LX/A89;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1627508
    invoke-direct {p0, p1}, LX/A89;-><init>(Landroid/content/Context;)V

    .line 1627509
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->a(Landroid/content/Context;)V

    .line 1627510
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627505
    invoke-direct {p0, p1, p2}, LX/A89;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1627506
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->a(Landroid/content/Context;)V

    .line 1627507
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627502
    invoke-direct {p0, p1, p2, p3}, LX/A89;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627503
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->a(Landroid/content/Context;)V

    .line 1627504
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1627487
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1627488
    const v0, 0x7f03153f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1627489
    const v0, 0x7f0d2fd0

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1627490
    const v1, 0x7f0d2fd1

    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    .line 1627491
    const v3, 0x7f08243e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1627492
    new-instance v2, LX/A8I;

    invoke-direct {v2, p0}, LX/A8I;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1627493
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1627494
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1627495
    const v0, 0x7f0d2fd7

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1627496
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackThankyouView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1627497
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v2, v3, :cond_0

    .line 1627498
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxWidth(I)V

    .line 1627499
    :goto_0
    invoke-super {p0, p1, p2}, LX/A89;->onMeasure(II)V

    .line 1627500
    return-void

    .line 1627501
    :cond_0
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    const v2, 0x3f333333    # 0.7f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxWidth(I)V

    goto :goto_0
.end method
