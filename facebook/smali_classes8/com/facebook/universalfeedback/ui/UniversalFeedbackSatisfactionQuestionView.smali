.class public Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;
.super LX/A89;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbButton;

.field public b:LX/A8H;

.field public c:Landroid/widget/ImageButton;

.field private final d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1627476
    invoke-direct {p0, p1}, LX/A89;-><init>(Landroid/content/Context;)V

    .line 1627477
    new-instance v0, LX/A8E;

    invoke-direct {v0, p0}, LX/A8E;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;)V

    iput-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->d:Landroid/view/View$OnClickListener;

    .line 1627478
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a(Landroid/content/Context;)V

    .line 1627479
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627472
    invoke-direct {p0, p1, p2}, LX/A89;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1627473
    new-instance v0, LX/A8E;

    invoke-direct {v0, p0}, LX/A8E;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;)V

    iput-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->d:Landroid/view/View$OnClickListener;

    .line 1627474
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a(Landroid/content/Context;)V

    .line 1627475
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627480
    invoke-direct {p0, p1, p2, p3}, LX/A89;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627481
    new-instance v0, LX/A8E;

    invoke-direct {v0, p0}, LX/A8E;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;)V

    iput-object v0, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->d:Landroid/view/View$OnClickListener;

    .line 1627482
    invoke-direct {p0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a(Landroid/content/Context;)V

    .line 1627483
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1627456
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1627457
    const v0, 0x7f031542

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1627458
    const v0, 0x7f0d2fd0

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1627459
    const v1, 0x7f0d2fd1

    invoke-virtual {p0, v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    iput-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 1627460
    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a:Lcom/facebook/resources/ui/FbButton;

    const v4, 0x7f08243f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1627461
    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1627462
    iget-object v1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a:Lcom/facebook/resources/ui/FbButton;

    new-instance v4, LX/A8F;

    invoke-direct {v4, p0}, LX/A8F;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;)V

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1627463
    const v1, 0x7f08243e

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1627464
    new-instance v1, LX/A8G;

    invoke-direct {v1, p0}, LX/A8G;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1627465
    const v0, 0x7f0d2fda

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1627466
    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_0

    .line 1627467
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 1627468
    iget-object v3, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1627469
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1627470
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1627471
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v12, -0x1

    const/4 v1, 0x0

    .line 1627437
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1627438
    const v0, 0x7f10003d

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 1627439
    const v0, 0x7f10003e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 1627440
    const v0, 0x7f0d2fda

    invoke-virtual {p0, v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1627441
    new-array v6, v7, [I

    const v2, 0x10100a7

    aput v2, v6, v1

    .line 1627442
    new-array v7, v7, [I

    const v2, 0x10100a1

    aput v2, v7, v1

    .line 1627443
    new-array v8, v1, [I

    move v2, v1

    .line 1627444
    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_0

    .line 1627445
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 1627446
    invoke-virtual {v5, v2, v12}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 1627447
    new-instance v10, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v10}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1627448
    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    invoke-virtual {v10, v6, v11}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1627449
    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v10, v7, v9}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1627450
    invoke-virtual {v4, v2, v12}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v10, v8, v9}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1627451
    invoke-virtual {v1, v10}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1627452
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1627453
    :cond_0
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 1627454
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 1627455
    return-void
.end method

.method public setRatingListener(LX/A8H;)V
    .locals 0

    .prologue
    .line 1627435
    iput-object p1, p0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->b:LX/A8H;

    .line 1627436
    return-void
.end method
