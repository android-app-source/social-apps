.class public Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:LX/A7t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1626810
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1626811
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1626812
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1626813
    invoke-virtual {p0, v0, v1}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1626814
    return-object v0
.end method

.method private a(LX/A7t;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1626815
    iput-object p1, p0, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->p:LX/A7t;

    .line 1626816
    iget-object v0, p0, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->p:LX/A7t;

    new-instance v1, LX/A7u;

    invoke-direct {v1, p0}, LX/A7u;-><init>(Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;)V

    .line 1626817
    iput-object v1, v0, LX/A7t;->k:LX/A7s;

    .line 1626818
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    invoke-static {v0}, LX/A7t;->b(LX/0QB;)LX/A7t;

    move-result-object v0

    check-cast v0, LX/A7t;

    invoke-direct {p0, v0}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->a(LX/A7t;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1626819
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1626820
    invoke-direct {p0}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->a()Landroid/view/View;

    .line 1626821
    invoke-static {p0, p0}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1626822
    if-nez p1, :cond_0

    .line 1626823
    invoke-virtual {p0}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_feedback_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1626824
    if-eqz v0, :cond_0

    const-string v1, "UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1626825
    new-instance v1, LX/A7p;

    const-string v2, "NFX_FEEDBACK"

    const-string v3, "SYSTEM_TEST"

    invoke-direct {v1, v2, v3}, LX/A7p;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626826
    iput-object v0, v1, LX/A7p;->c:Ljava/lang/String;

    .line 1626827
    iget-object v0, p0, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->p:LX/A7t;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/A7t;->a(LX/A7p;LX/0gc;)V

    .line 1626828
    :cond_0
    return-void
.end method
