.class public final Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1436624
    const-class v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;

    new-instance v1, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1436625
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1436626
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1436606
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1436607
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1436608
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1436609
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1436610
    if-eqz v2, :cond_2

    .line 1436611
    const-string p0, "addressbooks"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1436612
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1436613
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1436614
    if-eqz p0, :cond_1

    .line 1436615
    const-string v0, "edges"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1436616
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1436617
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1436618
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/A82;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1436619
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1436620
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1436621
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1436622
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1436623
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1436605
    check-cast p1, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel$Serializer;->a(Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
