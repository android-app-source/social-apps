.class public final Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1436437
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1436438
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1436544
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1436545
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1436508
    if-nez p1, :cond_0

    .line 1436509
    :goto_0
    return v0

    .line 1436510
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1436511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1436512
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1436513
    const v2, 0xae5d6da

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1436514
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1436515
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1436516
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1436517
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1436518
    const v2, -0x68c867c0

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1436519
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1436520
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1436521
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1436522
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1436523
    const v2, 0x63d58d9a

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1436524
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 1436525
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1436526
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1436527
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1436528
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 1436529
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 1436530
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1436531
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1436532
    const v2, 0x57e83eec

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1436533
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1436534
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1436535
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1436536
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1436537
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1436538
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1436539
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1436540
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1436541
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1436542
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1436543
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x68c867c0 -> :sswitch_2
        -0x2d520ef0 -> :sswitch_0
        0xae5d6da -> :sswitch_1
        0x57e83eec -> :sswitch_4
        0x63d58d9a -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1436507
    new-instance v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1436496
    sparse-switch p2, :sswitch_data_0

    .line 1436497
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1436498
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1436499
    const v1, 0xae5d6da

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1436500
    :goto_0
    :sswitch_1
    return-void

    .line 1436501
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1436502
    const v1, -0x68c867c0

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1436503
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1436504
    const v1, 0x63d58d9a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1436505
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1436506
    const v1, 0x57e83eec

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x68c867c0 -> :sswitch_3
        -0x2d520ef0 -> :sswitch_0
        0xae5d6da -> :sswitch_2
        0x57e83eec -> :sswitch_1
        0x63d58d9a -> :sswitch_4
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1436486
    if-nez p1, :cond_0

    move v0, v1

    .line 1436487
    :goto_0
    return v0

    .line 1436488
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1436489
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1436490
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1436491
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1436492
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1436493
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1436494
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1436495
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1436479
    if-eqz p1, :cond_0

    .line 1436480
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1436481
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1436482
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1436483
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1436484
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1436485
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1436473
    if-eqz p1, :cond_0

    .line 1436474
    invoke-static {p0, p1, p2}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1436475
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;

    .line 1436476
    if-eq v0, v1, :cond_0

    .line 1436477
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1436478
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1436472
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1436470
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1436471
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1436465
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1436466
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1436467
    :cond_0
    iput-object p1, p0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1436468
    iput p2, p0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->b:I

    .line 1436469
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1436464
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1436463
    new-instance v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1436460
    iget v0, p0, LX/1vt;->c:I

    .line 1436461
    move v0, v0

    .line 1436462
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1436457
    iget v0, p0, LX/1vt;->c:I

    .line 1436458
    move v0, v0

    .line 1436459
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1436454
    iget v0, p0, LX/1vt;->b:I

    .line 1436455
    move v0, v0

    .line 1436456
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1436451
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1436452
    move-object v0, v0

    .line 1436453
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1436442
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1436443
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1436444
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1436445
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1436446
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1436447
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1436448
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1436449
    invoke-static {v3, v9, v2}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1436450
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1436439
    iget v0, p0, LX/1vt;->c:I

    .line 1436440
    move v0, v0

    .line 1436441
    return v0
.end method
