.class public final Lcom/facebook/contacts/upload/ContactsUploadState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/upload/ContactsUploadState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/94z;

.field public final b:I

.field public final c:I

.field public final d:I

.field private final e:Lcom/facebook/fbservice/service/OperationResult;

.field private final f:Lcom/facebook/fbservice/service/ServiceException;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1436283
    new-instance v0, LX/94y;

    invoke-direct {v0}, LX/94y;-><init>()V

    sput-object v0, Lcom/facebook/contacts/upload/ContactsUploadState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/94z;IIILcom/facebook/fbservice/service/OperationResult;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1
    .param p5    # Lcom/facebook/fbservice/service/OperationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/fbservice/service/ServiceException;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1436284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436285
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1436286
    iput-object p1, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    .line 1436287
    iput p2, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    .line 1436288
    iput p3, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    .line 1436289
    iput p4, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    .line 1436290
    iput-object p5, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->e:Lcom/facebook/fbservice/service/OperationResult;

    .line 1436291
    iput-object p6, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 1436292
    return-void

    .line 1436293
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1436294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436295
    const-class v0, LX/94z;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/94z;

    iput-object v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    .line 1436296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    .line 1436297
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    .line 1436298
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    .line 1436299
    const-class v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    iput-object v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->e:Lcom/facebook/fbservice/service/OperationResult;

    .line 1436300
    const-class v0, Lcom/facebook/fbservice/service/ServiceException;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    iput-object v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 1436301
    return-void
.end method

.method public static a(III)Lcom/facebook/contacts/upload/ContactsUploadState;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1436302
    new-instance v0, Lcom/facebook/contacts/upload/ContactsUploadState;

    sget-object v1, LX/94z;->RUNNING:LX/94z;

    move v2, p0

    move v3, p1

    move v4, p2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/contacts/upload/ContactsUploadState;-><init>(LX/94z;IIILcom/facebook/fbservice/service/OperationResult;Lcom/facebook/fbservice/service/ServiceException;)V

    return-object v0
.end method

.method public static e()Lcom/facebook/contacts/upload/ContactsUploadState;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1436303
    new-instance v0, Lcom/facebook/contacts/upload/ContactsUploadState;

    sget-object v1, LX/94z;->NOT_STARTED:LX/94z;

    move v3, v2

    move v4, v2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/contacts/upload/ContactsUploadState;-><init>(LX/94z;IIILcom/facebook/fbservice/service/OperationResult;Lcom/facebook/fbservice/service/ServiceException;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1436304
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1436305
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ContactsUploadState ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") (processed/matched/total): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1436306
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    invoke-virtual {v0}, LX/94z;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1436307
    iget v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1436308
    iget v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1436309
    iget v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1436310
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->e:Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1436311
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContactsUploadState;->f:Lcom/facebook/fbservice/service/ServiceException;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1436312
    return-void
.end method
