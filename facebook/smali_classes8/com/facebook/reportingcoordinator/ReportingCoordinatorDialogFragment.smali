.class public Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/9yI;

.field private final n:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1596075
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1596076
    new-instance v0, LX/9yJ;

    invoke-direct {v0, p0}, LX/9yJ;-><init>(Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;->n:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public static a()Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;
    .locals 1

    .prologue
    .line 1596077
    new-instance v0, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;

    invoke-direct {v0}, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1596078
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0e09a8

    invoke-direct {v0, v1, v3}, LX/31Y;-><init>(Landroid/content/Context;I)V

    .line 1596079
    new-instance v1, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 1596080
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1983

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move v4, v2

    move v5, v2

    .line 1596081
    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1596082
    const v1, 0x7f080017

    iget-object v2, p0, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;->n:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1596083
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/9yI;)V
    .locals 0

    .prologue
    .line 1596084
    iput-object p1, p0, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;->m:LX/9yI;

    .line 1596085
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xf6e9301

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1596086
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1596087
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1596088
    const/16 v1, 0x2b

    const v2, -0x7e49b9b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4445a5dc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1596089
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1596090
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1596091
    if-eqz v1, :cond_0

    .line 1596092
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1596093
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 1596094
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1596095
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 1596096
    const/4 v3, -0x1

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1596097
    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1596098
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1596099
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x43bda85

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
