.class public final Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5ec4b668
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546318
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546317
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1546315
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546316
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1546312
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546313
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546314
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;
    .locals 8

    .prologue
    .line 1546287
    if-nez p0, :cond_0

    .line 1546288
    const/4 p0, 0x0

    .line 1546289
    :goto_0
    return-object p0

    .line 1546290
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    if-eqz v0, :cond_1

    .line 1546291
    check-cast p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    goto :goto_0

    .line 1546292
    :cond_1
    new-instance v0, LX/9qB;

    invoke-direct {v0}, LX/9qB;-><init>()V

    .line 1546293
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->a()Z

    move-result v1

    iput-boolean v1, v0, LX/9qB;->a:Z

    .line 1546294
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/9qB;->b:Z

    .line 1546295
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/9qB;->c:Z

    .line 1546296
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/9qB;->d:Z

    .line 1546297
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1546298
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1546299
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 1546300
    iget-boolean v3, v0, LX/9qB;->a:Z

    invoke-virtual {v2, v7, v3}, LX/186;->a(IZ)V

    .line 1546301
    iget-boolean v3, v0, LX/9qB;->b:Z

    invoke-virtual {v2, v6, v3}, LX/186;->a(IZ)V

    .line 1546302
    const/4 v3, 0x2

    iget-boolean v5, v0, LX/9qB;->c:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1546303
    const/4 v3, 0x3

    iget-boolean v5, v0, LX/9qB;->d:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1546304
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1546305
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1546306
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1546307
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1546308
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1546309
    new-instance v3, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;-><init>(LX/15i;)V

    .line 1546310
    move-object p0, v3

    .line 1546311
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1546279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546280
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1546281
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546282
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546283
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546284
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546285
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546286
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1546276
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546278
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1546257
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1546258
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->e:Z

    .line 1546259
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->f:Z

    .line 1546260
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->g:Z

    .line 1546261
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->h:Z

    .line 1546262
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1546274
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546275
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1546271
    new-instance v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;-><init>()V

    .line 1546272
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546273
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1546269
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546270
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1546267
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546268
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1546265
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546266
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1546264
    const v0, -0x13cc620f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1546263
    const v0, -0x511effc4

    return v0
.end method
