.class public final Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6e43a4b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546480
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546486
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1546484
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546485
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1546481
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546482
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546483
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;
    .locals 11

    .prologue
    .line 1546425
    if-nez p0, :cond_0

    .line 1546426
    const/4 p0, 0x0

    .line 1546427
    :goto_0
    return-object p0

    .line 1546428
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    if-eqz v0, :cond_1

    .line 1546429
    check-cast p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    goto :goto_0

    .line 1546430
    :cond_1
    new-instance v0, LX/9qD;

    invoke-direct {v0}, LX/9qD;-><init>()V

    .line 1546431
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v1

    iput-object v1, v0, LX/9qD;->a:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546432
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v1

    iput-object v1, v0, LX/9qD;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546433
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v1

    iput-object v1, v0, LX/9qD;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546434
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v1

    iput-object v1, v0, LX/9qD;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546435
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1546436
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1546437
    iget-object v3, v0, LX/9qD;->a:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1546438
    iget-object v5, v0, LX/9qD;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1546439
    iget-object v7, v0, LX/9qD;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-virtual {v2, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1546440
    iget-object v8, v0, LX/9qD;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-virtual {v2, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1546441
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1546442
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1546443
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1546444
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1546445
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1546446
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1546447
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1546448
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1546449
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1546450
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1546451
    new-instance v3, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;-><init>(LX/15i;)V

    .line 1546452
    move-object p0, v3

    .line 1546453
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1546468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546469
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1546470
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1546471
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1546472
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1546473
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1546474
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1546475
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1546476
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1546477
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1546478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546479
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1546465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546466
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546467
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546487
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546488
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546463
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546464
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1546460
    new-instance v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;-><init>()V

    .line 1546461
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546462
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546458
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546459
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546456
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 1546457
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1546455
    const v0, -0x870b50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1546454
    const v0, -0x582f1829

    return v0
.end method
