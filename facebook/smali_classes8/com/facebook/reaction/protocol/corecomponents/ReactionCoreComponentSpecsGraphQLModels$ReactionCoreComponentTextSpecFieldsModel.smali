.class public final Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3909eaec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:I

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546540
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546617
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1546615
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546616
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1546612
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546613
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546614
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 13

    .prologue
    .line 1546567
    if-nez p0, :cond_0

    .line 1546568
    const/4 p0, 0x0

    .line 1546569
    :goto_0
    return-object p0

    .line 1546570
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    if-eqz v0, :cond_1

    .line 1546571
    check-cast p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    goto :goto_0

    .line 1546572
    :cond_1
    new-instance v0, LX/9qE;

    invoke-direct {v0}, LX/9qE;-><init>()V

    .line 1546573
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    move-result-object v1

    iput-object v1, v0, LX/9qE;->a:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 1546574
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    move-result-object v1

    iput-object v1, v0, LX/9qE;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 1546575
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/9qE;->c:Z

    .line 1546576
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9qE;->d:Ljava/lang/String;

    .line 1546577
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->e()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    move-result-object v1

    iput-object v1, v0, LX/9qE;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 1546578
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->gL_()Z

    move-result v1

    iput-boolean v1, v0, LX/9qE;->f:Z

    .line 1546579
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->gM_()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    move-result-object v1

    iput-object v1, v0, LX/9qE;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 1546580
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->j()I

    move-result v1

    iput v1, v0, LX/9qE;->h:I

    .line 1546581
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->k()I

    move-result v1

    iput v1, v0, LX/9qE;->i:I

    .line 1546582
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9qE;->j:Ljava/lang/String;

    .line 1546583
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/9qE;->k:Z

    .line 1546584
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v12, 0x0

    .line 1546585
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1546586
    iget-object v3, v0, LX/9qE;->a:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1546587
    iget-object v5, v0, LX/9qE;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1546588
    iget-object v7, v0, LX/9qE;->d:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1546589
    iget-object v8, v0, LX/9qE;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    invoke-virtual {v2, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1546590
    iget-object v9, v0, LX/9qE;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    invoke-virtual {v2, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1546591
    iget-object v10, v0, LX/9qE;->j:Ljava/lang/String;

    invoke-virtual {v2, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1546592
    const/16 v11, 0xb

    invoke-virtual {v2, v11}, LX/186;->c(I)V

    .line 1546593
    invoke-virtual {v2, v12, v3}, LX/186;->b(II)V

    .line 1546594
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1546595
    const/4 v3, 0x2

    iget-boolean v5, v0, LX/9qE;->c:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1546596
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1546597
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1546598
    const/4 v3, 0x5

    iget-boolean v5, v0, LX/9qE;->f:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1546599
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1546600
    const/4 v3, 0x7

    iget v5, v0, LX/9qE;->h:I

    invoke-virtual {v2, v3, v5, v12}, LX/186;->a(III)V

    .line 1546601
    const/16 v3, 0x8

    iget v5, v0, LX/9qE;->i:I

    invoke-virtual {v2, v3, v5, v12}, LX/186;->a(III)V

    .line 1546602
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1546603
    const/16 v3, 0xa

    iget-boolean v5, v0, LX/9qE;->k:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1546604
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1546605
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1546606
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1546607
    invoke-virtual {v3, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1546608
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1546609
    new-instance v3, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;-><init>(LX/15i;)V

    .line 1546610
    move-object p0, v3

    .line 1546611
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1546546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546547
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1546548
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1546549
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1546550
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->e()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1546551
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->gM_()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1546552
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1546553
    const/16 v6, 0xb

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1546554
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1546555
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1546556
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546557
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1546558
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1546559
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546560
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1546561
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->l:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1546562
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->m:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1546563
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1546564
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->o:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1546565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1546543
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546545
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546541
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 1546542
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1546510
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1546511
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->g:Z

    .line 1546512
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->j:Z

    .line 1546513
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->l:I

    .line 1546514
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->m:I

    .line 1546515
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->o:Z

    .line 1546516
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546538
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 1546539
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1546535
    new-instance v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;-><init>()V

    .line 1546536
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546537
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1546618
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546619
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->g:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546533
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->h:Ljava/lang/String;

    .line 1546534
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1546532
    const v0, -0x144362b7

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546530
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->i:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->i:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 1546531
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->i:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1546529
    const v0, 0x2b1271fd

    return v0
.end method

.method public final gL_()Z
    .locals 2

    .prologue
    .line 1546527
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546528
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->j:Z

    return v0
.end method

.method public final gM_()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546525
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->k:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->k:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 1546526
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->k:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    return-object v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1546523
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546524
    iget v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->l:I

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1546521
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546522
    iget v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->m:I

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546519
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->n:Ljava/lang/String;

    .line 1546520
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1546517
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1546518
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->o:Z

    return v0
.end method
