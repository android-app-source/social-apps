.class public final Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x320a9953
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546400
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1546399
    const-class v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1546397
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546398
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1546394
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1546395
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546396
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;
    .locals 11

    .prologue
    .line 1546365
    if-nez p0, :cond_0

    .line 1546366
    const/4 p0, 0x0

    .line 1546367
    :goto_0
    return-object p0

    .line 1546368
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    if-eqz v0, :cond_1

    .line 1546369
    check-cast p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    goto :goto_0

    .line 1546370
    :cond_1
    new-instance v0, LX/9qC;

    invoke-direct {v0}, LX/9qC;-><init>()V

    .line 1546371
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v1

    iput-object v1, v0, LX/9qC;->a:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546372
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v1

    iput-object v1, v0, LX/9qC;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546373
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v1

    iput-object v1, v0, LX/9qC;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546374
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v1

    iput-object v1, v0, LX/9qC;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546375
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1546376
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1546377
    iget-object v3, v0, LX/9qC;->a:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1546378
    iget-object v5, v0, LX/9qC;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1546379
    iget-object v7, v0, LX/9qC;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-virtual {v2, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1546380
    iget-object v8, v0, LX/9qC;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-virtual {v2, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1546381
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1546382
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1546383
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1546384
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1546385
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1546386
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1546387
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1546388
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1546389
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1546390
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1546391
    new-instance v3, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;-><init>(LX/15i;)V

    .line 1546392
    move-object p0, v3

    .line 1546393
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1546353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546354
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1546355
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1546356
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1546357
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1546358
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1546359
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1546360
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1546361
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1546362
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1546363
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546364
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1546401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1546402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1546403
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546351
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546352
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546349
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546350
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1546346
    new-instance v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;-><init>()V

    .line 1546347
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1546348
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546344
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546345
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1546342
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 1546343
    iget-object v0, p0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1546341
    const v0, 0x609aaddd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1546340
    const v0, -0x48677f42

    return v0
.end method
