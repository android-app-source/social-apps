.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x104a4aa6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542602
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542608
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1542606
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542607
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1542603
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542604
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542605
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;
    .locals 8

    .prologue
    .line 1542563
    if-nez p0, :cond_0

    .line 1542564
    const/4 p0, 0x0

    .line 1542565
    :goto_0
    return-object p0

    .line 1542566
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    if-eqz v0, :cond_1

    .line 1542567
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    goto :goto_0

    .line 1542568
    :cond_1
    new-instance v0, LX/9p2;

    invoke-direct {v0}, LX/9p2;-><init>()V

    .line 1542569
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9p2;->a:Ljava/lang/String;

    .line 1542570
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1542571
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1542572
    iget-object v3, v0, LX/9p2;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1542573
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1542574
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1542575
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1542576
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1542577
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1542578
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1542579
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1542580
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;-><init>(LX/15i;)V

    .line 1542581
    move-object p0, v3

    .line 1542582
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1542596
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542597
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1542598
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1542599
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1542600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542601
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1542593
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542595
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1542609
    new-instance v0, LX/9p3;

    invoke-direct {v0, p1}, LX/9p3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542591
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;->e:Ljava/lang/String;

    .line 1542592
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1542589
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1542590
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1542588
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1542585
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;-><init>()V

    .line 1542586
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542587
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1542584
    const v0, -0x2f6a981a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1542583
    const v0, 0x285feb

    return v0
.end method
