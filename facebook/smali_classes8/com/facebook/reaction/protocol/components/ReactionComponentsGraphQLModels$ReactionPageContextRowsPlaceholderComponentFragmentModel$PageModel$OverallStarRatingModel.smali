.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d369c4a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541887
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541886
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541884
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541885
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1541881
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541882
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541883
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;
    .locals 13

    .prologue
    .line 1541860
    if-nez p0, :cond_0

    .line 1541861
    const/4 p0, 0x0

    .line 1541862
    :goto_0
    return-object p0

    .line 1541863
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    if-eqz v0, :cond_1

    .line 1541864
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    goto :goto_0

    .line 1541865
    :cond_1
    new-instance v0, LX/9ou;

    invoke-direct {v0}, LX/9ou;-><init>()V

    .line 1541866
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->a()I

    move-result v1

    iput v1, v0, LX/9ou;->a:I

    .line 1541867
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/9ou;->b:D

    .line 1541868
    const/4 v5, 0x1

    const/4 v12, 0x0

    const/4 v10, 0x0

    .line 1541869
    new-instance v4, LX/186;

    const/16 v6, 0x80

    invoke-direct {v4, v6}, LX/186;-><init>(I)V

    .line 1541870
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, LX/186;->c(I)V

    .line 1541871
    iget v6, v0, LX/9ou;->a:I

    invoke-virtual {v4, v10, v6, v10}, LX/186;->a(III)V

    .line 1541872
    iget-wide v6, v0, LX/9ou;->b:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1541873
    invoke-virtual {v4}, LX/186;->d()I

    move-result v6

    .line 1541874
    invoke-virtual {v4, v6}, LX/186;->d(I)V

    .line 1541875
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1541876
    invoke-virtual {v7, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1541877
    new-instance v6, LX/15i;

    move-object v8, v12

    move-object v9, v12

    move v10, v5

    move-object v11, v12

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1541878
    new-instance v4, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    invoke-direct {v4, v6}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;-><init>(LX/15i;)V

    .line 1541879
    move-object p0, v4

    .line 1541880
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1541858
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1541859
    iget v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1541888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541889
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1541890
    iget v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1541891
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1541892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541893
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1541855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541857
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1541851
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1541852
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->e:I

    .line 1541853
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->f:D

    .line 1541854
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1541849
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1541850
    iget-wide v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1541846
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel$OverallStarRatingModel;-><init>()V

    .line 1541847
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541848
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1541845
    const v0, -0x66588cce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1541844
    const v0, -0x6e856243

    return v0
.end method
