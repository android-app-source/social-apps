.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x37fd0773
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542531
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542530
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1542528
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542529
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1542525
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542526
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542527
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;
    .locals 12

    .prologue
    .line 1542493
    if-nez p0, :cond_0

    .line 1542494
    const/4 p0, 0x0

    .line 1542495
    :goto_0
    return-object p0

    .line 1542496
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    if-eqz v0, :cond_1

    .line 1542497
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    goto :goto_0

    .line 1542498
    :cond_1
    new-instance v0, LX/9oy;

    invoke-direct {v0}, LX/9oy;-><init>()V

    .line 1542499
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/9oy;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1542500
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oy;->b:Ljava/lang/String;

    .line 1542501
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oy;->c:Ljava/lang/String;

    .line 1542502
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v1

    iput-object v1, v0, LX/9oy;->d:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    .line 1542503
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->gK_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v1

    iput-object v1, v0, LX/9oy;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    .line 1542504
    const/4 v6, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 1542505
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1542506
    iget-object v3, v0, LX/9oy;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1542507
    iget-object v5, v0, LX/9oy;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1542508
    iget-object v7, v0, LX/9oy;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1542509
    iget-object v8, v0, LX/9oy;->d:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1542510
    iget-object v9, v0, LX/9oy;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1542511
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, LX/186;->c(I)V

    .line 1542512
    invoke-virtual {v2, v11, v3}, LX/186;->b(II)V

    .line 1542513
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1542514
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1542515
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1542516
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1542517
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1542518
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1542519
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1542520
    invoke-virtual {v3, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1542521
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1542522
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;-><init>(LX/15i;)V

    .line 1542523
    move-object p0, v3

    .line 1542524
    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1542486
    iput-object p1, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1542487
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1542488
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1542489
    if-eqz v0, :cond_0

    .line 1542490
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1542491
    :cond_0
    return-void

    .line 1542492
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542484
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    .line 1542485
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542482
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    .line 1542483
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1542468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542469
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1542470
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1542471
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1542472
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1542473
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1542474
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1542475
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1542476
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1542477
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1542478
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1542479
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1542480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542481
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1542455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542456
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1542457
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    .line 1542458
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1542459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    .line 1542460
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    .line 1542461
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1542462
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    .line 1542463
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1542464
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    .line 1542465
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    .line 1542466
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542467
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1542532
    new-instance v0, LX/9oz;

    invoke-direct {v0, p1}, LX/9oz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542432
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1542433
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1542434
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1542435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1542436
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1542437
    :goto_0
    return-void

    .line 1542438
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1542439
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1542440
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1542441
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542442
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1542443
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1542444
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;-><init>()V

    .line 1542445
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542446
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542447
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->f:Ljava/lang/String;

    .line 1542448
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542449
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->g:Ljava/lang/String;

    .line 1542450
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1542451
    const v0, -0x644bb6b7

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542452
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1542453
    const v0, 0x285feb

    return v0
.end method

.method public final synthetic gK_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542454
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method
