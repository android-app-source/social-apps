.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1540525
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1540526
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1540527
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1540528
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1540529
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1540530
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1540531
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;
    .locals 8

    .prologue
    .line 1540532
    if-nez p0, :cond_0

    .line 1540533
    const/4 p0, 0x0

    .line 1540534
    :goto_0
    return-object p0

    .line 1540535
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    if-eqz v0, :cond_1

    .line 1540536
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    goto :goto_0

    .line 1540537
    :cond_1
    new-instance v0, LX/9og;

    invoke-direct {v0}, LX/9og;-><init>()V

    .line 1540538
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9og;->a:Ljava/lang/String;

    .line 1540539
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1540540
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1540541
    iget-object v3, v0, LX/9og;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1540542
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1540543
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1540544
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1540545
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1540546
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1540547
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1540548
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1540549
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;-><init>(LX/15i;)V

    .line 1540550
    move-object p0, v3

    .line 1540551
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1540552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1540553
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1540554
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1540555
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1540556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1540557
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1540558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1540559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1540560
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540561
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->e:Ljava/lang/String;

    .line 1540562
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1540563
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;-><init>()V

    .line 1540564
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1540565
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1540566
    const v0, -0x5a8ac105

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1540567
    const v0, 0x437b93b

    return v0
.end method
