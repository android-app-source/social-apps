.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9oI;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5c6cc542
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541983
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541984
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541985
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541986
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541987
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    .line 1541988
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1541989
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541990
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1541991
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1541992
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1541993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541994
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541996
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1541997
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    .line 1541998
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1541999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;

    .line 1542000
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel$PageModel;

    .line 1542001
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542002
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1542003
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionPageContextRowsPlaceholderComponentFragmentModel;-><init>()V

    .line 1542004
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542005
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1542006
    const v0, 0x71d9ba0e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1542007
    const v0, 0x1b5a957a

    return v0
.end method
