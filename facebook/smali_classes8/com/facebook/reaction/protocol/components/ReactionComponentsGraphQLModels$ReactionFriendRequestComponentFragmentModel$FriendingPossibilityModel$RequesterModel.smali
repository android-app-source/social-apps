.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x43af5c6e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541687
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541686
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541684
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541685
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1541681
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541682
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541683
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;
    .locals 11

    .prologue
    .line 1541652
    if-nez p0, :cond_0

    .line 1541653
    const/4 p0, 0x0

    .line 1541654
    :goto_0
    return-object p0

    .line 1541655
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    if-eqz v0, :cond_1

    .line 1541656
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    goto :goto_0

    .line 1541657
    :cond_1
    new-instance v0, LX/9oq;

    invoke-direct {v0}, LX/9oq;-><init>()V

    .line 1541658
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/9oq;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1541659
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oq;->b:Ljava/lang/String;

    .line 1541660
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oq;->c:Ljava/lang/String;

    .line 1541661
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v1

    iput-object v1, v0, LX/9oq;->d:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    .line 1541662
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1541663
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1541664
    iget-object v3, v0, LX/9oq;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1541665
    iget-object v5, v0, LX/9oq;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1541666
    iget-object v7, v0, LX/9oq;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1541667
    iget-object v8, v0, LX/9oq;->d:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1541668
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1541669
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1541670
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1541671
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1541672
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1541673
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1541674
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1541675
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1541676
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1541677
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1541678
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;-><init>(LX/15i;)V

    .line 1541679
    move-object p0, v3

    .line 1541680
    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1541645
    iput-object p1, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1541646
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1541647
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1541648
    if-eqz v0, :cond_0

    .line 1541649
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1541650
    :cond_0
    return-void

    .line 1541651
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541643
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    .line 1541644
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1541631
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541632
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1541633
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1541634
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1541635
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1541636
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1541637
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1541638
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1541639
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1541640
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1541641
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541642
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541601
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1541602
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    .line 1541603
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1541604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    .line 1541605
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->h:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    .line 1541606
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541607
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1541630
    new-instance v0, LX/9or;

    invoke-direct {v0, p1}, LX/9or;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541629
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1541623
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1541624
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1541625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1541626
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1541627
    :goto_0
    return-void

    .line 1541628
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1541620
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1541621
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1541622
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541618
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1541619
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1541615
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;-><init>()V

    .line 1541616
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541617
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541613
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->f:Ljava/lang/String;

    .line 1541614
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541611
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->g:Ljava/lang/String;

    .line 1541612
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1541610
    const v0, -0x404886c9

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541609
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1541608
    const v0, 0x285feb

    return v0
.end method
