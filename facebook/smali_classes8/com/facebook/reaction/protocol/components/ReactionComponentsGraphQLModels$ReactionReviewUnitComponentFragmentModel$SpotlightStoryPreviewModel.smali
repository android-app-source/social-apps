.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2fadcc53
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542213
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542212
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1542210
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542211
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1542207
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542208
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542209
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;
    .locals 9

    .prologue
    .line 1542182
    if-nez p0, :cond_0

    .line 1542183
    const/4 p0, 0x0

    .line 1542184
    :goto_0
    return-object p0

    .line 1542185
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    if-eqz v0, :cond_1

    .line 1542186
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    goto :goto_0

    .line 1542187
    :cond_1
    new-instance v0, LX/9ov;

    invoke-direct {v0}, LX/9ov;-><init>()V

    .line 1542188
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->a()I

    move-result v1

    iput v1, v0, LX/9ov;->a:I

    .line 1542189
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v1

    iput-object v1, v0, LX/9ov;->b:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    .line 1542190
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/9ov;->c:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1542191
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1542192
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1542193
    iget-object v3, v0, LX/9ov;->b:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1542194
    iget-object v5, v0, LX/9ov;->c:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1542195
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1542196
    iget v7, v0, LX/9ov;->a:I

    invoke-virtual {v2, v8, v7, v8}, LX/186;->a(III)V

    .line 1542197
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1542198
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1542199
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1542200
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1542201
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1542202
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1542203
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1542204
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;-><init>(LX/15i;)V

    .line 1542205
    move-object p0, v3

    .line 1542206
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542180
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    .line 1542181
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542178
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1542179
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1542214
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1542215
    iget v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1542146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542147
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1542148
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->k()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1542149
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1542150
    iget v2, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 1542151
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1542152
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1542153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542154
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1542165
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542166
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1542167
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    .line 1542168
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1542169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    .line 1542170
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    .line 1542171
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->k()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1542172
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->k()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1542173
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->k()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1542174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    .line 1542175
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1542176
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542177
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1542162
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1542163
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->e:I

    .line 1542164
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1542159
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;-><init>()V

    .line 1542160
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542161
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542158
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542157
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->k()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1542156
    const v0, -0x7050d6de

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1542155
    const v0, -0x6fefd79c

    return v0
.end method
