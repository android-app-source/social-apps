.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3af7f6c1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1543812
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1543857
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1543855
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1543856
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1543852
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1543853
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1543854
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;
    .locals 8

    .prologue
    .line 1543832
    if-nez p0, :cond_0

    .line 1543833
    const/4 p0, 0x0

    .line 1543834
    :goto_0
    return-object p0

    .line 1543835
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;

    if-eqz v0, :cond_1

    .line 1543836
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;

    goto :goto_0

    .line 1543837
    :cond_1
    new-instance v0, LX/9pG;

    invoke-direct {v0}, LX/9pG;-><init>()V

    .line 1543838
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9pG;->a:Ljava/lang/String;

    .line 1543839
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1543840
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1543841
    iget-object v3, v0, LX/9pG;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1543842
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1543843
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1543844
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1543845
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1543846
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1543847
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1543848
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1543849
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;-><init>(LX/15i;)V

    .line 1543850
    move-object p0, v3

    .line 1543851
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1543826
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1543827
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1543828
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1543829
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1543830
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1543831
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1543823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1543824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1543825
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1543858
    new-instance v0, LX/9pH;

    invoke-direct {v0, p1}, LX/9pH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1543821
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;->e:Ljava/lang/String;

    .line 1543822
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1543819
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1543820
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1543818
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1543815
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;-><init>()V

    .line 1543816
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1543817
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1543814
    const v0, -0x35ac81fa    # -3465089.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1543813
    const v0, -0x78fb05b

    return v0
.end method
