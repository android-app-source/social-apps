.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9oE;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1fe9da73
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541376
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541365
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541363
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541364
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541360
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1541361
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1541362
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541358
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    .line 1541359
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1541366
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541367
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1541368
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1541369
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1541370
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1541371
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1541372
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1541373
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1541374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541350
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541351
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1541352
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    .line 1541353
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1541354
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    .line 1541355
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    .line 1541356
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541357
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/9oD;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541349
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel$OfficialPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1541346
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;-><init>()V

    .line 1541347
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541348
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541344
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->g:Ljava/lang/String;

    .line 1541345
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1541343
    const v0, 0x72fc1672

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1541342
    const v0, 0x7757829c

    return v0
.end method
