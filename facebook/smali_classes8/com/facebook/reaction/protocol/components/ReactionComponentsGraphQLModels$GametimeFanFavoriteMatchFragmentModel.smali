.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/9oF;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1700e39
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541472
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541471
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541409
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541410
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1541465
    iput-boolean p1, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m:Z

    .line 1541466
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1541467
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1541468
    if-eqz v0, :cond_0

    .line 1541469
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1541470
    :cond_0
    return-void
.end method

.method private k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541463
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    .line 1541464
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541461
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->h:Ljava/lang/String;

    .line 1541462
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541459
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    .line 1541460
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    return-object v0
.end method

.method private n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541457
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    .line 1541458
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    return-object v0
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 1541455
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1541456
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1541436
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541437
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1541438
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1541439
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1541440
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1541441
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1541442
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1541443
    const/16 v6, 0x9

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1541444
    iget v6, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->e:I

    invoke-virtual {p1, v7, v6, v7}, LX/186;->a(III)V

    .line 1541445
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1541446
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1541447
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1541448
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1541449
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->j:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1541450
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1541451
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1541452
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1541453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541454
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541418
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541419
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1541420
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    .line 1541421
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1541422
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;

    .line 1541423
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    .line 1541424
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1541425
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    .line 1541426
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1541427
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;

    .line 1541428
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->i:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    .line 1541429
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1541430
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    .line 1541431
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1541432
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;

    .line 1541433
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    .line 1541434
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541435
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1541417
    new-instance v0, LX/9on;

    invoke-direct {v0, p1}, LX/9on;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541416
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1541411
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1541412
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->e:I

    .line 1541413
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->j:I

    .line 1541414
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m:Z

    .line 1541415
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1541384
    const-string v0, "viewer_can_vote_fan_favorite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1541385
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1541386
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1541387
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 1541388
    :goto_0
    return-void

    .line 1541389
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1541390
    const-string v0, "viewer_can_vote_fan_favorite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1541391
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->a(Z)V

    .line 1541392
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1541393
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1541394
    iget v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->e:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1541395
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;-><init>()V

    .line 1541396
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541397
    return-object v0
.end method

.method public final synthetic c()LX/9oA;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541398
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541399
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->g:Ljava/lang/String;

    .line 1541400
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1541401
    const v0, 0x5ef65794

    return v0
.end method

.method public final synthetic e()LX/9oB;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541402
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->m()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$MatchPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1541403
    const v0, 0x31509926

    return v0
.end method

.method public final gI_()I
    .locals 2

    .prologue
    .line 1541404
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1541405
    iget v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->j:I

    return v0
.end method

.method public final synthetic gJ_()LX/9oE;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541406
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$SecondTeamObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541407
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->l:Ljava/lang/String;

    .line 1541408
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method
