.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1543549
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1543550
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1543551
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1543552
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1543553
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1543554
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1543555
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1543556
    if-eqz v2, :cond_0

    .line 1543557
    const-string p0, "match"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1543558
    invoke-static {v1, v2, p1, p2}, LX/9q3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1543559
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1543560
    if-eqz v2, :cond_1

    .line 1543561
    const-string p0, "only_display_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1543562
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1543563
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1543564
    if-eqz v2, :cond_2

    .line 1543565
    const-string p0, "status_text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1543566
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1543567
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1543568
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1543569
    check-cast p1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
