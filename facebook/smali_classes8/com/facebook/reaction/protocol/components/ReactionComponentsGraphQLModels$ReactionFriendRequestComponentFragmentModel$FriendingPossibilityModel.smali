.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x15a2a42d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541743
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541742
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541740
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541741
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1541737
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541738
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541739
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;
    .locals 8

    .prologue
    .line 1541717
    if-nez p0, :cond_0

    .line 1541718
    const/4 p0, 0x0

    .line 1541719
    :goto_0
    return-object p0

    .line 1541720
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    if-eqz v0, :cond_1

    .line 1541721
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    goto :goto_0

    .line 1541722
    :cond_1
    new-instance v0, LX/9op;

    invoke-direct {v0}, LX/9op;-><init>()V

    .line 1541723
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v1

    iput-object v1, v0, LX/9op;->a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    .line 1541724
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1541725
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1541726
    iget-object v3, v0, LX/9op;->a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1541727
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1541728
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1541729
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1541730
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1541731
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1541732
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1541733
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1541734
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;-><init>(LX/15i;)V

    .line 1541735
    move-object p0, v3

    .line 1541736
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541715
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    .line 1541716
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1541709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541710
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1541711
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1541712
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1541713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541714
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541702
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1541703
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    .line 1541704
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1541705
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    .line 1541706
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    .line 1541707
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541708
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541700
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1541695
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;-><init>()V

    .line 1541696
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541697
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1541699
    const v0, 0x2398d3e1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1541698
    const v0, -0x5b53e988

    return v0
.end method
