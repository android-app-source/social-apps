.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/9o9;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x44d5c01a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541137
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1541136
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541134
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1541135
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541132
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    .line 1541133
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1541126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541127
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1541128
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1541129
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1541130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1541109
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1541110
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1541111
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    .line 1541112
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1541113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;

    .line 1541114
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->e:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    .line 1541115
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1541116
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1541138
    new-instance v0, LX/9ol;

    invoke-direct {v0, p1}, LX/9ol;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1541124
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1541125
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1541123
    return-void
.end method

.method public final synthetic b()LX/9o8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1541122
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1541119
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeFanFavoriteMatchFragmentModel$FirstTeamObjectModel$OfficialPageModel;-><init>()V

    .line 1541120
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1541121
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1541118
    const v0, 0x66562ef2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1541117
    const v0, 0x25d6af

    return v0
.end method
