.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x70fab3b0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1540394
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1540395
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1540396
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1540397
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1540441
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1540442
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1540443
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;
    .locals 11

    .prologue
    .line 1540398
    if-nez p0, :cond_0

    .line 1540399
    const/4 p0, 0x0

    .line 1540400
    :goto_0
    return-object p0

    .line 1540401
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    if-eqz v0, :cond_1

    .line 1540402
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    goto :goto_0

    .line 1540403
    :cond_1
    new-instance v0, LX/9oa;

    invoke-direct {v0}, LX/9oa;-><init>()V

    .line 1540404
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oa;->a:Ljava/lang/String;

    .line 1540405
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oa;->b:Ljava/lang/String;

    .line 1540406
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v1

    iput-object v1, v0, LX/9oa;->c:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    .line 1540407
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9oa;->d:Ljava/lang/String;

    .line 1540408
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1540409
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1540410
    iget-object v3, v0, LX/9oa;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1540411
    iget-object v5, v0, LX/9oa;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1540412
    iget-object v7, v0, LX/9oa;->c:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1540413
    iget-object v8, v0, LX/9oa;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1540414
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1540415
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1540416
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1540417
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1540418
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1540419
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1540420
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1540421
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1540422
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1540423
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1540424
    new-instance v3, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;-><init>(LX/15i;)V

    .line 1540425
    move-object p0, v3

    .line 1540426
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540427
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    .line 1540428
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1540429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1540430
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1540431
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1540432
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1540433
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1540434
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1540435
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1540436
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1540437
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1540438
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1540439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1540440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1540385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1540386
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1540387
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    .line 1540388
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1540389
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    .line 1540390
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    .line 1540391
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1540392
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1540393
    new-instance v0, LX/9ob;

    invoke-direct {v0, p1}, LX/9ob;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540369
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1540370
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1540371
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1540372
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1540373
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;-><init>()V

    .line 1540374
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1540375
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540376
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e:Ljava/lang/String;

    .line 1540377
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540378
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->f:Ljava/lang/String;

    .line 1540379
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540380
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1540381
    const v0, -0x42f048a0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1540382
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->h:Ljava/lang/String;

    .line 1540383
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1540384
    const v0, 0x25d6af

    return v0
.end method
