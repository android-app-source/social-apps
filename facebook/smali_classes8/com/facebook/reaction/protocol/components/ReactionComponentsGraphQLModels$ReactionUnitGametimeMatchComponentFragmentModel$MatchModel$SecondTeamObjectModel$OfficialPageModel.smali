.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x29c7db85
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1543344
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1543349
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1543347
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1543348
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1543345
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->e:Ljava/lang/String;

    .line 1543346
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1543317
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    .line 1543318
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1543336
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1543337
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1543338
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1543339
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1543340
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1543341
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1543342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1543343
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1543328
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1543329
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1543330
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    .line 1543331
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1543332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;

    .line 1543333
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->f:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    .line 1543334
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1543335
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1543350
    new-instance v0, LX/9pB;

    invoke-direct {v0, p1}, LX/9pB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1543327
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1543325
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1543326
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1543324
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1543321
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;-><init>()V

    .line 1543322
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1543323
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1543320
    const v0, 0x41625e5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1543319
    const v0, 0x25d6af

    return v0
.end method
