.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7d38b7b0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542688
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1542687
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1542685
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542686
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1542682
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1542683
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542684
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;
    .locals 11

    .prologue
    .line 1542651
    if-nez p0, :cond_0

    .line 1542652
    const/4 p0, 0x0

    .line 1542653
    :goto_0
    return-object p0

    .line 1542654
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;

    if-eqz v0, :cond_1

    .line 1542655
    check-cast p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;

    goto :goto_0

    .line 1542656
    :cond_1
    new-instance v2, LX/9ox;

    invoke-direct {v2}, LX/9ox;-><init>()V

    .line 1542657
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->a()Z

    move-result v0

    iput-boolean v0, v2, LX/9ox;->a:Z

    .line 1542658
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->b()Z

    move-result v0

    iput-boolean v0, v2, LX/9ox;->b:Z

    .line 1542659
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->c()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v0

    iput-object v0, v2, LX/9ox;->c:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    .line 1542660
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1542661
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1542662
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;->a(Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;)Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1542663
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1542664
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9ox;->d:LX/0Px;

    .line 1542665
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 1542666
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1542667
    iget-object v5, v2, LX/9ox;->c:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1542668
    iget-object v7, v2, LX/9ox;->d:LX/0Px;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1542669
    const/4 v9, 0x4

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 1542670
    iget-boolean v9, v2, LX/9ox;->a:Z

    invoke-virtual {v4, v10, v9}, LX/186;->a(IZ)V

    .line 1542671
    iget-boolean v9, v2, LX/9ox;->b:Z

    invoke-virtual {v4, v8, v9}, LX/186;->a(IZ)V

    .line 1542672
    const/4 v9, 0x2

    invoke-virtual {v4, v9, v5}, LX/186;->b(II)V

    .line 1542673
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v7}, LX/186;->b(II)V

    .line 1542674
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1542675
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1542676
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1542677
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1542678
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1542679
    new-instance v5, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;

    invoke-direct {v5, v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;-><init>(LX/15i;)V

    .line 1542680
    move-object p0, v5

    .line 1542681
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542610
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    .line 1542611
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1542641
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542642
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1542643
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->d()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1542644
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1542645
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1542646
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->f:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1542647
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1542648
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1542649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542650
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1542628
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1542629
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1542630
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    .line 1542631
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1542632
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;

    .line 1542633
    iput-object v0, v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->g:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    .line 1542634
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->d()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1542635
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1542636
    if-eqz v2, :cond_1

    .line 1542637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;

    .line 1542638
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 1542639
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1542640
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1542624
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1542625
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->e:Z

    .line 1542626
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->f:Z

    .line 1542627
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1542622
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1542623
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1542619
    new-instance v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;-><init>()V

    .line 1542620
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1542621
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1542617
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1542618
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->f:Z

    return v0
.end method

.method public final synthetic c()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1542616
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->j()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$RequesterModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1542614
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel$SuggestersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->h:Ljava/util/List;

    .line 1542615
    iget-object v0, p0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitFriendRequestListComponentFragmentModel$FriendingPossibilitiesModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1542613
    const v0, -0x988e7d6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1542612
    const v0, -0x5b53e988

    return v0
.end method
