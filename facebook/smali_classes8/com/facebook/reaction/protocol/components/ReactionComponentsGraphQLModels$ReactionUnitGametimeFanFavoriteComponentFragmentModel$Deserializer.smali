.class public final Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1542727
    const-class v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1542728
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1542729
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1542730
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1542731
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1542732
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1542733
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1542734
    :goto_0
    move v1, v2

    .line 1542735
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1542736
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1542737
    new-instance v1, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel;-><init>()V

    .line 1542738
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1542739
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1542740
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1542741
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1542742
    :cond_0
    return-object v1

    .line 1542743
    :cond_1
    const-string p0, "total_votes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1542744
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v4, v1

    move v1, v3

    .line 1542745
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_7

    .line 1542746
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1542747
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1542748
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 1542749
    const-string p0, "first_voting_page"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1542750
    invoke-static {p1, v0}, LX/9pt;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1542751
    :cond_3
    const-string p0, "match"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1542752
    invoke-static {p1, v0}, LX/9pg;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1542753
    :cond_4
    const-string p0, "second_voting_page"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1542754
    invoke-static {p1, v0}, LX/9pu;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1542755
    :cond_5
    const-string p0, "title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1542756
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1542757
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1542758
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1542759
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1542760
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1542761
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1542762
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1542763
    if-eqz v1, :cond_8

    .line 1542764
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4, v2}, LX/186;->a(III)V

    .line 1542765
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto :goto_1
.end method
