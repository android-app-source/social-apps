.class public final Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x68484c0e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1538408
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1538440
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1538438
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1538439
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1538435
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1538436
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1538437
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;)Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;
    .locals 10

    .prologue
    .line 1538409
    if-nez p0, :cond_0

    .line 1538410
    const/4 p0, 0x0

    .line 1538411
    :goto_0
    return-object p0

    .line 1538412
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;

    if-eqz v0, :cond_1

    .line 1538413
    check-cast p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;

    goto :goto_0

    .line 1538414
    :cond_1
    new-instance v0, LX/9nk;

    invoke-direct {v0}, LX/9nk;-><init>()V

    .line 1538415
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9nk;->a:Ljava/lang/String;

    .line 1538416
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9nk;->b:Ljava/lang/String;

    .line 1538417
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->d()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;->a(Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;)Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v1

    iput-object v1, v0, LX/9nk;->c:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    .line 1538418
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1538419
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1538420
    iget-object v3, v0, LX/9nk;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1538421
    iget-object v5, v0, LX/9nk;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1538422
    iget-object v7, v0, LX/9nk;->c:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1538423
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1538424
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1538425
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1538426
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1538427
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1538428
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1538429
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1538430
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1538431
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1538432
    new-instance v3, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;-><init>(LX/15i;)V

    .line 1538433
    move-object p0, v3

    .line 1538434
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538374
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->g:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->g:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    .line 1538375
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->g:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1538398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1538399
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1538400
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1538401
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1538402
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1538403
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1538404
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1538405
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1538406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1538407
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1538390
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1538391
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1538392
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    .line 1538393
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1538394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;

    .line 1538395
    iput-object v0, v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->g:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    .line 1538396
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1538397
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1538389
    new-instance v0, LX/9nl;

    invoke-direct {v0, p1}, LX/9nl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538441
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1538387
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1538388
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1538386
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1538383
    new-instance v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;-><init>()V

    .line 1538384
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1538385
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538381
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->e:Ljava/lang/String;

    .line 1538382
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538379
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->f:Ljava/lang/String;

    .line 1538380
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538378
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1538377
    const v0, 0xa73b2c8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1538376
    const v0, 0x25d6af

    return v0
.end method
