.class public final Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9ng;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x30e40e15
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1539323
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1539322
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1539290
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1539291
    return-void
.end method

.method private a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1539320
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1539321
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1539318
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    .line 1539319
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1539310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1539311
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1539312
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1539313
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1539314
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1539315
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1539316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1539317
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1539297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1539298
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1539299
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1539300
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1539301
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;

    .line 1539302
    iput-object v0, v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1539303
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1539304
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    .line 1539305
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1539306
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;

    .line 1539307
    iput-object v0, v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    .line 1539308
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1539309
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1539294
    new-instance v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel;-><init>()V

    .line 1539295
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1539296
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1539293
    const v0, 0x5d932b07

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1539292
    const v0, 0x5c439a75

    return v0
.end method
