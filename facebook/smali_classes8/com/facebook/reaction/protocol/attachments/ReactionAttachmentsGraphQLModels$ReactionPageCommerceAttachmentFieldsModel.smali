.class public final Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9nd;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x44a2970a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1538785
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1538784
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1538782
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1538783
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538780
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    .line 1538781
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1538774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1538775
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1538776
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1538777
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1538778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1538779
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1538766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1538767
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1538768
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    .line 1538769
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1538770
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;

    .line 1538771
    iput-object v0, v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    .line 1538772
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1538773
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1538761
    new-instance v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;-><init>()V

    .line 1538762
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1538763
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1538765
    const v0, 0x68e4b6fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1538764
    const v0, 0x12b5a405

    return v0
.end method
