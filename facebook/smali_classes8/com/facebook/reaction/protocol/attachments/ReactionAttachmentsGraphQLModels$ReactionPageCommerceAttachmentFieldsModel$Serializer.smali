.class public final Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1538758
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1538759
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1538760
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1538749
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1538750
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1538751
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1538752
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1538753
    if-eqz v2, :cond_0

    .line 1538754
    const-string p0, "product"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1538755
    invoke-static {v1, v2, p1, p2}, LX/9o1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1538756
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1538757
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1538748
    check-cast p1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$Serializer;->a(Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
