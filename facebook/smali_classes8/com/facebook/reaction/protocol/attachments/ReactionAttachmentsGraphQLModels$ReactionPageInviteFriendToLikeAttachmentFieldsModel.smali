.class public final Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9ne;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3993108f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1539003
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1539002
    const-class v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1539000
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1539001
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538998
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    .line 1538999
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    return-object v0
.end method

.method private j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1538970
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    .line 1538971
    iget-object v0, p0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1538990
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1538991
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1538992
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1538993
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1538994
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1538995
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1538996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1538997
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1538977
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1538978
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1538979
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    .line 1538980
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->a()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1538981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;

    .line 1538982
    iput-object v0, v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->e:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    .line 1538983
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1538984
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    .line 1538985
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->j()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1538986
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;

    .line 1538987
    iput-object v0, v1, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$PageModel;

    .line 1538988
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1538989
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1538974
    new-instance v0, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel;-><init>()V

    .line 1538975
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1538976
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1538973
    const v0, -0x1ee8e678

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1538972
    const v0, 0x56554534

    return v0
.end method
