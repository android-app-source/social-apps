.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1568090
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1568091
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1568089
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1568060
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1568061
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1568062
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1568063
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1568064
    if-eqz v2, :cond_0

    .line 1568065
    const-string p0, "action"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1568066
    invoke-static {v1, v2, p1, p2}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1568067
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1568068
    if-eqz v2, :cond_1

    .line 1568069
    const-string p0, "address"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1568070
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1568071
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1568072
    if-eqz v2, :cond_2

    .line 1568073
    const-string p0, "job_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1568074
    invoke-static {v1, v2, p1, p2}, LX/9we;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1568075
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1568076
    if-eqz v2, :cond_3

    .line 1568077
    const-string p0, "job_type"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1568078
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1568079
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1568080
    if-eqz v2, :cond_4

    .line 1568081
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1568082
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1568083
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1568084
    if-eqz v2, :cond_5

    .line 1568085
    const-string p0, "wage"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1568086
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1568087
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1568088
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1568059
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
