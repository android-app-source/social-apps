.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1567433
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1567434
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1567435
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1567436
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1567437
    const/4 v2, 0x0

    .line 1567438
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 1567439
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1567440
    :goto_0
    move v1, v2

    .line 1567441
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1567442
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1567443
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;-><init>()V

    .line 1567444
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1567445
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1567446
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1567447
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1567448
    :cond_0
    return-object v1

    .line 1567449
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1567450
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_9

    .line 1567451
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1567452
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1567453
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 1567454
    const-string p0, "affirmative_button_icon"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1567455
    invoke-static {p1, v0}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1567456
    :cond_3
    const-string p0, "affirmative_button_title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1567457
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1567458
    :cond_4
    const-string p0, "affirmative_result_text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1567459
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1567460
    :cond_5
    const-string p0, "negative_button_title"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1567461
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1567462
    :cond_6
    const-string p0, "negative_result_text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1567463
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1567464
    :cond_7
    const-string p0, "reaction_friend_request_state"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1567465
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1567466
    :cond_8
    const-string p0, "requester"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1567467
    invoke-static {p1, v0}, LX/9wb;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1567468
    :cond_9
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1567469
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1567470
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1567471
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1567472
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1567473
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1567474
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1567475
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1567476
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
