.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9oK;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x486b707
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1557710
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1557709
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1557707
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1557708
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1557704
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1557705
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1557706
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;
    .locals 15

    .prologue
    .line 1557661
    if-nez p0, :cond_0

    .line 1557662
    const/4 p0, 0x0

    .line 1557663
    :goto_0
    return-object p0

    .line 1557664
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    if-eqz v0, :cond_1

    .line 1557665
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    goto :goto_0

    .line 1557666
    :cond_1
    new-instance v0, LX/9sh;

    invoke-direct {v0}, LX/9sh;-><init>()V

    .line 1557667
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1557668
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    .line 1557669
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->d()I

    move-result v1

    iput v1, v0, LX/9sh;->c:I

    .line 1557670
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->d:Ljava/lang/String;

    .line 1557671
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->e:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 1557672
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->hf_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->f:Ljava/lang/String;

    .line 1557673
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->hg_()LX/5sY;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;->a(LX/5sY;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1557674
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->j()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557675
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->k()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sh;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557676
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v14, 0x0

    .line 1557677
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1557678
    iget-object v3, v0, LX/9sh;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1557679
    iget-object v5, v0, LX/9sh;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1557680
    iget-object v7, v0, LX/9sh;->d:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1557681
    iget-object v8, v0, LX/9sh;->e:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1557682
    iget-object v9, v0, LX/9sh;->f:Ljava/lang/String;

    invoke-virtual {v2, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1557683
    iget-object v10, v0, LX/9sh;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1557684
    iget-object v11, v0, LX/9sh;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1557685
    iget-object v12, v0, LX/9sh;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1557686
    const/16 v13, 0x9

    invoke-virtual {v2, v13}, LX/186;->c(I)V

    .line 1557687
    invoke-virtual {v2, v14, v3}, LX/186;->b(II)V

    .line 1557688
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1557689
    const/4 v3, 0x2

    iget v5, v0, LX/9sh;->c:I

    invoke-virtual {v2, v3, v5, v14}, LX/186;->a(III)V

    .line 1557690
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1557691
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1557692
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1557693
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1557694
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1557695
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1557696
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1557697
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1557698
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1557699
    invoke-virtual {v3, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1557700
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1557701
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;-><init>(LX/15i;)V

    .line 1557702
    move-object p0, v3

    .line 1557703
    goto/16 :goto_0
.end method

.method private l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557659
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    .line 1557660
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    return-object v0
.end method

.method private m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557657
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1557658
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557655
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557656
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557653
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557654
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1557632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1557633
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1557634
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1557635
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1557636
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1557637
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->hf_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1557638
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1557639
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1557640
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1557641
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1557642
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1557643
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1557644
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->g:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 1557645
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1557646
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1557647
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1557648
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1557649
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1557650
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1557651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1557652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1557609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1557610
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1557611
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    .line 1557612
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1557613
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    .line 1557614
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    .line 1557615
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1557616
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1557617
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1557618
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    .line 1557619
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1557620
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1557621
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557622
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1557623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    .line 1557624
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557625
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1557626
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557627
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1557628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    .line 1557629
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1557630
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1557631
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557607
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->i:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->i:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 1557608
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->i:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1557586
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1557587
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->g:I

    .line 1557588
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557604
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1557605
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1557606
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1557601
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;-><init>()V

    .line 1557602
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1557603
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557600
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1557598
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1557599
    iget v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1557597
    const v0, 0x1096a5ee

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557595
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->h:Ljava/lang/String;

    .line 1557596
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1557594
    const v0, -0x2605f7f0

    return v0
.end method

.method public final hf_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557592
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->j:Ljava/lang/String;

    .line 1557593
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic hg_()LX/5sY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557591
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557590
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557589
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method
