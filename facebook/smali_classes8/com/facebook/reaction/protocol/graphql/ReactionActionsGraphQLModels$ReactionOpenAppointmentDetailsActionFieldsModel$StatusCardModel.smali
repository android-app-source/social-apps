.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7dafc8b6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556664
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556665
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1556666
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556667
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1556693
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556694
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556695
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .locals 9

    .prologue
    .line 1556668
    if-nez p0, :cond_0

    .line 1556669
    const/4 p0, 0x0

    .line 1556670
    :goto_0
    return-object p0

    .line 1556671
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    if-eqz v0, :cond_1

    .line 1556672
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    goto :goto_0

    .line 1556673
    :cond_1
    new-instance v0, LX/9sV;

    invoke-direct {v0}, LX/9sV;-><init>()V

    .line 1556674
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v1

    iput-object v1, v0, LX/9sV;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    .line 1556675
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sV;->b:Ljava/lang/String;

    .line 1556676
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1556677
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1556678
    iget-object v3, v0, LX/9sV;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1556679
    iget-object v5, v0, LX/9sV;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1556680
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1556681
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1556682
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1556683
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1556684
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1556685
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1556686
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1556687
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1556688
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;-><init>(LX/15i;)V

    .line 1556689
    move-object p0, v3

    .line 1556690
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556691
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    .line 1556692
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1556639
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556640
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1556641
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1556642
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1556643
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1556644
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1556645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556646
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1556656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556657
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1556658
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    .line 1556659
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1556660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1556661
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    .line 1556662
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556663
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556655
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1556652
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;-><init>()V

    .line 1556653
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556654
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556651
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556649
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->f:Ljava/lang/String;

    .line 1556650
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1556648
    const v0, -0x15f6487d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1556647
    const v0, 0x513ada31

    return v0
.end method
