.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1583304
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1583305
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1583303
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1583269
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1583270
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    .line 1583271
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1583272
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1583273
    if-eqz v2, :cond_0

    .line 1583274
    const-string v3, "hour_ranges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583275
    invoke-static {v1, v2, p1, p2}, LX/4aY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1583276
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1583277
    if-eqz v2, :cond_1

    .line 1583278
    const-string v3, "place_category"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583279
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1583280
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1583281
    if-eqz v2, :cond_2

    .line 1583282
    const-string v3, "place_info_blurb_breadcrumbs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583283
    invoke-static {v1, v2, p1, p2}, LX/9y3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1583284
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1583285
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_3

    .line 1583286
    const-string v4, "place_info_blurb_rating"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583287
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1583288
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1583289
    if-eqz v2, :cond_4

    .line 1583290
    const-string v3, "price_range"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583291
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1583292
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1583293
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_5

    .line 1583294
    const-string v4, "rating_count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583295
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1583296
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1583297
    if-eqz v2, :cond_6

    .line 1583298
    const-string v3, "timezone"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1583299
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1583300
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1583301
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1583302
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
