.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x40aac1b8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556163
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556162
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1556160
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556161
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1556157
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556158
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556159
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;
    .locals 9

    .prologue
    .line 1556134
    if-nez p0, :cond_0

    .line 1556135
    const/4 p0, 0x0

    .line 1556136
    :goto_0
    return-object p0

    .line 1556137
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    if-eqz v0, :cond_1

    .line 1556138
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    goto :goto_0

    .line 1556139
    :cond_1
    new-instance v0, LX/9sR;

    invoke-direct {v0}, LX/9sR;-><init>()V

    .line 1556140
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9sR;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1556141
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->b()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sR;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1556142
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1556143
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1556144
    iget-object v3, v0, LX/9sR;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1556145
    iget-object v5, v0, LX/9sR;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1556146
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1556147
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1556148
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1556149
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1556150
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1556151
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1556152
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1556153
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1556154
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;-><init>(LX/15i;)V

    .line 1556155
    move-object p0, v3

    .line 1556156
    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556132
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1556133
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1556124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556125
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1556126
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1556127
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1556128
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1556129
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1556130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1556164
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556165
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1556166
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1556167
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1556168
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    .line 1556169
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1556170
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556171
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1556114
    new-instance v0, LX/9sS;

    invoke-direct {v0, p1}, LX/9sS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556111
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1556112
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1556113
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1556115
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1556116
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1556117
    return-void
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556118
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1556119
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;-><init>()V

    .line 1556120
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556121
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1556123
    const v0, -0x560cbc72

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1556122
    const v0, 0x3c2b9d5

    return v0
.end method
