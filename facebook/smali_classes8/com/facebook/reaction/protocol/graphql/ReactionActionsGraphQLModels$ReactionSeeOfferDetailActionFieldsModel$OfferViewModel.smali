.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x36ab0b96
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1558139
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1558140
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1558092
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1558093
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1558136
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1558137
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1558138
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
    .locals 9

    .prologue
    .line 1558113
    if-nez p0, :cond_0

    .line 1558114
    const/4 p0, 0x0

    .line 1558115
    :goto_0
    return-object p0

    .line 1558116
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    if-eqz v0, :cond_1

    .line 1558117
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    goto :goto_0

    .line 1558118
    :cond_1
    new-instance v0, LX/9sj;

    invoke-direct {v0}, LX/9sj;-><init>()V

    .line 1558119
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sj;->a:Ljava/lang/String;

    .line 1558120
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/9sj;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    .line 1558121
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1558122
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1558123
    iget-object v3, v0, LX/9sj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1558124
    iget-object v5, v0, LX/9sj;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1558125
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1558126
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1558127
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1558128
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1558129
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1558130
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1558131
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1558132
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1558133
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;-><init>(LX/15i;)V

    .line 1558134
    move-object p0, v3

    .line 1558135
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1558111
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    .line 1558112
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1558103
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1558104
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1558105
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1558106
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1558107
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1558108
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1558109
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1558110
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1558141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1558142
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1558143
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    .line 1558144
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1558145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    .line 1558146
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    .line 1558147
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1558148
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1558102
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1558099
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;-><init>()V

    .line 1558100
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1558101
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1558097
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->e:Ljava/lang/String;

    .line 1558098
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1558096
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1558095
    const v0, 0x63270b4b    # 3.08142E21f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1558094
    const v0, -0x14a5a4ff

    return v0
.end method
