.class public final Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1547603
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1547602
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1547560
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1547561
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1547599
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1547600
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1547601
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;
    .locals 8

    .prologue
    .line 1547580
    if-nez p0, :cond_0

    .line 1547581
    const/4 p0, 0x0

    .line 1547582
    :goto_0
    return-object p0

    .line 1547583
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    if-eqz v0, :cond_1

    .line 1547584
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    goto :goto_0

    .line 1547585
    :cond_1
    new-instance v0, LX/9qb;

    invoke-direct {v0}, LX/9qb;-><init>()V

    .line 1547586
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;->a()I

    move-result v1

    iput v1, v0, LX/9qb;->a:I

    .line 1547587
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1547588
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1547589
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1547590
    iget v3, v0, LX/9qb;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1547591
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1547592
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1547593
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1547594
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1547595
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1547596
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;-><init>(LX/15i;)V

    .line 1547597
    move-object p0, v3

    .line 1547598
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1547578
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1547579
    iget v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1547573
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1547574
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1547575
    iget v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1547576
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1547577
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1547570
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1547571
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1547572
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1547567
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1547568
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;->e:I

    .line 1547569
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1547564
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;-><init>()V

    .line 1547565
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1547566
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1547563
    const v0, -0x6ecdb51f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1547562
    const v0, 0x25f82de5

    return v0
.end method
