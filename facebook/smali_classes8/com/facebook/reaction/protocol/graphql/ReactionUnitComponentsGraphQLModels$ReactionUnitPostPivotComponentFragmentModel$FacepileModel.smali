.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1f1f5795
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1583468
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1583467
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1583465
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1583466
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1583413
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1583414
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1583415
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;
    .locals 9

    .prologue
    .line 1583442
    if-nez p0, :cond_0

    .line 1583443
    const/4 p0, 0x0

    .line 1583444
    :goto_0
    return-object p0

    .line 1583445
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;

    if-eqz v0, :cond_1

    .line 1583446
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;

    goto :goto_0

    .line 1583447
    :cond_1
    new-instance v0, LX/9wS;

    invoke-direct {v0}, LX/9wS;-><init>()V

    .line 1583448
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    move-result-object v1

    iput-object v1, v0, LX/9wS;->a:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 1583449
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->b()LX/5sX;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;->a(LX/5sX;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    move-result-object v1

    iput-object v1, v0, LX/9wS;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    .line 1583450
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1583451
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1583452
    iget-object v3, v0, LX/9wS;->a:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1583453
    iget-object v5, v0, LX/9wS;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1583454
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1583455
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1583456
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1583457
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1583458
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1583459
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1583460
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1583461
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1583462
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;-><init>(LX/15i;)V

    .line 1583463
    move-object p0, v3

    .line 1583464
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1583440
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    .line 1583441
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1583432
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1583433
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1583434
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1583435
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1583436
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1583437
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1583438
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1583439
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1583424
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1583425
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1583426
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    .line 1583427
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1583428
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;

    .line 1583429
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    .line 1583430
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1583431
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1583422
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 1583423
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    return-object v0
.end method

.method public final synthetic b()LX/5sX;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1583421
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFacepileProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1583418
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPostPivotComponentFragmentModel$FacepileModel;-><init>()V

    .line 1583419
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1583420
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1583417
    const v0, 0x1e6d3534

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1583416
    const v0, -0x4cec28dc

    return v0
.end method
