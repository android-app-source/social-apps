.class public final Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x42c7fc11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1550394
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1550393
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1550391
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1550392
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1550385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1550386
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1550387
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1550388
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1550389
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1550390
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1550377
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1550378
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1550379
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    .line 1550380
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1550381
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    .line 1550382
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    .line 1550383
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1550384
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550370
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    .line 1550371
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1550374
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;-><init>()V

    .line 1550375
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1550376
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1550373
    const v0, -0x3858ec30    # -85543.625f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1550372
    const v0, -0x6747e1ce

    return v0
.end method
