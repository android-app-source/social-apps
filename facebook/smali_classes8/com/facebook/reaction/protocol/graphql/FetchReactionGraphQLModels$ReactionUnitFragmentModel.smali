.class public final Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/9qS;
.implements LX/9qX;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x26e39a51
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I

.field private p:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1550827
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1550830
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1550831
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1550832
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1550833
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1550834
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1550835
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 4

    .prologue
    .line 1550836
    if-nez p0, :cond_0

    .line 1550837
    const/4 p0, 0x0

    .line 1550838
    :goto_0
    return-object p0

    .line 1550839
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-eqz v0, :cond_1

    .line 1550840
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    goto :goto_0

    .line 1550841
    :cond_1
    new-instance v2, LX/9qr;

    invoke-direct {v2}, LX/9qr;-><init>()V

    .line 1550842
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1550843
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->b:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    .line 1550844
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->p()Z

    move-result v0

    iput-boolean v0, v2, LX/9qr;->c:Z

    .line 1550845
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->d:Ljava/lang/String;

    .line 1550846
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->e:Ljava/lang/String;

    .line 1550847
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gW_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->f:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 1550848
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    .line 1550849
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    .line 1550850
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1550851
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1550852
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1550853
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1550854
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->i:LX/0Px;

    .line 1550855
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->j:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 1550856
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k()I

    move-result v0

    iput v0, v2, LX/9qr;->k:I

    .line 1550857
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->l:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 1550858
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->m:Ljava/lang/String;

    .line 1550859
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->n()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1550860
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->o()LX/1U8;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;->a(LX/1U8;)Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v0

    iput-object v0, v2, LX/9qr;->o:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 1550861
    invoke-virtual {v2}, LX/9qr;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object p0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 1550862
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1550863
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1550864
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1550865
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1550866
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1550867
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1550868
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->t()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1550869
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->u()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1550870
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1550871
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->v()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1550872
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1550873
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1550874
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->w()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v11

    invoke-static {p1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1550875
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->x()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v12

    invoke-static {p1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1550876
    const/16 v13, 0xf

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 1550877
    const/4 v13, 0x0

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 1550878
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1550879
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1550880
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1550881
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1550882
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1550883
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1550884
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1550885
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1550886
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1550887
    const/16 v0, 0xa

    iget v1, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->o:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1550888
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1550889
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1550890
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1550891
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1550892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1550893
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1550894
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1550895
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1550896
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 1550897
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1550898
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550899
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 1550900
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->t()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1550901
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->t()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    .line 1550902
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->t()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1550903
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550904
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    .line 1550905
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->u()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1550906
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->u()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    .line 1550907
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->u()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1550908
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550909
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    .line 1550910
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1550911
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1550912
    if-eqz v2, :cond_3

    .line 1550913
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550914
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m:Ljava/util/List;

    move-object v1, v0

    .line 1550915
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->v()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1550916
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->v()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 1550917
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->v()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1550918
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550919
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 1550920
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->w()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1550921
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->w()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1550922
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->w()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1550923
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550924
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1550925
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->x()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1550926
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->x()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 1550927
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->x()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1550928
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1550929
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 1550930
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1550931
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550932
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1550933
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1550934
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->g:Z

    .line 1550935
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->o:I

    .line 1550936
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550937
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1550938
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1550939
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1550940
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;-><init>()V

    .line 1550941
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1550942
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550943
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    .line 1550944
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550945
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->h:Ljava/lang/String;

    .line 1550946
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1550947
    const v0, -0x634e1f94

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550948
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->i:Ljava/lang/String;

    .line 1550949
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1550828
    const v0, -0x5c7cc093

    return v0
.end method

.method public final synthetic gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550829
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->u()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic gW_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550800
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550801
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->v()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1550802
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1550803
    iget v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->o:I

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550804
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->p:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->p:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 1550805
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->p:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550806
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q:Ljava/lang/String;

    .line 1550807
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic n()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550808
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->w()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic o()LX/1U8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550809
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->x()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 1550810
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1550811
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->g:Z

    return v0
.end method

.method public final synthetic q()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550812
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->t()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1550813
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m:Ljava/util/List;

    .line 1550814
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550815
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    .line 1550816
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    return-object v0
.end method

.method public final t()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550817
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    .line 1550818
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    return-object v0
.end method

.method public final u()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550819
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    .line 1550820
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    return-object v0
.end method

.method public final v()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550821
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 1550822
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550823
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1550824
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final x()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1550825
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 1550826
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->s:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    return-object v0
.end method
