.class public final Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x73b23e90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1564289
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1564292
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1564290
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1564291
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1564281
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    .line 1564282
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1564283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1564284
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1564285
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1564286
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1564287
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1564288
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1564273
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1564274
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1564275
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    .line 1564276
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1564277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;

    .line 1564278
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel$ViewerModel;

    .line 1564279
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1564280
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1564270
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionMutationFragmentsModels$AdsAfterPartyXoutMutationFragmentModel;-><init>()V

    .line 1564271
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1564272
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1564268
    const v0, -0x5c8cc6bb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1564269
    const v0, -0x1ae1f7cd

    return v0
.end method
