.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1553746
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1553747
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1553745
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1553528
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1553529
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x1a

    const/16 v6, 0x16

    const/16 v5, 0xe

    const/16 v4, 0xc

    const/4 v3, 0x0

    .line 1553530
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1553531
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1553532
    if-eqz v2, :cond_0

    .line 1553533
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553534
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1553535
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553536
    if-eqz v2, :cond_1

    .line 1553537
    const-string v3, "action_default_activated_submessage"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553538
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1553539
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553540
    if-eqz v2, :cond_2

    .line 1553541
    const-string v3, "action_default_message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553542
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1553543
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553544
    if-eqz v2, :cond_3

    .line 1553545
    const-string v3, "action_og_object"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553546
    invoke-static {v1, v2, p1}, LX/9tQ;->a(LX/15i;ILX/0nX;)V

    .line 1553547
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553548
    if-eqz v2, :cond_4

    .line 1553549
    const-string v3, "album"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553550
    invoke-static {v1, v2, p1}, LX/9tV;->a(LX/15i;ILX/0nX;)V

    .line 1553551
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553552
    if-eqz v2, :cond_5

    .line 1553553
    const-string v3, "author"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553554
    invoke-static {v1, v2, p1, p2}, LX/9t9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553555
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1553556
    if-eqz v2, :cond_6

    .line 1553557
    const-string v3, "can_share_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553558
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1553559
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553560
    if-eqz v2, :cond_7

    .line 1553561
    const-string v3, "collection"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553562
    invoke-static {v1, v2, p1}, LX/9tf;->a(LX/15i;ILX/0nX;)V

    .line 1553563
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553564
    if-eqz v2, :cond_8

    .line 1553565
    const-string v3, "comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553566
    invoke-static {v1, v2, p1, p2}, LX/9tl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553567
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553568
    if-eqz v2, :cond_9

    .line 1553569
    const-string v3, "component_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553570
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553571
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553572
    if-eqz v2, :cond_a

    .line 1553573
    const-string v3, "components_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553574
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553575
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553576
    if-eqz v2, :cond_b

    .line 1553577
    const-string v3, "composer_inline_activity"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553578
    invoke-static {v1, v2, p1, p2}, LX/9tD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553579
    :cond_b
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1553580
    if-eqz v2, :cond_c

    .line 1553581
    const-string v2, "connection_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553582
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553583
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553584
    if-eqz v2, :cond_d

    .line 1553585
    const-string v3, "display_style"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553586
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553587
    :cond_d
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1553588
    if-eqz v2, :cond_e

    .line 1553589
    const-string v2, "entry_point"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553590
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553591
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553592
    if-eqz v2, :cond_f

    .line 1553593
    const-string v3, "event"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553594
    invoke-static {v1, v2, p1, p2}, LX/9sz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553595
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553596
    if-eqz v2, :cond_10

    .line 1553597
    const-string v3, "event_space"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553598
    invoke-static {v1, v2, p1, p2}, LX/9tE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553599
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553600
    if-eqz v2, :cond_11

    .line 1553601
    const-string v3, "friend"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553602
    invoke-static {v1, v2, p1}, LX/9tM;->a(LX/15i;ILX/0nX;)V

    .line 1553603
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553604
    if-eqz v2, :cond_12

    .line 1553605
    const-string v3, "full_address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553606
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553607
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553608
    if-eqz v2, :cond_13

    .line 1553609
    const-string v3, "fundraiser"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553610
    invoke-static {v1, v2, p1}, LX/9tp;->a(LX/15i;ILX/0nX;)V

    .line 1553611
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553612
    if-eqz v2, :cond_14

    .line 1553613
    const-string v3, "fundraiser_interface"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553614
    invoke-static {v1, v2, p1, p2}, LX/9tP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553615
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553616
    if-eqz v2, :cond_15

    .line 1553617
    const-string v3, "group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553618
    invoke-static {v1, v2, p1}, LX/9t0;->a(LX/15i;ILX/0nX;)V

    .line 1553619
    :cond_15
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1553620
    if-eqz v2, :cond_16

    .line 1553621
    const-string v2, "guest_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553622
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553623
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553624
    if-eqz v2, :cond_17

    .line 1553625
    const-string v3, "job_opening"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553626
    invoke-static {v1, v2, p1}, LX/9tc;->a(LX/15i;ILX/0nX;)V

    .line 1553627
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553628
    if-eqz v2, :cond_18

    .line 1553629
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553630
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1553631
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553632
    if-eqz v2, :cond_19

    .line 1553633
    const-string v3, "match_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553634
    invoke-static {v1, v2, p1}, LX/9tX;->a(LX/15i;ILX/0nX;)V

    .line 1553635
    :cond_19
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1553636
    if-eqz v2, :cond_1a

    .line 1553637
    const-string v2, "nux_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553638
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553639
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553640
    if-eqz v2, :cond_1b

    .line 1553641
    const-string v3, "offer_view"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553642
    invoke-static {v1, v2, p1, p2}, LX/9te;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553643
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553644
    if-eqz v2, :cond_1c

    .line 1553645
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553646
    invoke-static {v1, v2, p1, p2}, LX/9t1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553647
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553648
    if-eqz v2, :cond_1d

    .line 1553649
    const-string v3, "phone_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553650
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553651
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553652
    if-eqz v2, :cond_1e

    .line 1553653
    const-string v2, "photo_source_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553654
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553655
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553656
    if-eqz v2, :cond_1f

    .line 1553657
    const-string v3, "place_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553658
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553659
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553660
    if-eqz v2, :cond_20

    .line 1553661
    const-string v3, "places_query_location_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553662
    invoke-static {v1, v2, p1}, LX/9tU;->a(LX/15i;ILX/0nX;)V

    .line 1553663
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553664
    if-eqz v2, :cond_21

    .line 1553665
    const-string v3, "places_query_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553666
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553667
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553668
    if-eqz v2, :cond_22

    .line 1553669
    const-string v3, "places_query_topic_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553670
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553671
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553672
    if-eqz v2, :cond_23

    .line 1553673
    const-string v3, "post_target"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553674
    invoke-static {v1, v2, p1, p2}, LX/9tG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553675
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553676
    if-eqz v2, :cond_24

    .line 1553677
    const-string v3, "profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553678
    invoke-static {v1, v2, p1, p2}, LX/9t2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553679
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553680
    if-eqz v2, :cond_25

    .line 1553681
    const-string v3, "profile_pic_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553682
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553683
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553684
    if-eqz v2, :cond_26

    .line 1553685
    const-string v3, "query"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553686
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553687
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553688
    if-eqz v2, :cond_27

    .line 1553689
    const-string v3, "related_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553690
    invoke-static {v1, v2, p1, p2}, LX/9t3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553691
    :cond_27
    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553692
    if-eqz v2, :cond_28

    .line 1553693
    const-string v3, "replacement_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553694
    invoke-static {v1, v2, p1, p2}, LX/9tb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553695
    :cond_28
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553696
    if-eqz v2, :cond_29

    .line 1553697
    const-string v3, "service"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553698
    invoke-static {v1, v2, p1, p2}, LX/9a3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553699
    :cond_29
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553700
    if-eqz v2, :cond_2a

    .line 1553701
    const-string v3, "source"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553702
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553703
    :cond_2a
    const/16 v2, 0x2b

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553704
    if-eqz v2, :cond_2b

    .line 1553705
    const-string v3, "source_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553706
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553707
    :cond_2b
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553708
    if-eqz v2, :cond_2c

    .line 1553709
    const-string v3, "status_card"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553710
    invoke-static {v1, v2, p1, p2}, LX/9tT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553711
    :cond_2c
    const/16 v2, 0x2d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553712
    if-eqz v2, :cond_2d

    .line 1553713
    const-string v3, "story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553714
    invoke-static {v1, v2, p1, p2}, LX/9t5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553715
    :cond_2d
    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1553716
    if-eqz v2, :cond_2e

    .line 1553717
    const-string v3, "subscribe_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553718
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1553719
    :cond_2e
    const/16 v2, 0x2f

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553720
    if-eqz v2, :cond_2f

    .line 1553721
    const-string v3, "suggestion_token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553722
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553723
    :cond_2f
    const/16 v2, 0x30

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553724
    if-eqz v2, :cond_30

    .line 1553725
    const-string v3, "target"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553726
    invoke-static {v1, v2, p1, p2}, LX/9tJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553727
    :cond_30
    const/16 v2, 0x31

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553728
    if-eqz v2, :cond_31

    .line 1553729
    const-string v3, "thread_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553730
    invoke-static {v1, v2, p1}, LX/9tg;->a(LX/15i;ILX/0nX;)V

    .line 1553731
    :cond_31
    const/16 v2, 0x32

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553732
    if-eqz v2, :cond_32

    .line 1553733
    const-string v3, "unit_type_token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553734
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553735
    :cond_32
    const/16 v2, 0x33

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1553736
    if-eqz v2, :cond_33

    .line 1553737
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553738
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1553739
    :cond_33
    const/16 v2, 0x34

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1553740
    if-eqz v2, :cond_34

    .line 1553741
    const-string v3, "video_channel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1553742
    invoke-static {v1, v2, p1, p2}, LX/9tY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1553743
    :cond_34
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1553744
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1553527
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
