.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x19eb50c2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1568544
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1568543
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1568541
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1568542
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1568538
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1568539
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1568540
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;
    .locals 11

    .prologue
    .line 1568509
    if-nez p0, :cond_0

    .line 1568510
    const/4 p0, 0x0

    .line 1568511
    :goto_0
    return-object p0

    .line 1568512
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    if-eqz v0, :cond_1

    .line 1568513
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    goto :goto_0

    .line 1568514
    :cond_1
    new-instance v0, LX/9us;

    invoke-direct {v0}, LX/9us;-><init>()V

    .line 1568515
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9us;->a:Ljava/lang/String;

    .line 1568516
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9us;->b:Ljava/lang/String;

    .line 1568517
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9us;->c:Ljava/lang/String;

    .line 1568518
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v1

    iput-object v1, v0, LX/9us;->d:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    .line 1568519
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1568520
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1568521
    iget-object v3, v0, LX/9us;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1568522
    iget-object v5, v0, LX/9us;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1568523
    iget-object v7, v0, LX/9us;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1568524
    iget-object v8, v0, LX/9us;->d:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1568525
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1568526
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1568527
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1568528
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1568529
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1568530
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1568531
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1568532
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1568533
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1568534
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1568535
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;-><init>(LX/15i;)V

    .line 1568536
    move-object p0, v3

    .line 1568537
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1568475
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    .line 1568476
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1568497
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1568498
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1568499
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1568500
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1568501
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1568502
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1568503
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1568504
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1568505
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1568506
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1568507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1568508
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1568489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1568490
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1568491
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    .line 1568492
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1568493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    .line 1568494
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    .line 1568495
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1568496
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1568487
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->e:Ljava/lang/String;

    .line 1568488
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1568484
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;-><init>()V

    .line 1568485
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1568486
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1568482
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->f:Ljava/lang/String;

    .line 1568483
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1568480
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->g:Ljava/lang/String;

    .line 1568481
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1568479
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel$IconImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1568478
    const v0, -0x6f36844f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1568477
    const v0, -0x476a6334

    return v0
.end method
