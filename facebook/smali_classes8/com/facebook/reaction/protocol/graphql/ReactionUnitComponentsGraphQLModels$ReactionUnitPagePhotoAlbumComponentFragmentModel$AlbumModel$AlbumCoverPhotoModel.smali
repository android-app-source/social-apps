.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x61b1cf66
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1582912
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1582915
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1582913
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1582914
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1582909
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1582910
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1582911
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;
    .locals 9

    .prologue
    .line 1582886
    if-nez p0, :cond_0

    .line 1582887
    const/4 p0, 0x0

    .line 1582888
    :goto_0
    return-object p0

    .line 1582889
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    if-eqz v0, :cond_1

    .line 1582890
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    goto :goto_0

    .line 1582891
    :cond_1
    new-instance v0, LX/9wN;

    invoke-direct {v0}, LX/9wN;-><init>()V

    .line 1582892
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9wN;->a:Ljava/lang/String;

    .line 1582893
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v1

    iput-object v1, v0, LX/9wN;->b:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    .line 1582894
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1582895
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1582896
    iget-object v3, v0, LX/9wN;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1582897
    iget-object v5, v0, LX/9wN;->b:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1582898
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1582899
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1582900
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1582901
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1582902
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1582903
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1582904
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1582905
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1582906
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;-><init>(LX/15i;)V

    .line 1582907
    move-object p0, v3

    .line 1582908
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1582884
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    .line 1582885
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1582916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1582917
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1582918
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1582919
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1582920
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1582921
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1582922
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1582923
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1582876
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1582877
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1582878
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    .line 1582879
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1582880
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    .line 1582881
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    .line 1582882
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1582883
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1582875
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1582872
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;-><init>()V

    .line 1582873
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1582874
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1582870
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->e:Ljava/lang/String;

    .line 1582871
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1582867
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1582869
    const v0, -0x23f2a2bd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1582868
    const v0, 0x4984e12

    return v0
.end method
