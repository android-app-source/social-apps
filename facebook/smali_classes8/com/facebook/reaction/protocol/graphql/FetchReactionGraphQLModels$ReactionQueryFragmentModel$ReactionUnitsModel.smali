.class public final Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9qT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5ff1fab8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1548770
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1548749
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1548750
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1548751
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1548752
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1548753
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1548754
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1548755
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1548756
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1548757
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1548758
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1548759
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1548760
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1548761
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1548762
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1548763
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1548764
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1548765
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1548766
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1548767
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1548768
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1548769
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1548729
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->f:Ljava/util/List;

    .line 1548730
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1548731
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1548732
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1548733
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1548734
    if-eqz v1, :cond_3

    .line 1548735
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    .line 1548736
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1548737
    :goto_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1548738
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1548739
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1548740
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    .line 1548741
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1548742
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1548743
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1548744
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1548745
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    .line 1548746
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1548747
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1548748
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1548726
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;-><init>()V

    .line 1548727
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1548728
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1548725
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1548722
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1548723
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1548724
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final synthetic d()LX/0us;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1548717
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1548721
    const v0, 0x68bf82

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1548719
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->i:Ljava/lang/String;

    .line 1548720
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1548718
    const v0, 0x46457224

    return v0
.end method
