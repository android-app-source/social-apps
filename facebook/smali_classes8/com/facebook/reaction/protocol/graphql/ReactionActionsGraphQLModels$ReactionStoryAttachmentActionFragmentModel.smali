.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9rk;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x59250d78
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:Z

.field private af:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ai:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ak:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1559647
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1559646
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1559644
    const/16 v0, 0x3b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1559645
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1559641
    const/16 v0, 0x3b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1559642
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1559643
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1559567
    if-nez p0, :cond_0

    .line 1559568
    const/4 p0, 0x0

    .line 1559569
    :goto_0
    return-object p0

    .line 1559570
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v0, :cond_1

    .line 1559571
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    goto :goto_0

    .line 1559572
    :cond_1
    new-instance v3, LX/9sr;

    invoke-direct {v3}, LX/9sr;-><init>()V

    .line 1559573
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1559574
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->b()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->b:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559575
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->c()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559576
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;->a(LX/5sY;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1559577
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559578
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->hi_()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559579
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->hh_()Z

    move-result v0

    iput-boolean v0, v3, LX/9sr;->g:Z

    .line 1559580
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    .line 1559581
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 1559582
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    .line 1559583
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->k:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1559584
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->n()Z

    move-result v0

    iput-boolean v0, v3, LX/9sr;->l:Z

    .line 1559585
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->o()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    .line 1559586
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    .line 1559587
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->o:Ljava/lang/String;

    .line 1559588
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->p:Ljava/lang/String;

    .line 1559589
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1559590
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->r:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    .line 1559591
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->s:Ljava/lang/String;

    .line 1559592
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->v()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 1559593
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    .line 1559594
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1559595
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->w:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    .line 1559596
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->x:Ljava/lang/String;

    .line 1559597
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    .line 1559598
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1559599
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    .line 1559600
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->B:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1559601
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1559602
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1559603
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9rk;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;->a(LX/9rk;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1559604
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1559605
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->C:LX/0Px;

    .line 1559606
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->E()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    .line 1559607
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->F()LX/1k1;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a(LX/1k1;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1559608
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->F:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    .line 1559609
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->G:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 1559610
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    .line 1559611
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->I:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    .line 1559612
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->J:Ljava/lang/String;

    .line 1559613
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->K:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    .line 1559614
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->L:Ljava/lang/String;

    .line 1559615
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->N()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    .line 1559616
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->O()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->N:Ljava/lang/String;

    .line 1559617
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->P()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->O:Ljava/lang/String;

    .line 1559618
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->P:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1559619
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 1559620
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->S()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->R:Ljava/lang/String;

    .line 1559621
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->S:Ljava/lang/String;

    .line 1559622
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1559623
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1559624
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1559625
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1559626
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->T:LX/0Px;

    .line 1559627
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->V()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    .line 1559628
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->W()LX/9Zu;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->a(LX/9Zu;)Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->V:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 1559629
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->W:Ljava/lang/String;

    .line 1559630
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->X:Ljava/lang/String;

    .line 1559631
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Z()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1559632
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->Z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    .line 1559633
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ab()Z

    move-result v0

    iput-boolean v0, v3, LX/9sr;->aa:Z

    .line 1559634
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->ab:Ljava/lang/String;

    .line 1559635
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ad()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    .line 1559636
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ae()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    .line 1559637
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->af()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->ae:Ljava/lang/String;

    .line 1559638
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->af:Ljava/lang/String;

    .line 1559639
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    iput-object v0, v3, LX/9sr;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    .line 1559640
    invoke-virtual {v3}, LX/9sr;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object p0

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559566
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559565
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559564
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559562
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->F:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->F:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1559563
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->F:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559561
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()LX/1k1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559560
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559559
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559537
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->K:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->K:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 1559538
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->K:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    return-object v0
.end method

.method public final synthetic I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559557
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559556
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559554
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->N:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->N:Ljava/lang/String;

    .line 1559555
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559552
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->O:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->O:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    .line 1559553
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->O:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559550
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->P:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->P:Ljava/lang/String;

    .line 1559551
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic N()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559549
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559547
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R:Ljava/lang/String;

    .line 1559548
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559545
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->S:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->S:Ljava/lang/String;

    .line 1559546
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->S:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559544
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559543
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559541
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->V:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->V:Ljava/lang/String;

    .line 1559542
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->V:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559539
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->W:Ljava/lang/String;

    .line 1559540
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final U()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1559649
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X:Ljava/util/List;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X:Ljava/util/List;

    .line 1559650
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic V()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559648
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic W()LX/9Zu;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559984
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v0

    return-object v0
.end method

.method public final X()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559982
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa:Ljava/lang/String;

    const/16 v1, 0x30

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa:Ljava/lang/String;

    .line 1559983
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559980
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ab:Ljava/lang/String;

    const/16 v1, 0x31

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ab:Ljava/lang/String;

    .line 1559981
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic Z()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559979
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 59

    .prologue
    .line 1559860
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1559861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1559862
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1559863
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1559864
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1559865
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1559866
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1559867
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1559868
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1559869
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1559870
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1559871
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1559872
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1559873
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1559874
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1559875
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1559876
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1559877
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1559878
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->v()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1559879
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1559880
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1559881
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1559882
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1559883
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1559884
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1559885
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1559886
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    .line 1559887
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v28

    .line 1559888
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1559889
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1559890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1559891
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v32

    .line 1559892
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1559893
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1559894
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->K()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 1559895
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1559896
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 1559897
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1559898
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->O()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    .line 1559899
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->P()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 1559900
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1559901
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1559902
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->S()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 1559903
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 1559904
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U()LX/0Px;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v45

    .line 1559905
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1559906
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1559907
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 1559908
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 1559909
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1559910
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1559911
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 1559912
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1559913
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 1559914
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->af()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 1559915
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v56

    .line 1559916
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 1559917
    const/16 v58, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1559918
    const/16 v58, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1559919
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1559920
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1559921
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1559922
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1559923
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1559924
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1559925
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1559926
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1559927
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1559928
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1559929
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1559930
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1559931
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1559932
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1559933
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1559934
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559935
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559936
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559937
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559938
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559939
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559940
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559941
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559942
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559943
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559944
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559945
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559946
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559947
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559948
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559949
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559950
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559951
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559952
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559953
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559954
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559955
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559956
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559957
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559958
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559959
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559960
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559961
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559962
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559963
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559964
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559965
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559966
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559967
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559968
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559969
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559970
    const/16 v2, 0x34

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ae:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1559971
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559972
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559973
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559974
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559975
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559976
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1559977
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1559978
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1559687
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1559688
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1559689
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559690
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1559691
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559692
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559693
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1559694
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559695
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1559696
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559697
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559698
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1559699
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1559700
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1559701
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559702
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1559703
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1559704
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559705
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1559706
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559707
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559708
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1559709
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559710
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1559711
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559712
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559713
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1559714
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    .line 1559715
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1559716
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559717
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    .line 1559718
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1559719
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    .line 1559720
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1559721
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559722
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    .line 1559723
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1559724
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1559725
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1559726
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559727
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1559728
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1559729
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    .line 1559730
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1559731
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559732
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    .line 1559733
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1559734
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    .line 1559735
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1559736
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559737
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    .line 1559738
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1559739
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1559740
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1559741
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559742
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1559743
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1559744
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    .line 1559745
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1559746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559747
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    .line 1559748
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1559749
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1559750
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1559751
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559752
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1559753
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1559754
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    .line 1559755
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1559756
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559757
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    .line 1559758
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1559759
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    .line 1559760
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1559761
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559762
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    .line 1559763
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1559764
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1559765
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 1559766
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559767
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1559768
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1559769
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    .line 1559770
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 1559771
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559772
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    .line 1559773
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1559774
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1559775
    if-eqz v2, :cond_11

    .line 1559776
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559777
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->G:Ljava/util/List;

    move-object v1, v0

    .line 1559778
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1559779
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    .line 1559780
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1559781
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559782
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    .line 1559783
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1559784
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1559785
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1559786
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559787
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1559788
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1559789
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    .line 1559790
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 1559791
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559792
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    .line 1559793
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1559794
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    .line 1559795
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 1559796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559797
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    .line 1559798
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 1559799
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    .line 1559800
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 1559801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559802
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    .line 1559803
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 1559804
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    .line 1559805
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 1559806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559807
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    .line 1559808
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 1559809
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1559810
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 1559811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559812
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1559813
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 1559814
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 1559815
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 1559816
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559817
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 1559818
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 1559819
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1559820
    if-eqz v2, :cond_1a

    .line 1559821
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559822
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X:Ljava/util/List;

    move-object v1, v0

    .line 1559823
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 1559824
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    .line 1559825
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 1559826
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559827
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    .line 1559828
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 1559829
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 1559830
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 1559831
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559832
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Z:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 1559833
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 1559834
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1559835
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 1559836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559837
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1559838
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 1559839
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    .line 1559840
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 1559841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559842
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    .line 1559843
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 1559844
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    .line 1559845
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 1559846
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559847
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    .line 1559848
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 1559849
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    .line 1559850
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 1559851
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559852
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    .line 1559853
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 1559854
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    .line 1559855
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 1559856
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1559857
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    .line 1559858
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1559859
    if-nez v1, :cond_22

    :goto_0
    return-object p0

    :cond_22
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559684
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1559685
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1559686
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1559679
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1559680
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k:Z

    .line 1559681
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->p:Z

    .line 1559682
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ae:Z

    .line 1559683
    return-void
.end method

.method public final aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559677
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    .line 1559678
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    return-object v0
.end method

.method public final aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559675
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1559676
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method public final aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559673
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    .line 1559674
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    return-object v0
.end method

.method public final aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559985
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    .line 1559986
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    return-object v0
.end method

.method public final aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559671
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    .line 1559672
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    return-object v0
.end method

.method public final aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559669
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    .line 1559670
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    return-object v0
.end method

.method public final aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559667
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1559668
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    return-object v0
.end method

.method public final aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559665
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 1559666
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    return-object v0
.end method

.method public final aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559663
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    .line 1559664
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    return-object v0
.end method

.method public final aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559500
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Z:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Z:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 1559501
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Z:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    return-object v0
.end method

.method public final aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559661
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1559662
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    return-object v0
.end method

.method public final aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559659
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    .line 1559660
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    return-object v0
.end method

.method public final aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559657
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    .line 1559658
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    return-object v0
.end method

.method public final aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559655
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    .line 1559656
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    return-object v0
.end method

.method public final aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559653
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    .line 1559654
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    return-object v0
.end method

.method public final synthetic aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559558
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final ab()Z
    .locals 2

    .prologue
    .line 1559651
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1559652
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ae:Z

    return v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559497
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->af:Ljava/lang/String;

    const/16 v1, 0x35

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->af:Ljava/lang/String;

    .line 1559498
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->af:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic ad()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559496
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ae()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559495
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v0

    return-object v0
.end method

.method public final af()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559493
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai:Ljava/lang/String;

    const/16 v1, 0x38

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai:Ljava/lang/String;

    .line 1559494
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai:Ljava/lang/String;

    return-object v0
.end method

.method public final ag()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559491
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj:Ljava/lang/String;

    const/16 v1, 0x39

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj:Ljava/lang/String;

    .line 1559492
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559490
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final ai()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1559488
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->G:Ljava/util/List;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->G:Ljava/util/List;

    .line 1559489
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->G:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559486
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559487
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559484
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559485
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559469
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1559470
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    return-object v0
.end method

.method public final am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559481
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559482
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559479
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559480
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559477
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    .line 1559478
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->l:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    return-object v0
.end method

.method public final ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559455
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    .line 1559456
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    return-object v0
.end method

.method public final aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559457
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1559458
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    return-object v0
.end method

.method public final ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559459
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    .line 1559460
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    return-object v0
.end method

.method public final as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559461
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    .line 1559462
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    return-object v0
.end method

.method public final at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559463
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1559464
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    return-object v0
.end method

.method public final au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559465
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    .line 1559466
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    return-object v0
.end method

.method public final av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559467
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1559468
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    return-object v0
.end method

.method public final aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559453
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    .line 1559454
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    return-object v0
.end method

.method public final ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559471
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    .line 1559472
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    return-object v0
.end method

.method public final ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559473
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1559474
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    return-object v0
.end method

.method public final az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559475
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    .line 1559476
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    return-object v0
.end method

.method public final synthetic b()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559519
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1559534
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;-><init>()V

    .line 1559535
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1559536
    return-object v0
.end method

.method public final synthetic c()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559533
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()LX/5sY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559483
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1559532
    const v0, -0x102cf1ec

    return v0
.end method

.method public final synthetic e()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559531
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1559530
    const v0, 0x4735e982

    return v0
.end method

.method public final hh_()Z
    .locals 2

    .prologue
    .line 1559528
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1559529
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k:Z

    return v0
.end method

.method public final synthetic hi_()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559527
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559526
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559524
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 1559525
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559523
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559522
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1559520
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1559521
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->p:Z

    return v0
.end method

.method public final synthetic o()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559499
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559518
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559516
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s:Ljava/lang/String;

    .line 1559517
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559514
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->t:Ljava/lang/String;

    .line 1559515
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559513
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559511
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->v:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->v:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    .line 1559512
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->v:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559509
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->w:Ljava/lang/String;

    .line 1559510
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559507
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->x:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->x:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 1559508
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->x:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559506
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559505
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559504
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1559502
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->B:Ljava/lang/String;

    .line 1559503
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->B:Ljava/lang/String;

    return-object v0
.end method
