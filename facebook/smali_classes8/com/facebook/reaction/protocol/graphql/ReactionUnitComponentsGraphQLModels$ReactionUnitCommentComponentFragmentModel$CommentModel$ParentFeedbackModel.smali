.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x13febfa7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1571867
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1571866
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1571864
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1571865
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1571825
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1571826
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1571827
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;
    .locals 8

    .prologue
    .line 1571844
    if-nez p0, :cond_0

    .line 1571845
    const/4 p0, 0x0

    .line 1571846
    :goto_0
    return-object p0

    .line 1571847
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    if-eqz v0, :cond_1

    .line 1571848
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    goto :goto_0

    .line 1571849
    :cond_1
    new-instance v0, LX/9vQ;

    invoke-direct {v0}, LX/9vQ;-><init>()V

    .line 1571850
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9vQ;->a:Ljava/lang/String;

    .line 1571851
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1571852
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1571853
    iget-object v3, v0, LX/9vQ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1571854
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1571855
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1571856
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1571857
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1571858
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1571859
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1571860
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1571861
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;-><init>(LX/15i;)V

    .line 1571862
    move-object p0, v3

    .line 1571863
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1571838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1571839
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1571840
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1571841
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1571842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1571843
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1571835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1571836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1571837
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1571868
    new-instance v0, LX/9vR;

    invoke-direct {v0, p1}, LX/9vR;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571834
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1571832
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1571833
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1571831
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1571828
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;-><init>()V

    .line 1571829
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1571830
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571823
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->e:Ljava/lang/String;

    .line 1571824
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1571822
    const v0, -0x41fff6eb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1571821
    const v0, -0x78fb05b

    return v0
.end method
