.class public final Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1550501
    const-class v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1550502
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1550503
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1550504
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1550505
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1550506
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1550507
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1550508
    if-eqz v2, :cond_0

    .line 1550509
    const-string p0, "action"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1550510
    invoke-static {v1, v2, p1, p2}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1550511
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1550512
    if-eqz v2, :cond_1

    .line 1550513
    const-string p0, "badgable_profiles"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1550514
    invoke-static {v1, v2, p1, p2}, LX/9rH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1550515
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1550516
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1550517
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
