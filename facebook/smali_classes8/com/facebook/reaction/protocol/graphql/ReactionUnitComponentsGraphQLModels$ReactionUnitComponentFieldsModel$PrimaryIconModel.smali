.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5d133fa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1574768
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1574767
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1574765
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1574766
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1574762
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1574763
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1574764
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;
    .locals 9

    .prologue
    .line 1574728
    if-nez p0, :cond_0

    .line 1574729
    const/4 p0, 0x0

    .line 1574730
    :goto_0
    return-object p0

    .line 1574731
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    if-eqz v0, :cond_1

    .line 1574732
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    goto :goto_0

    .line 1574733
    :cond_1
    new-instance v0, LX/9vv;

    invoke-direct {v0}, LX/9vv;-><init>()V

    .line 1574734
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;->a(LX/5sY;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9vv;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1574735
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->b()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v1

    iput-object v1, v0, LX/9vv;->b:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    .line 1574736
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1574737
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1574738
    iget-object v3, v0, LX/9vv;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1574739
    iget-object v5, v0, LX/9vv;->b:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1574740
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1574741
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1574742
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1574743
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1574744
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1574745
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1574746
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1574747
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1574748
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;-><init>(LX/15i;)V

    .line 1574749
    move-object p0, v3

    .line 1574750
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574760
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1574761
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1574769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1574770
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1574771
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->b()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1574772
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1574773
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1574774
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1574775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1574776
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1574752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1574753
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1574754
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1574755
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1574756
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    .line 1574757
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1574758
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1574759
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/5sY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574751
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574726
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    .line 1574727
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->f:Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1574723
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;-><init>()V

    .line 1574724
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1574725
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1574722
    const v0, -0x464f0c20

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1574721
    const v0, 0x63601ac8    # 4.1340005E21f

    return v0
.end method
