.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x70643303
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1571943
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1571944
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1571945
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1571946
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1571947
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1571948
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1571949
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;
    .locals 4

    .prologue
    .line 1571950
    if-nez p0, :cond_0

    .line 1571951
    const/4 p0, 0x0

    .line 1571952
    :goto_0
    return-object p0

    .line 1571953
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    if-eqz v0, :cond_1

    .line 1571954
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    goto :goto_0

    .line 1571955
    :cond_1
    new-instance v2, LX/9vL;

    invoke-direct {v2}, LX/9vL;-><init>()V

    .line 1571956
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1571957
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1571958
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1571959
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1571960
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9vL;->a:LX/0Px;

    .line 1571961
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v0

    iput-object v0, v2, LX/9vL;->b:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    .line 1571962
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->c()LX/3Ab;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a(LX/3Ab;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    iput-object v0, v2, LX/9vL;->c:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1571963
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->d()J

    move-result-wide v0

    iput-wide v0, v2, LX/9vL;->d:J

    .line 1571964
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->e()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v0

    iput-object v0, v2, LX/9vL;->e:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    .line 1571965
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hn_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v0

    iput-object v0, v2, LX/9vL;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    .line 1571966
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hm_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    iput-object v0, v2, LX/9vL;->g:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    .line 1571967
    invoke-virtual {v2}, LX/9vL;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571968
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    .line 1571969
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571970
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1571971
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571972
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    .line 1571973
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    return-object v0
.end method

.method private m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571974
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    .line 1571975
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    return-object v0
.end method

.method private n()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571941
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    .line 1571942
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1571909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1571910
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1571911
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1571912
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1571913
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1571914
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1571915
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->n()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1571916
    const/4 v3, 0x7

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1571917
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1571918
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1571919
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1571920
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->h:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1571921
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1571922
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1571923
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1571924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1571925
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1571976
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->e:Ljava/util/List;

    .line 1571977
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1571876
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1571877
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1571878
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1571879
    if-eqz v1, :cond_6

    .line 1571880
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 1571881
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1571882
    :goto_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1571883
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    .line 1571884
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1571885
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 1571886
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    .line 1571887
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1571888
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1571889
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1571890
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 1571891
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1571892
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1571893
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    .line 1571894
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1571895
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 1571896
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    .line 1571897
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1571898
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    .line 1571899
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1571900
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 1571901
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    .line 1571902
    :cond_3
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->n()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1571903
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->n()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    .line 1571904
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->n()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1571905
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    .line 1571906
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    .line 1571907
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1571908
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1571926
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1571927
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->h:J

    .line 1571928
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1571929
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;-><init>()V

    .line 1571930
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1571931
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571932
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/3Ab;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571933
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1571934
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1571935
    iget-wide v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->h:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1571936
    const v0, -0x7c7b827e

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571937
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1571938
    const v0, -0x642179c1

    return v0
.end method

.method public final synthetic hm_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571939
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->n()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic hn_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1571940
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v0

    return-object v0
.end method
