.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9rT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6debd0b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556729
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556728
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1556726
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556727
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556724
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    .line 1556725
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    return-object v0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556722
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1556723
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1556714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556715
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1556716
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1556717
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1556718
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1556719
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1556720
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556721
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1556701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556702
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1556703
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    .line 1556704
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1556705
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;

    .line 1556706
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$PageModel;

    .line 1556707
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1556708
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1556709
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1556710
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;

    .line 1556711
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1556712
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556713
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1556698
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel;-><init>()V

    .line 1556699
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556700
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1556696
    const v0, -0x5d0c438e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1556697
    const v0, 0x1d406922

    return v0
.end method
