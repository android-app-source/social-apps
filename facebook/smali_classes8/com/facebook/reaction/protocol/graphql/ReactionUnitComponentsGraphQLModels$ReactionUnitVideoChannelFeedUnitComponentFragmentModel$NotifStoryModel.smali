.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x23a96375
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1586274
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1586275
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1586276
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1586277
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1586302
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1586303
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1586304
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;
    .locals 2

    .prologue
    .line 1586278
    if-nez p0, :cond_0

    .line 1586279
    const/4 p0, 0x0

    .line 1586280
    :goto_0
    return-object p0

    .line 1586281
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    if-eqz v0, :cond_1

    .line 1586282
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    goto :goto_0

    .line 1586283
    :cond_1
    new-instance v0, LX/9wU;

    invoke-direct {v0}, LX/9wU;-><init>()V

    .line 1586284
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9wU;->a:Ljava/lang/String;

    .line 1586285
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    iput-object v1, v0, LX/9wU;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1586286
    invoke-virtual {v0}, LX/9wU;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V
    .locals 4

    .prologue
    .line 1586287
    iput-object p1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1586288
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1586289
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1586290
    if-eqz v0, :cond_0

    .line 1586291
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1586292
    :cond_0
    return-void

    .line 1586293
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1586294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1586295
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1586296
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1586297
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1586298
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1586299
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1586300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1586301
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1586270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1586271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1586272
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1586273
    new-instance v0, LX/9wV;

    invoke-direct {v0, p1}, LX/9wV;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1586269
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1586263
    const-string v0, "seen_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1586264
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1586265
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1586266
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1586267
    :goto_0
    return-void

    .line 1586268
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1586260
    const-string v0, "seen_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1586261
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-direct {p0, p2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    .line 1586262
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1586257
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;-><init>()V

    .line 1586258
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1586259
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1586255
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->e:Ljava/lang/String;

    .line 1586256
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1586253
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1586254
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1586252
    const v0, 0x282ca595

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1586251
    const v0, 0x4c808d5

    return v0
.end method
