.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9ub;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4d72375b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1595820
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1595819
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1595817
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1595818
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595815
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595816
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1595821
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1595822
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1595823
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1595824
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1595825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1595826
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1595802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1595803
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1595804
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595805
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1595806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;

    .line 1595807
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595808
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1595809
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1595812
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel;-><init>()V

    .line 1595813
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1595814
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1595811
    const v0, -0x866ab16

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1595810
    const v0, 0x7a0aa3c9

    return v0
.end method
