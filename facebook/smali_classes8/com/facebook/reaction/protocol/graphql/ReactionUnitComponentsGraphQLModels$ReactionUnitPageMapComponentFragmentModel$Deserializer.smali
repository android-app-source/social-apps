.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1581962
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1581963
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1581964
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1581965
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1581966
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1581967
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 1581968
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1581969
    :goto_0
    move v1, v2

    .line 1581970
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1581971
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1581972
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;-><init>()V

    .line 1581973
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1581974
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1581975
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1581976
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1581977
    :cond_0
    return-object v1

    .line 1581978
    :cond_1
    const-string p0, "is_self_location"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1581979
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v3

    .line 1581980
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_8

    .line 1581981
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1581982
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1581983
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1581984
    const-string p0, "action"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1581985
    invoke-static {p1, v0}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1581986
    :cond_3
    const-string p0, "locations"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1581987
    invoke-static {p1, v0}, LX/9xt;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1581988
    :cond_4
    const-string p0, "map_bounding_box"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1581989
    invoke-static {p1, v0}, LX/5tJ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1581990
    :cond_5
    const-string p0, "place_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1581991
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 1581992
    :cond_6
    const-string p0, "zoom_level"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1581993
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 1581994
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1581995
    :cond_8
    const/4 v11, 0x6

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1581996
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1581997
    if-eqz v4, :cond_9

    .line 1581998
    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1581999
    :cond_9
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1582000
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1582001
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1582002
    if-eqz v1, :cond_a

    .line 1582003
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5, v2}, LX/186;->a(III)V

    .line 1582004
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
