.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1581214
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1581215
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1581217
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1581218
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1581219
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1581220
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1581221
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1581222
    if-eqz v2, :cond_1

    .line 1581223
    const-string v3, "breadcrumbs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1581224
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1581225
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1581226
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/9xk;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1581227
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1581228
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1581229
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1581230
    if-eqz v2, :cond_2

    .line 1581231
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1581232
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1581233
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1581234
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1581216
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitMessageAndBreadcrumbsComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
