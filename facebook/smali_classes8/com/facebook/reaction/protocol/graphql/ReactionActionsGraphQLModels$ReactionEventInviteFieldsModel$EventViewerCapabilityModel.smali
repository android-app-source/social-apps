.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5881875c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1555472
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1555471
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1555469
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1555470
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1555466
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1555467
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1555468
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;
    .locals 8

    .prologue
    .line 1555447
    if-nez p0, :cond_0

    .line 1555448
    const/4 p0, 0x0

    .line 1555449
    :goto_0
    return-object p0

    .line 1555450
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;

    if-eqz v0, :cond_1

    .line 1555451
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;

    goto :goto_0

    .line 1555452
    :cond_1
    new-instance v0, LX/9sG;

    invoke-direct {v0}, LX/9sG;-><init>()V

    .line 1555453
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;->a()Z

    move-result v1

    iput-boolean v1, v0, LX/9sG;->a:Z

    .line 1555454
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1555455
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1555456
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1555457
    iget-boolean v3, v0, LX/9sG;->a:Z

    invoke-virtual {v2, v5, v3}, LX/186;->a(IZ)V

    .line 1555458
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1555459
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1555460
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1555461
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1555462
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1555463
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;-><init>(LX/15i;)V

    .line 1555464
    move-object p0, v3

    .line 1555465
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1555473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1555474
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1555475
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1555476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1555477
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1555444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1555445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1555446
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1555441
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1555442
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;->e:Z

    .line 1555443
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1555439
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1555440
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1555436
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel$EventViewerCapabilityModel;-><init>()V

    .line 1555437
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1555438
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1555435
    const v0, 0x3e2f6631

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1555434
    const v0, -0x71db289c

    return v0
.end method
