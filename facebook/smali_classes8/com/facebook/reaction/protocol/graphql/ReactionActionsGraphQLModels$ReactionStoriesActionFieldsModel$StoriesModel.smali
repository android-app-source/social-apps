.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6a3c293d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1558549
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1558550
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1558547
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1558548
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1558544
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1558545
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1558546
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;
    .locals 8

    .prologue
    .line 1558524
    if-nez p0, :cond_0

    .line 1558525
    const/4 p0, 0x0

    .line 1558526
    :goto_0
    return-object p0

    .line 1558527
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;

    if-eqz v0, :cond_1

    .line 1558528
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;

    goto :goto_0

    .line 1558529
    :cond_1
    new-instance v0, LX/9so;

    invoke-direct {v0}, LX/9so;-><init>()V

    .line 1558530
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9so;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    .line 1558531
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1558532
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1558533
    iget-object v3, v0, LX/9so;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1558534
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1558535
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1558536
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1558537
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1558538
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1558539
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1558540
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1558541
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;-><init>(LX/15i;)V

    .line 1558542
    move-object p0, v3

    .line 1558543
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1558522
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    .line 1558523
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1558516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1558517
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1558518
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1558519
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1558520
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1558521
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1558551
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1558552
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1558553
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    .line 1558554
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1558555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;

    .line 1558556
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    .line 1558557
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1558558
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1558515
    new-instance v0, LX/9sp;

    invoke-direct {v0, p1}, LX/9sp;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1558514
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1558512
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1558513
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1558511
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1558508
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoriesActionFieldsModel$StoriesModel;-><init>()V

    .line 1558509
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1558510
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1558506
    const v0, 0x384be7e9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1558507
    const v0, 0x4c808d5

    return v0
.end method
