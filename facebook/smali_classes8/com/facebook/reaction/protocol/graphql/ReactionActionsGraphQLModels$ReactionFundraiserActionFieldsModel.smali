.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9rR;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x99855e6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556281
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556282
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1556283
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556284
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556285
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1556286
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556287
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->f:Ljava/lang/String;

    .line 1556288
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1556289
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556290
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1556291
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1556292
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1556293
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1556294
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1556295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556296
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1556297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556298
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1556299
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1556300
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1556301
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;

    .line 1556302
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1556303
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556304
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1556305
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel;-><init>()V

    .line 1556306
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556307
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1556308
    const v0, -0x568e520d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1556309
    const v0, -0x5a6ee876

    return v0
.end method
