.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9rP;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x246b59fe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1555396
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1555395
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1555393
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1555394
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555391
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1555392
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    return-object v0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555389
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1555390
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555387
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1555388
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555397
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1555398
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555385
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->i:Ljava/lang/String;

    .line 1555386
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1555371
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1555372
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1555373
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1555374
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1555375
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1555376
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1555377
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1555378
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1555379
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1555380
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1555381
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1555382
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1555383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1555384
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1555348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1555349
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1555350
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1555351
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1555352
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;

    .line 1555353
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1555354
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1555355
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1555356
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1555357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;

    .line 1555358
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1555359
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1555360
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1555361
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1555362
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;

    .line 1555363
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1555364
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1555365
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1555366
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1555367
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;

    .line 1555368
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1555369
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1555370
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1555345
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel;-><init>()V

    .line 1555346
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1555347
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1555344
    const v0, -0x454d6796

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1555343
    const v0, 0x66c6aa1f

    return v0
.end method
