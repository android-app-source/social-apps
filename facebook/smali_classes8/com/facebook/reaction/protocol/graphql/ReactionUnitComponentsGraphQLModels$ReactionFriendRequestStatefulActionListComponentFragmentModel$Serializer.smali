.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1567583
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1567584
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1567582
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1567548
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1567549
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    .line 1567550
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1567551
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1567552
    if-eqz v2, :cond_0

    .line 1567553
    const-string v3, "affirmative_button_icon"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567554
    invoke-static {v1, v2, p1}, LX/5tK;->a(LX/15i;ILX/0nX;)V

    .line 1567555
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1567556
    if-eqz v2, :cond_1

    .line 1567557
    const-string v3, "affirmative_button_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567558
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1567559
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1567560
    if-eqz v2, :cond_2

    .line 1567561
    const-string v3, "affirmative_result_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567562
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1567563
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1567564
    if-eqz v2, :cond_3

    .line 1567565
    const-string v3, "negative_button_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567566
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1567567
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1567568
    if-eqz v2, :cond_4

    .line 1567569
    const-string v3, "negative_result_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567570
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1567571
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1567572
    if-eqz v2, :cond_5

    .line 1567573
    const-string v2, "reaction_friend_request_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567574
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1567575
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1567576
    if-eqz v2, :cond_6

    .line 1567577
    const-string v3, "requester"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1567578
    invoke-static {v1, v2, p1}, LX/9wb;->a(LX/15i;ILX/0nX;)V

    .line 1567579
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1567580
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1567581
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
