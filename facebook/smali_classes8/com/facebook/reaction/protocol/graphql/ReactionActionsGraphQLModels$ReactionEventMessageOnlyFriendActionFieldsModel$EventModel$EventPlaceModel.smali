.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x145cdd32
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1555773
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1555772
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1555770
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1555771
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1555767
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1555768
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1555769
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;
    .locals 9

    .prologue
    .line 1555744
    if-nez p0, :cond_0

    .line 1555745
    const/4 p0, 0x0

    .line 1555746
    :goto_0
    return-object p0

    .line 1555747
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    if-eqz v0, :cond_1

    .line 1555748
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    goto :goto_0

    .line 1555749
    :cond_1
    new-instance v0, LX/9sK;

    invoke-direct {v0}, LX/9sK;-><init>()V

    .line 1555750
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9sK;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1555751
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sK;->b:Ljava/lang/String;

    .line 1555752
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1555753
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1555754
    iget-object v3, v0, LX/9sK;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1555755
    iget-object v5, v0, LX/9sK;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1555756
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1555757
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1555758
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1555759
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1555760
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1555761
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1555762
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1555763
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1555764
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;-><init>(LX/15i;)V

    .line 1555765
    move-object p0, v3

    .line 1555766
    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1555738
    iput-object p1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->f:Ljava/lang/String;

    .line 1555739
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1555740
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1555741
    if-eqz v0, :cond_0

    .line 1555742
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1555743
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1555730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1555731
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1555732
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1555733
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1555734
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1555735
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1555736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1555737
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1555727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1555728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1555729
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1555707
    new-instance v0, LX/9sL;

    invoke-direct {v0, p1}, LX/9sL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555724
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1555725
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1555726
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1555718
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1555719
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1555720
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1555721
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1555722
    :goto_0
    return-void

    .line 1555723
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1555715
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1555716
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->a(Ljava/lang/String;)V

    .line 1555717
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1555712
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;-><init>()V

    .line 1555713
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1555714
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1555710
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->f:Ljava/lang/String;

    .line 1555711
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1555709
    const v0, 0x691e2b0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1555708
    const v0, 0x499e8e7

    return v0
.end method
