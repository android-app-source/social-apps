.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1560161
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1560162
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1560160
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1560163
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1560164
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1560165
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1560166
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1560167
    if-eqz v2, :cond_0

    .line 1560168
    const-string p0, "comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1560169
    invoke-static {v1, v2, p1, p2}, LX/9tl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1560170
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1560171
    if-eqz v2, :cond_1

    .line 1560172
    const-string p0, "story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1560173
    invoke-static {v1, v2, p1}, LX/9tm;->a(LX/15i;ILX/0nX;)V

    .line 1560174
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1560175
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1560159
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
