.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1yA;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y9;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf979a8b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1574156
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1574157
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1574158
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1574159
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1574160
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1574161
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1574162
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1574163
    if-nez p0, :cond_0

    .line 1574164
    const/4 p0, 0x0

    .line 1574165
    :goto_0
    return-object p0

    .line 1574166
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    if-eqz v0, :cond_1

    .line 1574167
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    goto :goto_0

    .line 1574168
    :cond_1
    new-instance v2, LX/9vn;

    invoke-direct {v2}, LX/9vn;-><init>()V

    .line 1574169
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1574170
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v0, v1

    .line 1574171
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1574172
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1574173
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1574174
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->b:LX/0Px;

    .line 1574175
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->c:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1574176
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;)Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1574177
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->e:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1574178
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->f:Ljava/lang/String;

    .line 1574179
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->r()Z

    move-result v0

    iput-boolean v0, v2, LX/9vn;->g:Z

    .line 1574180
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->k()Z

    move-result v0

    iput-boolean v0, v2, LX/9vn;->h:Z

    .line 1574181
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->l()Z

    move-result v0

    iput-boolean v0, v2, LX/9vn;->i:Z

    .line 1574182
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->v_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->j:Ljava/lang/String;

    .line 1574183
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->u()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    .line 1574184
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->l:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1574185
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1574186
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1574187
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-static {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1574188
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1574189
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->m:LX/0Px;

    .line 1574190
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->n:Ljava/lang/String;

    .line 1574191
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9vn;->o:Ljava/lang/String;

    .line 1574192
    invoke-virtual {v2}, LX/9vn;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574193
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1574194
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    return-object v0
.end method

.method private x()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574195
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1574196
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    return-object v0
.end method

.method private y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574197
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    .line 1574198
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    return-object v0
.end method

.method private z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574230
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1574231
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 1574199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1574200
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1574201
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1574202
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1574203
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->x()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1574204
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1574205
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1574206
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->v_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1574207
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1574208
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1574209
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 1574210
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1574211
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1574212
    const/16 v12, 0xf

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 1574213
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 1574214
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1574215
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1574216
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1574217
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1574218
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1574219
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1574220
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1574221
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1574222
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1574223
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1574224
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1574225
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1574226
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1574227
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1574228
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1574229
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1574110
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1574111
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1574112
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1574113
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1574114
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    .line 1574115
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1574116
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->x()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1574117
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->x()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1574118
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->x()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1574119
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    .line 1574120
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1574121
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1574122
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    .line 1574123
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1574124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    .line 1574125
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    .line 1574126
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1574127
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1574128
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1574129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    .line 1574130
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1574131
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1574132
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1574133
    if-eqz v2, :cond_4

    .line 1574134
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    .line 1574135
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->q:Ljava/util/List;

    move-object v1, v0

    .line 1574136
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1574137
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1574245
    new-instance v0, LX/9vo;

    invoke-direct {v0, p1}, LX/9vo;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574244
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1574239
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1574240
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->k:Z

    .line 1574241
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->l:Z

    .line 1574242
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->m:Z

    .line 1574243
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1574246
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1574247
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1574238
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574235
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1574236
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1574237
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1574232
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;-><init>()V

    .line 1574233
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1574234
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1574152
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->f:Ljava/util/List;

    .line 1574153
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574154
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1574155
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1574098
    const v0, 0x5dffbb52

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574099
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    .line 1574100
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1574101
    const v0, 0x7c02d003

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574102
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    .line 1574103
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1574104
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1574105
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->l:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1574106
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1574107
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->m:Z

    return v0
.end method

.method public final synthetic m()LX/3Bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574108
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()LX/1yP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574109
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1574138
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->q:Ljava/util/List;

    .line 1574139
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic p()LX/6R4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574140
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574141
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->x()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1574142
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1574143
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->k:Z

    return v0
.end method

.method public final synthetic s()LX/1yO;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574144
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574145
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574146
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->y()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574147
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->z()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574148
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    .line 1574149
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1574150
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    .line 1574151
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    return-object v0
.end method
