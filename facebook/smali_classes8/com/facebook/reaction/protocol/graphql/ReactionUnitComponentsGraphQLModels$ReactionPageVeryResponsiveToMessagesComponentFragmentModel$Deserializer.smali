.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1569420
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1569421
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1569422
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1569423
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1569424
    const/4 v2, 0x0

    .line 1569425
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1569426
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1569427
    :goto_0
    move v1, v2

    .line 1569428
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1569429
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1569430
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel;-><init>()V

    .line 1569431
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1569432
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1569433
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1569434
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1569435
    :cond_0
    return-object v1

    .line 1569436
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1569437
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_7

    .line 1569438
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1569439
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1569440
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 1569441
    const-string p0, "action"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1569442
    invoke-static {p1, v0}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1569443
    :cond_3
    const-string p0, "badge_icon"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1569444
    invoke-static {p1, v0}, LX/9wq;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1569445
    :cond_4
    const-string p0, "response_delay"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1569446
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1569447
    :cond_5
    const-string p0, "responsive_badge"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1569448
    invoke-static {p1, v0}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1569449
    :cond_6
    const-string p0, "responsive_badge_color"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1569450
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1569451
    :cond_7
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1569452
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1569453
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1569454
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1569455
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1569456
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1569457
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_1
.end method
