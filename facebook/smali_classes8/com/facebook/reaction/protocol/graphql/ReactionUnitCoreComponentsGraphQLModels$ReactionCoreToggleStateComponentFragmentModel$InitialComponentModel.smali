.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9oK;
.implements LX/9uY;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x77beb441
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1595734
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1595698
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1595699
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1595700
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1595701
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1595702
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1595703
    return-void
.end method

.method private A()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595704
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595705
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595706
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595707
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private C()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595708
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595709
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    return-object v0
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;
    .locals 2

    .prologue
    .line 1595710
    if-nez p0, :cond_0

    .line 1595711
    const/4 p0, 0x0

    .line 1595712
    :goto_0
    return-object p0

    .line 1595713
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    if-eqz v0, :cond_1

    .line 1595714
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    goto :goto_0

    .line 1595715
    :cond_1
    new-instance v0, LX/9yC;

    invoke-direct {v0}, LX/9yC;-><init>()V

    .line 1595716
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1595717
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1595718
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->c:Ljava/lang/String;

    .line 1595719
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 1595720
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->e:Ljava/lang/String;

    .line 1595721
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->f:Ljava/lang/String;

    .line 1595722
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->o()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->g:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    .line 1595723
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->h:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    .line 1595724
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->p()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->i:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    .line 1595725
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->q()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->j:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    .line 1595726
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->r()LX/5sY;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;->a(LX/5sY;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1595727
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->b()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595728
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->c()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->m:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595729
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->d()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595730
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->e()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->o:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595731
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->hk_()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595732
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->hj_()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9yC;->q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595733
    invoke-virtual {v0}, LX/9yC;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595636
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1595637
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    return-object v0
.end method

.method private t()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595773
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->k:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->k:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    .line 1595774
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->k:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    return-object v0
.end method

.method private u()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595787
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    .line 1595788
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    return-object v0
.end method

.method private v()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595785
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->n:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->n:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    .line 1595786
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->n:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    return-object v0
.end method

.method private w()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595783
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->o:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->o:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1595784
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->o:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595781
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595782
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595779
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595780
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595777
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595778
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595775
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->l:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->l:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    .line 1595776
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->l:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 1595735
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1595736
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1595737
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1595738
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1595739
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1595740
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1595741
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->n()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1595742
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1595743
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1595744
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1595745
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->v()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1595746
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->w()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1595747
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->x()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1595748
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1595749
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->z()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1595750
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->A()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1595751
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->B()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1595752
    invoke-direct/range {p0 .. p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->C()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1595753
    const/16 v19, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1595754
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1595755
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1595756
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1595757
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1595758
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1595759
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1595760
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1595761
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1595762
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1595763
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1595764
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1595765
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1595766
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1595767
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1595768
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1595769
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1595770
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1595771
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1595772
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1595638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1595639
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1595640
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1595641
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1595642
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595643
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1595644
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1595645
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    .line 1595646
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1595647
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595648
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->k:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    .line 1595649
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1595650
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    .line 1595651
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1595652
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595653
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    .line 1595654
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->v()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1595655
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->v()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    .line 1595656
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->v()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1595657
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595658
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->n:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    .line 1595659
    :cond_3
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->w()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1595660
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->w()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1595661
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->w()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1595662
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595663
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->o:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1595664
    :cond_4
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->x()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1595665
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->x()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595666
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->x()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1595667
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595668
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595669
    :cond_5
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1595670
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595671
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1595672
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595673
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->q:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595674
    :cond_6
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->z()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1595675
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->z()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595676
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->z()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1595677
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595678
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595679
    :cond_7
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->A()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1595680
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->A()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595681
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->A()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1595682
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595683
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595684
    :cond_8
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->B()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1595685
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->B()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595686
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->B()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1595687
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595688
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1595689
    :cond_9
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->C()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1595690
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->C()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595691
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->C()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1595692
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    .line 1595693
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    .line 1595694
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1595695
    if-nez v1, :cond_b

    :goto_0
    return-object p0

    :cond_b
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595696
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 1595697
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->h:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method public final synthetic b()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595622
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->x()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1595619
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;-><init>()V

    .line 1595620
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1595621
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595618
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595617
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->z()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1595616
    const v0, -0x169afc74

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595615
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->A()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1595614
    const v0, -0x2605f7f0

    return v0
.end method

.method public final synthetic hj_()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595613
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->C()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic hk_()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595611
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->B()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595623
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1595624
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1595625
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595626
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595627
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->g:Ljava/lang/String;

    .line 1595628
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595629
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->i:Ljava/lang/String;

    .line 1595630
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595631
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->j:Ljava/lang/String;

    .line 1595632
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic o()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595612
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->t()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic p()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595633
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->u()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595634
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->v()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()LX/5sY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1595635
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->w()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    return-object v0
.end method
