.class public final Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1547485
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1547486
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1547483
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1547484
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1547470
    if-nez p1, :cond_0

    move v0, v6

    .line 1547471
    :goto_0
    return v0

    .line 1547472
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1547473
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1547474
    :pswitch_0
    invoke-virtual {p0, p1, v6, v6}, LX/15i;->a(III)I

    move-result v7

    .line 1547475
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-virtual {p0, p1, v9, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1547476
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1547477
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1547478
    const/4 v8, 0x3

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1547479
    invoke-virtual {p3, v6, v7, v6}, LX/186;->a(III)V

    .line 1547480
    invoke-virtual {p3, v9, v0}, LX/186;->b(II)V

    move-object v0, p3

    .line 1547481
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1547482
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xbbbd4b6
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1547469
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1547464
    if-eqz p0, :cond_0

    .line 1547465
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1547466
    if-eq v0, p0, :cond_0

    .line 1547467
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1547468
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1547459
    packed-switch p2, :pswitch_data_0

    .line 1547460
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1547461
    :pswitch_0
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1547462
    invoke-static {v0, p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1547463
    return-void

    :pswitch_data_0
    .packed-switch -0xbbbd4b6
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1547458
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1547425
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1547426
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1547453
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1547454
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1547455
    :cond_0
    iput-object p1, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1547456
    iput p2, p0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->b:I

    .line 1547457
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1547452
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1547451
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1547448
    iget v0, p0, LX/1vt;->c:I

    .line 1547449
    move v0, v0

    .line 1547450
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1547445
    iget v0, p0, LX/1vt;->c:I

    .line 1547446
    move v0, v0

    .line 1547447
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1547442
    iget v0, p0, LX/1vt;->b:I

    .line 1547443
    move v0, v0

    .line 1547444
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1547439
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1547440
    move-object v0, v0

    .line 1547441
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1547430
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1547431
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1547432
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1547433
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1547434
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1547435
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1547436
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1547437
    invoke-static {v3, v9, v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1547438
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1547427
    iget v0, p0, LX/1vt;->c:I

    .line 1547428
    move v0, v0

    .line 1547429
    return v0
.end method
