.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1567659
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1567660
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1567661
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1567662
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1567663
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1567664
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1567665
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1567666
    :goto_0
    move v1, v2

    .line 1567667
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1567668
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1567669
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel;-><init>()V

    .line 1567670
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1567671
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1567672
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1567673
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1567674
    :cond_0
    return-object v1

    .line 1567675
    :cond_1
    const-string p0, "is_verified"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1567676
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    .line 1567677
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_8

    .line 1567678
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1567679
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1567680
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 1567681
    const-string p0, "action"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1567682
    invoke-static {p1, v0}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1567683
    :cond_3
    const-string p0, "header_image100"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1567684
    invoke-static {p1, v0}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1567685
    :cond_4
    const-string p0, "message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1567686
    invoke-static {p1, v0}, LX/8sa;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1567687
    :cond_5
    const-string p0, "sub_message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1567688
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1567689
    :cond_6
    const-string p0, "verification_actor"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1567690
    invoke-static {p1, v0}, LX/9wc;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1567691
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1567692
    :cond_8
    const/4 v10, 0x6

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1567693
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1567694
    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1567695
    if-eqz v1, :cond_9

    .line 1567696
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1567697
    :cond_9
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1567698
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1567699
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1567700
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
