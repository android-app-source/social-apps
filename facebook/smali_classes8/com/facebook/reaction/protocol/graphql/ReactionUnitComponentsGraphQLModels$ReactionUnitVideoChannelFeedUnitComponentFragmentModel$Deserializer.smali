.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1586165
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1586166
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1586167
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1586168
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1586169
    const/4 v2, 0x0

    .line 1586170
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_c

    .line 1586171
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1586172
    :goto_0
    move v1, v2

    .line 1586173
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1586174
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1586175
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel;-><init>()V

    .line 1586176
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1586177
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1586178
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1586179
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1586180
    :cond_0
    return-object v1

    .line 1586181
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1586182
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 1586183
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1586184
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1586185
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1586186
    const-string p0, "action"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1586187
    invoke-static {p1, v0}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1586188
    :cond_3
    const-string p0, "feed_unit"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1586189
    invoke-static {p1, v0}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1586190
    :cond_4
    const-string p0, "head_style"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1586191
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1586192
    :cond_5
    const-string p0, "notif_story"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1586193
    invoke-static {p1, v0}, LX/9y7;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1586194
    :cond_6
    const-string p0, "prune_behavior"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1586195
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 1586196
    :cond_7
    const-string p0, "publisher_action"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1586197
    invoke-static {p1, v0}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1586198
    :cond_8
    const-string p0, "publisher_context"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1586199
    invoke-static {p1, v0}, LX/5tZ;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1586200
    :cond_9
    const-string p0, "recommendation_context"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1586201
    invoke-static {p1, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1586202
    :cond_a
    const-string p0, "video_notification_context"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1586203
    invoke-static {p1, v0}, LX/9y9;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1586204
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1586205
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1586206
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1586207
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1586208
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1586209
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1586210
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1586211
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1586212
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1586213
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1586214
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
