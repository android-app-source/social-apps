.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x544edb68
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1557564
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1557563
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1557561
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1557562
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1557558
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1557559
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1557560
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;
    .locals 9

    .prologue
    .line 1557514
    if-nez p0, :cond_0

    .line 1557515
    const/4 p0, 0x0

    .line 1557516
    :goto_0
    return-object p0

    .line 1557517
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    if-eqz v0, :cond_1

    .line 1557518
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    goto :goto_0

    .line 1557519
    :cond_1
    new-instance v0, LX/9sg;

    invoke-direct {v0}, LX/9sg;-><init>()V

    .line 1557520
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9sg;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1557521
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    iput-object v1, v0, LX/9sg;->b:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 1557522
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1557523
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1557524
    iget-object v3, v0, LX/9sg;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1557525
    iget-object v5, v0, LX/9sg;->b:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1557526
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1557527
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1557528
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1557529
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1557530
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1557531
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1557532
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1557533
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1557534
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;-><init>(LX/15i;)V

    .line 1557535
    move-object p0, v3

    .line 1557536
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1557550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1557551
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1557552
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1557553
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1557554
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1557555
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1557556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1557557
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1557547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1557548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1557549
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557544
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1557545
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1557546
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1557542
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 1557543
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1557539
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel$ReactionUnitComponentsModel$ActionModel;-><init>()V

    .line 1557540
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1557541
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1557538
    const v0, -0x69688178

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1557537
    const v0, 0x4735e982

    return v0
.end method
