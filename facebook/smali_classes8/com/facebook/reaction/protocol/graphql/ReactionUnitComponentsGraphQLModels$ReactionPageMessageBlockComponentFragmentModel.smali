.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9uD;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x25c0c211
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1569342
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1569341
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1569339
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1569340
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569337
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1569338
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569335
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->f:Ljava/lang/String;

    .line 1569336
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569333
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    .line 1569334
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569331
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1569332
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569329
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569330
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569265
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569266
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1569327
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569328
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1569308
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1569309
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1569310
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1569311
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1569312
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->l()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1569313
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1569314
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1569315
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1569316
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1569317
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1569318
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1569319
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1569320
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1569321
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1569322
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1569323
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1569324
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->l:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1569325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1569326
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1569275
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1569276
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1569277
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1569278
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1569279
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    .line 1569280
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 1569281
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1569282
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    .line 1569283
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1569284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    .line 1569285
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;

    .line 1569286
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->l()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1569287
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->l()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1569288
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->l()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1569289
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    .line 1569290
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1569291
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1569292
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569293
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1569294
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    .line 1569295
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569296
    :cond_3
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1569297
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569298
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1569299
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    .line 1569300
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569301
    :cond_4
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1569302
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569303
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1569304
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    .line 1569305
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1569306
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1569307
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1569272
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1569273
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;->l:J

    .line 1569274
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1569269
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel;-><init>()V

    .line 1569270
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1569271
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1569268
    const v0, 0x267b15c9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1569267
    const v0, -0x43d802c1

    return v0
.end method
