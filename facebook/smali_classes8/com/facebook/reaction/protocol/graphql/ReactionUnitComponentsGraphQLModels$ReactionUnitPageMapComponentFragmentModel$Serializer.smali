.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1582113
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1582114
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1582112
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1582082
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1582083
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x0

    .line 1582084
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1582085
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1582086
    if-eqz v2, :cond_0

    .line 1582087
    const-string v3, "action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1582088
    invoke-static {v1, v2, p1, p2}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1582089
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1582090
    if-eqz v2, :cond_1

    .line 1582091
    const-string v3, "is_self_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1582092
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1582093
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1582094
    if-eqz v2, :cond_2

    .line 1582095
    const-string v3, "locations"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1582096
    invoke-static {v1, v2, p1, p2}, LX/9xt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1582097
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1582098
    if-eqz v2, :cond_3

    .line 1582099
    const-string v3, "map_bounding_box"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1582100
    invoke-static {v1, v2, p1}, LX/5tJ;->a(LX/15i;ILX/0nX;)V

    .line 1582101
    :cond_3
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1582102
    if-eqz v2, :cond_4

    .line 1582103
    const-string v2, "place_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1582104
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1582105
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1582106
    if-eqz v2, :cond_5

    .line 1582107
    const-string v3, "zoom_level"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1582108
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1582109
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1582110
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1582111
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapComponentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
