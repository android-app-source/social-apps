.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x60ce2ccd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556263
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1556262
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1556260
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556261
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1556257
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1556258
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556259
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .locals 12

    .prologue
    .line 1556179
    if-nez p0, :cond_0

    .line 1556180
    const/4 p0, 0x0

    .line 1556181
    :goto_0
    return-object p0

    .line 1556182
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    if-eqz v0, :cond_1

    .line 1556183
    check-cast p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    goto :goto_0

    .line 1556184
    :cond_1
    new-instance v0, LX/9sP;

    invoke-direct {v0}, LX/9sP;-><init>()V

    .line 1556185
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9sP;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1556186
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sP;->b:Ljava/lang/String;

    .line 1556187
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v1

    iput-object v1, v0, LX/9sP;->c:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    .line 1556188
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sP;->d:Ljava/lang/String;

    .line 1556189
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->hd_()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/9sP;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    .line 1556190
    const/4 v6, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 1556191
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1556192
    iget-object v3, v0, LX/9sP;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1556193
    iget-object v5, v0, LX/9sP;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1556194
    iget-object v7, v0, LX/9sP;->c:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1556195
    iget-object v8, v0, LX/9sP;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1556196
    iget-object v9, v0, LX/9sP;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1556197
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, LX/186;->c(I)V

    .line 1556198
    invoke-virtual {v2, v11, v3}, LX/186;->b(II)V

    .line 1556199
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1556200
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1556201
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1556202
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1556203
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1556204
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1556205
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1556206
    invoke-virtual {v3, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1556207
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1556208
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;-><init>(LX/15i;)V

    .line 1556209
    move-object p0, v3

    .line 1556210
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556255
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    .line 1556256
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556253
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    .line 1556254
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1556239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556240
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1556241
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1556242
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1556243
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1556244
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1556245
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1556246
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1556247
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1556248
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1556249
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1556250
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1556251
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556252
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1556226
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1556227
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1556228
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    .line 1556229
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1556230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1556231
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->g:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    .line 1556232
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1556233
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    .line 1556234
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1556235
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1556236
    iput-object v0, v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->i:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    .line 1556237
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1556238
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556225
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556222
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1556223
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1556224
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1556219
    new-instance v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;-><init>()V

    .line 1556220
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1556221
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556217
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->f:Ljava/lang/String;

    .line 1556218
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556216
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1556215
    const v0, 0x2a7a8445

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556213
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->h:Ljava/lang/String;

    .line 1556214
    iget-object v0, p0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1556212
    const v0, -0x5be312d5

    return v0
.end method

.method public final synthetic hd_()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1556211
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v0

    return-object v0
.end method
