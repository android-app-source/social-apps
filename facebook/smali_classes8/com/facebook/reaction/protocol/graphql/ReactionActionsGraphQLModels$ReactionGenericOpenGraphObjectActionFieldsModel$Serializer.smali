.class public final Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1556422
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1556423
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1556424
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1556425
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1556426
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1556427
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1556428
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1556429
    if-eqz v2, :cond_0

    .line 1556430
    const-string p0, "action_og_object"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1556431
    invoke-static {v1, v2, p1}, LX/9tQ;->a(LX/15i;ILX/0nX;)V

    .line 1556432
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1556433
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1556434
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$Serializer;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
