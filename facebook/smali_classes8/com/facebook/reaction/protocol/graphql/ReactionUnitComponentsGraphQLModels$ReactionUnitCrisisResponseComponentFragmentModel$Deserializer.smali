.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1580458
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1580459
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1580460
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1580461
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1580462
    const/4 v2, 0x0

    .line 1580463
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 1580464
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1580465
    :goto_0
    move v1, v2

    .line 1580466
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1580467
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1580468
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel;-><init>()V

    .line 1580469
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1580470
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1580471
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1580472
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1580473
    :cond_0
    return-object v1

    .line 1580474
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1580475
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_a

    .line 1580476
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1580477
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1580478
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 1580479
    const-string p0, "crisis"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1580480
    invoke-static {p1, v0}, LX/9xd;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1580481
    :cond_3
    const-string p0, "footer_actions"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1580482
    invoke-static {p1, v0}, LX/9tj;->b(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1580483
    :cond_4
    const-string p0, "mark_not_in_crisis_location_message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1580484
    invoke-static {p1, v0}, LX/9xe;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1580485
    :cond_5
    const-string p0, "mark_safe_message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1580486
    invoke-static {p1, v0}, LX/9xf;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1580487
    :cond_6
    const-string p0, "selected_state"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1580488
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1580489
    :cond_7
    const-string p0, "undo_message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1580490
    invoke-static {p1, v0}, LX/9xg;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1580491
    :cond_8
    const-string p0, "user_not_in_crisis_location_message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1580492
    invoke-static {p1, v0}, LX/9xh;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1580493
    :cond_9
    const-string p0, "user_safe_message"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1580494
    invoke-static {p1, v0}, LX/9xi;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1580495
    :cond_a
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1580496
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1580497
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1580498
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1580499
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1580500
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1580501
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1580502
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1580503
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1580504
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
