.class public final Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1569543
    const-class v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1569544
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1569545
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1569546
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1569547
    const/4 v11, 0x0

    .line 1569548
    const/4 v10, 0x0

    .line 1569549
    const/4 v9, 0x0

    .line 1569550
    const/4 v8, 0x0

    .line 1569551
    const/4 v7, 0x0

    .line 1569552
    const/4 v6, 0x0

    .line 1569553
    const/4 v5, 0x0

    .line 1569554
    const/4 v4, 0x0

    .line 1569555
    const/4 v3, 0x0

    .line 1569556
    const/4 v2, 0x0

    .line 1569557
    const/4 v1, 0x0

    .line 1569558
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 1569559
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1569560
    const/4 v1, 0x0

    .line 1569561
    :goto_0
    move v1, v1

    .line 1569562
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1569563
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1569564
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsMetricWithChartComponentFragmentModel;-><init>()V

    .line 1569565
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1569566
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1569567
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1569568
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1569569
    :cond_0
    return-object v1

    .line 1569570
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1569571
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_b

    .line 1569572
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1569573
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1569574
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1569575
    const-string p0, "delta"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1569576
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1569577
    :cond_3
    const-string p0, "delta_tooltip"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1569578
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1569579
    :cond_4
    const-string p0, "has_data"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1569580
    const/4 v2, 0x1

    .line 1569581
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1569582
    :cond_5
    const-string p0, "insights_action_type"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1569583
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1569584
    :cond_6
    const-string p0, "is_larger_better"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1569585
    const/4 v1, 0x1

    .line 1569586
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1569587
    :cond_7
    const-string p0, "metric_name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1569588
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1569589
    :cond_8
    const-string p0, "page"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1569590
    invoke-static {p1, v0}, LX/9wr;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1569591
    :cond_9
    const-string p0, "preview_data"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1569592
    invoke-static {p1, v0}, LX/9ws;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1569593
    :cond_a
    const-string p0, "value"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1569594
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1569595
    :cond_b
    const/16 v12, 0x9

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1569596
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1569597
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1569598
    if-eqz v2, :cond_c

    .line 1569599
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 1569600
    :cond_c
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1569601
    if-eqz v1, :cond_d

    .line 1569602
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1569603
    :cond_d
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1569604
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1569605
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1569606
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1569607
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
