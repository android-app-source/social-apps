.class public Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1438230
    new-instance v0, LX/968;

    invoke-direct {v0}, LX/968;-><init>()V

    sput-object v0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1438231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1438232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    .line 1438233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    .line 1438234
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1438235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1438236
    iput-object p1, p0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    .line 1438237
    iput-object p2, p0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    .line 1438238
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1438239
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1438240
    iget-object v0, p0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1438241
    iget-object v0, p0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1438242
    return-void
.end method
