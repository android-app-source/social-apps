.class public final Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7999e5d4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438643
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438619
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1438620
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1438621
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438622
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1438623
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1438624
    :cond_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1438625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438626
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1438627
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1438628
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1438629
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1438630
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1438631
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438632
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1438633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438634
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438635
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438636
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->f:Ljava/lang/String;

    .line 1438637
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1438638
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;-><init>()V

    .line 1438639
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1438640
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1438641
    const v0, -0x35c89971

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1438642
    const v0, -0x4471e47c

    return v0
.end method
