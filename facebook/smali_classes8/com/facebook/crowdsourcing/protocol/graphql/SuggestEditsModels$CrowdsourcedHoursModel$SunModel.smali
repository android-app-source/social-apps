.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4a2dffad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1443018
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1443017
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1443015
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1443016
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1443012
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1443013
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1443014
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;
    .locals 4

    .prologue
    .line 1442983
    if-nez p0, :cond_0

    .line 1442984
    const/4 p0, 0x0

    .line 1442985
    :goto_0
    return-object p0

    .line 1442986
    :cond_0
    instance-of v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;

    if-eqz v0, :cond_1

    .line 1442987
    check-cast p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;

    goto :goto_0

    .line 1442988
    :cond_1
    new-instance v0, LX/97q;

    invoke-direct {v0}, LX/97q;-><init>()V

    .line 1442989
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->a()J

    move-result-wide v2

    iput-wide v2, v0, LX/97q;->a:J

    .line 1442990
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->b()J

    move-result-wide v2

    iput-wide v2, v0, LX/97q;->b:J

    .line 1442991
    invoke-virtual {v0}, LX/97q;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1443006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1443007
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1443008
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1443009
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->f:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1443010
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1443011
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1443019
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1443020
    iget-wide v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1443003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1443004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1443005
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1442999
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1443000
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->e:J

    .line 1443001
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->f:J

    .line 1443002
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1442997
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1442998
    iget-wide v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->f:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1442994
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;-><init>()V

    .line 1442995
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1442996
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1442993
    const v0, -0x4130dcac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1442992
    const v0, 0x78e1ce50

    return v0
.end method
