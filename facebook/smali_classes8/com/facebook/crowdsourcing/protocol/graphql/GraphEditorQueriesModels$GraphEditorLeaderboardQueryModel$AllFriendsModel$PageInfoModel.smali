.class public final Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x778473f6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1439702
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1439705
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1439684
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1439685
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1439703
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;->e:Ljava/lang/String;

    .line 1439704
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1439692
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1439693
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1439694
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1439695
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1439696
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1439697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1439698
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1439699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1439700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1439701
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1439689
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1439690
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;->f:Z

    .line 1439691
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1439686
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel$PageInfoModel;-><init>()V

    .line 1439687
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1439688
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1439683
    const v0, -0x1bfafb41

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1439682
    const v0, 0x370fbffd

    return v0
.end method
