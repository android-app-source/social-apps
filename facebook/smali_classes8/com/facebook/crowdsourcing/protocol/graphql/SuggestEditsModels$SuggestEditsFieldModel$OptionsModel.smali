.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x786ec186
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1443814
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1443813
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1443811
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1443812
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1443808
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1443809
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1443810
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1443784
    if-nez p0, :cond_0

    .line 1443785
    const/4 p0, 0x0

    .line 1443786
    :goto_0
    return-object p0

    .line 1443787
    :cond_0
    instance-of v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    if-eqz v0, :cond_1

    .line 1443788
    check-cast p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    goto :goto_0

    .line 1443789
    :cond_1
    new-instance v2, LX/97x;

    invoke-direct {v2}, LX/97x;-><init>()V

    .line 1443790
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1443791
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1443792
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1443793
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1443794
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/97x;->a:LX/0Px;

    .line 1443795
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1443796
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1443797
    iget-object v5, v2, LX/97x;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1443798
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1443799
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1443800
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1443801
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1443802
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1443803
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1443804
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1443805
    new-instance v5, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    invoke-direct {v5, v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;-><init>(LX/15i;)V

    .line 1443806
    move-object p0, v5

    .line 1443807
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1443815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1443816
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1443817
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1443818
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1443819
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1443820
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1443782
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->e:Ljava/util/List;

    .line 1443783
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1443774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1443775
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1443776
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1443777
    if-eqz v1, :cond_0

    .line 1443778
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    .line 1443779
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->e:Ljava/util/List;

    .line 1443780
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1443781
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1443771
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;-><init>()V

    .line 1443772
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1443773
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1443770
    const v0, -0x3ae9e772

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1443769
    const v0, 0x1b597347

    return v0
.end method
