.class public final Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6580128f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438892
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438891
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1438846
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1438847
    return-void
.end method

.method private j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438889
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    .line 1438890
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    return-object v0
.end method

.method private k()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438887
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    .line 1438888
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    return-object v0
.end method

.method private l()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438885
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->i:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->i:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    .line 1438886
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->i:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1438871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438872
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1438873
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1438874
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->d()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1438875
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1438876
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1438877
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1438878
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1438879
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1438880
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1438881
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1438882
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1438883
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438884
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1438848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438849
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1438850
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    .line 1438851
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1438852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438853
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    .line 1438854
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->d()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1438855
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1438856
    if-eqz v2, :cond_1

    .line 1438857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438858
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 1438859
    :cond_1
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1438860
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    .line 1438861
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1438862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438863
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    .line 1438864
    :cond_2
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1438865
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    .line 1438866
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1438867
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438868
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->i:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    .line 1438869
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438870
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438893
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438835
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1438836
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;-><init>()V

    .line 1438837
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1438838
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438839
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->f:Ljava/lang/String;

    .line 1438840
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1438841
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->g:Ljava/util/List;

    .line 1438842
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1438843
    const v0, -0x5549eabb

    return v0
.end method

.method public final synthetic e()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438844
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1438845
    const v0, 0x626a1068

    return v0
.end method

.method public final synthetic gp_()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438834
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    return-object v0
.end method
