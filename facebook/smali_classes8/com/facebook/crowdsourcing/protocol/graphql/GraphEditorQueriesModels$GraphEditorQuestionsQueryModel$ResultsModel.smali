.class public final Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5832a3e0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1439897
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1439936
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1439934
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1439935
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1439932
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1439933
    iget v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1439923
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1439924
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1439925
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1439926
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1439927
    iget v2, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 1439928
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1439929
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1439930
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1439931
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1439910
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1439911
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1439912
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1439913
    if-eqz v1, :cond_2

    .line 1439914
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    .line 1439915
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1439916
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1439917
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    .line 1439918
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1439919
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    .line 1439920
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    .line 1439921
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1439922
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1439907
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1439908
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->e:I

    .line 1439909
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1439904
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;-><init>()V

    .line 1439905
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1439906
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1439903
    const v0, 0x119dd37b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1439902
    const v0, -0x72e8a5c5

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1439900
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->f:Ljava/util/List;

    .line 1439901
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1439898
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    .line 1439899
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    return-object v0
.end method
