.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7bf86270
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1444976
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1444975
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1444973
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1444974
    return-void
.end method

.method private j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCrowdsourcingSuggestEditsCards"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1444971
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    .line 1444972
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1444969
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1444970
    iget v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->f:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1444962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1444963
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1444964
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1444965
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1444966
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1444967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1444968
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1444954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1444955
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1444956
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    .line 1444957
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1444958
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    .line 1444959
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    .line 1444960
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1444961
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1444953
    new-instance v0, LX/986;

    invoke-direct {v0, p1}, LX/986;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1444950
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1444951
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->f:I

    .line 1444952
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1444948
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1444949
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1444941
    return-void
.end method

.method public final synthetic b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCrowdsourcingSuggestEditsCards"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1444947
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1444944
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;-><init>()V

    .line 1444945
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1444946
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1444943
    const v0, 0x21c3be7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1444942
    const v0, 0x25d6af

    return v0
.end method
