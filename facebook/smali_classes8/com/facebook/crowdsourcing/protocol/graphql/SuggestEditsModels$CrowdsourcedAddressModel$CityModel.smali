.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7a8af513
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1441945
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1441946
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1441947
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1441948
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1441980
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1441981
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1441982
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1441949
    if-nez p0, :cond_0

    .line 1441950
    const/4 p0, 0x0

    .line 1441951
    :goto_0
    return-object p0

    .line 1441952
    :cond_0
    instance-of v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    if-eqz v0, :cond_1

    .line 1441953
    check-cast p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    goto :goto_0

    .line 1441954
    :cond_1
    new-instance v0, LX/97h;

    invoke-direct {v0}, LX/97h;-><init>()V

    .line 1441955
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/97h;->a:Ljava/lang/String;

    .line 1441956
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/97h;->b:LX/15i;

    iput v1, v0, LX/97h;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1441957
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/97h;->d:Ljava/lang/String;

    .line 1441958
    invoke-virtual {v0}, LX/97h;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object p0

    goto :goto_0

    .line 1441959
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1441960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1441961
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1441962
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x1327ce11

    invoke-static {v2, v1, v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1441963
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1441964
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1441965
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1441966
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1441967
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1441968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1441969
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1441970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1441971
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1441972
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1327ce11

    invoke-static {v2, v0, v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1441973
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1441974
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    .line 1441975
    iput v3, v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->f:I

    .line 1441976
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1441977
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1441978
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1441979
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1441943
    new-instance v0, LX/97i;

    invoke-direct {v0, p1}, LX/97i;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441944
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1441940
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1441941
    const/4 v0, 0x1

    const v1, -0x1327ce11

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->f:I

    .line 1441942
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1441938
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1441939
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1441937
    return-void
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441935
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1441936
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1441932
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;-><init>()V

    .line 1441933
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1441934
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441930
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->e:Ljava/lang/String;

    .line 1441931
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441928
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->g:Ljava/lang/String;

    .line 1441929
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1441927
    const v0, 0x6d718e11

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1441926
    const v0, 0x25d6af

    return v0
.end method
