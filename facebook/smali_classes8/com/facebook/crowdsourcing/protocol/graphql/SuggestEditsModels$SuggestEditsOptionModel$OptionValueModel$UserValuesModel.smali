.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x230363b8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1444459
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1444458
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1444508
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1444509
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1444505
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1444506
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1444507
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1444481
    if-nez p0, :cond_0

    .line 1444482
    const/4 p0, 0x0

    .line 1444483
    :goto_0
    return-object p0

    .line 1444484
    :cond_0
    instance-of v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;

    if-eqz v0, :cond_1

    .line 1444485
    check-cast p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;

    goto :goto_0

    .line 1444486
    :cond_1
    new-instance v2, LX/983;

    invoke-direct {v2}, LX/983;-><init>()V

    .line 1444487
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1444488
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1444489
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1444490
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1444491
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/983;->a:LX/0Px;

    .line 1444492
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1444493
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1444494
    iget-object v5, v2, LX/983;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1444495
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1444496
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1444497
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1444498
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1444499
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1444500
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1444501
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1444502
    new-instance v5, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;

    invoke-direct {v5, v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;-><init>(LX/15i;)V

    .line 1444503
    move-object p0, v5

    .line 1444504
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1444475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1444476
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1444477
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1444478
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1444479
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1444480
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1444473
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->e:Ljava/util/List;

    .line 1444474
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1444465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1444466
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1444467
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1444468
    if-eqz v1, :cond_0

    .line 1444469
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;

    .line 1444470
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;->e:Ljava/util/List;

    .line 1444471
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1444472
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1444462
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel$OptionValueModel$UserValuesModel;-><init>()V

    .line 1444463
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1444464
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1444461
    const v0, -0x63d1542a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1444460
    const v0, -0x4e57ab30

    return v0
.end method
