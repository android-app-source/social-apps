.class public final Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1441726
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1441725
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1441723
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1441724
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1441715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1441716
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1441717
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1441718
    iget v1, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1441719
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1441720
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1441721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1441722
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1441712
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1441713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1441714
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441701
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->f:Ljava/lang/String;

    .line 1441702
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1441708
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1441709
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->e:I

    .line 1441710
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->g:I

    .line 1441711
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1441705
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;-><init>()V

    .line 1441706
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1441707
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1441704
    const v0, -0x10a56ca

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1441703
    const v0, 0x437b93b

    return v0
.end method
