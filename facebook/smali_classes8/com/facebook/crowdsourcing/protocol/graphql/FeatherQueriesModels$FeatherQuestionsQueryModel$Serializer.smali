.class public final Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1439571
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1439572
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1439573
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1439574
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1439575
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1439576
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1439577
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1439578
    if-eqz v2, :cond_0

    .line 1439579
    const-string p0, "crowdsourcing_questions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439580
    invoke-static {v1, v2, p1, p2}, LX/97I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1439581
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1439582
    if-eqz v2, :cond_1

    .line 1439583
    const-string p0, "display_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439584
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1439585
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1439586
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1439587
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel$Serializer;->a(Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
