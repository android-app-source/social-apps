.class public final Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1440993
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1440992
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1440990
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1440991
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1440987
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1440988
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1440989
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;)Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;
    .locals 8

    .prologue
    .line 1440967
    if-nez p0, :cond_0

    .line 1440968
    const/4 p0, 0x0

    .line 1440969
    :goto_0
    return-object p0

    .line 1440970
    :cond_0
    instance-of v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    if-eqz v0, :cond_1

    .line 1440971
    check-cast p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    goto :goto_0

    .line 1440972
    :cond_1
    new-instance v0, LX/979;

    invoke-direct {v0}, LX/979;-><init>()V

    .line 1440973
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/979;->a:Ljava/lang/String;

    .line 1440974
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1440975
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1440976
    iget-object v3, v0, LX/979;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1440977
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1440978
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1440979
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1440980
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1440981
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1440982
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1440983
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1440984
    new-instance v3, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    invoke-direct {v3, v2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;-><init>(LX/15i;)V

    .line 1440985
    move-object p0, v3

    .line 1440986
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1440994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1440995
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1440996
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1440997
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1440998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1440999
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1440964
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1440965
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1440966
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440962
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->e:Ljava/lang/String;

    .line 1440963
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1440959
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;-><init>()V

    .line 1440960
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1440961
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1440957
    const v0, -0x348bfab8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1440958
    const v0, 0x437b93b

    return v0
.end method
