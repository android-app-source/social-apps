.class public final Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4589e627
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1441753
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1441754
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1441755
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1441756
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1441765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1441766
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1441767
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1441768
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1441769
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1441770
    iget v3, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->e:I

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 1441771
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1441772
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1441773
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1441774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1441775
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1441757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1441758
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1441759
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    .line 1441760
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1441761
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;

    .line 1441762
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    .line 1441763
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1441764
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1441777
    new-instance v0, LX/97S;

    invoke-direct {v0, p1}, LX/97S;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441776
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1441748
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1441749
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->e:I

    .line 1441750
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1441751
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1441752
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1441739
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1441736
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;-><init>()V

    .line 1441737
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1441738
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1441735
    const v0, -0x5c553c58

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1441734
    const v0, 0x285feb

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1441740
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1441741
    iget v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->e:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441742
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->f:Ljava/lang/String;

    .line 1441743
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441744
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->g:Ljava/lang/String;

    .line 1441745
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1441746
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    .line 1441747
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->h:Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    return-object v0
.end method
