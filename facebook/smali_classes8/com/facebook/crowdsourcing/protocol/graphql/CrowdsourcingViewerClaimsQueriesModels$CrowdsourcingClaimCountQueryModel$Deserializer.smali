.class public final Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1439397
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;

    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1439398
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1439344
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1439345
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1439346
    const/4 v2, 0x0

    .line 1439347
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1439348
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1439349
    :goto_0
    move v1, v2

    .line 1439350
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1439351
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1439352
    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;

    invoke-direct {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;-><init>()V

    .line 1439353
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1439354
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1439355
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1439356
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1439357
    :cond_0
    return-object v1

    .line 1439358
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1439359
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1439360
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1439361
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1439362
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1439363
    const-string v4, "crowdsourced_field"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1439364
    const/4 v3, 0x0

    .line 1439365
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1439366
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1439367
    :goto_2
    move v1, v3

    .line 1439368
    goto :goto_1

    .line 1439369
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1439370
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1439371
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1439372
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1439373
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1439374
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1439375
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1439376
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1439377
    const-string v5, "viewer_claims"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1439378
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1439379
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_d

    .line 1439380
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1439381
    :goto_4
    move v1, v4

    .line 1439382
    goto :goto_3

    .line 1439383
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1439384
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1439385
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 1439386
    :cond_9
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_b

    .line 1439387
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1439388
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1439389
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v7, :cond_9

    .line 1439390
    const-string p0, "count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1439391
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    goto :goto_5

    .line 1439392
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1439393
    :cond_b
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1439394
    if-eqz v1, :cond_c

    .line 1439395
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 1439396
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_d
    move v1, v4

    move v6, v4

    goto :goto_5
.end method
