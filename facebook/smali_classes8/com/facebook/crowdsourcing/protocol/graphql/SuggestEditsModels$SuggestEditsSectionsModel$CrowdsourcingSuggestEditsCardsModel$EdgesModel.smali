.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c682dc4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1444835
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1444834
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1444856
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1444857
    return-void
.end method

.method private j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1444858
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    .line 1444859
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1444842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1444843
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1444844
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1444845
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1444846
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1444847
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1444848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1444849
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1444850
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    .line 1444851
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1444852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;

    .line 1444853
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    .line 1444854
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1444855
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1444841
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1444838
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;-><init>()V

    .line 1444839
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1444840
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1444837
    const v0, 0xe6d5ffd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1444836
    const v0, 0x7a124e9b

    return v0
.end method
