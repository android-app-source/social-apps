.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1442910
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel;

    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1442911
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1442912
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1442913
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1442914
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1442915
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1442916
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442917
    if-eqz v2, :cond_0

    .line 1442918
    const-string p0, "fri"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442919
    invoke-static {v1, v2, p1, p2}, LX/98F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442920
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442921
    if-eqz v2, :cond_1

    .line 1442922
    const-string p0, "mon"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442923
    invoke-static {v1, v2, p1, p2}, LX/98G;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442924
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442925
    if-eqz v2, :cond_2

    .line 1442926
    const-string p0, "sat"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442927
    invoke-static {v1, v2, p1, p2}, LX/98H;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442928
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442929
    if-eqz v2, :cond_3

    .line 1442930
    const-string p0, "sun"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442931
    invoke-static {v1, v2, p1, p2}, LX/98I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442932
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442933
    if-eqz v2, :cond_4

    .line 1442934
    const-string p0, "thu"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442935
    invoke-static {v1, v2, p1, p2}, LX/98J;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442936
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442937
    if-eqz v2, :cond_5

    .line 1442938
    const-string p0, "tue"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442939
    invoke-static {v1, v2, p1, p2}, LX/98K;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442940
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1442941
    if-eqz v2, :cond_6

    .line 1442942
    const-string p0, "wed"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1442943
    invoke-static {v1, v2, p1, p2}, LX/98L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1442944
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1442945
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1442946
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$Serializer;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel;LX/0nX;LX/0my;)V

    return-void
.end method
