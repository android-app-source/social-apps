.class public final Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1ca60ab8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1439950
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1439951
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1439952
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1439953
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1439954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1439955
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1439956
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1439957
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1439958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1439959
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1439960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1439961
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1439962
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    .line 1439963
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1439964
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;

    .line 1439965
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    .line 1439966
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1439967
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1439968
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    .line 1439969
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1439970
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;-><init>()V

    .line 1439971
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1439972
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1439973
    const v0, -0x1890a3e6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1439974
    const v0, -0x343055c2    # -2.7219068E7f

    return v0
.end method
