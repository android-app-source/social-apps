.class public final Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x9261dc1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438940
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438941
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1438942
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1438943
    return-void
.end method

.method private j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438944
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438945
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1438946
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438947
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1438948
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1438949
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1438950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438951
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1438952
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438953
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1438954
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438955
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1438956
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;

    .line 1438957
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    .line 1438958
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438959
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438960
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1438961
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;-><init>()V

    .line 1438962
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1438963
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1438964
    const v0, 0x7d4451df

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1438965
    const v0, -0x6ab29e98

    return v0
.end method
