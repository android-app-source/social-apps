.class public final Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438703
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1438706
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1438704
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1438705
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1438697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438698
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1438699
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1438700
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1438701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438702
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1438707
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1438708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1438709
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1438695
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->e:Ljava/lang/String;

    .line 1438696
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1438690
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;-><init>()V

    .line 1438691
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1438692
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1438694
    const v0, 0x7d09c271

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1438693
    const v0, -0x726d476c

    return v0
.end method
