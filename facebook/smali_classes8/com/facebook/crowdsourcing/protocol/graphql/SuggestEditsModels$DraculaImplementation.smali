.class public final Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1443619
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1443620
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1443617
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1443618
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1443574
    if-nez p1, :cond_0

    .line 1443575
    :goto_0
    return v1

    .line 1443576
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1443577
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1443578
    :sswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1443579
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1443580
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1443581
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1443582
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1443583
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1443584
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1443585
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1443586
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1443587
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1443588
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1443589
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1443590
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1443591
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1443592
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1443593
    const v2, 0x7a3687e0

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1443594
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1443595
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1443596
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1443597
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1443598
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1443599
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1443600
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1443601
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1443602
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1443603
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1443604
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1443605
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1443606
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1443607
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1443608
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1443609
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1443610
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1443611
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1443612
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1443613
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1443614
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1443615
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1443616
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69a50608 -> :sswitch_5
        -0x1327ce11 -> :sswitch_0
        0x31f7b0b9 -> :sswitch_1
        0x43639b9b -> :sswitch_6
        0x72e9906d -> :sswitch_2
        0x7576b2cb -> :sswitch_4
        0x7a3687e0 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1443573
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1443568
    sparse-switch p2, :sswitch_data_0

    .line 1443569
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1443570
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1443571
    const v1, 0x7a3687e0

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1443572
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69a50608 -> :sswitch_1
        -0x1327ce11 -> :sswitch_1
        0x31f7b0b9 -> :sswitch_1
        0x43639b9b -> :sswitch_1
        0x72e9906d -> :sswitch_0
        0x7576b2cb -> :sswitch_1
        0x7a3687e0 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1443562
    if-eqz p1, :cond_0

    .line 1443563
    invoke-static {p0, p1, p2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    move-result-object v1

    .line 1443564
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    .line 1443565
    if-eq v0, v1, :cond_0

    .line 1443566
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1443567
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1443561
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1443533
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1443534
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1443621
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1443622
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1443623
    :cond_0
    iput-object p1, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a:LX/15i;

    .line 1443624
    iput p2, p0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->b:I

    .line 1443625
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1443560
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1443559
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1443556
    iget v0, p0, LX/1vt;->c:I

    .line 1443557
    move v0, v0

    .line 1443558
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1443553
    iget v0, p0, LX/1vt;->c:I

    .line 1443554
    move v0, v0

    .line 1443555
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1443550
    iget v0, p0, LX/1vt;->b:I

    .line 1443551
    move v0, v0

    .line 1443552
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1443547
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1443548
    move-object v0, v0

    .line 1443549
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1443538
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1443539
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1443540
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1443541
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1443542
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1443543
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1443544
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1443545
    invoke-static {v3, v9, v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1443546
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1443535
    iget v0, p0, LX/1vt;->c:I

    .line 1443536
    move v0, v0

    .line 1443537
    return v0
.end method
