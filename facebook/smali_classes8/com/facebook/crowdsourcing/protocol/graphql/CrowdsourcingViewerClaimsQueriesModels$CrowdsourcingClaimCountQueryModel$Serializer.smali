.class public final Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1439401
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;

    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1439402
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1439403
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1439404
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1439405
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1439406
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1439407
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1439408
    if-eqz v2, :cond_2

    .line 1439409
    const-string p0, "crowdsourced_field"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439410
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1439411
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1439412
    if-eqz p0, :cond_1

    .line 1439413
    const-string v0, "viewer_claims"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439414
    const/4 v0, 0x0

    .line 1439415
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1439416
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1439417
    if-eqz v0, :cond_0

    .line 1439418
    const-string v2, "count"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439419
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1439420
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1439421
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1439422
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1439423
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1439424
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel$Serializer;->a(Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingViewerClaimsQueriesModels$CrowdsourcingClaimCountQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
