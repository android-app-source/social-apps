.class public final Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6ca71761
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1440598
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1440691
    const-class v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1440689
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1440690
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1440686
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1440687
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1440688
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;)Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;
    .locals 15

    .prologue
    .line 1440647
    if-nez p0, :cond_0

    .line 1440648
    const/4 p0, 0x0

    .line 1440649
    :goto_0
    return-object p0

    .line 1440650
    :cond_0
    instance-of v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    if-eqz v0, :cond_1

    .line 1440651
    check-cast p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    goto :goto_0

    .line 1440652
    :cond_1
    new-instance v1, LX/971;

    invoke-direct {v1}, LX/971;-><init>()V

    .line 1440653
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;)Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    iput-object v0, v1, LX/971;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    .line 1440654
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1440655
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1440656
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1440657
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1440658
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/971;->b:LX/0Px;

    .line 1440659
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;)Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v0

    iput-object v0, v1, LX/971;->c:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    .line 1440660
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/971;->d:Ljava/lang/String;

    .line 1440661
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a(LX/1k1;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/971;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1440662
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->gs_()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;)Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v0

    iput-object v0, v1, LX/971;->f:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    .line 1440663
    const/4 v8, 0x1

    const/4 v14, 0x0

    const/4 v6, 0x0

    .line 1440664
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1440665
    iget-object v5, v1, LX/971;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1440666
    iget-object v7, v1, LX/971;->b:LX/0Px;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 1440667
    iget-object v9, v1, LX/971;->c:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1440668
    iget-object v10, v1, LX/971;->d:Ljava/lang/String;

    invoke-virtual {v4, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1440669
    iget-object v11, v1, LX/971;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-static {v4, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1440670
    iget-object v12, v1, LX/971;->f:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    invoke-static {v4, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1440671
    const/4 v13, 0x6

    invoke-virtual {v4, v13}, LX/186;->c(I)V

    .line 1440672
    invoke-virtual {v4, v14, v5}, LX/186;->b(II)V

    .line 1440673
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1440674
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1440675
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1440676
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v11}, LX/186;->b(II)V

    .line 1440677
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v12}, LX/186;->b(II)V

    .line 1440678
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1440679
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1440680
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1440681
    invoke-virtual {v5, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1440682
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1440683
    new-instance v5, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-direct {v5, v4}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;-><init>(LX/15i;)V

    .line 1440684
    move-object p0, v5

    .line 1440685
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440645
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    .line 1440646
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    return-object v0
.end method

.method private k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440643
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    .line 1440644
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440641
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1440642
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440639
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    .line 1440640
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1440623
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1440624
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1440625
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1440626
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1440627
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1440628
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1440629
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1440630
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1440631
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1440632
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1440633
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1440634
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1440635
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1440636
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1440637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1440638
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1440600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1440601
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1440602
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    .line 1440603
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1440604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    .line 1440605
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    .line 1440606
    :cond_0
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1440607
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    .line 1440608
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1440609
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    .line 1440610
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->g:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    .line 1440611
    :cond_1
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1440612
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1440613
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1440614
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    .line 1440615
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1440616
    :cond_2
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1440617
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    .line 1440618
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1440619
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    .line 1440620
    iput-object v0, v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    .line 1440621
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1440622
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1440599
    new-instance v0, LX/974;

    invoke-direct {v0, p1}, LX/974;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440582
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1440583
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1440584
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1440585
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1440586
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->f:Ljava/util/List;

    .line 1440587
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1440588
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-direct {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;-><init>()V

    .line 1440589
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1440590
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440591
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$CityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440592
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->h:Ljava/lang/String;

    .line 1440593
    iget-object v0, p0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1440594
    const v0, -0x233345c6

    return v0
.end method

.method public final synthetic e()LX/1k1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440595
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1440596
    const v0, 0x25d6af

    return v0
.end method

.method public final synthetic gs_()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1440597
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method
