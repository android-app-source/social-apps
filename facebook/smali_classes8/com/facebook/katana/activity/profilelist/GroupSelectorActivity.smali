.class public Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1497877
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1497868
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1497869
    if-nez p1, :cond_0

    .line 1497870
    const v0, 0x7f0824a8

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/9UY;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1497871
    new-instance v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    invoke-direct {v0}, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    .line 1497872
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1a78

    iget-object v2, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1497873
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    new-instance v1, LX/9UJ;

    invoke-direct {v1, p0}, LX/9UJ;-><init>(Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;)V

    .line 1497874
    iput-object v1, v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->d:LX/9UI;

    .line 1497875
    return-void

    .line 1497876
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1a78

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1497865
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->setResult(I)V

    .line 1497866
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/GroupSelectorActivity;->finish()V

    .line 1497867
    return-void
.end method
