.class public Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements LX/0fj;


# instance fields
.field public a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2Rd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/A5m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private h:LX/9UH;

.field private i:Landroid/widget/TextView;

.field public j:Ljava/lang/String;

.field public k:LX/9UZ;

.field public l:LX/9U7;

.field private m:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/9U9;

.field public final o:LX/8SB;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1497841
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1497842
    const-string v0, "STATE_KEY_LIST_VIEWERS_CANNOT_POST"

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->g:Ljava/lang/String;

    .line 1497843
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1497844
    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    .line 1497845
    new-instance v0, LX/8SB;

    invoke-direct {v0}, LX/8SB;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->o:LX/8SB;

    .line 1497846
    return-void
.end method

.method public static b(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1497778
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v0, LX/9UD;

    .line 1497779
    iget-object v1, v0, LX/9UA;->d:Landroid/database/Cursor;

    move-object v1, v1

    .line 1497780
    if-nez v1, :cond_0

    .line 1497781
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->k:LX/9UZ;

    invoke-virtual {v0, v3}, LX/9UZ;->a(Z)V

    .line 1497782
    :goto_0
    return-void

    .line 1497783
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->k:LX/9UZ;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/9UZ;->a(Z)V

    .line 1497784
    iget-object v1, v0, LX/9UA;->d:Landroid/database/Cursor;

    move-object v1, v1

    .line 1497785
    invoke-virtual {v0, v1}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1497786
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    invoke-virtual {v0}, LX/3Tf;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1497787
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1497788
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->k:LX/9UZ;

    invoke-virtual {v0, v3}, LX/9UZ;->a(Z)V

    .line 1497789
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1497839
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->o:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->c()V

    .line 1497840
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1497828
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1497829
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-static {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v4

    check-cast v4, LX/2Rd;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/A5m;->b(LX/0QB;)LX/A5m;

    move-result-object v0

    check-cast v0, LX/A5m;

    iput-object v3, v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    iput-object v4, v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->b:LX/2Rd;

    iput-object v5, v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->c:LX/0ad;

    iput-object v6, v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->d:LX/0tX;

    iput-object v7, v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->e:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->f:LX/A5m;

    .line 1497830
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 1497831
    new-instance v0, LX/9UE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->b:LX/2Rd;

    iget-object v6, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->c:LX/0ad;

    iget-object v7, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/9UE;-><init>(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;LX/2Rd;LX/0ad;LX/0Rf;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    .line 1497832
    new-instance v0, LX/9UH;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1}, LX/9UH;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->h:LX/9UH;

    .line 1497833
    if-eqz p1, :cond_0

    .line 1497834
    const-string v0, "STATE_KEY_LIST_VIEWERS_CANNOT_POST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 1497835
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/1YA;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    .line 1497836
    :cond_0
    return-void

    .line 1497837
    :cond_1
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1497838
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x6ce60313

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1497847
    const v0, 0x7f03106a

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1497848
    new-instance v0, LX/9UZ;

    invoke-direct {v0, v2, p0}, LX/9UZ;-><init>(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->k:LX/9UZ;

    .line 1497849
    const v0, 0x7f0d0d65

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1497850
    const v3, 0x7f081476

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1497851
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->k:LX/9UZ;

    .line 1497852
    iget-object v3, v0, LX/9UZ;->a:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v3

    .line 1497853
    iget-object v3, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1497854
    const v0, 0x7f0d2749

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    .line 1497855
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1497856
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    new-instance v3, LX/9UF;

    invoke-direct {v3, p0}, LX/9UF;-><init>(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1497857
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->o:LX/8SB;

    iget-object v3, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/8SB;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 1497858
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1497859
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 1497860
    const/16 v0, 0x2b

    const v3, -0x22a7803d

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x76bbb74

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1497824
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->e:LX/1Ck;

    if-eqz v1, :cond_0

    .line 1497825
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1497826
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1497827
    const/16 v1, 0x2b

    const v2, -0x78b4f35f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    .line 1497806
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497807
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    iget-wide v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1497808
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 1497809
    const/4 v1, -0x1

    .line 1497810
    iput v1, v0, LX/0hs;->t:I

    .line 1497811
    invoke-virtual {v0, p2}, LX/0ht;->c(Landroid/view/View;)V

    .line 1497812
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08148e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1497813
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1497814
    :goto_0
    return-void

    .line 1497815
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1497816
    const-string v2, "extra_composer_target_data"

    new-instance v3, LX/89I;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    sget-object v6, LX/2rw;->USER:LX/2rw;

    invoke-direct {v3, v4, v5, v6}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1497817
    iput-object v4, v3, LX/89I;->c:Ljava/lang/String;

    .line 1497818
    move-object v3, v3

    .line 1497819
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1497820
    iput-object v0, v3, LX/89I;->d:Ljava/lang/String;

    .line 1497821
    move-object v0, v3

    .line 1497822
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1497823
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->l:LX/9U7;

    invoke-interface {v0, v1}, LX/9U7;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v3, -0x136143d3

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1497794
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1497795
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v0, LX/9UD;

    .line 1497796
    iget-object v1, v0, LX/9UA;->d:Landroid/database/Cursor;

    move-object v0, v1

    .line 1497797
    if-nez v0, :cond_1

    .line 1497798
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->h:LX/9UH;

    const/4 v1, 0x1

    sget-object v3, LX/0P0;->g:Landroid/net/Uri;

    sget-object v4, LX/9UW;->a:[Ljava/lang/String;

    const-string v5, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, LX/9UH;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1497799
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->c:LX/0ad;

    sget-short v1, LX/1EB;->L:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1497800
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->f:LX/A5m;

    new-instance v1, LX/9UG;

    invoke-direct {v1, p0}, LX/9UG;-><init>(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V

    invoke-virtual {v0, v1, v2}, LX/A5m;->a(LX/0TF;LX/0TF;)V

    .line 1497801
    :cond_0
    invoke-static {p0}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->b(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V

    .line 1497802
    const v0, 0x3ec9548d

    invoke-static {v0, v8}, LX/02F;->f(II)V

    return-void

    .line 1497803
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v0, LX/9UD;

    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    .line 1497804
    iput-object v1, v0, LX/9UD;->k:LX/0Rf;

    .line 1497805
    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1497790
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1497791
    const-string v0, "STATE_KEY_LIST_VIEWERS_CANNOT_POST"

    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->m:LX/0Rf;

    invoke-static {v1}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 1497792
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1497793
    return-void
.end method
