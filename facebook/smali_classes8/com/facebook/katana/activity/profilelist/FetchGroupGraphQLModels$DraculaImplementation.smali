.class public final Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1496804
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1496805
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1496885
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1496886
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1496856
    if-nez p1, :cond_0

    .line 1496857
    :goto_0
    return v0

    .line 1496858
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1496859
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1496860
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1496861
    const v2, 0xbd658ac

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1496862
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1496863
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1496864
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1496865
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1496866
    const v2, -0x441244e4

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1496867
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1496868
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1496869
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1496870
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1496871
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1496872
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1496873
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1496874
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1496875
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1496876
    const v2, 0x582507db

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1496877
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1496878
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1496879
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1496880
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1496881
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1496882
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1496883
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1496884
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x788f32c0 -> :sswitch_0
        -0x441244e4 -> :sswitch_2
        0xbd658ac -> :sswitch_1
        0xde48306 -> :sswitch_3
        0x582507db -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1496855
    new-instance v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1496846
    sparse-switch p2, :sswitch_data_0

    .line 1496847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1496848
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1496849
    const v1, 0xbd658ac

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1496850
    :goto_0
    :sswitch_1
    return-void

    .line 1496851
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1496852
    const v1, -0x441244e4

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1496853
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1496854
    const v1, 0x582507db

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x788f32c0 -> :sswitch_0
        -0x441244e4 -> :sswitch_1
        0xbd658ac -> :sswitch_2
        0xde48306 -> :sswitch_3
        0x582507db -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1496840
    if-eqz p1, :cond_0

    .line 1496841
    invoke-static {p0, p1, p2}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1496842
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    .line 1496843
    if-eq v0, v1, :cond_0

    .line 1496844
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1496845
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1496839
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1496837
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1496838
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1496832
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1496833
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1496834
    :cond_0
    iput-object p1, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1496835
    iput p2, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->b:I

    .line 1496836
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1496831
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1496830
    new-instance v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1496827
    iget v0, p0, LX/1vt;->c:I

    .line 1496828
    move v0, v0

    .line 1496829
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1496824
    iget v0, p0, LX/1vt;->c:I

    .line 1496825
    move v0, v0

    .line 1496826
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1496821
    iget v0, p0, LX/1vt;->b:I

    .line 1496822
    move v0, v0

    .line 1496823
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496818
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1496819
    move-object v0, v0

    .line 1496820
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1496809
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1496810
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1496811
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1496812
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1496813
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1496814
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1496815
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1496816
    invoke-static {v3, v9, v2}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1496817
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1496806
    iget v0, p0, LX/1vt;->c:I

    .line 1496807
    move v0, v0

    .line 1496808
    return v0
.end method
