.class public final Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d080ea9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1496998
    const-class v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1496974
    const-class v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1496999
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1497000
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1497001
    iput-object p1, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1497002
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1497003
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1497004
    if-eqz v0, :cond_0

    .line 1497005
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x6

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1497006
    :cond_0
    return-void

    .line 1497007
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1497008
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->i:Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->i:Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    .line 1497009
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->i:Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1497010
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1497011
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1497012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1497013
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x788f32c0

    invoke-static {v1, v0, v2}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1497014
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1497015
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0xde48306

    invoke-static {v3, v2, v4}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1497016
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1497017
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->o()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1497018
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1497019
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1497020
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1497021
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1497022
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1497023
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1497024
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1497025
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1497026
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1497027
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1497028
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1497029
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1497030
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1497031
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1497032
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x788f32c0

    invoke-static {v2, v0, v3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1497033
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1497034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;

    .line 1497035
    iput v3, v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->e:I

    move-object v1, v0

    .line 1497036
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1497037
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xde48306

    invoke-static {v2, v0, v3}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1497038
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1497039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;

    .line 1497040
    iput v3, v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->g:I

    move-object v1, v0

    .line 1497041
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->o()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1497042
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->o()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    .line 1497043
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->o()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1497044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;

    .line 1497045
    iput-object v0, v1, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->i:Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    .line 1497046
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1497047
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1497048
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1497049
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 1497050
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1497051
    new-instance v0, LX/9Tu;

    invoke-direct {v0, p1}, LX/9Tu;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496997
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1497052
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1497053
    const/4 v0, 0x0

    const v1, -0x788f32c0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->e:I

    .line 1497054
    const/4 v0, 0x2

    const v1, 0xde48306

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->g:I

    .line 1497055
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1496991
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1496992
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1496993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1496994
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    .line 1496995
    :goto_0
    return-void

    .line 1496996
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1496988
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1496989
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1496990
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1496985
    new-instance v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;

    invoke-direct {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;-><init>()V

    .line 1496986
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1496987
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1496984
    const v0, -0x52916c2b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1496983
    const v0, 0x41e065f

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1496981
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1496982
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496979
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->f:Ljava/lang/String;

    .line 1496980
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupIcon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496977
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1496978
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496975
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->h:Ljava/lang/String;

    .line 1496976
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496972
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1496973
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method
