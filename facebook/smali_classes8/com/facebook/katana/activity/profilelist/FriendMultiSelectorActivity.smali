.class public Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;
.super Lcom/facebook/katana/activity/profilelist/ProfileListActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/10X;


# instance fields
.field private A:LX/2Rd;

.field private B:LX/0ad;

.field private C:LX/0zG;

.field public p:LX/9U6;

.field public q:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field public s:I

.field public t:Landroid/widget/ImageView;

.field public u:Landroid/widget/TextView;

.field public v:Ljava/lang/String;

.field public y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field private z:LX/0gd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1497593
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;-><init>()V

    .line 1497594
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->r:Ljava/util/ArrayList;

    .line 1497595
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/Set;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1497590
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1497591
    const-string v1, "profiles"

    invoke-static {p1}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 1497592
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1497587
    invoke-static {p0, p1}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a(Landroid/content/Context;Ljava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    .line 1497588
    const-string v1, "exclude_profiles"

    invoke-static {p2}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 1497589
    return-object v0
.end method

.method private a(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/0gd;LX/0zG;LX/2Rd;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1497581
    iput-object p1, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1497582
    iput-object p2, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->z:LX/0gd;

    .line 1497583
    iput-object p3, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->C:LX/0zG;

    .line 1497584
    iput-object p4, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->A:LX/2Rd;

    .line 1497585
    iput-object p5, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->B:LX/0ad;

    .line 1497586
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    invoke-static {v5}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v5}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v2

    check-cast v2, LX/0gd;

    invoke-static {v5}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {v5}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v4

    check-cast v4, LX/2Rd;

    invoke-static {v5}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/0gd;LX/0zG;LX/2Rd;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;IZ)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 1497570
    if-eqz p2, :cond_0

    .line 1497571
    iget v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->s:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->s:I

    .line 1497572
    :goto_0
    iget v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->s:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1497573
    :goto_1
    if-eqz v0, :cond_2

    .line 1497574
    const v0, 0x7f0d0d65

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1497575
    const v0, 0x7f0d0d66

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1497576
    :goto_2
    return-void

    .line 1497577
    :cond_0
    iget v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->s:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v0, v2

    iput v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->s:I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1497578
    goto :goto_1

    .line 1497579
    :cond_2
    const v0, 0x7f0d0d65

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1497580
    const v0, 0x7f0d0d66

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public static m(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;)V
    .locals 3

    .prologue
    .line 1497563
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1497564
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1497565
    const-string v1, "profiles"

    iget-object v2, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->q:LX/0Rf;

    invoke-static {v2}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 1497566
    const-string v1, "full_profiles"

    iget-object v2, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1497567
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 1497568
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->finish()V

    .line 1497569
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1497596
    const v0, 0x7f0312ca

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->setContentView(I)V

    .line 1497597
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1497598
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->x:Lcom/facebook/widget/listview/BetterListView;

    .line 1497599
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->x:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1497600
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->x:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->p:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1497601
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->o()V

    .line 1497602
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->x:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1497603
    const v0, 0x7f0d2749

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->u:Landroid/widget/TextView;

    .line 1497604
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1497605
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->u:Landroid/widget/TextView;

    new-instance v1, LX/9U4;

    invoke-direct {v1, p0}, LX/9U4;-><init>(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1497606
    const v0, 0x7f0d274a

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->t:Landroid/widget/ImageView;

    .line 1497607
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->t:Landroid/widget/ImageView;

    new-instance v1, LX/9U5;

    invoke-direct {v1, p0}, LX/9U5;-><init>(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1497608
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1497560
    const v0, 0x7f0d0d65

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1497561
    const v1, 0x7f081476

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1497562
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1497559
    const-string v0, "select_friends_view"

    return-object v0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 8

    .prologue
    .line 1497509
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    check-cast v0, LX/9UD;

    .line 1497510
    invoke-virtual {v0, p2}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497511
    iget-object v5, v0, LX/9UD;->k:LX/0Rf;

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/9UD;->k:LX/0Rf;

    iget-wide v6, v4, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1497512
    :cond_0
    iget-object v5, v0, LX/9UD;->c:LX/9Tr;

    iget-wide v6, v4, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1497513
    const v6, 0x7f0d092e

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 1497514
    iget-object v7, v5, LX/9Tr;->a:Ljava/util/Collection;

    invoke-interface {v7, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1497515
    iget-object v7, v5, LX/9Tr;->a:Ljava/util/Collection;

    invoke-interface {v7, v4}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1497516
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1497517
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    invoke-virtual {v0, p2}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497518
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->q:LX/0Rf;

    iget-wide v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1497519
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1497520
    :goto_1
    return-void

    .line 1497521
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1497522
    :cond_3
    iget-object v7, v5, LX/9Tr;->a:Ljava/util/Collection;

    invoke-interface {v7, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1497523
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1497558
    const v0, 0x7f081474

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1497544
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->b(Landroid/os/Bundle;)V

    .line 1497545
    invoke-static {p0, p0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1497546
    if-eqz p1, :cond_0

    const-string v0, "profiles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 1497547
    :goto_0
    invoke-static {v0}, LX/1YA;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->q:LX/0Rf;

    .line 1497548
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "exclude_profiles"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    .line 1497549
    if-nez v0, :cond_1

    .line 1497550
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v6, v0

    .line 1497551
    :goto_1
    new-instance v0, LX/9UD;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->q:LX/0Rf;

    iget-object v4, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->A:LX/2Rd;

    iget-object v5, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->B:LX/0ad;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/9UD;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;LX/2Rd;LX/0ad;LX/0Rf;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->w:LX/9U9;

    .line 1497552
    new-instance v0, LX/9U6;

    invoke-direct {v0, p0, p0}, LX/9U6;-><init>(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->p:LX/9U6;

    .line 1497553
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->n()V

    .line 1497554
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 1497555
    return-void

    .line 1497556
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profiles"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    goto :goto_0

    .line 1497557
    :cond_1
    invoke-static {v0}, LX/1YA;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v6

    goto :goto_1
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/16 v0, 0x22

    const v3, -0x1dfa85fb

    invoke-static {v9, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1497537
    invoke-super {p0}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->onResume()V

    .line 1497538
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    check-cast v0, LX/9UD;

    .line 1497539
    iget-object v3, v0, LX/9UA;->d:Landroid/database/Cursor;

    move-object v0, v3

    .line 1497540
    if-nez v0, :cond_0

    .line 1497541
    invoke-static {p0, v1, v1}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a$redex0(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;IZ)V

    .line 1497542
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->p:LX/9U6;

    sget-object v3, LX/0P0;->g:Landroid/net/Uri;

    sget-object v4, LX/9UW;->a:[Ljava/lang/String;

    const-string v5, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, LX/9U6;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1497543
    :cond_0
    const/16 v0, 0x23

    const v1, 0x69f3497c

    invoke-static {v9, v0, v1, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1497534
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1497535
    const-string v0, "profiles"

    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->q:LX/0Rf;

    invoke-static {v1}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 1497536
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2e9b1de4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1497524
    invoke-super {p0}, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->onStart()V

    .line 1497525
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v2, 0x7f0824a4

    invoke-virtual {p0, v2}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1497526
    iput-object v2, v0, LX/108;->g:Ljava/lang/String;

    .line 1497527
    move-object v0, v0

    .line 1497528
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1497529
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->C:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1497530
    if-eqz v0, :cond_0

    .line 1497531
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1497532
    new-instance v2, LX/9U3;

    invoke-direct {v2, p0}, LX/9U3;-><init>(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;)V

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1497533
    :cond_0
    const/16 v0, 0x23

    const v2, 0x2118f410

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
