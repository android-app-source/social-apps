.class public final Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1496952
    const-class v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1496961
    const-class v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1496959
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1496960
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1496953
    iput-object p1, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->f:Ljava/lang/String;

    .line 1496954
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1496955
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1496956
    if-eqz v0, :cond_0

    .line 1496957
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1496958
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496950
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->e:Ljava/lang/String;

    .line 1496951
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496948
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->f:Ljava/lang/String;

    .line 1496949
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1496940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1496941
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1496942
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1496943
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1496944
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1496945
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1496946
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1496947
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1496962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1496963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1496964
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1496939
    new-instance v0, LX/9Tv;

    invoke-direct {v0, p1}, LX/9Tv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1496938
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1496932
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1496933
    invoke-direct {p0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1496934
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1496935
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1496936
    :goto_0
    return-void

    .line 1496937
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1496924
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1496925
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;->a(Ljava/lang/String;)V

    .line 1496926
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1496929
    new-instance v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;

    invoke-direct {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel$ParentGroupModel;-><init>()V

    .line 1496930
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1496931
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1496928
    const v0, -0x1e26617d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1496927
    const v0, 0x41e065f

    return v0
.end method
