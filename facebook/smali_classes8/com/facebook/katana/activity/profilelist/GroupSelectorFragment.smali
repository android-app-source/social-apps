.class public Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements LX/0fj;


# instance fields
.field private a:Landroid/widget/TextView;

.field public b:LX/9U9;

.field public c:LX/9UZ;

.field public d:LX/9UI;

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/8SB;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1497972
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1497973
    new-instance v0, LX/8SB;

    invoke-direct {v0}, LX/8SB;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->h:LX/8SB;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    iput-object v1, p1, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->e:LX/1Ck;

    iput-object v2, p1, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->f:LX/0kL;

    iput-object p0, p1, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->g:LX/0tX;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1497974
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->h:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->c()V

    .line 1497975
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1497976
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1497977
    const-class v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    invoke-static {v0, p0}, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1497978
    new-instance v0, LX/9UM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9UM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->b:LX/9U9;

    .line 1497979
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x3b54ea08

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1497980
    const v0, 0x7f03106a

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1497981
    new-instance v0, LX/9UZ;

    invoke-direct {v0, v2, p0}, LX/9UZ;-><init>(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->c:LX/9UZ;

    .line 1497982
    const v0, 0x7f0d0d65

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1497983
    const v3, 0x7f0824a7

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1497984
    const v0, 0x7f0d2749

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->a:Landroid/widget/TextView;

    .line 1497985
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->a:Landroid/widget/TextView;

    new-instance v3, LX/9UN;

    invoke-direct {v3, p0}, LX/9UN;-><init>(Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1497986
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->h:LX/8SB;

    iget-object v3, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/8SB;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 1497987
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->h:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1497988
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 1497989
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->c:LX/9UZ;

    .line 1497990
    iget-object v3, v0, LX/9UZ;->a:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v3

    .line 1497991
    iget-object v3, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->b:LX/9U9;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1497992
    invoke-virtual {v0, v5}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollEnabled(Z)V

    .line 1497993
    const/16 v0, 0x2b

    const v3, -0x2bdefffc

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    .line 1497994
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->b:LX/9U9;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497995
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1497996
    const-string v2, "extra_composer_target_data"

    new-instance v3, LX/89I;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    sget-object v6, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v3, v4, v5, v6}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1497997
    iput-object v0, v3, LX/89I;->c:Ljava/lang/String;

    .line 1497998
    move-object v0, v3

    .line 1497999
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1498000
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->d:LX/9UI;

    invoke-interface {v0, v1}, LX/9UI;->a(Landroid/content/Intent;)V

    .line 1498001
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x467ae42e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1498002
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1498003
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1498004
    const/16 v1, 0x2b

    const v2, 0x1ea10d1d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x68380ed6

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1498005
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1498006
    iget-object v1, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->c:LX/9UZ;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/9UZ;->a(Z)V

    .line 1498007
    new-instance v1, LX/9Ts;

    invoke-direct {v1}, LX/9Ts;-><init>()V

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    .line 1498008
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 1498009
    iget-object v2, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->g:LX/0tX;

    invoke-static {}, LX/9Tt;->a()LX/9Ts;

    move-result-object v3

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1498010
    iget-object v2, p0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->e:LX/1Ck;

    const-string v3, "fetchGroups"

    new-instance v4, LX/9UO;

    invoke-direct {v4, p0}, LX/9UO;-><init>(Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1498011
    const/16 v1, 0x2b

    const v2, 0x4abc66d6    # 6173547.0f

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
