.class public Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field private p:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1497628
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1497627
    const-string v0, "select_friends_view"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1497618
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1497619
    if-nez p1, :cond_0

    .line 1497620
    const v0, 0x7f0824ab

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/9UY;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1497621
    new-instance v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-direct {v0}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    .line 1497622
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1a78

    iget-object v2, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1497623
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    new-instance v1, LX/9U8;

    invoke-direct {v1, p0}, LX/9U8;-><init>(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;)V

    .line 1497624
    iput-object v1, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->l:LX/9U7;

    .line 1497625
    return-void

    .line 1497626
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1a78

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iput-object v0, p0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->p:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1497613
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->setResult(I)V

    .line 1497614
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1a78

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    .line 1497615
    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->e:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1497616
    invoke-virtual {p0}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;->finish()V

    .line 1497617
    return-void
.end method
