.class public final Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1405727
    const-class v0, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    new-instance v1, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1405728
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1405729
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1405730
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1405731
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1405732
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1405733
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1405734
    if-eqz v2, :cond_5

    .line 1405735
    const-string p0, "translatability_for_viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1405736
    const/4 p2, 0x4

    .line 1405737
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1405738
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1405739
    if-eqz p0, :cond_0

    .line 1405740
    const-string v0, "source_dialect"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1405741
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1405742
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1405743
    if-eqz p0, :cond_1

    .line 1405744
    const-string v0, "source_dialect_name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1405745
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1405746
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1405747
    if-eqz p0, :cond_2

    .line 1405748
    const-string v0, "target_dialect"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1405749
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1405750
    :cond_2
    const/4 p0, 0x3

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1405751
    if-eqz p0, :cond_3

    .line 1405752
    const-string v0, "target_dialect_name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1405753
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1405754
    :cond_3
    invoke-virtual {v1, v2, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1405755
    if-eqz p0, :cond_4

    .line 1405756
    const-string p0, "translation_type"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1405757
    invoke-virtual {v1, v2, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1405758
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1405759
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1405760
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1405761
    check-cast p1, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Serializer;->a(Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
