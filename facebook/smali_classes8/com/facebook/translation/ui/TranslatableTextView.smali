.class public Lcom/facebook/translation/ui/TranslatableTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/0wO;


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/String;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Landroid/view/View;

.field private f:Ljava/lang/CharSequence;

.field private g:Ljava/lang/CharSequence;

.field private h:Landroid/content/res/Resources;

.field private i:LX/2da;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1406080
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1406081
    invoke-direct {p0, p1}, Lcom/facebook/translation/ui/TranslatableTextView;->a(Landroid/content/Context;)V

    .line 1406082
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1406104
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1406105
    invoke-direct {p0, p1}, Lcom/facebook/translation/ui/TranslatableTextView;->a(Landroid/content/Context;)V

    .line 1406106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1406101
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1406102
    invoke-direct {p0, p1}, Lcom/facebook/translation/ui/TranslatableTextView;->a(Landroid/content/Context;)V

    .line 1406103
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Landroid/text/Spannable;
    .locals 7

    .prologue
    const/16 v6, 0x21

    const/4 v5, 0x0

    .line 1406095
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->g:Ljava/lang/CharSequence;

    .line 1406096
    :goto_0
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    .line 1406097
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, Lcom/facebook/translation/ui/TranslatableTextView;->h:Landroid/content/res/Resources;

    const v4, 0x7f0a0042

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-interface {v1, v2, v5, v3, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1406098
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    iget-object v3, p0, Lcom/facebook/translation/ui/TranslatableTextView;->h:Landroid/content/res/Resources;

    const v4, 0x7f0b0ae0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-interface {v1, v2, v5, v0, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1406099
    return-object v1

    .line 1406100
    :cond_0
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->f:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1406083
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->h:Landroid/content/res/Resources;

    .line 1406084
    const v0, 0x7f081204

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->c:Ljava/lang/String;

    .line 1406085
    const v0, 0x7f081202

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->f:Ljava/lang/CharSequence;

    .line 1406086
    const v0, 0x7f081203

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->g:Ljava/lang/CharSequence;

    .line 1406087
    const v0, 0x7f031520

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1406088
    invoke-virtual {p0, v2}, Lcom/facebook/translation/ui/TranslatableTextView;->setOrientation(I)V

    .line 1406089
    const v0, 0x7f0d0618

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1406090
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1406091
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/8pZ;

    invoke-direct {v1, p0}, LX/8pZ;-><init>(Lcom/facebook/translation/ui/TranslatableTextView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1406092
    new-instance v0, LX/2da;

    invoke-direct {v0, p0}, LX/2da;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->i:LX/2da;

    .line 1406093
    iput-boolean v2, p0, Lcom/facebook/translation/ui/TranslatableTextView;->j:Z

    .line 1406094
    return-void
.end method

.method private b(Ljava/lang/CharSequence;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V
    .locals 2

    .prologue
    .line 1406047
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1406048
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1406049
    invoke-direct {p0, p2}, Lcom/facebook/translation/ui/TranslatableTextView;->a(Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1406050
    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->b:Ljava/lang/CharSequence;

    .line 1406051
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1406074
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 1406075
    :goto_0
    return-void

    .line 1406076
    :cond_0
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1406077
    const v0, 0x7f0d2faf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->e:Landroid/view/View;

    .line 1406078
    :cond_1
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/translation/ui/TranslatableTextView;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1406079
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1406071
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1406072
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1406073
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1406065
    invoke-direct {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->e()V

    .line 1406066
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 1406067
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0a0042

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/translation/ui/TranslatableTextView;->c:Ljava/lang/String;

    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1406068
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/facebook/translation/ui/TranslatableTextView;->a:Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1406069
    iget-object v1, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1406070
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Z
    .locals 1

    .prologue
    .line 1406058
    invoke-direct {p0, p1, p2}, Lcom/facebook/translation/ui/TranslatableTextView;->b(Ljava/lang/CharSequence;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V

    .line 1406059
    invoke-virtual {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406060
    invoke-virtual {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->b()V

    .line 1406061
    const/4 v0, 0x0

    .line 1406062
    :goto_0
    return v0

    .line 1406063
    :cond_0
    invoke-direct {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->d()V

    .line 1406064
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1406055
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/translation/ui/TranslatableTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1406056
    invoke-direct {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->e()V

    .line 1406057
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1406052
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 1406053
    const/4 v0, 0x0

    .line 1406054
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/translation/ui/TranslatableTextView;->b:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1406107
    iget-boolean v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->i:LX/2da;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406108
    const/4 v0, 0x1

    .line 1406109
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1406034
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getLayout()Landroid/text/Layout;
    .locals 1

    .prologue
    .line 1406033
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1406032
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 1406031
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getTextSize()F

    move-result v0

    return v0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1406030
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getTotalPaddingLeft()I
    .locals 1

    .prologue
    .line 1406029
    invoke-virtual {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public getTotalPaddingTop()I
    .locals 1

    .prologue
    .line 1406035
    invoke-virtual {p0}, Lcom/facebook/translation/ui/TranslatableTextView;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1406036
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1406037
    return-void
.end method

.method public final setMovementMethod(Landroid/text/method/MovementMethod;)V
    .locals 1

    .prologue
    .line 1406038
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1406039
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1406040
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1406041
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1406042
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1406043
    return-void
.end method

.method public setUntranslatedText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1406044
    iput-object p1, p0, Lcom/facebook/translation/ui/TranslatableTextView;->a:Ljava/lang/CharSequence;

    .line 1406045
    iget-object v0, p0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/translation/ui/TranslatableTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1406046
    return-void
.end method
