.class public final Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1405673
    const-class v0, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    new-instance v1, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1405674
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1405675
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1405676
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1405677
    const/4 v2, 0x0

    .line 1405678
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1405679
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1405680
    :goto_0
    move v1, v2

    .line 1405681
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1405682
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1405683
    new-instance v1, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;-><init>()V

    .line 1405684
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1405685
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1405686
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1405687
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1405688
    :cond_0
    return-object v1

    .line 1405689
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1405690
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1405691
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1405692
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1405693
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1405694
    const-string v4, "translatability_for_viewer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1405695
    const/4 v3, 0x0

    .line 1405696
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1405697
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1405698
    :goto_2
    move v1, v3

    .line 1405699
    goto :goto_1

    .line 1405700
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1405701
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1405702
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1405703
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1405704
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_b

    .line 1405705
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1405706
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1405707
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v8, :cond_6

    .line 1405708
    const-string p0, "source_dialect"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1405709
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_3

    .line 1405710
    :cond_7
    const-string p0, "source_dialect_name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1405711
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1405712
    :cond_8
    const-string p0, "target_dialect"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1405713
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1405714
    :cond_9
    const-string p0, "target_dialect_name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1405715
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1405716
    :cond_a
    const-string p0, "translation_type"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1405717
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_3

    .line 1405718
    :cond_b
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1405719
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1405720
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1405721
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1405722
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1405723
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1405724
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_c
    move v1, v3

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto/16 :goto_3
.end method
