.class public Lcom/facebook/userfilter/UserSearchService;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:LX/11H;

.field private final b:LX/7Ba;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7Ba;LX/0Or;Ljava/lang/String;)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/7Ba;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1412685
    iput-object p2, p0, Lcom/facebook/userfilter/UserSearchService;->b:LX/7Ba;

    .line 1412686
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iput-object v0, p0, Lcom/facebook/userfilter/UserSearchService;->a:LX/11H;

    .line 1412687
    iput-object p4, p0, Lcom/facebook/userfilter/UserSearchService;->c:Ljava/lang/String;

    .line 1412688
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b188f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/userfilter/UserSearchService;->d:I

    .line 1412689
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/userfilter/UserSearchService;->e:Ljava/util/Set;

    .line 1412690
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/userfilter/UserSearchService;->f:Ljava/util/Set;

    .line 1412691
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1412692
    new-instance v0, LX/7BS;

    invoke-direct {v0}, LX/7BS;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    .line 1412693
    iput-object v1, v0, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1412694
    move-object v0, v0

    .line 1412695
    sget-object v1, LX/7BK;->USER:LX/7BK;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1412696
    iput-object v1, v0, LX/7BS;->d:Ljava/util/List;

    .line 1412697
    move-object v0, v0

    .line 1412698
    const/16 v1, 0x14

    .line 1412699
    iput v1, v0, LX/7BS;->e:I

    .line 1412700
    move-object v0, v0

    .line 1412701
    iget v1, p0, Lcom/facebook/userfilter/UserSearchService;->d:I

    .line 1412702
    iput v1, v0, LX/7BS;->c:I

    .line 1412703
    move-object v0, v0

    .line 1412704
    invoke-virtual {v0}, LX/7BS;->f()Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    move-result-object v0

    move-object v0, v0

    .line 1412705
    iget-object v1, p0, Lcom/facebook/userfilter/UserSearchService;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/userfilter/UserSearchService;->b:LX/7Ba;

    const-class v3, LX/8tE;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Hc;

    .line 1412706
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1412707
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v3, v1

    .line 1412708
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 1412709
    new-instance v6, LX/0XI;

    invoke-direct {v6}, LX/0XI;-><init>()V

    iget-object v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 1412710
    iput-object v7, v6, LX/0XI;->h:Ljava/lang/String;

    .line 1412711
    move-object v6, v6

    .line 1412712
    iget-object v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    const/4 v11, 0x1

    .line 1412713
    if-nez v7, :cond_2

    .line 1412714
    invoke-static {}, Lcom/facebook/user/model/Name;->k()Lcom/facebook/user/model/Name;

    move-result-object v8

    .line 1412715
    :goto_1
    move-object v7, v8

    .line 1412716
    iput-object v7, v6, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1412717
    move-object v6, v6

    .line 1412718
    iget-object v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1412719
    iput-object v7, v6, LX/0XI;->n:Ljava/lang/String;

    .line 1412720
    move-object v6, v6

    .line 1412721
    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    iget-wide v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v6

    invoke-virtual {v6}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v6

    move-object v0, v6

    .line 1412722
    iget-object v5, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1412723
    iget-object v6, p0, Lcom/facebook/userfilter/UserSearchService;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    move v5, v5

    .line 1412724
    if-nez v5, :cond_4

    .line 1412725
    iget-object v5, p0, Lcom/facebook/userfilter/UserSearchService;->f:Ljava/util/Set;

    .line 1412726
    iget-object v6, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1412727
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    move v5, v5

    .line 1412728
    if-nez v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 1412729
    if-eqz v5, :cond_0

    .line 1412730
    iget-object v5, p0, Lcom/facebook/userfilter/UserSearchService;->e:Ljava/util/Set;

    .line 1412731
    iget-object v6, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1412732
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_3
    move v5, v5

    .line 1412733
    new-instance v6, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v6, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1412734
    iput-boolean v5, v6, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 1412735
    move-object v0, v6

    .line 1412736
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1412737
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1412738
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1412739
    :cond_2
    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1412740
    array-length v8, v9

    if-le v8, v11, :cond_3

    .line 1412741
    new-instance v8, Lcom/facebook/user/model/Name;

    const/4 v10, 0x0

    aget-object v10, v9, v10

    aget-object v9, v9, v11

    invoke-direct {v8, v10, v9}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1412742
    :cond_3
    new-instance v8, Lcom/facebook/user/model/Name;

    invoke-direct {v8, v7}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_3
.end method
