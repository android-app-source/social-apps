.class public Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;
.super LX/998;
.source ""

# interfaces
.implements LX/4Ba;
.implements LX/39D;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/1wz;

.field private c:Landroid/os/Handler;

.field private d:LX/31M;

.field public e:LX/994;

.field public f:LX/995;

.field public g:LX/992;

.field public h:LX/993;

.field private final i:Ljava/lang/Runnable;

.field private j:LX/997;

.field private k:LX/996;

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1446893
    const-class v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    sput-object v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1446809
    invoke-direct {p0, p1}, LX/998;-><init>(Landroid/content/Context;)V

    .line 1446810
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446811
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    .line 1446812
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->d:LX/31M;

    .line 1446813
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 1446814
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 1446815
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 1446816
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    .line 1446817
    new-instance v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout$1;-><init>(Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;)V

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i:Ljava/lang/Runnable;

    .line 1446818
    sget-object v0, LX/997;->AT_REST:LX/997;

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446819
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446820
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 1446821
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1446822
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1446823
    invoke-direct {p0, p1, p2}, LX/998;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1446824
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446825
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    .line 1446826
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->d:LX/31M;

    .line 1446827
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 1446828
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 1446829
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 1446830
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    .line 1446831
    new-instance v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout$1;-><init>(Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;)V

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i:Ljava/lang/Runnable;

    .line 1446832
    sget-object v0, LX/997;->AT_REST:LX/997;

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446833
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446834
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 1446835
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1446836
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1446837
    invoke-direct {p0, p1, p2, p3}, LX/998;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1446838
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446839
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    .line 1446840
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->d:LX/31M;

    .line 1446841
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 1446842
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 1446843
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 1446844
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    .line 1446845
    new-instance v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout$1;-><init>(Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;)V

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i:Ljava/lang/Runnable;

    .line 1446846
    sget-object v0, LX/997;->AT_REST:LX/997;

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446847
    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446848
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 1446849
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1446850
    return-void
.end method

.method private a(LX/997;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/997;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1446851
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    if-ne v1, p1, :cond_1

    .line 1446852
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1446853
    :cond_0
    :goto_0
    return-object v0

    .line 1446854
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    if-eqz v1, :cond_3

    .line 1446855
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446856
    iget-object v2, v1, LX/996;->a:LX/997;

    move-object v1, v2

    .line 1446857
    if-ne v1, p1, :cond_2

    .line 1446858
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446859
    iget-object v1, v0, LX/996;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v1

    .line 1446860
    goto :goto_0

    .line 1446861
    :cond_2
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    invoke-virtual {v1}, LX/996;->d()V

    .line 1446862
    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446863
    :cond_3
    invoke-virtual {p0}, LX/998;->g()Z

    .line 1446864
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-virtual {v1}, LX/1wz;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1446865
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-virtual {v1}, LX/1wz;->c()V

    goto :goto_0
.end method

.method private a(LX/31M;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1446866
    sget-object v0, LX/991;->a:[I

    invoke-virtual {p1}, LX/31M;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 1446867
    :goto_0
    invoke-virtual {p0, v0, v1}, LX/998;->a(II)V

    .line 1446868
    iget-object v2, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    if-eqz v2, :cond_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 1446869
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    invoke-interface {v0}, LX/993;->a()V

    .line 1446870
    :cond_1
    return-void

    .line 1446871
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    :goto_1
    move v3, v0

    move v0, v1

    move v1, v3

    .line 1446872
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1446873
    goto :goto_1

    .line 1446874
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v0

    if-gez v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    :goto_2
    move v3, v0

    move v0, v1

    move v1, v3

    .line 1446875
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1446876
    goto :goto_2

    .line 1446877
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 1446878
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v0

    if-gez v0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    neg-int v0, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 1446879
    const-class v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-static {v0, p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1446880
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446881
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 1446882
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446883
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1446884
    if-eqz p2, :cond_0

    .line 1446885
    sget-object v0, LX/03r;->DismissibleFrameLayout:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1446886
    const/16 v1, 0x0

    sget-object v2, LX/31M;->RIGHT:LX/31M;

    invoke-virtual {v2}, LX/31M;->flag()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1446887
    const/16 v2, 0x1

    sget-object v3, LX/1x0;->HORIZONTAL:LX/1x0;

    invoke-virtual {v3}, LX/1x0;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 1446888
    iget-object v3, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-static {}, LX/1x0;->values()[LX/1x0;

    move-result-object v4

    aget-object v2, v4, v2

    .line 1446889
    iput-object v2, v3, LX/1wz;->h:LX/1x0;

    .line 1446890
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1446891
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->setDirectionFlags(I)V

    .line 1446892
    :cond_0
    return-void
.end method

.method private a(Landroid/os/Handler;LX/1wz;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446797
    iput-object p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    .line 1446798
    iput-object p2, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446799
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-static {v1}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object v1

    check-cast v1, LX/1wz;

    invoke-direct {p0, v0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Landroid/os/Handler;LX/1wz;)V

    return-void
.end method

.method private a(Landroid/view/View;ZIIII)Z
    .locals 11

    .prologue
    .line 1446894
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v7, p1

    .line 1446895
    check-cast v7, Landroid/view/ViewGroup;

    .line 1446896
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v9

    .line 1446897
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v10

    .line 1446898
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1446899
    add-int/lit8 v0, v0, -0x1

    move v8, v0

    :goto_0
    if-ltz v8, :cond_1

    .line 1446900
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1446901
    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt v0, v2, :cond_0

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge v0, v2, :cond_0

    add-int v0, p6, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_0

    add-int v0, p6, v10

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v5, v0, v3

    add-int v0, p6, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v6, v0, v3

    move-object v0, p0

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Landroid/view/View;ZIIII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446902
    const/4 v0, 0x1

    .line 1446903
    :goto_1
    return v0

    .line 1446904
    :cond_0
    add-int/lit8 v0, v8, -0x1

    move v8, v0

    goto :goto_0

    .line 1446905
    :cond_1
    if-nez p2, :cond_2

    .line 1446906
    const/4 v0, 0x0

    goto :goto_1

    .line 1446907
    :cond_2
    if-eqz p3, :cond_3

    .line 1446908
    invoke-static {p1, p3}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v0

    goto :goto_1

    .line 1446909
    :cond_3
    if-eqz p4, :cond_4

    .line 1446910
    invoke-static {p1, p4}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    goto :goto_1

    .line 1446911
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(LX/997;)V
    .locals 2

    .prologue
    .line 1446912
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446913
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446914
    if-eqz v0, :cond_0

    .line 1446915
    iget-object v1, v0, LX/996;->a:LX/997;

    move-object v1, v1

    .line 1446916
    if-ne v1, p1, :cond_1

    .line 1446917
    invoke-virtual {v0}, LX/996;->c()V

    .line 1446918
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446919
    return-void

    .line 1446920
    :cond_1
    invoke-virtual {v0}, LX/996;->d()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 6

    .prologue
    .line 1446921
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    .line 1446922
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v1

    .line 1446923
    if-lez v0, :cond_0

    if-gtz v1, :cond_1

    .line 1446924
    :cond_0
    :goto_0
    return-void

    .line 1446925
    :cond_1
    iget-object v2, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446926
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v3

    .line 1446927
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v4

    .line 1446928
    iget-object v5, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-virtual {v5}, LX/1wz;->b()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {p0}, LX/998;->f()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1446929
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v5, v0, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lt v0, v1, :cond_3

    .line 1446930
    :cond_2
    sget-object v0, LX/997;->DISMISSED:LX/997;

    if-eq v2, v0, :cond_0

    .line 1446931
    sget-object v0, LX/997;->DISMISSED:LX/997;

    invoke-direct {p0, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b(LX/997;)V

    .line 1446932
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->m()V

    goto :goto_0

    .line 1446933
    :cond_3
    if-nez v3, :cond_4

    if-nez v4, :cond_4

    .line 1446934
    sget-object v0, LX/997;->AT_REST:LX/997;

    if-eq v2, v0, :cond_0

    .line 1446935
    sget-object v0, LX/997;->AT_REST:LX/997;

    invoke-direct {p0, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b(LX/997;)V

    .line 1446936
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k()V

    goto :goto_0

    .line 1446937
    :cond_4
    sget-object v0, LX/997;->ANIMATING:LX/997;

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446938
    if-eqz p1, :cond_5

    .line 1446939
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i()V

    goto :goto_0

    .line 1446940
    :cond_5
    invoke-static {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j(Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;)V

    goto :goto_0
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1446941
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v2

    .line 1446942
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v3

    .line 1446943
    if-eqz v2, :cond_1

    .line 1446944
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    .line 1446945
    div-int/lit8 v3, v0, 0x2

    .line 1446946
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v4, v3, :cond_3

    .line 1446947
    if-gez v2, :cond_0

    neg-int v0, v0

    .line 1446948
    :cond_0
    :goto_0
    invoke-virtual {p0, v0, v1}, LX/998;->a(II)V

    .line 1446949
    return-void

    .line 1446950
    :cond_1
    if-eqz v3, :cond_3

    .line 1446951
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    .line 1446952
    div-int/lit8 v2, v0, 0x2

    .line 1446953
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v4, v2, :cond_3

    .line 1446954
    if-gez v3, :cond_2

    neg-int v0, v0

    :cond_2
    move v5, v0

    move v0, v1

    move v1, v5

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1446955
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i:Ljava/lang/Runnable;

    const v2, -0x53073b5e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1446956
    return-void
.end method

.method public static j(Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;)V
    .locals 4

    .prologue
    .line 1446957
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    if-nez v0, :cond_1

    .line 1446958
    :cond_0
    :goto_0
    return-void

    .line 1446959
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    .line 1446960
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v1

    .line 1446961
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1446962
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v2

    .line 1446963
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v3

    .line 1446964
    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 1446965
    int-to-float v2, v3

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 1446966
    iget-object v2, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    invoke-interface {v2, v0, v1}, LX/992;->a(FF)V

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1446967
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    if-eqz v0, :cond_0

    .line 1446968
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    invoke-interface {v0}, LX/995;->d()V

    .line 1446969
    :cond_0
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1446970
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    if-eqz v0, :cond_0

    .line 1446971
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    invoke-interface {v0}, LX/994;->b()V

    .line 1446972
    :cond_0
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1446973
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    if-eqz v0, :cond_1

    .line 1446974
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    invoke-interface {v0}, LX/994;->c()V

    .line 1446975
    :cond_0
    :goto_0
    return-void

    .line 1446976
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1446977
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 1446978
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/31M;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/31M;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1446979
    sget-object v0, LX/997;->DISMISSED:LX/997;

    invoke-direct {p0, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(LX/997;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1446980
    if-eqz v0, :cond_0

    .line 1446981
    :goto_0
    return-object v0

    .line 1446982
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v0

    .line 1446983
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v2

    .line 1446984
    if-eqz v0, :cond_1

    .line 1446985
    if-ltz v0, :cond_4

    .line 1446986
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    move v2, v0

    move v0, v1

    .line 1446987
    :goto_1
    new-instance v3, LX/996;

    sget-object v4, LX/997;->DISMISSED:LX/997;

    invoke-direct {v3, v4}, LX/996;-><init>(LX/997;)V

    iput-object v3, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446988
    if-eqz p2, :cond_5

    .line 1446989
    invoke-virtual {p0, v2, v0}, LX/998;->a(II)V

    .line 1446990
    :goto_2
    iget-object v0, v3, LX/996;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v0

    .line 1446991
    goto :goto_0

    .line 1446992
    :cond_1
    if-eqz v2, :cond_3

    .line 1446993
    if-gez v2, :cond_2

    .line 1446994
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    move v2, v1

    goto :goto_1

    .line 1446995
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    move v2, v1

    goto :goto_1

    .line 1446996
    :cond_3
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l()V

    .line 1446997
    sget-object v0, LX/991;->a:[I

    invoke-virtual {p1}, LX/31M;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1446998
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    move v2, v1

    .line 1446999
    goto :goto_1

    .line 1447000
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    move v2, v1

    .line 1447001
    goto :goto_1

    .line 1447002
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    move v2, v0

    move v0, v1

    .line 1447003
    goto :goto_1

    .line 1447004
    :cond_4
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v0

    neg-int v0, v0

    move v2, v0

    move v0, v1

    goto :goto_1

    .line 1447005
    :cond_5
    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(IIZ)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1446800
    sget-object v0, LX/997;->AT_REST:LX/997;

    invoke-direct {p0, v0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(LX/997;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1446801
    if-eqz v0, :cond_0

    .line 1446802
    :goto_0
    return-object v0

    .line 1446803
    :cond_0
    new-instance v0, LX/996;

    sget-object v1, LX/997;->AT_REST:LX/997;

    invoke-direct {v0, v1}, LX/996;-><init>(LX/997;)V

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k:LX/996;

    .line 1446804
    if-eqz p1, :cond_1

    .line 1446805
    invoke-virtual {p0, v2, v2}, LX/998;->a(II)V

    .line 1446806
    :goto_1
    iget-object v1, v0, LX/996;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v1

    .line 1446807
    goto :goto_0

    .line 1446808
    :cond_1
    invoke-virtual {p0, v2, v2, v2}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(IIZ)V

    goto :goto_1
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1446769
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h()V

    .line 1446770
    return-void
.end method

.method public final a(IIZ)V
    .locals 0

    .prologue
    .line 1446766
    invoke-super {p0, p1, p2, p3}, LX/998;->a(IIZ)V

    .line 1446767
    invoke-direct {p0, p3}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b(Z)V

    .line 1446768
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1446760
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446761
    iget p2, v0, LX/1wz;->p:I

    move v0, p2

    .line 1446762
    invoke-virtual {p1, v0}, LX/31M;->isSetInFlags(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446763
    invoke-direct {p0, p1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(LX/31M;)V

    .line 1446764
    :goto_0
    return-void

    .line 1446765
    :cond_0
    invoke-virtual {p0, v1, v1}, LX/998;->a(II)V

    goto :goto_0
.end method

.method public final a(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1446748
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v2

    .line 1446749
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v3

    .line 1446750
    if-gez v2, :cond_2

    .line 1446751
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_1

    .line 1446752
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1446753
    goto :goto_0

    .line 1446754
    :cond_2
    if-lez v2, :cond_3

    .line 1446755
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getWidth()I

    move-result v3

    sub-int v2, v3, v2

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1446756
    :cond_3
    if-gez v3, :cond_4

    .line 1446757
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v2, p2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1446758
    :cond_4
    if-lez v3, :cond_0

    .line 1446759
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getHeight()I

    move-result v2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final a(FFLX/31M;)Z
    .locals 8

    .prologue
    const/4 v0, -0x1

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1446738
    invoke-virtual {p3}, LX/31M;->isXAxis()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/31M;->LEFT:LX/31M;

    if-ne p3, v1, :cond_0

    move v3, v0

    .line 1446739
    :goto_0
    invoke-virtual {p3}, LX/31M;->isYAxis()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/31M;->UP:LX/31M;

    if-ne p3, v1, :cond_2

    move v4, v0

    .line 1446740
    :goto_1
    float-to-int v5, p1

    float-to-int v6, p2

    move-object v0, p0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Landroid/view/View;ZIIII)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1446741
    :goto_2
    return v2

    :cond_0
    move v3, v7

    .line 1446742
    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_0

    :cond_2
    move v4, v7

    .line 1446743
    goto :goto_1

    :cond_3
    move v4, v2

    goto :goto_1

    .line 1446744
    :cond_4
    iput-object p3, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->d:LX/31M;

    .line 1446745
    invoke-virtual {p0}, LX/998;->g()Z

    .line 1446746
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l()V

    move v2, v7

    .line 1446747
    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1446713
    sget-object v0, LX/997;->AT_REST:LX/997;

    iput-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    .line 1446714
    invoke-direct {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->k()V

    .line 1446715
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1446716
    invoke-virtual {p3}, LX/31M;->isXAxis()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446717
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollX()I

    move-result v0

    float-to-int v2, p1

    sub-int/2addr v0, v2

    .line 1446718
    sget-object v2, LX/991;->a:[I

    iget-object v3, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->d:LX/31M;

    invoke-virtual {v3}, LX/31M;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v2, v0

    move v0, v1

    .line 1446719
    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(IIZ)V

    .line 1446720
    return-void

    .line 1446721
    :pswitch_0
    if-gez v0, :cond_2

    move v0, v1

    move v2, v1

    .line 1446722
    goto :goto_0

    .line 1446723
    :pswitch_1
    if-lez v0, :cond_2

    move v0, v1

    move v2, v1

    .line 1446724
    goto :goto_0

    .line 1446725
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v0

    float-to-int v2, p2

    sub-int/2addr v0, v2

    .line 1446726
    sget-object v2, LX/991;->a:[I

    iget-object v3, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->d:LX/31M;

    invoke-virtual {v3}, LX/31M;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :cond_1
    move v2, v1

    goto :goto_0

    .line 1446727
    :pswitch_2
    if-gez v0, :cond_1

    move v0, v1

    move v2, v1

    .line 1446728
    goto :goto_0

    .line 1446729
    :pswitch_3
    if-lez v0, :cond_1

    move v0, v1

    move v2, v1

    .line 1446730
    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 1446732
    invoke-virtual {p0}, LX/998;->f()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1446733
    invoke-virtual {p0}, LX/998;->g()Z

    .line 1446734
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1446735
    return-void
.end method

.method public getCurrentState()LX/997;
    .locals 1

    .prologue
    .line 1446731
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->j:LX/997;

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x14164c23

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1446771
    iget-object v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->c:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->i:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1446772
    invoke-super {p0}, LX/998;->onDetachedFromWindow()V

    .line 1446773
    const/16 v1, 0x2d

    const v2, 0x71cdf41b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1446774
    iget-boolean v1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    if-nez v1, :cond_1

    .line 1446775
    :cond_0
    :goto_0
    return v0

    .line 1446776
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 1446777
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    const v1, 0x53da6cce

    invoke-static {v3, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1446778
    iget-boolean v2, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    if-nez v2, :cond_0

    .line 1446779
    const v2, -0x42612d6d

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1446780
    :goto_0
    return v0

    .line 1446781
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v4, :cond_1

    .line 1446782
    const v2, 0x23d10de8

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1446783
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x4b688319    # 1.5237913E7f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAnimationListener(LX/992;)V
    .locals 0

    .prologue
    .line 1446784
    iput-object p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 1446785
    return-void
.end method

.method public setDirectionFlags(I)V
    .locals 1

    .prologue
    .line 1446786
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    .line 1446787
    iput p1, v0, LX/1wz;->p:I

    .line 1446788
    return-void
.end method

.method public varargs setDirections([LX/31M;)V
    .locals 1

    .prologue
    .line 1446789
    iget-object v0, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a([LX/31M;)V

    .line 1446790
    return-void
.end method

.method public setDraggingEnabled(Z)V
    .locals 0

    .prologue
    .line 1446791
    iput-boolean p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 1446792
    return-void
.end method

.method public setFlingListener(LX/993;)V
    .locals 0

    .prologue
    .line 1446793
    iput-object p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    .line 1446794
    return-void
.end method

.method public setOnDismissListener(LX/994;)V
    .locals 0

    .prologue
    .line 1446736
    iput-object p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 1446737
    return-void
.end method

.method public setOnResetListener(LX/995;)V
    .locals 0

    .prologue
    .line 1446795
    iput-object p1, p0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 1446796
    return-void
.end method
