.class public Lcom/facebook/fbui/drawable/NetworkDrawable;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Landroid/graphics/Paint;


# instance fields
.field public b:LX/999;

.field private c:Z

.field private d:Z

.field public e:LX/99A;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/36B;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1447131
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->a:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(LX/1HI;Ljava/util/concurrent/Executor;ILX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HI;",
            "Ljava/util/concurrent/Executor;",
            "I",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1447070
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1447071
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->f:Ljava/util/List;

    .line 1447072
    new-instance v0, LX/999;

    invoke-direct {v0, p3, p1, p2, p4}, LX/999;-><init>(ILX/1HI;Ljava/util/concurrent/Executor;LX/0Ot;)V

    iput-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447073
    return-void
.end method

.method public constructor <init>(LX/999;)V
    .locals 1

    .prologue
    .line 1447132
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1447133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->f:Ljava/util/List;

    .line 1447134
    iput-object p1, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447135
    return-void
.end method

.method private b()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 1447136
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    iget-object v0, v0, LX/999;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->a:Landroid/graphics/Paint;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    iget-object v0, v0, LX/999;->a:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method public static d(Lcom/facebook/fbui/drawable/NetworkDrawable;)V
    .locals 3

    .prologue
    .line 1447169
    iget-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->d:Z

    if-nez v0, :cond_0

    .line 1447170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->d:Z

    .line 1447171
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447172
    iget-object v1, v0, LX/999;->b:LX/99D;

    .line 1447173
    iget v2, v1, LX/99D;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, LX/99D;->k:I

    .line 1447174
    iget-object v2, v1, LX/99D;->l:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    iget-object v2, v1, LX/99D;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1447175
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 1447176
    iget-object v0, v1, LX/99D;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1447177
    iget-object v2, v1, LX/99D;->l:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    iget-object v2, v1, LX/99D;->n:LX/1ca;

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1447178
    if-eqz v2, :cond_0

    .line 1447179
    iget-object v2, v1, LX/99D;->b:LX/1HI;

    iget-object v0, v1, LX/99D;->i:LX/1bf;

    iget-object p0, v1, LX/99D;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, p0}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v2

    iput-object v2, v1, LX/99D;->n:LX/1ca;

    .line 1447180
    iget-object v2, v1, LX/99D;->n:LX/1ca;

    iget-object v0, v1, LX/99D;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v1, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1447181
    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/fbui/drawable/NetworkDrawable;)V
    .locals 4

    .prologue
    .line 1447137
    iget-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->d:Z

    if-eqz v0, :cond_3

    .line 1447138
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447139
    iget-object v1, v0, LX/999;->b:LX/99D;

    .line 1447140
    const/4 v3, 0x0

    .line 1447141
    iget v2, v1, LX/99D;->k:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, LX/99D;->k:I

    if-nez v2, :cond_0

    .line 1447142
    iget-object v2, v1, LX/99D;->l:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 1447143
    iget-object v2, v1, LX/99D;->m:LX/1FJ;

    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 1447144
    iput-object v3, v1, LX/99D;->m:LX/1FJ;

    .line 1447145
    iput-object v3, v1, LX/99D;->l:Landroid/graphics/Bitmap;

    .line 1447146
    :cond_0
    iget-object v2, v1, LX/99D;->e:Ljava/util/List;

    if-eqz v2, :cond_2

    .line 1447147
    const/4 v2, 0x0

    iget-object v3, v1, LX/99D;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_1

    .line 1447148
    iget-object v2, v1, LX/99D;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 1447149
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1447150
    iget-object v2, v1, LX/99D;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1447151
    :cond_1
    iget-object v2, v1, LX/99D;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1447152
    iget-object v2, v1, LX/99D;->n:LX/1ca;

    if-eqz v2, :cond_2

    .line 1447153
    iget-object v2, v1, LX/99D;->n:LX/1ca;

    invoke-interface {v2}, LX/1ca;->g()Z

    .line 1447154
    const/4 v2, 0x0

    iput-object v2, v1, LX/99D;->n:LX/1ca;

    .line 1447155
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->d:Z

    .line 1447156
    :cond_3
    return-void

    .line 1447157
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/36B;)V
    .locals 2

    .prologue
    .line 1447158
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {v0}, LX/999;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1447159
    if-nez v0, :cond_0

    .line 1447160
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->f:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1447161
    invoke-static {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->d(Lcom/facebook/fbui/drawable/NetworkDrawable;)V

    .line 1447162
    :goto_0
    return-void

    .line 1447163
    :cond_0
    invoke-interface {p1, v0}, LX/36B;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1447164
    invoke-static {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->d(Lcom/facebook/fbui/drawable/NetworkDrawable;)V

    .line 1447165
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {v0}, LX/999;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1447166
    if-eqz v0, :cond_0

    .line 1447167
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->b()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1447168
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1447121
    iget-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->d:Z

    if-eqz v0, :cond_0

    .line 1447122
    invoke-virtual {p0, v1, v1}, Lcom/facebook/fbui/drawable/NetworkDrawable;->setVisible(ZZ)Z

    .line 1447123
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NetworkDrawable with id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {v2}, LX/999;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447124
    iget-object v3, v2, LX/999;->b:LX/99D;

    iget-object v3, v3, LX/99D;->h:Ljava/lang/String;

    move-object v2, v3

    .line 1447125
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")  wasn\'t hidden before it was GC\'d. Please call setVisible(false, ___ )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in View.onDetachedFromWindow()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1447126
    iget-object v1, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447127
    iget-object v2, v1, LX/999;->b:LX/99D;

    iget-object v2, v2, LX/99D;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    move-object v1, v2

    .line 1447128
    const-string v2, "NetworkDrawableNotClosed"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1447129
    :cond_0
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 1447130
    invoke-direct {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->b()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 1447120
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1447117
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447118
    iget-object p0, v0, LX/999;->b:LX/99D;

    iget p0, p0, LX/99D;->g:I

    move v0, p0

    .line 1447119
    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1447114
    iget-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447115
    iget-object p0, v0, LX/999;->b:LX/99D;

    iget p0, p0, LX/99D;->f:I

    move v0, p0

    .line 1447116
    return v0
.end method

.method public final getOpacity()I
    .locals 3

    .prologue
    const/4 v0, -0x3

    .line 1447111
    iget-object v1, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {v1}, LX/999;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1447112
    if-nez v1, :cond_1

    .line 1447113
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->getAlpha()I

    move-result v1

    const/16 v2, 0xff

    if-lt v1, v2, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1447086
    sget-object v0, LX/03r;->NetworkDrawable:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1447087
    const/16 v0, 0x3

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1447088
    const/16 v0, 0x1

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 1447089
    const/16 v1, 0x0

    invoke-virtual {v2, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1447090
    const/16 v4, 0x2

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    .line 1447091
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1447092
    int-to-float v5, v5

    int-to-float v4, v4

    div-float v4, v5, v4

    .line 1447093
    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 1447094
    int-to-float v0, v1

    mul-float/2addr v0, v4

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v4, v0

    .line 1447095
    const/16 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1447096
    if-nez v0, :cond_0

    .line 1447097
    const-string v0, "unknown"

    .line 1447098
    :cond_0
    const/16 v1, 0x5

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1447099
    if-nez v1, :cond_1

    .line 1447100
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "network_drawable_"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {v6}, LX/999;->b()I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1447101
    :cond_1
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1447102
    const-class v2, Lcom/facebook/fbui/drawable/NetworkDrawable;

    invoke-static {v2, v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 1447103
    iget-object v1, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447104
    iget-object v2, v1, LX/999;->b:LX/99D;

    .line 1447105
    iput-object v3, v2, LX/99D;->h:Ljava/lang/String;

    .line 1447106
    iput v5, v2, LX/99D;->f:I

    .line 1447107
    iput v4, v2, LX/99D;->g:I

    .line 1447108
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    iput-object v1, v2, LX/99D;->i:LX/1bf;

    .line 1447109
    iput-object v0, v2, LX/99D;->j:Lcom/facebook/common/callercontext/CallerContext;

    .line 1447110
    return-void
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1447082
    iget-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->c:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1447083
    new-instance v0, LX/999;

    iget-object v1, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-direct {v0, v1}, LX/999;-><init>(LX/999;)V

    iput-object v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    .line 1447084
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/drawable/NetworkDrawable;->c:Z

    .line 1447085
    :cond_0
    return-object p0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1447080
    invoke-direct {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->b()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1447081
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1447078
    invoke-direct {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->b()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1447079
    return-void
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 1447074
    if-eqz p1, :cond_0

    .line 1447075
    invoke-static {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->d(Lcom/facebook/fbui/drawable/NetworkDrawable;)V

    .line 1447076
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    return v0

    .line 1447077
    :cond_0
    invoke-static {p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->e(Lcom/facebook/fbui/drawable/NetworkDrawable;)V

    goto :goto_0
.end method
