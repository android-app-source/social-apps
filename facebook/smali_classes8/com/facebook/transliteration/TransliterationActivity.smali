.class public Lcom/facebook/transliteration/TransliterationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/transliteration/TransliterationFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1624624
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1624627
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1624628
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 1624629
    const v0, 0x7f031523

    invoke-virtual {p0, v0}, Lcom/facebook/transliteration/TransliterationActivity;->setContentView(I)V

    .line 1624630
    const v0, 0x7f0d2fb3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1624631
    const v1, 0x7f081215

    invoke-virtual {p0, v1}, Lcom/facebook/transliteration/TransliterationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1624632
    new-instance v1, LX/A6f;

    invoke-direct {v1, p0}, LX/A6f;-><init>(Lcom/facebook/transliteration/TransliterationActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1624633
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f081212

    invoke-virtual {p0, v2}, Lcom/facebook/transliteration/TransliterationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1624634
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1624635
    move-object v1, v1

    .line 1624636
    const/4 v2, -0x2

    .line 1624637
    iput v2, v1, LX/108;->h:I

    .line 1624638
    move-object v1, v1

    .line 1624639
    const/4 v2, 0x1

    .line 1624640
    iput-boolean v2, v1, LX/108;->q:Z

    .line 1624641
    move-object v1, v1

    .line 1624642
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1624643
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1624644
    new-instance v1, LX/A6g;

    invoke-direct {v1, p0}, LX/A6g;-><init>(Lcom/facebook/transliteration/TransliterationActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1624645
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2fb4

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/transliteration/TransliterationFragment;

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationActivity;->p:Lcom/facebook/transliteration/TransliterationFragment;

    .line 1624646
    invoke-virtual {p0}, Lcom/facebook/transliteration/TransliterationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1624647
    const-string v1, "entry_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1624648
    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationActivity;->p:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v2, v1}, Lcom/facebook/transliteration/TransliterationFragment;->a(Ljava/lang/String;)V

    .line 1624649
    const-string v1, "composer_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1624650
    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationActivity;->p:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/transliteration/TransliterationFragment;->b(Ljava/lang/String;)V

    .line 1624651
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1624625
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationActivity;->p:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v0}, Lcom/facebook/transliteration/TransliterationFragment;->b()V

    .line 1624626
    return-void
.end method
