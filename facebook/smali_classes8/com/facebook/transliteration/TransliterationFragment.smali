.class public Lcom/facebook/transliteration/TransliterationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fk;


# instance fields
.field public a:Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Lcom/facebook/resources/ui/FbEditText;

.field private e:Landroid/text/TextWatcher;

.field public f:LX/A6e;

.field public g:LX/A6X;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/A6V;

.field public j:LX/A6P;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/A6N;

.field private m:I

.field private n:Z

.field private o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1624932
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private a(LX/A6o;LX/A6X;LX/0Or;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/A6o;",
            "LX/A6X;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1624914
    iput-object p2, p0, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    .line 1624915
    iput-object p3, p0, Lcom/facebook/transliteration/TransliterationFragment;->o:LX/0Or;

    .line 1624916
    sget-object v0, LX/A6N;->BIGRAM:LX/A6N;

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    .line 1624917
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->m:I

    .line 1624918
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->n:Z

    .line 1624919
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-boolean v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->n:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, LX/A6o;->a(LX/A6N;Ljava/lang/Boolean;Ljava/lang/Integer;)LX/A6e;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624920
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624921
    iget-object v1, v0, LX/A6e;->a:LX/A6N;

    sget-object v2, LX/A6N;->UNIGRAM:LX/A6N;

    if-ne v1, v2, :cond_0

    .line 1624922
    iget-object v1, v0, LX/A6e;->g:LX/0TD;

    new-instance v2, LX/A6Y;

    invoke-direct {v2, v0}, LX/A6Y;-><init>(LX/A6e;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1624923
    new-instance v2, LX/A6Z;

    invoke-direct {v2, v0}, LX/A6Z;-><init>(LX/A6e;)V

    iget-object p1, v0, LX/A6e;->g:LX/0TD;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1624924
    iget-object v1, v0, LX/A6e;->g:LX/0TD;

    new-instance v2, Lcom/facebook/transliteration/Transliteration$3;

    invoke-direct {v2, v0}, Lcom/facebook/transliteration/Transliteration$3;-><init>(LX/A6e;)V

    const p1, -0x5b561e6

    invoke-static {v1, v2, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1624925
    :cond_0
    iget-boolean v1, v0, LX/A6e;->e:Z

    if-eqz v1, :cond_1

    .line 1624926
    iget-object v1, v0, LX/A6e;->g:LX/0TD;

    new-instance v2, LX/A6c;

    invoke-direct {v2, v0}, LX/A6c;-><init>(LX/A6e;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1624927
    new-instance v2, LX/A6d;

    invoke-direct {v2, v0}, LX/A6d;-><init>(LX/A6e;)V

    iget-object p1, v0, LX/A6e;->g:LX/0TD;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1624928
    iget-object v1, v0, LX/A6e;->g:LX/0TD;

    new-instance v2, Lcom/facebook/transliteration/Transliteration$4;

    invoke-direct {v2, v0}, Lcom/facebook/transliteration/Transliteration$4;-><init>(LX/A6e;)V

    const p1, 0x25421354

    invoke-static {v1, v2, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1624929
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    .line 1624930
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->k:Ljava/lang/String;

    .line 1624931
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1624912
    new-instance v0, Lcom/facebook/transliteration/TransliterationFragment$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/transliteration/TransliterationFragment$4;-><init>(Lcom/facebook/transliteration/TransliterationFragment;Landroid/view/View;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1624913
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/transliteration/TransliterationFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/transliteration/TransliterationFragment;

    const-class v0, LX/A6o;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/A6o;

    invoke-static {v2}, LX/A6X;->a(LX/0QB;)LX/A6X;

    move-result-object v1

    check-cast v1, LX/A6X;

    const/16 v3, 0x122d

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/transliteration/TransliterationFragment;->a(LX/A6o;LX/A6X;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/transliteration/TransliterationFragment;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1624906
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1624907
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v2, p1, 0x1

    if-lt v0, v2, :cond_0

    .line 1624908
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1624909
    invoke-virtual {p0, v0, p1, v1}, Lcom/facebook/transliteration/TransliterationFragment;->a(Ljava/lang/String;IZ)V

    .line 1624910
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1624911
    goto :goto_0
.end method

.method public static c(Lcom/facebook/transliteration/TransliterationFragment;)I
    .locals 2

    .prologue
    .line 1624905
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getSelectionStart()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/transliteration/TransliterationFragment;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1624900
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getSelectionStart()I

    move-result v0

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1624901
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getSelectionEnd()I

    move-result v0

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1624902
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move-object v3, p1

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 1624903
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {p0, v0}, Lcom/facebook/transliteration/TransliterationFragment;->a(Landroid/view/View;)V

    .line 1624904
    return-void
.end method


# virtual methods
.method public final a(LX/A6Q;)V
    .locals 2

    .prologue
    .line 1624895
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1624896
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p1, LX/A6Q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1624897
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    iget v1, p1, LX/A6Q;->b:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 1624898
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1624899
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1624892
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1624893
    const-class v0, Lcom/facebook/transliteration/TransliterationFragment;

    invoke-static {v0, p0}, Lcom/facebook/transliteration/TransliterationFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1624894
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1624884
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624885
    iget p0, v2, LX/A6e;->h:I

    move v2, p0

    .line 1624886
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1624887
    const-string v4, "entry_point"

    invoke-interface {v3, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624888
    const-string v4, "algorithm"

    invoke-virtual {v1}, LX/A6N;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624889
    const-string v4, "version"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624890
    sget-object v4, LX/A6W;->OPENED:LX/A6W;

    iget-object v4, v4, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v0, v4, v3}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624891
    return-void
.end method

.method public final a(Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1624853
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1624854
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1624855
    if-nez p3, :cond_1

    .line 1624856
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    iget-object v1, v1, LX/A6P;->a:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1624857
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624858
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1624859
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624860
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1624861
    iget-object v2, v0, LX/A6e;->n:LX/A73;

    .line 1624862
    iget-object v5, v2, LX/A73;->b:LX/A72;

    invoke-virtual {v5, v1}, LX/A72;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    :goto_3
    move v2, v5

    .line 1624863
    if-nez v2, :cond_5

    .line 1624864
    iget v2, v0, LX/A6e;->f:I

    invoke-static {v0, v2, v1, p1, v4}, LX/A6e;->a(LX/A6e;ILjava/lang/String;Ljava/lang/String;I)V

    .line 1624865
    :cond_0
    :goto_4
    iget-object v2, v0, LX/A6e;->n:LX/A73;

    .line 1624866
    iget-object v3, v2, LX/A73;->b:LX/A72;

    invoke-virtual {v3, v1, p1}, LX/A72;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624867
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    iget-object v1, v1, LX/A6P;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624868
    iget v3, v2, LX/A6e;->h:I

    move v5, v3

    .line 1624869
    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, LX/A6X;->a(Ljava/lang/String;Ljava/lang/String;ILX/A6N;I)V

    .line 1624870
    :cond_1
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v0, v1}, LX/A6p;->a(Ljava/lang/String;LX/A6P;Ljava/lang/String;)LX/A6Q;

    move-result-object v0

    .line 1624871
    invoke-virtual {p0, v0}, Lcom/facebook/transliteration/TransliterationFragment;->a(LX/A6Q;)V

    .line 1624872
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1624873
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    invoke-virtual {v0}, LX/A6V;->a()V

    .line 1624874
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1624875
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 1624876
    return-void

    :cond_2
    move v0, v6

    .line 1624877
    goto/16 :goto_0

    :cond_3
    move v2, v4

    .line 1624878
    goto :goto_1

    :cond_4
    move v2, v4

    .line 1624879
    goto :goto_2

    .line 1624880
    :cond_5
    iget-object v2, v0, LX/A6e;->n:LX/A73;

    .line 1624881
    iget-object v4, v2, LX/A73;->b:LX/A72;

    invoke-virtual {v4, v1}, LX/A72;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 1624882
    invoke-virtual {v2, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1624883
    iget v2, v0, LX/A6e;->f:I

    invoke-static {v0, v2, v1, p1, v3}, LX/A6e;->a(LX/A6e;ILjava/lang/String;Ljava/lang/String;I)V

    goto :goto_4

    :cond_6
    const/4 v5, 0x1

    goto :goto_3
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 1624828
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1624829
    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->k:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->k:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1624830
    :cond_0
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624831
    iget v3, v2, LX/A6e;->h:I

    move v2, v3

    .line 1624832
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1624833
    const-string v4, "algorithm"

    invoke-virtual {v1}, LX/A6N;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624834
    const-string v4, "version"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624835
    sget-object v4, LX/A6W;->BACK_BUTTON:LX/A6W;

    iget-object v4, v4, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v0, v4, v3}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624836
    :goto_0
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "989272751122317"

    .line 1624837
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1624838
    move-object v0, v0

    .line 1624839
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 1624840
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1624841
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1624842
    return-void

    .line 1624843
    :cond_1
    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-object v3, p0, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624844
    iget v4, v3, LX/A6e;->h:I

    move v3, v4

    .line 1624845
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1624846
    const-string v5, "final_text"

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624847
    const-string v5, "algorithm"

    invoke-virtual {v2}, LX/A6N;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624848
    const-string v5, "version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624849
    sget-object v5, LX/A6W;->FINISHED:LX/A6W;

    iget-object v5, v5, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v1, v5, v4}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624850
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1624851
    const-string v2, "transliterated_text"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1624852
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1624823
    iput-object p1, p0, Lcom/facebook/transliteration/TransliterationFragment;->k:Ljava/lang/String;

    .line 1624824
    if-eqz p1, :cond_0

    .line 1624825
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1624826
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {p1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 1624827
    :cond_0
    return-void
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1624813
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1624814
    const-string v1, "trans_source_text"

    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624815
    const-string v1, "trans_language"

    iget v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624816
    const-string v1, "trans_dict_enabled"

    iget-boolean v2, p0, Lcom/facebook/transliteration/TransliterationFragment;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624817
    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    .line 1624818
    const-string v2, "trans_suggestion_1"

    iget-object p0, v1, LX/A6V;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624819
    const-string v2, "trans_suggestion_2"

    iget-object p0, v1, LX/A6V;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624820
    const-string v2, "trans_suggestion_3"

    iget-object p0, v1, LX/A6V;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624821
    const-string v2, "trans_suggestion_4"

    iget-object p0, v1, LX/A6V;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624822
    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, -0x425500e3

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1624793
    const v0, 0x7f031524

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1624794
    const v0, 0x7f0d2fb5

    invoke-static {v5, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    .line 1624795
    const v0, 0x7f0d2fb6

    invoke-static {v5, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1624796
    const v0, 0x7f0d2fbb

    invoke-static {v5, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1624797
    new-instance v0, LX/A6V;

    invoke-direct {v0, p0}, LX/A6V;-><init>(Lcom/facebook/transliteration/TransliterationFragment;)V

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    .line 1624798
    iget-object v6, p0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    const v0, 0x7f0d2fb7

    invoke-static {v5, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0d2fb8

    invoke-static {v5, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f0d2fb9

    invoke-static {v5, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0d2fba

    invoke-static {v5, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    .line 1624799
    iput-object v0, v6, LX/A6V;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1624800
    iput-object v1, v6, LX/A6V;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1624801
    iput-object v2, v6, LX/A6V;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1624802
    iput-object v3, v6, LX/A6V;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1624803
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, v6, LX/A6V;->f:Ljava/util/ArrayList;

    .line 1624804
    iget-object p1, v6, LX/A6V;->b:Lcom/facebook/resources/ui/FbTextView;

    new-instance p2, LX/A6R;

    invoke-direct {p2, v6}, LX/A6R;-><init>(LX/A6V;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1624805
    iget-object p1, v6, LX/A6V;->c:Lcom/facebook/resources/ui/FbTextView;

    new-instance p2, LX/A6S;

    invoke-direct {p2, v6}, LX/A6S;-><init>(LX/A6V;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1624806
    iget-object p1, v6, LX/A6V;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance p2, LX/A6T;

    invoke-direct {p2, v6}, LX/A6T;-><init>(LX/A6V;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1624807
    iget-object p1, v6, LX/A6V;->e:Lcom/facebook/resources/ui/FbTextView;

    new-instance p2, LX/A6U;

    invoke-direct {p2, v6}, LX/A6U;-><init>(LX/A6V;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1624808
    new-instance v0, LX/A6i;

    invoke-direct {v0, p0}, LX/A6i;-><init>(Lcom/facebook/transliteration/TransliterationFragment;)V

    iput-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->e:Landroid/text/TextWatcher;

    .line 1624809
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationFragment;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1624810
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->b:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/A6k;

    invoke-direct {v1, p0}, LX/A6k;-><init>(Lcom/facebook/transliteration/TransliterationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1624811
    iget-object v0, p0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/A6n;

    invoke-direct {v1, p0}, LX/A6n;-><init>(Lcom/facebook/transliteration/TransliterationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1624812
    const/16 v0, 0x2b

    const v1, 0x3e3bcd0e

    invoke-static {v7, v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v5
.end method
