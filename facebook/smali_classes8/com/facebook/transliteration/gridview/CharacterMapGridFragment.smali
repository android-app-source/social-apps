.class public Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static m:Ljava/lang/String;


# instance fields
.field public n:Landroid/content/Context;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/A7C;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/widget/AdapterView$OnItemClickListener;

.field public q:LX/A6X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1625665
    const-class v0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1625666
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/A6X;->a(LX/0QB;)LX/A6X;

    move-result-object p0

    check-cast p0, LX/A6X;

    iput-object p0, p1, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->q:LX/A6X;

    iput-object v1, p1, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->n:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 1625667
    const-class v0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    invoke-static {v0, p0}, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1625668
    new-instance v1, Landroid/app/Dialog;

    iget-object v0, p0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->n:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1625669
    new-instance v2, LX/A7E;

    iget-object v0, p0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->n:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->o:Ljava/util/List;

    invoke-direct {v2, v0, v3}, LX/A7E;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 1625670
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1625671
    const v0, 0x7f030277

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(I)V

    .line 1625672
    const v0, 0x7f0d0919

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 1625673
    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1625674
    iget-object v2, p0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->p:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v2, :cond_0

    .line 1625675
    iget-object v2, p0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->p:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1625676
    :cond_0
    new-instance v0, LX/A7A;

    invoke-direct {v0, p0}, LX/A7A;-><init>(Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;)V

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 1625677
    new-instance v0, LX/A7B;

    invoke-direct {v0, p0}, LX/A7B;-><init>(Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;)V

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1625678
    return-object v1
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1625679
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1625680
    iget-object v0, p0, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->q:LX/A6X;

    invoke-virtual {v0}, LX/A6X;->b()V

    .line 1625681
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x72decc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1625682
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1625683
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1625684
    const/16 v1, 0x2b

    const v2, -0x30c76d79

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
