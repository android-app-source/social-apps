.class public final Lcom/facebook/transliteration/Transliteration$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/A6e;


# direct methods
.method public constructor <init>(LX/A6e;)V
    .locals 0

    .prologue
    .line 1624441
    iput-object p1, p0, Lcom/facebook/transliteration/Transliteration$3;->a:LX/A6e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1624442
    iget-object v0, p0, Lcom/facebook/transliteration/Transliteration$3;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->l:LX/A6z;

    iget-object v1, p0, Lcom/facebook/transliteration/Transliteration$3;->a:LX/A6e;

    iget v1, v1, LX/A6e;->f:I

    const/4 v5, 0x0

    .line 1624443
    iget-object v2, v0, LX/A6z;->a:LX/A78;

    const/4 v4, 0x0

    .line 1624444
    :try_start_0
    new-instance v3, Lcom/facebook/transliteration/api/DownloadModelParams;

    const/4 v6, 0x0

    const-string p0, "transliteration_version"

    invoke-static {v2, p0, v5, v1}, LX/A78;->a(LX/A78;Ljava/lang/String;II)I

    move-result p0

    invoke-direct {v3, v5, v1, v6, p0}, Lcom/facebook/transliteration/api/DownloadModelParams;-><init>(IIZI)V

    .line 1624445
    iget-object v6, v2, LX/A78;->g:LX/11H;

    iget-object p0, v2, LX/A78;->e:LX/A71;

    invoke-virtual {v6, p0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/transliteration/api/TransliterationModelResponse;

    .line 1624446
    if-eqz v3, :cond_0

    .line 1624447
    iget-object v6, v3, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mLanguageModel:Ljava/lang/String;

    move-object v6, v6

    .line 1624448
    if-nez v6, :cond_3

    .line 1624449
    :cond_0
    iget-object v3, v2, LX/A78;->h:LX/A6X;

    invoke-virtual {v3}, LX/A6X;->f()V

    move-object v3, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1624450
    :goto_0
    move-object v2, v3

    .line 1624451
    const/4 v4, -0x1

    .line 1624452
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    move v3, v4

    .line 1624453
    :goto_1
    move v3, v3

    .line 1624454
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 1624455
    iget-object v4, v0, LX/A6z;->a:LX/A78;

    invoke-virtual {v4, v2, v5, v1, v3}, LX/A78;->a(Ljava/lang/String;III)V

    .line 1624456
    :cond_2
    return-void

    .line 1624457
    :cond_3
    :try_start_1
    iget-object v6, v3, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mLanguageModel:Ljava/lang/String;

    move-object v3, v6

    .line 1624458
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1624459
    invoke-static {v6}, LX/A78;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1624460
    move-object v3, v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1624461
    goto :goto_0

    .line 1624462
    :catch_0
    move-exception v3

    .line 1624463
    sget-object v6, LX/A78;->c:Ljava/lang/String;

    const-string p0, "Download Failed"

    invoke-static {v6, p0, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1624464
    iget-object v3, v2, LX/A78;->h:LX/A6X;

    invoke-virtual {v3}, LX/A6X;->f()V

    move-object v3, v4

    .line 1624465
    goto :goto_0

    .line 1624466
    :cond_4
    invoke-static {v0, v2}, LX/A6z;->a(LX/A6z;Ljava/lang/String;)LX/A6x;

    move-result-object v3

    check-cast v3, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;

    .line 1624467
    if-nez v3, :cond_5

    move v3, v4

    .line 1624468
    goto :goto_1

    .line 1624469
    :cond_5
    iget v3, v3, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;->mVersion:I

    goto :goto_1
.end method
