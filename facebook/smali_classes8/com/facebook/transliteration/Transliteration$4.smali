.class public final Lcom/facebook/transliteration/Transliteration$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/A6e;


# direct methods
.method public constructor <init>(LX/A6e;)V
    .locals 0

    .prologue
    .line 1624470
    iput-object p1, p0, Lcom/facebook/transliteration/Transliteration$4;->a:LX/A6e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1624471
    iget-object v0, p0, Lcom/facebook/transliteration/Transliteration$4;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->m:LX/A79;

    iget-object v1, p0, Lcom/facebook/transliteration/Transliteration$4;->a:LX/A6e;

    iget v1, v1, LX/A6e;->f:I

    .line 1624472
    iget-object v2, v0, LX/A79;->b:LX/A78;

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 1624473
    const-string v4, "transliteration_dictionary"

    .line 1624474
    sget-object v9, LX/A78;->a:LX/0Tn;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_last_download_time"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v9

    check-cast v9, LX/0Tn;

    .line 1624475
    iget-object v10, v2, LX/A78;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v11, 0x0

    invoke-interface {v10, v9, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v9

    .line 1624476
    iget-object v11, v2, LX/A78;->i:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v11

    sub-long v9, v11, v9

    sget-object v11, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v13, 0x7

    invoke-virtual {v11, v13, v14}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-lez v9, :cond_6

    .line 1624477
    const/4 v9, 0x1

    .line 1624478
    :goto_0
    move v4, v9

    .line 1624479
    if-nez v4, :cond_2

    move-object v4, v5

    .line 1624480
    :goto_1
    move-object v2, v4

    .line 1624481
    if-eqz v2, :cond_1

    .line 1624482
    invoke-static {v0, v2}, LX/A79;->a(LX/A79;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 1624483
    if-eqz v2, :cond_1

    .line 1624484
    iget-object v3, v0, LX/A79;->b:LX/A78;

    .line 1624485
    iget-object v4, v3, LX/A78;->b:LX/A77;

    invoke-virtual {v4, v1}, LX/A77;->a(I)Ljava/util/Map;

    move-result-object v6

    .line 1624486
    iget-object v4, v3, LX/A78;->b:LX/A77;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 1624487
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1624488
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1624489
    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1624490
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1624491
    invoke-static {v4}, LX/A78;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1624492
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1624493
    sget-object v10, LX/A74;->b:LX/0U1;

    .line 1624494
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 1624495
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1624496
    sget-object v10, LX/A74;->c:LX/0U1;

    .line 1624497
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 1624498
    invoke-virtual {v9, v10, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624499
    sget-object v5, LX/A74;->d:LX/0U1;

    .line 1624500
    iget-object v10, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v10

    .line 1624501
    invoke-virtual {v9, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624502
    const-string v4, "dictionary_table"

    const/4 v5, 0x0

    const v10, -0x542eefce

    invoke-static {v10}, LX/03h;->a(I)V

    invoke-virtual {v7, v4, v5, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v4, 0x74935845

    invoke-static {v4}, LX/03h;->a(I)V

    goto :goto_2

    .line 1624503
    :cond_1
    return-void

    .line 1624504
    :cond_2
    :try_start_0
    const-string v4, "transliteration_dictionary"

    invoke-static {v2, v4, v3, v1}, LX/A78;->a(LX/A78;Ljava/lang/String;II)I

    move-result v6

    .line 1624505
    new-instance v4, Lcom/facebook/transliteration/api/DownloadModelParams;

    const/4 v7, 0x1

    invoke-direct {v4, v3, v1, v7, v6}, Lcom/facebook/transliteration/api/DownloadModelParams;-><init>(IIZI)V

    .line 1624506
    iget-object v7, v2, LX/A78;->g:LX/11H;

    iget-object v8, v2, LX/A78;->e:LX/A71;

    invoke-virtual {v7, v8, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/transliteration/api/TransliterationModelResponse;

    .line 1624507
    if-eqz v4, :cond_3

    .line 1624508
    iget-object v7, v4, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mDictionary:Ljava/lang/String;

    move-object v7, v7

    .line 1624509
    if-nez v7, :cond_4

    .line 1624510
    :cond_3
    iget-object v4, v2, LX/A78;->h:LX/A6X;

    invoke-virtual {v4}, LX/A6X;->f()V

    move-object v4, v5

    .line 1624511
    goto/16 :goto_1

    .line 1624512
    :cond_4
    const-string v7, "transliteration_dictionary"

    .line 1624513
    sget-object v9, LX/A78;->a:LX/0Tn;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_last_download_time"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v9

    check-cast v9, LX/0Tn;

    .line 1624514
    iget-object v10, v2, LX/A78;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v10

    iget-object v11, v2, LX/A78;->i:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v11

    invoke-interface {v10, v9, v11, v12}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v9

    invoke-interface {v9}, LX/0hN;->commit()V

    .line 1624515
    iget-object v7, v4, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mVersion:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v7, v6, :cond_5

    .line 1624516
    const-string v6, "transliteration_dictionary"

    iget-object v7, v4, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mVersion:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v2, v6, v3, v1, v7}, LX/A78;->b(LX/A78;Ljava/lang/String;III)V

    .line 1624517
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "{ \"dictionary\" : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v4, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mDictionary:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " }"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v4, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1624518
    goto/16 :goto_1

    .line 1624519
    :catch_0
    move-exception v4

    .line 1624520
    sget-object v6, LX/A78;->c:Ljava/lang/String;

    const-string v7, "Download Failed"

    invoke-static {v6, v7, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1624521
    iget-object v4, v2, LX/A78;->h:LX/A6X;

    invoke-virtual {v4}, LX/A6X;->f()V

    move-object v4, v5

    .line 1624522
    goto/16 :goto_1

    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_0
.end method
