.class public Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/A6x;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# instance fields
.field public mLocale:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "locale"
    .end annotation
.end field

.field public mModel:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "model"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/transliteration/algorithms/unigram/UnigramModelDataItem;",
            ">;"
        }
    .end annotation
.end field

.field public mVersion:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1625187
    const-class v0, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModelDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1625188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
