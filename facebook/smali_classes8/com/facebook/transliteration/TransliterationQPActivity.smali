.class public Lcom/facebook/transliteration/TransliterationQPActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private p:LX/A6h;

.field private q:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private r:LX/A6X;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1624937
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1624938
    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->p:LX/A6h;

    invoke-virtual {v1}, LX/A6h;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1624939
    invoke-virtual {p0}, Lcom/facebook/transliteration/TransliterationQPActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "enable"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1624940
    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/A6h;->b:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v2

    .line 1624941
    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v2

    if-eq v2, v1, :cond_2

    .line 1624942
    :cond_0
    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/A6h;->b:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1624943
    iget-object v2, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->r:LX/A6X;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/A6X;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1624944
    :cond_2
    return-void
.end method

.method private a(LX/A6h;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/A6X;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1624945
    iput-object p1, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->p:LX/A6h;

    .line 1624946
    iput-object p2, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1624947
    iput-object p3, p0, Lcom/facebook/transliteration/TransliterationQPActivity;->r:LX/A6X;

    .line 1624948
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/transliteration/TransliterationQPActivity;

    invoke-static {v2}, LX/A6h;->b(LX/0QB;)LX/A6h;

    move-result-object v0

    check-cast v0, LX/A6h;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/A6X;->a(LX/0QB;)LX/A6X;

    move-result-object v2

    check-cast v2, LX/A6X;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/transliteration/TransliterationQPActivity;->a(LX/A6h;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/A6X;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1624949
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1624950
    invoke-static {p0, p0}, Lcom/facebook/transliteration/TransliterationQPActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1624951
    invoke-direct {p0}, Lcom/facebook/transliteration/TransliterationQPActivity;->a()V

    .line 1624952
    invoke-virtual {p0}, Lcom/facebook/transliteration/TransliterationQPActivity;->finish()V

    .line 1624953
    return-void
.end method
