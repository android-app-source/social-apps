.class public Lcom/facebook/transliteration/api/TransliterationModelResponse;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# instance fields
.field public final mDictionary:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dictionary"
    .end annotation
.end field

.field public final mLanguageModel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "language_model"
    .end annotation
.end field

.field public final mVersion:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1625388
    const-class v0, Lcom/facebook/transliteration/api/TransliterationModelResponseDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1625389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625390
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mVersion:Ljava/lang/Integer;

    .line 1625391
    iput-object v1, p0, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mLanguageModel:Ljava/lang/String;

    .line 1625392
    iput-object v1, p0, Lcom/facebook/transliteration/api/TransliterationModelResponse;->mDictionary:Ljava/lang/String;

    .line 1625393
    return-void
.end method
