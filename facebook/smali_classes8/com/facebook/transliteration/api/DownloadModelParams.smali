.class public Lcom/facebook/transliteration/api/DownloadModelParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(IIZI)V
    .locals 1

    .prologue
    .line 1625381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625382
    iput p1, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->a:I

    .line 1625383
    iput p2, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->b:I

    .line 1625384
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->c:I

    .line 1625385
    iput p4, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->d:I

    .line 1625386
    return-void

    .line 1625387
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1625375
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1625376
    iget v0, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1625377
    iget v0, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1625378
    iget v0, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1625379
    iget v0, p0, Lcom/facebook/transliteration/api/DownloadModelParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1625380
    return-void
.end method
