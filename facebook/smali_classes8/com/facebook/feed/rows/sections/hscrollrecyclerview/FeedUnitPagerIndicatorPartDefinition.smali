.class public Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        "E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/99i",
        "<TT;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/1Ua;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1447533
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1447534
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->a:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    .line 1447535
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1447536
    invoke-static {}, LX/1UY;->e()LX/1UY;

    move-result-object v0

    .line 1447537
    iput v1, v0, LX/1UY;->d:F

    .line 1447538
    move-object v0, v0

    .line 1447539
    iput v1, v0, LX/1UY;->b:F

    .line 1447540
    move-object v0, v0

    .line 1447541
    invoke-virtual {p3}, LX/1V7;->c()F

    move-result v1

    invoke-virtual {p3}, LX/1V7;->e()F

    move-result v2

    sub-float/2addr v1, v2

    .line 1447542
    iput v1, v0, LX/1UY;->c:F

    .line 1447543
    move-object v0, v0

    .line 1447544
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->c:LX/1Ua;

    .line 1447545
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;
    .locals 6

    .prologue
    .line 1447546
    const-class v1, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    monitor-enter v1

    .line 1447547
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1447548
    sput-object v2, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1447549
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1447550
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1447551
    new-instance p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V7;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;)V

    .line 1447552
    move-object v0, p0

    .line 1447553
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1447554
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447555
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1447556
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b()LX/99l;
    .locals 1

    .prologue
    .line 1447557
    new-instance v0, LX/99l;

    invoke-direct {v0}, LX/99l;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1447558
    sget-object v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1447559
    check-cast p2, LX/99i;

    .line 1447560
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->a:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    new-instance v2, LX/99m;

    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1447561
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1447562
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1447563
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1447564
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v4

    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1447565
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1447566
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v5, p2, LX/99i;->b:LX/8yy;

    invoke-direct {v2, v3, v4, v0, v5}, LX/99m;-><init>(Ljava/lang/String;IILX/8yy;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1447567
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->c:LX/1Ua;

    sget-object v4, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1447568
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1447569
    const/4 v0, 0x1

    return v0
.end method
