.class public Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/99m;",
        "LX/99n;",
        "LX/1Pr;",
        "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1447679
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1447680
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->a:Landroid/content/Context;

    .line 1447681
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;
    .locals 4

    .prologue
    .line 1447682
    const-class v1, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    monitor-enter v1

    .line 1447683
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1447684
    sput-object v2, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1447685
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1447686
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1447687
    new-instance p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1447688
    move-object v0, p0

    .line 1447689
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1447690
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447691
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1447692
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/99m;LX/99n;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;)V
    .locals 8

    .prologue
    .line 1447693
    iget-object v0, p0, LX/99m;->d:LX/8yy;

    instance-of v0, v0, LX/99l;

    if-nez v0, :cond_0

    .line 1447694
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong PageChangeListener passed in as a prop"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447695
    :cond_0
    iget-object v0, p0, LX/99m;->d:LX/8yy;

    check-cast v0, LX/99l;

    .line 1447696
    iput-object p2, v0, LX/99l;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;

    .line 1447697
    iget-object v1, p1, LX/99n;->c:LX/99o;

    iget v1, v1, LX/99o;->e:I

    .line 1447698
    iput v1, v0, LX/99l;->c:I

    .line 1447699
    iget-object v1, p2, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->b:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    move-object v7, v1

    .line 1447700
    iget-object v6, v0, LX/99l;->e:LX/6Vj;

    .line 1447701
    iget v1, v6, LX/6Vj;->a:I

    move v1, v1

    .line 1447702
    iget v2, v6, LX/6Vj;->a:I

    move v2, v2

    .line 1447703
    iget v3, v0, LX/99l;->a:I

    iget v4, v0, LX/99l;->b:I

    iget-object v0, p1, LX/99n;->c:LX/99o;

    iget v5, v0, LX/99o;->e:I

    move-object v0, p2

    invoke-static/range {v0 .. v6}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->b(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;IIIIILX/6Vj;)V

    .line 1447704
    iget v0, p1, LX/99n;->b:F

    iget-object v1, p1, LX/99n;->a:Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p1, LX/99n;->c:LX/99o;

    .line 1447705
    iget v3, v2, LX/99o;->a:I

    invoke-virtual {v7, v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setFillColor(I)V

    .line 1447706
    iget v3, v2, LX/99o;->b:I

    invoke-virtual {v7, v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setPageColor(I)V

    .line 1447707
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setStrokeWidth(F)V

    .line 1447708
    invoke-virtual {v7, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setRadius(F)V

    .line 1447709
    const/4 v3, 0x2

    invoke-virtual {v7, v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setArrowStrokeWidth(I)V

    .line 1447710
    iget v3, v2, LX/99o;->a:I

    invoke-virtual {v7, v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setArrowColor(I)V

    .line 1447711
    const/16 v3, 0x19

    .line 1447712
    iput v3, v7, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->A:I

    .line 1447713
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1447714
    iput v3, v7, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->y:F

    .line 1447715
    iget v3, v2, LX/99o;->e:I

    .line 1447716
    iput v3, v7, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    .line 1447717
    const/high16 v3, 0x40800000    # 4.0f

    .line 1447718
    iput v3, v7, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    .line 1447719
    invoke-static {v7, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1447720
    return-void
.end method

.method public static b(IIII)I
    .locals 1

    .prologue
    .line 1447721
    if-le p2, p0, :cond_2

    .line 1447722
    add-int/lit8 v0, p3, -0x1

    if-ge p1, v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    move v0, p1

    .line 1447723
    :goto_0
    if-nez p2, :cond_1

    .line 1447724
    const/4 v0, 0x0

    .line 1447725
    :cond_1
    return v0

    .line 1447726
    :cond_2
    if-ge p2, p0, :cond_4

    .line 1447727
    if-lez p1, :cond_3

    add-int/lit8 p1, p1, -0x1

    :cond_3
    move v0, p1

    goto :goto_0

    :cond_4
    move v0, p1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;IIIIILX/6Vj;)V
    .locals 2

    .prologue
    .line 1447728
    iput p2, p6, LX/6Vj;->a:I

    .line 1447729
    if-eqz p0, :cond_1

    .line 1447730
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->b:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    move-object v1, v0

    .line 1447731
    if-eqz p1, :cond_0

    add-int/lit8 v0, p5, -0x1

    if-ne p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1447732
    :goto_0
    invoke-virtual {v1, p4}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCount(I)V

    .line 1447733
    invoke-virtual {v1, p3, p2, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(IIZ)V

    .line 1447734
    :cond_1
    return-void

    .line 1447735
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1447736
    check-cast p2, LX/99m;

    check-cast p3, LX/1Pr;

    .line 1447737
    iget-object v0, p2, LX/99m;->d:LX/8yy;

    instance-of v0, v0, LX/99l;

    if-nez v0, :cond_0

    .line 1447738
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong PageChangeListener passed in as a prop"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447739
    :cond_0
    iget-object v0, p2, LX/99m;->d:LX/8yy;

    check-cast v0, LX/99l;

    .line 1447740
    iget v1, p2, LX/99m;->b:I

    .line 1447741
    iput v1, v0, LX/99l;->a:I

    .line 1447742
    iget v1, p2, LX/99m;->c:I

    .line 1447743
    iput v1, v0, LX/99l;->b:I

    .line 1447744
    iget-object v3, p2, LX/99m;->e:LX/99o;

    .line 1447745
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->a:Landroid/content/Context;

    .line 1447746
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 1447747
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v2, v4

    move v4, v2

    .line 1447748
    const/16 p1, 0x8

    const/4 v1, 0x0

    .line 1447749
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0969

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1447750
    new-instance v6, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1447751
    iget v2, v3, LX/99o;->c:I

    invoke-virtual {v6, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 1447752
    new-array v7, p1, [F

    move v2, v1

    .line 1447753
    :goto_0
    if-ge v2, p1, :cond_2

    .line 1447754
    const/4 v1, 0x4

    if-lt v2, v1, :cond_1

    int-to-float v1, v5

    :goto_1
    aput v1, v7, v2

    .line 1447755
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1447756
    :cond_1
    const v1, 0x3dcccccd    # 0.1f

    goto :goto_1

    .line 1447757
    :cond_2
    invoke-virtual {v6, v7}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 1447758
    iget v1, v3, LX/99o;->d:I

    invoke-virtual {v6, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1447759
    move-object v5, v6

    .line 1447760
    new-instance v1, LX/6Vk;

    iget-object v2, p2, LX/99m;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, LX/6Vk;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v1}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Vj;

    .line 1447761
    iput-object v1, v0, LX/99l;->e:LX/6Vj;

    .line 1447762
    iget v1, v0, LX/99l;->a:I

    iget v2, v3, LX/99o;->e:I

    rem-int v2, v1, v2

    .line 1447763
    iget-object v1, v0, LX/99l;->e:LX/6Vj;

    .line 1447764
    iget v6, v1, LX/6Vj;->a:I

    move v1, v6

    .line 1447765
    iget-object v6, v0, LX/99l;->e:LX/6Vj;

    if-ltz v1, :cond_3

    iget v0, v0, LX/99l;->a:I

    if-lez v0, :cond_3

    move v0, v1

    .line 1447766
    :goto_2
    iput v0, v6, LX/6Vj;->a:I

    .line 1447767
    new-instance v0, LX/99n;

    invoke-direct {v0, v5, v4, v3}, LX/99n;-><init>(Landroid/graphics/drawable/GradientDrawable;FLX/99o;)V

    return-object v0

    :cond_3
    move v0, v2

    .line 1447768
    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5b8a7e67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1447769
    check-cast p1, LX/99m;

    check-cast p2, LX/99n;

    check-cast p4, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;

    invoke-static {p1, p2, p4}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->a(LX/99m;LX/99n;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;)V

    const/16 v1, 0x1f

    const v2, 0x16382460

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1447770
    check-cast p1, LX/99m;

    .line 1447771
    iget-object v0, p1, LX/99m;->d:LX/8yy;

    instance-of v0, v0, LX/99l;

    if-nez v0, :cond_0

    .line 1447772
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong PageChangeListener passed in as a prop"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447773
    :cond_0
    iget-object v0, p1, LX/99m;->d:LX/8yy;

    check-cast v0, LX/99l;

    .line 1447774
    const/4 v1, 0x0

    .line 1447775
    iput-object v1, v0, LX/99l;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;

    .line 1447776
    return-void
.end method
