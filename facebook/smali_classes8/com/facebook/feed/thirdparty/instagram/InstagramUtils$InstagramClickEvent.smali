.class public final Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# direct methods
.method public constructor <init>(LX/99r;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 2
    .param p2    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1447895
    const-string v0, "link_click"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1447896
    const-string v0, "instagram_fb"

    .line 1447897
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1447898
    const-string v0, "type"

    invoke-virtual {p1}, LX/99r;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1447899
    if-eqz p2, :cond_0

    .line 1447900
    const-string v0, "story_id"

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1447901
    :cond_0
    const-string v0, "instagram_url"

    invoke-virtual {p0, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1447902
    return-void
.end method
