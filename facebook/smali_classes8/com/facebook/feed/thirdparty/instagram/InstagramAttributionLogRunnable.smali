.class public Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private final c:LX/0Zb;

.field private final d:Lcom/facebook/analytics/logger/HoneyClientEvent;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1447782
    const-string v0, "content://%s.provider.AttributionIdProvider"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Zb;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1447783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1447784
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->b:Landroid/content/ContentResolver;

    .line 1447785
    iput-object p2, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->c:LX/0Zb;

    .line 1447786
    iput-object p3, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1447787
    return-void
.end method

.method private static a(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1447788
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "aid"

    aput-object v1, v2, v0

    .line 1447789
    :try_start_0
    sget-object v1, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1447790
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1447791
    const-string v0, "aid"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1447792
    if-eqz v1, :cond_0

    .line 1447793
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1447794
    :cond_0
    :goto_0
    return-object v0

    .line 1447795
    :cond_1
    if-eqz v1, :cond_2

    .line 1447796
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v6

    .line 1447797
    goto :goto_0

    .line 1447798
    :catch_0
    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_2

    .line 1447799
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1447800
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 1447801
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1447802
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1447803
    iget-object v0, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "advertising_id"

    iget-object v2, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->b:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1447804
    iget-object v0, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->c:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1447805
    return-void
.end method
