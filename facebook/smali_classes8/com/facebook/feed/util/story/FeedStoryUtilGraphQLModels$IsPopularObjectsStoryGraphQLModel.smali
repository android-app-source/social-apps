.class public final Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/9A9;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x36b1ce81
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1449076
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1449039
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1449040
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1449041
    return-void
.end method

.method private a(LX/3Sb;)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mutateAttachmentsPRIVATE"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1449042
    iput-object p1, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->e:LX/3Sb;

    .line 1449043
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1449044
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1449045
    if-eqz v0, :cond_0

    .line 1449046
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const v3, -0x6b59ddfb

    invoke-static {v0, v1, v2, p1, v3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/3Sb;I)V

    .line 1449047
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1449048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1449049
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->d()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1449050
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1449051
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1449052
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1449053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1449054
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1449055
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->d()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1449056
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->d()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1449057
    if-eqz v1, :cond_0

    .line 1449058
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;

    .line 1449059
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->e:LX/3Sb;

    .line 1449060
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1449061
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1449062
    new-instance v0, LX/9AB;

    invoke-direct {v0, p1}, LX/9AB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1449063
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1449064
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1449065
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1449066
    check-cast p2, LX/3Sb;

    invoke-direct {p0, p2}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->a(LX/3Sb;)V

    .line 1449067
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1449068
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1449069
    new-instance v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;-><init>()V

    .line 1449070
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1449071
    return-object v0
.end method

.method public final d()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1449072
    iget-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x6b59ddfb

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->e:LX/3Sb;

    .line 1449073
    iget-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$IsPopularObjectsStoryGraphQLModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1449074
    const v0, 0xf118dc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1449075
    const v0, 0x4c808d5

    return v0
.end method
