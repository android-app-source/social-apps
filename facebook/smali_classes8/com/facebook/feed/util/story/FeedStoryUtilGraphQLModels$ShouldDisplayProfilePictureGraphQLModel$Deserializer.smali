.class public final Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1449311
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    new-instance v1, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1449312
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1449313
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1449314
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1449315
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1449316
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1449317
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1449318
    :goto_0
    move v1, v2

    .line 1449319
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1449320
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1449321
    new-instance v1, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;-><init>()V

    .line 1449322
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1449323
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1449324
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1449325
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1449326
    :cond_0
    return-object v1

    .line 1449327
    :cond_1
    const-string p0, "has_comprehensive_title"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1449328
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    .line 1449329
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_a

    .line 1449330
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1449331
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1449332
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1449333
    const-string p0, "actors"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1449334
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1449335
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_3

    .line 1449336
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_3

    .line 1449337
    invoke-static {p1, v0}, LX/9AM;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1449338
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1449339
    :cond_3
    invoke-static {v10, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v10

    move v10, v10

    .line 1449340
    goto :goto_1

    .line 1449341
    :cond_4
    const-string p0, "all_substories"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1449342
    invoke-static {p1, v0}, LX/9AO;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1449343
    :cond_5
    const-string p0, "attached_story"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1449344
    invoke-static {p1, v0}, LX/9AP;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1449345
    :cond_6
    const-string p0, "attachments"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1449346
    invoke-static {p1, v0}, LX/9AL;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1449347
    :cond_7
    const-string p0, "feedback"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1449348
    invoke-static {p1, v0}, LX/9AQ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1449349
    :cond_8
    const-string p0, "message"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1449350
    invoke-static {p1, v0}, LX/9AR;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1449351
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1449352
    :cond_a
    const/4 v11, 0x7

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1449353
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1449354
    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1449355
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1449356
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1449357
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1449358
    if-eqz v1, :cond_b

    .line 1449359
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 1449360
    :cond_b
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1449361
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
